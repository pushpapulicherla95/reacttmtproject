import React, { Component } from "react";
import { connect } from "react-redux";
import { userLogin } from "../../service/login/action";
import SocialButton from "../common/SocialButton";
import { NavLink } from "react-router-dom";
import axios from "axios";
import queryString from "query-string";
import { withRouter } from "react-router-dom";
import TwitterLogin from  'react-twitter-auth'

// import Registration from "../registration/registration";
// import browserHistory from "../../view/App";
// import Viewprofile from "../viewprofile/Viewprofile";
// import Header from "../common/Header";
import { LinkedIn } from "react-linkedin-login-oauth2";
import URL from "../../asset/configUrl";
import { toastr } from "react-redux-toastr";
// import InstagramLogin from "react-instagram-login";

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      errors: {
        email: "",
        password: ""
      },
      emailValid: false,
      passwordValid: false,
      formValid: false,
      submitted: false,
      type: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    // this.onLoginSuccess = this.onLoginSuccess.bind(this);
    // this.onLoginFailure = this.onLoginFailure.bind(this);
  }
  handleSuccess = data => {
    console.log("data", data);
  };
  componentDidMount = () => {
    // const parsed = queryString.parse(this.props.location.search);
    // console.log(this.props.location.search, parsed, window.location);
      console.log("in side component did mount");
      axios
      .get("https://www.ifixit.com/api/2.0/categories")
      .then((response) => {
          console.log("response>>>>",response);
      })
        // this.setState({categories: Object.entries(response.data).map(( [k, v] ) => ({ [k]: v }) )}});
  }

  handleSocialLogin = user => {
    console.log("user", user);

    const payload = {
      firstName: user._profile.firstName,
      lastName: user._profile.lastName,
      email: user._profile.email ? user._profile.email : "",
      mobileNo: " ",
      provider: user._provider
    };
    const configs = {
      method: "post",
      url: URL.SOCIAL_LOGIN,
      data: payload,
      headers: {
        "Content-Type": "application/json"
      }
    };
    axios(configs).then(res => {
      sessionStorage.setItem("loginDetails", JSON.stringify(res.data));
      this.props.history.push("/mainmenu");
      console.log("response", res);
      // https://api.instagram.com/v1/users/self/?access_token
    });
  };

  handleSocialLoginFailure = err => {
    console.log(err);
  };

  logout() {
    const { logged, currentProvider } = this.state;
    if (logged && currentProvider) {
      this.nodes[currentProvider].props.triggerLogout();
    }
  }

  componentWillMount() {
    this.setState({ type: this.props.location.state });
    const parsed = queryString.parse(this.props.location.search);
    console.log("-->", this.props.location.search, parsed, window.location);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    let userinfo = {
      email: this.state.email,
      password: this.state.password
    };
   this.props.userLogin(userinfo);
  }

  componentWillReceiveProps = nextProps => {
    console.log("nextprops", nextProps);
     const { userInfo } = nextProps.loginDetails;
    if (
      nextProps.loginDetails.userInfo &&
      nextProps.loginDetails.status == "success" &&
      this.state.type == undefined
    ) {
      const { userInfo } = nextProps.loginDetails;

      console.log("userInfo", userInfo);
      if (userInfo.userType === "patient") {
        this.props.history.push("/mainmenu");
      }
    } else {
      this.props.history.push("/login/patient");
      toastr.error("Error", "Enter valid details");
    }
    if (userInfo.userType != "patient") {
      toastr.error("Error", "Sorry this Patient login");
    }
  };
  //validation
  validateField(fieldName, value) {
    const { errors, emailValid, passwordValid } = this.state;
    let emailvalid = emailValid;
    let passwordvalid = passwordValid;
    let fieldValidationErrors = errors;
    switch (fieldName) {
      case "email":
        emailvalid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        fieldValidationErrors.email = emailvalid
          ? ""
          : "Please enter valid email address.";
        break;
      case "password":
        passwordvalid = value.length !== 0;
        fieldValidationErrors.password = passwordvalid
          ? ""
          : "Please enter valid password.";
        break;

      default:
        break;
    }
    this.setState({
      errors: fieldValidationErrors,
      emailValid: emailvalid,
      passwordValid: emailvalid
    });
  }



  onSuccess = (response) => {
    const token = response.headers.get('x-auth-token');
    response.json().then(user => {
      if (token) {
        this.setState({isAuthenticated: true, user: user, token: token});
      }
    });
  };
  
  onFailed = (error) => {
    // alert(error);
  };

  render() {
    const { email, password, errors } = this.state;
    // console.log("this.prosp.state",this.props)
    return (
      <div>
        <section className="loginSection">
          <a href="/" className="logoDiv">
            <img src="../images/logo.jpg" />
          </a>
          <div className="loginInner">
            <div className="loginFormContent">
              <h4 className="popupTitle">Login</h4>
              <form onSubmit={this.handleSubmit}>
                <div className="form-group emailIcon">
                  <input
                    type="text"
                    className="form-control commonInput"
                    placeholder="Email"
                    name="email"
                    value={email}
                    onChange={this.handleChange}
                  />
                  <div style={{ color: "red" }}>{errors.email}</div>
                </div>
                <div className="form-group pass">
                  <input
                    type="password"
                    className="form-control commonInput"
                    placeholder="Password"
                    name="password"
                    value={password}
                    onChange={this.handleChange}
                  />
                  <div style={{ color: "red" }}>{errors.password}</div>
                </div>
                <div className="form-group ">
                  <NavLink to="/login/forgot" className="forgotPass text-right">
                    Forgot Password ?
                  </NavLink>
                </div>
                <div className="form-group">
                  <a href="#">
                    <button type="submit" className="btn loginBtn">
                      Login
                    </button>
                  </a>
                </div>
                <p className="linkToReg">
                  Don't have an account ?
                  <NavLink to="/register/patient"> Register Here</NavLink>
                </p>
              </form>
            </div>

            <div className="socialLogin">
              <h4 className="popupTitle">Login with Social Network </h4>
              <ul>
                <li>
                  {/* <InstagramLogin
                    clientId="d4c608d8db3d43259142f0f52bf6c66e"
                    buttonText="Login"
                    onSuccess={this.responseInstagram}
                    onFailure={this.responseInstagram}
                  /> */}

                  {/* <SocialButton
                    provider="linkedin"
                    appId="81i5wqfm3ijz8w"
                    onLoginSuccess={this.handleSocialLogin}
                    onLoginFailure={this.handleSocialLoginFailure}
                  >
                    Login with linkedin
                  </SocialButton> */}

                  <SocialButton
                    provider="facebook"
                    appId="1895168490540078"
                    onLoginSuccess={this.handleSocialLogin}
                    // onLoginFailure={this.onLoginFailure}
                    /* onLogoutSuccess={this.onLogoutSuccess} */
                  >
                    <a title="Facebook" className="fb">
                      <img src="../images/facebook-logo.png" />
                    </a>
                  </SocialButton>
                </li>
                <li>
                  <SocialButton
                    provider="google"
                    appId="542596200490-86c48vs66bgrddho89il30koegpoca43.apps.googleusercontent.com"
                    onLoginSuccess={this.handleSocialLogin}
                    onLoginFailure={this.handleSocialLoginFailure}
                  >
                    <a title="Google Plus" className="google">
                      <img src="../images/googlePlus.png" />
                    </a>
                  </SocialButton>
                </li>
                <li>
                  <SocialButton
                    provider="instagram"
                    appId="d4c608d8db3d43259142f0f52bf6c66e"
                    onLoginSuccess={this.handleSocialLogin}
                    /* onLoginFailure={handleSocialLoginFailure} */
                    redirect="https://localhost:3000/login/patient"
                  >
                    <a title="Linked In" className="linked">
                      <img src="../images/instagram.png" />
                    </a>
                  </SocialButton>
                </li>
                <li>
                  <a href="" title="We Chat" className="wechat">
                    <img src="../images/wechat.png" />
                  </a>
                </li>
                <li>
                  <SocialButton
                    provider="amazon"
                    appId="amzn1.application-oa2-client.de2ccae83e3f463b966faaa519d5c040"
                    onLoginSuccess={this.handleSocialLogin}
                    onLoginFailure={this.handleSocialLoginFailure}
                  >
                    <a title="Amazon" className="amazon">
                      <img src="../images/amazon.png" />
                    </a>
                  </SocialButton>
                </li>
                {/* <li>
                  <a href="" title="Flickr" className="flickr">
                    <img src="../images/flickr-.png" />
                  </a>
                </li> */}
                <li>
                  {/* <a href="" title="Twitter" className="twit">
                    <img src="../images/twitter-logo.png" />
                  </a> */
                  }
                  <TwitterLogin loginUrl="http://localhost:3000/api/v1/auth/twitter"
                    onFailure={this.onFailed} onSuccess={this.onSuccess}
                    requestTokenUrl="http://localhost:3000/api/v1/auth/twitter/reverse"/>
                </li>
                <li>
                  <LinkedIn
                    clientId="81vdzvq642rd12"
                    onFailure={this.handleFailure}
                    onSuccess={this.handleSocialLogin}
                    redirectUri="https://localhost:3000/"
                    className="btntransparent"
                  >
                    <a href="" title="Twitter" className="twit">
                      <img src="../images/linkedin-logo1.png" />
                    </a>
                  </LinkedIn>
                </li>

                {/* <li>
                  <a href="" title="Yahoo" className="yahoo">
                    <img src="../images/yahoo-big-logo.png" />
                  </a>
                </li> */}
              </ul>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loginDetails: state.loginReducer.loginDetails,
  userLoginStatus: state.loginReducer.userLoginStatus
});

const mapDispatchToProps = dispatch => ({
  userLogin: userinfo => dispatch(userLogin(userinfo))
});
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Login)
);
