import React, { Component } from "react";
import { addTextSymptom, getTextSymptomList, searchSymptoms, updateSymptoms, updateSymptomMain, symptomMainList, getQuestionAndOptionList, updateQuestion, updateOptionFunction ,updateSymptomMainFunction , deleteOption , deleteQuestion} from "../../../service/admin/TextRobo/action";
import { connect } from "react-redux";
import TextRobotSecondView from "./TextRobotSecondView";
import SymptomTableView from "./SymptomTableView";
import { CSSTransition } from 'react-transition-group';
import { convertToRaw, EditorState, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { stateToHTML } from 'draft-js-export-html';
import { stateFromHTML } from 'draft-js-import-html';
import MultipleSelect from "react-select";
import Lottie from 'react-lottie';
import animationData from "../admindashboard/lotties/tick.json"

const defaultOptionsEditor = { options: ['inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 'link', 'embedded', 'image', 'remove', 'history'] }

const components = {
    DropdownIndicator: null
};

const createOption = label => ({
    label,
    value: label
});

const buttonStyle = {
    display: 'block',
    margin: '10px auto'
  };

  const defaultOptions = {
    loop: true,
    autoplay: true, 
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  };

class TextRobot extends Component {
    constructor(props) {
        super(props);
        this.state = {
            symptoms: "",
            question: "",
            answer: EditorState.createEmpty(),
            causes: "",
            treatment: "",
            diagnosis: "",
            prevention: "",
            FAQ: [
                {
                    question: "",
                    answer: ""
                }
            ],
            tabId: "1",
            summary_description:  EditorState.createEmpty(),
            otherNames: "",
            showList: false,
            suggestionSymptoms: "",
            symptomCheckerArray:[{}],
            symptomCheckerMainQues:null,
            optionArrayCause:false,
            tabMainParent: "cause",
            showTextRobot : true,
            widthChange:false,
            causeSelected : false,
            selectedSymptomMainId: "",
            selectedOptionValue:"",

            inputValue: "",
            value: [],

            isAnimationView: false
        }
        this.timeout = 0;
    }
    componentWillMount() {
        this.props.getTextSymptomList();
        this.props.symptomMainList();
        this.props.dispatch({type:"QUESTION_AND_OPTION_LIST_SUCCESS",payload:null})
    }
    componentWillReceiveProps(nextProps) {
        if ((nextProps.searchSymptomsResult && nextProps.searchSymptomsResult.symptoms) && nextProps.searchSymptomsResult != this.props.searchSymptomsResult ) {
            const { symptoms } = nextProps.searchSymptomsResult;
            this.setState({
                symptoms: symptoms[0].symptoms,
                causes: this.utilCheckContent(null,symptoms[0].causes),
                summary_description: EditorState.createWithContent(stateFromHTML(symptoms[0].summary_description)),
                diagnosis: this.utilCheckContent(null,symptoms[0].diagnosis),
                treatment: this.utilCheckContent(null,symptoms[0].treatment),
                prevention: this.utilCheckContent(null,symptoms[0].prevention), 
                FAQ: symptoms[0].FAQ
            },()=>{
                let tabTitle = this.state.tabTitle;
                this.setState({
                    question: this.state[tabTitle] && this.state[tabTitle].question,
                    answer: this.state[tabTitle] && this.state[tabTitle].answer
                })
            });
        }

    }
    handleFaq = (index, e) => {
        const updatedArray = [...this.state.FAQ];
        updatedArray[index] = e.target.name == "question" ? {
            "question": e.target.value,
            answer: updatedArray[index].answer
        } : {
                "answer": e.target.value,
                question: updatedArray[index].question
            };
        this.setState({
            FAQ: updatedArray,
        });
    }
    deleteFaq = (index, value) => {
        const array = [...this.state.FAQ];
        for (var i in array) {
            if (array[i] == value) {
                array.splice(i, 1);
                break;
            }
        }
        this.setState({ FAQ: array })
    }
    addfaq = (e) => {
        e.preventDefault();
        const updatedArray = [...this.state.FAQ];
        updatedArray.push({ question: "", answer: "" })
        this.setState({ FAQ: updatedArray })

    }

    utilCheckContent = (tabType, htmlData) => {
        if (tabType)
            return (this.state[tabType] && this.state[tabType].answer && { ...this.state[tabType], answer: stateToHTML(this.state[tabType].answer.getCurrentContent()).replace(/([‒,↩])+/g,"") }) || { question: "", answer: "" }
        else
            return htmlData && { ...htmlData, answer: EditorState.createWithContent(stateFromHTML(htmlData.answer)) }
    }
    
    addSymptoms = (e) => {
        e.preventDefault();
        const { causes, FAQ, diagnosis, treatment, prevention, otherNames, summary_description,symptoms } = this.state;
        const textdata = {
            symptoms: symptoms,
            causes: this.utilCheckContent("causes"),
            summary_description: (summary_description && stateToHTML(summary_description.getCurrentContent()) ) || "",
            diagnosis: this.utilCheckContent("diagnosis"),
            treatment: this.utilCheckContent("treatment"),
            prevention: this.utilCheckContent("prevention"),
            FAQ: FAQ,
            "otherNames": ""
        }
        this.state.causeSelected ?  this.props.updateSymptoms(textdata) : this.props.addTextSymptom(textdata).then(()=>{
            this.setState({
                symptoms:symptoms,
                showList: false,
                causeSelected:true
            });
            document.getElementById("causeInput").style.backgroundColor = "rgb(238, 238, 238)"
        }) 
    }

    handleChange = (e) => {
        e.preventDefault();
        this.setState({ [e.target.name]: e.target.value })
        this.setState({ [this.state.tabTitle]: e.target.name == "question" ? Object.assign({}, { question: e.target.value, answer: this.state.answer }) : Object.assign({}, { question: this.state.question, answer: e.target.value }) })

    }

    handleChangeEditorDraft = (e) => {
        this.setState({ answer : e })
        this.setState({ [this.state.tabTitle]: Object.assign({}, { question: this.state.question, answer: e }) })
    }

    handleChangeDefault = (e) => {
        e.preventDefault();
        this.setState({ [e.target.name]: e.target.value })
    }

    handleTab = (value, key) => {
        const { causes,treatment,diagnosis,prevention} = this.state;
        this.setState({ tabId: value, tabTitle: key, question: "", answer: EditorState.createEmpty() },()=>{
            switch (key) {
                case "causes":
                    this.setState({ tabId: value, tabTitle: key, question: causes.question, answer: causes.answer });
                    break;
                case "treatment":
                    this.setState({ tabId: value, tabTitle: key, question: treatment.question, answer: treatment.answer });
                    break;
                case "diagnosis":
                    this.setState({ tabId: value, tabTitle: key, question: diagnosis.question, answer: diagnosis.answer });
                    break;
                case "prevention":
                    this.setState({ tabId: value, tabTitle: key, question: prevention.question, answer: prevention.answer });
                    break;
            }
        });
    }

    handleSummary = (e) => {
        this.setState({ summary_description : e })
    }
    autoCompleteList = (Arr, Input) => {
        return Arr.filter(e => e.toLowerCase().includes(Input.toLowerCase()));
    };
    handleSymptoms = e => {
        document.getElementById("causeInput").style.backgroundColor = ""
        this.setState({
            [e.target.name]: e.target.value,
            causeSelected: false,
            question: "",
            answer: EditorState.createEmpty(),
            causes: { question: "", answer: EditorState.createEmpty() },
            summary_description: EditorState.createEmpty(),
            diagnosis: { question: "", answer: EditorState.createEmpty() },
            treatment: { question: "", answer: EditorState.createEmpty() },
            prevention: { question: "", answer: EditorState.createEmpty() },
            FAQ: [{ question: "", answer: "" }]
        });
        if (e.target.value) {
            const { symptomsList } = this.props;
            this.setState({
                suggestionSymptoms:  symptomsList.symptomList && symptomsList.symptomList.length > 0 ? this.autoCompleteList(symptomsList.symptomList, e.target.value) : []
            });
            this.setState({ clinicpopup: true });
        } else {
            this.setState({ showList: true })
        }
    }
    handleSymptomMain = (e) =>{
        const val = e.target.value;
        document.getElementById("mainsymptomtext").style.backgroundColor = ""
        this.props.dispatch({type:"QUESTION_AND_OPTION_LIST_SUCCESS",payload:null})
        this.setState({ [e.target.name]: e.target.value ,selectedSymptomMainId : ""},()=>{
            if (!val) {
                this.setState({
                    showSymptomMainDropDown: false
                })
                return;
            }
            const allListSymptom = this.props.symptomMainListData;
            const upDatedallListSymptom = allListSymptom.filter((each) => {
                return each.symptomsName.includes(val);
            })
            const upDatedallListSymptomPerfect = allListSymptom.filter((each) => {
                return each.symptomsName.toLowerCase() == val.toLowerCase();
            })
            if(upDatedallListSymptomPerfect.length != 0){
                this.onSymptomMainClick(upDatedallListSymptomPerfect[0])
                return
            }
            this.setState({
                showSymptomMainDropDown: !upDatedallListSymptom.length == 0
            })

        });
    }
    handleSearchSymptoms = each => {  
        document.getElementById("causeInput").style.backgroundColor = "rgb(238, 238, 238)"
        this.setState({
            symptoms: each,
            showList: false,
            causeSelected:true
        });
        const info = {
            symptoms: each
        }
        this.props.searchSymptoms(info);
    }

    onSymptomMainClick = (each) => {
        document.getElementById("mainsymptomtext").style.backgroundColor = "rgb(238, 238, 238)"
        this.setState({ symptomsMain: each.symptomsName, showSymptomMainDropDown: false ,selectedSymptomMainId : each.symptomsId }, () => {
            this.props.getQuestionAndOptionList({ "symptomsId": each.symptomsId })
        })
    }

    onQuestionChange = (e, dataQA) => {
        const inputVal = e.target.value;
        if (this.timeout) clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
            this.props.updateQuestion({"questionId":  dataQA.id  ,"questionName": inputVal})
        }, 300);
    }

    onClickFormSubmitButton = ()=>{
        if(this.state.selectedSymptomMainId){

        }else{
            const payload = {
                "symptomsName":this.state.symptomsMain,
                "symptomsId":0,
                "addSymptom" : true
            }
            updateSymptomMainFunction(payload,(data)=>{     
                this.props.symptomMainList();
                this.onSymptomMainClick({symptomsName:this.state.symptomsMain, symptomsId : data.symptomsId })                                            
            })  
        }
             
    }
    
    //Option multislelect handlers
    handleChangeMultiSelect(value, actionMeta) {
        if ((actionMeta.action = "clear")) {
            this.setState({ value: [] });
        }
        this.setState({ value: value }, () => {
        });
    }

    handleInputChangeMultiSelect(inputValue) {
        console.log("upatex",inputValue)
        this.setState({ inputValue });
    }

    handleKeyDownMultiSelect(event) {
        const { inputValue, value } = this.state;
        if (!inputValue) return;
        switch (event.key) {
            case "Enter":
                event.preventDefault();
                break;
            default:
                break;
        }
    }

    modalAddCauseForOption = () => {

        const selectedOptionValue = this.state.selectedOptionValue;
        const allCauseList = (this.props.symptomsList && this.props.symptomsList.symptomList && this.props.symptomsList.symptomList) || []
        let optionArray = []
        allCauseList.map((valCause)=>{
            optionArray.push({label:valCause,value:valCause})
        })
        const defaultOptionarray = (selectedOptionValue && selectedOptionValue.causes && selectedOptionValue.causes.map(val => { return { label: val, value: val } })) || []
        // if(selectedOptionValue){
        //     this.state.optionVal = !this.state.optionVal ? (selectedOptionValue.option || "") : this.state.optionVal
        //     this.state.causeVal = !this.state.causeVal ? (selectedOptionValue.causes || "") : this.state.causeVal
        //     this.state.descriptionVal = !this.state.descriptionVal ? (selectedOptionValue.description || "") : this.state.descriptionVal
        // } 
        return (
            <div class="modal d-block show" >
                <div class="modal-dialog">
                    <div class="modal-content modalPopUp">
                        <div class="modal-header borderNone">
                            <h5 class="modal-title">{selectedOptionValue ? "Update option" : "Add option"}</h5>
                            <button type="button" class="popupClose" onClick={()=>{
                               this.setState({optionArrayCause:false,optionVal:"",causeVal:"",descriptionVal:"",selectedOptionValue:""})
                            }}><i class="fas fa-times"></i></button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="form-group">
                                        <label class="commonLabel clr1002612" >Option</label>
                                            <textarea defaultValue={selectedOptionValue && selectedOptionValue.option} style={{height:"30px"}} onChange={this.handleChangeDefault.bind(this)} class="form-control" id="optionVal" name="optionVal"  ></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label class="commonLabel clr1002612" >Cause</label>
                                            <MultipleSelect
                                                inputValue={this.state.inputValue}
                                                defaultValue={defaultOptionarray}
                                                isClearable
                                                isMulti
                                                options={optionArray}
                                                onChange={this.handleChangeMultiSelect.bind(this)}
                                                onInputChange={this.handleInputChangeMultiSelect.bind(this)}
                                                onKeyDown={this.handleKeyDownMultiSelect.bind(this)}
                                                placeholder="Select Cause"
                                            />
                                            {/* <textarea  style={{height:"30px"}} onChange={this.handleChangeDefault.bind(this)} class="form-control" name="causeVal"   value={this.state.causeVal}  ></textarea> */}
                                            {/* <select  defaultValue={selectedOptionValue && selectedOptionValue.causes} onChange={this.handleChangeDefault.bind(this)} id="causeVal"  name="causeVal" className="form-control">
                                                {this.props.symptomsList && this.props.symptomsList.symptomList && this.props.symptomsList.symptomList.map((each, i) => (
                                                    <option>{each}</option>
                                                ))
                                                }
                                        </select> */}
                                        </div>
                                        <div class="form-group">
                                        <label class="commonLabel clr1002612" >Description</label>
                                            <textarea  defaultValue={selectedOptionValue && selectedOptionValue.description} style={{height:"100px"}} onChange={this.handleChangeDefault.bind(this)} class="form-control" id="descriptionVal" name="descriptionVal"    ></textarea>
                                        </div>
                                    </div>
                                   <div class="col-md-12 col-lg-12 col-sm-12 mt-3 text-center">
                                        <button type="button" class="commonBtn" onClick={()=>{
                                                const payload = {
                                                    "optionId": selectedOptionValue.optionId || 0 ,
                                                    "optionName": document.getElementById("optionVal").value,
                                                    "questionId": this.state.symptomCheckerMainQues.id,
                                                    "causes": this.state.value.map(val => val.value),
                                                    "description": document.getElementById("descriptionVal").value
                                                }
                                                updateOptionFunction(payload, () => {
                                                    this.props.getQuestionAndOptionList({ "symptomsId": this.state.selectedSymptomMainId })
                                                    this.setState({ optionArrayCause: false })
                                                    this.onViewAnimation()
                                                })                           
                                        }} >{selectedOptionValue ? "Update" : "Submit"} </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>)
    }

    onViewAnimation = () => {
        this.setState({ isAnimationView: true })
        setTimeout(() => {
            this.setState({ isAnimationView: false })
        }, 2000)
    }

    render() {
        const { FAQ, question, answer, tabId, summary_description, showList, symptoms, suggestionSymptoms ,showSymptomMainDropDown = false} = this.state;
        const { symptomMainListData, questionAndOptionList } = this.props;
        return (
            <React.Fragment>
                <div className="mainMenuSide" id="main-content">
                    {this.state.optionArrayCause && this.modalAddCauseForOption()}
                    <div className="wrapper">
                        <div className="container-fluid">
                            <div className="body-content">
                                <div className="tomCard">
                                    <div className="tomCardBody row">
                                        <div className={`${this.state.widthChange ? 'col-lg-12':"col-lg-6"} col-md-6 col-sm-12 col-12 symptomsQAEntry`}>
                                            <div className="row">
                                                <div className="col-lg-12 col-md-12 col-12 col-sm-12">
                                                  </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-lg-12 col-md-12 col-12 col-sm-12 mt-3 symptomsQAEntryTab">
                                                    <ul>
                                                        <li onClick={()=> this.setState({tabMainParent:"cause"})} className={this.state.tabMainParent == "cause" && 'active' }>Add Causes</li>
                                                        <li onClick={()=> this.setState({tabMainParent:"causeTable"})} className={this.state.tabMainParent  == "causeTable" && 'active'}>Cause List</li>
                                                        <li onClick={() => {
                                                            this.setState({ tabMainParent: "symptom" })
                                                        }}
                                                            className={this.state.tabMainParent == "symptom" && 'active'}>Add Symptom QA</li>
                                                        <li onClick={()=> this.setState({tabMainParent:"symptomTable"})} className={this.state.tabMainParent  == "symptomTable" && 'active'}>Symptom List</li>
                                                      
                                                    </ul>
                                                    {this.state.widthChange && <button type="button" onClick={()=> this.setState({showTextRobot:!this.state.showTextRobot})} class="commonBtn float-right">{this.state.showTextRobot ?'Hide TextRobot':'Show TextRobot'}</button>}
                                                    <div className="clearfix"></div>
                                                </div>
                                                <div className="col-lg-12 col-md-12 col-12 col-sm-12 ">
                                                    <div className="clr1002203">
                                                  
                                                       {this.state.tabMainParent == "cause"  && <div>
                                                        <div className="search-doctor mb-3">
                                                        <label>Causes</label>
                                                        <div className="search-doctor1">
                                                        <input id="causeInput" type="text" style={this.state.symptoms ? { backgroundColor : "rgb(238, 238, 238)"} : {}} className="sesyinput" name="symptoms" value={symptoms} onChange={this.handleSymptoms} autoComplete="off" onKeyPress={() =>
                                                            this.setState({ showList: true })
                                                        } />
                                                          <CSSTransition
                                                          in={showList}
                                                          timeout={300}
                                                          classNames="alert"
                                                          unmountOnExit
                                                        >
                                                          <div className="searchResultSuggestion">
                                                              <ul>
                                                                  {showList && suggestionSymptoms ? (
                                                                      suggestionSymptoms.map((each, i) => (
                                                                          <li key={i}
                                                                              onClick={e => this.handleSearchSymptoms(each)}
                                                                          >
                                                                              {each}
                                                                          </li>
                                                                      ))
                                                                  ) : ""}
                                                              </ul>
                                                          </div>
                                                      </CSSTransition>
                                                        </div>
                                                       
                                                       
                                                    </div>  
                                                      <div className="row">
                                                <div className="col-lg-12 col-md-12 col-12 col-sm-12 mt-3 symptomsQAEntryTab">
                                                    <ul>
                                                        <li onClick={e => this.handleTab("1", "summary")} className={tabId == 1 ? "active" : ""}>Summary</li>
                                                        <li onClick={e => this.handleTab("2", "causes")} className={tabId == 2 ? "active" : ""}>Causes</li>
                                                        <li onClick={e => this.handleTab("3", "treatment")} className={tabId == 3 ? "active" : ""}>Treatment</li>
                                                        <li onClick={e => this.handleTab("4", "diagnosis")} className={tabId == 4 ? "active" : ""}>Diagnosis</li>
                                                        <li onClick={e => this.handleTab("5", "prevention")} className={tabId == 5 ? "active" : ""}>Prevention</li>

                                                    </ul>
                                                </div>

                                                <div className="col-lg-12 col-md-12 col-12 col-sm-12 ">
                                                    <div className="clr1002203">
                                                        {tabId == 1 && <div className="Qusset">
                                                            <div className="form-group">
                                                                <label>Description</label>
                                                                <Editor toolbar={defaultOptionsEditor} editorState={this.state.summary_description} onEditorStateChange={this.handleSummary}/>
                                                                {/* <textarea rows="4" className="form-control" name="summary_description" value={summary_description} onChange={this.handleSummary}></textarea> */}
                                                            </div>
                                                        </div>
                                                        }
                                                        {(tabId == 2 || tabId == 3 || tabId == 4 || tabId == 5) && <div className="Qusset">
                                                            <div className="form-group">
                                                           <label>Title</label>
                                                                <input type="text" className="form-control" name="question" value={question} onChange={this.handleChange} />
                                                            </div>
                                                            <div className="form-group">
                                                                <label>Description</label>
                                                                <Editor toolbar={defaultOptionsEditor} editorState={this.state.answer} onEditorStateChange={this.handleChangeEditorDraft}/>
                                                                {/* <textarea rows="2" className="form-control" name="answer" value={answer} onChange={this.handleChange}></textarea> */}
                                                            </div>
                                                        </div>}
                                                    </div>
                                                </div>
                                            </div>
                                            {/* FAQ Section */}
                                            <div className="row">
                                                <div className="col-lg-12 col-md-12 col-12 col-sm-12 mt-3 symptomsQAEntryTab">
                                                    <ul>
                                                        <li className="active">FAQ</li>

                                                    </ul>
                                                </div>

                                                <div className="col-lg-12 col-md-12 col-12 col-sm-12 ">
                                                    <div className="clr1002203">

                                                        {FAQ && FAQ.map((each, i) => (

                                                            <div key={i} className="faqQAset mb-3">
                                                                <div className="form-group">
                                                                    <label>Question</label>
                                                                    {i != 0 ? <a className="float-right delFaqSymptoms" onClick={e => this.deleteFaq(i, each)}><i className="far fa-trash-alt delFaqSym"></i></a> : ''}
                                                                    <input type="text" name="question" value={each.question} onChange={e => this.handleFaq(i, e)} className="form-control" />
                                                                </div>
                                                                <div className="form-group">
                                                                    <label>Answer</label>
                                                                    <textarea rows="2" name="answer" value={each.answer} onChange={e => this.handleFaq(i, e)} className="form-control"></textarea>
                                                                </div>
                                                            </div>
                                                        ))}
                                                        <a className="AddFaqSymptoms" onClick={this.addfaq}><i className="fas fa-plus AddFaqSym "></i></a>
                                                        <div className="clearfix"></div>

                                                    </div>

                                                </div>

                                            </div>
                                            </div>}
                                            {this.state.tabMainParent == "symptom" && <div>
                                                    <div className="search-doctor mb-3">
                                                        <label>Symptoms</label>
                                                        <div className="search-doctor1">
                                                        <input id="mainsymptomtext" style={this.state.selectedSymptomMainId ? { backgroundColor : "rgb(238, 238, 238)"} : {}} type="text" className="sesyinput" name="symptomsMain" autoComplete="off" value={this.state.symptomsMain} onChange={this.handleSymptomMain} onKeyPress={() =>
                                                          console.log("ds")
                                                        } />
                                                    
                                                        {showSymptomMainDropDown && <div className="searchResultSuggestion" >
                                                            <ul>
                                                                {symptomMainListData.map((each, i) => (
                                                                    <li key={i} onClick={() => this.onSymptomMainClick(each)}>
                                                                        {each.symptomsName}
                                                                    </li>
                                                                ))}
                                                            </ul>
                                                        </div>}   
                                                        </div> 
                                                        {this.state.isAnimationView && <div className="Sym_loader">
                                                            <Lottie options={defaultOptions}
                                                                height={40}
                                                                width={40}
                                                            /> 
                                                         </div>}                                            
                                                    </div>

                                                    {questionAndOptionList.map((dataQA,i)=>{                  
                                                       return (
                                                          <div key={i} className="faqQAset mb-3">
                                                              <div className="form-group">
                                                                  <label>Question No : {i + 1}</label>
                                                                  <a title={!dataQA.options.length == 0 && 'Delete all options'} className={`float-right delFaqSymptoms ${!dataQA.options.length == 0 && 'disabled'}`} onClick={()=>{
                                                                   dataQA.options.length == 0 && this.props.deleteQuestion({questionId:dataQA.id,symptomsId: this.state.selectedSymptomMainId})
                                                                  }}><i className="far fa-trash-alt delFaqSym"></i></a> 
                                                                  <input type="text" autoComplete="off" onChange={(e)=>this.onQuestionChange(e,dataQA)} defaultValue={dataQA.question} name="question" className="form-control" />
                                                              </div>
                                                              <div className="form-group">
                                                                  <label>Options</label>
                                                                  
                                                                  <a><button onClick={()=>{
                                                                      this.setState({optionArrayCause:true,
                                                                        symptomCheckerMainQues:dataQA,
                                                                        selectedOptionValue:"",
                                                                        optionVal:'',
                                                                        descriptionVal:'',
                                                                        causeVal:''
                                                                      })
                                                                  }} className="commonBtnadd float-right">Add Option</button></a>
                                                                  <div className="clearfix"></div>
                                                                  <div className="txtrbtresult">
                                                                      <div className="row">
                                                                          {dataQA.options.map((optionValue,insideIndex)=>{
                                                                              return(
                                                                                  <div className="col-lg-6 col-md-6 col-12 col-sm-12">
                                                                                  <a>             
                                                                                      <div className="listTextRobot12">
                                                                                        <h5>{(insideIndex + 1) + "." + optionValue.option}</h5>
                                                                                          <span  onClick={()=> {
                                                                                               this.props.deleteOption({optionId:optionValue.optionId,symptomsId: this.state.selectedSymptomMainId})
                                                                                          }} >
                                                                                          <i class="fas fa-times float-right" ></i></span>
                                                                                          <p><strong>Associated causes</strong> <br/>{optionValue.causes.join() || "No Cause Associated"} <a href="javascript:void(0);" onClick={()=>{
                                                                                              this.setState({selectedOptionValue:optionValue,symptomCheckerMainQues : dataQA},()=>{
                                                                                                this.setState({optionArrayCause: true})
                                                                                              })
                                                                                          }}>View  More</a></p>
                                                                                      </div>
                                                                                  </a>
                                                                              </div>
                                                                              )
                                                                          }) }
                                                                         
                                                                      </div>
                                                                  </div>

                                                              </div>
                                                          </div>
                                                          
                                                      )
                                                    })}
                                                       
                                                       {this.state.selectedSymptomMainId && <a title="Add Question" className="AddFaqSymptoms" onClick={()=>{ 
                                                            this.props.updateQuestion({"questionId":0,"symptomsId":  this.state.selectedSymptomMainId  ,"questionName": ""})
                                                             setTimeout(()=>{
                                                              this.props.getQuestionAndOptionList({"symptomsId" : this.state.selectedSymptomMainId})
                                                             },800)
                                                        }}><i className="fas fa-plus AddFaqSym "></i></a>}
                                                        <div className="clearfix"></div></div>}
                                                        
                                                        {this.state.tabMainParent == "symptomTable" && <SymptomTableView type="symptom" />}
                                                        {this.state.tabMainParent == "causeTable" && <SymptomTableView type="cause"/>}

                                                    </div>

                                                </div>
                                          
                                            </div>
                                           


                                            {this.state.tabMainParent == "cause" && <div className="row mt-3">
                                                <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <button type="button" className="commonBtn float-right" onClick={this.addSymptoms}>{this.state.causeSelected ? "Update" : "Save"}</button></div>
                                            </div>}
                                            {(this.state.tabMainParent == "symptom" && !this.state.selectedSymptomMainId ) && <div className="row mt-3">
                                                <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <button type="button" onClick={()=>{
                                                           this.onClickFormSubmitButton()                                                                                                      
                                                    }} className="commonBtn float-right" >{this.state.selectedSymptomMainId ? "Update Changes" : "Add New Symptom"}</button></div>
                                            </div>}


                                        </div>
                                        <CSSTransition
                                            in={this.state.showTextRobot}
                                            timeout={300}
                                            classNames="alert"
                                            unmountOnExit
                                            onEnter={() => this.setState({ widthChange: false })}
                                            onExited={() => this.setState({ widthChange: true })}
                                        >
                                         <TextRobotSecondView onClickHideButton={()=> this.setState({showTextRobot:!this.state.showTextRobot})}/>
                                        </CSSTransition>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails,
    symptomsList: state.textRoboReducer.symptomsList,
    searchSymptomsResult: state.textRoboReducer.searchSymptomsResult,
    symptomMainListData:  (state.textRoboReducer.symptomMainListData && state.textRoboReducer.symptomMainListData.symptomsList) || [] ,
    questionAndOptionList : (state.textRoboReducer.questionAndOptionData && state.textRoboReducer.questionAndOptionData.QuestionAndOption) || [] 
})
const mapDispatchToProps = dispatch => ({
    addTextSymptom: (textdata) => dispatch(addTextSymptom(textdata)),
    updateSymptoms : (textdata) => dispatch(updateSymptoms(textdata)),
    getTextSymptomList: () => dispatch(getTextSymptomList()),
    searchSymptoms: (info) => dispatch(searchSymptoms(info)),
    updateSymptomMain: (payload) => dispatch(updateSymptomMain(payload)),
    symptomMainList: () => dispatch(symptomMainList()),
    getQuestionAndOptionList : (payload) => (dispatch(getQuestionAndOptionList(payload))),
    updateQuestion: (payload) => (dispatch(updateQuestion(payload))),
    deleteOption: payload => (dispatch(deleteOption(payload))),
    deleteQuestion : payload => (dispatch(deleteQuestion(payload))),
    dispatch
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TextRobot);
