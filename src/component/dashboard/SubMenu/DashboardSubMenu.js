import React, { Component } from 'react';
import PersonalMenu from "../SubMenu/PersonalMenu";
import HealthRecordMenu from "../SubMenu/HealthRecordMenu";
import MedicalSpecialistMenu from "../SubMenu/MedicalSpecialistMenu";
import MedicalSpecialList from "../InnerMenu/MedicalSpecialistInfoMenu"
import TeleMedicineMenu from "../SubMenu/TelemedicineMenu";
import { connect } from "react-redux";
import InsuranceMenu from "../InnerMenu/insuranceMenu";
import AsideMenu from '../../common/AsideMenu';
import ShareDataMenu from "../SubMenu/ShareDataMenu";
class DashboardSubMenu extends Component {
    constructor(props){
        super(props);
        this.state ={
            submenuType :"Personal"
        }

    }
    handleArrow = () => {
        this.props.history.push("/mainmenu");
    }
    render() {
        const { submenuType } = this.props.location.state || this.state;
        const {userInfo} = this.props.loginDetails;

        return (
            <div>
                <div className="inner-pageNew">
                    <section>
                        <header className="header">
                            <div className="brand">
                                <a href="javascript:void(0)" className="backArrow" onClick={this.handleArrow}>
                                    <i className="fas fa-chevron-left" ></i>
                                </a>
                                <div className="mobileLogo">
                                    <a href="javascript:void(0)" onClick={() => this.props.history.push({pathname:'mainmenu'})} className="logo">
                                        <img src="images/logo.jpg" /> </a></div>

                            </div>
                            <div className="top-nav">

                                <div className="dropdown float-right mr-2" >
                                { userInfo && userInfo.firstName != "" && <a className="dropdown-toggle" data-toggle="dropdown">
                                     { userInfo.firstName}
                                    </a>}
                                    <div className="dropdown-menu">
                                        <a className="dropdown-item" href="javascript:void(0);" onClick={() => this.props.history.push({pathname:'/login/patient'})}>Logout</a>
                                    </div>
                                </div>
                            </div>
                        </header>

                        <AsideMenu/>
                        {/* <aside className="sideMenu">
                            <ul className="sidemenuItems">
                                <li><a href="javascript:void(0)" onClick={() =>this.props.history.push({pathname: "/submenu",state: { submenuType: "Personal" }})} title="Personal"><img src="images/icon2-color.png" /></a><div className="menu-text"><span>Personal</span></div></li>
                                <li><a href="javascript:void(0)" onClick={() =>this.props.history.push({pathname: "/submenu",state: { submenuType: "Telemedicine" }})} title="Telemedicine"><img src="images/icon5-color.png" /></a><div className="menu-text"><span>Telemedicine</span></div></li>
                                <li><a href="javascript:void(0)" onClick={() =>this.props.history.push({pathname: "/submenu",state: { submenuType: "HealthRecord" }})} title="Health Records"><img src="images/icon1-color.png" /></a><div className="menu-text"><span>Health Records</span></div></li>
                                <li><a href="/mainmenu" title="Pharmacy"><img src="images/icon4-color.png" /></a> <div className="menu-text"><span>Pharmacy</span></div></li>
                                <li><a href="/mainmenu" title="Find Your medical specialist"><img src="../images/icon7-color.png" /></a><div className="menu-text"><span>Find Your medical specialist</span></div></li>

                                <li><a href="/mainmenu" title="Services"><img src="images/icon7-color.png" /></a><div className="menu-text"><span>Services</span></div></li>
                                <li><a href="/mainmenu" title="Share Data"><img src="images/icon6-color.png" /></a><div className="menu-text"><span>Share data</span></div></li>


                            </ul>
                        </aside> */}

                        {submenuType == "Personal" && <PersonalMenu history={this.props.history} />} 
                        {submenuType == "HealthRecord" && <HealthRecordMenu history={this.props.history}/>}   
                        {submenuType == "MedicalSpecialist" && <MedicalSpecialistMenu history={this.props.history}/>} 
                        {submenuType == "Telemedicine" && <TeleMedicineMenu history={this.props.history}/>} 
					   {submenuType == "shareData" && <ShareDataMenu history={this.props.history} />}   						
                        </section>

                </div>
            </div>



        )
    }

}
const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails
});

const mapDispatchToProps = dispatch => ({
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DashboardSubMenu);

