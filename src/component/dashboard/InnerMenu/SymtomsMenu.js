import React, { Component } from "react";
import SymptomsRounting from "../innerrouting/SymptomsRounting";
import Logout from "../../common/Logout";
import AsideMenu from "../../common/AsideMenu"

class SymptomsMenu extends Component {
    constructor(props) {
        super(props);
        this.state={
            name:""
        }
    }
    
    componentWillMount() {
           const {userInfo} = JSON.parse(sessionStorage.getItem("loginDetails"))
             this.setState({ name: userInfo });
    }
    
  render() {
      console.log("loginDetails",this.state.name)
    return (
      <div className="inner-pageNew">
        <section>
          <header className="header">
            <div className="brand">
              <a
                href="javascript:void(0)"
                className="backArrow"
                onClick={() =>
                  this.props.history.push({
                    pathname: "/submenu",
                    state: { submenuType: "HealthRecord" }
                  })
                }
              >
                <i className="fas fa-chevron-left" />
              </a>
              <div className="mobileLogo">
                <a href="/mainmenu" className="logo">
                  <img src="../images/logo.jpg" />{" "}
                </a>
              </div>
            </div>
            <div className="top-nav">
              <div className="dropdown float-right mr-2">
                <a className="dropdown-toggle" data-toggle="dropdown">
                  {this.state.name.firstName}
                </a>
                <Logout />
              </div>
            </div>
          </header>

          <AsideMenu/>

          {/* <div className="" id="main-content">
                        <div className="wrapper">
                            <div className="container-fluid">
                                <div className="body-content">
                                    <div id="ponal-sectioners">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> */}
          <SymptomsRounting />
        </section>
      </div>
    );
  }
}
export default SymptomsMenu;
