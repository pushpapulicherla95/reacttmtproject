import React, { Component } from 'react';
import { Switch, Route, Redirect } from "react-router-dom";
import { withRouter } from "react-router";
import EmployeeList from '../component/EmployeeList';
import CityList from '../component/CityList';
import EmployeeCreate from '../component/EmployeeCreate';
import EmployeeEdit from '../component/EmployeeEdit';
import Login from '../component/login/Login';
import HealthLogin from '../component/login/HealthLogin';
import Payment from '../component/common/Payment'
import Registration from '../component/registration/registration';
import Dashboard from '../component/dashboard/Dashboard';
import Forgotpassword from '../component/forgotpassord/Forgotpsw';
import Changepassword from '../component/changeforgotpassword/Changepassword';
import Viewprofile from '../component/viewprofile/Viewprofile';
import Passwordchange from '../component/passwordchange/ChangePsw';
import Editprofile from '../component/editprofile/Editprofile';
import Jitsimeet from '../component/jitsimeet/Joinmeet';
import Landing from '../component/landing/Landing';
import RegistrationAs from '../component/registration/RegisterAs';
import PatientRegistration from '../component/registration/PatientRegistration';
import HealthCareRegistration from "../component/registration/HealthCareRegister";
import MainDashboard from "../component/dashboard/MainDashboard";
import Subscription from "../component/dashboard/subscription";
import { connect } from "react-redux";
import PrivateRoute from "./Auth";
import DashboardSubMenu from "../component/dashboard/SubMenu/DashboardSubMenu";
import PersonalMenu from "../component/dashboard/InnerMenu/PersonalMenu";
import MedicalMenu from "../component/dashboard/InnerMenu/MedicalMenu";
import TelemediineMenu from "../component/dashboard/InnerMenu/TelemedicineMenu";
import CurriculamMenu from "../component/dashboard/InnerMenu/CurriculamMenu";
import Dieases from "../component/dashboard/InnerMenu/DieasesMenu";
import TelemedicineMenu from "../component/dashboard/InnerMenu/TelemedicineInnerMenu";
import MydoctorMenu from "../component/dashboard/InnerMenu/MydoctorMenu";
import SymptomsMenu from "../component/dashboard/InnerMenu/SymtomsMenu";
import DoctorMainMenu from "../component/doctordashboard/doctorMainmenu/DoctorMainMenu";
import DoctorRouting from "../component/doctordashboard/doctorMainmenu/DoctorRouting";
import InsuranceMenu from "../component/dashboard/InnerMenu/insuranceMenu";
import MedicalSpecialList from "../component/dashboard/InnerMenu/MedicalSpecialistInfoMenu";
import AdminLogin from "../component/login/AdminLogin";
import AdminMainMenu from "../component/admin/admindashboard/AdminMainMenu";
import Admin from "../component/admin/admindashboard/Admin";
import MyPatientList from "../component/doctor/MyPatientList";
import ErrorPage from './Error'
import ChatBot from '../../src/component/landing/ChatBot1';
import HospitalMainMenu from "../../src/component/hospitaldashboard/HospitalMainMenu";
import HospitalRouting from "../../src/component/hospitaldashboard/HospitalRouting";
import Qrcode from '../../src/component/Qrcode';
import QuestionAndOptions from '../component/symptomchecker/QuestionAndOptions'
import SymptomCauseList from '../component/symptomchecker/SymptomCauseList'
import CauseDescription from '../component/symptomchecker/CauseDescription'
import DashBoardAnalytics from '../component/dataanalyticsdashboard/DashBoardAnalytics'
import BarChart from "../component/dataanalyticsdashboard/chart";
import Chart1 from "../component/dataanalyticsdashboard/chart1";
import ShareDataDownload from '../../src/component/dashboard/SubMenu/shareDataDownload'
// import Aboutus from '../component/footerdescription/aboutus';
import Aboutus from '../component/footerdescription/aboutus'
import Contact from '../component/footerdescription/contact'
import Datasecurity from '../component/footerdescription/datasecurity'
import Imprint from '../component/footerdescription/imprint'
import Partner from '../component/footerdescription/partners'
import PrivacyPolicy from '../component/footerdescription/privacypolicy'
import dotenv from "dotenv";
dotenv.config();
class App extends Component {

  render() {

    return (
      <div>
        {/* <div>
          <ChatBot />
        </div> */}
        <Switch>
          <Route exact path="/" component={Landing}></Route>
          <Route path="/login/patient" component={Login}></Route>
          <Route path="/qrcode" component={Qrcode}></Route>
          <Route
            path="/login/healthcareprovider"
            component={HealthLogin}
          ></Route>
          <Route
            path="/register/patient"
            component={RegistrationAs}
          ></Route>
          <Route
            path="/register/healthcareprovider"
            component={Registration}
          ></Route>
          <Route
            path="/patientregistration"
            component={PatientRegistration}
          ></Route>
          <Route
            path="/register/health"
            component={HealthCareRegistration}
          ></Route>
          <PrivateRoute
            path="/dashboard"
            component={Dashboard}
          ></PrivateRoute>
          <Route path="/login/forgot" component={Forgotpassword}></Route>
          <Route
            path="/login/newPassword"
            component={Changepassword}
          ></Route>
          <PrivateRoute
            path="/viewprofile"
            component={Viewprofile}
          ></PrivateRoute>
          <Route path="/passwordchange" component={Passwordchange}></Route>
          <PrivateRoute
            path="/editprofile"
            component={Editprofile}
          ></PrivateRoute>
          <PrivateRoute
            path="/jitsimeet"
            component={Jitsimeet}
          ></PrivateRoute>
          <PrivateRoute
            path="/mainmenu"
            component={MainDashboard}
          ></PrivateRoute>
          <Route path="/submenu" component={DashboardSubMenu}></Route>
          <PrivateRoute
            path="/personal"
            component={PersonalMenu}
          ></PrivateRoute>
          <PrivateRoute
            path="/medicine"
            component={MedicalMenu}
          ></PrivateRoute>
          <PrivateRoute
            path="/appointments"
            component={TelemediineMenu}
          ></PrivateRoute>
          <PrivateRoute
            path="/curriculam"
            component={CurriculamMenu}
          ></PrivateRoute>
          <PrivateRoute path="/diseases" component={Dieases}></PrivateRoute>
          <PrivateRoute
            path="/telemedicine"
            component={TelemedicineMenu}
          ></PrivateRoute>
          <PrivateRoute
            path="/mydoctor"
            component={MydoctorMenu}
          ></PrivateRoute>
          <PrivateRoute
            path="/symptoms"
            component={SymptomsMenu}
          ></PrivateRoute>
          <PrivateRoute
            path="/doctormainmenu"
            component={DoctorMainMenu}
          ></PrivateRoute>
          <PrivateRoute
            path="/docdashboard"
            {...this.props}
            component={DoctorRouting}
          ></PrivateRoute>
          <PrivateRoute
            path="/insurance"
            component={InsuranceMenu}
          ></PrivateRoute>
          <PrivateRoute
            path="/docsearchdetails"
            {...this.props}
            component={MedicalSpecialList}
          ></PrivateRoute>
          <PrivateRoute
            path="/listpatient"
            {...this.props}
            component={MyPatientList}
          ></PrivateRoute>
          <Route path="/login/admin" component={AdminLogin}></Route>
          <PrivateRoute
            path="/adminmainmenu"
            component={AdminMainMenu}
          ></PrivateRoute>
          <PrivateRoute
            path="/admindashboard"
            component={Admin}
          ></PrivateRoute>
          <PrivateRoute
            path="/subscription"
            component={Subscription}
          ></PrivateRoute>
          <Route path="/payment" component={Payment}></Route>
          <Route path="/chatbot" component={ChatBot}></Route>
          <Route
            path="/symptomcheckerQA"
            component={QuestionAndOptions}
          ></Route>
          <Route
            path="/symptomcauselist"
            component={SymptomCauseList}
          ></Route>
          <Route
            path="/causedescription"
            component={CauseDescription}
          ></Route>

          <PrivateRoute
            path="/hospitalmainmenu"
            component={HospitalMainMenu}
          ></PrivateRoute>
          <PrivateRoute
            path="/hospitaldashboard"
            component={HospitalRouting}
          ></PrivateRoute>

          {/* Dataanalytics routes */}
          <Route
            path="/analyticsdashboard"
            component={DashBoardAnalytics}
          ></Route>
          <Route path="/chart" component={BarChart}></Route>
          <Route path="/chart1" component={Chart1}></Route>

          <Route path="/shareDataDownload" component={ShareDataDownload}></Route>
          <Route path="/aboutus" component={Aboutus}></Route>
          <Route path="/contact" component={Contact}></Route>
          <Route path="/datasecurity" component={Datasecurity}></Route>
          <Route path="/imprint" component={Imprint}></Route>
          <Route path="/partners" component={Partner}></Route>
          <Route path="/privacypolicy" component={PrivacyPolicy}></Route>
          <Route component={ErrorPage}></Route>


        </Switch>
      </div>
    );
  }
}

export default withRouter(App);