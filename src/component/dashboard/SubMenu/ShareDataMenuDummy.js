// import React, { Component } from 'react';
// import axios from 'axios'
// import { toastr } from "react-redux-toastr";

// // import JSZip from 'jszip'
// // import JSZipUtils from 'jszip-utils'
// // import { saveAs } from 'file-saver'
// class ShareDataMenu extends Component {
//   constructor(props) {
//     super(props)
//     this.state = {
//       sendData: true,
//       shareWithyou: false,
//       invite: false,
//       popUp: false,
//       upload: false,
//       next: false,
//       receiverEmail: "kuzhali@gmail.com",
//       fileName: "",
//       base64: ''
//     }
//   }

//   handleEmailValidation = () => {
//     const configs = {
//       method: 'post',
//       url: 'http://192.168.2.10:9001/tomatomedical/API/sharedata/email/check',
//       data: { "receiverEmailId": this.state.receiverEmail },
//       headers: {
//         'Content-Type': 'application/x-www-form-urlencoded'
//       },
//     }
//     axios(configs)
//       .then(response => {
//         if (response.data.status == "success") {
//           this.setState({ upload: true, next: false })
//         } else {
//           toastr.error("Error", response.data.message);
//         }

//       })
//       .catch((error) => {
//         console.log("err", error.response)
//       })
//   }

//   handleChange = (event) => {
//     if (event.target.files) {
//       let files = event.target.files
//       let zip = require('jszip')();
//       Array.from(files).forEach(each => {
//         zip.file(each.name, each)
//       })
//       zip.generateAsync({ type: "blob" })
//         .then((content) => {
//           // saveAs(content, "example.zip");
//           var reader = new FileReader();
//           reader.readAsDataURL(content);
//           reader.onloadend = () => {
//             var base64data = reader.result;
//             console.log("base64data", base64data)
//             this.setState({ base64: base64data })

//           }
//         });
//     } else {
//       this.setState({ fileName: document.getElementById("fileName").value })
//     }
//   }

//   handleSubmit = () => {
//     const payload =
//     {
//       "uid": 246,
//       "email": "hi@gmail.com",
//       "receiveremailid": this.state.receiverEmail,
//       "files": this.state.base64,
//       "title": this.state.fileName
//     }

//     const configs = {
//       method: 'post',
//       url: 'http://192.168.2.10:9001/tomatomedical/API/sharedata/file/send',
//       data: payload,
//       headers: {
//         'Content-Type': 'application/x-www-form-urlencoded'
//       },
//     }
//     axios(configs)
//       .then(response => {
//         console.log("response", response)
//       })
//       .catch(error => {
//         console.log("error", error)
//       })
//   }

//   render() {
//     // console.log("this.state", this.state)
//     return (<div className="mainMenuSide" id="main-content">
//       <div className="wrapper">
//         <div className="container-fluid">
//           <div className="body-content">
//             <div className="tomCard ">
//               <div className="tomCardHead">
//                 <h5 className="float-left">Share/Sent Data</h5>
//               </div>
//               <div className="tomCardBody">
//                 <div className="tab-menu-title">
//                   <ul>
//                     <li className={this.state.sendData ? "menu1 active" : "menu1"}><a href="JavaScript:Void(0);" onClick={() => this.setState({ sendData: true, invite: false, shareWithyou: false })}>
//                       <p>Sent data</p>
//                     </a></li>
//                     <li className={this.state.invite ? "menu1 active" : "menu1"}><a href="JavaScript:Void(0);" onClick={() => this.setState({ sendData: false, invite: true, shareWithyou: false })}>
//                       <p>Invite data  </p>
//                     </a></li>
//                     <li className={this.state.shareWithyou ? "menu1 active" : "menu1"}><a href="JavaScript:Void(0);" onClick={() => this.setState({ sendData: false, invite: false, shareWithyou: true })}>
//                       <p>Share with you  </p>
//                     </a></li>
//                   </ul>
//                 </div>
//                 <div className="tab-menu-content-section">
//                   {this.state.invite || this.state.sendData ? <div className="row">
//                     <div className="col-lg-6 col-md-6 col-sm-12 col-12">
//                       <div className="share_pageBtn">
//                         <ul>
//                           <li>
//                             <a href="JavaScript:Void(0);" onClick={() => this.setState({ popUp: true, next: true })}>
//                               <div className="defLink  br_share">
//                                 <img src="images/copy-with-in-tomato.png" />
//                               </div>
//                               <div className="share-optionName">With In Tomato Medical</div>
//                             </a>
//                           </li>
//                           <li>
//                             <a href="">
//                               <div className="cplink br_share">
//                                 <img src="images/copy-link-white.png" />
//                               </div>
//                               <div className="share-optionName">Copy Link</div>
//                             </a>
//                           </li>
//                           <li>
//                             <a href="">
//                               <div className="defLink br_share">
//                                 <img src="images/gmail-icon.png" />
//                               </div>
//                               <div className="share-optionName">Gmail</div>
//                             </a>
//                           </li>
//                           <li>
//                             <a href="">
//                               <div className="fb br_share">
//                                 <img src="images/facebook-logo.png" />
//                               </div>
//                               <div className="share-optionName">Facebook</div>
//                             </a>
//                           </li>

//                           <li>
//                             <a href="">
//                               <div className="wechat br_share">
//                                 <img src="images/whatsapp-icon-white.png" />
//                               </div>
//                               <div className="share-optionName">Whats App</div>
//                             </a>
//                           </li>
//                           <li>
//                             <a href="">
//                               <div className="linked br_share">
//                                 <img src="images/linkedin-logo1.png" />
//                               </div>
//                               <div className="share-optionName">linkedin</div>
//                             </a>
//                           </li>
//                         </ul>
//                       </div>
//                     </div>
//                   </div> : ""}
//                   {this.state.shareWithyou && <div id="content-1" >
//                     <div className="search-input">
//                       <input type="text" className="searchField" />
//                       <div className="searchIcon">
//                         <i className="fas fa-search"></i>
//                       </div>
//                     </div>
//                     <div className="table-responsive">
//                       <table className="table table-hover">
//                         <thead className="thead-default">
//                           <tr>
//                             <th>Date</th>
//                             <th>Sent By</th>
//                             <th>File Count</th>
//                             <th>File size</th>
//                             <th>Action</th>
//                           </tr>
//                         </thead>
//                         <tbody>
//                           <tr>
//                             <td data-label="Date">2019-06-20</td>
//                             <td data-label="Time">abcd@mailid.com</td>
//                             <td data-label="Ip">03</td>
//                             <td data-label="Description">50Mb</td>
//                             <td data-label="Action">
//                               <a title="Download" className="deleteBtn"><i className="fas fa-download"></i></a>
//                               <a title="Delete" className="deleteBtn"> <i className="fa fa-trash" data-toggle="modal" data-target="#deleteModal"></i> </a>
//                             </td>
//                           </tr>
//                         </tbody>
//                       </table>
//                     </div>
//                   </div>}
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//       <div
//         className={this.state.popUp ? "modal d-block" : "modal"}
//         id="myModal"
//       >
//         <div className="modal-dialog">
//           <div className="modal-content modalPopUp">
//             <div className="modal-header borderNone">
//               <h5 className="modal-title">Sent with in TomatoMedical</h5>
//               <button type="button" className="close" onClick={() => this.setState({ popUp: false, next: false, upload: false })}>
//                 <i className="fas fa-times" />
//               </button>
//             </div>
//             <div className="modal-body">
//               <form>
//                 <div className="row">
//                   <div className="col-md-12 col-lg-12 col-sm-12">
//                     {this.state.next && <div>
//                       <div className="form-group" >
//                         <label className="form-label">Email-Id</label>
//                         <input
//                           className="form-control"
//                           type="email"
//                           name="receiverEmail"
//                           value={this.state.receiverEmail}
//                           onChange={(e) => this.setState({ receiverEmail: e.target.value })}
//                         />
//                         <span />
//                       </div>
//                     </div>}
//                     {this.state.upload && <div>
//                       <div className="form-group">
//                         <label className="form-label">Name of Upload</label>
//                         <input
//                           className="form-control"
//                           type="text"
//                           id="fileName"
//                           name="fileName"
//                           value={this.state.fileName}
//                           onChange={this.handleChange}
//                         />
//                         <span />
//                       </div>
//                       <div className="form-group">
//                         <label className="form-label">Upload files</label>
//                         <div className="form-group">
//                           <input type="file" className="form-control-file border" name="file" id="file" multiple onChange={this.handleChange} />
//                         </div>
//                         <span />
//                       </div>
//                     </div>}
//                   </div>
//                   <div className="col-md-12 col-lg-12 col-sm-12 mt-2">
//                     {this.state.upload && <button
//                       type="button"
//                       className="commonBtn "
//                       onClick={() => this.setState({ next: true, upload: false })}
//                     >
//                       previous
//                       </button>}
//                     {this.state.next && <button
//                       type="button"
//                       className="commonBtn float-right"
//                       onClick={() => this.handleEmailValidation()}
//                     >
//                       Next
//                       </button>}
//                     {this.state.upload && <button
//                       type="button"
//                       className="commonBtn float-right"
//                       onClick={this.handleSubmit}
//                     >
//                       Sent
//                       </button>}
//                   </div>
//                 </div>
//               </form>
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>)
//   }
// }
// export default ShareDataMenu