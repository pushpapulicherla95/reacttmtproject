import React from 'react';
import axios from 'axios';
import _ from 'lodash'
import URL from '../../../asset/configUrl'
class ShareBy extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            emailList: [],
            enableList: false,
            getshareByList: [],
            visible: false,
            tabs: "toOthers",
            shareDataUrlList: [],
            doctorList: []
        }
    }


    tomatoShare = () => {
        const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
        // loginDetails.userInfo.userId
        const configs = {
            method: 'post',
            url: URL.SHAREBY_FILEURL,
            data:{
              "uid": loginDetails.userInfo.userId
            },
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
          }
        axios(configs)
            .then(response => this.setState({ shareDataUrlList: response.data.shareDataUrlList }))
            .catch(error => this.setState({ shareDataUrlList: error.response }))
    }

    doctorList = () => {
        const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
       
        if (loginDetails.userInfo.userType == "patient") {
            axios.get(URL.ADMIN_DOCTOR_LIST)
                .then(response => {
                    this.setState({ doctorList: response.data.doctorList })
                    console.log("response.data", response.data.doctorList)
                })
        } else if (loginDetails.userInfo.userType == "doctor") {
            axios.get(URL.ADMIN_PATIENT_LIST)
                .then(response => {
                    this.setState({ doctorList: response.data.patientList })
                })
        }
    }

    deleteTomatoShare = (id) => {
        const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
        const configs = {
            method: 'post',
            url: URL.SHAREBY_FILEURLDELETE,
            data:{ "id": id },
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
          }
        // loginDetails.userInfo.userId
        axios(configs)
            .then(response => {
                this.tomatoShare()
                console.log("response", response)
            })
        // .catch(error => this.setState({ shareDataUrlList: error.response }))
    }

    componentDidMount() {
        document.addEventListener('contextmenu', this._handleContextMenu);
        document.addEventListener('click', this._handleClick);
        document.addEventListener('scroll', this._handleScroll);
    };

    componentWillUnmount() {
        document.removeEventListener('contextmenu', this._handleContextMenu);
        document.removeEventListener('click', this._handleClick);
        document.removeEventListener('scroll', this._handleScroll);
    }

    contextMenu = (event) => {
        event.preventDefault();
        this.setState({ visible: true });
        try {
            const clickX = event.clientX;
            const clickY = event.clientY;
            const screenW = window.innerWidth;
            const screenH = window.innerHeight;
            const rootW = this.root.offsetWidth;
            const rootH = this.root.offsetHeight;

            const right = (screenW - clickX) > rootW;
            const left = !right;
            const top = (screenH - clickY) > rootH;
            const bottom = !top;

            if (right) {
                this.root.style.left = `${clickX + 5}px`;
            }

            if (left) {
                this.root.style.left = `${clickX - rootW - 5}px`;
            }

            if (top) {
                this.root.style.top = `${clickY + 5}px`;
            }

            if (bottom) {
                this.root.style.top = `${clickY - rootH - 5}px`;
            }
        } catch
        {

        }

    }

    _handleClick = (event) => {
        const { visible } = this.state;
        const wasOutside = !(event.target.contains === this.root);

        if (wasOutside && visible) this.setState({ visible: false, });
    };

    _handleScroll = () => {
        const { visible } = this.state;

        if (visible) this.setState({ visible: false, });
    };

    componentWillMount = () => {
        this.getEmailList()
        this.tomatoShare()
        this.doctorList()
    }

    getEmailList = () => {
        const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
        const configs = {
            method: 'post',
            url: URL.SHAREBY_SENDERLIST,
            data:{ "uid": loginDetails.userInfo.userId },
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
          }
        axios(configs)
            .then(response => {
                this.setState({ emailList: response.data.senderEmailList }, () => {
                    var result = [];
                    _.forEach(this.state.doctorList, (n, key) => {
                        _.forEach(this.state.emailList, (n2, key2) => {
                            if (n.email === n2) {
                                result.push(n);
                            }
                        });
                    });

                    this.setState({ emailList: result })
                })
            })
    }


    getshareByList = (email) => {

        const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
        const configs = {
            method: 'post',
            url: URL.SHAREBY_RECEIVERLIST,
            data:{ "uid": loginDetails.userInfo.userId, "receiverEmailId": email },
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
          }
        axios(configs)
            .then(response => this.setState({ getshareByList: response.data.receiverListBySenderEmail }))
    }

    render() {
        const { emailList, getshareByList, visible, shareDataUrlList } = this.state
        console.log("this.tate", this.state)
        return (<div>

            {(visible || null) &&
                <div ref={ref => { this.root = ref }} className="contextMenu">
                    <div className="contextMenu--option"><i class="fas fa-cloud-download-alt"></i> Download</div>
                    {/* <div className="contextMenu--option" onClick={() => this.setState({ invitePopUp: true, fileUrl: selectFile.fileUrl })}><i class="fas fa-share-alt"></i> Share</div>
                <div className="contextMenu--option" onClick={() => this.props.myDriveListDelete(selectFile.fileName + selectFile.fileType)}><i class="fas fa-trash"></i> Remove</div> */}
                </div>}
            <div class="share_card_new">

                <div class="tab_list">
                    <button className={this.state.tabs == "toOthers" && "active"} onClick={() => this.setState({ tabs: "toOthers" })}>Tomato</button>
                    <button className={this.state.tabs == "tomato" && "active"} onClick={() => this.setState({ tabs: "tomato" })}>  To Others</button>
                </div>
                {this.state.tabs == "toOthers" && !this.state.enableList && <div className="email_list_share">
                    {emailList.map((each, i) => <div class="lsit_share_card">
                        <label className="d-block"><i class="fas fa-user-alt"></i> {each.firstName + " " + each.lastName}</label>
                        <label className="d-block"><i class="fas fa-envelope"></i> {each.email}</label>
                        <label className="d-block"><i class="fas fa-mobile-alt"></i> {each.mobileNo ? each.mobileNo : "-"}</label>
                        <label className="h-40 d-block"><i class="fas fa-map-marker-alt"></i>{each.address + each.address2 ? each.address + each.address2 : "-"}</label>
                        <button onClick={() => this.setState({ enableList: true }, () => this.getshareByList(each.email))}>View</button>
                    </div>)}
                </div>}
                {this.state.tabs == "toOthers" && <div class="file_list_share" onContextMenu={(e) => this.contextMenu(e)}>
                    {this.state.enableList && getshareByList.map((each, i) => {
                        return each.fileUrl.map((val) =>
                            <div class="file_list_card">
                                <div class="file_format"> <img src="images/pdf_icon.png" />
                                </div>
                                <div class="file_details">
                                    <div>
                                        <label><i class="fas fa-file"></i> {val}</label>
                                    </div>
                                    <div class="menu">
                                        <a title="Upload date: 10-10-2019">  <i class="fas fa-ellipsis-v"></i></a>
                                    </div>
                                </div>
                            </div>)
                    })}
                </div>
                }
                {this.state.tabs == "tomato" && <div
                    id="content-2"
                    className="d-block"
                >
                    <div className="table-responsive">
                        <table className="table table-hover">
                            <thead className="thead-default">
                                <tr>
                                    <th>Id</th>
                                    <th>fileName</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {shareDataUrlList.map((each, i) => {
                                    return each.fileUrl.map(val =>

                                        <tr>
                                            <td data-label="#">{i + 1}</td>
                                            <td data-label="Name">{val}</td>
                                            <td data-label="Action">
                                                <button class="download_btn" onClick={() => this.deleteTomatoShare(each.id)}>
                                                    <i class="fa fa-trash" aria-hidden="true"></i> Stop Sharing
                                      </button>
                                            </td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>}

            </div>


        </div>)
    }
}
export default ShareBy