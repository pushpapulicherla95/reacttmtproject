import React from "react";
import moment from 'moment';
import axios from 'axios';
class DataListCard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {

    }

  }
  fileDownload = (value, fileName) => {

    axios.post('http://192.168.2.30:8000/tomatomedical/API/dataAnalystFileDownload', { file_path: value })
      .then(response => {
        var bob = "data:application/vnd.ms-excel;base64," + response.data.fileUrl
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.style = "display: none";
        a.href = bob;
        a.download = fileName + ".csv";
        a.click();
      })
  }
  render() {
    return (
      <div>
        {this.props.quickViewList.length > 0 ? this.props.quickViewList.map((each, i) =>
          <div class="card-data">
            <ul>
              <li class="img-sec-crad">
                <img src="https://images.pexels.com/photos/736716/pexels-photo-736716.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=650&w=940" />
              </li>
              <li>
                <h6>{each.title}</h6>
                <p>
                  <i class="fas fa-user"></i> <span>{each.name}</span>
                </p>
                <p>
                  <i class="fas fa-calendar-alt"></i> <span>{moment(each.created_date).format("MM-DD-YYYY")}</span>{" "}
                  &nbsp; <i class="fas fa-database"></i>
                  <span>{each.file_size}</span> &nbsp; <i class="fas fa-file-alt"></i>{" "}
                  <span>{each.file_type}</span>
                </p>
              </li>
              <li>
                <div class="cardBtn">
                  <button
                    data-toggle="modal"
                    data-target="#Quickinfo"
                    onClick={() => this.props.quickView(each)}
                  >
                    <i class="fas fa-eye"></i>
                    <span>Quick Info</span>
                  </button>
                  <button onClick={() => this.fileDownload(each.file_path, each.title)}>
                    <i class="fas fa-cloud-download-alt"></i>
                    <span> Download</span>
                  </button>
                </div>
              </li>
            </ul>
          </div>) : <div style={{ textAlign: "center", color: 'red' }}>No Filter Data Found</div>}
      </div>
    );
  }
}
export default DataListCard;
