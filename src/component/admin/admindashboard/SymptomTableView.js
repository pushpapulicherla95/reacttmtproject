
import React, { Component } from "react";
import { connect } from "react-redux";
import { addTextSymptom, getTextSymptomList, searchSymptoms, updateSymptoms, updateSymptomMain, symptomMainList, getQuestionAndOptionList, updateQuestion, updateOptionFunction ,updateSymptomMainFunction } from "../../../service/admin/TextRobo/action";
import _ from "lodash"; 
import CauseDescription from '../../symptomchecker/CauseDescription'

class SymptomTableView extends React.Component {
   
        state = {
            selectedCauseList: [],
            selectedSymptomList: [],
            selectedLetter: '',
            // type:"cause",
            type:"symptom",
            selectedCause:"",
            viewAssociatedCause : false
        }  

        componentWillMount(){
          this.props.getTextSymptomList()
          this.props.symptomMainList()
        }

        filterSelectedLetter=(e)=>{
            const causeListGroup = _.groupBy(this.props.causeList,"keyLetter");
            const symptomListGroup = _.groupBy(this.props.symptomList,"keyLetter")
            if(this.props.type == "cause"){
                causeListGroup[e] && this.setState({
                    selectedCause : "",
                    selectedLetter : e.toUpperCase(),
                    selectedCauseList : causeListGroup[e] || []
                })
            }else{
                symptomListGroup[e] && this.setState({
                    selectedCause : "",
                    viewAssociatedCause : false,
                    selectedLetter :  e.toUpperCase(),
                    selectedSymptomList : symptomListGroup[e] || []
                })
            }
         
        }

        render(){
            const causeListGroup = _.groupBy(this.props.causeList,"keyLetter");
            const symptomListGroup = _.groupBy(this.props.symptomList,"keyLetter")
            if(this.props.type == "cause"){
                return (
                    <div>
                        <h4>Find Cause by letter</h4>
                        <ul className="filterAlphabets">  {Array(26).fill(1).map((val, index) => {
                            const alphaCharacter = String.fromCharCode(65 + index)
                            const charCheck = _.has(causeListGroup, alphaCharacter.toLowerCase())
                            return <li >
                                <a className={!charCheck ? 'disabledChar' : ''} href="javascript:void(0);" onClick={() => this.filterSelectedLetter(alphaCharacter.toLowerCase())}>{alphaCharacter} </a>
                            </li>
                        })}   </ul>
                        {!this.state.selectedCause && <div className="search-list">
                            <h2>{(this.state.selectedLetter && this.state.selectedLetter)}</h2>
                            <ul>
                                {this.state.selectedCauseList.map((val) =>
                                    <li onClick={() => this.setState({ selectedCause: val.value })}> &#35; {val.value} </li>
                                )}</ul>
                        </div>}
                        {this.state.selectedCause && <CauseDescription minimalView={true} cause={this.state.selectedCause} />}
                    </div>
                )
            } else {
                return (
                    <div>
                        <h4>Find Symptom By Letter</h4>
                        <ul className="filterAlphabets">  {Array(26).fill(1).map((val, index) => {
                            const alphaCharacter = String.fromCharCode(65 + index)
                            const charCheck = _.has(symptomListGroup, alphaCharacter.toLowerCase())
                            return <li >
                                <a className={!charCheck ? 'disabledChar' : ''} href="javascript:void(0);" onClick={() => this.filterSelectedLetter(alphaCharacter.toLowerCase())}>{alphaCharacter} </a>
                            </li>
                        })}   </ul>

                        {!this.state.viewAssociatedCause && <div className="search-list">
                            <h2>{(this.state.selectedLetter && this.state.selectedLetter)}</h2>
                            <ul>
                                {this.state.selectedSymptomList.map((val) =>
                                    <li onClick={() =>{ 
                                        this.props.getQuestionAndOptionList({ "symptomsId": val.symptomsId })
                                        this.setState({
                                            viewAssociatedCause : true,
                                            seletedSymptom : val.symptomsName
                                        })
                                    
                                    }
                                    }> &#35; {val.symptomsName} </li>
                                )}</ul>
                        </div>}

                        {(this.state.viewAssociatedCause && !this.state.selectedCause) && <div className="searchContent">
                            <div class="searchCard22">
                                <div class="row">
                                    <div class="col-12">
                                        <h1 style={{ textAlign: "center", color: "#000", textTransform: "capitalize" }}>
                                            {this.state.seletedSymptom && this.state.seletedSymptom}
                                        </h1>
                                    </div>
                                </div>
                                <h5>  Associated Causes</h5>
                                <hr />
                                <p><ul>
                                    {this.props.uniqOptionArray.map((val) =>
                                        <li style={{cursor:"pointer"}} onClick={()=>{
                                            this.setState({
                                                selectedCause:val.causes
                                            })
                                        }} >
                                            {val.causes}
                                        </li>
                                    )}
                                </ul> {this.props.uniqOptionArray.length == 0 && <div> No Causes Associated</div>}</p>
                            </div></div>}
                        {this.state.selectedCause && <CauseDescription minimalView={true} cause={this.state.selectedCause} />}    
                    </div>
                )
            }
           
        }
    }

    const mapStateToProps = state => {
        const causeListData = (state.textRoboReducer.symptomsList && state.textRoboReducer.symptomsList.symptomList).map((val, index) => {
            return {
                keyLetter: val.charAt(0).toLowerCase(),
                value: val
            }
        });
        const symptionMainListDefault = (state.textRoboReducer.symptomMainListData && state.textRoboReducer.symptomMainListData.symptomsList) || []
        const symptionMainListData = symptionMainListDefault.map((val,index)=>{
            return {
                keyLetter: val.symptomsName.charAt(0).toLowerCase(),
                ...val
            }
        })

        const questionAndOptionListDefault =  state.textRoboReducer.questionAndOptionData && state.textRoboReducer.questionAndOptionData.QuestionAndOption || [];
        let optionArray = []
        questionAndOptionListDefault.map((val)=>{
            optionArray.push(val.options)
        })
        const alloptionArray = [].concat.apply([], optionArray);
        let newDestucturedArray = []
        
        alloptionArray.map((val)=>{
            const causes = val.causes;
            const option = val.option;
            const optionId = val.optionId;
            const description = val.description;
            causes.map((each,i) =>{
                newDestucturedArray.push({
                    "causes":each,
                    option,
                    optionId,
                    description
    
                })
            })
        })

        return {
            questionAndOptionList : state.textRoboReducer.questionAndOptionData && state.textRoboReducer.questionAndOptionData.QuestionAndOption || [],
            uniqOptionArray : _.uniqBy(newDestucturedArray,"causes"),
            symptomList: symptionMainListData,
            causeList: causeListData
        }
    }

    const mapDispatchToProps = dispatch => ({
        getTextSymptomList: () => dispatch(getTextSymptomList()),
        symptomMainList: () => dispatch(symptomMainList()),
        getQuestionAndOptionList : (payload) => (dispatch(getQuestionAndOptionList(payload))),
        dispatch
    })
    
    export default connect(
        mapStateToProps,
        mapDispatchToProps
    )(SymptomTableView);

    