
import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
// import ScheduleList from "../doctor/MySchedule";
import DoctorList from "../../component/dashboard/DoctorList";
import ProductList from "../../component/dashboard/ProductList";
import PatientList from "../../component/dashboard/PatientList";
import CategoryList from "../../component/dashboard/ProductCategory";
import Appointment from "../../component/patient/telemedicine/appointment/Appointment";
import BookAppointment from "../../component/patient/telemedicine/appointment/BookAppointment";
import JoinMeeting from "../jitsimeet/Joinmeet";
// import Addallery from "../../component/medicaldata/Addallery";
// import Accident from "../../component/medicaldata/Accident";
// import Finding from "../../component/medicaldata/Finding";
// import Therapy from "../../component/medicaldata/Therapy";
// import HospitalLength from "../../component/medicaldata/HospitalLength";
// import Immunization from "../../component/medicaldata/Immunization";

class DashboardMenu extends Component {

    render() {
        return (
            <Switch>
                <Redirect path="/dashboard" exact to="/dashboard/shedulelist" />
                {/* <Route path="/dashboard/shedulelist" component={ScheduleList} /> */}
                <Route path="/dashboard/doctorlist" component={DoctorList} />
                <Route path="/dashboard/pharmacydetails" component={ProductList} />
                <Route path="/dashboard/categorylist" component={CategoryList} />
                <Route path="/dashboard/Patientdetails" component={PatientList} />
                <Route path="/dashboard/patient/appointment" component={Appointment} />
                <Route path="/dashboard/appointment/book" component={BookAppointment} />
                <Route path="/dashboard/appointment/joinmeeting" component={JoinMeeting} />
                {/* <Route path="/dashboard/addAllergy" component={Addallery} />
                <Route path="/dashboard/accident" component={Accident} />
                <Route path="/dashboard/finding" component={Finding} />
                <Route path="/dashboard/immunization" component={Immunization} />

                <Route path="/dashboard/therapy" component={Therapy} />
                <Route path="/dashboard/hospital" component={HospitalLength} /> */}


            </Switch>
        )
    }

}
export default DashboardMenu;