import React, { Component } from "react";
import { createproductcategory, listproductcategory, updateproductcategory, deleteproductcategory } from "../../service/productcategory/action";
import { connect } from "react-redux";

class ProductCategory extends Component {

    state = {
        categoryName: "",
        parentCategoryName: "",
        addpatientpop: false,
        selectedOption: '',
        categorylist: [],
        editproductpop: false,
        editDetails: {},

    }
    handleChange = (e) => {
        e.preventDefault();
        this.setState({ [e.target.name]: e.target.value });

    }
    
    addPatientpopUp = (e) => {

        this.setState({ addpatientpop: true });

    }
    //update
    modalPopupOpen = (e) => {

        this.setState({ editproductpop: true, editDetails: e });
        this.setState({
            categoryName: e.categoryName,
            Id: e.Id
        })
    }
    updateCategory = () => {
        const { Id, categoryName } = this.state;
        const updatecategory = {
            id: Id,
            categoryName: categoryName,

        }
        this.props.updateproductcategory(updatecategory);
        this.setState({ editproductpop: false })
    }
    modalPopUpClose = () => {
        this.setState({ addpatientpop: false });
        this.setState({ editproductpop: false });

    }
    categorySubmit = e => {
        const { categoryName, selectedOption } = this.state;
        const createproduct = {
            categoryId: selectedOption,
            categoryName: categoryName,
            // parentCategoryName: parentCategoryName,
        }
        this.props.createproductcategory(createproduct);
        this.setState({ addpatientpop: false })
    }
    componentWillMount = (e) => {

        //listproduct
        const listproduct = {
            "Id": "2"
        }
        this.props.listproductcategory(listproduct);
    }
    //deletecategory
    delete = (event) => {
        const deletecategory = {
            id: event.Id
        }
        this.props.deleteproductcategory(deletecategory);
    }

    render() {
        const { categoryName } = this.state;
        const { categoryList } = this.props.listproductcategoryDetails;
        return (

            <React.Fragment>

                <div className="cui-layout-content">
                    <div className="cui-utils-content">
                        <div className="cui-utils-title mb-3"><strong className="text-uppercase font-size-16">List</strong></div>
                        <div className="row">
                            <div className="col-xl-12">
                                <div className="card">
                                    <div className="card-header">
                                        <div className="cui-utils-title"><strong>Category List</strong>
                                            <button type="button" className="btn commonBtn float-right" data-toggle="modal" data-target="#myModal" onClick={() => this.addPatientpopUp()} ><i className="fa fa-plus"></i> Add ProductCategory</button>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead class="thead-default">
                                                    <tr>
                                                        <th>CategoryName</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                {<tbody>
                                                    {
                                                        categoryList && categoryList.map((each, i) => (
                                                            <tr key={i}>
                                                                <td data-label="categoryName">{each.categoryName}</td>
                                                                <td data-label="Action"><a title="Edit" class="editBtn" onClick={(e) => this.modalPopupOpen(each)}><span id={5} class="fa fa-edit"></span></a>
                                                                    <a title="Delete" className="deleteBtn" value={this.state.selectedOption} onChange={(e) => this.setState({ selectedOption: e.target.value })} onClick={(e) => this.delete(each)}><i className="fa fa-trash" data-toggle="modal" data-target="#myModal2"></i></a></td>

                                                            </tr>))
                                                    }

                                                </tbody>}
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={this.state.addpatientpop ? "modal show d-block" : "modal"} >
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Add Catogery</h4>
                                <button type="button" className="close" data-dismiss="modal" onClick={this.modalPopUpClose}>&times;</button>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label className="form-label">CategoryName</label>
                                        <input id="validation-email" className="form-control" name="categoryName" onChange={this.handleChange} type="text" value={categoryName} /><span ></span>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-label">MainCategoryName</label><sup>*</sup>
                                        <select className="form-control" name="categoryName" value={this.state.selectedOption} onChange={(e) => this.setState({ selectedOption: e.target.value })}>>
                                        <option value="" disabled>---Select Product Category----</option>
                                            {
                                                categoryList && categoryList.map(each => (

                                                    <option value={each.Id}>  {each.categoryName}</option>))
                                            }
                                        </select>
                                    </div>
                                    <div className="form-actions">
                                        <button type="button" className="btn commonBtn mr-3" onClick={this.categorySubmit} >Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={this.state.editproductpop ? "modal show d-block" : "modal"} id="edit_btn">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Edit Category</h4>
                                <button type="button" className="close" data-dismiss="modal" onClick={this.modalPopUpClose}>&times;</button>
                            </div>
                            <div className="modal-body">
                                <div className="form-group"><label className="form-label">CategoryName </label>
                                    <input type="text" className="form-control" placeholder="Enter your Email" name="categoryName" onChange={this.handleChange} defaultValue={this.state.editDetails.categoryName} />
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn commonBtn" onClick={this.updateCategory} >Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
const mapStateToProps = state => ({

    createproductcategoryDetails: state.categoryReducer.createproductcategoryDetails,
    listproductcategoryDetails: state.categoryReducer.listproductcategoryDetails,
    updateproductcategoryDetails: state.categoryReducer.updateproductcategoryDetails,
    deleteproductcategoryDetails: state.categoryReducer.deleteproductcategoryDetails
});

const mapDispatchToProps = dispatch => ({

    createproductcategory: (createproduct) => dispatch(createproductcategory(createproduct)),
    listproductcategory: (listproduct) => dispatch(listproductcategory(listproduct)),
    updateproductcategory: (updatecategory) => dispatch(updateproductcategory(updatecategory)),
    deleteproductcategory: (deletecategory) => dispatch(deleteproductcategory(deletecategory)),
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductCategory);