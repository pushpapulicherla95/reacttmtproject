import React, { Component } from 'react';

class Footer extends Component{
    render(){
        return(<footer class="page-footer font-small blue">

        <div class="footer-block">
            <div class="footer-copyright  py-3">Copyright © 2014-2019
                <a href="http://www.tomatomedical.de"> tomatomedical</a>&nbsp;&nbsp;&nbsp;
                <span>Version 1.5.0</span>
            </div>
            <div class="footer-link py-3">
                <ul>
                    <li><a href="https://cloud.tomatomedical.com/cloud/imprint.xhtml">Imprint</a></li>
                    <li><a href="https://cloud.tomatomedical.com/cloud/imprint.xhtml#datenschutz">Privacy
                            policy</a></li>
                    <li><a href="https://cloud.tomatomedical.com/cloud/agb.xhtml">Terms and Conditions</a></li>
                    <li><a href="https://cloud.tomatomedical.com/cloud/withdrawal.xhtml">Right of withdrawal</a></li>

                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </footer>)
    }
}
export default Footer;