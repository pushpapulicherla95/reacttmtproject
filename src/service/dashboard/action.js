import URL from '../../asset/configUrl';
import axios from "axios";
import actiontype from '../../service/dashboard/actionType'

export const viewProfile = profileinfo => dispatch => {

    const configs = {
        method: 'post',
        url: URL.USER_GETUSERPROFILE,
        data: profileinfo,
        headers: {

            'Content-Type': 'application/x-www-form-urlencoded'
        },

    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actiontype.GETUSERPROFILE_SUCCESS,
                payload: res.data
            })
        }
    })
        .catch(err => {
            dispatch({
                type: actiontype.GETUSERPROFILE_FAILURE,
                error: err
            })
        })
}
export const editProfile = editinfo => dispatch => {

    const configs = {
        method: 'post',
        url: URL.USER_EDITUSERPROFILE,
        data: editinfo,
        headers: {

            'Content-Type': 'application/x-www-form-urlencoded'
        },

    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actiontype.EDITUSERPROFILE_SUCCESS,
                payload: res.data
            })
        }
    })
        .catch(err => {
            dispatch({
                type: actiontype.EDITUSERPROFILE_FAILURE,
                error: err
            })
        })
}