import actionType from "../../service/hospital/actionType";
import axios from "axios";
import { toastr } from "react-redux-toastr";
import { toastrOptions } from "../../component/common/toasteroptions";
import URL from "../../asset/configUrl";

export const getAdminInfo = info => dispatch => {
    const configs = {
        method: 'post',
        url: URL.HOSPITAL_ADMIN_INFO,
        data: info,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }

    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.GET_HOSPITAL_ADMININFO_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', res.data.message, toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_HOSPITAL_ADMININFO_FAILURE,
            error: err
        })
    })

}

//UPDTAE HOSPTAL ADMIN INFO
export const updateAdminInfo = info => dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDATE_HOSPITAL_INFO,
        data: info,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
    let listdata = {
        "uid": info.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getAdminInfo(listdata))
            dispatch({
                type: actionType.GET_HOSPITAL_ADMININFO_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', res.data.message, toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_HOSPITAL_ADMININFO_FAILURE,
            error: err
        })
    })

}
//add hospital doctor
export const addHospitalDoctor = info => dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_HOSPITAL_DOCTOR,
        data: info,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
    let listdata = {
        "uid": info.hospitalId
    }
   return axios(configs).then((res) => {
        if (res.data.status == "success") {
            dispatch(getHospitalDoctorList(listdata))
            dispatch({
                type: actionType.ADD_HOSPITAL_DOCTOR_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
            return Promise.resolve("success");
        } else if (res.data.status == "failure") {
            toastr.error('Error', res.data.message, toastrOptions);
            return Promise.resolve("failure");
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_HOSPITAL_DOCTOR_FAILURE,
            error: err
        })
    })

}

//GET HOSPITAL DOCTOR LIST
export const getHospitalDoctorList = info => dispatch => {
    const configs = {
        method: 'post',
        url: URL.GET_HOSPITAL_DOCTPRLIST,
        data: info,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
   
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.GET_HOSPITAL_DOCTOR_SUCCESS,
                payload: res.data
            })
            // toastr.success('Message', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', res.data.message, toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_HOSPITAL_DOCTOR_FAILURE,
            error: err
        })
    })

}

//update hospital doctor
export const updateHospitalDoctor = info => dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDATE_HOSPITAL_DOCTOR,
        data: info,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
    let listdata = {
        "uid": info.hospitalId
    }
    return axios(configs).then((res) => {
        if (res.data.status == "success") {
            dispatch(getHospitalDoctorList(listdata))
            dispatch({
                type: actionType.UPDTAE_HOSPITAL_DOCTOR_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
            return Promise.resolve("success");
        } else if (res.data.status == "failure") {
            toastr.error('Error', res.data.message, toastrOptions);
            return Promise.resolve("failure");

        }
    }).catch(err => {
        dispatch({
            type: actionType.UPDTAE_HOSPITAL_DOCTOR_FAILURE,
            error: err
        })
    })

}

//LIST HOSPITAL PATIENT
export const getHospitalPatientList = info => dispatch => {
    const configs = {
        method: 'post',
        url: URL.GET_HOSPITAL_PATIENT_LIST,
        data: info,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
  
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.GET_HOSPITAL_PATIENT_SUCCESS,
                payload: res.data
            })
            toastr.success('Success',"Patient list retrieved successfully", toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', res.data.message, toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_HOSPITAL_PATIENT_FAILURE,
            error: err
        })
    })

}
//ADD HOSPITAL PATIENT
export const addHospitalPatient = info => dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_HOSPITAL_PATIENT,
        data: info,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
    let listdata = {
        "uid": info.uid
    }
 return axios(configs).then((res) => {
        if (res.data.status == "success") {
            dispatch(getHospitalPatientList(listdata))
            dispatch({
                type: actionType.ADD_HOSPITAL_PATIENT_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
            return Promise.resolve("success")
        } else if (res.data.status == "failure") {
            toastr.error('Error', res.data.message, toastrOptions);
            return Promise.resolve("failure")
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_HOSPITAL_PATIENT_FAILURE,
            error: err
        })
    })

}
//UPDATE HOSPITAL PATIENT
export const updateHospitalPatient = info => dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDTAE_HOSPITAL_PATIENT,
        data: info,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
    let listdata = {
        "uid": info.userId
    }
   return  axios(configs).then((res) => {
        if (res.data.status == "success") {
            dispatch(getHospitalPatientList(listdata))
            dispatch({
                type: actionType.UPDTAE_HOSPITAL_PATIENT_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
            return Promise.resolve("success")

        } else if (res.data.status == "failure") {
            toastr.error('Error', res.data.message, toastrOptions);
            return Promise.resolve("failure")

        }
    }).catch(err => {
        dispatch({
            type: actionType.UPDTAE_HOSPITAL_PATIENT_FAILURE,
            error: err
        })
    })

}

//UPLOAD PROFILE PICTURE
export const uploadProfilepic = info => dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPLOAD_PROFILE_PIC,
        data: info,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
    let listdata = {
        "uid": info.userId
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            // dispatch(getHospitalPatientList(listdata))
            dispatch({
                type: actionType.UPLOAD_PROFILEPIC_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', res.data.message, toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.UPLOAD_PROFILEPIC_FAILURE,
            error: err
        })
    })

}