import React, { Component } from "react";
import TelemedicineRouting from "../innerrouting/TelemedicineRouting";
import Logout from '../../common/Logout'
class TeleMedicineMenu extends Component {
    handleChange = (path) => {
        this.props.history.push({ pathname: path, state: { submenuType: "Telemedicine" } })
    }
    handleArrow = () => {
        this.props.history.push("/mainmenu");
    }
    render() {

        return (
            <section>
                <header className="header">
                    <div className="brand">
                        <a href="javascript:void(0)" className="backArrow" onClick={this.handleArrow}>
                            <i className="fas fa-chevron-left"></i>
                        </a>
                        <div className="mobileLogo">
                            <a href="/mainmenu" className="logo">
                                <img src="../images/logo.jpg" /> </a>
                        </div>

                    </div>
                    <div className="top-nav">

                        <div className="dropdown float-right mr-2" >
                            <a className="dropdown-toggle" data-toggle="dropdown">
                                Patient Name
                    </a>
                            <Logout />
                        </div>
                    </div>
                </header>


                {/* <aside className="sideMenu">
                    <ul className="sidemenuItems">
                        <li><a  href="javascript:void(0)" onClick={() =>this.props.history.push({pathname: "/submenu",state: { submenuType: "Personal" }})} title="Personal"><img src="../images/icon2-color.png" /></a><div className="menu-text"><span>Personal</span></div></li>
                        <li><a onClick={() =>this.props.history.push({pathname: "/submenu",state: { submenuType: "Telemedicine" }})}  title="Telemedicine"><img src="../images/icon5-color.png" /></a><div className="menu-text"><span>Telemedicine</span></div></li>
                        <li><a href="javascript:void(0)" onClick={() =>this.props.history.push({pathname: "/submenu",state: { submenuType: "HealthRecord" }})} title="Health Records"><img src="../images/icon1-color.png" /></a><div className="menu-text"><span>Health Records</span></div></li>
                        <li><a href="javascript:void(0)" title="Pharmacy"><img src="../images/icon4-color.png" /></a> <div className="menu-text"><span>Pharmacy</span></div></li>
                        <li><a href="javascript:void(0)" title="Find Your medical specialist"><img src="../images/icon7-color.png" /></a><div className="menu-text"><span>Find Your medical specialist</span></div></li>

                        <li><a href="javascript:void(0)" title="Services"><img src="../images/icon7-color.png" /></a><div className="menu-text"><span>Services</span></div></li>
                        <li><a href="javascript:void(0)" title="Share Data"><img src="../images/icon6-color.png" /></a><div className="menu-text"><span>Share data</span></div></li>
                    </ul>

                </aside> */}

                <div className="mainMenuSide" id="main-content">
                    <div className="wrapper">
                        <div className="container-fluid">
                            <div className="body-content">
                                <div className="row">
                                    <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                                        <a href="javaScript:void(0);" onClick={() => this.handleChange("/telemedicine/patientappointments")} className="card bg-c submenuIcon">
                                            <div className="card-img1 submenuIconImg ">
                                                <img src="images/personal-color.png" />
                                            </div>
                                            <div className="card-title">
                                                <h5>My Appointments</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                                        <a href="javaScript:void(0);" onClick={() => this.handleChange("/telemedicine/mydoctor")} className="card bg-c submenuIcon">
                                            <div className="card-img1 submenuIconImg ">
                                                <img src="images/personal-color.png" />
                                            </div>
                                            <div className="card-title">
                                                <h5>My Doctors FeedBack</h5>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }


}
export default TeleMedicineMenu;