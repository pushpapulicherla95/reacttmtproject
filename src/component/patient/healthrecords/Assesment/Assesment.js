import React, { Component } from "react";
import axios from "axios";
import URL from "../../../../asset/configUrl";
import { PDFDownloadLink } from "@react-pdf/renderer";
import MyDocument from "./pdf";
import ChatBot from "../../../../component/landing/ChatBot1";
class Assesment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listReport: [],
      tab: "finalReport",
      option: [],
      question: [],
      symptoms: sessionStorage.getItem("symptoms"),
      open: false,
      continue: null,
      viewStep: null,
      chatbotEnable: false
    };
  }

  componentWillMount = () => {
    const assessmentId = parseInt(localStorage.getItem("assesmentId"));
    this.listReports();
    if (assessmentId) {
      this.finalReport();
      this.nonRegister();
    }
  };

  componentWillReceiveProps(newprops) {
    if (newprops.location.pathname == "/symptoms/assesment") {
      this.setState({ open: false, chatbotEnable: false }, () => {
        this.listReports();
        this.finalReport();
        this.nonRegister();
      });
    }
  }

  handleChatbot = () => {
    this.setState({ open: !this.state.open });
  };

  nonRegister = () => {
    const user = JSON.parse(sessionStorage.getItem("loginDetails"));

    const configs = {
      method: 'post',
      url: URL.NONREGISTERED_CHAT,
      data: {
        uid: user.userInfo.userId,
        assesmentId: parseInt(localStorage.getItem("assesmentId"))
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }


    axios(configs)
      .then(response => {
        console.log("assesmentInfo---------->", response.data);
      })
      .catch(error => { });
  };

  finalReport = () => {
    this.setState({ open: false });
    const user = JSON.parse(sessionStorage.getItem("loginDetails"));
    const configs = {
      method: 'post',
      url: URL.OPENREPORT_CHAT,
      data: {
        uid: user.userInfo.userId,
        assesmentId: parseInt(localStorage.getItem("assesmentId"))
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }


    axios(configs)
      .then(response => {
        this.handleTab(JSON.parse(response.data.assesmentInfo), "start");
        this.setState({
          question: JSON.parse(response.data.assesmentInfo)
        });
      })
      .catch(error => { });
  };

  listReports = () => {
    const user = JSON.parse(sessionStorage.getItem("loginDetails"));
    const configs = {
      method: 'post',
      url: URL.OPENREPORT_LIST,
      data: {
        uid: user.userInfo.userId
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }

    axios(configs)
      .then(response => {
        localStorage.setItem(
          "assesmentId",
          localStorage.getItem("assesmentId")
        );
        console.log("resposenmr",response.data.openReport)
        this.setState({ listReport: response.data.openReport });
      });
  };
  handleTab = (each, str) => {
    if (str == "reports") {
      const { openReport } = each;
      const { question } = JSON.parse(openReport);
      this.setState({ tab: str, option: question, symptoms: each.symptoms });
    } else if (str === "start") {
      this.setState({
        option: each.question
      });
    }
  };

  render() {
    const { question } = this.state.question;
    const option =
      this.state.option &&
      this.state.option.filter(each => {
        return each.possibleCauses != null && each.possibleCauses;
      });
      console.log("yjs.sate",this.state)
    return (
      <div>
        {this.state.listReport.length > 0 ?
          <div>

            <div className="mainMenuSide" id="main-content">
              <div className="wrapper">
                <div className="container-fluid">
                  <div className="body-content">
                    <div className="tomCard">
                      <div class="assList">
                        <div class="row">
                          {this.state.listReport.map(each => (
                            <div class="col-lg-2 col-sm-2 col-md-6 col-6 padRig">
                              <div class="position-relative">
                                <a onClick={() => this.handleTab(each, "reports")}>
                                  <div class="assList1">
                                    <h5>
                                      {each.symptoms &&
                                        each.symptoms.charAt(0).toUpperCase() +
                                        each.symptoms.slice(1)}
                                    </h5>
                                    <span>
                                      {/* new Date(Date.now()).toJSON().slice(0, 10) */}
                                      {each.dateTime}
                                    </span>
                                  </div>
                                </a>

                                <div className="PDF_icon_section">
                                  {each.status == "end" ? (
                                    <PDFDownloadLink
                                      document={<MyDocument value={each} />}
                                      fileName="symptoms.pdf"
                                    >
                                      <i class="fas fa-file-download" />
                                    </PDFDownloadLink>
                                  ) : (
                                      <a
                                        className="btn-transperent size-12"
                                        onClick={() =>
                                          this.setState(
                                            {
                                              continue: each,
                                              viewStep: "step5",
                                              chatbotEnable: true
                                            },
                                            () => this.handleChatbot()
                                          )
                                        }
                                      >
                                        {" "}
                                        <i class="fas fa-redo-alt" />
                                      </a>
                                    )}
                                </div>
                              </div>
                            </div>
                          ))}
                        </div>
                        <div />
                      </div>
                      <div className="assDetails">
                        <div className="tomCardHead tomcardheadh5">
                          <h5 className="float-left">
                            {this.state.symptoms &&
                              this.state.symptoms.charAt(0).toUpperCase() +
                              this.state.symptoms.slice(1)}
                          </h5>
                        </div>
                        {/* <div className="box1">
                      <h5>Summary</h5>
                      <p>
                        Eye pain is a catch-all phrase to describe discomfort
                        on, in, behind or around the eye. The pain can be
                        unilateral or bilateral — in other words, you can
                        experience right eye pain, left eye pain, or the
                        discomfort can affect both eyes. There's no evidence
                        that right eye pain occurs more frequently than left eye
                        pain, or vice versa. In some cases, such as an eye
                        injury, the cause of the pain is obvious. But often it's
                        difficult to know why your eye hurts.
                      </p>
                    </div> */}

                        <div className="box1">
                          {option.length > 0 && <h5>Possible Causes</h5>}
                          {option.map((each, i) => {
                            if (each.possibleCauses) {
                              return (
                                <div class="row">
                                  <div class="col-lg-2 col-md-2 col-sm-2 col-2">
                                    <div class="indexValue">
                                      <label>{i + 1}</label>
                                    </div>
                                  </div>
                                  <div class="col-lg-10 col-md-10 col-sm-10 col-10">
                                    <div class="causeTitle">
                                      {/* <h5>Blepharitis</h5> */}
                                      <p>{each.possibleCauses}</p>
                                    </div>
                                  </div>
                                </div>
                              );
                            }
                          })}
                          {/* <div class="row">
                          <div class="col-lg-2 col-md-2 col-sm-2 col-2">
                            <div class="indexValue">
                              <label>2 </label>
                            </div>
                          </div>
                          <div class="col-lg-10 col-md-10 col-sm-10 col-10">
                            <div class="causeTitle">
                              <h5>Iritis</h5>
                              <p>
                                Iritis is an inflammation of the iris, or
                                colored part of the eye, that causes one to
                                feel deep eye or orbital pain, usually
                                accompanied by blurred vision and light
                                sensitivity.
                              </p>
                            </div>
                          </div>
                        </div> */}
                          {/* <div class="row">
                          <div class="col-lg-2 col-md-2 col-sm-2 col-2">
                            <div class="indexValue">
                              <label>3 </label>
                            </div>
                          </div>
                          <div class="col-lg-10 col-md-10 col-sm-10 col-10">
                            <div class="causeTitle">
                              <h5>Sinusitis</h5>
                              <p>
                                Sinusitis, which is a bacterial or viral
                                infection of the sinuses, can cause a
                                sensation of orbital or eye socket pain.
                                Pain coming from the sinus cavities can be
                                interpreted as eye pain.
                              </p>
                            </div>
                          </div>
                        </div> */}
                        </div>

                        <div
                          className="box1"
                          id="content"
                          style={{ backgroundColor: "#ffffff" }}
                        >
                          <h5>Symptoms</h5>
                          <div class="symptomsList">
                            <ul>
                              <li class="symptomTitle">
                                {this.state.symptoms &&
                                  this.state.symptoms.charAt(0).toUpperCase() +
                                  this.state.symptoms.slice(1)}
                                {/* <li>Pounding : yes</li>
                              <li>Laterality : one side</li> */}
                                {question &&
                                  question.map((each, i) => {
                                    if (each.question && each.selectedOption) {
                                      return (
                                        <ul>
                                          <li>
                                            <strong>{each.question}</strong>
                                          </li>
                                          <li>{each.selectedOption}</li>
                                        </ul>
                                      );
                                    }
                                  })}
                                {this.state.option &&
                                  this.state.option.map((each, i) => {
                                    if (each.question && each.selectedOption) {
                                      return (
                                        <ul>
                                          <li>
                                            <strong>{each.question}</strong>
                                          </li>
                                          <li>{each.selectedOption}</li>
                                        </ul>
                                      );
                                    }
                                  })}
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {this.state.chatbotEnable && (
              <div class="live-chat">
                <button
                  type="button"
                  id="chatBtn"
                  class="livechatbtn positionfixed"
                  onClick={this.handleChatbot}
                >
                  <img src="../images/chat-icon.png" alt="Live Chat" />
                </button>
                <ChatBot
                  handleChatbot={this.handleChatbot}
                  open={this.state.open}
                  continue={this.state.continue}
                  viewStep={this.state.viewStep}
                />
              </div>
            )}

          </div> : "No Records Found"
        }
      </div>
    );
  }
}
export default Assesment;
