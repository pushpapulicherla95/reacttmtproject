import React, { Component } from 'react';
import PersonalRouting from "../innerrouting/PersonalRouting";
import HealthRecordRoute from "../innerrouting/HealthRecordRouting";
import $ from "jquery";
import Logout from "../../common/Logout";
import AsideMenu from "../../common/AsideMenu"

class MedicalMenu extends Component {

    
    handleChange = (path) => {
        const clientWidth = window.innerWidth;
        if (clientWidth < 479) {
            document.getElementById("submenuHideOnMobile").style.display = "none";
        }
        this.props.history.push(path)
    }
    handleArrow = () =>{
        const clientWidth = window.innerWidth;
        if (clientWidth < 479) {
            if (document.getElementById("submenuHideOnMobile").style.display == "none") {
                document.getElementById("submenuHideOnMobile").style.display = "block";
            } else {
                this.props.history.push({pathname:'/submenu',state:{submenuType:"HealthRecord"}})
                document.getElementById("submenuHideOnMobile").style.display = "none";
            }
        }
    }
    render() {
        return (
          <div className="inner-pageNew">
            <section>
              <header className="header">
                <div className="brand">
                  <a
                    href="javascript:void(0)"
                    className="backArrow"
                    onClick={this.handleArrow}
                  >
                    <i className="fas fa-chevron-left" />
                  </a>
                  <div className="mobileLogo">
                    <a href="/mainmenu" className="logo">
                      <img src="../images/logo.jpg" />{" "}
                    </a>
                  </div>
                </div>
                <div className="top-nav">
                  <div className="dropdown float-right mr-2">
                    <a
                      className="dropdown-toggle"
                      data-toggle="dropdown"
                    >
                      Patient Name
                    </a>
                    <Logout />
                  </div>
                </div>
              </header>

              <AsideMenu/>

              <div className="inner-subMenu" id="submenuHideOnMobile">
                <ul className="sid-subMenuitems">
                  <li>
                    <a
                      title="Personal"
                      href="JavaScript:Void(0);"
                      onClick={() =>
                        this.handleChange("/medicine/data")
                      }
                      id="per-cli"
                    >
                      Medicines
                    </a>
                  </li>
                  <li>
                    <a
                      title="Personal"
                      href="JavaScript:Void(0);"
                      onClick={() =>
                        this.handleChange("/medicine/allergies")
                      }
                      id="per-cli"
                    >
                      Allergies
                    </a>
                  </li>
                  <li>
                    <a
                      title="Personal"
                      href="JavaScript:Void(0);"
                      onClick={() =>
                        this.handleChange("/medicine/accident")
                      }
                      id="per-cli"
                    >
                      Accidents
                    </a>
                  </li>
                  <li>
                    <a
                      title="Personal"
                      href="JavaScript:Void(0);"
                      onClick={() =>
                        this.handleChange("/medicine/hospital")
                      }
                      id="per-cli"
                    >
                      Length of Hospital/Rehab stay
                    </a>
                  </li>
                  <li>
                    <a
                      title="Personal"
                      href="JavaScript:Void(0);"
                      onClick={() =>
                        this.handleChange("/medicine/finding")
                      }
                      id="per-cli"
                    >
                      Finding
                    </a>
                  </li>
                  <li>
                    <a
                      title="Personal"
                      href="JavaScript:Void(0);"
                      onClick={() =>
                        this.handleChange("/medicine/therapy")
                      }
                      id="per-cli"
                    >
                      Diagnosis/Therapy
                    </a>
                  </li>
                  <li>
                    <a
                      title="Personal"
                      href="JavaScript:Void(0);"
                      onClick={() =>
                        this.handleChange("/medicine/immunization")
                      }
                      id="per-cli"
                    >
                      Immunizations
                    </a>
                  </li>
                </ul>
              </div>
              <HealthRecordRoute />
            </section>
          </div>
        );
    }

}

export default MedicalMenu;