

// api to social login
const socialLogin = async (req, res) => {

   console.log('socialLogin : ', req.body)
   let loginResponse = new Object()
   let userType = "patient"
   let cityName = "Munich"
   let userTypeId, cityId
   let gender = "Male"
   let age = 25
   try {
      const [rows, fields] = await pool.query('select id from user_type_info where user_type=?', [userType])
      console.log('social login : user_type_info : rows : ', rows);
      userTypeId = rows[0].id;
      console.log('social login : user_type_info : userTypeId : ', userTypeId);
   } catch (error) {
      console.log('social login : user_type_info : error : ', error.message);
      loginResponse = {
         'status': 'failure',
         'message': error.message
      };
      //console.log('Message %s sent: %s', info.messageId, info.response);
      console.log('socialLogin : loginResponse : ', loginResponse);
      console.log('JSON.stringify(socialLogin) : ', JSON.stringify(loginResponse));
      return res.end(JSON.stringify(loginResponse));

   }

   try {
      const [rows, fields] = await pool.query('select id from city_info where city_name=?', [cityName])
      console.log('social login : city_info : results : ', rows);
      cityId = rows[0].id;
      console.log('social login : city_info : cityId : ', cityId);
   } catch (error) {
      console.log('social login : city_info : error : ', error.message);
      loginResponse = {
         'status': 'failure',
         'message': error.message
      };
      //console.log('Message %s sent: %s', info.messageId, info.response);
      console.log('socialLogin : loginResponse : ', loginResponse);
      console.log('JSON.stringify(socialLogin) : ', JSON.stringify(loginResponse));

   }

   let userId = 0;
   //SELECT id FROM user_info where first_name="karthik" and last_name="venkat" and social_provider_type="google";
   try {
      const [rows, fields] = await pool.query('select id from user_info where first_name=? and last_name=? and social_provider_type=?', [req.body.firstName, req.body.lastName, req.body.provider])
      console.log('social login : user_info : results : ', rows);
      if (rows.length > 0) {
         userId = rows[0].id;
         console.log('social login : user_info : userId : ', userId);
      }
   } catch (error) {
      console.log('social login error', error.message);
      loginResponse = {
         'status': 'failure',
         'message': error.message
      };
      //console.log('Message %s sent: %s', info.messageId, info.response);
      console.log('socialLogin : loginResponse : ', loginResponse);
      console.log('JSON.stringify(socialLogin) : ', JSON.stringify(loginResponse));
      return res.end(JSON.stringify(loginResponse));
   }
   if (userId > 0) {
      console.log('socialLogin : ', userId)
      loginResponse = {
         'status': 'success',
         'message': 'Logged in successfully'
      };
      //console.log('Message %s sent: %s', info.messageId, info.response);
      console.log('send email : loginResponse : ', loginResponse);
      console.log('JSON.stringify(loginResponse) : ', JSON.stringify(loginResponse));
      return res.end(JSON.stringify(loginResponse));
   }
   console.log("before inserting : userTypeId" + userTypeId + " : cityId : " + cityId)

   try {
      const [rows, fields] = await pool.execute('INSERT INTO user_info(first_name,last_name,email,age,gender,mobile_no,user_type_id,city_id,social_provider_type) values(?,?,?,?,?,?,?,?,?)',
         [req.body.firstName, req.body.lastName, req.body.email, age, gender, req.body.mobileNo, userTypeId, cityId, req.body.provider])//, function (error, results, fields) {
      console.log('socialLogin : ', rows)
      loginResponse = {
         'status': 'success',
         'message': 'Logged in successfully'
      };
      //console.log('Message %s sent: %s', info.messageId, info.response);
      console.log('send email : loginResponse : ', loginResponse);
      console.log('JSON.stringify(loginResponse) : ', JSON.stringify(loginResponse));
      return res.end(JSON.stringify(loginResponse));
   } catch (error) {
      console.log('social login error', error.message);
      loginResponse = {
         'status': 'failure',
         'message': error.message
      };
      //console.log('Message %s sent: %s', info.messageId, info.response);
      console.log('socialLogin : loginResponse : ', loginResponse);
      console.log('JSON.stringify(socialLogin) : ', JSON.stringify(loginResponse));
      return res.end(JSON.stringify(loginResponse));
   } 

};




module.exports = {
   socialLogin
}
