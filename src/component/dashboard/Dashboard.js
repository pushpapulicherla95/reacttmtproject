import React, { Component } from 'react';
import { createDoctorSchedule, insertDoctorInfo, listDoctorInfo, listScheduleInfo, deleteSchedule, updateDoctorSchedule } from '../../service/doctor/action';
import { connect } from "react-redux";
import DashboardMenu from "../../component/dashboard/DashboardMenu"
import Header from '../common/Header';
import SideMenu from '../common/SideMenu';
import Footer from '../common/Footer';
class Dashboard extends Component {
    render() {
        return (
            <section>
                <Header />
                {/* <SideMenu /> */}
                <aside class="app-sidebar" id="sidebar">
                    <div class="app-sidebar__user">
                        <div class="imgupload">
                            <img class="app-sidebar__user-avatar" src="../images/user.png" alt="Admin Image" />
                        </div>
                        <div>
                            <p class="app-sidebar__user-name">John Doe</p>
                            <span class="app-sidebar__user-designation">Admin</span>

                        </div>

                    </div>
                    <ul class="icon-menu">
                        <li>
                            <a class="dropdown-item" href="patientProfile.html" title="My account">
                                <i class="fa fa-edit icon-head-menu"></i>
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="Yourpurchases.html" title="Your purchases">
                                <i class="fas fa-dollar-sign icon-head-menu"></i>
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="Shop.html" title="My account">
                                <i class="fa fa-shopping-cart"></i>
                            </a>
                        </li>
                    </ul>
                    <ul class="app-menu">
                        <li>
                            <a class="app-menu__item " href="Dashboard">
                                <div class="imgclrs clrMain">
                                    <i class="fas fa-tachometer-alt"></i>
                                </div>
                                <span class="app-menu__label">Dashboard</span>
                            </a>
                        </li>

                        <li>
                            <a class="app-menu__item dropdown-toggle" href="#patientsubmenu" aria-expanded="false" data-toggle="collapse">
                                <div class="imgclrs clrMain">
                                    <i class="fas fa-user"></i>
                                </div>

                                <span class="app-menu__label">Personal</span>
                            </a>
                            <ul class="submenu collapse" id="patientsubmenu">
                                <li>
                                    <a class=" app-menu__item dropdown-toggle" href="#patientsubmenu1" aria-expanded="false" data-toggle="collapse">
                                        <div class="imgclrs clrSub">
                                            <i class="far fa-user"></i>
                                        </div>

                                        <span class="app-menu__label"> Personal Data </span>
                                    </a>
                                    <ul class="submenutosub appmenu collapse" id="patientsubmenu1">
                                        <li>
                                            <a class="app-menu__item" href="Personal.html">
                                                <div class="imgclrs clrSubToSub">
                                                    <i class="far fa-user"></i>
                                                </div>

                                                <span class="app-menu__label"> Personal</span>
                                            </a>

                                        </li>

                                        <li>
                                            <a class="app-menu__item" href="coreData.html">
                                                <div class="imgclrs clrSubToSub">
                                                    <i class="fas fa-address-book"></i>
                                                </div>

                                                <span class="app-menu__label">Core Data</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="app-menu__item" href="job.html">
                                                <div class="imgclrs clrSubToSub">
                                                    <i class="fas fa-user-md"></i>
                                                </div>

                                                <span class="app-menu__label">Job </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="app-menu__item" href="Emergency.html">
                                                <div class="imgclrs clrSubToSub">
                                                    <i class="fas fa-id-card"></i>
                                                </div>

                                                <span class="app-menu__label">Emergency</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="app-menu__item" href="Benefits.html">
                                                <div class="imgclrs clrSubToSub">
                                                    <i class="fas fa-id-card"></i>
                                                </div>

                                                <span class="app-menu__label">Benefits</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="app-menu__item" href="Sport.html">
                                                <div class="imgclrs clrSubToSub">
                                                    <i class="fas fa-id-card"></i>
                                                </div>

                                                <span class="app-menu__label">Sports</span>
                                            </a>
                                        </li>

                                    </ul>
                                </li>

                                <li>
                                    <a class="app-menu__item" href="Curriculum.html">
                                        <div class="imgclrs clrSub">
                                            <i class="fas fa-address-book"></i>
                                        </div>

                                        <span class="app-menu__label">Curriculum</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="app-menu__item" href="MyDoctors.html">
                                        <div class="imgclrs clrSub">
                                            <i class="fas fa-user-md"></i>
                                        </div>

                                        <span class="app-menu__label">My Doctors</span>
                                    </a>
                                </li>
                                <li>
                                    <a class=" app-menu__item dropdown-toggle" href="#insurabcesubmenu1" aria-expanded="false"
                                        data-toggle="collapse">
                                        <div class="imgclrs clrSub">
                                            <i class="far fa-user"></i>
                                        </div>

                                        <span class="app-menu__label"> Insurance</span>
                                    </a>
                                    <ul class="submenutosub appmenu collapse" id="insurabcesubmenu1">
                                        <li>
                                            <a class="app-menu__item" href="HealthInsuranceCompany.html">
                                                <div class="imgclrs clrSubToSub">
                                                    <i class="far fa-user"></i>
                                                </div>

                                                <span class="app-menu__label">Health Insurance Company</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="app-menu__item" href="AdditionalInsurance.html">
                                                <div class="imgclrs clrSubToSub">
                                                    <i class="far fa-user"></i>
                                                </div>

                                                <span class="app-menu__label">Additional Insurance</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="app-menu__item" href="InsuranceAccident.html">
                                                <div class="imgclrs clrSubToSub">
                                                    <i class="far fa-user"></i>
                                                </div>

                                                <span class="app-menu__label">Insurance Accident</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a class=" app-menu__item dropdown-toggle" href="#medical" aria-expanded="false" data-toggle="collapse">
                                <div class="imgclrs clrMain">
                                    <i class="far fa-circle"></i>
                                </div>

                                <span class="app-menu__label">Medical data</span>
                            </a>
                            <ul class="submenu appmenu collapse" id="medical">
                                <li>
                                    <a class="app-menu__item dropdown-toggle" href="#medicalData" aria-expanded="false" data-toggle="collapse">
                                        <div class="imgclrs clrSub">
                                            <i class="far fa-circle"></i>
                                        </div>

                                        <span class="app-menu__label">Medicines</span>
                                    </a>
                                    <ul class="submenutosub appmenu collapse" id="medicalData">
                                        <li>
                                            <a class="app-menu__item " onClick={() => this.props.history.push("/dashboard/addAllergy")}>
                                                <div class="imgclrs clrSubToSub">
                                                    <i class="far fa-circle"></i>
                                                </div>

                                                <span class="app-menu__label">Allergies</span>
                                            </a>
                                        </li>

                                        <li>
                                            <a class="app-menu__item "onClick={() => this.props.history.push("/dashboard/accident")}>
                                                <div class="imgclrs clrSubToSub">
                                                    <i class="far fa-circle"></i>
                                                </div>

                                                <span class="app-menu__label">Accidents</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="app-menu__item" onClick={()=>this.props.history.push("/dashboard/hospital")}>
                                                <div class="imgclrs clrSubToSub">
                                                    <i class="far fa-circle"></i>
                                                </div>

                                                <span class="app-menu__label">Lenght of Hospital/Rehab stay</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="app-menu__item " onClick={() => this.props.history.push("/dashboard/finding")}>
                                                <div class="imgclrs clrSubToSub">
                                                    <i class="far fa-circle"></i>
                                                </div>

                                                <span class="app-menu__label">Finding</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="app-menu__item " onClick={() => this.props.history.push("/dashboard/therapy")}>
                                                <div class="imgclrs clrSubToSub">
                                                    <i class="far fa-circle"></i>
                                                </div>

                                                <span class="app-menu__label">Diagnosis/Therapy</span>
                                            </a>
                                        </li>

                                        <li>
                                            <a class="app-menu__item " onClick={() => this.props.history.push("/dashboard/immunization")}>
                                                <div class="imgclrs clrSubToSub">
                                                    <i class="far fa-circle"></i>
                                                </div>

                                                <span class="app-menu__label">Immunizations</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="app-menu__item " href="Misc.html">
                                                <div class="imgclrs ">
                                                    <i class="far fa-circle"></i>
                                                </div>

                                                <span class="app-menu__label">Misc</span>
                                            </a>
                                        </li>

                                    </ul>
                                </li>
                                <li>
                                    <a class="app-menu__item " href="Diseases.html">
                                        <div class="imgclrs clrSub">
                                            <i class="far fa-circle"></i>
                                        </div>

                                        <span class="app-menu__label">Diseases</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="app-menu__item" href="Symptoms.html">
                                        <div class="imgclrs clrSub">
                                            <i class="far fa-circle"></i>
                                        </div>

                                        <span class="app-menu__label">Symptoms</span>
                                    </a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a class="app-menu__item " href="Sharedata.html">
                                <div class="imgclrs clrMain">
                                    <i class="fa fa-share-alt-square"></i>
                                </div>
                                <span class="app-menu__label">Send / Receive data</span>
                            </a>
                        </li>
                        <li>
                            <a class="app-menu__item dropdown-toggle" href="#service" aria-expanded="false" data-toggle="collapse">
                                <div class="imgclrs clrMain">
                                    <i class="fa fa-user-secret"></i>
                                </div>

                                <span class="app-menu__label">Services
        </span>
                            </a>
                            <ul class="submenu appmenu collapse" id="service">
                                <li>
                                    <a class="app-menu__item " href="">
                                        <div class="imgclrs clrSub">
                                            <i class="far fa-circle"></i>
                                        </div>

                                        <span class="app-menu__label"> Call Center</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="app-menu__item " href="#">
                                        <div class="imgclrs clrSub">
                                            <i class="far fa-circle"></i>
                                        </div>

                                        <span class="app-menu__label">Doctor Callback</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="app-menu__item " href="">
                                        <div class="imgclrs clrSub">
                                            <i class="far fa-circle"></i>
                                        </div>

                                        <span class="app-menu__label">Expertise </span>
                                    </a>
                                </li>
                                <li>
                                    <a class="app-menu__item " href="">
                                        <div class="imgclrs clrSub">
                                            <i class="far fa-circle"></i>
                                        </div>

                                        <span class="app-menu__label">Digitalisation</span>
                                    </a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a class="app-menu__item " onClick={() => this.props.history.push("/dashboard/shedulelist")} >
                                <div class="imgclrs clrMain">
                                    <i class="fa fa-share-alt-square"></i>
                                </div>
                                <span class="app-menu__label">Schedule List</span>
                            </a>
                        </li>
                        <li>
                            <a class="app-menu__item " onClick={()=>this.props.history.push("/dashboard/doctorlist")} >
                                <div class="imgclrs clrMain">
                                    <i class="fa fa-share-alt-square"></i>
                                </div>
                                <span class="app-menu__label">Doctor Info</span>
                            </a>
                        </li>
                        <li>
                            <a class="app-menu__item " onClick={()=>this.props.history.push("/dashboard/addPatient")} >
                                <div class="imgclrs clrMain">
                                    <i class="fa fa-share-alt-square"></i>
                                </div>
                                <span class="app-menu__label">Add Patient Healthdata</span>
                            </a>
                        </li>
                        <li>
                            <a class="app-menu__item " onClick={()=>this.props.history.push("/dashboard/pharmacydetails")} >
                                <div class="imgclrs clrMain">
                                    <i class="fa fa-share-alt-square"></i>
                                </div>
                                <span class="app-menu__label">PharmacyProduct List</span>
                            </a>
                        </li> 
                        <li>
                            <a class="app-menu__item " onClick={()=>this.props.history.push("/dashboard/categorylist")} >
                                <div class="imgclrs clrMain">
                                    <i class="fa fa-share-alt-square"></i>
                                </div>
                                <span class="app-menu__label">Product Catogery List</span>
                            </a>
                        </li>
                        <li>
                            <a class="app-menu__item " onClick={()=>this.props.history.push("/dashboard/patient/appointment")} >
                                <div class="imgclrs clrMain">
                                    <i class="fa fa-share-alt-square"></i>
                                </div>
                                <span class="app-menu__label">Patient Appointments</span>
                            </a>
                        </li>
                        <li>
                            <a class="app-menu__item " onClick={()=>this.props.history.push("/dashboard/appointment/book")} >
                                <div class="imgclrs clrMain">
                                    <i class="fa fa-share-alt-square"></i>
                                </div>
                                <span class="app-menu__label">Book Appointment</span>
                            </a>
                        </li>

                    </ul>
                </aside>
                <section id="main-content" class="marginLeftRes">
                    <div class="wrapper">
                        <div class="container-fluid">
                            <DashboardMenu />
                        </div>
                    </div>

                </section>
                <Footer />
            </section>
        )
    }
}


const mapStateToProps = state => ({
    // loginDetails: state.loginReducer.loginDetails
});

const mapDispatchToProps = dispatch => ({
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Dashboard);