import React, { Component } from "react";
import {
  patientAppointmentListAPICall,
  updateFeedback
} from "../../../../service/appointment/action";
import { connect } from "react-redux";
import Loader from "react-loader-spinner";
import { NavLink } from "react-router-dom";
import moment from 'moment';


class AppointmentList extends Component {
  constructor(props) {
    super(props)
    this.state = {
    schedule: "false",
    manageuser: "true",
    userInfoId: "2",
    startTime: "",
    endTime: "",
    appointmentDate: "",
    doctorId: "2",
    status: "A",
    editSchedulepop: false,
    editDetails: {},
    patientAppointmentList: [],
    addSchedulepop: false,
    openfeedback: false,
    anamnesis: "",
    objectives: "",
    diagnosis: "",
    treatment: ""
  };
  this.allInterval = []

}
  componentWillMount = () => {
    const { userInfo } = this.props.loginDetails;
    const patientInfo = {
      //"patientId": "19"
      patientId: userInfo.userId
    };

    this.props.patientAppListMethod(patientInfo);
  };
  componentDidMount = () => {
    const { userInfo } = this.props.loginDetails;
    if (!navigator.onLine) {
      this.setState({
        patientAppointmentList: JSON.parse(
          localStorage.getItem("patientAppointmentList")
        )
      });
    } else {
      const patientInfo = {
        patientId: userInfo.userId
      };
      this.props.patientAppListMethod(patientInfo);
    }
  };
  componentWillReceiveProps = () => {
    const {
      appointmentDoctorLists,
      patientName
    } = this.props.listPatientAppointment;
    this.setState({ patientAppointmentList: appointmentDoctorLists });
  };
  handleChange = e => {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });
  };
  handleSchedule = () => {
    const { userInfo } = this.props.loginDetails;

    const {
      doctorId,
      startTime,
      endTime,
      appointmentDate,
      status
    } = this.state;
    const doctorSchedule = {
      doctorId: userInfo.userId,
      startTime: startTime,
      endTime: endTime,
      appointmentDate: appointmentDate,
      status: status
    };
    this.props.createDoctorSchedule(doctorSchedule);
    const scheduleinfo = {
      doctorId: userInfo.userId
    };
    this.props.listScheduleInfo(scheduleinfo);
    this.setState({ addSchedulepop: false });
  };
  //updateSchedule
  updateSchedule = () => {
    const {
      scheduleId,
      startTime,
      endTime,
      appointmentDate,
      status
    } = this.state;
    const updateSchedule = {
      scheduleId: scheduleId,
      startTime: startTime,
      endTime: endTime,
      appointmentDate: appointmentDate,
      status: status
    };
    this.props.updateDoctorSchedule(updateSchedule);
    this.setState({ editSchedulepop: false });
  };
  joinMeeting = e => {
    const meetingInfo = {
      meetingRoom: e.meetingroom
    };
  };
  componentWillUnmount = () => {
    this.allInterval.map((data) => {
        clearInterval(data)
    })
}
  openJitseeMeet = data => {
    let meetingRoom = data.meetingRoom;
    let patientName = data.patientName;
    localStorage.setItem("meetingRoom", meetingRoom);
    localStorage.setItem("patientName", patientName);
    localStorage.setItem("doctorId", data.doctorId);
    localStorage.setItem("patientId", data.patientId);
    this.props.history.push("/telemedicine/joinmeeting");
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  // handleFeedback = () => {
  //     const { anamnesis, objectives, diagnosis, treatment } = this.state;

  //     let feedBack = {
  //         "doctorId": "1",
  //         "patientId": "9",
  //         anamnesis: anamnesis,
  //         objectives: objectives,
  //         diagnosis: diagnosis,
  //         treatment: treatment

  //     }
  //     console.log("feedBack", feedBack);
  //     this.props.updateFeedback(feedBack);
  //     this.setState({ openfeedback: false });
  // }
  render() {
    const { isLoading } = this.props;
    const {
      startTime,
      endTime,
      appointmentDate,
      status,
      anamnesis,
      objectives,
      diagnosis,
      treatment
    } = this.state;
    const { patientAppointmentList } = this.state;
    return (
      <React.Fragment>
        {isLoading && (
          <div className="pop-mod">
            <div className="loder-back">
              <Loader type="Circles" color="#00BFFF" height="100" width="100" />
            </div>
          </div>
        )}
        <div class="mainMenuSide" id="main-content">
          <div class="wrapper">
            <div class="container-fluid">
              <div class="body-content">
                <div class="tomCard">
                  <div class="tomCardHead">
                    <h5 class="float-left">Appointments</h5>
                  </div>

                  <div class="tab-menu-content tomCardBody">
                    <div class="tab-menu-title">
                      <ul>
                        <li class="menu1 active">
                          <a href="JavaScript:Void(0);">
                            <p>List of Appointments </p>
                          </a>
                        </li>
                      </ul>
                    </div>
                    <div class="tab-menu-content-section">
                      <div id="content-1" style={{ display: "block" }}>
                        <div class="table-responsive">
                          <table class="table table-hover">
                            <thead class="thead-default">
                              <tr>
                                <th>Appointment Date</th>
                                <th>Timings</th>
                                <th>Doctor Name</th>
                                <th>Status</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {/* <tr>
                                                                <td data-label="Appointment Date">2019-06-20</td>
                                                                <td data-label="Start Time">13:00 - 14:00 - 13:00 - 14:00</td>
                                                                <td data-label="Patient Name">John</td>
                                                                <td data-label="status">Active </td>
                                                                <td data-label="Action"><a title="Join Meeting" onClick={() => this.openFeedbackPop()} class="commonBtn2 ">Join Meeting</a></td>
                                                            </tr> */}
                              {
                                  patientAppointmentList && patientAppointmentList.map((each, i) => {
                                    // console.log(moment(each.appointmentDate).isAfter())
                                  let disabled;

                                  if (moment().format("YYYY-MM-DD") == each.appointmentDate) {
                                      disabled = false;
                                      var newInterval = setInterval(() => {
                                          const currentTime = moment();
                                          const startTime = moment(each.startTime, "HH:mm ");
                                          const endTime = moment(each.endTime, "HH:mm");
                                          const amIBetween = currentTime.isBetween(startTime, endTime);
                                          const beforeCheck = currentTime.isBefore(startTime)
                                          const afterCheck = currentTime.isAfter(endTime)
                                          if (document.getElementById("meeting" + i)) {
                                              if (amIBetween) {
                                                  document.getElementById("meeting" + i).textContent = "Join Meeting"
                                                  document.getElementById("meeting" + i).disabled = false

                                              } else if (beforeCheck) {
                                                  let duration = moment.duration(startTime.diff(currentTime))
                                                  let hours = parseInt(duration.asHours());
                                                  let minutes = parseInt(duration.asMinutes()) % 60;
                                                  let seconds =  parseInt(duration.asSeconds()) % 60;
                                      
                                                  let timeString = hours + ':' + minutes +':'+ seconds + ''
                                              
                                                  document.getElementById("meeting" + i).textContent = "Meeting in " + timeString
                                                  document.getElementById("meeting" + i).disabled = true

                                              } else if (afterCheck) {
                                                  document.getElementById("meeting" + i).textContent = "Meeting Ended"
                                                  document.getElementById("meeting" + i).disabled = true
                                              }
                                          }

                                      }, 100)
                                      this.allInterval.push(newInterval)
                                  } else {
                                      disabled = true;
                                      const currentTime = moment();
                                      const beforeCheck = moment(each.appointmentDate).isAfter(currentTime)
                                      if (document.getElementById("meeting" + i)) {
                                          if (beforeCheck) {
                                            
                                                  document.getElementById("meeting" + i).textContent = "Yet To Start"
                                        
                                          } else {
                                            
                                                  document.getElementById("meeting" + i).textContent = "Meeting Ended"
                                           
                                          }
                                      }

                                  }

                                return(

                                

                                  <tr key={i}>
                                    <td data-label="Appointment Date">
                                      {each.appointmentDate}
                                    </td>
                                    <td data-label="Start Time">
                                      {each.startTime} - {each.endTime}
                                    </td>
                                    <td data-label="Doctor Name">
                                      {each.doctorName}
                                    </td>
                                    <td data-label="status">{each.status} </td>
                                    <td data-label="Action"><button id={"meeting" + i} title="Join Meeting" disabled={disabled} onClick={() => this.openJitseeMeet(each)} class="commonBtn2 ">Join Meeting</button></td>
                                  </tr>
                                )
                                      })
                                    }
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* feedback form */}
        {/* <div className={this.state.openfeedback ? "modal show d-block" : "modal"} >
                    <div class="modal-dialog">
                        <div class="modal-content modalPopUp">
                            <div class="modal-header borderNone">
                                <h5 class="modal-title">Update Feedback</h5>
                                <button type="button" class="popupClose" data-dismiss="modal" onClick={this.modalPopUpClose}><i class="fas fa-times"></i></button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12">

                                            <div class="form-group">
                                                <label class="commonLabel">Anamnesis</label>
                                                <textarea className="form-control" name="anamnesis" value={anamnesis} onChange={this.handleChange}></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Objectives</label>
                                                <textarea className="form-control" name="objectives" value={objectives} onChange={this.handleChange}></textarea>

                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Diagnosis</label>
                                                <textarea className="form-control" name="diagnosis" value={diagnosis} onChange={this.handleChange}></textarea>

                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Treatment</label>
                                                <textarea className="form-control" name="treatment" value={treatment} onChange={this.handleChange}></textarea>

                                            </div>
                                        </div>

                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <button type="button" class="commonBtn float-right" onClick={this.handleFeedback}>Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
 */}

        {/* <div className="cui-utils-content">
                    <div className="cui-utils-title mb-3"><strong className="text-uppercase font-size-16">My Appointments</strong></div>
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="card">
                                
                                <div className="card-body">
                                    <div className="table-responsive">
                                        <table className="table table-hover">
                                            <thead className="thead-default">
                                                <tr>
                                                    <th>Appointment Date</th>
                                                    <th>Start Time</th>
                                                    <th>End Time</th>
                                                    <th>Doctor Name</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    patientAppointmentList && patientAppointmentList.map((each,i) => (
                                                        <tr key={i}>
                                                            <td data-label="Appointment Date">{each.appointmentDate}</td>
                                                            <td data-label="Start Time">{each.startTime}</td>
                                                            <td data-label="End Time">{each.endTime}</td>
                                                            <td data-label="Doctor Name">{each.doctorName}</td>
                                                            <td data-label="status">{each.status} </td>
                                                             <td data-label="Action"><a title="Join Telemedicine" className="deleteBtn" onClick={(e)=>this.joinMeeting(each)}>V</a></td> 
                                                            <td data-label="Action">  <button onClick={()=>this.openJitseeMeet(each)}   title="Join Telemedicine" className="deleteBtn" >V</button></td>

                                                        </tr>))
                                                }

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> */}
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  listPatientAppointment: state.appointmentReducer.listPatientAppointment,
  isLoading: state.loginReducer.isLoading,
  loginDetails: state.loginReducer.loginDetails
});

const mapDispatchToProps = dispatch => ({
  patientAppListMethod: patientInfo =>
    dispatch(patientAppointmentListAPICall(patientInfo)),
  joinMeetingMethod: patientInfo =>
    dispatch(patientAppointmentListAPICall(patientInfo)),
  updateFeedback: feedBack => dispatch(updateFeedback(feedBack))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppointmentList);
