
const validate = (fieldName, value) => {
    let errorValid;
    let errorMessage;
    switch (fieldName) {
        // email validation //
        case "email":
            errorValid = value.match(
                /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/
            );
            errorMessage = errorValid
                ? ""
                : "Please enter valid email address.";
            let emailValidData = {
                errorValid,
                errorMessage
            }
            return emailValidData
        //password validation
        case "password":
            errorValid =
                value.length >= 8 &&
                value.match(
                    /(?=.*[_!@#$%^&*-])(?=.*\d)(?!.*[.\n])(?=.*[a-z])(?=.*[A-Z])^.{8,}$/
                );
            errorMessage = errorValid
                ? ""
                : "Please enter valid Password.";
            let passwordValidData = {
                errorValid,
                errorMessage
            }
            return passwordValidData;
        //Name validation
       
        case "fristName":
        case "lastName":
        case "familyName":
        case "title":
        case "pob":
            errorValid =
                value.length >= 3 &&
                value.match(
                    "^[a-zA-Z -]*$"
                );
            errorMessage = errorValid
                ? ""
                : fieldName  + " "+ "should be 3 to 20 characters  " ;
            let nameValidData = {
                errorValid,
                errorMessage
            }
            return nameValidData;
        //title validation
        // case "title":

        //     errorValid =
        //         value.length >= 3
        //     errorMessage = errorValid
        //         ? ""
        //         : "Please enter valid" + fieldName;
        //     let titleValidData = {
        //         errorValid,
        //         errorMessage
        //     }
        //     return titleValidData; //  
        //dataofbirth validation
        case "dob":
            errorValid =

                value.match(
                    /^([0-9]{2})-([0-9]{2})-([0-9]{4})$/
                );
            errorMessage = errorValid
                ? ""
                : "Please enter valid Data Of Birth.";
            let dobValidData = {
                errorValid,
                errorMessage
            }
            return dobValidData;
        default:
            break;

    }

}

export default validate