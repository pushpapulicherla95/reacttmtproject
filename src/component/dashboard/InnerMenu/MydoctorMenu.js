import React, { Component } from "react";
import MydoctorRounting from "../innerrouting/MydoctorRounting";
import Logout from '../../common/Logout'
import { withRouter } from "react-router";
import AsideMenu from '../../common/AsideMenu'

class MydoctorMenu extends Component {

    render() {
        return (
            <div className="inner-pageNew">
                <section>
                    <header className="header">
                        <div className="brand">
                            <a href="/mainmenu" className="backArrow">
                                <i className="fas fa-chevron-left"></i>
                            </a>
                            <div className="mobileLogo">
                                <a href="/mainmenu" className="logo">
                                    <img src="../images/logo.jpg" /> </a>
                            </div>

                        </div>
                        <div className="top-nav">

                            <div className="dropdown float-right mr-2" >
                                <a className="dropdown-toggle" data-toggle="dropdown">
                                    Patient Name
                    </a>
                                <Logout />

                            </div>
                        </div>
                    </header>

                    <AsideMenu/>
                    {/* <aside className="sideMenu">
                        <ul className="sidemenuItems">
                            <li><a href="/mainmenu" onClick={() => this.props.history.push("/personal/data")} title="Personal"><img src="../images/icon2-color.png" /></a><div className="menu-text"><span>Personal</span></div></li>
                            <li><a href="/mainmenu" title="Telemedicine"><img src="../images/icon5-color.png" /></a><div className="menu-text"><span>Telemedicine</span></div></li>
                            <li><a href="/mainmenu" title="Health Records"><img src="../images/icon1-color.png" /></a><div className="menu-text"><span>Health Records</span></div></li>
                            <li><a href="/mainmenu" title="Pharmacy"><img src="../images/icon4-color.png" /></a> <div className="menu-text"><span>Pharmacy</span></div></li>
                            <li><a href="/mainmenu" title="Find Your medical specialist"><img src="../images/icon7-color.png" /></a><div className="menu-text"><span>Find Your medical specialist</span></div></li>
                            <li><a href="/mainmenu" title="Services"><img src="../images/icon7-color.png" /></a><div className="menu-text"><span>Services</span></div></li>
                            <li><a href="/mainmenu" title="Share Data"><img src="../images/icon6-color.png" /></a><div className="menu-text"><span>Share data</span></div></li>
                        </ul>

                    </aside> */}


                    {/* <div className="" id="main-content">
                        <div className="wrapper">
                            <div className="container-fluid">
                                <div className="body-content">
                                    <div id="ponal-sectioners">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> */}
                    <MydoctorRounting />

                </section>
            </div >
        )
    }
}
export default withRouter(MydoctorMenu);