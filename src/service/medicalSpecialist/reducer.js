import actionType from "../medicalSpecialist/actionType";

const initialState = {
  clinicDetails: "",
  clinicDoctorDetails: "",
  searchDoctorDetails: ""

};
const medicalSpecialistReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SEARCHCLINIC_SUCCESS:
      return {
        ...state,
        clinicDetails: action.payload
      };
    case actionType.SEARCHCLINIC_FAILURE:
      return {
        ...state,
        clinicDetails: action.error
      };
    case actionType.CLINICDOCTOR_SUCCESS:
      return {
        ...state,
        clinicDoctorDetails: action.payload
      };
    case actionType.CLINICDOCTOR_FAILURE:
      return {
        ...state,
        clinicDoctorDetails: action.error
      };
    case actionType.SEARCH_DOCTOR_DETAILS_SUCCESS:
      return {
        ...state,
        searchDoctorDetails: action.payload
      }
    case actionType.SEARCH_DOCTOR_DETAILS_FAILURE:
      return {
        ...state,
        searchDoctorDetails: action.error
      }
    default:
      return state;
  }
};
export default medicalSpecialistReducer;
