import React from "react";
class ShareData extends React.Component {
  render() {
    return (
      <div class="mainMenuSide" id="main-content">
        <div class="wrapper">
          <div class="container-fluid">
            <div class="body-content">
              <div class="tomCard shareSent">
                <div class="tomCardHead">
                  <h5 class="float-left">Share/Sent Data</h5>
                </div>
                <div class="tomCardBody">
                  <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                      <div class="share_pageBtn">
                        <ul>
                          <li>
                            <a href="">
                              <div class="cplink br_share">
                                <img src="images/copy-link-white.png" />
                              </div>
                              <div class="share-optionName">Copy Link</div>
                            </a>
                          </li>
                          <li>
                            <a href="">
                              <div class="defLink br_share">
                                <img src="images/gmail-icon.png" />
                              </div>
                              <div class="share-optionName">Gmail</div>
                            </a>
                          </li>
                          <li>
                            <a href="">
                              <div class="fb br_share">
                                <img src="images/facebook-logo.png" />
                              </div>
                              <div class="share-optionName">Facebook</div>
                            </a>
                          </li>

                          <li>
                            <a href="">
                              <div class="wechat br_share">
                                <img src="images/whatsapp-icon-white.png" />
                              </div>
                              <div class="share-optionName">Whats App</div>
                            </a>
                          </li>
                          <li>
                            <a href="">
                              <div class="linked br_share">
                                <img src="images/linkedin-logo1.png" />
                              </div>
                              <div class="share-optionName">linkedin</div>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    {/* <!-- <div class="col-lg-1 col-md-1 col-sm-4 col-12">
                                           <a href="#"><img src="images/copy-link.png"/>
                                            <span> Copy Link</span></a> 
                                        </div> -->
                                  <!-- <div class="col-lg-1 col-md-1 col-sm-4 col-4">
                                     <a href=""> <img src="images/download-icon.png"/>
                                      <span>Download</span></a>
                                  </div> -->
                                  <!-- <div class="col-lg-1 col-md-1 col-sm-4 col-4">
                                      <a href="#">
                                        <img src="images/share-icon.png"/>
                                        <span>Email</span></a>
                                    </div> --> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default ShareData;
