import actionType from '../pharmacy/actionType';
const initialState = {
    
    createproductDetails:"",
    listproductDetails:"",
    updateproductDetails:"",
    deleteproductDetails:"",
    }
    const pharmacyReducer = (state = initialState, action) => {
    
        switch (action.type) {
            case actionType.CREATE_PRODUCT_SUCCESS:
            return {
                ...state,
                createproductDetails:action.payload,
            
            }
        case actionType.CREATE_PRODUCT_FAILURE:
            return {
                ...state,
                createproductDetails:action.payload,

            }
            case actionType.LIST_PRODUCT_SUCCESS:
            return {
                ...state,
                listproductDetails:action.payload,
            
            }
        case actionType.LIST_PRODUCT_FAILURE:
            return {
                ...state,
                listproductDetails:action.payload,

            }
            case actionType.UPDATE_PRODUCT_SUCCESS:
            return {
                ...state,
                updateproductDetails:action.payload,
            
            }
        case actionType.UPDATE_PRODUCT_FAILURE:
            return {
                ...state,
                updateproductDetails:action.payload,

            }
            case actionType.DELETE_PRODUCT_SUCCESS:
            return {
                ...state,
                deleteproductDetails:action.payload,
            
            }
        case actionType.DELETE_PRODUCT_FAILURE:
            return {
                ...state,
                deleteproductDetails:action.payload,

            }
        default:
            return state;
    }
}
export default pharmacyReducer;