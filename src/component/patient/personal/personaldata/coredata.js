import React, { Component } from "react";
import {
  addCoredata,
  getPersonaldata
} from "../../../../service/healthrecord/action";
import { getUploadFiles, handleFileDelete } from "../../../../service/upload/action";
import URL from "../../../../asset/configUrl";
import { connect } from "react-redux";
import $ from "jquery";
import axios from "axios";
import { toastr } from "react-redux-toastr";
import { awsPersonalFiles } from "../../../../service/common/action";
import { NumbersAndDot } from "../KeyValidation"
class coredata extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 1,
      tabone: true,
      tabtwo: false,
      weight: "",
      height: "",
      alcohol: "",
      smoke: "",
      patientFindingListData: [],
      getpersonalDetails: null,
      uploadPopup: false,
      name: "",
      fileName: "",
      fileType: "",
      fileURL: "",
      errors: {

        fileURL: "",

      },
      fileURLValid: false,
      Valid: false,
    };
  }
  ValidationField = () => {
    const {
      weight,
      height,
      alcohol,
      smoke } = this.state
    this.setState({
      Valid:
        weight &&
        height

    });

  }
  handleAwsFiles = value => {
    window.open("https://data.tomatomedical.com/patientpersonal/" + value, '_blank');

  };

  // handleChangePopup = e => {
  //   var reader = new FileReader();
  //   var file = e.target.files[0];
  //   reader.onload = () => {
  //     this.setState({
  //       fileURL: reader.result,
  //       fileName: file.name,
  //       fileType: file.type
  //     });
  //   };
  //   reader.readAsDataURL(file);
  // };
  handleChangePopup = e => {
    var reader = new FileReader();
    const name = e.target.name;

    var value = e.target.files[0];
    console.log("file", e.target.files)
    let name1 = value.name.split('.')[0]
    let type = "." + value.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileURL: reader.result,
        fileName: name1,
        fileType: type
      }, () => {
        this.validateField(name, value);
      });
    };
    reader.readAsDataURL(value);
  };
  popupSubmit = () => {
    var phrase = this.state.fileURL;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);
    var { userInfo } = this.props.loginDetails;
    const payload = {
      uid: userInfo.userId,
      z: match[1],
      fileName: this.state.fileName,
      fileType: this.state.fileType,
      name: this.state.name,
      category: "coreData"
    };
    this.setState({ uploadPopup: false });

    axios
      .post(URL.FILECOREDATA_UPLOAD, payload)
      .then(response => {
        response.data.status === "success"
          ? toastr.success("Message", response.data.message)
          : toastr.error("Message", response.data.message);
        if (response.data.status === "success") {
          var { userInfo } = this.props.loginDetails;
          const uploadFiles = {
            uid: userInfo.userId,
            category: "coreData"
          };
          this.props.getUploadFiles(uploadFiles);
          this.setState({ uploadPopup: false });
          this.setState({
            name: "",
            fileName: "",
            fileType: "",
            fileURL: ""
          });
        }
      })
      .catch(error => {
        // this.setState({ uploadPopup: true });
      });
  };

  componentWillMount() {
    const { userInfo } = this.props.loginDetails;

    const listpersonal = {
      uid: userInfo.userId
    };

    const uploadFiles = { uid: userInfo.userId, category: "coreData" };
    this.props.getUploadFiles(uploadFiles);
    this.props.getPersonaldata(listpersonal);
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.getpersonalDetails &&
      nextProps.getpersonalDetails.coredata != null &&
      nextProps.getpersonalDetails.coredata != 0
    ) {
      const listpersonal = nextProps.getpersonalDetails.coredata;
      let patientFindingListData = JSON.parse(listpersonal);
      this.setState({
        weight: patientFindingListData.weight,
        height: patientFindingListData.height,
        alcohol: patientFindingListData.alcohol,
        smoke: patientFindingListData.smoke
      });
    }
  }
  tabOne = () => {
    this.setState({ tabone: true, tabtwo: false });
  };
  tabTwo = () => {
    this.setState({ tabone: false, tabtwo: true });
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value }, () => this.ValidationField());
  };

  trigger = () => {
    this.setState({ uploadPopup: !this.state.uploadPopup });
  };

  handleData = () => {
    const { weight, height, alcohol, smoke } = this.state;
    const { userInfo } = this.props.loginDetails;
    const coreData = {
      userId: userInfo.userId,
      core: {
        weight: weight,
        height: height,
        alcohol: alcohol,
        smoke: smoke
      }
    };

    this.props.addCoredata(coreData);
  };
  emptystate = () => {
    this.setState({
      fileURL: "",

    })
  }
  validateField(fieldName, value) {

    const { errors, fileURLValid } = this.state;

    let fileURLvalid = fileURLValid;

    let fieldValidationErrors = errors;

    switch (fieldName) {


      case "fileURL":

        console.log("FILEs", value.size)
        fileURLvalid = value.size > 50000 && value.size <= 1000000
        fieldValidationErrors.fileURL = fileURLvalid
          ? ""
          : "File size should be less than 1MB.";
        break;

    }
    this.setState({
      errors: fieldValidationErrors,

      fileURLValid: fileURLvalid,

    },
      this.validateForm
    )
  }
  //
  validateForm() {
    const {
      fileURLValid
    } = this.state;
    this.setState({
      formValid:
        fileURLValid,

    });

  }

  render() {
    const {
      tabone,
      tabtwo,
      weight,
      height,
      alchohal,
      smoke,
      patientFindingListData, errors, formValid, Valid
    } = this.state;

    return (
      <div className="" id="main-content">
        <div className="wrapper">
          <div className="container-fluid">
            <div className="body-content">
              <div className="tomCard">
                <div className="tomCardHead">
                  <h5 className="float-left">COREDATA</h5>
                </div>

                <div className="tab-menu-content">
                  <div class="tab-menu-title">
                    <ul>
                      <li className={this.state.tabone ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabOne}>
                          <p>Core Data</p>
                        </a>
                      </li>
                      <li className={this.state.tabtwo ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabTwo}>
                          <p>Attached Files</p>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>

                {tabone && (
                  <div className="tab-menu-content-section">
                    <div
                      id="content-1"
                      className={tabone ? "d-block" : "d-none"}
                    >
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Weight (in kg) <sup>*</sup></label>
                            <input
                              type="text"
                              class="form-control"
                              name="weight"
                              onChange={this.handleChange} onKeyPress={NumbersAndDot}
                              value={this.state.weight}
                            />
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Height (in cm)<sup>*</sup></label>
                            <input
                              type="text"
                              class="form-control"
                              name="height"
                              onChange={this.handleChange}
                              value={this.state.height}
                              onKeyPress={NumbersAndDot}
                            />
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Alcohol consumption</label>
                            <select
                              class="form-control"
                              name="alcohol"

                              onChange={this.handleChange}
                              value={this.state.alcohol}
                            >
                              <option value="">-- Please select --</option>
                              <option value="I never drink alcohol">
                                I never drink alcohol
                              </option>
                              <option value="Up to 0.5 litre Beer per day or 0.25 litre Wine per day">
                                Up to 0.5 litre Beer per day or 0.25 litre Wine
                                per day
                              </option>
                              <option value="Up to 1 litre Beer per day or 0.5 litre Wine per day">
                                Up to 1 litre Beer per day or 0.5 litre Wine per
                                day
                              </option>
                              <option value="More than 1 litre Beer per day or 0.5 litre Wine day">
                                More than 1 litre Beer per day or 0.5 litre Wine
                                day
                              </option>
                            </select>
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Smoke</label>
                            <select
                              class="form-control"
                              name="smoke"
                              onChange={this.handleChange}
                              value={this.state.smoke}
                            >
                              <option value="" >
                                Please select
                              </option>
                              <option value="Non-smoker">Non-smoker</option>
                              <option value="0 to 1 Packs per day">
                                0 to 1 Pack per day
                              </option>
                              <option value="More than 1 Pack per day">
                                More than 1 Pack per day
                              </option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-12">
                          <button
                            type="button"
                            class="commonBtn float-right"
                            onClick={this.handleData} disabled={!Valid}
                          >
                            Save
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                {tabtwo && (
                  <div id="content-2" className={tabtwo ? "d-block" : "d-none"}>
                    <button
                      type="button"
                      className="commonBtn2 float-right mb-2"
                      id="upBtn"
                      onClick={this.trigger}
                    >
                      <i className="fa fa-plus" />
                      <span> Upload new files</span>                  </button>

                    <div className="table-responsive">
                      <table className="table table-hover">
                        <thead className="thead-default">
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>FileName</th>

                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.props.getFilesDetails.fileDetails &&
                            this.props.getFilesDetails.fileDetails.map(
                              (each, i) => (
                                <tr>
                                  <td data-label="#">{i + 1}</td>
                                  <td data-label="Name">{each.name}</td>
                                  <td data-label="FileName"> {each.fileName}</td>
                                  <td data-label="Action">
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.handleAwsFiles(each.fileUrl)
                                      }
                                    >
                                      <i class="fas fa-download" />
                                    </button>
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.props.handleFileDelete(each.id, each.fileUrl, "coreData")
                                      }
                                    >
                                      <i class="fas fa-trash" />
                                    </button>
                                  </td>
                                </tr>
                              )
                            )}
                          {/* <tr>
                            <td data-label="#">1</td>
                            <td data-label="Name">Eye Care Test Report</td>
                            <td data-label="Type">png</td> */}
                          {/* <td data-label="Size">87.9 kB</td>
                            <td data-label="Date">16.05.2019 11:56</td> */}
                          {/* <td data-label="Action">
                              <a
                                title="Edit"
                                className="editBtn"
                                data-toggle="modal"
                                data-target="#myModal"
                              >
                                <i className="fa fa-edit" />
                              </a>
                              <a
                                title="Delete"
                                className="deleteBtn"
                                onClick={e => this.deletePatient()}
                              >
                                <i
                                  className="fa fa-trash"
                                  data-toggle="modal"
                                  data-target="#myModal2"
                                />
                              </a>
                            </td> */}
                          {/* </tr> */}
                        </tbody>
                      </table>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <div
          className={this.state.uploadPopup ? "modal d-block" : "modal"}
          id="myModal"
        >
          <div class="modal-dialog">
            <div class="modal-content modalPopUp">
              <div class="modal-header borderNone">
                <h5 class="modal-title">Upload File</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  onClick={this.trigger}
                >
                  <i class="fas fa-times" />
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="form-group">
                        <label className="form-label">File name</label>
                        <input
                          className="form-control"
                          type="text"
                          name="name"
                          value={this.state.name}
                          onChange={this.handleChange}
                        />
                        <span />
                      </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="fileUp">
                        <label
                          for="fileUp1"
                          class="commonBtn text-center upload"
                        >
                          Upload File
                        </label>
                        <input
                          type="file"
                          name="fileURL"
                          id="fileUp1"
                          style={{ display: "none" }}
                          onChange={this.handleChangePopup}
                        />

                        <div style={{ color: "red" }}>{errors.fileURL}</div>
                      </div>

                    </div>

                    <div class="col-md-12 col-lg-12 col-sm-12 mt-2">
                      <button
                        type="button"
                        class="commonBtn float-right"
                        disabled={!this.state.fileURL || !formValid}

                        onClick={this.popupSubmit}
                      >
                        Submit
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  getpersonalDetails: state.healthrecordReducer.getpersonalDetails,
  loginDetails: state.loginReducer.loginDetails,
  getFilesDetails: state.uploadReducer.getFilesDetails
});

const mapDispatchToProps = dispatch => ({
  addCoredata: coreData => dispatch(addCoredata(coreData)),
  getPersonaldata: listpersonal => dispatch(getPersonaldata(listpersonal)),
  getUploadFiles: payload => dispatch(getUploadFiles(payload)),
  handleFileDelete: (id, fileUrl, type) => dispatch(handleFileDelete(id, fileUrl, type))

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(coredata);
