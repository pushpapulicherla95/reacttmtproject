// const API = "https://api1.tomatomedical.com/tomatomedical/API";
const API = "http://192.168.2.10:9001/tomatomedical/API";
const NodeAPIURL = "https://api2.tomatomedical.com/API";
const SocketBaseURL = "https://api2.tomatomedical.com/";

const configUrl = {
  BUCKETURL: "https://data.tomatomedical.com/",
  SOCKET_URL: SocketBaseURL,
  USER_LOGIN: `${API}/login `,
  PATIENT_REGISTER: `${API}/patient/register `,
  USER_REGISTER: `${API}/register`,
  HOSPITAL_REGISTER: `${API}/hospital/register`,
  USER_FORGOTPASSWORD: `${API}/forgotpassword `,
  USER_CHANGEPASSWORD: `${API}/forgotpassword/update`,
  USER_GETUSERPROFILE: `${API}profile/view`,
  USER_PASSWORDCHANGE: `${API}/changepassword `,
  USER_EDITUSERPROFILE: `${API}/edit `,
  USER_TYPE_LIST: `${API}/usertype/list`,
  //doctor
  INSERT_DOCTOR_INFO: `${API}/schedule/insertdoctorinfo`,
  LIST_DOCTOR_INFO: `${API}/doctor/prefessional/info`,
  LIST_SCHEDULE_INFO: `${API}/schedule/doctor/getschedulelistbyuserid`,
  CREATE_SCHEDULE: `${API}/doctor/schedule/create`,
  DELETE_SCHEDULE: `${API}/doctor/schedule/delete`,
  UPDATE_SCHEDULE: `${API}/doctor/schedule/update`,
  GET_SEARCH_DOC_DETAILS: `${API}/schedule/searchbyspecialistdetails`,
  GET_DOC_PROFILE: `${API}/retrieve/doctor/profileinfo`,
  SAVE_DOC_PROFILE: `${API}/doctor/professional/info`,

  //patient
  ADD_PATIENT_HEALTH_RECORD: `${API}/patient/health/allergies/add`,
  UPDTAE_PATIENT_HEALTH_RECORD: `${API}/patient/health/allergies/edit`,
  GET_PATIENT_HEALTH_RECORD: `${API}/patient/health/retrive`,
  DELETE_PATIENT_HEALTH_RECORD: `${API}/patient/health/allergies/delete`,
  GET_SPECIALIST: `${API}/schedule/listspecialist`,
  GET_DOCTORLIST: `${API}/schedule/doctor/list`,
  //Accident
  ADD_ACCIDENT: `${API}/patient/health/accidents/add`,
  GET_ACCIDENT: `${API}/patient/health/retrive`,
  UPDATE_ACCIDENT: `${API}/patient/health/accidents/edit`,
  DELETE_ACCIDENT: `${API}/patient/health/accidents/delete`,
  //Immunization
  ADD_IMMUNIZATION: `${API}/patient/health/immunizations/add`,
  GET_IMMUNIZATION: `${API}/patient/health/retrive`,
  UPDATE_IMMUNIZATION: `${API}/patient/health/immunizations/edit`,
  DELETE_IMMUNIZATION: `${API}/patient/health/immunizations/delete`,
  //Finding
  ADD_FINDING: `${API}/patient/health/findings/add`,
  GET_FINDING: `${API}/patient/health/retrive`,
  UPDATE_FINDING: `${API}/patient/health/findings/edit`,
  DELETE_FINDING: `${API}/patient/health/findings/delete`,
  //medicine
  ADD_MEDICINE: `${API}/patient/health/medication/add`,
  EDIT_MEDICINE: `${API}/patient/health/medication/edit`,
  DELETE_MEDICINE: `${API}/patient/health/medication/delete`,

  //Therapy
  ADD_THERAPY: `${API}/patient/health/theraphy/add`,
  GET_THERAPY: `${API}/patient/health/retrive`,
  UPDATE_THERAPY: `${API}/patient/health/theraphy/edit`,
  DELETE_THERAPY: `${API}/patient/health/theraphy/delete`,
  //product
  CREATE_PRODUCT: `${API}/product/add`,
  LIST_PRODUCT: `${API}/product/list`,
  UPDATE_PRODUCT: `${API}/product/update`,
  DELETE_PRODUCT: `${API}/product/delete`,
  //productcatogery
  CREATE_PRODUCTCATEGORY: `${API}/category/add`,
  LIST_PRODUCTCATEGORY: `${API}/category/list`,
  UPDATE_PRODUCTCATEGORY: `${API}/category/update`,
  DELETE_PRODUCTCATEGORY: `${API}/category/delete`,
  //Appointment
  PATIENT_APPOINTMENT_LIST: `${API}/appointment/patient/list`,
  DOCTOR_APPOINTMENT_LIST: `${API}/appointment/doctor/list`,
  BOOK_APPOINTMENT: `${API}/appointment/add`,

  //health record
  ADD_HOSPITAL: `${API}/patient/health/hospitals/add`,
  EDIT_HOSPITAL: `${API}/patient/health/hospitals/edit`,
  DELETE_HOSPITAL: `${API}/patient/health/hospitals/delete`,

  //Node API's
  SEND_EMAIL: `${NodeAPIURL}/email/send`,
  SEND_SMS: `${NodeAPIURL}/sms/send`,
  SOCIAL_LOGIN: `${NodeAPIURL}/social/login`,
  CHAT_HISTORY: `${NodeAPIURL}/chat/history`,
  //PersonalData
  ADD_PERSONALDATA: `${API}/patient/personaldata/personal`,
  //Coredata
  ADD_COREDATA: `${API}/patient/personaldata/core`,
  //Job
  ADD_JOBDATA: `${API}/patient/personaldata/job`,

  //Emergency
  ADD_EMERGENCY: `${API}/patient/personaldata/emergency`,
  //Benifits
  ADD_BENEFITS: `${API}/patient/personaldata/benefits`,
  //list
  LIST_PERSONALDATA: `${API}/patient/allpersonaldata/list`,
  //dieases
  LIST_DIEASESDATA: `${API}/patient/medicaldata/allpersonaldata/diseaselist`,
  //add
  ADD_DIEASES: `${API}/patient/healthrecord/disease/addupdate`,
  //curriculumadd
  ADD_CURRICULUM: `${API}/patient/personaldata/curriculumgeneral`,
  ADD_EDUCATION: `${API}/patient/health/curriculum/education/add`,
  ADD_PREVIOUSJOB: `${API}/patient/health/curriculum/previous/jobs/add`,
  UPDATE_EDUCATION: `${API}/patient/health/curriculum/education/edit`,
  DELETE_EDUCATION: `${API}/patient/health/curriculum/education/delete`,
  UPDATE_PREVIOUSJOB: `${API}/patient/health/curriculum/previous/jobs/edit`,
  DELETE_PREVIOUSJOB: `${API}/patient/health/curriculum/previous/jobs/delete`,
  ADD_LANGUAGE: `${API}/patient/health/curriculum/language/skills/add`,
  UPDATE_LANGUAGE: `${API}/patient/health/curriculum/language/skills/edit`,
  DELETE_LANGUAGE: `${API}/patient/health/curriculum/language/skills/delete`,
  //mydoctor
  ADD_DOCTOR: `${API}/patient/health/personal/mydoctors/add`,
  UPDATE_DOCTOR: `${API}/patient/health/personal/mydoctors/edit`,
  DELETE_DOCTOR: `${API}/patient/health/personal/mydoctors/delete`,
  //doctor list country
  GET_COUNTRY: `${API}/location/countrylist`,
  GET_STATE: `${API}/location/search/bycountry`,
  GET_AREA: `${API}/location/search/bycities`,
  GET_POSTALCODE: `${API}/location/search/byarea`,
  GET_CITY: `${API}/location/search/bystate`,
  HEALTH_INSURANCE: `${API}/patient/health/personal/insurance/health`,
  ADDITIONAL_HEALTH_INSURANCE: `${API}/patient/health/personal/insurance/addionalinsurance`,
  ACCIDENT_INSURANCE: `${API}/patient/health/personal/insurance/accident`,
  GETALL_INSURANCE: `${API}/patient/allpersonaldata/list`,
  UPDATE_CALL_FEEDBACK: `${API}/tele/medicine/form/add`,
  UPDATE_DOCTOR_FEEDBACK: `${API}/tele/medicine/form/update`,
  DOCTOR_MANAGE_ADS: `${API}/managead/add`,
  DOC_MANAGEADS_EDIT: `${API}/managead/update`,
  DOC_MANAGEADS_LIST: `${API}/managead/list`,
  DOC_MANAGEADS_DELETE: `${API}/managead/delete`,

  //admin
  ADMIN_LOGIN: `${API}/admin/login`,
  ADMIN_PATIENT_LIST: `${API}/admin/patient/list`,
  ADMIN_DOCTOR_LIST: `${API}/admin/doctor/list`,
  ADMIN_HOSPITAL_LIST: `${API}/admin/hospital/list`,
  GET_LOGDATA: `${API}/log/details/`,
  UPLOAD_ADMIN__PROFILE_PIC: `${API}/admin/profile/picture`,
  //ChatBOT API
  LIST_CHAT: `${API}/symptoms/id/list`,
  CHAT: `${API}/chat`,
  ASSESSMENT_CHAT: `${API}/userassesment`,
  OPENREPORT_CHAT: `${API}/userassesment/openreport`,
  NONREGISTERED_CHAT: `${API}/user/not registerd`,
  OPENREPORT_LIST: `${API}/user/openreport/list`,



  APPROVE_DOCTOR: `${API}/admin/approval`,
  EDIT_PATIENT_DETAILS: `${API}/admin/view`,
  GET_ADMIN_PROFILE: `${API}/admin/profile`,
  UPDTAE_ADMIN_PROFILE: `${API}/admin/profile/add`,
  SYMPTOMS_CHECKER_ADD: `${API}/symptomchecker/add`,
  SYMPTOMS_CHECKER_LIST: `${API}/symptomchecker/list`,
  SEARCH_SYMPTOMS: `${API}/symptomchecker/listsymptombysearch`,
  UPDATE_CAUSE: `${API}/symptomchecker/edit`,

  //mailsending
  REGISTER_MAIL: `${NodeAPIURL}/email/register`,
  FORGET_MAIL: `${NodeAPIURL}/email/forget`,
  BOOKINGAPPOINMENT_MAIL: `${NodeAPIURL}/email/mailBookingAppoinment`,

  //listpatient
  LIST_PATIENT_INFO: `${API}/tele/medicine/form/listbydoctorid`,
  LIST_DOCTOR_INFO: `${API}/tele/medicine/form/listbypatientid`,
  ADD_SPORTDATA: `${API}/patient/health/personaldata/sports/add`,
  UPDATE_SPORTDATA: `${API}/patient/health/personaldata/sports/update`,
  DELETE_SPORTDATA: `${API}/patient/health/personaldata/sports/delete`,

  //File upload
  FILE_UPLOAD: `${API}/patient/health/medication/file/upload`,

  FILEALLIGERIES_UPLOAD: `${API}/patient/health/allergies/file/upload`,
  FILEACCIDENT_UPLOAD: `${API}/patient/health/accidents/file/upload`,
  FILEFINDING_UPLOAD: `${API}/patient/health/finding/file/upload`,
  FILELENGTHHOSPTIAL_UPLOAD: `${API}/patient/health/lengthofhospital/file/upload`,
  FILETHERAPHY_UPLOAD: `${API}/patient/health/theraphy/file/upload`,
  FILEIMMUNIZATION_UPLOAD: `${API}/patient/health/immunization/file/upload`,
  FILEMEDITATION_UPLOAD: `${API}/patient/health/medication/file/upload`,
  FILEPERSONAL_UPLOAD: `${API}/patient/personal/data/file/upload`,
  FILECOREDATA_UPLOAD: `${API}/patient/personal/coredata/file/upload`,
  FILEJOBDATA_UPLOAD: `${API}/patient/personal/jobdata/file/upload`,
  FILEEMERGENCY_UPLOAD: `${API}/patient/personal/emergencydata/file/upload`,
  FILEBENIFIT_UPLOAD: `${API}/patient/personal/benifitdata/file/upload`,
  FILESPORTS_UPLOAD: `${API}/patient/personal/sportsdata/file/upload`,
  FILEINSURANCEACCIDENT_UPLOAD: `${API}/patient/personal/insurenceaccident/file/upload`,
  FILEADDITIONALINSURANCE_UPLOAD: `${API}/patient/personal/additionalinsurence/file/upload`,
  FILEHEALTHINSURANCE_UPLOAD: `${API}/patient/personal/healthinsurence/file/upload`,
  FILEMYDOCTOR_UPLOAD: `${API}/patient/personal/mydoctor/file/upload`,
  FILECURRICULAM_UPLOAD: `${API}/patient/personal/curriculamdata/file/upload`,
  FILEPREGNANCIES_UPLOAD: `${API}/patient/personal/pregnanciesdata/file/upload`,

  //pregnancies
  ADD_PRGNANCIESDATA: `${API}/patient/personaldata/pregnancyoverview`,
  GET_UPLOADFILES: `${API}/patient/health/medication/file/get`,
  DELETE_HEALTHREDATA_UPLOADFILES: `${API}/patient/healthdata/file/delete`,
  DELETE_PERSONALDATA_UPLOADFILES: `${API}/patient/personal/file/delete`,
  ADD_PRGNANCIESLIST: `${API}/patient/personaldata/listofpregnacyadd`,
  UPDATE_PRGNANCIESLIST: `${API}/patient/personaldata/listofpregnacyupdate`,
  DELETE_PRGNANCIESLIST: `${API}/patient/personaldata/listofpregnacydelete`,
  //symptom tree
  ADD_OPTION: `${API}/category/information/save`,
  LIST_ALL_ADDED_SYMPTOM_TREE: `${API}/symptoms/id/list`,
  UPDATE_OPTION: `${API}/category/information/edit`,
  GET_ALL_OPTION: `${API}/category/information/list`,
  DELETE_OPTION: `${API}/category/information/delete`,
  GEO_LOCATION: `${API}/savelatlon`,

  //TEXT robot api
  TEXT_ROBOT_LIST: `${API}/textrobot/list`,
  GET_KEY_SEARCH_LIST: `${API}/textrobot/KeySearchList`,
  TEXT_ROBOT_SEARCH: `${API}/textrobot/search`,

  //QR Code
  GENERATE_QRCODE: `${API}/qrcode/create`,
  VALID_QRCODE: `${API}/readqrUid/`,

  //SymptomQustionAndOptions
  UPDATE_SYMPTOM_MAIN: `${API}/symptomschecker/symptomaddupdate`,
  SYMPTOM_MAIN_LIST: `${API}/symptomschecker/symptomsname/list`,
  UPDATE_QUESTION: `${API}/symptomschecker/questionaddupdate`,
  UPDATE_OPTION_SYMPTOM: `${API}/symptomschecker/optionaddupdate`,
  GET_ALL_QUESTION_OPTION: `${API}/symptomschecker/list/symptoms`,
  DELETE_OPTION_SYMPTOM: `${API}/symptomschecker/option/delete`,
  DELETE_QUESTION: `${API}/symptomschecker/question/delete`,

  //hospital 
  HOSPITAL_ADMIN_INFO: `${API}/hospital/list`,
  UPDATE_HOSPITAL_INFO: `${API}/hospital/info/update`,
  PDF_QRCODE: `${API}/readqr/`,
  ADD_HOSPITAL_DOCTOR: `${API}/hospital/add/doctor`,
  GET_HOSPITAL_DOCTPRLIST: `${API}/hospital/doctor/list`,
  UPDATE_HOSPITAL_DOCTOR: `${API}/hospital/update/doctor`,
  ADD_HOSPITAL_PATIENT: `${API}/hospital/add/patient`,
  GET_HOSPITAL_PATIENT_LIST: `${API}/hospital/patient/list`,
  UPDTAE_HOSPITAL_PATIENT: `${API}/hospital/update/patient`,
  LOCATION: `${API}/doctor/information/locationlist`,

  //PROFILE UPLOAD
  UPLOAD_PROFILE_PIC: `${API}/user/profile/upload`,


  // Tomato Drive
  RECEIVER_INBOXLIST: `${API}/sharedata/receive/notification/list`,
  SENDER_INBOXLIST: `${API}/sharedata/send/notification/list`,
  SEND_NOTIFICATION_MESSAGE: `${API}/sharedata/send/notification/message`,

  FILESEND_SHAREDATA: `${API}/sharedata/file/send`,
  EMAILCHECK_SHAREDATA: `${API}/sharedata/email/check`,
  LINKURLGENERATE_SHAREDATA: `${API}/sharedata/link/url/generate`,
  FILELINK_SHAREDATA: `${API}/sharedata/get/files/link/`,
  FILELIST_SHAREDATA: `${API}/sharedata/file/list`,
  FILEDELETE_SHAREDATA: `${API}/sharedata/file/delete`,
  FILEUPLOAD_SHAREDATA: `${API}/sharedata/file/upload`,
  SHAREBY_FILEURL: `${API}/sharedata/file/url/list`,
  SHAREBY_FILEURLDELETE: `${API}/sharedata/file/url/delete`,
  SHAREBY_SENDERLIST: `${API}/sharedata/link/share/sender/list`,
  SHAREBY_RECEIVERLIST: `${API}/sender/receiver/list`,
  SHAREWITH_RECEIVERLIST: `${API}/sharedata/link/share/receiver/list`,
  SHAREWITH_SENDERLIST: `${API}/API/receiver/sender/list`,


  //Logout
  USER_LOGOUT:`${API}/Logout`

};
export default configUrl;
