import {
  Document,
  Font,
  Page,
  StyleSheet,
  Text,
  View
} from "@react-pdf/renderer";
import React from "react";

// Create styles
Font.register({
  family: "Oswald",
  src: "https://fonts.gstatic.com/s/oswald/v13/Y_TKV6o8WovbUd3m_X9aAA.ttf"
});

const styles = StyleSheet.create({
  body: {
    paddingTop: 35,
    paddingBottom: 65,
    paddingLeft: 15,
    paddingRight: 15
  },
  title: {
    fontSize: 17,
    textAlign: "center",
    fontFamily: "Times-Roman"
  },
  author: {
    fontSize: 10,
    textAlign: "center",
    marginBottom: 40
  },
  subtitle: {
    fontSize: 14,
    margin: 12,
    fontFamily: "Times-Roman",
    borderBottom: "2px solid #000000"
  },
  subtitle1: {
    fontSize: 14,
    fontWeight: 600,
    marginLeft: 10,
    marginBottom: 10,
    fontFamily: "Times-Roman"
  },
  text: {
    margin: 12,
    fontSize: 12,
    textAlign: "justify",
    fontFamily: "Times-Roman"
  },
  text1: {
    marginLeft: 25,
    marginBottom: 5,
    fontSize: 10,
    textAlign: "left",
    fontFamily: "Times-Roman"
  },
  image: {
    marginVertical: 10,
    marginHorizontal: 25,
    width: "150px",
    height: "50px",
    margin: "auto"
  },
  header: {
    fontSize: 10,
    marginBottom: 20,
    textAlign: "center",
    color: "grey"
  },

  container: {
    flexDirection: "row",
    marginTop: 20
  },
  innerView: {
    flexDirection: "column",
    width: "33.3%"
  }
});

// Create Document Component
const MyDocument = props => {
  const userDetails = JSON.parse(sessionStorage.getItem("loginDetails"));
  const openReport = JSON.parse(props.value.openReport);

  console.log("this.props.value",props.value)
  return (
    <Document>
      <Page size="A4" orientation="portrait" style={styles.body}>
        {/* <Image style={styles.image} src="/static/images/quijote1.jpg" /> */}
        <Text style={styles.header} fixed>
          Assessment Report
        </Text>
        <Text style={styles.title}>{props.value && props.value.symptoms}</Text>
        <Text style={styles.author}>
          {userDetails && userDetails.userInfo.firstName} Male 1994
        </Text>

        <Text style={styles.subtitle}>Reported symptoms</Text>
        <View style={styles.container}>
          <View style={styles.innerView}>
            <Text style={styles.header}>Symptoms reported as present </Text>
            <Text style={styles.subtitle1}>
              {" "}
              ~ {props.value && props.value.symptoms}
            </Text>
            {openReport.question &&
              openReport.question.map(each => {
                if (
                  each.question &&
                  each.selectedOption &&
                  each.selectedOption != "no"
                ) {
                  return (
                    <Text style={styles.text1}>
                      * {each.question}: {each.selectedOption}
                    </Text>
                  );
                }
              })}

            {/* 
            <Text style={styles.header}>Possible Cause </Text>
             {openReport.question &&
              openReport.question.map((each, i) => {
                if (each.question && each.selectedOption) {
                  return (
                    <Text style={styles.text1}>
                      * {each.possibleCauses}
                    </Text>
                  );
                }
              })}
               */}

            {/* <Text style={styles.text1}>* laterality: unilateral</Text> */}
            {/* <Text style={styles.text1}>* pounding headache: yes</Text>
            <Text style={styles.text1}>* bending forward: exacerbates</Text>
            <Text style={styles.text1}>
              * time since onset: less than one day
            </Text>
            <Text style={styles.text1}>*intensity: mild</Text> */}
            {/* <Text style={styles.subtitle1}> ~ Runny nose</Text>
            <Text style={styles.subtitle1}> ~ Sinus pain</Text>
            <Text style={styles.subtitle1}> ~ Sore throat</Text> */}
          </View>

          <View style={styles.innerView}>
            <Text style={styles.header}>Symptoms reported as absent</Text>
            {openReport.question &&
              openReport.question.map(each => {
                if (
                  each.selectedOption &&
                  each.question &&
                  each.selectedOption == "no"
                )
                  return (
                    <Text style={styles.text1}>
                      {" "}
                      ~ {each.question} : {each.selectedOption}
                    </Text>
                  );
              })}

            {/* <Text style={styles.subtitle1}> ~ Sinus pain</Text>
            <Text style={styles.subtitle1}> ~ Sore throat</Text> */}
          </View>
          <View style={styles.innerView}>
            <Text style={styles.header}>Symptoms reported as unsure of</Text>
            {openReport.question &&
              openReport.question.map(each => {
                if (
                  each.selectedOption &&
                  each.question &&
                  each.selectedOption == "i Dont know"
                )
                  return (
                    <Text style={styles.text1}>
                      {" "}
                      * {each.question} : {each.selectedOption}
                    </Text>
                  );
              })}
          </View>
        </View>
        <View style={styles.container}>
          <View style={styles.innerView}>
            <Text style={styles.header}>Possible Cause</Text>
            {openReport.question &&
              openReport.question.map(each => {
                if (each.possibleCauses)
                  return (
                    <Text style={styles.text1}>* {each.possibleCauses}</Text>
                  );
              })}
          </View>
        </View>
      </Page>
    </Document>
  );
};
export default MyDocument;
