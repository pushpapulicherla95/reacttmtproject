import React, { Component } from "react";
import {
  addHospitaldata,
  deleteHospitaldata,
  editHospitaldata
} from "../../../../service/healthrecord/action";
import { getPatientHealthData ,} from "../../../../service/patient/action";
import { getUploadFiles ,handleHealthDataDelete} from "../../../../service/upload/action";
import { onlyDate } from "../../personal/KeyValidation";
import { connect } from "react-redux";
import axios from "axios";
import URL from "../../../../asset/configUrl";
import { awsFiles } from "../../../../service/common/action";

class HospitalLength extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabone: true,
      tabtwo: false,
      addHealth: false,
      editHealth: false,
      title: "",
      hospitaltype: "",
      description: "",
      dateofadmission: "",
      dateofdischarge: "",
      reasonadmission: "",
      hospitalListData: [],
      uploadPopup: false,
      name: "",
      fileName: "",
      fileType: "",
      fileURL: "",
      editDetails: {},
      errors: {
        title: "",
        description: ""
      },
      titleValid: false,
      descriptionValid: false,
      firstValid: false
    };
  }

  handleAwsFiles = value => {
    awsFiles(value).then(response => {
      window.location.href = response;
    });
  };
  handleChangePopup = e => {
    var reader = new FileReader();
    var file = e.target.files[0];
    let name = file.name.split('.')[0]
    let type = "." + file.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileURL: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(file);
  };
  popupSubmit = () => {
    var phrase = this.state.fileURL;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);
    var { userInfo } = this.props.loginDetails;
    const payload = {
      uid: userInfo.userId,
      z: match[1],
      fileName: this.state.fileName,
      fileType: this.state.fileType,
      name: this.state.name,
      category: "hosptialLength"
    };
    axios
      .post(URL.FILELENGTHHOSPTIAL_UPLOAD, payload)
      .then(response => {
        if (response.data.status === "success") {
          var { userInfo } = this.props.loginDetails;
          const uploadFiles = {
            uid: userInfo.userId,
            category: "hosptialLength"
          };

          this.props.getUploadFiles(uploadFiles);
          this.setState({ uploadPopup: false });
        }
      })
      .catch(error => {
        this.setState({ uploadPopup: true });
      });
  };

  componentWillMount() {
    const { userInfo } = this.props.loginDetails;

    const userinfo = {
      uid: userInfo.userId
    };
    const uploadFiles = {
      uid: userInfo.userId,
      category: "hosptialLength"
    };
    this.props.getUploadFiles(uploadFiles);

    this.props.getPatientHealthData(userinfo);
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.patientHealthList &&
      nextProps.patientHealthList.hospitals != null &&
      nextProps.patientHealthList.hospitals != 0
    ) {
      const hospitalList = nextProps.patientHealthList.hospitals;
      let parsedContent = JSON.parse(hospitalList);
      let hospitalListData = parsedContent.hospitals;

      this.setState({
        hospitalListData
      });
    }
  }
  tabOne = () => {
    this.setState({ tabone: true, tabtwo: false });
  };
  tabTwo = () => {
    this.setState({ tabone: false, tabtwo: true });
  };
  addHealthpop = () => {
    this.setState({ addHealth: true }, () => {
      this.emptystate();
    });
  };
  // editHealthpop = () => {
  //   this.setState({ editmedicine: true }, () => {
  //     this.emptystate();
  //   });
  // };
  modalPopUpClose = () => {
    this.setState({ addHealth: false, editHealth: false }, () => {
      this.emptystate();
    });
  };
  modalPopupOpen = e => {
    this.setState({ editHealth: true, editDetails: e });
    this.setState({
      title: e.title,
      hospitaltype: e.hospitaltype,
      description: e.description,
      dateofadmission: e.dateofadmission,
      dateofdischarge: e.dateofdischarge,
      reasonadmission: e.reasonadmission
    });
  };
  emptystate = () => {
    this.setState({
      title: "",
      hospitaltype: "",
      description: "",
      dateofadmission: "",
      dateofdischarge: "",
      reasonadmission: ""
    });
  };
  handleChange = e => {
    e.preventDefault();
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };
  validateField(fieldName, value) {
    const { errors, titleValid, descriptionValid } = this.state;

    let titlevalid = titleValid;
    let descriptionvalid = descriptionValid;
    let fieldValidationErrors = errors;
    switch (fieldName) {
      case "title":
        titlevalid = !(value.length < 3 || value.length > 20);
        fieldValidationErrors.title = titlevalid
          ? ""
          : "Title should be 3 to 20 characters";
        break;
      case "description":
        descriptionvalid = !(value.length < 3 || value.length > 20);
        fieldValidationErrors.description = descriptionvalid
          ? ""
          : "Description should be 3 to 20 characters";
        break;
    }
    this.setState(
      {
        errors: fieldValidationErrors,
        titleValid: titlevalid,
        descriptionValid: descriptionvalid
      },
      this.validateForm
    );
  }

  validateForm() {
    const { titleValid, descriptionValid } = this.state;
    this.setState({
      firstValid: titleValid && descriptionValid
    });
  }
  updateHospital = () => {
    const { userInfo } = this.props.loginDetails;
    const {
      title,
      hospitaltype,
      description,
      dateofadmission,
      dateofdischarge,
      reasonadmission
    } = this.state;

    const hospitaleditinfo = {
      uid: userInfo.userId,
      hospitals: [
        {
          title: title,
          hospitaltype: hospitaltype,
          description: description,
          dateofadmission: dateofadmission,
          dateofdischarge: dateofdischarge,
          reasonadmission: reasonadmission
        }
      ]
    };
    this.props.editHospitaldata(hospitaleditinfo);

    this.setState({ editHealth: false });
  };

  deleteHospital = e => {
    const { userInfo } = this.props.loginDetails;

    const deleteHospital = {
      uid: userInfo.userId,
      hospitals: [
        {
          title: e.title,
          hospitaltype: e.hospitaltype,
          description: e.description,
          dateofadmission: e.dateofadmission,
          dateofdischarge: e.dateofdischarge,
          reasonadmission: e.reasonadmission
        }
      ]
    };
    this.props.deleteHospitaldata(deleteHospital);
  };
  handleHospital = e => {
    const {
      title,
      hospitaltype,
      description,
      dateofadmission,
      dateofdischarge,
      reasonadmission,
      hospitalListData
    } = this.state;
    const { userInfo } = this.props.loginDetails;

    const addhospitalInfo = {
      uid: userInfo.userId,
      hospitals: [
        {
          title: title,
          hospitaltype: hospitaltype,
          description: description,
          dateofadmission: dateofadmission,
          dateofdischarge: dateofdischarge,
          reasonadmission: reasonadmission
        }
      ]
    };
    this.props.addHospitaldata(addhospitalInfo);
    this.setState({ addHealth: false }, () => {
      this.emptystate();
    });
  };
  render() {
    const {
      tabone,
      tabtwo,
      addHealth,
      title,
      hospitaltype,
      description,
      dateofadmission,
      dateofdischarge,
      reasonadmission,
      hospitalListData,
      editHealth,
      firstValid,
      errors
    } = this.state;
    return (
      <React.Fragment>
        <div className="" id="main-content">
          <div className="wrapper">
            <div className="container-fluid">
              <div className="body-content">
                <div className="tomCard">
                  <div className="tomCardHead">
                    <h5 className="float-left">Hospital Of Length </h5>
                  </div>

                  <div className="tab-menu-content">
                    <div class="tab-menu-title">
                      <ul>
                        <li className={this.state.tabone ? "menu1 active" : ""}>
                          <a href="javascript:void(0)" onClick={this.tabOne}>
                            <p>List of HospitalLength</p>
                          </a>
                        </li>
                        <li className={this.state.tabtwo ? "menu1 active" : ""}>
                          <a href="javascript:void(0)" onClick={this.tabTwo}>
                            <p>Attached Files</p>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>

                  {tabone && (
                    <div className="tab-menu-content-section">
                      <div
                        id="content-1"
                        className={tabone ? "d-block" : "d-none"}
                      >
                        <button
                          type="button"
                          onClick={this.addHealthpop}
                          className="commonBtn2 float-right mb-2"
                        >
                          <i className="fa fa-plus" /> Add New Entry
                         </button>
                        <div className="table-responsive">
                          <table className="table table-hover">
                            <thead className="thead-default">
                              <tr>
                                <th>Title</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {hospitalListData.length > 0 ? (
                                hospitalListData.map((each, i) => (
                                  <tr key={i}>
                                    <td data-label="startDate">{each.title}</td>
                                    <td data-label="Action">
                                      <a
                                        title="Edit"
                                        className="editBtn"
                                        onClick={e => this.modalPopupOpen(each)}
                                      >
                                        <span id={5} className="fa fa-edit" />
                                      </a>
                                      <a
                                        title="Delete"
                                        className="deleteBtn"
                                        onClick={e => this.deleteHospital(each)}
                                      >
                                        <i
                                          className="fa fa-trash"
                                          data-toggle="modal"
                                          data-target="#myModal2"
                                        />
                                      </a>
                                    </td>
                                  </tr>
                                ))
                              ) : (
                                  <tr>No data available</tr>
                                )}
                            </tbody>{" "}
                          </table>
                        </div>
                      </div>
                    </div>
                  )}

                  {tabtwo && (
                    <div
                      id="content-2"
                      className={tabtwo ? "d-block" : "d-none"}
                    >
                      <button
                        type="button"
                        className="commonBtn2 float-right mb-2"
                        onClick={() =>
                          this.setState({
                            uploadPopup: !this.state.uploadPopup
                          })
                        }
                      >
                        <i className="fa fa-plus" /> <span> Upload new files</span>
                      </button>
                      <div className="table-responsive">
                        <table className="table table-hover">
                          <thead className="thead-default">
                            <tr>
                              <th>#</th>
                              <th>Name</th>
                            <th>FileName</th>
                            <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.props.getFilesDetails.fileDetails &&
                              this.props.getFilesDetails.fileDetails.map(
                                (each, i) => (
                                  <tr>
                                    <td data-label="#">{i + 1}</td>
                                    <td data-label="Name">{each.name}</td>
                                    <td data-label="FileName"> {each.fileName}</td>

                                    <td data-label="Action">
                                      <button
                                        class="download_btn"
                                        onClick={() =>
                                          this.handleAwsFiles(each.fileUrl)
                                        }
                                      >
                                        <i class="fas fa-download" /> 
                                      </button> <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.props.handleHealthDataDelete(each.id, each.fileUrl, "hosptialLength")
                                      }
                                    >
                                      <i class="fas fa-trash" />
                                    </button>
                                    </td>
                                  </tr>
                                )
                              )}
                            {/* <tr>
                              <td data-label="#">1</td>
                              <td data-label="Name">Eye Care Test Report</td>
                              <td data-label="Type">png</td>
                              <td data-label="Size">87.9 kB</td>
                              <td data-label="Date">16.05.2019 11:56</td>
                              <td data-label="Action">
                                <a
                                  title="Edit"
                                  className="editBtn"
                                  data-toggle="modal"
                                  data-target="#myModal"
                                >
                                  <i className="fa fa-edit" />
                                </a>
                                <a title="Delete" className="deleteBtn">
                                  <i
                                    className="fa fa-trash"
                                    data-toggle="modal"
                                    data-target="#myModal2"
                                  />
                                </a>
                              </td>
                            </tr> */}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  )}
                </div>

                <div
                  className={addHealth ? "modal d-block" : "modal"}
                  id="myModal"
                >
                  <div class="modal-dialog">
                    <div class="modal-content modalPopUp">
                      <div class="modal-header borderNone">
                        <h5 class="modal-title">Add HospitalLength</h5>
                        <button
                          type="button"
                          className="close"
                          data-dismiss="modal"
                          onClick={this.modalPopUpClose}
                        >
                          <i class="fas fa-times" />
                        </button>
                      </div>
                      <div class="modal-body">
                        <form>
                          <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Title for list entry (e.g. rehab) <sup>*</sup>
                                </label>
                                <input
                                  className="form-control"
                                  name="title"
                                  onChange={this.handleChange}
                                  value={title}
                                  type="text"
                                />
                                <div style={{ color: "red" }}>
                                  {errors.title}
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Type (Hospital/Rehab)
                                </label>
                                {/* <input className="form-control" name="hospitaltype" onChange={this.handleChange} value={hospitaltype} type="text" /><span ></span> */}
                                <select
                                  className="form-control"
                                  name="hospitaltype"
                                  onChange={this.handleChange}
                                >
                                  <option value="Hospital">Hospital</option>
                                  <option value="Rehab">Rehab</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Description <sup>*</sup>
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="description"
                                  onChange={this.handleChange}
                                  type="text"
                                  value={description}
                                />
                                <div style={{ color: "red" }}>
                                  {errors.description}
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Date of Admission
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="dateofadmission"
                                  onChange={this.handleChange}
                                  type="date"
                                  value={dateofadmission}
                                  onKeyPress={onlyDate}
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Date of Discharge
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="dateofdischarge"
                                  onChange={this.handleChange}
                                  type="date"
                                  value={dateofdischarge}
                                  onKeyPress={onlyDate} min={dateofadmission} 
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Reason for Admission
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="reasonadmission"
                                  onChange={this.handleChange}
                                  type="text"
                                  value={reasonadmission}
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Operations :
                                </label>
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <button
                                type="button"
                                class="commonBtn float-right"
                                onClick={this.handleHospital}
                                disabled={!firstValid}
                              >
                                CREATE
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  className={editHealth ? "modal d-block" : "modal"}
                  id="myModal"
                >
                  <div class="modal-dialog">
                    <div class="modal-content modalPopUp">
                      <div class="modal-header borderNone">
                        <h5 class="modal-title">Edit Health</h5>
                        <button
                          type="button"
                          className="close"
                          data-dismiss="modal"
                          onClick={this.modalPopUpClose}
                        >
                          <i class="fas fa-times" />
                        </button>
                      </div>
                      <div class="modal-body">
                        <form>
                          <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Title for list entry (e.g. rehab)
                                </label>
                                <input
                                  className="form-control"
                                  name="title"
                                  onChange={this.handleChange}
                                  defaultValue={this.state.editDetails.title}
                                  type="text"
                                  readOnly
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Type (Hospital/Rehab)
                                </label>
                                {/* <input className="form-control" name="hospitaltype" onChange={this.handleChange} value={hospitaltype} type="text" /><span ></span> */}
                                <select
                                  className="form-control"
                                  name="hospitaltype"
                                  defaultValue={
                                    this.state.editDetails.hospitaltype
                                  }
                                  onChange={this.handleChange}
                                >
                                  <option value="Hospital">Hospital</option>
                                  <option value="Rehab">Rehab</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Description
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="description"
                                  onChange={this.handleChange}
                                  type="text"
                                  defaultValue={
                                    this.state.editDetails.description
                                  }
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Date of Admission
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="dateofadmission"
                                  onChange={this.handleChange}
                                  type="date"
                                  defaultValue={
                                    this.state.editDetails.dateofadmission
                                  }
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Date of Discharge
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="dateofdischarge"
                                  onChange={this.handleChange}
                                  type="date"
                                  defaultValue={
                                    this.state.editDetails.dateofdischarge
                                  }
                                  min={dateofadmission} 
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Reason for Admission
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="reasonadmission"
                                  onChange={this.handleChange}
                                  type="text"
                                  defaultValue={
                                    this.state.editDetails.reasonadmission
                                  }
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Operations :
                                </label>
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <button
                                type="button"
                                class="commonBtn float-right"
                                onClick={this.updateHospital}
                              >
                                Update
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            className={this.state.uploadPopup ? "modal d-block" : "modal"}
            id="myModal"
          >
            <div class="modal-dialog">
              <div class="modal-content modalPopUp">
                <div class="modal-header borderNone">
                  <h5 class="modal-title">Upload File</h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    onClick={() =>
                      this.setState({ uploadPopup: !this.state.uploadPopup })
                    }
                  >
                    <i class="fas fa-times" />
                  </button>
                </div>
                <div class="modal-body">
                  <form>
                    <div class="row">
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="form-group">
                          <label className="form-label">File name</label>
                          <input
                            className="form-control"
                            type="text"
                            name="name"
                            onChange={this.handleChange}
                          />
                          <span />
                        </div>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="fileUp">
                          <label
                            for="fileUp1"
                            class="commonBtn text-center upload"
                          >
                            Upload File
                          </label>
                          <input
                            type="file"
                            id="fileUp1"
                            style={{ display: "none" }}
                            onChange={this.handleChangePopup}
                          />
                          {/* <p style={{ textAlign: "center" }}>hsfkjhsfj</p> */}
                        </div>
                      </div>

                      <div class="col-md-12 col-lg-12 col-sm-12 mt-2">
                        <button
                          type="button"
                          class="commonBtn float-right"
                          onClick={this.popupSubmit} disabled={!this.state.fileURL}
                        >
                          submit
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  patientHealthList: state.patientReducer.patientHealthList,
  loginDetails: state.loginReducer.loginDetails,
  getFilesDetails: state.uploadReducer.getFilesDetails
});
const mapDispatchToProps = dispatch => ({
  addHospitaldata: addhospitalInfo =>
    dispatch(addHospitaldata(addhospitalInfo)),
  getPatientHealthData: userinfo => dispatch(getPatientHealthData(userinfo)),
  deleteHospitaldata: deleteHospital =>
    dispatch(deleteHospitaldata(deleteHospital)),
  editHospitaldata: hospitaleditinfo =>
    dispatch(editHospitaldata(hospitaleditinfo)),
  getUploadFiles: payload => dispatch(getUploadFiles(payload)),
  handleHealthDataDelete: (id, fileUrl, type) => dispatch(handleHealthDataDelete(id, fileUrl, type))

});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HospitalLength);
