const actionType = {
    LOGIN_SUCCESS: "LOGIN_SUCCESS",
    LOGIN_FAILURE: "LOGIN_FAILURE",
    REGISTER_SUCCESS:"REGISTER_SUCCESS",
    REGISTER_FAILURE:"REGISTER_FAILURE",
    FORGOTPASSWORD_SUCCESS:"FORGOTPASSWORD_SUCCESS",
    FORGOTPASSWORD_FAILURE:"FORGOTPASSWORD_FAILURE",
    CHANGEPASSWORD_SUCCESS:"CHANGEPASSWORD_SUCCESS",
    CHANGEPASSWORD_FAILURE:"CHANGEPASSWORD_FAILURE",
    USERCHANGEPASSWORD_SUCCESS:"USERCHANGEPASSWORD_SUCCESS",
    USERCHANGEPASSWORD_FAILURE:"USERCHANGEPASSWORD_FAILURE",
    SHOW_IS_LOADING:"SHOW_IS_LOADING",
    HIDE_IS_LOADING:"HIDE_IS_LOADING",
    HEALTHCARE_REGISTER_SUCCESS : "HEALTHCARE_REGISTER_SUCCESS",
    HEALTHCARE_REGISTER_FAILURE : "HEALTHCARE_REGISTER_FAILURE",
    GET_USERTYPE_SUCCESS:"GET_USERTYPE_SUCCESS",
    GET_USERTYPE_FAILURE:"GET_USERTYPE_FAILURE",
    REGISTER_MAIL_SUCCESS:"REGISTER_MAIL_SUCCESS",
    REGISTER_MAIL_FAILURE:"REGISTER_MAIL_FAILURE",
    ADMIN_LOGIN_SUCCESS:"ADMIN_LOGIN_SUCCESS",
    ADMIN_LOGIN_FAILURE:"ADMIN_LOGIN_FAILURE",
    HOSPITAL_REGISTER_SUCCESS:"HOSPITAL_REGISTER_SUCCESS",
    HOSPITAL_REGISTER_FAILURE:"HOSPITAL_REGISTER_FAILURE"


}

export default actionType;