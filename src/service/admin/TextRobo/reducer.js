import actionType from "../TextRobo/actionType";

const initialState = {
    symptomsList: "",
    searchSymptomsResult: "",
    symptomMainListData : null
}
const textRoboReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.ADD_SYMPTOMS_SUCCESS:
            return {
                ...state
            }
        case actionType.ADD_SYMPTOMS_FAILURE:
            return {
                ...state
            }
        case actionType.LIST_SYMPTOMS_SUCCESS:
            return {
                ...state,
                symptomsList: action.payload
            }
        case actionType.LIST_SYMPTOMS_FAILURE:
            return {
                ...state,
                symptomsList: action.error
            }
        case actionType.SEARCH_SYMPTOMS_SUCCESS:
            return {
                ...state,
                searchSymptomsResult: action.payload
            }
        case actionType.SEARCH_SYMPTOMS_FAILURE:
            return {
                ...state,
                searchSymptomsResult: action.error
            }
        case actionType.SYMPTOM_MAIN_LIST_SUCESS:
            return {
                ...state,
                symptomMainListData: action.payload
            }
        case actionType.QUESTION_AND_OPTION_LIST_SUCCESS:
            return {
                ...state,
                questionAndOptionData: action.payload
            }    
        default: return state;
    }

}
export default textRoboReducer;