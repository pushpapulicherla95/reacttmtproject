import React, { Component } from 'react';
import axios from 'axios';

 class EmployeeCreate extends Component {
  constructor(props) {
    super(props);
    this.onChangeFirstName = this.onChangeFirstName.bind(this);
    this.onChangeLastName = this.onChangeLastName.bind(this);
    this.onChangeMobileNumber = this.onChangeMobileNumber.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);

    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      first_name: '',
      last_name: '',
      mobile_no:'',
      email:''
    }
  }
  onChangeFirstName(e) {
    this.setState({
        first_name: e.target.value
    });
  }
  onChangeLastName(e) {
    this.setState({
        last_name: e.target.value
    })  
  }
  onChangeMobileNumber(e) {
    this.setState({
        mobile_no: e.target.value
    })
  }
  onChangeEmail(e){
    this.setState({
    email: e.target.value
    })
  }

  onSubmit(e) {
    e.preventDefault();
    const obj = {
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        mobile_no: this.state.mobile_no,
        email: this.state.email

    };
    axios.post('http://localhost:4000/API/employee/create', obj)
        .then(res => console.log(res.data));
    
    this.setState({
        first_name: '',
        last_name: '',
        mobile_no: '',
        email:''
    })
  }
 
  render() {
    return (
        <div style={{ marginTop: 10 }}>
            <h3>Add New Employee</h3>
            <form onSubmit={this.onSubmit}>
                <div className="form-group">
                    <label>First Name:  </label>
                    <input 
                      type="text" 
                      className="form-control" 
                      value={this.state.first_name}
                      onChange={this.onChangeFirstName}
                      />
                </div>
                <div className="form-group">
                    <label>Last Name: </label>
                    <input type="text" 
                      className="form-control"
                      value={this.state.last_name}
                      onChange={this.onChangeLastName}
                      />
                </div>
                <div className="form-group">
                    <label>Mobile Number: </label>
                    <input type="text" 
                      className="form-control"
                      value={this.state.mobile_no}
                      onChange={this.onChangeMobileNumber}
                      />
                </div>
                <div className="form-group">
                    <label>Email: </label>
                    <input type="text" 
                      className="form-control"
                      value={this.state.email}
                      onChange={this.onChangeEmail}
                      />
                </div>
                <div className="form-group">
                    <input type="submit" value="Register Business" className="btn btn-primary"/>
                </div>
            </form>
        </div>
    )
  }
}
export default EmployeeCreate;