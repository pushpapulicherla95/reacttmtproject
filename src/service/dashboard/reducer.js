import actionType from '../../service/dashboard/actionType';

const initialState = {
    viewprofileDetails: "",
    editprofileDetails:"",
}
const dashboardReducer = (state = initialState, action) => {

    switch (action.type) {
        case actionType.GETUSERPROFILE_SUCCESS:
            return {
                ...state,
                viewprofileDetails: action.payload,
            }
        case actionType.GETUSERPROFILE_FAILURE:
            return {
                ...state,
                viewprofileDetails: action.error
            }
            case actionType.EDITUSERPROFILE_SUCCESS:
            return {
                ...state,
                editprofileDetails: action.payload,
            }
        case actionType.EDITUSERPROFILE_FAILURE:
            return {
                ...state,
                editprofileDetails: action.error
            }
            default:
            return state;
        }
    }
    export default dashboardReducer;

            