import actionType from '../../service/jitsi/actionType';

const initialState = {
    jitsiDetails: ""
}

const jitsiReducer = (state = initialState, action) => {

    switch (action.type) {
        case actionType.JITSI_SUCCESS:
            return {
                ...state,
                jitsiDetails: action.payload,
            }
        case actionType.JITSI_FAILURE:
            return {
                ...state,
                jitsiDetails: action.error
            }
        default:
            return state;
    }

}


  
    


export default jitsiReducer;
 