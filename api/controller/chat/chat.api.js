// api to chat save  - Calling from socket
const chatSave = async (req, res) => {
  let chatResponse = new Object();
  try {
    const [rows, fields] = await pool.execute(
      "INSERT INTO video_chat_info(patient_id,doctor_id,sent_by,message,is_file,meeting_room) values(?,?,?,?,?,?)",
      [
        req.patientId,
        req.doctorId,
        req.sentBy,
        req.message,
        req.isfile,
        req.meetingRoom
      ]
    ); //, function (error, results, fields) {
    chatResponse = {
      status: "success",
      message: "chat saved successfully"
    };
    //console.log('Message %s sent: %s', info.messageId, info.response);
    console.log("chatSave : chatResponse : ", chatResponse);
    console.log(
      "JSON.stringify(chatResponse) : ",
      JSON.stringify(chatResponse)
    );
    return JSON.stringify(chatResponse);
  } catch (error) {
    console.log("chat save error", error.message);
    chatResponse = {
      status: "failure",
      message: error.message
    };
    //console.log('Message %s sent: %s', info.messageId, info.response);
    console.log("chatSave : chatResponse : ", chatResponse);
    console.log("JSON.stringify(chatSave) : ", JSON.stringify(chatResponse));
    return JSON.stringify(chatResponse);
  }
};

// api to get chat history  - calling from react as API
const chatHistory = async (req, res) => {
  console.log("chatHistory : ", req.body);
  let chatResponse = new Object();
  try {
    const [rows, fields] = await pool.execute(
      "select * from video_chat_info where meeting_room=?",
      [req.body.meetingRoom]
    );
    var history = [];
    if (rows.length > 0) {
      for (i = 0; i < rows.length; i++) {
        var userId, name, isfile;
        if (rows[i].sent_by === "doctor") {
          userId = rows[i].doctor_id;
        } else {
          userId = rows[i].patient_id;
        }
        const [prows, fields] = await pool.execute(
          "select first_name from user_info where id=?",
          [userId]
        );
        if (prows.length > 0) {
          name = prows[0].first_name;
          //console.log('chat name : ', name);
        }
        if (rows[i].is_file === 0) {
          isfile = false;
        } else {
          isfile = true;
        }
        history.push({
          msg: rows[i].message,
          room: rows[i].meeeting_room,
          type: rows[i].sent_by,
          name: name,
          patientId: rows[i].patient_id,
          doctorId: rows[i].doctor_id,
          isfile: isfile,
          fileUrl: rows[i].file_url
        });
      }
    }
    chatResponse = {
      status: "success",
      message: "chat retrieved successfully",
      chatHistory: history
    };
    //console.log('Message %s sent: %s', info.messageId, info.response);
    console.log("chatHistory : history : ", history.length);
    //console.log('JSON.stringify(chatResponse) : ', JSON.stringify(chatResponse));
    return res.end(JSON.stringify(chatResponse));
  } catch (error) {
    console.log("chatHistory error", error.message);
    chatResponse = {
      status: "failure",
      message: error.message,
      chatHistory: []
    };
    //console.log('Message %s sent: %s', info.messageId, info.response);
    console.log("chatHistory : chatResponse : ", chatResponse);
    console.log("JSON.stringify(chatHistory) : ", JSON.stringify(chatResponse));
    return res.end(JSON.stringify(chatResponse));
  }
};

module.exports = {
  chatSave,
  chatHistory
};
