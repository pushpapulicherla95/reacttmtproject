import React, { Component } from "react";
import { addHospitalDoctor, getHospitalDoctorList, updateHospitalDoctor } from "../../service/hospital/action";
import { connect } from "react-redux";
import { getClinic } from "../../service/medicalSpecialist/action";
import { approveDoctor } from "../../service/admin/Patient/action";
import { onlyNumbers } from "../patient/personal/KeyValidation";
class DoctorList extends Component {
    constructor(props) {
        super(props);
        var loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
        const { userInfo } = loginDetails;
        var userId = userInfo.userId || "";
        this.state = {
            docTab: "List",
            qualification: "",
            specialistId: "",
            mobileNo: "",
            experience: "",
            address: "",
            description: "",
            gender: "",
            email: "",
            firstName: "",
            lastName: "",
            password: "",
            userId: userId,
            specialistList: [],
            doctorList: [],
            totalDoctorList: [],
            userInfoId: "",
            updateDoc: false,

            errors: {
                qualification: "",
                specialistId: "",
                mobileNo: "",
                experience: "",
                address: "",
                description: "",
                gender: "",
                email: "",
                firstName: "",
                lastName: "",
                password: "",

            },
            firstNameValid: false,
            lastNameValid: false,
            passwordValid: false,
            mobileNoValid: false,
            experienceValid: false,
            addressValid: false,
            descriptionValid: false,
            genderValid: false,
            emailValid: false,
            specialistIdValid: false,
            qualificationValid: false,


        }
    }
    validateField(fieldName, value) {
        const { errors,
            qualificationValid,
            specialistIdValid,
            emailValid,
            mobileNoValid,
            addressValid,
            experienceValid,
            genderValid,
            descriptionValid,
            firstNameValid,
            lastNameValid,
            passwordValid,

        } = this.state;
        let emailvalid = emailValid;
        let firstNamevalid = firstNameValid;
        let lastNamevalid = lastNameValid;
        let mobileNovalid = mobileNoValid;
        let addressvalid = addressValid;
        let experiencevalid = experienceValid;
        let gendervalid = genderValid;
        let descriptionvalid = descriptionValid;
        let specialistIdvalid = specialistIdValid;
        let qualificationvalid = qualificationValid;
        let passwordvalid = passwordValid;
        let fieldValidationErrors = errors;
        switch (fieldName) {
            case "email":
                emailvalid = value.match(
                    /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i
                );
                fieldValidationErrors.email = emailvalid
                    ? ""
                    : "Please enter valid email address.";
                break;
            case "firstName":
                firstNamevalid = !(value.length < 3 || value.length > 20);
                fieldValidationErrors.firstName = firstNamevalid
                    ? ""
                    : "First Name should be 3 to 20 characters";
                break;
            case "lastName":
                lastNamevalid = !(value.length < 3 || value.length > 20);
                fieldValidationErrors.lastName = lastNamevalid
                    ? ""
                    : "Last Name should be 3 to 20 characters";
                break;
            case "password":
                passwordvalid = !(value.length < 3 || value.length > 20);
                fieldValidationErrors.password = passwordvalid
                    ? ""
                    : "Last Name should be 3 to 20 characters";
                break;
            case "qualification":
                qualificationvalid = !(value.length < 1 || value.length > 300);
                fieldValidationErrors.qualification = qualificationvalid
                    ? ""
                    : "Qualification  should be 1 to 300 characters";
                break;
            case "specialistId":
                specialistIdvalid = !(value.length < 1);
                fieldValidationErrors.specialistId = specialistIdvalid
                    ? ""
                    : "Please select the Specialist";
                break;
            case "mobileNo":
                mobileNovalid = !(value.length < 1);
                fieldValidationErrors.mobileNo = mobileNovalid
                    ? ""
                    : "Please enter phone number";
                break;
            case "experience":
                experiencevalid = !(value.length < 1 || value.length > 300);
                fieldValidationErrors.experience = experiencevalid
                    ? ""
                    : "Please enter your Exprience";
                break;

            case "address":
                addressvalid = !(value.length < 1 || value.length > 300);
                fieldValidationErrors.address = addressvalid
                    ? ""
                    : "Please enter your Address";
                break;
            case "gender":
                gendervalid = !(value.length < 1);
                fieldValidationErrors.gender = gendervalid
                    ? ""
                    : "Please select Gender";
                break;
            case "description":
                descriptionvalid = !(value.length < 3 || value.length > 300);
                fieldValidationErrors.description = descriptionvalid
                    ? ""
                    : "Description should be 3 characters  ";
                break;



        }

        this.setState({
            errors: fieldValidationErrors,
            emailValid: emailvalid,
            firstNameValid: firstNamevalid,
            lastNameValid: lastNamevalid,
            mobileNoValid: mobileNovalid,
            addressValid: addressvalid,
            experienceValid: experiencevalid,
            genderValid: gendervalid,
            descriptionValid: descriptionvalid,
            specialistIdValid: specialistIdvalid,
            qualificationValid: qualificationvalid,
            passwordValid: passwordvalid
        },
            this.validateForm
        )
    }

    validateForm() {
        const {
            qualificationValid,
            specialistIdValid,
            emailValid,
            mobileNoValid,
            addressValid,
            experienceValid,
            genderValid,
            descriptionValid,
            firstNameValid,
            lastNameValid,
            passwordValid,
        } = this.state;
        this.setState({
            formValid:
                qualificationValid &&
                specialistIdValid &&
                emailValid &&
                mobileNoValid &&
                addressValid &&
                experienceValid &&
                genderValid &&
                descriptionValid &&
                firstNameValid &&
                lastNameValid &&
                passwordValid,
        }, () => {
            let {

                qualificationValid,
                specialistIdValid,
                emailValid,
                mobileNoValid,
                addressValid,
                experienceValid,
                genderValid,
                descriptionValid,
                firstNameValid,
                lastNameValid,
                passwordValid,
            } = this.state;


            let validatingData = {
                qualificationValid,
                specialistIdValid,
                emailValid,
                mobileNoValid,
                addressValid,
                experienceValid,
                genderValid,
                descriptionValid,
                firstNameValid,
                lastNameValid,
                passwordValid,
            }
        }
        );

    }
    componentWillMount() {
        this.props.getClinic();
        const info = {
            uid: this.state.userId
        }
        this.props.getHospitalDoctorList(info)
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.clinicDetails && nextProps.clinicDetails.therapists.length > 0) {
            this.setState({ specialistList: nextProps.clinicDetails.therapists })

        }
        if (nextProps.hospitalDoctorList && nextProps.hospitalDoctorList.hospitalDoctorList) {
            this.setState({ doctorList: nextProps.hospitalDoctorList.hospitalDoctorList, totalDoctorList: nextProps.hospitalDoctorList.hospitalDoctorList })

        }
    }
    handleMenu(name) {
        this.setState({ docTab: name, updateDoc: false }, this.emptyStateValues);

    }
    handleChange = (e) => {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value }, () => {
            this.validateField(name, value);
        });

    };
    handleSubmit = (e) => {
        const { firstName, lastName, email, password, mobileNo, address, description, experience, gender, qualification,
            specialistId, userId, userInfoId } = this.state;

        const savedoc = {
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password,
            mobileNo: mobileNo,
            address: address,
            description: description,
            experience: experience,
            gender: gender,
            qualification: qualification,
            specialistId: specialistId,
            userTypeId: 2,
            hospitalId: userId,
        }
        const updateInfo = {
            uid: userInfoId,
            firstName: firstName,
            lastName: lastName,
            email: email,
            mobileNo: mobileNo,
            address: address,
            description: description,
            experience: experience,
            gender: gender,
            qualification: qualification,
            specialistId: specialistId,
            userTypeId: 2,
            hospitalId: userId,
        }
        if (this.state.updateDoc) {
            this.props.updateHospitalDoctor(updateInfo).then(response => {
                if (response == "success") {
                    this.setState({ updateDoc: false, docTab: "List" })
                }
            }).catch(err => {

            })
            this.setState({ updateDoc: false, docTab: "List" })
        } else {
            this.props.addHospitalDoctor(savedoc).then(res => {
                if (res == "success") {
                    this.setState({ updateDoc: false, docTab: "List" })
                }
            }).catch(err => {

            });
        }
        //    
    }
    emptyStateValues = () => {

        this.setState({
            mobileNo: "",
            address: "",
            description: "",
            experience: "",
            gender: "",
            qualification: "",
            specialistId: "",
            firstName: "",
            lastName: "",
            email: "",
            password: "",
            userInfoId: "",
            updateDoc: false
        })
    }

    viewDoctor = (list) => {
        const { phone, password, address, description, experience, gender, qualification, specialListId, firstName, lastName, email, userInfoId } = list;
        this.setState({
            mobileNo: phone,
            address: address,
            description: description,
            experience: experience,
            gender: gender,
            qualification: qualification,
            specialistId: specialListId,
            firstName: firstName,
            lastName: lastName,
            email: email,
            docTab: "Add",
            password: password,
            userInfoId: userInfoId,
            updateDoc: true
        })

    }
    handleDoctorApproval = (e, approvedStatus) => {
        const status = {
            uid: e.userInfoId,
            approval: approvedStatus,
            hospitalId: this.state.userId
        }
        this.props.approveDoctor(status);
    }
    searchPatient = (e) => {
        e.preventDefault();
        const { doctorList, totalDoctorList } = this.state;
        const childArrays = totalDoctorList.filter((data) => {
            return data.firstName.includes(e.target.value) || data.phone.includes(e.target.value)
        })
        this.setState({ doctorList: childArrays })
    }
    render() {
        const { docTab, firstName, lastName, email, password, mobileNo, address, description, experience, gender, qualification, errors,
            formValid, specialistId, specialistList, doctorList } = this.state;

        return (
            <div className="mainMenuSide" id="main-content">
                <div className="wrapper">
                    <div className="container-fluid">
                        <div className="body-content">
                            <div className="newAdmin">
                                <div className="row mt-2">
                                    <div className="col-12">
                                        {docTab == "Add" && <button className="newcommonBtn1" onClick={e => this.handleMenu("List")} ><i className="fas fa-list"></i> Doctors List</button>}
                                        {docTab == "List" && <button className="newcommonBtn1" onClick={e => this.handleMenu("Add")} ><i className="fas fa-plus"></i>  Add Doctors</button>}
                                       {docTab == "List" && <div className="searchInput_new"><input type="text" name="seachText" onChange={this.searchPatient} placeholder="Search by name,phone" /><i class="fas fa-search icn_sea" ></i></div>}

                                    </div>

                                </div>
                                {docTab == "Add" && <div className="row mt-2" id="contant-1" >
                                    <div className="col-12">
                                        <div className="admin_pro">
                                            <form>
                                                <div className="row">
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Firstname</label>
                                                            <input type="text" className="form-control input_h_35 boxnone" name="firstName" value={firstName} onChange={this.handleChange} readOnly={this.state.updateDoc ? "readonly" : ""} />
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.firstName}</div>

                                                    </div>
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Lastname</label>
                                                            <input type="text" className="form-control input_h_35 boxnone" name="lastName" value={lastName} onChange={this.handleChange} readOnly={this.state.updateDoc ? "readonly" : ""} />
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.lastName}</div>
                                                    </div>

                                                </div>
                                                <div className="row">
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Email</label>
                                                            <input type="text" className="form-control input_h_35 boxnone" name="email" value={email} onChange={this.handleChange} readOnly={this.state.updateDoc ? "readonly" : ""} />
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.email}</div>
                                                    </div>
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Password</label>
                                                            <input type="password" className="form-control input_h_35 boxnone" name="password" value={password} readOnly={this.state.updateDoc ? "readonly" : ""} onChange={this.handleChange} />
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.password}</div>

                                                    </div>
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Mobile</label>

                                                            <input type="text" className="form-control input_h_35 boxnone" name="mobileNo" value={mobileNo} onChange={this.handleChange} onKeyPress={onlyNumbers} />
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.mobileNo}</div>

                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Gender</label>
                                                            <select className="form-control input_h_35 boxnone" name="gender" value={gender} onChange={this.handleChange}>
                                                                <option value="">--select gender--</option>
                                                                <option value="Male">Male</option>
                                                                <option value="Female">Female</option>
                                                            </select>
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.gender}</div>

                                                    </div>
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Education</label>
                                                            <input type="text" className="form-control input_h_35 boxnone" name="qualification" value={qualification} onChange={this.handleChange} />
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.qualification}</div>

                                                    </div>
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Specialization</label>

                                                            <select className="form-control" name='specialistId' value={specialistId} onChange={this.handleChange}>
                                                                <option value="">--select specialist--</option>
                                                                {specialistList.length > 0 && specialistList.map((link, i) =>
                                                                    <option key={i} value={link.id} >{link.name}</option>
                                                                )
                                                                }
                                                            </select>
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.specialistId}</div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Address</label>
                                                            <textarea className="form-control boxnone" name="address" value={address} onChange={this.handleChange}></textarea>
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.address}</div>
                                                    </div>
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">About Doctor</label>
                                                            <textarea className="form-control boxnone" name="description" value={description} onChange={this.handleChange}></textarea>
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.description}</div>
                                                    </div>
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Experience</label>
                                                            <input type="text" className="form-control input_h_35 boxnone" name="experience" value={experience} onChange={this.handleChange} />
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.experience}</div>
                                                    </div>
                                                    {/* <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel d-block">Picture Upload</label>
                                                            <input type="file" className="input_h_35 boxnone" />

                                                        </div>
                                                    </div> */}
                                                </div>
                                                <div className="row">
                                                    <div className="col-12 text-center">
                                                        {!this.state.updateDoc && <button type="button" className="newBtnsave" onClick={this.handleSubmit} disabled={!formValid}> Save</button>}
                                                        {this.state.updateDoc && <button type="button" className="newBtnsave" onClick={this.handleSubmit}  > Update</button>}
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>}
                                {docTab == "List" && <div className="row" id="contant-2">
                                    <div className="search-result-section">
                                        {doctorList.length > 0 ? doctorList.map((value, i) =>
                                            <div className="search-card2" key={i}>
                                                <img src="https://images.pexels.com/photos/1239291/pexels-photo-1239291.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=650&w=940" />
                                                <div className="doc-title">
                                                    <p className="title-name"> {value.firstName}</p>
                                                    <p> {value.phone}</p>

                                                </div>
                                                <div className="patientView">
                                                    <button onClick={e => this.viewDoctor(value)}><i className="far fa-eye"></i></button>
                                                    {value.status == "approved" && <button className="RejectBtn" onClick={(e) => this.handleDoctorApproval(value, "pending")}><i className="fas fa-times"></i>
                                                    </button>}
                                                    {value.status == "pending" && <button className="approveBtn" onClick={(e) => this.handleDoctorApproval(value, "approved")}><i className="fas fa-check"></i>
                                                    </button>}
                                                </div>
                                            </div>
                                        ) : <div className="doc-title"><p> No DoctorList </p></div>
                                        }

                                    </div>
                                </div>}

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails,
    clinicDetails: state.medicalSpecialistReducer.clinicDetails,
    hospitalDoctorList: state.hospitalReducer.hospitalDoctorList
})
const mapDispatchToprops = dispatch => ({
    addHospitalDoctor: (savedoc) => dispatch(addHospitalDoctor(savedoc)),
    getClinic: () => dispatch(getClinic()),
    getHospitalDoctorList: (info) => dispatch(getHospitalDoctorList(info)),
    updateHospitalDoctor: (updateInfo) => dispatch(updateHospitalDoctor(updateInfo)),
    approveDoctor: (status) => dispatch(approveDoctor(status))
})
export default connect(
    mapStateToProps,
    mapDispatchToprops
)(DoctorList);


