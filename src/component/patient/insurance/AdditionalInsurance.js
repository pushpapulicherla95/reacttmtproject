import React from "react";
import {
  additionalinsurance,
  getAllinsurance
} from "../../../service/insurance/action";
import { getUploadFiles, handleFileDelete } from "../../../service/upload/action";
import { connect } from "react-redux";
import axios from "axios";
import { uniqBy as _uniqBy } from "lodash";
import { onlyNumbers } from "../personal/KeyValidation";
import URL from "../../../asset/configUrl";
import { awsPersonalFiles } from "../../../service/common/action";

import { toastr } from "react-redux-toastr";
let coverage = [
  { name: "Suite for 1 Patient", id: "checkbox", value: "suitefor1Patient", props: false },
  { name: "Suite for 2 Patient", id: "checkbox", value: "suitefor2Patient", props: false },
  { name: "Chief Medical Doctor", id: "checkbox", value: "chiefMedicalDoctor", props: false },
  { name: "Dental", id: "checkbox", value: "dental", props: false },
  { name: " In-Patient", id: "checkbox", value: "inpatient", props: false }
];
class InsuranceAccident extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      address: "",
      phone: "",
      coverage: [],
      dentalprostheses: "",
      tab: true,
      checked: [],
      uploadPopup: false,
      name: "",
      fileName: "",
      fileType: "",
      fileURL: "",
      index: 1,
      tabone: true,
      tabtwo: false,
      formValid: false
    };
  }

  handleAwsFiles = value => {
    window.open("https://data.tomatomedical.com/patientpersonal/" + value, '_blank');

  };
  handleChangePopup = e => {
    var reader = new FileReader();
    var file = e.target.files[0];
    let name = file.name.split('.')[0]
    let type = "." + file.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileURL: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(file);
  };
  popupSubmit = () => {
    var phrase = this.state.fileURL;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);
    var { userInfo } = this.props.loginDetails;
    const payload = {
      uid: userInfo.userId,
      z: match[1],
      fileName: this.state.fileName,
      fileType: this.state.fileType,
      name: this.state.name,
      category: "additionalinsurance"
    };
    this.setState({ uploadPopup: false });

    axios
      .post(URL.FILEADDITIONALINSURANCE_UPLOAD, payload)
      .then(response => {
        response.data.status === "success"
          ? toastr.success("Message", response.data.message)
          : toastr.error("Message", response.data.message);
        if (response.data.status === "success") {
          var { userInfo } = this.props.loginDetails;
          const uploadFiles = {
            uid: userInfo.userId,
            category: "additionalinsurance"
          };
          this.props.getUploadFiles(uploadFiles);

          this.setState({
            name: "",
            fileName: "",
            fileType: "",
            fileURL: ""
          });
          this.setState({ uploadPopup: false });
        }
      })
      .catch(error => {
        // this.setState({ uploadPopup: true });
      });
  };

  componentDidMount = () => {
    const { userInfo } = this.props.loginDetails;
    this.props.getAllinsurance(userInfo.userId);
    const uploadFiles = {
      uid: userInfo.userId,
      category: "additionalinsurance"
    };
    this.props.getUploadFiles(uploadFiles);
  };

  componentWillReceiveProps(nextProps) {
    const data = nextProps.additionalInsurance;
    data &&
      this.setState({
        name: data.name,
        address: data.address,
        phone: data.phone,
        dentalprostheses: data.dentalProstheses,
        coverage: this.state.coverage.concat(data.coverage)
      });
  }
  handleTab = () => {
    this.setState({ tab: !this.state.tab }, () => {
      const { userInfo } = this.props.loginDetails;
      this.props.getAllinsurance(userInfo.userId);
    });
  };
  ///
  ValidationField = () => {
    const {
      name,
      address,
      phone,
      coverage,
      dentalProstheses } = this.state
    this.setState({
      formValid:
        name




    });

  }
  handleChange = (e) => {

    let check = e.target.checked;

    this.setState({ [e.target.name]: e.target.value }, () => this.ValidationField());
    if (check) {
      this.state.coverage.push(e.target.name);
    } else {
      this.state.coverage.splice(this.state.coverage.indexOf(e.target.name), 1);
    }
  };

  handleSubmit = e => {
    e.preventDefault();
    const { userInfo } = this.props.loginDetails;

    const payload = {
      userId: userInfo.userId,
      additionalInsurance: {
        name: this.state.name,
        address: this.state.address,
        phone: this.state.phone,
        coverage: this.state.coverage,
        dentalProstheses: this.state.dentalprostheses
      }
    };
    this.props.additionalinsurance(payload);
  };
  tabOne = () => {
    this.setState({ tabone: true, tabtwo: false, formValid: false });
  };
  tabTwo = () => {
    this.setState({ tabone: false, tabtwo: true });
  };
  render() {
    const { tabone, tabtwo, formValid } = this.state;
    const Selectedcoverage = _uniqBy(this.state.coverage, function (e) {
      return e;
    });
    return (
      <div className="" id="main-content">
        <div className="wrapper">
          <div className="container-fluid">
            <div className="body-content">
              <div className="tomCard">
                <div className="tomCardHead">
                  <h5 class="float-left">Insurances</h5>
                </div>

                <div class="tab-menu-content">
                  <div class="tab-menu-title">
                    <ul>
                      <li class="menu1 active" onClick={this.tabOne}>
                        <a href="#">
                          <p>Additional Insurance</p>
                        </a>
                      </li>

                      <li class="menu2" onClick={this.tabTwo}>
                        <a href="#">
                          <p>Attached Files</p>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                {tabone && (
                  <div className="tab-menu-content-section">
                    <div
                      id="content-1"
                      className={tabone ? "d-block" : "d-none"}
                    >
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Name <sup>*</sup></label>
                            <input
                              type="text"
                              class="form-control"
                              name="name"
                              value={this.state.name}
                              onChange={this.handleChange}
                            />
                          </div>

                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Phone</label>
                            <input
                              type="text"
                              class="form-control"
                              name="phone"
                              value={this.state.phone}
                              onChange={this.handleChange} onKeyPress={onlyNumbers} maxLength="15"
                            />
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Coverage</label>
                            <div class="business_checkbox">
                              <ul class="checkbox_list_box">
                                {coverage.map((each, i) => {


                                  console.log("dfhdjh+dkjkdf", Selectedcoverage.filter((e, i) => {
                                    return each.value == e
                                  }))

                                  return (<li>
                                    <p>
                                      <input
                                        type="checkbox"
                                        id={each.id + i}
                                        name={each.value}
                                        onChange={(e) => { this.handleChange(e) }}
                                        checked={
                                          Selectedcoverage.filter((e, i) => {
                                            return each.value == e
                                          }).join("") == each.value ? true : false

                                        }
                                      />
                                      <label
                                        htmlFor={each.id + i}
                                      >
                                        {each.name}
                                      </label>
                                    </p>
                                  </li>
                                  )
                                })}

                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Dental Prostheses</label>
                            <input
                              type="text"
                              class="form-control"
                              name="dentalprostheses"
                              value={this.state.dentalprostheses}
                              onChange={this.handleChange}
                            />
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-12">
                          <button
                            type="button"
                            class="commonBtn float-right"
                            onClick={this.handleSubmit} disabled={!formValid}
                          >
                            Save
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                {tabtwo && (
                  <div id="content-1" className={tabtwo ? "d-block" : "d-none"}>
                    <button
                      type="button"
                      class="commonBtn2 float-right mb-2"
                      id="upBtn"
                      onClick={() =>
                        this.setState({
                          uploadPopup: !this.state.uploadPopup
                        })
                      }
                    >

                      <i className="fa fa-plus" />
                      <span> Upload new files</span></button>
                    <input
                      type="file"
                      id="file1"
                      name="file1"
                      style={{ display: "none" }}
                    />
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead class="thead-default">
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>FileName</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.props.getFilesDetails.fileDetails &&
                            this.props.getFilesDetails.fileDetails.map(
                              (each, i) => (
                                <tr>
                                  <td data-label="#">{i + 1}</td>
                                  <td data-label="Name"> {each.name}</td>
                                  <td data-label="FileName"> {each.fileName}</td>

                                  <td data-label="Action">
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.handleAwsFiles(each.fileUrl)
                                      }
                                    >
                                      <i class="fas fa-download" />
                                    </button> <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.props.handleFileDelete(each.id, each.fileUrl, "additionalinsurance")
                                      }
                                    >
                                      <i class="fas fa-trash" />
                                    </button>
                                  </td>
                                </tr>
                              )
                            )}

                        </tbody>
                      </table>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>

        {
          <div
            className={this.state.uploadPopup ? "modal d-block" : "modal"}
            id="myModal4"
          >
            <div class="modal-dialog">
              <div class="modal-content modalPopUp">
                <div class="modal-header borderNone">
                  <h5 class="modal-title">Upload File</h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    onClick={() =>
                      this.setState({
                        uploadPopup: !this.state.uploadPopup
                      })
                    }
                  >
                    <i class="fas fa-times" />
                  </button>
                </div>
                <div class="modal-body">
                  <form>
                    <div class="row">
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="form-group">
                          <label className="form-label">File Name</label>
                          <input
                            name="name"
                            className="form-control"
                            type="text"
                            id="name"
                            value={this.state.name}
                            onChange={this.handleChange}
                          />
                          <span />
                        </div>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="fileUp">
                          <label
                            for="fileUp1"
                            class="commonBtn text-center upload"
                          >
                            Upload File
                          </label>
                          <input
                            type="file"
                            id="fileUp1"
                            onChange={this.handleChangePopup}
                            style={{ display: "none" }}
                          />
                          {/* <p style={{ textAlign: "center" }}>hsfkjhsfj</p> */}
                        </div>
                      </div>

                      <div class="col-md-12 col-lg-12 col-sm-12 mt-2">
                        <button
                          type="button"
                          class="commonBtn float-right"
                          onClick={this.popupSubmit} disabled={!this.state.fileURL}
                        >
                          Submit
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}
const mapStateToProps = state => ({
  loginDetails: state.loginReducer.loginDetails,
  healthInsurance: state.insuranceReducer.healthInsurance,
  insuranceAccident: state.insuranceReducer.insuranceAccident,
  additionalInsurance: state.insuranceReducer.additionalInsurance,
  getFilesDetails: state.uploadReducer.getFilesDetails
});

const mapDispatchToProps = dispatch => ({
  additionalinsurance: payload => dispatch(additionalinsurance(payload)),
  getAllinsurance: payload => dispatch(getAllinsurance(payload)),
  getUploadFiles: payload => dispatch(getUploadFiles(payload)),
  handleFileDelete: (id, fileUrl, type) => dispatch(handleFileDelete(id, fileUrl, type))

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsuranceAccident);
