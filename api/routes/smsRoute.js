const express = require('express');
const smsRouter = express.Router();
const { sendSMS } = require('../controller/sms/sms.api');

smsRouter.route('/send')
    .post(sendSMS)
module.exports = smsRouter;