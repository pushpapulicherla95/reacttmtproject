import React, { Component } from "react";
import DoctorList from "../../component/hospital/DoctorList";
import PatientList from "../../component/hospital/PatientList";
import BookedAppontments from "../../component/hospital/BookedAppointmentList";
import DoctorAvailability from "../../component/hospital/DoctorAvailability";
import HospitalAdminInfo from "../../component/hospital/HospitalAdminInfo";
import { connect } from "react-redux";

class HospitalRouting extends Component {
    constructor(props) {
        super(props);
        var loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
        const { userInfo } = loginDetails;
        var userId = userInfo.userId || "";
        this.state = {
            submenuType: "DoctorList",
        }
    }
    handleTab(type) {
        this.props.history.push({ pathname: '/hospitaldashboard', state: { submenuType: type } })
    }
    componentWillMount(){

    }
    render() {
        const { submenuType } = this.props.location.state || this.state;
        return (
            <div className="inner-pageNew">
                <section>
                    <header className="header">
                        <div className="brand">
                            <a href="Dashboard.html" className="backArrow">
                                <i className="fas fa-chevron-left"></i>
                            </a>
                            <div className="mobileLogo">
                                <a href="javascript:void(0)" onClick={()=> this.props.history.push({ pathname: '/hospitalmainmenu'})} className="logo">
                                    <img src="../images/logo.jpg" /> </a></div>

                        </div>
                        <div className="top-nav">

                            <div className="dropdown float-right mr-2">
                                <a className="dropdown-toggle" data-toggle="dropdown">
                                    Tomato Medical
                            </a>
                                <div className="dropdown-menu">
                                    <a className="dropdown-item" href="#"><i className="fa fa-edit icon-head-menu"></i><label className="head-menu">
                                        My account</label></a>
                                    <a className="dropdown-item" href="#">Setting</a>
                                    <a className="dropdown-item" href="#">Logout</a>
                                </div>
                            </div>
                        </div>
                    </header>


                    <aside className="sideMenu">
                        <ul className="sidemenuItems">
                            <li><a href="javascript:void(0);" onClick={() => this.handleTab("DoctorList")} title="Doctors"><img src="images/icon-doctor-color.png" /></a><div className="menu-text"><span>My Doctors</span></div></li>
                            <li><a href="javascript:void(0);" onClick={() => this.handleTab("PatientList")} title="Patients"><img src="../images/icon2-color.png" /></a><div className="menu-text"><span>Personal</span></div></li>
                            <li><a href="javascript:void(0);" onClick={() => this.handleTab("BookedAppointment")} title="Appointment"><img src="images/book-icon-color.png" /></a><div className="menu-text"><span>Booked Appointments</span></div></li>
                            <li><a href="javascript:void(0);" onClick={() => this.handleTab("DoctorAvailability")} title="Doctors Availablity"><img src="images/app-icon-color.png" /></a><div className="menu-text"><span>My Schedules</span></div></li> </ul>
                    </aside>
                    {submenuType == "DoctorList" && <DoctorList />}
                    {submenuType == "PatientList" && <PatientList/>}
                    {submenuType == "BookedAppointment" && <BookedAppontments/>}
                    {submenuType == "DoctorAvailability" && <DoctorAvailability/>}
                    {submenuType == "Hospitaladmininfo" && <HospitalAdminInfo/>}

                </section>
                {/* <div className="modal" id="myModal1">
                    <div className="modal-dialog">
                        <div className="modal-content modalPopUp">
                            <div className="modal-header borderNone">
                                <h5 className="modal-title">Add Doctor</h5>
                                <button type="button" className="popupClose" data-dismiss="modal"><i className="fas fa-times"></i></button>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="row">
                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <div className="form-group">
                                                <label className="commonLabel">First Name</label>
                                                <input type="text" className="form-control input_h_35" />
                                            </div>
                                        </div>

                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <div className="form-group">
                                                <label className="commonLabel">Last Name </label>
                                                <input type="text" className="form-control input_h_35" />
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <div className="form-group">
                                                <label className="commonLabel">Email </label>
                                                <input type="text" className="form-control input_h_35" />
                                            </div>
                                        </div>
                                        <div className="col-md-9 col-lg-9 col-sm-12">
                                            <div className="form-group">
                                                <label className="commonLabel">Password</label>
                                                <input type="text" className="form-control input_h_35" />
                                            </div>
                                        </div>
                                        <div className="col-md-3 col-lg-3 col-sm-12 genPass">
                                            <div className="form-group">

                                                <button className="editbtn1"> Generate</button>
                                            </div>
                                        </div>

                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <div className="form-group">
                                                <label className="commonLabel">Contact </label>
                                                <input type="text" className="form-control input_h_35" />
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <div className="form-group">
                                                <label className="commonLabel">Address </label>
                                                <input type="text" className="form-control " />
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <div className="form-group">
                                                <label className="commonLabel">Upload Document </label>
                                                <input type="file" className="form-control hei-45px" />
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <button type="button" className="commonBtn float-right">Create</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div> */}
                {/* <div className="modal" id="myModal2">
                    <div className="modal-dialog">
                        <div className="modal-content modalPopUp">
                            <div className="modal-header borderNone">
                                <h5 className="modal-title">Delete</h5>
                                <button type="button" className="popupClose" data-dismiss="modal"><i className="fas fa-times"></i></button>
                            </div>
                            <div className="modal-body">
                                <h4 className="deleteMsg"><i className="fas fa-info-circle"></i> Are you sure you want to delete this
                                record</h4>
                                <button type="text" className="commonBtn smallBtn float-right" data-dismiss="modal">No</button>
                                <button type="text" className="commonBtn smallBtn float-right mr-2">Yes</button>

                            </div>
                        </div>
                    </div>
                </div> */}

            </div>
        )
    }
}
const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails
})

const mapDispatchToprops = dispatch => ({
})
export default connect(
    mapStateToProps, 
    mapDispatchToprops
)(HospitalRouting);