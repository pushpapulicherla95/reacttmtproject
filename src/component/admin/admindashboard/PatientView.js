import React,{Component} from "react";
import { connect } from "react-redux";
import {viewDetails} from "../../../service/admin/Patient/action";
class ViewPatient extends Component{
    constructor (props) {
        super(props)
        this.state = {

        }
    }
    componentWillMount(){
       
        const { patientId } = this.props.location.state;
        const viewdata = {
            uid:patientId
        }

        this.props.viewDetails(viewdata);
    }
    render(){
        const {viewPatientDetails}=this.props;
        return(
            <div class="mainMenuSide" id="main-content">
            <div class="wrapper">
                <div class="container-fluid">
                    <div class="body-content">
                        <div class="row">
                            
                           <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="doSection card">
                               <div class="row">
                                   
                               <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                   <div class="doctorImage">
                               <img src="https://images.pexels.com/photos/1239291/pexels-photo-1239291.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=650&w=940"/></div>
                            </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-12">

                                        <div class="doc-title">
                                                <h6 class="title-name"> {viewPatientDetails && viewPatientDetails.firstName} ( {viewPatientDetails && viewPatientDetails.gender})</h6>
                                             
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In suscipit laborum dolores veniam! Accusantium nostrum molestiae doloremque, cupiditate quaerat eveniet.</p>
                                                  <p><span class="title-name clr10022">Phone Number</span></p>
                                                  <p>{viewPatientDetails && viewPatientDetails.mobileNo}</p>
                                                  <p><span class="title-name clr10022">Address</span></p>
                                                  <p>
                                                     {viewPatientDetails && viewPatientDetails.address}
                                                      
                                                  </p>
                                                </div>
                                                </div>
                                        </div>
                           </div> 
                      
                        </div>
                        </div>
                        </div>
         
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state =>({
    loginDetails:state.loginReducer.loginDetails,
    viewPatientDetails:state.adminPatientReducer.viewPatientDetails
    })
const mapDispatchToProps = dispatch =>({
    viewDetails:(viewdata)=>dispatch(viewDetails(viewdata))
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ViewPatient) ;
