import React, { Component } from "react";
import { connect } from "react-redux";
import AWS from "aws-sdk";
import { uploadAdminProfilepic } from "../../../service/admin/Patient/action"
import axios from 'axios';
import URL from '../../../asset/configUrl'
class AdminMainMenu extends Component {
  constructor(props) {
    super(props);
    var loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
    const { adminInfo } = loginDetails;
    var uid = adminInfo.uid || "";

    this.state = {
      loginDetails: props.loginDetails,
      uid: uid,
      profilePicture: adminInfo.profilePicture,
      tabone: true,
      tabtwo: false,
      tabthree: false,
      fileUrl: "",
      fileName: "",
      fileType: "",
      openModel: false,
      currentpassword: "",
      password: "",
      confirmpassword: "",
      errors: {
        password: "",
        confirmpassword: "",
        currentpassword: ""
      },
      passwordValid: false,
      confirmpasswordValid: false,
      currentpasswordValid: false,
      formValid: false,
      randomNumber: Math.random(),
      fileImgUrl: "",
      imageHash: Date.now()

    }
  }
  // componentWillMount(){
  //     AWS.config.update({
  //         accessKeyId: process.env.REACT_APP_ACCESSKEY,
  //         secretAccessKey: process.env.REACT_APP_SECRETACCESSKEY,
  //         region: process.env.REACT_APP_REGION
  //       });
  //     //   var s3 = new AWS.S3();
  //     //   var data = {
  //     //     Bucket: "tomato-medical-bucket",
  //     //     Key: "10.png"
  //     //   };
  //     //   s3.getSignedUrl("getObject", data, async (err, data) => {
  //     //     if (err) {
  //     //       console.log("Error uploading data: ", err);
  //     //     } else {
  //     //       console.log("succesfully uploaded the image!", data);

  //     //       (await data) && this.setState({ img: data });
  //     //     }
  //     //   });

  //         // this.props.symptomMainList()

  //         // const { profilePic } = this.state;
  //         // if (this.props.adminprofilepicture) {
  //         //   this.setState({ profilePic: this.props.profilePicInfo.profilePic })
  //         //   this.getprofileImage(this.props.profilePicInfo.profilePic);

  //         // } else {
  //         //   this.getprofileImage(profilePic);

  //         // }
  //         // const { userInfo } = this.props.loginDetails;
  //         // this.setState({ fileImgUrl: `https://data.tomatomedical.com/profilepicture/${userInfo.userId}_.png?${this.state.imageHash}` })

  // }
  componentWillMount() {
    // this.props.symptomMainList()
    AWS.config.update({
      accessKeyId: process.env.REACT_APP_ACCESSKEY,
      secretAccessKey: process.env.REACT_APP_SECRETACCESSKEY,
      region: process.env.REACT_APP_REGION
    });
    const { profilePicture } = this.state;
    if (this.props.adminprofilepicture) {
      this.setState({ profilePic: this.props.adminprofilepicture.profilePicture })
      this.getprofileImage(this.props.adminprofilepicture.profilePicture);

    } else {
      this.getprofileImage(profilePicture);

    }
    const { adminInfo } = this.props.loginDetails;
    this.setState({ fileImgUrl: `https://data.tomatomedical.com/profilepicture/${adminInfo.uid}_admin.png?${this.state.imageHash}` })
  }
  componentWillReceiveProps = (nextProps) => {
    console.log("nextProps", nextProps);
    if (this.props.adminprofilepicture != nextProps.adminprofilepicture) {
      //   const { profilePicture } = nextProps.adminprofilepicture;
      console.log("nextProps", nextProps);
      // this.setState({ profilePic: nextProps.profilePicInfo.profilePic });
      //   console.log("profilePic", profilePicture);
      const { adminInfo } = this.props.loginDetails;
      this.setState({ imageHash: Date.now() }, () => {
        setTimeout(() => {
          this.setState({ fileImgUrl: `https://data.tomatomedical.com/profilepicture/${adminInfo.uid}_admin.png?${this.state.imageHash}` })
        }, 3000)

      })
      //   this.getprofileImage(profilePicture);
    }
  }
  getprofileImage = (profilePicture) => {

    var data = `https://data.tomatomedical.com/profilepicture/${profilePicture}`;
    this.setState({ fileUrl: data });
    // this.validateQrcode();
    console.log("fileUrl", this.state.fileUrl);
  }
  tabOne = () => {
    this.setState({
      tabone: true,
      tabtwo: false,
      tabthree: false
    });
  };

  tabTwo = () => {
    this.setState({
      tabone: false,
      tabtwo: true,
      tabthree: false
    });
  };

  tabThree = () => {
    this.setState({
      tabone: false,
      tabtwo: false,
      tabthree: true
    });
  };
  handleChangePopup = e => {
    var reader = new FileReader();

    var value = e.target.files[0];
    let name = value.name.split('.')[0]
    let type = "." + value.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileUrl: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(value);
  }
  submitUpload = (e) => {
    e.preventDefault();
    const { fileType, fileUrl, uid, fileName, profilePicture } = this.state;
    var phrase = this.state.fileUrl;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);

    const filedata = {
      uid: uid,
      fileType: fileType,
      fileName: fileName,
      fileUrl: match[1],
    }
    this.props.uploadAdminProfilepic(filedata);
    this.getprofileImage(profilePicture);
    this.setState({
      openModel: false,
      fileUrl: "",
      fileName: "",
      fileType: "",
    })

  }

  modalPopClose = () => {
    this.setState({ openModel: !this.state.openModel });
  }
  openModelPop = () => {
    this.setState({ openModel: true });

  }
  handleLogout = () => {
    const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
    const configs = {
      method: 'post',
      url: URL.USER_LOGOUT,
      data: {
        "uid": loginDetails.userInfo.userId,
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }
    axios(configs)
      .then(response => {
        sessionStorage.clear("loginDetails");
        this.props.history.push('/login/admin')
      })

  }
  render() {
    const { loginDetails } = this.state;
    const { tabone, tabtwo, tabthree, uid, openModel, profilePic, fileUrl, currentpassword, password, errors, formValid, fileImgUrl } = this.state;

    return (
      <div className="inner-pageNew">
        <header className="header">
          <div className="brand">
            <a href="javascript:void(0)" onClick={() => this.props.history.push("/adminmainmenu")} className="logo">
              <img src="../images/logo.jpg" /> </a>

          </div>
          <div className="top-nav" style={{ display: "none" }}>

            <div className="searchBox float-right " >

              <input type="text" className="searchTxt" placeholder="Search Symptom Checker" />
              <a href="javascript:void(0)" className="searchBtn"  >
                <i className="fas fa-search"></i>
              </a>
            </div>

          </div>
          <div className="languageSel text-right mb-2" style={{ margin_top: "20px", margin_right: "10px" }}>
            <span>Language :</span>
            <select className="">
              <option>EN</option>
            </select>
          </div>
        </header>

        <div className="body-content">
          <div className="container-fluid">
            <div className="row">
              <div className="col-lg-3 col-md-3 col-sm-12 col-12 bg-purple-card">
                <div className="card user-profile-card">
                  <div className="row">
                    <div className="col-12">
                      <a href="javaScript:void(0);" onClick={this.openModelPop} >

                        <img src={fileImgUrl ? fileImgUrl : "../images/no-profile-pic-icon-5.png"} />
                      </a>
                      <h5>Tomato Medical</h5>
                      <p>{loginDetails && loginDetails.adminInfo.userType}</p>
                    </div>

                  </div>
                  <div className="set-section">
                    <ul>
                      <li><a href="javaScript:void(0);" onClick={this.openModelPop}><i className="fas fa-cog"></i></a></li>
                      <li><a href=""><i className="fas fa-shopping-cart"></i></a></li>
                      <li><a href="javascript:void(0)" onClick={this.handleLogout}><i className="fas fa-power-off"></i></a></li>
                    </ul>
                  </div>

                </div>

              </div>

              <div className="col-lg-9 col-md-9 col-sm-12 col-12">
                <div className="row">

                  <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                    <a href="javascript:void(0)" onClick={() => this.props.history.push({ pathname: '/admindashboard', state: { submenuType: "AdminProfile" } })} className="card bg-c">
                      <div className="card-img1">
                        <img src="../images/personal-color.png" />
                      </div>
                      <div className="card-title">
                        <h5>Profile</h5>
                      </div>
                    </a>
                  </div>

                  <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                    <a href="javascript:void(0)" onClick={() => this.props.history.push({ pathname: '/admindashboard', state: { submenuType: "AdminPatient" } })} className="card bg-c">
                      <div className="card-img1">
                        <img src="images/myPatient.png" />
                      </div>
                      <div className="card-title">
                        <h5>Patients</h5>
                      </div>
                    </a>
                  </div>
                  <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                    <a href="javascript:void(0)" onClick={() => this.props.history.push({ pathname: '/admindashboard', state: { submenuType: "Hospitalist" } })} className="card bg-c">
                      <div className="card-img1">
                        <img src="images/myPatient.png" />
                      </div>
                      <div className="card-title">
                        <h5>Hospital List</h5>
                      </div>
                    </a>
                  </div>
                  <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                    <a href="javascript:void(0)" onClick={() => this.props.history.push({ pathname: '/admindashboard', state: { submenuType: "AdminMedical" } })} className="card bg-c">
                      <div className="card-img1">
                        <img src="../images/doctors-color.png" />
                      </div>
                      <div className="card-title">
                        <h5>Medical professionals</h5>
                      </div>
                    </a>
                  </div>
                  <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                    <a href="javascript:void(0)" onClick={() => this.props.history.push({ pathname: '/admindashboard', state: { submenuType: "AdminPharma" } })} className="card bg-c">
                      <div className="card-img1">
                        <img src="../images/pharma-color.png" />
                      </div>
                      <div className="card-title">
                        <h5>Pharma Industry</h5>
                      </div>
                    </a>
                  </div>
                  <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                    <a href="javascript:void(0)" onClick={() => this.props.history.push({ pathname: '/admindashboard', state: { submenuType: "AdminInsurance" } })} className="card bg-c">
                      <div className="card-img1">
                        <img src="../images/insurance-color.png" />
                      </div>
                      <div className="card-title">
                        <h5>Insurance</h5>
                      </div>
                    </a>
                  </div>
                  <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                    <a href="javascript:void(0);" className="card bg-c">
                      <div className="card-img1">
                        <img src="../images/insurance-color.png" />
                      </div>
                      <div className="card-title">
                        <h5>Medtech Industry</h5>
                      </div>
                    </a>
                  </div>
                  <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                    <a href="javascript:void(0)" onClick={() => this.props.history.push({ pathname: '/admindashboard', state: { submenuType: "ViewSymptomsQAentry" } })} className="card bg-c">
                      <div className="card-img1">
                        <img src="../images/insurance-color.png" />
                      </div>
                      <div className="card-title">
                        <h5>Symptoms QAentry</h5>
                      </div>
                    </a>
                  </div>
                  <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                    <a href="javascript:void(0)" onClick={() => this.props.history.push({ pathname: '/admindashboard', state: { submenuType: "Textrobot" } })} className="card bg-c">
                      <div className="card-img1">
                        <img src="../images/insurance-color.png" />
                      </div>
                      <div className="card-title">
                        <h5>Text Robot</h5>
                      </div>
                    </a>
                  </div>
                  <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                    <a href="javascript:void(0)" onClick={() => this.props.history.push({ pathname: '/admindashboard', state: { submenuType: "Logdata" } })} className="card bg-c">
                      <div className="card-img1">
                        <img src="../images/insurance-color.png" />
                      </div>
                      <div className="card-title">
                        <h5>Logs Data</h5>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {openModel && <div className="modal d-block show" >
          <div className="modal-dialog">
            <div className="modal-content modalPopUp">
              <div className="modal-header borderNone">
                <h5 className="modal-title">Setting</h5>
                <button type="button" className="popupClose" onClick={this.modalPopClose} ><i className="fas fa-times"></i></button>
              </div>
              <div className="modal-body">
                <form>
                  <div className="tomCardBody">
                    <div className="tab-menu-content">

                      <div className="tab-menu-title">
                        <ul>
                          <li className={this.state.tabone ? "menu1 active" : ""}>
                            <a href="#" onClick={this.tabOne}>
                              <p>Profile</p>
                            </a></li>
                          <li className={this.state.tabtwo ? "menu1 active" : ""}>
                            <a href="#" onClick={this.tabTwo}>
                              <p>Change Password</p>
                            </a></li>
                          <li className={this.state.tabthree ? "menu1 active" : ""}>
                            <a href="#" onClick={this.tabThree}>
                              <p>Delete Account</p>
                            </a></li>
                        </ul>
                      </div>

                      <div className="tab-menu-content-section">
                        {tabone &&
                          <div id="pcontent-1" >

                            <div className="row">
                              <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                <div className="form-group">
                                  <div className="profile_model_image">

                                    <img src={fileImgUrl} />
                                  </div>
                                </div>
                              </div>
                              <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                <div className="form-group">
                                  <label>Upload Profile {tabone} <sup>*</sup></label>
                                  <input type="file" className="form-control" name="fileUrl" onChange={this.handleChangePopup} />
                                </div>
                              </div>

                            </div>
                            <div className="row">
                              <div className="col-12">
                                <button type="button" className="commonBtn float-right" onClick={this.submitUpload}>Update</button></div>
                            </div>
                          </div>}
                        {tabtwo &&
                          <div id="pcontent-2"  >

                            <div className="row">
                              <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                <div className="form-group">
                                  <label>Current Password <sup>*</sup></label>
                                  <input type="text" className="form-control" name="currentpassword" value={currentpassword} onChange={this.handleChange}
                                  />
                                </div>
                                <div style={{ color: "red" }}>{errors.currentpassword}</div>

                              </div>
                              <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                <div className="form-group">
                                  <label>New Password <sup>*</sup></label>
                                  <input type="text" name="password" value={password} className="form-control" onChange={this.handleChange}
                                  />
                                </div>
                                <div style={{ color: "red" }}>{errors.password}</div>

                              </div>
                              <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                <div className="form-group">
                                  <label>Confirm Password <sup>*</sup></label>
                                  <input type="text" name="confirmpassword" value={this.state.confirmpassword} className="form-control" onChange={this.handleChange} />
                                </div>
                                <div style={{ color: "red" }}>{errors.confirmpassword}</div>

                              </div>

                            </div>
                            <div className="row">
                              <div className="col-12">
                                <button type="button" className="commonBtn float-right" onClick={this.changePassword} disabled={!formValid} >Update</button></div>
                            </div>
                          </div>
                        }

                        {tabthree &&
                          <div id="pcontent-3"  >
                            <div className="row">
                              <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                <div className="form-group">
                                  <p className="mb-0">Are you sure you want to delete <b>Tomato Medical Account</b>?
                                                                        </p>
                                  <p>All the Records and Medical Document will be deleted and cannot be undone.</p>
                                </div>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-12">
                                <button type="button" className="commonBtn float-right">Delete</button></div>
                            </div>
                          </div>
                        }


                      </div>
                    </div>
                  </div>

                </form>
              </div>
            </div>
          </div>
        </div>
        }
      </div>
    )
  }
}
const mapStateToProps = state => ({
  loginDetails: state.loginReducer.loginDetails,
  adminprofilepicture: state.adminPatientReducer.adminprofilepicture
});

const mapDispatchToProps = dispatch => ({
  uploadAdminProfilepic: (filedata) => dispatch(uploadAdminProfilepic(filedata)),

});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminMainMenu);