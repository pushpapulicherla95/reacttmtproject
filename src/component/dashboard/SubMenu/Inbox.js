import React from 'react';
import axios from 'axios';
import MultipleSelect from 'react-select'
import moment from 'moment'
import URL from '../../../asset/configUrl'
class Inbox extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            inboxList: [],
            senderList: [],
            type: "inbox",
            doctorList: [],
            receiverMailId: "",
            message: ""
        }
    }

    componentWillMount = () => {
        this.inboxList()
        this.senderList()
        this.doctorList()
    }

    inboxList = () => {
        const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))

        const configs = {
            method: 'post',
            url: URL.RECEIVER_INBOXLIST,
            data: { "uid": loginDetails.userInfo.userId },
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
          }
        axios(configs)
            .then(response => {
                this.setState({ inboxList: response.data.notificationList })
            })

    }
    senderList = () => {
        const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))

        const configs = {
            method: 'post',
            url: URL.SENDER_INBOXLIST,
            data: { "uid": loginDetails.userInfo.userId },
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
          }
        axios(configs)
            .then(response => {
                this.setState({ senderList: response.data.notificationList })
            })
    }

    composeSubmit = () => {
        // document.getElementById("composeSubmit").setAttribute("data-dismiss", "modal");

        const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
        const configs = {
            method: 'post',
            url: URL.SEND_NOTIFICATION_MESSAGE,
            data: {
                "uid": loginDetails.userInfo.userId,
                "email": loginDetails.userInfo.email,
                "receiverEmailId": this.state.receiverMailId,
                "message": this.state.message
            },
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
          }
        axios(configs)
            .then((response) => {
                var plant = document.getElementById('composeSubmitId');
                plant.setAttribute("data-dismiss", "modal");
                this.senderList();
                this.inboxList();

            })
    }



    doctorList = () => {
        const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
        if (loginDetails.userInfo.userType == "patient") {
            axios.get(URL.ADMIN_DOCTOR_LIST)
                .then(response => {
                    const optionArray = response.data.doctorList.map((val) => {
                        return {
                            option: val.email,
                            label: val.email
                        }
                    })
                    this.setState({ doctorList: optionArray })
                    console.log("response.data", response.data.doctorList)
                })
        } else if (loginDetails.userInfo.userType == "doctor") {
            axios.get(URL.ADMIN_PATIENT_LIST)
                .then(response => {
                    const optionArray = response.data.patientList.map((val) => {
                        return {
                            option: val.email,
                            label: val.email
                        }
                    })
                    this.setState({ doctorList: optionArray })
                })
        }
    }



    render() {
        const { type, senderList, inboxList } = this.state
        return (<div class="share_card_new">
            <div className="mail_create">
                <button data-toggle="modal" data-target="#shareModal2"><i class="far fa-paper-plane"></i> Compose</button>
            </div>
            <div class="tab_list">
                <button className={this.state.type == "inbox" && "active"} onClick={() => this.setState({ type: "inbox" })}>Inbox</button>
                <button className={this.state.type == "sender" && "active"} onClick={() => this.setState({ type: "sender" })} >Send Items</button>
            </div>
            <div className="inbox_message">
                <ul>

                    {type == 'inbox' && inboxList.length > 0 && inboxList.map((each) =>
                        <li class="message_list">
                            <p className="w-20per">{each.senderMailId}</p>
                            <p className="w-70per">{each.message}</p>
                            <p className="w-10per">{moment(each.date).format("MMM DD YYYY")}</p>
                        </li>
                    )}

                    {type == 'sender' && senderList.length > 0 && senderList.map((each) =>

                        <li class="message_list">

                            <p className="w-20per">{each.receiverMailId}</p>
                            <p className="w-70per">{each.message}</p>
                            <p className="w-10per">{moment(each.date).format("MMM DD YYYY")}</p>
                        </li>
                    )}
                </ul>
            </div>

            <div class="modal" id="shareModal2" >
                <div class="modal-dialog">
                    <div class="modal-content modalPopUp">
                        <div class="modal-header borderNone">
                            <h5 class="modal-title">Email</h5>
                            <button type="button" class="popupClose" data-dismiss="modal"><i class="fas fa-times"></i></button>
                        </div>
                        <div class="modal-body">
                            <h6>Email</h6>
                            <div class="form-group">
                                {/* <input type="text" name="browsers" class="form-control" id="usr" list="browsers" />
                                <datalist id="browsers" style={{ width: "100%" }} >
                                    {
                                     this.state.doctorList.map((each,i)=>
                                        <option value={each.email} />
                                     )
                                    }
                                </datalist> */}
                                <MultipleSelect
                                    options={this.state.doctorList}
                                    placeholder="Enter Sender Email"
                                    onChange={(e) => this.setState({ receiverMailId: e.option })}
                                />
                            </div>
                            <h6>Message</h6>
                            <div class="form-group">
                                <textarea class="form-control" name="message" value={this.state.message} rows="2" id="comment" onChange={(e) => {
                                    this.setState({ [e.target.name]: e.target.value })
                                }}></textarea>
                            </div>
                            <button class="send_btn" id="composeSubmitId" onClick={this.composeSubmit}><i class="far fa-paper-plane" ></i> Send</button>

                        </div>
                    </div>
                </div>
            </div>
        </div>)
    }
}
export default Inbox;