import React, { Component } from "react";
import {
  addJobdata,
  getPersonaldata
} from "../../../../service/healthrecord/action";
import { getUploadFiles, handleFileDelete } from "../../../../service/upload/action";
import { connect } from "react-redux";
import $ from "jquery";
import axios from "axios";
import { onlyAlphabets, onlyNumbers } from "../KeyValidation";
import URL from "../../../../asset/configUrl";
import { awsPersonalFiles } from "../../../../service/common/action";

import { toastr } from "react-redux-toastr";

// import { geolocated } from "react-geolocated";
class job extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 1,
      tabone: true,
      tabtwo: false,
      trainedJob: "",
      currentJob: "",
      employer: "",
      uploadPopup: false,
      patientFindingListData: [],
      name: "",
      fileName: "",
      fileType: "",
      fileURL: "",
      latitude: "",
      longitude: "",
      Valid: false
    };
  }
  handleAwsFiles = value => {
    window.open("https://data.tomatomedical.com/patientpersonal/" + value, '_blank');

  };
  ValidationField = () => {
    const {
      tainedJob,
      currentJob,
      employeer } = this.state
    this.setState({
      Valid:
        tainedJob ||
        currentJob ||
        employeer
    });

  }
  handleChangePopup = e => {
    var reader = new FileReader();
    var file = e.target.files[0];
    let name = file.name.split('.')[0]
    let type = "." + file.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileURL: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(file);
  };
  popupSubmit = () => {
    var phrase = this.state.fileURL;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);
    var { userInfo } = this.props.loginDetails;
    const payload = {
      uid: userInfo.userId,
      z: match[1],
      fileName: this.state.fileName,
      fileType: this.state.fileType,
      name: this.state.name,
      category: "job"
    };
    this.setState({ uploadPopup: false });

    axios
      .post(URL.FILEJOBDATA_UPLOAD, payload)
      .then(response => {
        response.data.status === "success"
          ? toastr.success("Message", response.data.message)
          : toastr.error("Message", response.data.message);
        if (response.data.status === "success") {
          var { userInfo } = this.props.loginDetails;
          const uploadFiles = {
            uid: userInfo.userId,
            category: "job"
          };
          this.props.getUploadFiles(uploadFiles);
          this.setState({ uploadPopup: false });
          this.setState({
            name: "",
            fileName: "",
            fileType: "",
            fileURL: ""
          });
        }
      })
      .catch(error => {
        //  this.setState({ uploadPopup: true });
      });
  };

  tabOne = () => {
    this.setState({ tabone: true, tabtwo: false });
  };
  tabTwo = () => {
    this.setState({ tabone: false, tabtwo: true });
  };
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value }, () => this.ValidationField());
  };
  componentWillMount() {
    const { userInfo } = this.props.loginDetails;
    const listpersonal = {
      uid: userInfo.userId
    };
    const uploadFiles = {
      uid: userInfo.userId,
      category: "job"
    };
    this.props.getUploadFiles(uploadFiles);
    this.props.getPersonaldata(listpersonal);
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.getpersonalDetails &&
      nextProps.getpersonalDetails.job != null &&
      nextProps.getpersonalDetails.job != 0
    ) {
      const listpersonal = nextProps.getpersonalDetails.job;
      let patientFindingListData = JSON.parse(listpersonal);
      this.setState({
        trainedJob: patientFindingListData.trainedJob,
        currentJob: patientFindingListData.currentJob,
        employer: patientFindingListData.employer
      });
    }
  }
  trigger = () => {
    this.setState({ uploadPopup: !this.state.uploadPopup });
  };
  componentDidMount = () => {
    const Geo = window.navigator && window.navigator.geolocation;
    if (Geo) {
      Geo.getCurrentPosition(position => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        });
        console.log(
          "latitude",
          this.state.latitude,
          "longitude",
          this.state.longitude
        );
      });
    }
  };
  handleData = () => {
    const { trainedJob, currentJob, employer } = this.state;
    const { userInfo } = this.props.loginDetails;
    const jobdata = {
      userId: userInfo.userId,
      job: {
        trainedJob: trainedJob,
        currentJob: currentJob,
        employer: employer
      }
    };

    this.props.addJobdata(jobdata);
  };
  render() {
    const {
      tabone,
      tabtwo,
      trainedJob, currentJob, employer,
      patientFindingListData, Valid
    } = this.state;
    console.log(
      "latitude",
      this.state.latitude,
      "longitude",
      this.state.longitude
    );
    return (
      <div className="" id="main-content">
        <div className="wrapper">
          <div className="container-fluid">
            <div className="body-content">
              <div className="tomCard">
                <div className="tomCardHead">
                  <h5 className="float-left">JOB DATA</h5>
                </div>

                <div className="tab-menu-content">
                  <div class="tab-menu-title">
                    <ul>
                      <li className={this.state.tabone ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabOne}>
                          <p>Job</p>
                        </a>
                      </li>
                      <li className={this.state.tabtwo ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabTwo}>
                          <p>Attached Files</p>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>

                {tabone && (
                  <div className="tab-menu-content-section">
                    <div
                      id="content-1"
                      className={tabone ? "d-block" : "d-none"}
                    >
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Trained Job</label>
                            <input
                              type="text"
                              class="form-control"
                              name="trainedJob"
                              onChange={this.handleChange}
                              value={trainedJob}
                              onKeyPress={onlyAlphabets}
                            />
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Current Job</label>
                            <input
                              type="text"
                              class="form-control"
                              name="currentJob"
                              onChange={this.handleChange}
                              value={currentJob}
                              onKeyPress={onlyAlphabets}
                            />
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Employer</label>
                            <input
                              type="text"
                              class="form-control"
                              name="employer"
                              onChange={this.handleChange}
                              value={employer}
                              onKeyPress={onlyAlphabets}
                            />
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-12">
                          <button
                            type="button"
                            class="commonBtn float-right"
                            onClick={this.handleData} disabled={!Valid}
                          >
                            Save
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                {tabtwo && (
                  <div id="content-2" className={tabtwo ? "d-block" : "d-none"}>
                    <button
                      type="button"
                      className="commonBtn2 float-right mb-2"
                      onClick={this.trigger}
                    >
                      <i className="fa fa-plus" />
                      Upload new files
                    </button>
                    <div className="table-responsive">
                      <table className="table table-hover">
                        <thead className="thead-default">
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>FileName</th>

                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.props.getFilesDetails.fileDetails &&
                            this.props.getFilesDetails.fileDetails.map(
                              (each, i) => (
                                <tr>
                                  <td data-label="#">{i + 1}</td>
                                  <td data-label="Name">{each.name}</td>
                                  <td data-label="FileName"> {each.fileName}</td>
                                  <td data-label="Action">
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.handleAwsFiles(each.fileUrl)
                                      }
                                    >
                                      <i class="fas fa-download" />
                                    </button>
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.props.handleFileDelete(each.id, each.fileUrl, "job")
                                      }
                                    >
                                      <i class="fas fa-trash" />
                                    </button>
                                  </td>
                                </tr>
                              )
                            )}

                        </tbody>
                      </table>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        {
          <div
            className={this.state.uploadPopup ? "modal d-block" : "modal"}
            id="myModal"
          >
            <div class="modal-dialog">
              <div class="modal-content modalPopUp">
                <div class="modal-header borderNone">
                  <h5 class="modal-title">Upload File</h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    onClick={this.trigger}
                  >
                    <i class="fas fa-times" />
                  </button>
                </div>
                <div class="modal-body">
                  <form>
                    <div class="row">
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="form-group">
                          <label className="form-label">File name</label>
                          <input
                            className="form-control"
                            type="text"
                            name="name"
                            value={this.state.name}
                            onChange={this.handleChange}
                          />
                          <span />
                        </div>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="fileUp">
                          <label
                            for="fileUp1"
                            class="commonBtn text-center upload"
                          >
                            Upload File
                          </label>
                          <input
                            type="file"
                            id="fileUp1"
                            style={{ display: "none" }}
                            onChange={this.handleChangePopup}
                          />
                          {/* <p style={{ textAlign: "center" }}>hsfkjhsfj</p> */}
                        </div>
                      </div>

                      <div class="col-md-12 col-lg-12 col-sm-12 mt-2">
                        <button
                          type="button"
                          class="commonBtn float-right"
                          onClick={this.popupSubmit} disabled={!this.state.fileURL}
                        >
                          Add
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  getpersonalDetails: state.healthrecordReducer.getpersonalDetails,
  loginDetails: state.loginReducer.loginDetails,
  getFilesDetails: state.uploadReducer.getFilesDetails
});

const mapDispatchToProps = dispatch => ({
  addJobdata: jobdata => dispatch(addJobdata(jobdata)),
  getPersonaldata: listpersonal => dispatch(getPersonaldata(listpersonal)),
  getUploadFiles: payload => dispatch(getUploadFiles(payload)),
  handleFileDelete: (id, fileUrl, type) => dispatch(handleFileDelete(id, fileUrl, type))

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(job);
