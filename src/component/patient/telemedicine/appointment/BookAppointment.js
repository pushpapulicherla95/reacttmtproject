import React, { Component } from "react";
import { bookAppointmentAPICall } from "../../../../service/appointment/action";
import { listScheduleInfo } from "../../../../service/doctor/action";
import { connect } from "react-redux";
import Loader from "react-loader-spinner";

class BookAppointment extends Component {
  state = {
    schedule: "false",
    manageuser: "true",
    userInfoId: "2",
    startTime: "",
    endTime: "",
    appointmentDate: "",
    // doctorId: "2",
    status: "A",
    editSchedulepop: false,
    editDetails: {},
    scheduleLists: [],
    addSchedulepop: false
  };
  componentWillMount = () => {
    const { userInfo } = this.props.loginDetails;
    const scheduleInfo = {
      doctorId: userInfo.userId
    };

    this.props.listSchedulesMethod(scheduleInfo);
  };

  componentWillReceiveProps = () => {
    const { scheduleLists } = this.props.listscheduleDetails;
    this.setState({ scheduleLists: scheduleLists });
  };
  handleChange = e => {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });
  };
  handleSchedule = () => {
    const {
      doctorId,
      startTime,
      endTime,
      appointmentDate,
      status
    } = this.state;
    const doctorSchedule = {
      doctorId: "2",
      startTime: startTime,
      endTime: endTime,
      appointmentDate: appointmentDate,
      status: status
    };
    this.props.createDoctorSchedule(doctorSchedule);
    const scheduleinfo = {
      doctorId: "2"
    };
    this.props.listScheduleInfo(scheduleinfo);
    this.setState({ addSchedulepop: false });
  };
  //updateSchedule
  updateSchedule = () => {
    const {
      scheduleId,
      startTime,
      endTime,
      appointmentDate,
      status
    } = this.state;
    const updateSchedule = {
      scheduleId: "39",
      startTime: startTime,
      endTime: endTime,
      appointmentDate: appointmentDate,
      status: status
    };
    this.props.updateDoctorSchedule(updateSchedule);
    this.setState({ editSchedulepop: false });
  };
  bookAppointment = e => {
    const bookAppointmentInfo = {
      scheduleId: e.scheduleId,
      patientId: e.userId,
      doctorId: e.doctorId,
      scheduleType: "online",
      status: "BOOKED"
    };
    this.props.bookAppointmentMethod(bookAppointmentInfo);
  };
  render() {
    const { isLoading } = this.props;
    const { startTime, endTime, appointmentDate, status } = this.state;
    const { scheduleLists } = this.state;

    return (
      <React.Fragment>
        {isLoading && (
          <div className="pop-mod">
            <div className="loder-back">
              <Loader
                type="Ball-Triangle"
                color="#00BFFF"
                height="100"
                width="100"
              />
            </div>
          </div>
        )}

        <div className="cui-utils-content">
          <div className="cui-utils-title mb-3">
            <strong className="text-uppercase font-size-16">
              Available Schedules
            </strong>
          </div>
          <div className="row">
            <div className="col-xl-12">
              <div className="card">
                <div className="card-body">
                  <div className="table-responsive">
                    <table className="table table-hover">
                      <thead className="thead-default">
                        <tr>
                          <th>Date</th>
                          <th>Start Time</th>
                          <th>End Time</th>
                          <th>status </th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        {scheduleLists &&
                          scheduleLists.map((each, i) => (
                            <tr key={i}>
                              <td data-label="appointmentDate">
                                {each.appointmentDate}
                              </td>
                              <td data-label="startDate">{each.startTime}</td>
                              <td data-label="endDate">{each.endTime}</td>
                              <td data-label="status">{each.status} </td>
                              <td data-label="Action">
                                {" "}
                                <a
                                  title="Book Appointment"
                                  className="deleteBtn"
                                  onClick={e => this.bookAppointment(each)}
                                >
                                  B
                                </a>
                              </td>
                            </tr>
                          ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  listscheduleDetails: state.doctorReducer.listscheduleDetails,
  isLoading: state.loginReducer.isLoading,
  emailResponse: state.commonReducer.emailResponse,
  loginDetails: state.loginReducer.loginDetails
});

const mapDispatchToProps = dispatch => ({
  listSchedulesMethod: scheduleInfo => dispatch(listScheduleInfo(scheduleInfo)),
  bookAppointmentMethod: scheduleInfo =>
    dispatch(bookAppointmentAPICall(scheduleInfo))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookAppointment);
