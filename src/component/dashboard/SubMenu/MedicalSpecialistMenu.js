import React, { Component } from "react";
import { connect } from "react-redux";
import { Switch, Route, Redirect, NavLink } from "react-router-dom";
import { withRouter } from "react-router";
import AWS from "aws-sdk";
import {
  getClinic,
  getClinicDoctor
} from "../../../service/medicalSpecialist/action";
import axios from "axios";
import {
  pick as _pick,
  map as _map,
  partialRight as _partialRight,
  uniqBy as _uniqBy
} from "lodash";
import URL from '../../../asset/configUrl';
import { userInfo } from "os";
class MedicalSpecialistMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCountry: "",
      suggestionCountry: "",
      suggestionpop: false,
      suggestionClinic: "",
      clinicpopup: false,
      selectedClinic: "",
      list: "",
      img: "",
      suggesstionList: []
    };
  }

  componentWillReceiveProps(nextProps) {
    console.log("list>>>>>>>", nextProps)

    this.setState({ list: nextProps.clinicDoctorDetails.lists });
  }

  componentDidMount = () => {
    this.props.getClinic();
    this.props.getClinicDoctor();
    this.location();
    AWS.config.update({
      accessKeyId: process.env.REACT_APP_ACCESSKEY,
      secretAccessKey: process.env.REACT_APP_SECRETACCESSKEY,
      region: process.env.REACT_APP_REGION
    });
    var s3 = new AWS.S3();
    var data = {
      Bucket: "tomato-medical-bucket",
      Key: "logo.jpg"
    };
    s3.getSignedUrl("getObject", data, async (err, data) => {
      if (err) {
        console.log("Error uploading data: ", err);
      } else {
        console.log("succesfully uploaded the image!", data);

        // (await data) && this.setState({ img: data });
      }
    });
  };

  autoComplete = (Arr, Input) => {
    return Arr.filter(
      (e, index) => e != null && e.toLowerCase().includes(Input.toLowerCase())
    );
  };

  handleLocation = e => {
    this.setState({ [e.target.name]: e.target.value });
    if (e.target.value.length) {
      var res = _map(
        this.state.list,
        _partialRight(_pick, [
          "id",
          "area",
          "city",
          "country",
          "pincode",
          "state"
        ])
      );
      this.setState({
        suggestionCountry:
          this.state.suggesstionList &&
          this.autoComplete(this.state.suggesstionList, e.target.value)
      });
    } else {
      this.props.getClinic();
      this.props.getClinicDoctor();
      this.setState({ suggestionCountry: "" });
    }
  };

  autoCompleteClinic = (Arr, Input) => {
    return Arr.filter(e => e.name.toLowerCase().includes(Input.toLowerCase()));
  };

  handleClinic = e => {
    const { therapists } = this.props.clinicDetails;
    this.setState({ [e.target.name]: e.target.value });
    if (e.target.value) {
      this.props.getClinic();
      this.props.getClinicDoctor();
      const res = this.autoCompleteClinic(therapists, e.target.value);
      this.setState({
        suggestionClinic: this.autoCompleteClinic(therapists, e.target.value)
      });
      this.setState({ clinicpopup: true });
    } else {
      this.setState({ clinicpopup: false });
      this.setState({ suggestionClinic: "" });
    }
  };

  searchFilter = () => {
    this.setState({
      list: this.props.clinicDoctorDetails.lists.filter((each, i) => {
        return (
          (this.state.selectedClinic == each.doctorSpecialistName ||
            this.state.selectedCountry == each.area) ||
          this.state.selectedCountry == each.city ||
          this.state.selectedCountry === each.state
        );
      })
    });
  };

  location = () => {
    axios
      .get(
        URL.LOCATION
      )
      .then(response => {
        const { listDoctorLocation } = response.data;
        this.setState({ suggesstionList: listDoctorLocation });
      });
  };

  render() {
    const { selectedCountry } = this.state;
    const { userInfo } = this.props.loginDetails
    return (
      <div className="mainMenuSide" id="main-content">
        <div className="wrapper">
          <div className="container-fluid">
            <div className="body-content">
              <div className="search-section">
                <div className="container">
                  <div className="row">
                    <div className="col-lg-3 col-md-6 col-sm-12 col-12">
                      <div className="search-doctor">
                        <input
                          type="text"
                          placeholder="Location"
                          name="selectedCountry"
                          value={this.state.selectedCountry}
                          onChange={this.handleLocation}
                          onKeyPress={() =>
                            this.setState({ suggestionpop: true })
                          }
                        />

                        <i className="fas fa-map-marker-alt search-icon" />
                        {
                          this.state.suggestionpop && (
                            <div className="searchResultSuggestion">
                              <ul>
                                {this.state.suggestionCountry.length > 0
                                  ? this.state.suggestionCountry.map(
                                    (each, i) => (
                                      <li
                                        value={each}
                                        onClick={() =>
                                          this.setState({
                                            selectedCountry: each,
                                            suggestionpop: false
                                          })
                                        }
                                      >
                                        {each}
                                      </li>
                                    )
                                  )
                                  : "No Record Found in this city"
                                }


                              </ul>
                            </div>
                          )}
                      </div>
                    </div>
                    <div className="col-lg-5 col-md-6 col-sm-12 col-12">
                      <div className="search-doctor">
                        <input
                          type="text"
                          name="selectedClinic"
                          value={this.state.selectedClinic}
                          placeholder="Search doctors, clinics, hospitals, etc."
                          onChange={this.handleClinic}
                        />
                        <i className="fas fa-search search-icon i-rotate-90" />
                        {this.state.clinicpopup && (
                          <div className="searchResultSuggestion">
                            <ul>
                              {this.state.suggestionClinic ? (
                                this.state.suggestionClinic.map((each, i) => (
                                  <li
                                    onClick={() => {
                                      this.setState({
                                        selectedClinic: each.name,
                                        clinicpopup: false
                                      });
                                    }}
                                  >
                                    {each.name}
                                  </li>
                                ))
                              ) : (
                                  <li>No search result</li>
                                )}
                              {/* <li>Country List show</li>
                            <li>Country List show</li>
                            <li>Country List show</li>
                            <li>Country List show</li> */}
                            </ul>
                          </div>
                        )}
                      </div>
                    </div>
                    <div className="search-doctor-btn col-lg-2 col-12">
                      <button
                        className="searchDocBtn"
                        disabled={!this.state.selectedClinic}
                        onClick={this.searchFilter}
                      >
                        Search
                      </button>
                    </div>
                    <div>
                      <div className="dropdown search-doctor-filter-section">
                        {/* <button
                          type="button"
                          className="search-doctor-filter dropdown-toggle"
                          data-toggle="dropdown"
                        >
                          <i className="fas fa-filter filter-icon" />{" "}
                          <span>All Filter</span>
                        </button> */}
                        <div className="dropdown-menu">
                          <div>
                            <h6>Availability</h6>
                            <div className="row">
                              <div className="form-group dropdown-item">
                                <input
                                  className="inp-cbx"
                                  id="cbx"
                                  type="checkbox"
                                  style={{ display: "none" }}
                                />
                                <label className="cbx" htmlFor="cbx">
                                  <span>
                                    <svg
                                      width="12px"
                                      height="10px"
                                      viewBox="0 0 12 10"
                                    >
                                      <polyline points="1.5 6 4.5 9 10.5 1" />
                                    </svg>
                                  </span>
                                  <span>Today</span>
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="search-result-section">
                {console.log("this.state.list ", this.state.list)
                }
                {this.state.list && this.state.list.length > 0
                  ? this.state.list.map((each, i) => ( 
                      <div
                        className="search-card"
                        key={i}
                        onClick={() =>
                          this.props.history.push({
                            pathname: "/docsearchdetails",
                            state: {
                              doctorSpecialistId: each.doctorSpecialistId,
                              doctorSpecialistName: each.doctorName
                            }
                          })
                        }
                      >
                        {/* <img src="../images/business-discussion-employee-2182976.jpg" /> */}
                        <img src={`https://data.tomatomedical.com/profilepicture/${each.doctorInfoId}_.png`} />
                        <div className="doc-title">
                          <p className="title-name">{each.doctorName}</p>
                          <p>{each.doctorSpecialistName} Specialist</p>
                        </div>
                      </div>
                  
                  ))
                  : "No Records Found "}
                {/* <a href="">
                  <div className="search-card">
                    <img src="../images/dummy-doctor.png" />
                    <div className="doc-title">
                      <p className="title-name">Dr. Salim Rana</p>
                      <p>Heart Surgeon</p>
                    </div>
                  </div>
                </a>

                <a href="">
                  <div className="search-card">
                    <img src="../images/business-discussion-employee-2182976.jpg" />
                    <div className="doc-title">
                      <p className="title-name">Dr. Salim Rana</p>
                      <p>Heart Surgeon</p>
                    </div>
                  </div>
                </a>
                <a href="">
                  <div className="search-card">
                    <img src="../images/dummy-doctor.png" />
                    <div className="doc-title">
                      <p className="title-name">Dr. Salim Rana</p>
                      <p>Heart Surgeon</p>
                    </div>
                  </div>
                </a> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  clinicDetails: state.medicalSpecialistReducer.clinicDetails,
  clinicDoctorDetails: state.medicalSpecialistReducer.clinicDoctorDetails,
  loginDetails: state.loginReducer.loginDetails,
});

const mapDispatchToProps = dispatch => ({
  getClinic: () => dispatch(getClinic()),
  getClinicDoctor: () => dispatch(getClinicDoctor())
});
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(MedicalSpecialistMenu)
);
