import URL from "../../asset/configUrl";
import axios from "axios";
import actiontype from "../../service/login/actionType";
import { viewProfile } from "../dashboard/action";
import { toastr } from "react-redux-toastr";

import { toastrOptions } from "../../component/common/toasteroptions";
export const userLogin = userinfo => dispatch => {
  const configs = {
    method: "post",
    url: URL.USER_LOGIN,
    data: userinfo,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(configs)
    .then(res => {
      if (res.data.status == "success") {
        sessionStorage.setItem("loginDetails", JSON.stringify(res.data));
        dispatch({
          type: actiontype.LOGIN_SUCCESS,
          payload: res.data,
          loginStatus: "123456789"
        });
        toastr.success("Success", res.data.message, toastrOptions);
      }
      else if (res.data.status == "failure") {
        toastr.error("Error", res.data.message, toastrOptions);
      }
    })
    .catch(err => {
      dispatch({
        type: actiontype.LOGIN_FAILURE,
        error: err
      });
      toastr.error("Server Error ", toastrOptions);
    });
};

export const loginAdmin = userinfo => dispatch => {
  const configs = {
    method: "post",
    url: URL.ADMIN_LOGIN,
    data: userinfo,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(configs)
    .then(res => {
      if (res.data.status == "success") {
        sessionStorage.setItem("loginDetails", JSON.stringify(res.data));
        dispatch({
          type: actiontype.ADMIN_LOGIN_SUCCESS,
          payload: res.data
        });
        toastr.success("Success", res.data.message, toastrOptions);
      }
      else if (res.data.status == "failure") {
        toastr.error("Error", res.data.message, toastrOptions);
      }
    })
    .catch(err => {
      dispatch({
        type: actiontype.ADMIN_LOGIN_FAILURE,
        error: err
      });
      toastr.error("Error", toastrOptions);
    });
};
export const healthcareRegister = registerinfo => dispatch => {
  const config = {
    method: "post",
    url: URL.USER_REGISTER,
    data: registerinfo,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(config)
    .then(res => {
      if (res.data.status == "success") {
        sendRegisterMail({ ...registerinfo, healthCare: true });
        dispatch({
          type: actiontype.HEALTHCARE_REGISTER_SUCCESS,
          payload: res.data
        });
        toastr.success("Success", res.data.message, toastrOptions);
      }
      // else if (res.data.status == "failure") {
      //   toastr.error("Error", res.data.message, toastrOptions);
      // }
    })
    .catch(err => {
      dispatch({
        type: actiontype.HEALTHCARE_REGISTER_FAILURE,
        error: err
      });
      toastr.error("Error", toastrOptions);
    });
};

export const hospitalRegister = registerinfo => dispatch => {
  const config = {
    method: "post",
    url: URL.HOSPITAL_REGISTER,
    data: registerinfo,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(config)
    .then(res => {
      if (res.data.status == "success") {
        sendRegisterMail({ ...registerinfo, healthCare: true });
        dispatch({
          type: actiontype.HOSPITAL_REGISTER_SUCCESS,
          payload: res.data
        });
        toastr.success("Success", res.data.message, toastrOptions);
      }
      // else if (res.data.status == "failure") {
      //   toastr.error("Error", res.data.message, toastrOptions);
      // }
    })
    .catch(err => {
      dispatch({
        type: actiontype.HOSPITAL_REGISTER_FAILURE,
        error: err
      });
      toastr.error("Error", toastrOptions);
    });
};

export const getUserType = () => dispatch => {
  var headers = {
    headers: {
      "Content-Type": "application/json"
    }
  };
  axios
    .get(URL.USER_TYPE_LIST)
    .then(res => {
      dispatch({
        type: actiontype.GET_USERTYPE_SUCCESS,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: actiontype.GET_USERTYPE_FAILURE,
        error: err
      });
    });
};
export const register = registerinfo => dispatch => {
  const config = {
    method: "post",
    url: URL.PATIENT_REGISTER,
    data: registerinfo,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(config)
    .then(res => {
      if (res.data.status == "success") {
        sendRegisterMail(registerinfo);
        dispatch({
          type: actiontype.REGISTER_SUCCESS,
          payload: res.data
        });
        toastr.success("Success", res.data.message, toastrOptions);
      } else if (res.data.status == "failure") {
        toastr.error("Error", res.data.message, toastrOptions);
      }
    })
    .catch(error => {
      dispatch({
        type: actiontype.REGISTER_FAILURE,
        error: error
      });
      toastr.error("errr", toastrOptions);
    });
};

export const forgot = forgotinfo => dispatch => {
  const configs = {
    method: "post",
    url: URL.USER_FORGOTPASSWORD,
    data: forgotinfo,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(configs)
    .then(res => {
      if (res.data.status === "success") {
      console.log("res", res)

        const payload = {
          email: forgotinfo.email,
          token: res.data.otp
        }; dispatch({
          type: actiontype.FORGOTPASSWORD_SUCCESS,
          payload: res.data
        });
        sendForgetMail(payload);

        toastr.success("Success", res.data.message, toastrOptions);
      } else if (res.data.status == "failure") {
        toastr.error("Error", res.data.message, toastrOptions);

      }

    })
    .catch(err => {
      dispatch({
        type: actiontype.FORGOTPASSWORD_FAILURE,
        error: err
      });
      toastr.error("errr", toastrOptions);
    });
};
export const change = changeinfo => dispatch => {
  const configs = {
    method: "post",
    url: URL.USER_CHANGEPASSWORD,
    data: changeinfo,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(configs)
    .then(res => {
      if (res.status === 200) {
        dispatch({
          type: actiontype.CHANGEPASSWORD_SUCCESS,
          payload: res.data
        });

        toastr.success("Success", res.data.message, toastrOptions);
      } else if (res.data.status == "failure") {
        toastr.error("Error", res.data.message, toastrOptions);
      }
    })
    .catch(err => {
      dispatch({
        type: actiontype.CHANGEPASSWORD_FAILURE,
        error: err
      });
      toastr.error("Error", err.response.data.message, toastrOptions);
    });
};

export const changepsw = changepswinfo => dispatch => {
  const configs = {
    method: "post",
    url: URL.USER_PASSWORDCHANGE,
    data: changepswinfo,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(configs)
    .then(res => {
      if (res.status === 200) {
        dispatch({
          type: actiontype.USERCHANGEPASSWORD_SUCCESS,
          payload: res.data
        });
      }
      if (res.data.status == "success") {
        toastr.success("Success", res.data.message, toastrOptions);
      } else if (res.data.status == "failure") {
        toastr.error("Error", res.data.message, toastrOptions);
      }
    })
    .catch(err => {
      dispatch({
        type: actiontype.USERCHANGEPASSWORD_FAILURE,
        error: err
      });
      toastr.error("Error", err.response.data.message, toastrOptions);
    });
};

export const showIsLoading = () => {
  return {
    type: actiontype.SHOW_IS_LOADING
  };
};

export const hideIsLoading = () => {
  return {
    type: actiontype.HIDE_IS_LOADING
  };
};

const sendRegisterMail = registerinfo => {
  const configs = {
    method: "post",
    url: URL.REGISTER_MAIL,
    data: registerinfo,
    headers: {
      "Content-Type": "application/json"
    }
  };
  axios(configs)
    .then(res => {
      if (res.data.status == "success") {
        //toastr.success("Success", res.data.message, toastrOptions);
      } else if (res.data.status == "failure") {
        //toastr.error("Error", "Mail sending failed", toastrOptions);
      }
    })
    .catch(err => {
      // dispatch({
      //     type: actiontype.REGISTER_MAIL_FAILURE,
      //     error: err
      // })  
      console.log("MAIL SENT ERROR", err)
      //toastr.error("error", toastrOptions);
    });
};

const sendForgetMail = info => {
  const configs = {
    method: "post",
    url: URL.FORGET_MAIL,
    data: info,
    headers: {
      "Content-Type": "application/json"
    }
  };

  axios(configs).then(async res => {
    if (res.data.status == "success") {
      await toastr.success("Success", res.data.message, toastrOptions);
    } else if (res.data.status == "failure") {
      toastr.error("Error", res.data.message, toastrOptions);
    }
  })
    .catch(err => {
      // toastr.error("Error", toastrOptions);
    });
};
