const stripe = require("stripe")("sk_test_hvPnrQ6yTlxPjvw8IMKw0JTW00kp6rSUAF");

const createCharge = (request, response) => {
  stripe.charges.create(request.body, (err, charge) => {
    if (err) {
      response.json({
        status: false,
        message: "failure",
        data: err
      });
    } else {
      return response.json({
        status: true,
        message: "success",
        data: charge
      });
    }
  });
};

const createCustomer = (request, response) => {
  stripe.customers.create(request.body, (err, customer) => {
    if (err) {
      response.json({
        status: false,
        message: "failure",
        data: err
      });
    } else {
      return response.json({
        status: true,
        message: "success",
        data: customer
      });
    }
  });
};

const refundCharge = (request, response) => {
  stripe.refunds.create(request.body, (err, refund) => {
    if (err) {
      response.json({
        status: false,
        message: "failure",
        data: err
      });
    } else {
      return response.json({
        status: true,
        message: "success",
        data: refund
      });
    }
  });
};
module.exports = {
  createCustomer,
  createCharge,
  refundCharge
};
