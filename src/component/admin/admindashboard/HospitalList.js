import React, { Component } from "react";
import { connect } from "react-redux";
import { getHospitalList, viewDetails,approveDoctor } from "../../../service/admin/Patient/action";
import { filter as _filter } from 'lodash';
class HospitalList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            patientList: [],
            hospitalList: [],
            showTab: "List",
            nameSearch:""
        }
    }
    componentWillMount() {
        this.props.getHospitalList();
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.hospitalList) {
            this.setState({ hospitalList: nextProps.hospitalList.hospitalList ,mainHospitalList : nextProps.hospitalList.hospitalList})
        }

    }
    viewHospital = (id) => {

        const info = {
            uid: id
        }
        this.setState({ showTab: "Viewdata" })
        this.props.viewDetails(info);
    }
    handleNameSearch = (e) => {
        e.preventDefault();
        const { mainHospitalList } = this.state;

        this.setState({ [e.target.name]: e.target.value })
        if (!e.target.value) {
            this.setState({ hospitalList: mainHospitalList })
        }
    }
    handleSearch = (e) => {
        e.preventDefault();
        var result = [];
        const { nameSearch, mainHospitalList } = this.state;
        result = _filter(mainHospitalList, function (list) {

            if (list.firstName != "" || list.firstName != undefined) {
                return list.firstName.includes(nameSearch);
            }

        })
        this.setState({ hospitalList: result })
    }
    handleDoctorApproval = (e, approvedStatus) => {
        const status = {
            uid: e.id,
            approval: approvedStatus,
            userName:"Hospital"
        }
        this.props.approveDoctor(status);
    }
    render() {
        const { hospitalList, showTab } = this.state;
        const { viewPatientDetails } = this.props;
        return (
            <div className="mainMenuSide" id="main-content">
                <div className="wrapper">
                    <div className="container-fluid">
                        <div className="body-content">
                        <div class="search-section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                                            <div class="search-doctor">
                                                <input type="text" placeholder="Hospital Name" name="nameSearch" onChange={this.handleNameSearch} />
                                                <i class="fas fa-user search-icon "></i>
                                            </div>
                                        </div>
                                        <div class="search-doctor-btn col-lg-2 col-12">
                                            <button class="searchDocBtn" disabled="" onClick={this.handleSearch}>Search</button>
                                        </div>
                                    </div></div></div>
                            {showTab == "List" && <div className="search-result-section">
                                {hospitalList ? hospitalList.map((each, i) => (
                                    <div key={i} className="search-card2">
                                        <img src="https://images.pexels.com/photos/1239291/pexels-photo-1239291.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=650&w=940" />
                                        <div className="doc-title">
                                            <p className="title-name"> {each.firstName} </p>
                                            <p> {each.mobileNo}</p>

                                        </div>
                                        <div className="patientView">
                                            <button onClick={() => this.viewHospital(each.id)}><i className="far fa-eye"></i></button>
                                            {each.approvedStatus == "pending" && <button className="approveBtn" onClick={(e) => this.handleDoctorApproval(each, "approved")}><i className="fas fa-check"></i></button>}
                                            {each.approvedStatus == "approved" && <button className="RejectBtn" onClick={() => this.handleDoctorApproval(each, "pending")}><i class="fas fa-times"></i></button>}
                                        </div>
                                    </div>)) : <div className="search-card2"><div className="doc-title">
                                        <p className="title-name">No Hospital list </p>


                                    </div> </div>}
                            </div>}

                            {/* viewpatient list */}
                            {showTab == "Viewdata" && <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="doSection card">
                                        <div class="row">

                                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                                <div class="doctorImage">
                                                    <img src="https://images.pexels.com/photos/1239291/pexels-photo-1239291.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=650&w=940" /></div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">

                                                <div class="doc-title">
                                                    <h6 class="title-name"> {viewPatientDetails && viewPatientDetails.firstName} ( {viewPatientDetails && viewPatientDetails.gender})</h6>

                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In suscipit laborum dolores veniam! Accusantium nostrum molestiae doloremque, cupiditate quaerat eveniet.</p>
                                                    <p><span class="title-name clr10022">Phone Number</span></p>
                                                    <p>{viewPatientDetails && viewPatientDetails.mobileNo}</p>
                                                    <p><span class="title-name clr10022">Address</span></p>
                                                    <p>
                                                        {viewPatientDetails && viewPatientDetails.address}

                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            }
                        </div>
                    </div>
                </div>
                <div class="wrapper">
                    <div class="container-fluid">
                        <div class="body-content">

                        </div>

                    </div>
                </div>

            </div>
        )


    }
}
const mapStateToProps = state => ({
    hospitalList: state.adminPatientReducer.hospitalList,
    viewPatientDetails: state.adminPatientReducer.viewPatientDetails
})
const mapDispatchToProps = dispatch => ({
    getHospitalList: () => dispatch(getHospitalList()),
    viewDetails: (info) => dispatch(viewDetails(info)),
    approveDoctor: (status) => dispatch(approveDoctor(status))


})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HospitalList);