import React, { Component } from "react";
import { createDoctorSchedule, insertDoctorInfo, listScheduleInfo, deleteSchedule, updateDoctorSchedule } from "../../service/doctor/action";
import { connect } from "react-redux";
import DatePicker from "react-datepicker";
import moment from 'moment'
import "react-datepicker/dist/react-datepicker.css";
import MultipleDatePicker from 'react-multiple-datepicker';
import { filter as _filter } from 'lodash';
import { onlyNumbers, onlyAlphabets, onlyDate } from "../patient/personal/KeyValidation";


class ScheduleList extends Component {
    state = {
        scheduleId: "",
        doctorId: "",
        modalPop1: false,
        scheduleLists: [],
        mainScheduleList:[],
        addSchedulepop: false,
        deleteproductpop: false,
        startTime: "",
        endTime: "",
        meetingType: "video call",
        appointmentDate: "",
        status: "A",
        editdate: "",
        searchdate:moment().format("YYYY-MM-DD"),
        errors: {
            appointmentDate: "",
            startTime: "",
            endTime: "",
            meetingType: "",
            status: "",
        },
        appointmentDateValid: false,
        startTimeValid: false,
        endTimeValid: false,
        endTimeValid: false,
        meetingTypeValid: false,
        statusValid: false,
        formValid: false
    }
    componentWillMount = () => {
        const { userInfo } = this.props.loginDetails;
        // console.log("loginDetails",this.props.loginDetails)
        // const scheduleinfo = {
        //     "uid": userInfo ? userInfo.userId : ""
        // }
        const scheduleinfo = {
            uid: userInfo.userId
        }
        this.props.listScheduleInfo(scheduleinfo);
    }
    componentDidMount = () => {
        const date = new Date().getDate();
        this.setState({
            date: date
        });
        if (!navigator.onLine) {

            this.setState({ scheduleLists: JSON.parse(localStorage.getItem('scheduleLists')) });

        } else {
            const { loginDetails } = this.props;
            const { userInfo } = loginDetails;

            const scheduleinfo = {
                "uid": userInfo.userId
            }
            this.props.listScheduleInfo(scheduleinfo);

        }


    }
    componentWillReceiveProps = (nextProps) => {
        console.log("nextprops",nextProps)
        if (
            nextProps.listscheduleDetails &&
            nextProps.listscheduleDetails.scheduleinfo != null &&
            nextProps.listscheduleDetails.scheduleinfo != 0
          ){
            const { scheduleinfo } = nextProps.listscheduleDetails;
    
            this.setState({ scheduleLists: scheduleinfo ,mainScheduleList:scheduleinfo },()=>this.searchAvailability())
          }
        // if (this.props.listscheduleDetails != nextProps.listscheduleDetails) {
        //     if (nextProps.listscheduleDetails && nextProps.listscheduleDetails.scheduleinfo) {
                
        //         // this.searchAvailability();
    
        //     }
        // }
      

    }
    modalPopupOpen = (event) => {

        this.setState({
            modalPop1: true,
            editdate: event.appointmentDate,
            status: event.status,
            startTime: event.startTime,
            endTime: event.endTime,
            meetingType: event.meetingType,
            scheduleId: event.scheduleId
        });
    }
    modalPopUpClose = () => {
        this.setState({
            modalPop1: false,
            addSchedulepop: false,
            deleteproductpop: false,
            appointmentDate: "",
            startTime: "",
            endTime: "",
            status: "",
            meetingType: "",
            appointment: []

        });


    }
    addScheduleopen = () => {
        this.setState({ addSchedulepop: true })
    }
    deleteScheduleopen = (event) => {
        this.setState({ deleteproductpop: true })
    }
    validateField(fieldName, value) {
        const { errors, meetingTypeValid,
            statusValid, appointmentDateValid, startTimeValid, endTimeValid, appointmentDate, startTime, endTime ,meetingType,status} = this.state;

        let appointmentTest = appointmentDateValid;
        let startTimeTest = startTimeValid;
        let endTimeTest = endTimeValid;
        let fieldValidationErrors = errors;
        let meetingTypeTest = meetingTypeValid;
        let statusTest = statusValid;


        switch (fieldName) {
            case "appointmentDate":
                appointmentTest = appointmentDate.length > 0;
                fieldValidationErrors.appointmentDate = appointmentTest ? "" : "Please Select Valid appontmentdate"
                break;
            case "startTime":
                startTimeTest = startTime.length > 0;
                fieldValidationErrors.startTime = startTimeTest ? "" : "Please Select Valid Starttime"
                break;
            case "endTime":
                endTimeTest = endTime.length > 0;
                fieldValidationErrors.endTime = endTimeTest ? "" : "Please Select Valid Endtime"
                break;
            case "meetingType":
                meetingTypeTest = meetingType.length > 0;
                fieldValidationErrors.meetingType = meetingTypeTest ? "" : "Please Select Valid meetingType"
                break;
            case "status":
                statusTest = status.length > 0;
                fieldValidationErrors.status = statusTest ? "" : "Please Select Valid status"
                break;
        }
        this.setState({
            errors: fieldValidationErrors,
            appointmentDateValid: appointmentTest,
            startTimeValid: startTimeTest,
            endTimeValid: endTimeTest,
            meetingTypeValid:meetingTypeTest,
            statusValid:statusTest

        },
            this.validateForm
        )
    }
    validateForm() {
        const { appointmentDateValid, startTimeValid, endTimeValid,meetingTypeValid,statusValid } = this.state;
        this.setState({
            formValid: appointmentDateValid && startTimeValid && endTimeValid&& meetingTypeValid&& statusValid
        })

    }

    handleChange = (e) => {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value }, () => {
            this.validateField(name, value);
        })
    }


    handleSchedule = () => {

        let appointment = [];
        const { appointmentDate, status, startTime, endTime, meetingType } = this.state;
        const { loginDetails } = this.props;
        const { userInfo } = loginDetails;
        if (startTime > endTime||startTime==endTime) {
            const errors = this.state.errors;
            errors.endTime = `Please choose valid End Time`
            this.setState({
                errors: errors
            }, () => {
                setTimeout(() => {
                    errors.endTime = ""
                    this.setState({
                        errors: errors
                    })
                }, 3000)
            })
            return
        }




        appointmentDate.length > 0 && appointmentDate.map((each, i) => {
            appointment.push(moment(each).format("YYYY-MM-DD"))
        })
        if (appointment.includes(moment().format("YYYY-MM-DD"))) {

            const currentDateTm = moment().format("HH:mm")
            const currentstarttime = currentDateTm <= startTime;
            
            const errors = this.state.errors;
            errors.appointmentDate = `Please choose valid startTime ${moment().format("YYYY-MM-DD")}`

            !currentstarttime && this.setState({
                errors: errors
            }, () => {
                setTimeout(() => {
                    errors.appointmentDate = ""
                    this.setState({
                        errors: errors
                    })
                }, 3000)
            })
            if (!currentstarttime) {
                return
            }
            // !currentstarttime && return 

        }


        const allListSchedulesArray = this.props.listscheduleDetails && this.props.listscheduleDetails.scheduleinfo;
        let validDate = true;
        // allListSchedulesArray && allListSchedulesArray.map((data)=>{ 
        // console.log("allListSchedulesArray",allListSchedulesArray);

        (allListSchedulesArray && allListSchedulesArray.length > 0) && allListSchedulesArray.map((data) => {

            if (appointment.includes(data.appointmentDate)) {

                const currentStartTime = moment(startTime, "HH:mm ");
                const startTimeMoment = moment(data.startTime, "HH:mm ");
                const endTimeMoment = moment(data.endTime, "HH:mm");
                const endTimeMomentCurrent = moment(endTime, "HH:mm");
                const amIBetween = currentStartTime.isBetween(startTimeMoment, endTimeMoment);
                const amIBetweenEnd = endTimeMomentCurrent.isBetween(startTimeMoment, endTimeMoment);
            
                // console.log("printing end time",amIBetween,amIBetweenEnd)
              
                if (amIBetween||amIBetweenEnd) {
                    const errors = this.state.errors;
                    errors.appointmentDate = `Please choose different timing for ${data.appointmentDate}`
                    validDate = false
                    this.setState({
                        errors: errors
                    }, () => {
                        setTimeout(() => {
                            errors.appointmentDate = ""
                            this.setState({
                                errors: errors
                            })
                        }, 3000)
                    })
                }
                
                // else if (equal) {
                //     const errors = this.state.errors;
                //     errors.appointmentDate = ` Choose different timing for ${data.appointmentDate}`
                //     validDate = false
                //     this.setState({
                //         errors: errors
                //     }, () => {
                //         setTimeout(() => {
                //             errors.appointmentDate = ""
                //             this.setState({
                //                 errors: errors
                //             })
                //         }, 3000)
                //     })
                // }
                
                // else if (endTime) {
                //     const errors = this.state.errors;
                //     errors.appointmentDate = ` Choose different timing for ${data.appointmentDate}`
                //     validDate = false
                //     this.setState({
                //         errors: errors
                //     }, () => {
                //         setTimeout(() => {
                //             errors.appointmentDate = ""
                //             this.setState({
                //                 errors: errors
                //             })
                //         }, 3000)
                //     })
                // }
            }

        });

        if (validDate) {
            const doctorSchedule = {
                doctorId: userInfo.userId,
                startTime: startTime,
                endTime: endTime,
                appointmentDate: appointment,
                status: status,
                meetingType: meetingType
            }
            this.props.createDoctorSchedule(doctorSchedule);

            this.setState({
               
                startTime: "",
                endTime: "",
                appointmentDate: "",
                status: "",
                meetingType: "",
                // appointment: [], 
                addSchedulepop: false,
            },);
        //     const scheduleinfo = {
        //         "uid": userInfo ? userInfo.userId : ""
        //     }
        //     this.props.listScheduleInfo(scheduleinfo);
        }


    }

    //updateSchedule
    updateSchedule = () => {
       let appointment=[];
        const { status, startTime, endTime, meetingType, scheduleId, editdate } = this.state;
        const { loginDetails } = this.props;
        const { userInfo } = loginDetails;
        if (startTime > endTime||startTime==endTime) {
            const errors = this.state.errors;
            errors.endTime = `Please choose valid End Time`
            this.setState({
                errors: errors
            }, () => {
                setTimeout(() => {
                    errors.endTime = ""
                    this.setState({
                        errors: errors
                    })
                }, 3000)
            })
            return
        }
        
        if (editdate.includes(moment().format("YYYY-MM-DD"))) {

            const currentDateTm = moment().format("HH:mm")
            const currentstarttime = currentDateTm <= startTime;
            const errors = this.state.errors;
            errors.editdate = `Please choose valid startTime ${moment().format("YYYY-MM-DD")}`

            !currentstarttime && this.setState({
                errors: errors
            }, () => {
                setTimeout(() => {
                    errors.editdate = ""
                    this.setState({
                        errors: errors
                    })
                }, 3000)
            })
            if (!currentstarttime) {
                return
            }
            

        }


        const allListSchedulesArray = this.props.listscheduleDetails && this.props.listscheduleDetails.scheduleinfo;
        let validDate = true;
     

        (allListSchedulesArray && allListSchedulesArray.length > 0) && allListSchedulesArray.map((data) => {

            if (appointment.includes(data.editdate)) {

                const currentStartTime = moment(startTime, "HH:mm ");
                const startTimeMoment = moment(data.startTime, "HH:mm ");
                const endTimeMoment = moment(data.endTime, "HH:mm");
                const endTimeMomentCurrent = moment(endTime, "HH:mm");
                const amIBetween = currentStartTime.isBetween(startTimeMoment, endTimeMoment);
                const amIBetweenEnd = endTimeMomentCurrent.isBetween(startTimeMoment, endTimeMoment);
                //const equal = currentStartTime.isSame(startTimeMoment, endTimeMoment);
                // const ENDTIME = data.endTime;
                console.log("printing end time",amIBetween,amIBetweenEnd)
              
                if (amIBetween||amIBetweenEnd) {
                    const errors = this.state.errors;
                    errors.editdate = `Please choose different timing for ${data.editdate}`
                    validDate = false
                    this.setState({
                        errors: errors
                    }, () => {
                        setTimeout(() => {
                            errors.editdate = ""
                            this.setState({
                                errors: errors
                            })
                        }, 3000)
                    })
                }
                
               
            }

        });
        if(validDate){
            const updateSchedule = {
                uid: userInfo.userId ,
                scheduleId: scheduleId,
                startTime: startTime,
                endTime: endTime,
                appointmentDate: editdate,
                status: status,
                meetingType: meetingType
            }
    
            this.props.updateDoctorSchedule(updateSchedule);
            this.setState({ modalPop1: false });
        }
        
       
    }

    handleSelect = (day) => {
        this.setState({ appointmentDate: day }, () => {
            this.validateField("appointmentDate", day);
        })
    }
    searchAvailability = () => {
        const { searchdate, scheduleLists,mainScheduleList } = this.state;
        var result = [];
        result = _filter(mainScheduleList, function (list) {
            return list.appointmentDate === searchdate;
        })
        this.setState({ scheduleLists: result })
    }

    render() {
        const { startTime, endTime, appointmentDate, date, status, meetingType, editdate, searchdate, scheduleLists, errors, formValid ,mainScheduleList} = this.state;
        const { loginDetails, listscheduleDetails } = this.props;
        // console.log("currentTime",(new Date).getTime())
        return (
            <React.Fragment>
                <div class="mainMenuSide" id="main-content">
                    <div class="wrapper">
                        <div class="container-fluid">
                            <div class="body-content">
                                <div class="tomCard">
                                    <div class="tomCardHead">
                                        <h5 class="float-left">My Availability</h5>
                                    </div>
                                    <div class="tab-menu-content tomCardBody">
                                        <div class="tab-menu-title">
                                            <ul>
                                                <li class="menu1 active"><a href="JavaScript:Void(0);">
                                                    <p>List of Availability  </p>
                                                </a></li>
                                            </ul>
                                        </div>
                                        <div class="tab-menu-content-section">
                                            <div id="content-1" style={{ display: "block" }}>
                                                <div class="row">
                                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-3 col-sm-12 col-12">
                                                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                                                        <div class="searchAvailablity mobile-width-100">
                                                            <input type="date" class="searchInputDate" name="searchdate" value={searchdate} onChange={this.handleChange} onKeyPress={onlyDate} />
                                                            <div class="calenderIcon"><i class="fas fa-calendar-alt"></i></div>
                                                           
                                                        </div>
                                                        <div>
                                                        <button type="button" class="commonBtn2 mobile-width-100" onClick={this.searchAvailability}> <i class="fa fa-search docPlus"></i> Search</button>
                                                        </div>
                                                        <div>
                                                        <button type="button" class="commonBtn2 mobile-width-100" onClick={()=> this.setState({scheduleLists:mainScheduleList,searchdate:""})}> <i class="fa fa-eye docPlus"></i> Show all list</button>
                                                        </div>
                                                        </div>
                                                    </div>
                                                   
                                                   
                                                    <div class="col-lg-6 col-md-7 col-sm-12 col-12">
                                                        <button type="button" class="commonBtn2 float-right mb-2 mobile-width-100" data-toggle="modal" data-target="#myModal1" onClick={() => this.addScheduleopen()}> <i class="fa fa-plus docPlus"></i> Add New Entry</button>
                                                    </div>
                                                </div>
                                                <div class="row mt-3 mb-3">
                                                    {
                                                        scheduleLists.length > 0 ? scheduleLists.map((each, i) => (
                                                            <div key={i} class="col-lg-3 col-md-3 col-sm-4 col-6 padRig">
                                                                <div class="availablityCard">
                                                                    <div class="cartType">
                                                                        <p class="video">{each.meetingType}</p>
                                                                    </div>
                                                                    <div class="cardTime">
                                                                        <p>{each.appointmentDate} &nbsp;&nbsp;({each.startTime} - {each.endTime})</p>
                                                                    </div>
                                                                    <div class="cardAction">
                                                                        <div class="availableEdit">
                                                                            <a href="javascript:void(0)" title="Edit" onClick={() => this.modalPopupOpen(each)}><i class="far fa-edit"></i> <span>Edit</span></a>
                                                                        </div>
                                                                        {each.status == "A" && <div class="availableStatus">
                                                                            <a href="" title="Available">  <i class="fas fa-check"></i> <span>Available</span></a>

                                                                        </div>}
                                                                        {each.status == "NA" && <div class="availableDelete">
                                                                            <a href="javascript:void(0)" title="Not Avaliable"><i class="fas fa-times"></i> <span>Not Avaliable</span></a>
                                                                        </div>}

                                                                        {/* <div class="availableDelete">
                                                                            <a href="javascript:void(0)" title="Delete" onClick={(e) => this.deleteScheduleopen(each)}><i class="far fa-trash-alt"></i></a>
                                                                        </div> */}

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        )) : <div>No availability </div>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Add pop */}
                <div className={this.state.addSchedulepop ? "modal show d-block" : "modal"} >
                    <div class="modal-dialog">
                        <div class="modal-content modalPopUp">
                            <div class="modal-header borderNone">
                                <h5 class="modal-title">Add  Availablity</h5>
                                <button type="button" class="popupClose" data-dismiss="modal" onClick={this.modalPopUpClose}><i class="fas fa-times"></i></button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group position-relative datePickerinput">
                                                <label class="commonLabel d-block">Date <sup>*</sup></label>
                                                {/* <input type="text" class="form-control"  name="appointmentDate" /> */}
                                                <MultipleDatePicker minDate={new Date()}
                                                    onSubmit={dates => this.handleSelect(dates)} />
                                                <div style={{ color: "red" }}>{errors.appointmentDate}</div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Start Time <sup>*</sup></label>
                                                <input type="time" class="form-control" name="startTime" onChange={this.handleChange} value={startTime} />
                                                <div style={{ color: "red" }}>{errors.startTime}</div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">End Time <sup>*</sup></label>
                                                <input type="time" class="form-control" name="endTime" onChange={this.handleChange} value={endTime} />
                                                <div style={{ color: "red" }}>{errors.endTime}</div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Type <sup>*</sup></label>
                                                <select class="form-control" onChange={this.handleChange} name="meetingType">
                                                    <option value="">----Please Select Type----</option>
                                                    <option value="video call">Video Call</option>
                                                    <option value="office meeting">Office Meeting</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Availablity <sup>*</sup></label>
                                                <select class="form-control" name="status" onChange={this.handleChange}>
                                                    <option value="">----Please Select Availablity----</option>
                                                    <option value="A">Available</option>
                                                    <option value="NA">Not Available</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <button type="button" class="commonBtn float-right" disabled={!formValid} onClick={this.handleSchedule}>Create</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Edit pop */}
                <div className={this.state.modalPop1 ? "modal show d-block" : "modal"} id="edit_btn">
                    <div class="modal-dialog">
                        <div class="modal-content modalPopUp">
                            <div class="modal-header borderNone">
                                <h5 class="modal-title">Add  Availablity</h5>
                                <button type="button" class="popupClose" data-dismiss="modal" onClick={this.modalPopUpClose}><i class="fas fa-times"></i></button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group position-relative datePickerinput">
                                                <label class="commonLabel d-block">Date</label>
                                                <input type="date" value={editdate} onChange={this.handleChange} min={moment().format("YYYY-MM-DD")} name="editdate" onKeyPress={onlyNumbers} minDate={date} />
                                            </div>
                                            <div style={{ color: "red" }}>{errors.editdate}</div>

                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Start Time</label>
                                                <input type="time" class="form-control" name="startTime" onChange={this.handleChange} value={startTime} />
                                            </div>
                                            <div style={{ color: "red" }}>{errors.startTime}</div>

                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">End Time</label>
                                                <input type="time" class="form-control" name="endTime" onChange={this.handleChange} value={endTime} />
                                            </div>
                                            <div style={{ color: "red" }}>{errors.endTime}</div>

                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Type</label>
                                                <select class="form-control" onChange={this.handleChange} value={meetingType} name="meetingType">
                                                    <option value="video call">Video Call</option>
                                                    <option value="office meeting">Office Meeting</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Availablity</label>
                                                <select class="form-control" name="status" value={status} onChange={this.handleChange}>
                                                    <option value="A">Available</option>
                                                    <option value="NA">Not Available</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <button type="button" class="commonBtn float-right" onClick={this.updateSchedule}>Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {/* deletepopup */}
                <div className={this.state.deleteproductpop ? "modal show d-block" : "modal"} id="edit_btn">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Comfirmation Message</h4>
                                <button type="button" className="close" data-dismiss="modal" onClick={this.modalPopUpClose}>&times;</button>
                            </div>
                            <div className="modal-body">
                                <p>Are u sure,You want to delete this Item</p>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn commonBtn" onClick={this.delete} >Yes</button>
                                <button type="button" className="btn commonBtn" onClick={this.modalPopUpClose} >No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment >
        )
    }
}
const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails,
    listscheduleDetails: state.doctorReducer.listscheduleDetails,
    deletescheduleDetails: state.doctorReducer.deletescheduleDetails,
    updatescheduleDetails: state.doctorReducer.updatescheduleDetails,
});

const mapDispatchToProps = dispatch => ({
    createDoctorSchedule: (doctorSchedule) => dispatch(createDoctorSchedule(doctorSchedule)),
    insertDoctorInfo: (doctorinfo) => dispatch(insertDoctorInfo(doctorinfo)),
    listScheduleInfo: (scheduleinfo) => dispatch(listScheduleInfo(scheduleinfo)),
    deleteSchedule: (deleteinfo) => dispatch(deleteSchedule(deleteinfo)),
    updateDoctorSchedule: (updateSchedule) => dispatch(updateDoctorSchedule(updateSchedule)),
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ScheduleList);