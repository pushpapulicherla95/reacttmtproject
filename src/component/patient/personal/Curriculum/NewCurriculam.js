import React, { Component } from "react";
import { getUploadFiles, handleFileDelete } from "../../../../service/upload/action";
import { getPersonaldata } from "../../../../service/healthrecord/action";
import { connect } from "react-redux";
import validator from "validator";

import _ from 'lodash'

import {
  addcurriculumData,
  addeducationData,
  addpreviousjobData,
  deleteeducationData,
  updateeducationData,
  updatepreviousjobData,
  deletepreviousjobData,
  addlanguageData,
  updatelanguageData,
  deletelanguageData
} from "../../../../service/patient/action";
import URL from "../../../../asset/configUrl";
import { toastr } from "react-redux-toastr";
import { awsPersonalFiles } from "../../../../service/common/action";

import axios from "axios";
import { onlyDate, onlyNumbers } from "../KeyValidation";
class NewCurriculum extends Component {
  constructor(props) {
    super(props);
    // this.uid = userId;
    this.state = {
      index: 1,
      tabone: true,
      tabtwo: false,
      tabthree: false,
      tabfour: false,
      tabfive: false,
      tabsix: false,
      name: "",
      date: "",
      drivingLicenceCar: "",
      drivingLicenceTruck: "",
      qualifications: "",
      preferredplace: "",
      preferredjob: "",
      earlieststart: "",
      relationship: "",
      children: "",
      hobby: "",
      Yes: "",
      No: "",
      patientFindingListData: [],
      education: [],
      previousJob: [],
      languageSkill: [],
      addallergies: false,
      addjob: false,
      endejob: "",
      startjob: "",
      jobtype: "",
      editallergies: false,
      editDetails: {},
      editjob: false,
      editjobDetails: {},
      addlanguage: false,
      languagelevel: "",
      language: "",
      editlanguage: false,
      editlanguageDetails: {},
      familyname: "",
      fristname: "",
      title: "",
      email: "",
      dob: "",
      pob: "",
      addressprivate: "",
      addressjob: "",
      privateno: "",
      workno: "",
      mobileno: "",
      gender: "",
      reletionship: "",
      personalData: [],
      uploadPopup: false,
      starteducation: "",
      endeducation: "",
      name: "",
      fileName: "",
      fileType: "",
      fileURL: "",
      errors: {
        endjob: "",
        startjob: "",
        starteducation: "",
        endeducation: "",
      },
      endjobValid: false,
      startjobValid: false,
      starteducationValid: false,
      endeducationValid: false,
      formValid: false,
      Valid: false

    };
  }


  handleAwsFiles = value => {
    window.open("https://data.tomatomedical.com/patientpersonal/" + value, '_blank');

  };
  ValidationField = () => {
    const {
      title, language, name } = this.state
    this.setState({
      Valid:
        title ||
        language ||
        name



    });

  }
  ValidationField(fieldName, value) {
    const { errors, starteducationValid, endeducationValid, endjobValid, startjobValid, starteducation, startjob
    } = this.state;

    let starteducationvalid = starteducationValid;
    let endeducationvalid = endeducationValid;
    let endjobvalid = endjobValid;
    let startjobvalid = startjobValid;
    let fieldValidationErrors = errors;
    switch (fieldName) {
      case "starteducation":
        starteducationvalid = value.length !== 0;
        fieldValidationErrors.starteducation = starteducationvalid
          ? ""
          : "Start education date should be less than end education date.";
        break;
      case "endeducation":
        endeducationvalid = validator.isAfter(value, starteducation);
        fieldValidationErrors.endeducation = endeducationvalid
          ? ""
          : "End education date should be grater than start education date.";
        break;
      case "startjob":
        startjobvalid = value.length !== 0;
        fieldValidationErrors.startjob = startjobvalid
          ? ""
          : "Start job date should be less than end job date.";
        break;
      case "endjob":
        endjobvalid = validator.isAfter(value, startjob);
        fieldValidationErrors.endjob = endjobvalid
          ? ""
          : "End job date should be grater than start job date.";
        break;






    }

    this.setState({
      errors: fieldValidationErrors,
      starteducationValid: starteducationvalid,
      endeducationValid: endeducationvalid,
      startjobValid: startjobvalid,
      endjobValid: endjobvalid

    },
      this.validateForm
    )
  }
  validateForm() {
    const { starteducationValid, endeducationValid, startjobValid, endjobValid } = this.state;
    this.setState({
      formValid:
        starteducationValid &&
        endeducationValid &&
        startjobValid &&
        endjobValid


    })
  }
  handleChangePopup = e => {
    var reader = new FileReader();
    var file = e.target.files[0];
    let name = file.name.split('.')[0]
    let type = "." + file.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileURL: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(file);
  };
  popupSubmit = () => {
    var phrase = this.state.fileURL;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);
    var { userInfo } = this.props.loginDetails;
    const payload = {
      uid: userInfo.userId,
      z: match[1],
      fileName: this.state.fileName,
      fileType: this.state.fileType,
      name: this.state.name,
      category: "curriculum"
    };
    this.setState({ uploadPopup: false });

    axios
      .post(URL.FILECURRICULAM_UPLOAD, payload)
      .then(response => {
        response.data.status === "success"
          ? toastr.success("Message", response.data.message)
          : toastr.error("Message", response.data.message);
        if (response.data.status === "success") {
          var { userInfo } = this.props.loginDetails;
          const uploadFiles = {
            uid: userInfo.userId,
            category: "curriculum"
          };
          this.props.getUploadFiles(uploadFiles);
          this.setState({ uploadPopup: false });
          this.setState({
            name: "",
            fileName: "",
            fileType: "",
            fileURL: ""
          });
        }
      })
      .catch(error => {
        // this.setState({ uploadPopup: true });
      });
  };
  tabOne = () => {
    this.setState({
      tabone: true,
      tabtwo: false,
      tabthree: false,
      tabfour: false,
      tabfive: false,
      tabsix: false
    });
  };
  tabTwo = () => {
    this.setState({
      tabone: false,
      tabtwo: true,
      tabthree: false,
      tabfour: false,
      tabfive: false,
      tabsix: false
    });
  };
  tabThree = () => {
    this.setState({
      tabone: false,
      tabtwo: false,
      tabthree: true,
      tabfour: false,
      tabfive: false,
      tabsix: false
    });
  };
  tabFour = () => {
    this.setState({
      tabone: false,
      tabtwo: false,
      tabthree: false,
      tabfour: true,
      tabfive: false,
      tabsix: false
    });
  };
  tabFive = () => {
    this.setState({
      tabone: false,
      tabtwo: false,
      tabthree: false,
      tabfour: false,
      tabfive: true,
      tabsix: false
    });
  };
  tabSix = () => {
    this.setState({
      tabone: false,
      tabtwo: false,
      tabthree: false,
      tabfour: false,
      tabfive: false,
      tabsix: true
    });
  };
  // handleChange = (e, index) => {
  //   this.setState({ [e.target.name]: e.target.value });
  // };
  handleChange = (e) => {
    e.preventDefault();
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value }, () => {
      this.ValidationField(name, value);
    });
  };
  modalPopUpClose = () => {
    this.setState({
      addallergies: false,
      addjob: false,
      editallergies: false,
      editjob: false,
      addlanguage: false,
      editlanguage: false
    });
  };

  handleAllergy = () => {
    const {
      name,
      date,
      drivingLicenceCar,
      drivingLicenceTruck,
      qualifications,
      preferredplace,
      preferredjob,
      earlieststart,
      relationship,
      children,
      hobby
    } = this.state;
    const { userInfo } = this.props.loginDetails;
    const addcurriculum = {
      userId: userInfo.userId,
      curriculumGeneral: {
        name: name,
        date: date,
        drivingLicenceCar: drivingLicenceCar,
        drivingLicenceTruck: drivingLicenceTruck,
        qualifications: qualifications,
        preferredplace: preferredplace,
        preferredjob: preferredjob,
        earlieststart: earlieststart,
        relationship: relationship,
        children: children,
        hobby: hobby
      }
    };

    this.props.addcurriculumData(addcurriculum);
  };
  deleteEducation = e => {
    const { userInfo } = this.props.loginDetails;

    const deleteeducation = {
      uid: userInfo.userId,

      curriculumEducation: [
        {
          title: e.title,
          completed: e.completed,
          address: e.address,
          country: e.country,
          homepage: e.homepage,
          starteducation: e.starteducation,
          endeducation: e.endeducation,
          degree: e.degree,
          field: e.field,
          name: e.name
        }
      ]
    };
    this.props.deleteeducationData(deleteeducation);
  };
  setGender(event) {
    this.setState({ [event.target.name]: event.target.value });
  }
  componentWillMount() {
    const { userInfo } = this.props.loginDetails;

    const listpersonal = {
      uid: userInfo.userId
    };
    this.props.getPersonaldata(listpersonal);
    //  var { userInfo } = this.props.loginDetails;
    const uploadFiles = { uid: userInfo.userId, category: "curriculum" };
    this.props.getUploadFiles(uploadFiles);
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.getpersonalDetails &&
      nextProps.getpersonalDetails.cirrculamEducation != null &&
      nextProps.getpersonalDetails.cirrculamEducation != 0
    ) {
      const Education = nextProps.getpersonalDetails.cirrculamEducation;

      let education = JSON.parse(Education);
      this.setState({ education: education });
    }

    if (
      nextProps.getpersonalDetails &&
      nextProps.getpersonalDetails.personal != null &&
      nextProps.getpersonalDetails.personal != 0
    ) {
      const listpersonal = nextProps.getpersonalDetails.personal;
      let personalData = JSON.parse(listpersonal);
      this.setState({ personalData: personalData });
    }

    if (
      nextProps.getpersonalDetails &&
      nextProps.getpersonalDetails.cirrculamPreviousJob != null &&
      nextProps.getpersonalDetails.cirrculamPreviousJob != 0
    ) {
      const PreviousJob = nextProps.getpersonalDetails.cirrculamPreviousJob;

      let previousJob = JSON.parse(PreviousJob);
      this.setState({ previousJob: previousJob });
    }
    //
    if (
      nextProps.getpersonalDetails &&
      nextProps.getpersonalDetails.cirrculamLanguageSkills != null &&
      nextProps.getpersonalDetails.cirrculamLanguageSkills != 0
    ) {
      const languageSkills =
        nextProps.getpersonalDetails.cirrculamLanguageSkills;

      let languageSkill = JSON.parse(languageSkills);
      this.setState({ languageSkill: languageSkill });
    }
    //

    if (
      nextProps.getpersonalDetails &&
      nextProps.getpersonalDetails.cirrculamGeneral != null &&
      nextProps.getpersonalDetails.cirrculamGeneral != 0
    ) {
      const listpersonal = nextProps.getpersonalDetails.cirrculamGeneral;

      let patientFindingListData = JSON.parse(listpersonal);

      this.setState({
        name: patientFindingListData.name,
        drivingLicenceCar: patientFindingListData.drivingLicenceCar,
        date: patientFindingListData.date,
        drivingLicenceTruck: patientFindingListData.drivingLicenceTruck,
        qualifications: patientFindingListData.qualifications,
        preferredplace: patientFindingListData.preferredplace,
        preferredjob: patientFindingListData.preferredjob,
        earlieststart: patientFindingListData.earlieststart,
        relationship: patientFindingListData.relationship,
        children: patientFindingListData.children,
        hobby: patientFindingListData.hobby
      });
    }
  }

  addallergiespop = () => {
    document.body.classList.add("modal-open");
    this.setState({ addallergies: true, Valid: false }, () => {
      this.emptystate();
    });
  };
  addjobpop = () => {
    document.body.classList.add("modal-open");
    this.setState({ addjob: true, Valid: false }, () => {
      this.emptystate();
    });
  };
  addlanguagepop = () => {
    document.body.classList.add("modal-open");
    this.setState({ addlanguage: true, Valid: false }, () => {
      this.emptystate();
    });
  };
  emptystate = () => {
    this.setState({
      name: "",
      title: "",
      date: "",
      drivingLicenceCar: "",
      drivingLicenceTruck: "",
      qualifications: "",
      preferredplace: "",
      preferredjob: "",
      earlieststart: "",
      relationship: "",
      children: "",
      hobby: "",
      Yes: "",
      No: "",
      endejob: "",
      startjob: "",
      jobtype: "",
      languagelevel: "",
      language: ""
    });
  };
  handleEducation = () => {
    const {
      title,
      completed,
      name,
      address,
      country,
      homepage,
      starteducation,
      endeducation,
      degree,
      field
    } = this.state;
    const { userInfo } = this.props.loginDetails;

    const addeducation = {
      uid: userInfo.userId,
      curriculumEducation: [
        {
          title: title,
          completed: completed,
          address: address,
          country: country,
          homepage: homepage,
          starteducation: starteducation,
          endeducation: endeducation,
          degree: degree,
          field: field,
          name: name
        }
      ]
    };

    this.props.addeducationData(addeducation);
    document.body.classList.remove("modal-open");
    this.setState({
      addallergies: false
    });
  };
  updateAllery = () => {
    const {
      title,
      completed,
      name,
      address,
      country,
      homepage,
      starteducation,
      endeducation,
      degree,
      field
    } = this.state;
    const { userInfo } = this.props.loginDetails;

    const updateeducation = {
      uid: userInfo.userId,
      curriculumEducation: [
        {
          title: title,
          completed: completed,
          address: address,
          country: country,
          homepage: homepage,
          starteducation: starteducation,
          endeducation: endeducation,
          degree: degree,
          field: field,
          name: name
        }
      ]
    };

    this.props.updateeducationData(updateeducation);
    document.body.classList.remove("modal-open");
    this.setState({ editallergies: false });
  };
  handleJob = () => {
    const {
      title,
      name,
      address,
      country,
      homepage,
      startjob,
      endjob,
      jobtype
    } = this.state;
    const { userInfo } = this.props.loginDetails;
    const addpreviousjob = {
      uid: userInfo.userId,
      curriculumPreviousJobs: [
        {
          title: title,
          address: address,
          country: country,
          homepage: homepage,
          startjob: startjob,
          endjob: endjob,
          jobtype: jobtype,
          name: name
        }
      ]
    };

    this.props.addpreviousjobData(addpreviousjob);
    this.setState({
      addjob: false
    });
  };
  modalPopupOpen = e => {
    document.body.classList.add("modal-open");

    this.setState({ editallergies: true, editDetails: e });
    this.setState({
      title: e.title,
      completed: e.completed,
      address: e.address,
      country: e.country,
      homepage: e.homepage,
      starteducation: e.starteducation,
      endeducation: e.endeducation,
      degree: e.degree,
      field: e.field,
      name: e.name
    });
  };
  //
  editjobpopupopen = e => {
    document.body.classList.add("modal-open");

    this.setState({ editjob: true, editjobDetails: e });
    this.setState({
      title: e.title,
      address: e.address,
      country: e.country,
      homepage: e.homepage,
      startjob: e.startjob,
      endjob: e.endjob,
      jobtype: e.jobtype,
      name: e.name
    });
  };
  //
  updateJob = () => {
    const {
      title,
      name,
      address,
      country,
      homepage,
      startjob,
      endjob,
      jobtype
    } = this.state;
    const { userInfo } = this.props.loginDetails;
    const updatepreviousjob = {
      uid: userInfo.userId,
      curriculumPreviousJobs: [
        {
          title: title,
          address: address,
          country: country,
          homepage: homepage,
          startjob: startjob,
          endjob: endjob,
          jobtype: jobtype,
          name: name
        }
      ]
    };
    this.props.updatepreviousjobData(updatepreviousjob);
    document.body.classList.remove("modal-open");
    this.setState({ editjob: false });
  };
  deleteJob = e => {
    // const {
    //   title,
    //   name,
    //   address,
    //   country,
    //   homepage,
    //   startjob,
    //   endejob,
    //   jobtype
    // } = this.state;
    const { userInfo } = this.props.loginDetails;

    const deletepreviousjob = {
      uid: userInfo.userId,
      curriculumPreviousJobs: [
        {
          title: e.title,
          address: e.address,
          country: e.country,
          homepage: e.homepage,
          startjob: e.startjob,
          endjob: e.endjob,
          jobtype: e.jobtype,
          name: e.name
        }
      ]
    };
    this.props.deletepreviousjobData(deletepreviousjob);
  };
  //
  handleLanguage = () => {
    const { language, languagelevel } = this.state;
    const { userInfo } = this.props.loginDetails;

    const addlanguage = {
      uid: userInfo.userId,
      curriculumLanguageSkills: [
        {
          language: language,
          languagelevel: languagelevel
        }
      ]
    };

    this.props.addlanguageData(addlanguage);
    this.setState({
      addlanguage: false
    });
  };
  editlanguagepopupopen = e => {
    document.body.classList.add("modal-open");

    this.setState({ editlanguage: true, editlanguageDetails: e });
    this.setState({
      language: e.language,
      languagelevel: e.languagelevel
    });
  };
  updateLanguage = () => {
    const { language, languagelevel } = this.state;
    const { userInfo } = this.props.loginDetails;

    const updatelanguage = {
      uid: userInfo.userId,
      curriculumLanguageSkills: [
        {
          language: language,
          languagelevel: languagelevel
        }
      ]
    };

    this.props.updatelanguageData(updatelanguage);
    document.body.classList.remove("modal-open");
    this.setState({ editlanguage: false });
  };
  deleteLanguage = e => {
    const { language, languagelevel } = this.state;

    const { userInfo } = this.props.loginDetails;

    const deletelanguage = {
      uid: userInfo.userId,
      curriculumLanguageSkills: [
        {
          language: e.language,
          languagelevel: e.languagelevel
        }
      ]
    };
    this.props.deletelanguageData(deletelanguage);
  };
  render() {
    const {
      tabone,
      errors,
      tabtwo,
      tabthree,
      tabfour,
      tabfive,
      tabsix,
      date,
      drivingLicenceCar,
      drivingLicenceTruck,
      qualifications,
      preferredplace,
      preferredjob,
      earlieststart,
      relationship,
      children,
      hobby,
      index,
      Yes,
      No,
      addallergies,
      title,
      completed,
      name,
      address,
      country,
      homepage,
      starteducation,
      endeducation,
      degree,
      field,
      addjob,
      startjob,
      endjob,
      jobtype,
      editallergies,
      languageSkill,
      education,
      previousJob,
      editjob,
      addlanguage,
      languagelevel,
      language,
      editlanguage,
      familyname,
      personalData, formValid, Valid
    } = this.state;
    const { curriculumEducation } = education;
    const { curriculumPreviousJobs } = previousJob;
    const { curriculumLanguageSkills } = languageSkill;
    console.log("aaaaaaaaaaaaaaaaaaaaaa", curriculumLanguageSkills)
    return (
      <div className="mainMenuSide" id="main-content">
        <div className="wrapper">
          <div className="container-fluid">
            <div className="body-content">
              <div className="tomCard">
                <div className="tomCardHead">
                  <h5 className="float-left">CURRICULUM DATA</h5>
                </div>

                <div className="tab-menu-content">
                  <div class="tab-menu-title">
                    <ul>
                      <li className={this.state.tabone ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabOne}>
                          <p>General data</p>
                        </a>
                      </li>
                      <li className={this.state.tabtwo ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabTwo}>
                          <p>Education</p>
                        </a>
                      </li>
                      <li className={this.state.tabthree ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabThree}>
                          <p>Previous Jobs</p>
                        </a>
                      </li>
                      <li className={this.state.tabfour ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabFour}>
                          <p>Language skills</p>
                        </a>
                      </li>
                      <li className={this.state.tabfive ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabFive}>
                          <p>Attached Files</p>
                        </a>
                      </li>
                      <li className={this.state.tabsix ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabSix}>
                          <p>Preview &amp; Submit</p>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                {tabone && (
                  <div class="tab-menu-content-section">
                    <div
                      id="content-1"
                      className={tabone ? "d-block" : "d-none"}
                    >
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                          <h5>Personal</h5>
                          <hr />
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>
                              Name <sup>*</sup>

                            </label>
                            <input
                              type="text"
                              class="form-control"
                              value={name}
                              name="name"
                              disabled=""
                              onChange={this.handleChange}
                            />
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>
                              Date Of Brith



                            </label>
                            <input
                              type="date"
                              class="form-control"
                              value={date}
                              name="date"
                              disabled=""
                              onChange={this.handleChange}
                              onKeyPress={onlyDate}
                            />
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                          <h5>Other qualificatons</h5>
                          <hr />
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Driving Licence Car</label>
                            <div class="genderInfo">
                              <input
                                type="radio"
                                id={"cloudlink1"}
                                name="drivingLicenceCar"
                                value="Yes"
                                checked={drivingLicenceCar === "Yes"}
                                onChange={this.setGender.bind(this)}
                              />
                              <label htmlFor={"cloudlink1"}>Yes</label>
                            </div>
                            <div class="genderInfo">
                              <input
                                type="radio"
                                id={"emailzip1"}
                                name="drivingLicenceCar"
                                value="No"
                                checked={drivingLicenceCar === "No"}
                                onChange={this.setGender.bind(this)}
                              />
                              <label htmlFor={"emailzip1"}>No</label>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Driving Licence Truck</label>
                            <div class="genderInfo">
                              <input
                                type="radio"
                                id={"cloudlink2"}
                                name="drivingLicenceTruck"
                                value="Yes"
                                checked={drivingLicenceTruck === "Yes"}
                                onChange={this.setGender.bind(this)}
                              />
                              <label htmlFor={"cloudlink2"}>Yes</label>
                            </div>
                            <div class="genderInfo">
                              <input
                                type="radio"
                                id={"emailzip2"}
                                name="drivingLicenceTruck"
                                value="No"
                                checked={drivingLicenceTruck === "No"}
                                onChange={this.setGender.bind(this)}
                              />
                              <label htmlFor={"emailzip2"}>No</label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Other qualifications</label>
                            <div class="genderInfo">
                              <input
                                type="radio"
                                id={"cloudlink3"}
                                name="qualifications"
                                value="Yes"
                                checked={qualifications === "Yes"}
                                onChange={this.setGender.bind(this)}
                              />
                              <label for={"cloudlink3"}>Yes</label>
                            </div>
                            <div class="genderInfo">
                              <input
                                type="radio"
                                id={"emailzip3"}
                                name="qualifications"
                                value="No"
                                checked={qualifications === "No"}
                                onChange={this.setGender.bind(this)}
                              />
                              <label for={"emailzip3"}>No</label>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                          <h5>Desired job</h5>
                          <hr />
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>
                              Preferred place

                            </label>
                            <input
                              type="text"
                              class="form-control"
                              name="preferredplace"
                              value={preferredplace}
                              onChange={this.handleChange}
                            />
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>
                              Preferred job

                            </label>
                            <input
                              type="email"
                              class="form-control"
                              name="preferredjob"
                              value={preferredjob}
                              onChange={this.handleChange}
                            />
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>
                              Earliest start

                            </label>
                            <input
                              type="date"
                              class="form-control"
                              name="earlieststart"
                              value={earlieststart}
                              onChange={this.handleChange}
                              onKeyPress={onlyDate}
                            />
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                          <h5>Family</h5>
                          <hr />
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Relationship</label>
                            <input
                              type="text"
                              class="form-control"
                              name="relationship"
                              value={relationship}
                              disabled=""
                              onChange={this.handleChange}
                            />
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Number of Children</label>
                            <input
                              type="text"
                              class="form-control"
                              value={children}
                              name="children"
                              disabled=""
                              onChange={this.handleChange}
                              onKeyPress={onlyNumbers}
                            />
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                          <h5>Misc</h5>
                          <hr />
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>
                              Hobby

                            </label>
                            <input
                              type="text"
                              class="form-control"
                              value={hobby}
                              name="hobby"
                              onChange={this.handleChange}
                            />
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-12">
                          <button
                            type="button"
                            class="commonBtn float-right"
                            onClick={this.handleAllergy} disabled={!Valid}
                          >
                            Save
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                {tabtwo && (
                  <div id="content-1" className={tabtwo ? "d-block" : "d-none"}>
                    <button
                      type="button"
                      onClick={this.addallergiespop}
                      class="commonBtn2 float-right mb-2"
                      data-toggle="modal"
                      data-target="#addEntry"
                    >
                      <svg
                        class="svg-inline--fa fa-plus fa-w-14"
                        aria-hidden="true"
                        focusable="false"
                        data-prefix="fa"
                        data-icon="plus"
                        role="img"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 448 512"
                        data-fa-i2svg=""
                      >
                        <path
                          fill="currentColor"
                          d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"
                        />
                      </svg>
                      Add new entry
                    </button>
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead class="thead-default">
                          <tr>
                            <th>Title</th>
                            <th>Name of institution</th>
                            <th>Degree</th>

                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {curriculumEducation &&
                            curriculumEducation.length > 0 ? (
                              curriculumEducation.map((each, i) => (
                                <tr key={i}>
                                  <td data-label="Title">{each.title}</td>
                                  <td data-label="Name of institution">
                                    {each.name}
                                  </td>
                                  <td data-label="Degree">{each.degree}</td>
                                  <td data-label="Action">
                                    <a
                                      title="Edit"
                                      onClick={e => this.modalPopupOpen(each)}
                                      class="editBtn"
                                      data-toggle="modal"
                                      data-target="#addEntry"
                                    >
                                      <svg
                                        class="svg-inline--fa fa-edit fa-w-18"
                                        aria-hidden="true"
                                        focusable="false"
                                        data-prefix="fa"
                                        data-icon="edit"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512"
                                        data-fa-i2svg=""
                                      >
                                        <path
                                          fill="currentColor"
                                          d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"
                                        />
                                      </svg>
                                    </a>
                                    <a
                                      title="Delete"
                                      onClick={e => this.deleteEducation(each)}
                                      class="deleteBtn"
                                    >
                                      <svg
                                        class="svg-inline--fa fa-trash fa-w-14"
                                        data-toggle="modal"
                                        data-target="#deleteModal"
                                        aria-hidden="true"
                                        focusable="false"
                                        data-prefix="fa"
                                        data-icon="trash"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 448 512"
                                        data-fa-i2svg=""
                                      >
                                        <path
                                          fill="currentColor"
                                          d="M432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16zM53.2 467a48 48 0 0 0 47.9 45h245.8a48 48 0 0 0 47.9-45L416 128H32z"
                                        />
                                      </svg>
                                    </a>
                                  </td>
                                </tr>
                              ))
                            ) : (
                              <tr>No data available</tr>
                            )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                )}
                {tabthree && (
                  <div
                    id="content-1"
                    className={tabthree ? "d-block" : "d-none"}
                  >
                    <button
                      type="button"
                      onClick={this.addjobpop}
                      class="commonBtn2 float-right mb-2"
                      data-toggle="modal"
                      data-target="#addEntry"
                    >
                      <svg
                        class="svg-inline--fa fa-plus fa-w-14"
                        aria-hidden="true"
                        focusable="false"
                        data-prefix="fa"
                        data-icon="plus"
                        role="img"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 448 512"
                        data-fa-i2svg=""
                      >
                        <path
                          fill="currentColor"
                          d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"
                        />
                      </svg>
                      Add new entry
                    </button>
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead class="thead-default">
                          <tr>
                            <th>Title</th>
                            <th>Name of Company</th>
                            <th>Start Job</th>
                            <th>End Job</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {curriculumPreviousJobs &&
                            curriculumPreviousJobs.length > 0 ? (
                              curriculumPreviousJobs.map((each, i) => (
                                <tr key={i}>
                                  <td data-label="Title">{each.title}</td>
                                  <td data-label="Name of Company">
                                    {each.name}
                                  </td>
                                  <td data-label="Start Job">{each.startjob}</td>
                                  <td data-label="End Job">{each.endjob}</td>
                                  <td data-label="Action">
                                    <a
                                      title="Edit"
                                      onClick={e => this.editjobpopupopen(each)}
                                      class="editBtn"
                                      data-toggle="modal"
                                      data-target="#addEntry"
                                    >
                                      <svg
                                        class="svg-inline--fa fa-edit fa-w-18"
                                        aria-hidden="true"
                                        focusable="false"
                                        data-prefix="fa"
                                        data-icon="edit"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512"
                                        data-fa-i2svg=""
                                      >
                                        <path
                                          fill="currentColor"
                                          d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"
                                        />
                                      </svg>
                                    </a>
                                    <a
                                      title="Delete"
                                      class="deleteBtn"
                                      onClick={e => this.deleteJob(each)}
                                    >
                                      <svg
                                        class="svg-inline--fa fa-trash fa-w-14"
                                        data-toggle="modal"
                                        data-target="#deleteModal"
                                        aria-hidden="true"
                                        focusable="false"
                                        data-prefix="fa"
                                        data-icon="trash"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 448 512"
                                        data-fa-i2svg=""
                                      >
                                        <path
                                          fill="currentColor"
                                          d="M432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16zM53.2 467a48 48 0 0 0 47.9 45h245.8a48 48 0 0 0 47.9-45L416 128H32z"
                                        />
                                      </svg>
                                    </a>
                                  </td>
                                </tr>
                              ))
                            ) : (
                              <tr ><th colSpan={5}><center>No data available</center></th></tr>
                            )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                )}
                {tabfour && (
                  <div
                    id="content-1"
                    className={tabfour ? "d-block" : "d-none"}
                  >
                    <button
                      type="button"
                      onClick={this.addlanguagepop}
                      class="commonBtn2 float-right mb-2"
                      data-toggle="modal"
                      data-target="#addEntry"
                    >
                      <svg
                        class="svg-inline--fa fa-plus fa-w-14"
                        aria-hidden="true"
                        focusable="false"
                        data-prefix="fa"
                        data-icon="plus"
                        role="img"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 448 512"
                        data-fa-i2svg=""
                      >
                        <path
                          fill="currentColor"
                          d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"
                        />
                      </svg>
                      Add new entry
                    </button>
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead class="thead-default">
                          <tr>
                            <th>Language</th>
                            <th>Language Level</th>
                            <th>Action</th>
                          </tr>
                        </thead>

                        <tbody>
                          {curriculumLanguageSkills &&
                            curriculumLanguageSkills.length > 0 ? (
                              _.uniqWith(curriculumLanguageSkills, _.isEqual).map((each, i) => (

                                <tr key={i}>
                                  <td data-label="Language">{each.language}</td>
                                  <td data-label="Language Level">
                                    {each.languagelevel}
                                  </td>

                                  <td data-label="Action">
                                    <a
                                      title="Edit"
                                      class="editBtn"
                                      onClick={e =>
                                        this.editlanguagepopupopen(each)
                                      }
                                    >
                                      <svg
                                        class="svg-inline--fa fa-edit fa-w-18"
                                        aria-hidden="true"
                                        focusable="false"
                                        data-prefix="fa"
                                        data-icon="edit"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512"
                                        data-fa-i2svg=""
                                      >
                                        <path
                                          fill="currentColor"
                                          d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"
                                        />
                                      </svg>
                                    </a>
                                    <a
                                      title="Delete"
                                      class="deleteBtn"
                                      onClick={e => this.deleteLanguage(each)}
                                    >
                                      <svg
                                        class="svg-inline--fa fa-trash fa-w-14"
                                        data-toggle="modal"
                                        data-target="#deleteModal"
                                        aria-hidden="true"
                                        focusable="false"
                                        data-prefix="fa"
                                        data-icon="trash"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 448 512"
                                        data-fa-i2svg=""
                                      >
                                        <path
                                          fill="currentColor"
                                          d="M432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16zM53.2 467a48 48 0 0 0 47.9 45h245.8a48 48 0 0 0 47.9-45L416 128H32z"
                                        />
                                      </svg>
                                    </a>
                                  </td>
                                </tr>
                              ))
                            ) : (
                              <tr ><th colSpan={3}><center>No data available</center></th></tr>
                            )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                )}

                {tabfive && (
                  <div
                    id="content-1"
                    className={tabfive ? "d-block" : "d-none"}
                  >
                    <button
                      type="button"
                      class="commonBtn2 float-right mb-2"
                      onClick={() =>
                        this.setState({ uploadPopup: !this.state.uploadPopup })
                      }
                    >
                      <svg
                        class="svg-inline--fa fa-plus fa-w-14"
                        aria-hidden="true"
                        focusable="false"
                        data-prefix="fa"
                        data-icon="plus"
                        role="img"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 448 512"
                        data-fa-i2svg=""
                      >
                        <path
                          fill="currentColor"
                          d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"
                        />
                      </svg>
                      <span> Upload new files</span>
                    </button>
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead class="thead-default">
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>FileName</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.props.getFilesDetails.fileDetails &&
                            this.props.getFilesDetails.fileDetails.map(
                              (each, i) => (
                                <tr>
                                  <td data-label="#">{i + 1}</td>
                                  <td data-label="Name">{each.name}</td>
                                  <td data-label="FileName"> {each.fileName}</td>

                                  <td data-label="Action">
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.handleAwsFiles(each.fileUrl)
                                      }
                                    >
                                      <i class="fas fa-download" />
                                    </button>
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.props.handleFileDelete(each.id, each.fileUrl, "curriculum")
                                      }
                                    >
                                      <i class="fas fa-trash" />
                                    </button>
                                  </td>
                                </tr>
                              )
                            )}

                        </tbody>
                      </table>
                    </div>
                  </div>
                )}

                {tabsix && (
                  <div id="content-1" className={tabsix ? "d-block" : "d-none"}>
                    <div class="previewItemDiv">
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="previewItem">
                            <label>Title :</label>
                            <span>{personalData.title}</span>{" "}
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="previewItem">
                            <label>First Name :</label>
                            <span>{personalData.fristName}</span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="previewItem">
                            <label>Family Name :</label>
                            <span>{personalData.familyName}</span>
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="previewItem">
                            <label>Date of Birth :</label>
                            <span>{personalData.dob}</span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="previewItem">
                            <label>Place of Birth :</label>
                            <span>{personalData.pob}</span>
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="previewItem">
                            <label>Gender :</label>
                            <span>{personalData.gender}</span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="previewItem">
                            <label>
                              Address (Street, ZIP/Postcode, City) :
                            </label>
                            <span>{personalData.addressprivate}</span>
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="previewItem">
                            <label>
                              Address Job (Street, ZIP/Postcode, City) :
                            </label>
                            <span>{personalData.addressjob}</span>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="previewItem">
                            <label>E-Mail :</label>
                            <span>{personalData.email}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
              <div
                className={addallergies ? "modal d-block" : "modal"}
                id="myModal"
              >
                <div class="modal-dialog">
                  <div class="modal-content modalPopUp">
                    <div class="modal-header borderNone">
                      <h5 class="modal-title">Add Education</h5>
                      <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        onClick={this.modalPopUpClose}
                      >
                        <i class="fas fa-times" />
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="row">
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">
                                Title for list entry (e.g. Elementary School,
                                University)<sup>*</sup>
                              </label>
                              <input
                                className="form-control"
                                name="title"
                                onChange={this.handleChange}
                                value={title}
                                type="text"
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">
                                Name of institution
                              </label>
                              <input
                                className="form-control"
                                name="name"
                                onChange={this.handleChange}
                                value={name}
                                type="text"
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">
                                Address (Street, ZIP/Postcode, City)
                              </label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="address"
                                onChange={this.handleChange}
                                type="text"
                                value={address}
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">Country</label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="country"
                                onChange={this.handleChange}
                                type="text"
                                value={country}
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">Homepage</label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="homepage"
                                onChange={this.handleChange}
                                type="text"
                                value={homepage}
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">
                                Start of education
                              </label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="starteducation"
                                onChange={this.handleChange}
                                type="date"
                                value={starteducation}
                                onKeyPress={onlyDate}
                              />
                              <span />
                              <div style={{ color: "red" }}>{errors.starteducation}</div>

                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">
                                End of education
                              </label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="endeducation"
                                onChange={this.handleChange}
                                type="date"
                                onKeyPress={onlyDate}
                                value={endeducation} min={starteducation}
                              />
                              <span />
                              <div style={{ color: "red" }}>{errors.endeducation}</div>

                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="d-block">Completed</label>
                              <div class="genderInfo">
                                <input
                                  type="radio"
                                  id={"cloud"}
                                  name="completed"
                                  value="Yes"
                                  checked={completed === "Yes"}
                                  onChange={this.setGender.bind(this)}
                                />
                                <label htmlFor={"cloud"}>Yes</label>
                              </div>
                              <div class="genderInfo">
                                <input
                                  type="radio"
                                  id={"email"}
                                  name="completed"
                                  value="No"
                                  checked={completed === "No"}
                                  onChange={this.setGender.bind(this)}
                                />
                                <label htmlFor={"email"}>No</label>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">Degree</label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="degree"
                                onChange={this.handleChange}
                                type="text"
                                value={degree}
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">Field</label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="field"
                                onChange={this.handleChange}
                                type="text"
                                value={field}
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <button
                              type="button"
                              class="commonBtn float-right"
                              onClick={this.handleEducation} disabled={!Valid}
                            >
                              Add
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div
                className={editallergies ? "modal d-block" : "modal"}
                id="myModal"
              >
                <div class="modal-dialog">
                  <div class="modal-content modalPopUp">
                    <div class="modal-header borderNone">
                      <h5 class="modal-title">Edit Education</h5>
                      <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        onClick={this.modalPopUpClose}
                      >
                        <i class="fas fa-times" />
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="row">
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">
                                Title for list entry (e.g. Elementary School,
                                University)
                              </label>
                              <input
                                className="form-control"
                                name="title"
                                onChange={this.handleChange}
                                defaultValue={this.state.editDetails.title}
                                type="text"
                                readOnly
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">
                                Name of institution
                              </label>
                              <input
                                className="form-control"
                                name="name"
                                onChange={this.handleChange}
                                defaultValue={this.state.editDetails.name}
                                type="text"
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">
                                Address (Street, ZIP/Postcode, City)
                              </label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="address"
                                onChange={this.handleChange}
                                type="text"
                                defaultValue={this.state.editDetails.address}
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">Country</label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="country"
                                onChange={this.handleChange}
                                type="text"
                                defaultValue={this.state.editDetails.country}
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">Homepage</label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="homepage"
                                onChange={this.handleChange}
                                type="text"
                                defaultValue={this.state.editDetails.homepage}
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">
                                Start of education
                              </label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="starteducation"
                                onChange={this.handleChange}
                                type="date"
                                onKeyPress={onlyDate}
                                defaultValue={
                                  this.state.editDetails.starteducation
                                }
                              />
                              <span />

                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">
                                End of education
                              </label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="endeducation"
                                onChange={this.handleChange}
                                type="date"
                                onKeyPress={onlyDate}
                                defaultValue={
                                  this.state.editDetails.endeducation
                                }
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="d-block">Completed</label>
                              <div class="genderInfo">
                                <input
                                  type="radio"
                                  id={"cloud"}
                                  name="completed"
                                  value="Yes"
                                  checked={completed === "Yes"}
                                  onChange={this.setGender.bind(this)}
                                />
                                <label htmlFor={"cloud"}>Yes</label>
                              </div>
                              <div class="genderInfo">
                                <input
                                  type="radio"
                                  id={"email"}
                                  name="completed"
                                  value="No"
                                  checked={completed === "No"}
                                  onChange={this.setGender.bind(this)}
                                />
                                <label htmlFor={"email"}>No</label>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">Degree</label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="degree"
                                onChange={this.handleChange}
                                type="text"
                                defaultValue={this.state.editDetails.degree}
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">Field</label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="field"
                                onChange={this.handleChange}
                                type="text"
                                defaultValue={this.state.editDetails.field}
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <button
                              type="button"
                              class="commonBtn float-right"
                              onClick={this.updateAllery}
                            >
                              Update
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div className={addjob ? "modal d-block" : "modal"} id="myModal">
                <div class="modal-dialog">
                  <div class="modal-content modalPopUp">
                    <div class="modal-header borderNone">
                      <h5 class="modal-title">Add PreviousJob</h5>
                      <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        onClick={this.modalPopUpClose}
                      >
                        <i class="fas fa-times" />
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="row">
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">
                                Title for list entry (e.g. CEO, Company Name) <sup>* </sup>
                              </label>
                              <input
                                className="form-control"
                                name="title"
                                onChange={this.handleChange}
                                value={title}
                                type="text"
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">
                                Name of company
                              </label>
                              <input
                                className="form-control"
                                name="name"
                                onChange={this.handleChange}
                                value={name}
                                type="text"
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">
                                Address (Street, ZIP/Postcode, City)
                              </label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="address"
                                onChange={this.handleChange}
                                type="text"
                                value={address}
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">Country</label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="country"
                                onChange={this.handleChange}
                                type="text"
                                value={country}
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">Homepage</label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="homepage"
                                onChange={this.handleChange}
                                type="text"
                                value={homepage}
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">Start of job </label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="startjob"
                                onChange={this.handleChange}
                                type="date"
                                onKeyPress={onlyDate}
                                value={startjob}
                              />
                              <span />
                              <div style={{ color: "red" }}>{errors.startjob}</div>
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">End of job <sup>* </sup></label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="endjob"
                                onChange={this.handleChange}
                                type="date"
                                onKeyPress={onlyDate}
                                value={endjob} min={startjob}
                              />
                              <span />
                              <div style={{ color: "red" }}>{errors.endjob}</div>
                            </div>
                          </div>

                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">Job type</label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="jobtype"
                                onChange={this.handleChange}
                                type="text"
                                value={jobtype}
                              />
                              <span />
                            </div>
                          </div>

                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <button
                              type="button"
                              class="commonBtn float-right"
                              onClick={this.handleJob} disabled={!Valid}
                            >
                              Add
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div className={editjob ? "modal d-block" : "modal"} id="myModal">
                <div class="modal-dialog">
                  <div class="modal-content modalPopUp">
                    <div class="modal-header borderNone">
                      <h5 class="modal-title">Edit PreviousJob</h5>
                      <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        onClick={this.modalPopUpClose}
                      >
                        <i class="fas fa-times" />
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="row">
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">
                                Title for list entry (e.g. CEO, Company Name)
                              </label>
                              <input
                                className="form-control"
                                name="title"
                                onChange={this.handleChange}
                                defaultValue={this.state.editjobDetails.title}
                                type="text"
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">
                                Name of company
                              </label>
                              <input
                                className="form-control"
                                name="name"
                                onChange={this.handleChange}
                                defaultValue={this.state.editjobDetails.name}
                                type="text"
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">
                                Address (Street, ZIP/Postcode, City)
                              </label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="address"
                                onChange={this.handleChange}
                                type="text"
                                defaultValue={this.state.editjobDetails.address}
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">Country</label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="country"
                                onChange={this.handleChange}
                                type="text"
                                defaultValue={this.state.editjobDetails.country}
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">Homepage</label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="homepage"
                                onChange={this.handleChange}
                                type="text"
                                defaultValue={
                                  this.state.editjobDetails.homepage
                                }
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">Start of job</label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="startjob"
                                onChange={this.handleChange}
                                type="date"
                                defaultValue={
                                  this.state.editjobDetails.startjob
                                }
                                onKeyPress={onlyDate}
                              />
                              <span />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">End of job</label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="endjob"
                                onChange={this.handleChange}
                                type="date"
                                defaultValue={this.state.editjobDetails.endjob}
                                onKeyPress={onlyDate}
                              />
                              <span />
                            </div>
                          </div>

                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="form-label">Job type</label>
                              <input
                                id="validation-email"
                                className="form-control"
                                name="jobtype"
                                onChange={this.handleChange}
                                type="text"
                                defaultValue={this.state.editjobDetails.jobtype}
                              />
                              <span />
                            </div>
                          </div>

                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <button
                              type="button"
                              class="commonBtn float-right"
                              onClick={this.updateJob}
                            >
                              Update
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

              <div
                className={addlanguage ? "modal d-block" : "modal"}
                id="myModal"
              >
                <div class="modal-dialog">
                  <div class="modal-content modalPopUp">
                    <div class="modal-header borderNone">
                      <h5 class="modal-title">Add Language</h5>
                      <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        onClick={this.modalPopUpClose}
                      >
                        <i class="fas fa-times" />
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="row">
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div className="form-group">
                              <label>Language <sup>*</sup></label>
                              <select
                                className="form-control"
                                name="language"
                                value={this.state.language}
                                onChange={this.handleChange}
                              >
                                <option value="">--Select Language--</option>
                                <option value="Chinese">Chinese</option>
                                <option value="Spanish">Spanish</option>
                                <option value="English">English</option>
                                <option value="Hindi">Hindi</option>
                                <option value="Portuguese">Portuguese</option>
                                <option value="Russian">Russian</option>
                                <option value="Japanese">Japanese</option>
                                <option value="Javanese">Javanese</option>
                                <option value="Lahnda">Lahnda</option>
                                <option value="German">German</option>
                                <option value="Korean">Korean</option>
                                <option value="French">French</option>
                                <option value="Telugu">Telugu</option>
                                <option value="Marathi">Marathi</option>
                                <option value="Turkish">Turkish</option>
                                <option value="Tamil">Tamil</option>
                                <option value="Vietnamese">Vietnamese</option>
                                <option value="Urdu">Urdu</option>
                                <option value="Italian">Italian</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="d-block">Language level</label>
                              <div class="genderInfo">
                                <input
                                  type="radio"
                                  id={"level"}
                                  name="languagelevel"
                                  value="good"
                                  checked={languagelevel === "good"}
                                  onChange={this.setGender.bind(this)}
                                />
                                <label htmlFor={"level"}>good</label>
                              </div>
                              <div class="genderInfo">
                                <input
                                  type="radio"
                                  id={"level1"}
                                  name="languagelevel"
                                  value="bad"
                                  checked={languagelevel === "bad"}
                                  onChange={this.setGender.bind(this)}
                                />
                                <label htmlFor={"level1"}>bad</label>
                              </div>
                              <div class="genderInfo">
                                <input
                                  type="radio"
                                  id={"level2"}
                                  name="languagelevel"
                                  value="nonexistent"
                                  checked={languagelevel === "nonexistent"}
                                  onChange={this.setGender.bind(this)}
                                />
                                <label htmlFor={"level2"}>nonexistent</label>
                              </div>
                            </div>
                          </div>

                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <button
                              type="button"
                              class="commonBtn float-right"
                              onClick={this.handleLanguage} disabled={!Valid}
                            >
                              Add
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div
                className={editlanguage ? "modal d-block" : "modal"}
                id="myModal"
              >
                <div class="modal-dialog">
                  <div class="modal-content modalPopUp">
                    <div class="modal-header borderNone">
                      <h5 class="modal-title">Edit Language</h5>
                      <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        onClick={this.modalPopUpClose}
                      >
                        <i class="fas fa-times" />
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="row">
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div className="form-group">
                              <label>Language</label>
                              <select
                                className="form-control"
                                name="language"
                                value={this.state.language}
                                onChange={this.handleChange}
                              >
                                <option>--Select Language--</option>
                                <option disabled="disabled" value="Chinese">
                                  Chinese
                                </option>
                                <option disabled="disabled" value="Spanish">
                                  Spanish
                                </option>
                                <option disabled="disabled" value="English">
                                  English
                                </option>
                                <option disabled="disabled" value="Hindi">
                                  Hindi
                                </option>
                                <option disabled="disabled" value="Portuguese">
                                  Portuguese
                                </option>
                                <option disabled="disabled" value="Russian">
                                  Russian
                                </option>
                                <option disabled="disabled" value="Japanese">
                                  Japanese
                                </option>
                                <option disabled="disabled" value="Javanese">
                                  Javanese
                                </option>
                                <option disabled="disabled" value="Lahnda">
                                  Lahnda
                                </option>
                                <option disabled="disabled" value="German">
                                  German
                                </option>
                                <option disabled="disabled" value="Korean">
                                  Korean
                                </option>
                                <option disabled="disabled" value="French">
                                  French
                                </option>
                                <option disabled="disabled" value="Telugu">
                                  Telugu
                                </option>
                                <option disabled="disabled" value="Marathi">
                                  Marathi
                                </option>
                                <option disabled="disabled" value="Turkish">
                                  Turkish
                                </option>
                                <option disabled="disabled" value="Tamil">
                                  Tamil
                                </option>
                                <option disabled="disabled" value="Vietnamese">
                                  Vietnamese
                                </option>
                                <option disabled="disabled" value="Urdu">
                                  Urdu
                                </option>
                                <option disabled="disabled" value="Italian">
                                  Italian
                                </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label className="d-block">Language level</label>
                              <div class="genderInfo">
                                <input
                                  type="radio"
                                  id={"level"}
                                  name="languagelevel"
                                  value="good"
                                  checked={languagelevel === "good"}
                                  onChange={this.setGender.bind(this)}
                                />
                                <label htmlFor={"level"}>good</label>
                              </div>
                              <div class="genderInfo">
                                <input
                                  type="radio"
                                  id={"level1"}
                                  name="languagelevel"
                                  value="bad"
                                  checked={languagelevel === "bad"}
                                  onChange={this.setGender.bind(this)}
                                />
                                <label htmlFor={"level1"}>bad</label>
                              </div>
                              <div class="genderInfo">
                                <input
                                  type="radio"
                                  id={"level2"}
                                  name="languagelevel"
                                  value="nonexistent"
                                  checked={languagelevel === "nonexistent"}
                                  onChange={this.setGender.bind(this)}
                                />
                                <label htmlFor={"level2"}>nonexistent</label>
                              </div>
                            </div>
                          </div>

                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <button
                              type="button"
                              class="commonBtn float-right"
                              onClick={this.updateLanguage}
                            >
                              Update
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* newly */}
        <div
          className={this.state.uploadPopup ? "modal d-block" : "modal"}
          id="myModal4"
        >
          <div class="modal-dialog">
            <div class="modal-content modalPopUp">
              <div class="modal-header borderNone">
                <h5 class="modal-title">Upload File</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  onClick={() =>
                    this.setState({ uploadPopup: !this.state.uploadPopup })
                  }
                >
                  <i class="fas fa-times" />
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="form-group">
                        <label className="form-label">File name</label>
                        <input

                          className="form-control"
                          type="text"
                          id="name"
                          name="name"
                          value={this.state.name}
                          onChange={this.handleChange}
                        />
                        <span />
                      </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="fileUp">
                        <label
                          for="fileUp1"
                          class="commonBtn text-center upload"
                        >
                          Upload File
                        </label>
                        <input
                          type="file"
                          id="fileUp1"
                          onChange={this.handleChangePopup}
                          style={{ display: "none" }}
                        />
                        {/* <p style={{ textAlign: "center" }}>hsfkjhsfj</p> */}
                      </div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-sm-12 mt-2">
                      <button
                        type="button"
                        class="commonBtn float-right"
                        onClick={this.popupSubmit} disabled={!this.state.fileURL}
                      >
                        Submit
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  getpersonalDetails: state.healthrecordReducer.getpersonalDetails,
  loginDetails: state.loginReducer.loginDetails,
  getFilesDetails: state.uploadReducer.getFilesDetails
});

const mapDispatchToProps = dispatch => ({
  addcurriculumData: addcurriculum =>
    dispatch(addcurriculumData(addcurriculum)),
  getPersonaldata: listpersonal => dispatch(getPersonaldata(listpersonal)),
  addeducationData: addeducation => dispatch(addeducationData(addeducation)),
  addpreviousjobData: addpreviousjob =>
    dispatch(addpreviousjobData(addpreviousjob)),
  deleteeducationData: deleteeducation =>
    dispatch(deleteeducationData(deleteeducation)),
  updateeducationData: updateeducation =>
    dispatch(updateeducationData(updateeducation)),
  updatepreviousjobData: updatepreviousjob =>
    dispatch(updatepreviousjobData(updatepreviousjob)),
  deletepreviousjobData: deletepreviousjob =>
    dispatch(deletepreviousjobData(deletepreviousjob)),
  addlanguageData: addlanguage => dispatch(addlanguageData(addlanguage)),
  updatelanguageData: updatelanguage =>
    dispatch(updatelanguageData(updatelanguage)),
  deletelanguageData: deletelanguage =>
    dispatch(deletelanguageData(deletelanguage)),
  getUploadFiles: payload => dispatch(getUploadFiles(payload)),
  handleFileDelete: (id, fileUrl, type) => dispatch(handleFileDelete(id, fileUrl, type))

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewCurriculum);
