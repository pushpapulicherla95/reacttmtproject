const stripe = require("stripe")("sk_test_hvPnrQ6yTlxPjvw8IMKw0JTW00kp6rSUAF");
//const logger = require("../../utils/logger");

// saving user card
const createCard = async (req, res) => {
  console.log("sdfsdfsfs", req.body);
  // if (!req.body.email) {
  //     return res.status(400).json({
  //         status: false,
  //         message: 'failure',
  //         data: "Please Provide mailid"
  //     });
  // }
  // let user = await User.findOne({
  //     email: req.body.email
  // })

  stripe.tokens
    .create({
      card: {
        number: req.body.number,
        exp_month: req.body.exp_month,
        exp_year: req.body.exp_year,
        cvc: req.body.cvc,
        name: req.body.name
      }
    })
    .then(token => {
      stripe.customers.createSource(
        user.customer_id,
        {
          source: token.id
        },
        (err, card) => {
          if (err) {
            return res.status(400).json({
              status: false,
              message: "failure",
              data: err.message
            });
          } else {
            return res.json({
              status: true,
              message: "success",
              data: card
            });
          }
        }
      );
    })
    .catch(err => {
      return res.status(400).json({
        status: false,
        message: "failure",
        data: err.message,
        type: err.type,
        statusCode: err.statusCode
      });
    });
};
//updating individual saved card
const updateSavedCard = async (req, res) => {
  if (!req.body.email) {
    return res.status(400).json({
      status: false,
      message: "failure",
      data: "Please Provide mailid"
    });
  }
  // let user = await User.findOne({
  //     email: req.body.email
  // })
  let user = new Object();
  if (user) {
    stripe.customers.updateCard(
      user.customer_id,
      req.body.card_id,
      {
        name: "Jenny Rosen",
        address_city: req.body.address_city,
        exp_year: req.body.exp_year
      },
      (err, card) => {
        if (err) {
          return res.json({
            status: false,
            message: err.message
          });
        } else {
          return res.json({
            status: true,
            message: "Card Updated Successfully",
            card: card
          });
        }
      }
    );
  } else {
    return res.json({
      status: false,
      message: "user does not exist"
    });
  }
};
//delete saved card
const deleteSavedCard = async (req, res) => {
  if (!req.body.email) {
    return res.status(400).json({
      status: false,
      message: "failure",
      data: "Please Provide mailid"
    });
  }
  // let user = await User.findOne({
  //     email: req.body.email
  // })
  let user = new Object();
  if (user) {
    stripe.customers.deleteCard(
      user.customer_id,
      req.body.card_id,
      (err, confirmation) => {
        if (err) {
          return res.status(400).json({
            status: false,
            message: "failure",
            data: err.message
          });
        } else {
          return res.json({
            status: true,
            message: "success",
            data: confirmation
          });
        }
      }
    );
  } else {
    return res.status(400).json({
      status: false,
      message: "User not exist"
    });
  }
};
//charge function
const createCharge = async (req, res) => {
  const { token } = req;
  let number = (amount * 100).toFixed();
  return new Promise((resolve, reject) => {
    stripe.charges.create(
      {
        amount: 10,
        source: token.id // obtained with Stripe.js
      },
      (err, charge) => {
        if (err) {
          reject(err);
        } else {
          resolve(charge);
        }
      }
    );
  });
};
//create charge for saved cards
const chargeWithSavedCards = async req => {
  const { paymentBreakup, user, source, totalAmount, userDetails } = req;
  let amount = totalAmount;
  let number = (amount * 100).toFixed();
  return new Promise((resolve, reject) => {
    stripe.charges.create(
      {
        amount: number,
        currency: paymentBreakup[0].currency,
        customer: user.customer_id,
        source: source, // obtained with Stripe.js
        description: userDetails.name
      },
      (err, charge) => {
        if (err) {
          reject(err);
        } else {
          // logger.info(
          //   `Charge Completed by ${userDetails.email} Amount: ${
          //     paymentBreakup[0].currency
          //   } ${amount} `
          // );
          resolve(charge);
        }
      }
    );
  });
};

//creating card token
const createCardToken = async req => {
  const { cards } = req.paymentMethod;
  return new Promise((resolve, reject) => {
    stripe.tokens.create(
      {
        card: {
          number: cards[0].num,
          exp_month: cards[0].expiry.month,
          exp_year: cards[0].expiry.year,
          cvc: cards[0].cvv,
          name: cards[0].nameOnCard
        }
      },
      (err, token) => {
        if (err) {
          reject(err);
        } else {
          resolve(token);
        }
      }
    );
  });
};
//refund amount
const refundChargeAmount = async (req, res) => {
  const { charge, refund_amount, bookingId, booking_type, reason } = req;
  let amount = refund_amount;
  let number = (amount * 100).toFixed();
  return new Promise((resolve, reject) => {
    stripe.refunds.create(
      {
        charge: charge.id,
        amount: number,
        metadata: {
          original_amount: charge.amount,
          refund_amount: refund_amount,
          booking_type: booking_type,
          booking_id: bookingId,
          reason: reason
        },
        reason: reason
      },
      (err, refund) => {
        if (err) {
          reject(err);
        } else {
          // logger.info(
          //   `Refund SucessFull ${booking_type} Booking Id:${bookingId} Amount:${amount}`
          // );
          resolve(refund);
        }
      }
    );
  });
};
//getCharge result using chargeid
const getCharge = async (req, res) => {
  const { charge_id } = req;
  return new Promise((resolve, reject) => {
    stripe.charges.retrieve(charge_id, (err, charge) => {
      if (err) {
        reject(err);
      } else {
        resolve(charge);
      }
    });
  });
};

// getcardList
const getSavedCardList = async (req, res) => {
  if (!req.query.email) {
    return res.status(400).json({
      status: false,
      message: "failure",
      data: "Please Provide mailid"
    });
  }
  // let user = await User.findOne({
  //     email: req.body.email
  // })
  let user = new Object();
  //stripe.customers.listCards(customerId)
  if (user) {
    stripe.customers.listCards(user.customer_id, function(err, cards) {
      if (err) {
        return res.status(400).json({
          status: false,
          message: "failure",
          data: err.message
        });
      } else {
        return res.json({
          status: true,
          message: "success",
          data: cards
        });
      }
    });
  } else {
    return res.status(400).json({
      status: true,
      message: "Incorrect Credentials"
    });
  }
};
//create product
const createProduct = async (req, res) => {
  const { productName, productType, planAmount, interval, currency } = req.body;
  let amount = planAmount;
  let planamt = (amount * 100).toFixed();
  return new Promise((resolve, reject) => {
    stripe.products.create(
      {
        name: productName,
        type: "service"
      },
      function(err, product) {
        //creating plan
        stripe.plans.create(
          {
            amount: planamt,
            interval: interval,
            product: product.id,
            currency: "usd"
          },
          function(err, plan) {
            if (err) {
              res.status(400).json({
                status: false,
                message: "failure",
                data: { message: "error while creating plan", error: err }
              });
            } else {
              res.status(200).json({
                status: true,
                message: "success",
                data: { message: "Plan created successfully", data: plan }
              });
            }
          }
        );
      }
    );
  });
};

const getPlans = async (req, res) => {
  stripe.plans.list({ limit: 1 }, function(err, plans) {
    if (err) {
      res.status(400).json({
        status: false,
        message: "failure",
        data: { message: "error while getting plan list", error: err }
      });
    } else {
      res.status(200).json({
        status: true,
        message: "success",
        data: { message: "Planlist retrieved successfully", data: plans }
      });
    }
  });
};
const deletePlan = (req, res) => {
  console.log("req.body.planId", req.body.planId);
  if (!req.body.planId) {
    res.status(400).json({
      status: false,
      message: "failure",
      data: { message: "please enter valid palnid" }
    });
  }
  stripe.products.del(req.body.planId, function(err, confirmation) {
    // asynchronously
    if (err) {
      res.status(400).json({
        status: false,
        message: "failure",
        data: { message: "error while deleting paln", error: err }
      });
    } else {
      res.status(200).json({
        status: true,
        message: "success",
        data: { message: "Plan deleted successfully", data: confirmation }
      });
    }
  });
};
const addSubscription = async (req, res) => {
  const { planId, customerId } = req;
  // console.log("planId, customerId", planId, customerId)
  return new Promise((resolve, reject) => {
    stripe.subscriptions.create(
      {
        customer: customerId,
        items: [
          {
            plan: planId
          }
        ]
      },
      function(err, subscription) {
        console.log("err, subscription", err, subscription);
        if (err) {
          reject(err);
        } else {
          resolve(subscription);
        }
      }
    );
  });
};
const createPaymentSource = async (req, res) => {
  const { cards } = req.paymentMethod;
  return new Promise((resolve, reject) => {
    stripe.tokens
      .create({
        card: {
          number: cards[0].num,
          exp_month: cards[0].expiry.month,
          exp_year: cards[0].expiry.year,
          cvc: cards[0].cvv,
          name: cards[0].nameOnCard
        }
      })
      .then(token => {
        stripe.customers.createSource(
          req.customerId,
          {
            source: token.id
          },
          (err, card) => {
            console.log("card>>>>", card);
            if (err) {
              reject(err);
            } else {
              resolve(card);
            }
          }
        );
      })
      .catch(err => {
        reject(err);
      });
  });
};
const retrievePlan = async (req, res) => {
  return new Promise((resolve, reject) => {
    stripe.plans.retrieve(req.planId, function(err, plan) {
      if (err) {
        reject(err);
      } else {
        resolve(plan);
      }
    });
  });
};
const deleteSubscriptionPlan = async (req, res) => {
  return new Promise((resolve, reject) => {
    stripe.subscriptions.del(req.subscriptionId, (err, confirmation) => {
      // asynchronously called
      if (err) {
        reject(err);
      } else {
        resolve(confirmation);
      }
    });
  });
};

const deleteCardSource = async (req, res) => {
  const { customerId, cardId } = req;
  return new Promise((resolve, reject) => {
    stripe.customers.deleteCard(customerId, cardId, (err, confirmation) => {
      // asynchronously called
      if (err) {
        reject(err);
      } else {
        resolve(confirmation);
      }
    });
  });
};

const createPayment = (request, response) => {
  const cards = {
    number: "4242424242424242",
    exp_month: 12,
    exp_year: 2020,
    cvc: "123"
  };
  stripe.tokens.create({ card: cards }, (err, token) => {
    stripe.charges.create(
      {
        amount: request.body.amount,
        currency: "usd",
        source: token.id, // obtained with Stripe.js
        description: "Charge for tomato@example.com"
      },
      (err, charge) => {
        if (err) {
          return response.status(400).json({
            status: false,
            message: "failure",
            data: err.message
          });
        } else {
          return response.json({
            status: true,
            message: "success",
            data: charge
          });
        }
      }
    );
  });
};

const createplans = (request, response) => {
  stripe.plans.create(request.body, (err, plan) => {
    if (err) {
    } else {
      stripe.plans.list({ limit: 2 }, (err, plans) => {
        return response.json({
          status: true,
          message: "success",
          data: plans
        });
      });
    }
  });
};

const createUserSubscription = async (request, response) => {
  const { email, planId, paymentMethod, userDetails } = request.body;
  // console.log("email>>>", email);
  // let userDetails = await User.findOne({
  //     email: email
  // })
  // console.log("userDetails.customerId", userDetails);

  if (userDetails) {
    if (userDetails.subscription_type != process.env.SUBSCRIPTION_PLAN_TYPE) {
      retrievePlan({ planId })
        .then(plan => {
          if (parseInt(plan.amount) == parseInt(totalAmount)) {
            createPaymentSource({
              paymentMethod,
              customerId: userDetails.customer_id
            })
              .then(card => {
                addSubscription({ planId, customerId: userDetails.customer_id })
                  .then(subscription => {
                    let fullCard = {
                      chargeId: subscription.id,
                      amount: totalAmount,
                      cardToken: card.id
                    };
                    let charge = {
                      ...card,
                      ...fullCard,
                      ...{ id: subscription.id }
                    };
                    console.log("charge", charge);
                    saveSubscriptionPlan({
                      userDetails,
                      subscription,
                      charge,
                      card
                    })
                      .then(successdata => {
                        response.status(200).json({
                          status: false,
                          message: "success",
                          data: {
                            message: successdata
                          }
                        });
                      })
                      .catch(err => {
                        response.status(400).json({
                          status: false,
                          message: "failure",
                          data: {
                            message: "error while saving subscription data"
                          }
                        });
                      });
                  })
                  .catch(err => {
                    response.status(400).json({
                      status: false,
                      message: "failure",
                      data: {
                        message: "Subscription creation failed",
                        error: err
                      }
                    });
                  });
              })
              .catch(err => {
                console.log("err", err);
                response.status(400).json({
                  status: false,
                  message: "failure",
                  data: {
                    message:
                      "stripe cardtoken creation failed, Please provide valid card details",
                    error: err
                  }
                });
              });
          } else {
            response.status(400).json({
              status: false,
              message: "failure",
              data: {
                message: "Please provide valid plan amount"
              }
            });
          }
        })
        .catch(err => {
          response.status(400).json({
            status: false,
            message: "failure",
            data: { message: "error while retrieving plandata", error: err }
          });
        });
    } else {
      response.status(400).json({
        status: false,
        message: "failure",
        data: {
          message: "Your subscription is already active for current month."
        }
      });
    }
  } else {
    response.status(400).json({
      status: false,
      message: "failure",
      data: {
        message: "please provide valid email"
      }
    });
  }
};

module.exports = {
  createCard,
  getSavedCardList,
  createCharge,
  createCardToken,
  updateSavedCard,
  deleteSavedCard,
  chargeWithSavedCards,
  refundChargeAmount,
  getCharge,
  createProduct,
  getPlans,
  deletePlan,
  addSubscription,
  createPaymentSource,
  retrievePlan,
  deleteSubscriptionPlan,
  deleteCardSource,
  createPayment,
  createplans
};


