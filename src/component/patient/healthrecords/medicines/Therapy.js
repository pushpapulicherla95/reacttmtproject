import React, { Component } from "react";
import { connect } from "react-redux";
import {
  createTherapyData,
  listTherapyData,
  updateTherapyData,
  deleteTherapyData
} from "../../../../service/patient/action";
import { getUploadFiles,handleHealthDataDelete } from "../../../../service/upload/action";
import Select from "react-select";
import axios from "axios";
import URL from '../../../../asset/configUrl'
import { awsFiles } from '../../../../service/common/action'

class Therapy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 1,
      tabone: true,
      tabtwo: false,
      name: "",
      diagnosiscode: "",
      comment: "",
      title: "",
      addtherapypop: false,
      editfinding: false,
      editDetails: {},
      patientTherapyListData: [],
      uploadPopup: false,
      name: "",
      fileName: "",
      fileType: "",
      fileURL: "",
      errors: {
        title: "",
        name: ""
      },
      titleValid: false,
      nameValid: false,
      firstValid: false
    };
  }

  handleAwsFiles = value => {
    awsFiles(value).then(response => {
      window.location.href = response;
    });
  };

  handleChangePopup = e => {
    var reader = new FileReader();
    var file = e.target.files[0];
    let name = file.name.split('.')[0]
    let type = "." + file.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileURL: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(file);
  };
  popupSubmit = () => {
    var phrase = this.state.fileURL;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);
    var { userInfo } = this.props.loginDetails;
    const payload = {
      uid: userInfo.userId,
      z: match[1],
      fileName: this.state.fileName,
      fileType: this.state.fileType,
      name: this.state.name,
      category: "Therapy"
    };
    axios
      .post(URL.FILETHERAPHY_UPLOAD, payload)
      .then(response => {
        if (response.data.status === "success") {
          var { userInfo } = this.props.loginDetails;
          const uploadFiles = {
            uid: userInfo.userId,
            category: "Therapy"
          };
          this.props.getUploadFiles(uploadFiles);
          this.setState({ uploadPopup: false });
        }
      })
      .catch(error => {
        this.setState({ uploadPopup: true });
      });
  };

  tabOne = () => {
    this.setState({ tabone: true, tabtwo: false });
  };
  tabTwo = () => {
    this.setState({ tabone: false, tabtwo: true });
  };
  handleChange = e => {
    e.preventDefault();
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };
  therapypop = () => {
    this.setState({ addtherapypop: true }, () => {
      this.emptystate();
    });
  };
  modalPopUpClose = () => {
    this.setState({
      addtherapypop: false,
      edittherapy: false
    });
  };
  handleTherapy = () => {
    const { title, name, comment, diagnosiscode } = this.state;
    const { userInfo } = this.props.loginDetails;

    const createtherapy = {
      uid: userInfo.userId,
      theraphy: [
        {
          title: title,
          name: name,
          comment: comment,
          diagnosiscode: diagnosiscode
        }
      ]
    };

    this.props.createTherapyData(createtherapy);

    this.setState({
      addtherapypop: false
    });
  };
  componentWillMount() {
    const { userInfo } = this.props.loginDetails;

    const listtherapy = {
      uid: userInfo.userId
    };
    // var { userInfo } = this.props.loginDetails;
    const uploadFiles = {
      uid: userInfo.userId,
      category: "Therapy"
    };
    this.props.getUploadFiles(uploadFiles);
    this.props.listTherapyData(listtherapy);
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.gettherapylist &&
      nextProps.gettherapylist.theraphy != null &&
      nextProps.gettherapylist.theraphy != 0
    ) {
      const listtherapy = nextProps.gettherapylist.theraphy;
      let parsedContent = JSON.parse(listtherapy);
      let patientTherapyListData = parsedContent.theraphy;

      this.setState({
        patientTherapyListData,
        addtherapypop: false
      });
    }
  }

  modalPopupOpen = e => {
    this.setState({ edittherapy: true, editDetails: e });
    this.setState({
      title: e.title,
      name: e.name,
      comment: e.comment,
      diagnosiscode: e.diagnosiscode
    });
  };
  updateTherapy = () => {
    const { title, name, diagnosiscode, comment } = this.state;
    const { userInfo } = this.props.loginDetails;

    const updatetherapy = {
      uid: userInfo.userId,
      theraphy: [
        {
          title: title,
          name: name,
          comment: comment,
          diagnosiscode: diagnosiscode
        }
      ]
    };

    this.props.updateTherapyData(updatetherapy);
    this.setState({ edittherapy: false });
  };
  deleteTherapy = e => {
    const { userInfo } = this.props.loginDetails;

    const deletetherapy = {
      uid: userInfo.userId,
      theraphy: [
        {
          title: e.title,
          name: e.name,
          comment: e.comment,
          diagnosiscode: e.diagnosiscode
        }
      ]
    };
    this.props.deleteTherapyData(deletetherapy);
  };
  emptystate = () => {
    this.setState({
      name: "",
      comment: "",
      title: "",
      diagnosiscode: ""
    });
  };
  //validation

  validateField(fieldName, value) {
    const { errors, titleValid, nameValid } = this.state;

    let titlevalid = titleValid;
    let namevalid = nameValid;
    let fieldValidationErrors = errors;
    switch (fieldName) {
      case "title":
        titlevalid = !(value.length < 3 || value.length > 20);
        fieldValidationErrors.title = titlevalid
          ? ""
          : "Title should be 3 to 20 characters";
        break;
      case "name":
        namevalid = !(value.length < 3 || value.length > 20);
        fieldValidationErrors.name = namevalid
          ? ""
          : "Name should be 3 to 20 characters";
        break;
    }
    this.setState(
      {
        errors: fieldValidationErrors,
        titleValid: titlevalid,
        nameValid: namevalid
      },
      this.validateForm
    );
  }

  validateForm() {
    const { titleValid, nameValid } = this.state;
    this.setState({
      firstValid: titleValid && nameValid
    });
  }
  render() {
    const {
      tabone,
      tabtwo,
      comment,
      title,
      name,
      addtherapypop,
      patientTherapyListData,
      edittherapy,
      diagnosiscode,
      firstValid,
      errors
    } = this.state;
    return (
      <React.Fragment>
        <div className="" id="main-content">
          <div className="wrapper">
            <div className="container-fluid">
              <div className="body-content">
                <div className="tomCard">
                  <div className="tomCardHead">
                    <h5 className="float-left">Therapy</h5>
                  </div>
                  <div className="tab-menu-content">
                    <div class="tab-menu-title">
                      <ul>
                        <li className={this.state.tabone ? "menu1 active" : ""}>
                          <a href="#" onClick={this.tabOne}>
                            <p>List Of Therapy</p>
                          </a>
                        </li>
                        <li className={this.state.tabtwo ? "menu1 active" : ""}>
                          <a href="#" onClick={this.tabTwo}>
                            <p>Attached Files</p>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>

                  {tabone && (
                    <div className="tab-menu-content-section">
                      <div
                        id="content-1"
                        className={tabone ? "d-block" : "d-none"}
                      >
                        <button
                          type="button"
                          onClick={this.therapypop}
                          className="commonBtn2 float-right mb-2"
                        >
                          <i className="fa fa-plus" /> Add New Entry
                         </button>
                        <div className="table-responsive">
                          <table className="table table-hover">
                            <thead className="thead-default">
                              <tr>
                                <th>Title</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {patientTherapyListData.length > 0 ? (
                                patientTherapyListData.map((each, i) => (
                                  <tr key={i}>
                                    <td data-label="startDate">{each.title}</td>
                                    <td data-label="Action">
                                      <a
                                        title="Edit"
                                        className="editBtn"
                                        onClick={e => this.modalPopupOpen(each)}
                                      >
                                        <span id={5} className="fa fa-edit" />
                                      </a>
                                      <a
                                        title="Delete"
                                        className="deleteBtn"
                                        onClick={e => this.deleteTherapy(each)}
                                      >
                                        <i
                                          className="fa fa-trash"
                                          data-toggle="modal"
                                          data-target="#myModal2"
                                        />
                                      </a>
                                    </td>
                                  </tr>
                                ))
                              ) : (
                                  <tr>No data available</tr>
                                )}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  )}

                  {tabtwo && (
                    <div
                      id="content-2"
                      className={tabtwo ? "d-block" : "d-none"}
                    >
                      <button
                        type="button"
                        className="commonBtn2 float-right mb-2"
                        onClick={() =>
                          this.setState({
                            uploadPopup: !this.state.uploadPopup
                          })
                        }
                      >
                        <i className="fa fa-plus" /> <span> Upload new files</span>
                      </button>
                      <div className="table-responsive">
                        <table className="table table-hover">
                          <thead className="thead-default">
                            <tr>
                              <th>#</th>
                              <th>Name</th>
                            <th>FileName</th>
                            <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.props.getFilesDetails.fileDetails &&
                              this.props.getFilesDetails.fileDetails.map(
                                (each, i) => (
                                  <tr>
                                    <td data-label="#">{i + 1}</td>
                                    <td data-label="Name">
                                      {each.name}
                                    </td>
                                    <td data-label="FileName"> {each.fileName}</td>

                                    <td data-label="Action">
                                      <button
                                        class="download_btn"
                                        onClick={() =>
                                          this.handleAwsFiles(each.fileUrl)
                                        }
                                      >
                                        <i class="fas fa-download" /> 
                                      </button>
                                      <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.props.handleHealthDataDelete(each.id, each.fileUrl, "Therapy")
                                      }
                                    >
                                      <i class="fas fa-trash" />
                                    </button>
                                    </td>
                                  </tr>
                                )
                              )}
                            {/* <tr>
                              <td data-label="#">1</td>
                              <td data-label="Name">
                                Eye Care Test Report
                              </td>
                              <td data-label="Type">png</td>
                              <td data-label="Size">87.9 kB</td>
                              <td data-label="Date">16.05.2019 11:56</td>
                              <td data-label="Action">
                                <a
                                  title="Edit"
                                  className="editBtn"
                                  data-toggle="modal"
                                  data-target="#myModal"
                                >
                                  <i className="fa fa-edit" />
                                </a>
                                <a
                                  title="Delete"
                                  className="deleteBtn"
                                  onClick={e => this.deletePatient()}
                                >
                                  <i
                                    className="fa fa-trash"
                                    data-toggle="modal"
                                    data-target="#myModal2"
                                  />
                                </a>
                              </td>
                            </tr> */}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  )}
                </div>

                <div
                  className={addtherapypop ? "modal d-block" : "modal"}
                  id="myModal"
                >
                  <div class="modal-dialog">
                    <div class="modal-content modalPopUp">
                      <div class="modal-header borderNone">
                        <h5 class="modal-title">Create New Entry</h5>
                        <button
                          type="button"
                          className="close"
                          data-dismiss="modal"
                          onClick={this.modalPopUpClose}
                        >
                          <i class="fas fa-times" />
                        </button>
                      </div>
                      <div class="modal-body">
                        <form>
                          <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Title for list entry (e.g. migraine){" "}
                                  <sup>*</sup>
                                </label>
                                <input
                                  className="form-control"
                                  name="title"
                                  onChange={this.handleChange}
                                  value={title}
                                  type="text"
                                />
                                <div style={{ color: "red" }}>
                                  {errors.title}
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Name <sup>*</sup>
                                </label>
                                <input
                                  className="form-control"
                                  name="name"
                                  onChange={this.handleChange}
                                  value={name}
                                  type="text"
                                />
                                <div style={{ color: "red" }}>
                                  {errors.name}
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">Comment</label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="comment"
                                  onChange={this.handleChange}
                                  type="text"
                                  value={comment}
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Diagnosis code ICD
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="diagnosiscode"
                                  onChange={this.handleChange}
                                  type="text"
                                  value={diagnosiscode}
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <button
                                type="button"
                                class="commonBtn float-right"
                                onClick={this.handleTherapy}
                                disabled={!firstValid}
                              >
                                Add
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  className={edittherapy ? "modal d-block" : "modal"}
                  id="myModal"
                >
                  <div class="modal-dialog">
                    <div class="modal-content modalPopUp">
                      <div class="modal-header borderNone">
                        <h5 class="modal-title">Edit Therapy</h5>
                        <button
                          type="button"
                          className="close"
                          onClick={this.modalPopUpClose}
                        >
                          <i class="fas fa-times" />
                        </button>
                      </div>
                      <div class="modal-body">
                        <form>
                          <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Title for list entry (e.g. migraine)
                                </label>
                                <input
                                  className="form-control"
                                  name="title"
                                  onChange={this.handleChange}
                                  defaultValue={this.state.editDetails.title}
                                  type="text"
                                  readOnly
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">Name</label>
                                <input
                                  className="form-control"
                                  name="name"
                                  onChange={this.handleChange}
                                  defaultValue={this.state.editDetails.name}
                                  type="text"
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">Comment</label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="comment"
                                  onChange={this.handleChange}
                                  type="text"
                                  defaultValue={this.state.editDetails.comment}
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Diagnosis code ICD
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="diagnosiscode"
                                  onChange={this.handleChange}
                                  type="text"
                                  defaultValue={
                                    this.state.editDetails.diagnosiscode
                                  }
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <button
                                type="button"
                                class="commonBtn float-right"
                                onClick={this.updateTherapy}
                              >
                                Update
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            className={this.state.uploadPopup ? "modal d-block" : "modal"}
            id="myModal"
          >
            <div class="modal-dialog">
              <div class="modal-content modalPopUp">
                <div class="modal-header borderNone">
                  <h5 class="modal-title">Upload File</h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    onClick={() =>
                      this.setState({
                        uploadPopup: !this.state.uploadPopup
                      })
                    }
                  >
                    <i class="fas fa-times" />
                  </button>
                </div>
                <div class="modal-body">
                  <form>
                    <div class="row">
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="form-group">
                          <label className="form-label">File name</label>
                          <input
                            className="form-control"
                            type="text"
                            name="name"
                            onChange={this.handleChange}
                          />
                          <span />
                        </div>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="fileUp">
                          <label
                            for="fileUp1"
                            class="commonBtn text-center upload"
                          >
                            Upload File
                          </label>
                          <input
                            type="file"
                            id="fileUp1"
                            style={{ display: "none" }}
                            onChange={this.handleChangePopup}
                          />
                          {/* <p style={{ textAlign: "center" }}>hsfkjhsfj</p> */}
                        </div>
                      </div>

                      <div class="col-md-12 col-lg-12 col-sm-12 mt-2">
                        <button
                          type="button"
                          class="commonBtn float-right"
                          onClick={this.popupSubmit} disabled={!this.state.fileURL}
                        >
                          Add
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  addtherapylist: state.patientReducer.addtherapylist,
  gettherapylist: state.patientReducer.gettherapylist,
  updatetherapylist: state.patientReducer.updatetherapylist,
  deletetherapylist: state.patientReducer.deletetherapylist,
  loginDetails: state.loginReducer.loginDetails,
  getFilesDetails: state.uploadReducer.getFilesDetails
});

const mapDispatchToProps = dispatch => ({
  createTherapyData: createtherapy =>
    dispatch(createTherapyData(createtherapy)),
  listTherapyData: listtherapy => dispatch(listTherapyData(listtherapy)),
  updateTherapyData: updatetherapy =>
    dispatch(updateTherapyData(updatetherapy)),
  deleteTherapyData: deletetherapy =>
    dispatch(deleteTherapyData(deletetherapy)),
  getUploadFiles: payload => dispatch(getUploadFiles(payload)),
  handleHealthDataDelete: (id, fileUrl, type) => dispatch(handleHealthDataDelete(id, fileUrl, type))

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Therapy);
