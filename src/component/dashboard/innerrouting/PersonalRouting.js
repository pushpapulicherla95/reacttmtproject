import React, { Component } from 'react';
import PersonalData from "../../patient/personal/personaldata/personaldata";
import { Switch, Route, Redirect } from 'react-router-dom';
import coredata from "../../patient/personal/personaldata/coredata";
import job from "../../patient/personal/personaldata/job";
import emergency from "../../patient/personal/personaldata/emergency";
import Benefits from "../../patient/personal/personaldata/Benefits";
import sports  from "../../patient/personal/personaldata/sports";
import Pregnancies from "../../patient/personal/personaldata/Pregnancies";


class InnerRouting extends Component {

    constructor(props){
        super(props);
        this.state = {
            showPersonal :""
        }
    }

    render() {
        return (
            <Switch>
                <Redirect path="/personal" exact to="/personal/data" />
                <Route path="/personal/data" component={PersonalData} />
                <Route path="/personal/coredata" component={coredata}/>
                <Route path="/personal/job" component={job}/>
                <Route path="/personal/emergency" component={emergency}/>
                <Route path="/personal/benifit" component={Benefits}/>
                <Route path="/personal/sports" component={sports}/>
                <Route path="/personal/pregancies" component={Pregnancies}/>
            </Switch>
        )
    }
}

export default InnerRouting;