import actionType from '../doctor/actionType';


const initialState = {
    listdoctorDetails: "",
    listscheduleDetails: "",
    deletescheduleDetails: "",
    updatescheduleDetails: "",
    doctorProfile: "",
    listpatientDetails: "",
    listcountryDetails: "",
    liststateDetails: "",
    doctorManageAddList: "",
    listcityDetails:"",
    listareaDetails:"",
    listpostalcodeDetails:"",
}

const doctorReducer = (state = initialState, action) => {

    switch (action.type) {
        case actionType.ADD_DOCTOR_INFO_SUCCESS:
            return {
                ...state
            }
        case actionType.ADD_DOCTOR_INFO_FAILURE:
            return {
                ...state
            }
        case actionType.UPDATE_SCHEDULE_SUCCESS:
            return {
                ...state,
                updatescheduleDetails: action.payload,
            }
        case actionType.UPDATE_SCHEDULE_FAILURE:
            return {
                ...state,
                updatescheduleDetails: action.payload,

            }
        case actionType.DELETE_SCHEDULE_SUCCESS:
            return {
                ...state,
                deletescheduleDetails: action.payload,

            }
        case actionType.DELETE_SCHEDULE_FAILURE:
            return {
                ...state,
                deletescheduleDetails: action.payload,

            }
        case actionType.LIST_DOCTOR_INFO_SUCCESS:
            return {
                ...state,
                listdoctorDetails: action.payload,
            }
        case actionType.LIST_DOCTOR_INFO_FAILURE:
            return {
                ...state,
                listdoctorDetails: action.payload,
            }
        case actionType.LIST_SCHEDULE_SUCCESS:
            return {
                ...state,
                listscheduleDetails: action.payload,
            }
        case actionType.LIST_SCHEDULE_FAILURE:
            return {
                ...state,
                listscheduleDetails: action.payload,
            }
        case actionType.GET_DOC_PROFILE_SUCCESS:
            return {
                ...state,
                doctorProfile: action.payload
            }
        case actionType.GET_DOC_PROFILE_FAILURE:
            return {
                ...state,
                doctorProfile: action.error
            }
        case actionType.SAVE_DOC_PROFILE_SUCCESS:
            return {
                ...state,
            }
        case actionType.SAVE_DOC_PROFILE_FAILURE:
            return {
                ...state
            }
        case actionType.GET_COUNTRY_SUCCESS:
            return {
                ...state,
                listcountryDetails: action.payload,
            }
        case actionType.GET_COUNTRY_FAILURE:
            return {
                ...state,
                listcountryDetails: action.payload,

            }
        //state
        case actionType.GET_STATE_SUCCESS:

            return {
                ...state,
                liststateDetails: action.payload,
            }
        case actionType.GET_STATE_FAILURE:
            return {
                ...state,
                liststateDetails: action.payload,

            }
            //area
            case actionType.GET_AREA_SUCCESS:

            return {
                ...state,
                listareaDetails: action.payload,
            }
        case actionType.GET_AREA_FAILURE:
            return {
                ...state,
                listareaDetails: action.payload,

            }
            //postalcode
            case actionType.GET_POSTALCODE_SUCCESS:

            return {
                ...state,
                listpostalcodeDetails: action.payload,
            }
        case actionType.GET_POSTALCODE_FAILURE:
            return {
                ...state,
                listpostalcodeDetails: action.payload,

            }
            //city
            case actionType.GET_CITY_SUCCESS:

            return {
                ...state,
                listcityDetails: action.payload,
            }
        case actionType.GET_CITY_FAILURE:
            return {
                ...state,
                listcityDetails: action.payload,

            }
        case actionType.DOCTOR_MANAGE_ADD_SUCCESS:
            return {
                ...state
            }
        case actionType.DOCTOR_MANAGE_ADD_FAILURE:
            return {
                ...state
            }
        case actionType.DOCTOR_MANAGE_EDIT_SUCCESS:
            return {
                ...state
            }
        case actionType.DOCTOR_MANAGE_EDIT_FAILURE:
            return {
                ...state
            }
        case actionType.DOCTOR_MANAGE_LIST_SUCCESS:
            return {
                ...state,
                doctorManageAddList: action.payload
            }
        case actionType.DOCTOR_MANAGE_LIST_FAILURE:
            return {
                ...state,
                doctorManageAddList: action.error
            }
        case actionType.DOCTOR_MANAGE_DELETE_SUCCESS:
            return {
                ...state
            }
        case actionType.DOCTOR_MANAGE_DELETE_FAILURE:
            return {
                ...state
            }
        default:
            return state;
    }
}
export default doctorReducer;