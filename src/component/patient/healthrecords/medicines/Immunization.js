import React, { Component } from "react";
import { connect } from "react-redux";
import {
  addImmunizationdata,
  deleteImmunizationdata,
  editImmunizationdata
} from "../../../../service/healthrecord/action";
import { getUploadFiles ,handleHealthDataDelete} from "../../../../service/upload/action";

import { getPatientHealthData } from "../../../../service/patient/action";
import Select from "react-select";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";
import axios from "axios";
import { onlyDate } from "../../personal/KeyValidation";
import URL from "../../../../asset/configUrl";
import { awsFiles } from "../../../../service/common/action";

class Immunization extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 1,
      tabone: true,
      tabtwo: false,
      description: "",
      title: "",
      addimmunizationpop: false,
      patientAccidentListData: [],
      editimmunization: false,
      editDetails: {},
      startDate: new Date(),
      hospitalListData: [],
      RenewalDate: "",
      uploadPopup: "",
      name: "",
      fileName: "",
      fileType: "",
      fileURL: "",
      errors: {
        title: "",
        description: ""
      },
      titleValid: false,
      descriptionValid: false,
      firstValid: false
    };
  }

  handleAwsFiles = value => {
    awsFiles(value).then(response => {
      window.location.href = response;
    });
  };

  handleChangePopup = e => {
    var reader = new FileReader();
    var file = e.target.files[0];
    let name = file.name.split('.')[0]
    let type = "." + file.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileURL: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(file);
  };
  popupSubmit = () => {
    var phrase = this.state.fileURL;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);
    var { userInfo } = this.props.loginDetails;
    const payload = {
      uid: userInfo.userId,
      z: match[1],
      fileName: this.state.fileName,
      fileType: this.state.fileType,
      name: this.state.name,
      category: "immunization"
    };
    axios
      .post(URL.FILEIMMUNIZATION_UPLOAD, payload)
      .then(response => {
        if (response.data.status === "success") {
          var { userInfo } = this.props.loginDetails;
          const uploadFiles = {
            uid: userInfo.userId,
            category: "immunization"
          };
          this.props.getUploadFiles(uploadFiles);
          this.setState({ uploadPopup: false });
        }
      })
      .catch(error => {
        this.setState({ uploadPopup: true });
      });
  };
  handleChangedate = date => {
    this.setState({
      appointmentDate: date
    });
  };
  //
  handleChangerenewal = date => {
    this.setState({
      RenewalDate: date
    });
  };
  //
  updatedaterenewal = date => {
    this.setState({ selectedrenewal: date });
  };
  updatedate = date => {
    this.setState({ selected: date });
  };
  componentWillMount() {
    const { userInfo } = this.props.loginDetails;

    const userinfo = {
      uid: userInfo.userId
    };
    //  var { userInfo } = this.props.loginDetails;
    const uploadFiles = {
      uid: userInfo.userId,
      category: "immunization"
    };
    this.props.getUploadFiles(uploadFiles);
    this.props.getPatientHealthData(userinfo);
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.patientHealthList &&
      nextProps.patientHealthList.immunizations != null
    ) {
      const hospitalList = nextProps.patientHealthList.immunizations;
      let parsedContent = JSON.parse(hospitalList);
      let hospitalListData = parsedContent.immunizations;

      this.setState({
        hospitalListData
      });
    }
  }
  //validation
  validateField(fieldName, value) {
    const { errors, titleValid, descriptionValid } = this.state;

    let titlevalid = titleValid;
    let descriptionvalid = descriptionValid;
    let fieldValidationErrors = errors;
    switch (fieldName) {
      case "title":
        titlevalid = !(value.length < 3 || value.length > 20);
        fieldValidationErrors.title = titlevalid
          ? ""
          : "Title should be 3 to 20 characters";
        break;
      case "description":
        descriptionvalid = !(value.length < 3 || value.length > 20);
        fieldValidationErrors.description = descriptionvalid
          ? ""
          : "Description should be 3 to 20 characters";
        break;
    }
    this.setState(
      {
        errors: fieldValidationErrors,
        titleValid: titlevalid,
        descriptionValid: descriptionvalid
      },
      this.validateForm
    );
  }

  validateForm() {
    const { titleValid, descriptionValid } = this.state;
    this.setState({
      firstValid: titleValid && descriptionValid
    });
  }

  tabOne = () => {
    this.setState({ tabone: true, tabtwo: false });
  };
  tabTwo = () => {
    this.setState({ tabone: false, tabtwo: true });
  };
  handleChange = e => {
    e.preventDefault();
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };
  immunizationpop = () => {
    this.setState({ addimmunizationpop: true }, () => {
      this.emptystate();
    });
  };
  modalPopUpClose = () => {
    this.setState({
      addimmunizationpop: false,
      editimmunization: false
    });
  };
  modalPopupOpen = e => {
    this.setState({ editimmunization: true, editDetails: e });
    this.setState({
      title: e.title,
      description: e.description,
      appointmentDate: e.appointmentDate,
      RenewalDate: e.RenewalDate
    });
  };

  handleImmunization = () => {
    const { title, appointmentDate, RenewalDate, description } = this.state;
    const { userInfo } = this.props.loginDetails;

    const createimmunization = {
      uid: userInfo.userId,
      immunizations: [
        {
          title: title,
          appointmentDate: appointmentDate,
          RenewalDate: RenewalDate,
          description: description
        }
      ]
    };
    // console.log("createAccidentData", createaccident)
    this.props.addImmunizationdata(createimmunization);

    this.setState({
      addimmunizationpop: false
    });
  };
  updateImmunization = () => {
    const { title, appointmentDate, RenewalDate, description } = this.state;
    const { userInfo } = this.props.loginDetails;

    const updateimmunization = {
      uid: userInfo.userId,
      immunizations: [
        {
          title: title,
          appointmentDate: appointmentDate,
          RenewalDate: RenewalDate,
          description: description
        }
      ]
    };
    // console.log("updateAccidentData", updateaccident)
    this.props.editImmunizationdata(updateimmunization);

    this.setState({ editimmunization: false }, () => {
      this.emptystate();
    });
  };
  deleteImmunization = e => {
    const { userInfo } = this.props.loginDetails;

    const deleteimmunization = {
      uid: userInfo.userId,
      immunizations: [
        {
          title: e.title,
          appointmentDate: e.appointmentDate,
          RenewalDate: e.RenewalDate,
          description: e.description
        }
      ]
    };
    // console.log("deleteAccidentData", this.deleteaccident)
    this.props.deleteImmunizationdata(deleteimmunization);
  };
  emptystate = () => {
    this.setState({
      title: "",
      appointmentDate: "",
      RenewalDate: "",
      description: ""
    });
  };

  render() {
    const {
      tabone,
      tabtwo,
      title,
      description,
      addimmunizationpop,
      hospitalListData,
      editimmunization,
      appointmentDate,
      selected,
      selectedrenewal,
      RenewalDate,
      firstValid,
      errors
    } = this.state;
    return (
      <React.Fragment>
        <div className="" id="main-content">
          <div className="wrapper">
            <div className="container-fluid">
              <div className="body-content">
                <div className="tomCard">
                  <div className="tomCardHead">
                    <h5 className="float-left">Immunization </h5>
                  </div>

                  <div className="tab-menu-content">
                    <div class="tab-menu-title">
                      <ul>
                        <li
                          className={
                            this.state.tabone ? "menu1 active" : ""
                          }
                        >
                          <a href="#" onClick={this.tabOne}>
                            <p>List of Immunization</p>
                          </a>
                        </li>
                        <li
                          className={
                            this.state.tabtwo ? "menu1 active" : ""
                          }
                        >
                          <a href="#" onClick={this.tabTwo}>
                            <p>Attached Files</p>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>

                  {tabone && (
                    <div className="tab-menu-content-section">
                      <div
                        id="content-1"
                        className={tabone ? "d-block" : "d-none"}
                      >
                        <button
                          type="button"
                          onClick={this.immunizationpop}
                          className="commonBtn2 float-right mb-2"
                        >
                          <i className="fa fa-plus" /> Add New Entry
                         </button>
                        <div className="table-responsive">
                          <table className="table table-hover">
                            <thead className="thead-default">
                              <tr>
                                <th>Title</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {hospitalListData.length > 0 ? (
                                hospitalListData.map((each, i) => (
                                  <tr key={i}>
                                    <td data-label="startDate">
                                      {each.title}
                                    </td>

                                    <td data-label="Action">
                                      <a
                                        title="Edit"
                                        className="editBtn"
                                        onClick={e =>
                                          this.modalPopupOpen(each)
                                        }
                                      >
                                        <span
                                          id={5}
                                          className="fa fa-edit"
                                        />
                                      </a>
                                      <a
                                        title="Delete"
                                        className="deleteBtn"
                                        onClick={e =>
                                          this.deleteImmunization(each)
                                        }
                                      >
                                        <i
                                          className="fa fa-trash"
                                          data-toggle="modal"
                                          data-target="#myModal2"
                                        />
                                      </a>
                                    </td>
                                  </tr>
                                ))
                              ) : (
                                  <tr>No data available</tr>
                                )}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  )}

                  {tabtwo && (
                    <div
                      id="content-2"
                      className={tabtwo ? "d-block" : "d-none"}
                    >
                      <button
                        type="button"
                        className="commonBtn2 float-right mb-2"
                        onClick={() =>
                          this.setState({
                            uploadPopup: !this.state.uploadPopup
                          })
                        }
                      >
                        <i className="fa fa-plus" /> <span> Upload new files</span>
                      </button>
                      <div className="table-responsive">
                        <table className="table table-hover">
                          <thead className="thead-default">
                            <tr>
                              <th>#</th>
                              <th>Name</th>
                            <th>FileName</th>
                            <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.props.getFilesDetails.fileDetails &&
                              this.props.getFilesDetails.fileDetails.map(
                                (each, i) => (
                                  <tr>
                                    <td data-label="#">{i + 1}</td>
                                    <td data-label="Name">{each.name}</td>
                                    <td data-label="FileName"> {each.fileName}</td>

                                    <td data-label="Action">
                                      <button
                                        class="download_btn"
                                        onClick={() =>
                                          this.handleAwsFiles(each.fileUrl)
                                        }
                                      >
                                        <i class="fas fa-download" />{" "}
                                        
                                      </button>
                                      <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.props.handleHealthDataDelete(each.id, each.fileUrl, "immunization")
                                      }
                                    >
                                      <i class="fas fa-trash" />
                                    </button>
                                    </td>
                                  </tr>
                                )
                              )}
                            {/* <tr>
                              <td data-label="#">1</td>
                              <td data-label="Name">Eye Care Test Report</td>
                              <td data-label="Type">png</td>
                              <td data-label="Size">87.9 kB</td>
                              <td data-label="Date">16.05.2019 11:56</td>
                              <td data-label="Action">
                                <a
                                  title="Edit"
                                  className="editBtn"
                                  data-toggle="modal"
                                  data-target="#myModal"
                                >
                                  <i className="fa fa-edit" />
                                </a>
                                <a
                                  title="Delete"
                                  className="deleteBtn"
                                  onClick={e => this.deletePatient()}
                                >
                                  <i
                                    className="fa fa-trash"
                                    data-toggle="modal"
                                    data-target="#myModal2"
                                  />
                                </a>
                              </td>
                            </tr> */}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  )}
                </div>

                <div
                  className={addimmunizationpop ? "modal d-block" : "modal"}
                  id="myModal"
                >
                  <div class="modal-dialog">
                    <div class="modal-content modalPopUp">
                      <div class="modal-header borderNone">
                        <h5 class="modal-title">Add Immunization</h5>
                        <button
                          type="button"
                          className="close"
                          data-dismiss="modal"
                          onClick={this.modalPopUpClose}
                        >
                          <i class="fas fa-times" />
                        </button>
                      </div>
                      <div class="modal-body">
                        <form>
                          <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Title for list entry (e.g. Vaccaniation){" "}
                                  <sup>*</sup>
                                </label>
                                <input
                                  className="form-control"
                                  name="title"
                                  onChange={this.handleChange}
                                  value={title}
                                  type="text"
                                />
                                <div style={{ color: "red" }}>
                                  {errors.title}
                                </div>
                              </div>
                            </div>

                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Immunization Type <sup>*</sup>
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="description"
                                  onChange={this.handleChange}
                                  type="text"
                                  value={description}
                                />
                                <div style={{ color: "red" }}>
                                  {errors.description}
                                </div>

                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div className="form-group">
                                <label className="form-label">Date</label>
                                <input
                                  type="date"
                                  className="form-control"
                                  value={appointmentDate}
                                  onChange={this.handleChange}
                                  name="appointmentDate"
                                  onKeyPress={onlyDate}
                                />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div className="form-group">
                                <label className="form-label">
                                  Renewal date
                                </label>
                                <input
                                  type="date"
                                  className="form-control"
                                  value={RenewalDate}
                                  onChange={this.handleChange}
                                  name="RenewalDate"
                                  onKeyPress={onlyDate} min={appointmentDate}
                                />
                              </div>
                            </div>

                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <button
                                type="button"
                                class="commonBtn float-right"
                                onClick={this.handleImmunization}
                                disabled={!firstValid}
                              >
                                Add
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  className={editimmunization ? "modal d-block" : "modal"}
                  id="myModal"
                >
                  <div class="modal-dialog">
                    <div class="modal-content modalPopUp">
                      <div class="modal-header borderNone">
                        <h5 class="modal-title">Edit Immunization</h5>
                        <button
                          type="button"
                          className="close"
                          onClick={this.modalPopUpClose}
                        >
                          <i class="fas fa-times" />
                        </button>
                      </div>
                      <div class="modal-body">
                        <form>
                          <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Title for list entry (e.g. Vaccaniation)
                                </label>
                                <input
                                  className="form-control"
                                  name="title"
                                  onChange={this.handleChange}
                                  defaultValue={
                                    this.state.editDetails.title
                                  }
                                  type="text"
                                  readOnly
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Immunization Type
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="description"
                                  onChange={this.handleChange}
                                  type="text"
                                  defaultValue={
                                    this.state.editDetails.description
                                  }
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div className="form-group">
                                <label className="form-label">
                                  AppointmentDate
                                </label>
                                <input
                                  type="date"
                                  className="form-control"
                                  defaultValue={
                                    this.state.editDetails.appointmentDate
                                  }
                                  onChange={this.handleChange}
                                  name="appointmentDate"
                                  onKeyPress={onlyDate}
                                />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div className="form-group">
                                <label className="form-label">
                                  Renewal date
                                </label>
                                <input
                                  type="date"
                                  className="form-control"
                                  defaultValue={
                                    this.state.editDetails.RenewalDate
                                  }
                                  onChange={this.handleChange}
                                  name="RenewalDate"
                                  onKeyPress={onlyDate} min={appointmentDate}
                                />
                              </div>
                            </div>

                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <button
                                type="button"
                                class="commonBtn float-right"
                                onClick={this.updateImmunization}
                                //  disabled={!this.state.fileURL}
                              >
                                Update
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            className={this.state.uploadPopup ? "modal d-block" : "modal"}
            id="myModal"
          >
            <div class="modal-dialog">
              <div class="modal-content modalPopUp">
                <div class="modal-header borderNone">
                  <h5 class="modal-title">Upload File</h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    onClick={() =>
                      this.setState({
                        uploadPopup: !this.state.uploadPopup
                      })
                    }
                  >
                    <i class="fas fa-times" />
                  </button>
                </div>
                <div class="modal-body">
                  <form>
                    <div class="row">
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="form-group">
                          <label className="form-label">File name</label>
                          <input
                            className="form-control"
                            type="text"
                            name="name"
                            onChange={this.handleChange}
                          />
                          <span />
                        </div>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="fileUp">
                          <label
                            for="fileUp1"
                            class="commonBtn text-center upload"
                          >
                            Upload File
                          </label>
                          <input
                            type="file"
                            id="fileUp1"
                            style={{ display: "none" }}
                            onChange={this.handleChangePopup}
                          />
                          {/* <p style={{ textAlign: "center" }}>hsfkjhsfj</p> */}
                        </div>
                      </div>

                      <div class="col-md-12 col-lg-12 col-sm-12 mt-2">
                        <button
                          type="button"
                          class="commonBtn float-right"
                          onClick={this.popupSubmit}
                        >
                          Add
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  patientHealthList: state.patientReducer.patientHealthList,
  loginDetails: state.loginReducer.loginDetails,
  getFilesDetails: state.uploadReducer.getFilesDetails
});

const mapDispatchToProps = dispatch => ({
  addImmunizationdata: createimmunization =>
    dispatch(addImmunizationdata(createimmunization)),
  deleteImmunizationdata: deleteimmunization =>
    dispatch(deleteImmunizationdata(deleteimmunization)),
  getPatientHealthData: userinfo => dispatch(getPatientHealthData(userinfo)),
  editImmunizationdata: updateimmunization =>
    dispatch(editImmunizationdata(updateimmunization)),
  getUploadFiles: payload => dispatch(getUploadFiles(payload)),
  handleHealthDataDelete: (id, fileUrl, type) => dispatch(handleHealthDataDelete(id, fileUrl, type))

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Immunization);
