import React, { Component } from 'react';
class TableText extends Component {
    render() {
        return (
            <div id="content-1">
                {/* <div class="search-input">
                                          <button>Download</button>
                                      </div> */}
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead class="thead-default">
                            <tr>
                                <th>TableName</th>
                                <th>F</th>
                                <th>PR</th>
                                <th>df</th>
                                <th>mean_sq</th>
                                <th>sum_sq</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.props.tableList && this.props.tableList.map((each, i) =>
                                    <tr>
                                        <td data-label="Company" class="pad-less">{each.tableName}</td>
                                        <td data-label="Company" class="pad-less">{each.value.F}</td>
                                        <td data-label="Company" class="pad-less">{each.value.PR}</td>
                                        <td data-label="Company" class="pad-less">{each.value.df}</td>
                                        <td data-label="Company" class="pad-less">{each.value.mean_sq}</td>
                                        <td data-label="Company" class="pad-less">{each.value.sum_sq}</td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
                <div class="search-input">
                    <button class="download_btn" onClick={() => window.print()}> Download</button>
                </div>
            </div>
        );
    }
}
export default TableText