const express = require('express');
const subscriptionRouter = express.Router();
const { createProduct, createSubscription, getPlans, deletePlan } = require('../controller/payment/stripe.api');
//const { createUserSubscription, createWebhook, deleteUserSubscription, getUserSubscriptionList,updatingUserCard,adminSubscriptionList } = require('../controllers/userSubscription');

subscriptionRouter.route('/addProduct')
    .post(createProduct)
// subscriptionRouter.route('/createSubscription')
//     .post(createSubscription)
subscriptionRouter.route('/planList')
    .get(getPlans)
subscriptionRouter.route('/deletePlan')
    .post(deletePlan)

//     // user subscribing plan
// subscriptionRouter.route('/addSubscription')
//     .post(createUserSubscription)
// subscriptionRouter.route('/webhook')
//     .post(createWebhook)
// subscriptionRouter.route('/deleteSubscription')
//     .post(deleteUserSubscription)
// subscriptionRouter.route('/getUserSubscription')
//     .get(getUserSubscriptionList) 
// subscriptionRouter.route('/updateCard')
//     .post(updatingUserCard) 
// subscriptionRouter.route('/adminSubscriptionCount')
//     .get(adminSubscriptionList)

module.exports = subscriptionRouter;
