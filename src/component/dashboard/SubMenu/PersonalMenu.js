import React, { Component } from 'react';
import { NavLink } from "react-router-dom";

class PersonalMenu extends Component {
    render() {
        return (
            <div className="mainMenuSide" id="main-content">
                <div className="wrapper">
                    <div className="container-fluid">
                        <div className="body-content">
                            <div className="row">
                                <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                                    <a href="javaScript:void(0);" onClick={() => this.props.history.push({pathname:'/personal/data',state:{submenuType:"Personal"}})} className="card bg-c submenuIcon">
                                        <div className="card-img1 submenuIconImg ">
                                            <img src="images/personal-color.png" />
                                        </div>
                                        <div className="card-title">
                                            <h5>Personal Data</h5>
                                        </div>
                                    </a>
                                </div>
                                <div className="col-lg-3 col-md-3 col-sm-6 col-6 padLeft">
                                    <a href="javaScript:void(0);" onClick={() => this.props.history.push({pathname:'/curriculam/newcurriculam'})} className="card bg-c submenuIcon">
                                        <div className="card-img1 submenuIconImg">
                                            <img src="images/curriculum-color.png" />
                                        </div>
                                        <div className="card-title">
                                            <h5>Curriculum</h5>
                                        </div>
                                    </a>
                                </div>
                                <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                                    <a href="javaScript:void(0);" onClick={() => this.props.history.push({pathname:'/mydoctor/mydoctorlist'})} className="card bg-c submenuIcon">
                                        <div className="card-img1 submenuIconImg">
                                            <img src="images/doctors-color.png" />
                                        </div>
                                        <div className="card-title">
                                            <h5>My Doctors</h5>
                                        </div>
                                    </a>
                                </div>
                                <div className="col-lg-3 col-md-3 col-sm-6 col-6 padLeft">
                                <a href="javaScript:void(0);"  className="card bg-c submenuIcon" onClick={() => this.props.history.push({pathname:'/insurance/healthInsurance'})}>
                                        <div className="card-img1 submenuIconImg">
                                            <img src="images/insurance-color.png" />
                                        </div>
                                        <div className="card-title">
                                            <h5>Insurance</h5>
                                        </div>
                                        </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}
export default PersonalMenu;