import URL from "../../asset/configUrl";
import axios from "axios";
import actionType from "../../service/insurance/actionType";
import { toastr } from "react-redux-toastr";
import { toastrOptions } from "../../component/common/toasteroptions";
export const healthinsurance = payload => dispatch => {
    const configs = {
      method: "post",
      url: URL.HEALTH_INSURANCE,
      data: payload,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };
  axios(configs).then(res => {
      if (res.status === 200) {
        dispatch({
          type: actionType.HEALTHINSURANCE_SUCCESS,
          payload: res.data
        });
        toastr.success("Success", res.data.message, toastrOptions);
      } else if (res.data.status == "failure") {
        toastr.error("Error", "Error", toastrOptions);
      }
    })
    .catch(error => {
      dispatch({
        type: actionType.HEALTHINSURANCE_FAILURE,
        error: error.response.data
      });
    });
};

export const additionalinsurance = payload => dispatch => {
    const configs = {
      method: "post",
      url: URL.ADDITIONAL_HEALTH_INSURANCE,
      data: payload,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };
  axios(configs).then(res => {
      if (res.status === 200) {
        dispatch({
          type: actionType.ADDITIONALINSURANCE_SUCCESS,
          payload: res.data
        });
        toastr.success("Success", res.data.message, toastrOptions);
      } else if (res.data.status == "failure") {
        toastr.error("Error", "Error", toastrOptions);
      }
    })
    .catch(error => {
      dispatch({
        type: actionType.ADDITIONALINSURANCE_FAILURE,
        error: error.response.data
      });
    });
};

export const accidentinsurance = payload => dispatch => {
    const configs = {
      method: "post",
      url: URL.ACCIDENT_INSURANCE,
      data: payload,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };
  axios(configs).then(res => {
      if (res.status === 200) {
        dispatch({
          type: actionType.ACCIDENTINSURANCE_SUCCESS,
          payload: res.data
        });
        toastr.success("Success", res.data.message, toastrOptions);
      } else if (res.data.status == "failure") {
        toastr.error("Error", "Error", toastrOptions);
      }
    })
    .catch(error => {
      dispatch({
        type: actionType.ACCIDENTINSURANCE_FAILURE,
        error: error.response.data
      });
    });
};

export const getAllinsurance = payload => dispatch => {
    const configs = {
      method: "post",
      url: URL.LIST_PERSONALDATA,
      data: { uid: payload },
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };
  axios(configs).then(res => {
      dispatch({
        type: actionType.GETALLINSURANCE_SUCCESS,
        payload: res.data
      });
    })
    .catch(error => {
      dispatch({
        type: actionType.GETALLINSURANCE_FAILURE,
        error: error
      });
    });
};
