import React, { Component } from "react";
import { insertDoctorInfo, listDoctorInfo} from "../../service/doctor/action";
import {connect} from "react-redux";
class DoctorList extends Component {
    state = {
        name: "",
        qualificaion: "",
        specialist: "",
        phone: "",
        experience: "",
        email: "",
        address: "",
        doctorId: "2",
        openaddDocpop: false,
        editDetails: {},
        userInfoId:"2"
    }
    componentWillMount = () => {
        const listinfo =
        {
            "doctorId": "2"
        }

        this.props.listDoctorInfo(listinfo);
    }
    modalPopUpClose = () => {
        this.setState({ openaddDocpop: false });

    }
    addDoctorinfoopen =()=>{
        this.setState({ openaddDocpop: true })
    }   
     handleChange = (e) => {
        e.preventDefault();
      
        console.log("sdsdsdsd", e.target.value, e.target.name);
        this.setState({ [e.target.name]: e.target.value })
    }
    handleSubmit = e => {
        const { name, qualificaion, specialist, phone, experience, email, address } = this.state;
        const doctorinfo = {
            userInfoId: "2",
            name: name,
            qualificaion: qualificaion,
            specialist: specialist,
            phone: phone,
            experience: experience,
            email: email,
            address: address

        }
        this.props.insertDoctorInfo(doctorinfo);
        this.setState({ openaddDocpop: false });
    }
    render() {
        const {name, qualificaion, specialist, phone, experience, email, address} = this.state;
        const { lists } = this.props.listdoctorDetails;
        return (
            <div className="cui-layout-content">
                <div className="cui-utils-content">
                    <div className="cui-utils-title mb-3"><strong className="text-uppercase font-size-16">List</strong></div>
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="card">
                                <div className="card-header">
                                    <div className="cui-utils-title"><strong>Doctor List</strong>
                                        <button type="button" className="btn commonBtn float-right" onClick={() => this.addDoctorinfoopen()} data-toggle="modal" data-target="#myModal"><i className="fa fa-plus"></i> Add Doctor Info</button>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="table-responsive">
                                        <table className="table table-hover">
                                            <thead className="thead-default">
                                                <tr>
                                                    <th>Name</th>
                                                    <th>qualificaion</th>
                                                    <th>specialist</th>
                                                    <th>phone No</th>
                                                    <th>Experience</th>
                                                    <th>Email</th>
                                                    <th>address</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {lists && lists.map((each,i) => (
                                                                <tr key={i}>
                                                                    <td data-label="Name">{each.firstname}</td>
                                                                    <td data-label="qualificaion">{each.qualification}</td>
                                                                    <td data-label="specialist">{each.specialist}</td>
                                                                    <td data-label="phone ">{each.phonenumber}</td>
                                                                    <td data-label="experience">{each.experience} </td>
                                                                    <td data-label="email">{each.email}</td>
                                                                    <td data-label="address">{each.address1}</td>
                                                                    <td data-label="Action"><a title="Edit" className="editBtn" data-toggle="modal" data-target="#myModal"><i className="fa fa-edit"></i></a>
                                                                        {/* <a title="Delete" className="deleteBtn"><i className="fa fa-trash" data-toggle="modal" data-target="#myModal2"></i></a> */}
                                                                        </td>
                                                                </tr>))
                                                            }

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className={this.state.openaddDocpop ? "modal show d-block" : "modal"} id="edit_btn">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Edit </h4>
                                <button type="button" className="close" data-dismiss="modal" onClick={this.modalPopUpClose}>&times;</button>

                            </div>
                            <div className="modal-body">
                            <form>
                                                    <div className="form-group">
                                                        <label className="form-label">name</label>
                                                        <input className="form-control" name="name" onChange={this.handleChange} value={name} type="text" /><span ></span>
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-label">qualificaion</label>
                                                        <input id="validation-email" className="form-control" name="qualificaion" onChange={this.handleChange} type="text" value={qualificaion} /><span ></span>
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-label">specialist</label>
                                                        <input id="validation-email" className="form-control" name="specialist" onChange={this.handleChange} type="text" value={specialist} /><span ></span>
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-label">phone</label>
                                                        <input id="validation-email" className="form-control" name="phone" onChange={this.handleChange} type="text" value={phone} /><span ></span>
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-label">experience</label>
                                                        <input id="validation-email" className="form-control" name="experience" type="text" onChange={this.handleChange} value={experience} /><span ></span>
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-label">email</label>
                                                        <input id="validation-email" className="form-control" name="email" type="text" onChange={this.handleChange} value={email} /><span ></span>
                                                    </div>
                                                    <div className="form-group">
                                                        <label className="form-label">address</label>
                                                        <input id="validation-email" className="form-control" name="address" type="text" onChange={this.handleChange} value={address} /><span ></span>
                                                    </div>
                                                    <div className="form-actions">
                                                        <button type="button" className="btn commonBtn mr-3" onClick={this.handleSubmit}>Submit</button>

                                                    </div>
                                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn commonBtn" onClick={this.updateSchedule} >Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails,
    listdoctorDetails: state.doctorReducer.listdoctorDetails
  
});

const mapDispatchToProps = dispatch => ({
    insertDoctorInfo: (doctorinfo) => dispatch(insertDoctorInfo(doctorinfo)),
    listDoctorInfo: (listinfo) => dispatch(listDoctorInfo(listinfo))
 
});
export default connect (
    mapStateToProps,
    mapDispatchToProps
)(DoctorList);