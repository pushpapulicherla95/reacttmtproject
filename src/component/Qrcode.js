import React, { Component } from "react";
import axios from "axios";
import AWS from "aws-sdk";
import URL from "../../src/asset/configUrl";
class Qrcode extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: null,
      width: null,
      height: null,
      validMessage: null
    };
  }

  componentWillMount = () => {
    this.setState({ width: window.screen.width, height: window.screen.height });
    const urlParams = new URLSearchParams(window.location.search);
    const uid = urlParams.get("uid");
    const token = urlParams.get("token");
   console.log("uiduid",uid,token)

    this.pdflink(uid, token);

      // 
   
  };


  pdflink = (uid, token) => {
   console.log("uiduid",uid,token)
    const configs = {
      method: "get",
      url: URL.PDF_QRCODE + uid + "/" + token,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };
    axios(configs)
      .then(res => {
        console.log("ressss",res)
        this.setState({ validMessage: res.data.message });
        this.setState({value:  "https://data.tomatomedical.com/QRCode/"+ uid +".pdf"})
      })
      .catch(err => {
        console.log("errrr",err)
      });
  };


  render() {
    const { validMessage } = this.state;
    console.log(this.state)
    return (
      <div>
        {validMessage == "valid" ? (
          <embed
            src={this.state.value}
            width={this.state.width}
            height={this.state.height}
          />
        ) : (
          "Sorry unable to open"
        )}
      </div>
    );
  }
}
export default Qrcode;
