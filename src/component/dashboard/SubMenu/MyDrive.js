import React from 'react';
import {
  FacebookShareButton,
  WhatsappShareButton,
  EmailShareButton,
  FacebookIcon,
  EmailIcon,
  WhatsappIcon
} from 'react-share';
import axios from 'axios'
import { toastr } from "react-redux-toastr";
import URL from '../../../asset/configUrl'
var addMenu;
class MyDrive extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      invitePopUp: false,
      fileUrl: "",
      shareUrl: "",
      tomatoShare: false,
      receiverEmailId: "",
      visible: false,
      selectFile: null,
      copyLinkEnable: false,
      isChecked: false,
      filterDoctorList: [],
      filterDoctorListcopy: [],
      filterName: ""
    }
  }

  tomatoShareSubmit = () => {

    const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
    const configs = {
      method: 'post',
      url: URL.FILESEND_SHAREDATA,
      data: {
        "uid": loginDetails.userInfo.userId,
        "email": loginDetails.userInfo.email,
        "receiverEmailId": this.state.receiverEmailId,
        "files": [this.state.fileUrl],
        "name": "fgsdfgdsgsdf"
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }
    axios(configs)
      .then(response => {
        if (response.data.status == "success") {
          toastr.success("success", response.data.message);
          this.setState({ tomatoShare: false })
        }
      })
      .catch(error => console.log("sdfdsfssssssssss", error.response))
  }

  handleEmailValidation = () => {
    const configs = {
      method: 'post',
      url: URL.EMAILCHECK_SHAREDATA,
      data: { "receiverEmailId": this.state.receiverEmailId },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }
    axios(configs)
      .then(response => {
        if (response.data.status == "success") {

          this.tomatoShareSubmit()
        } else {
          toastr.error("Error", response.data.message);
        }

      })
      .catch((error) => {
        console.log("err", error.response)
      })
  }

  copyToClipboard = () => {
    var textBox = document.getElementById("inviteLink");
    textBox.select();
    document.execCommand("copy");
  }

  generateValidUrl = () => {
    console.log("sdfsdf", document.getElementById("validLink").value)
    const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
    const configs = {
      method: 'post',
      url: URL.LINKURLGENERATE_SHAREDATA,
      data: {
        "uid": loginDetails.userInfo.userId,
        "fileUrl": [this.state.fileUrl],
        "valid": this.state.isChecked ? "" : "nodate"
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }
    axios(configs)
      .then(response => {
        let url = "http://192.168.2.11:3000/shareDataDownload?uid=" + response.data.uid + "&token=" + response.data.token
        console.log("resopsegeneretate", url)

        this.setState({ shareUrl: url })
      })
  }

  filterDoctorList = () => {
    const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
    if (loginDetails.userInfo.userType == "patient") {
      axios.get(URL.ADMIN_DOCTOR_LIST)
        .then(response => {
          this.setState({ filterDoctorList: response.data.doctorList, filterDoctorListcopy: response.data.doctorList })
          console.log("response.data", response.data.doctorList)
        })
    } else if (loginDetails.userInfo.userType == "doctor") {
      axios.get(URL.ADMIN_PATIENT_LIST)
        .then(response => {
          this.setState({ filterDoctorList: response.data.patientList, filterDoctorListcopy: response.data.patientList })
          console.log("response.data", response.data.patientList)
        })
    }

  }

  componentDidMount() {
    document.addEventListener('contextmenu', this._handleContextMenu);
    document.addEventListener('click', this._handleClick);
    document.addEventListener('scroll', this._handleScroll);
  };

  componentWillUnmount() {
    document.removeEventListener('contextmenu', this._handleContextMenu);
    document.removeEventListener('click', this._handleClick);
    document.removeEventListener('scroll', this._handleScroll);
  }

  componentWillMount = () => {
    this.filterDoctorList()
  }

  contextMenu = (event, each) => {
    console.log("SDfsdfsd", event)
    event.preventDefault();

    this.setState({ visible: true, selectFile: each });
    try {
      const clickX = event.clientX;
      const clickY = event.clientY;
      const screenW = window.innerWidth;
      const screenH = window.innerHeight;
      const rootW = this.root.offsetWidth;
      const rootH = this.root.offsetHeight;

      const right = (screenW - clickX) > rootW;
      const left = !right;
      const top = (screenH - clickY) > rootH;
      const bottom = !top;

      if (right) {
        this.root.style.left = `${clickX + 5}px`;
      }

      if (left) {
        this.root.style.left = `${clickX - rootW - 5}px`;
      }

      if (top) {
        this.root.style.top = `${clickY + 5}px`;
      }

      if (bottom) {
        this.root.style.top = `${clickY - rootH - 5}px`;
      }
    } catch
    {

    }

  }

  _handleClick = (event) => {
    const { visible } = this.state;
    const wasOutside = !(event.target.contains === this.root);

    if (wasOutside && visible) this.setState({ visible: false, });
  };

  _handleScroll = () => {
    const { visible } = this.state;

    if (visible) this.setState({ visible: false, });
  };

  toggleChange = (e) => {
    console.log(e.target)
    this.setState({
      isChecked: !this.state.isChecked,
    });
  }

  render() {
    const { visible, selectFile } = this.state;
    return (
      <div> <div class="share_card_new">
        {(visible || null) &&
          <div ref={ref => { this.root = ref }} className="contextMenu">
            <div className="contextMenu--option"><i class="fas fa-cloud-download-alt"></i> Download</div>
            <div className="contextMenu--option" onClick={() => this.setState({ invitePopUp: true, fileUrl: selectFile.fileUrl })}><i class="fas fa-share-alt"></i> Share</div>
            <div className="contextMenu--option" onClick={() => this.props.myDriveListDelete(selectFile.fileName + selectFile.fileType)}><i class="fas fa-trash"></i> Remove</div>
          </div>}
        <div class="share_card_title">
          <h5>My Drive</h5>
          <div class="share_card_search">
            <input type="text" placeholder="Search Drive" />
            <div class="search_icon_input">
              <i class="fas fa-search"></i>
            </div>
          </div>
        </div>

        <div class="file_list_share">
          {this.props.myDriveList.map((each, i) => {
            return <div class="file_list_card" onContextMenu={(e) => this.contextMenu(e, each)}>
              <div class="file_format"> <img src={each.fileType == ".pdf" ? "images/pdf_icon.png" : each.fileType == "zip"
                ? "images/zip_icon.png" : "images/doc_icon.png"} />
                <div id={"share" + i} class="share_option d-none " >
                  <ul>
                    <li><button><i class="fas fa-cloud-download-alt"></i> Download</button></li>
                    <li><button onClick={() => this.setState({ invitePopUp: true, fileUrl: each.fileUrl })}><i class="fas fa-share-alt"></i> Share</button></li>
                    <li><button onClick={() => this.props.myDriveListDelete(each.fileName + each.fileType, i)}><i class="fas fa-trash"></i> Remove</button></li>
                  </ul>
                </div>
              </div>
              <div class="file_details">
                <label>{each.fileName}</label>
                <div class="menu">
                  <a onClick={() => {
                    if (document.getElementById("share" + i).classList.contains("d-none")) {
                      document.getElementById("share" + i).classList.remove("d-none")
                    } else {
                      document.getElementById("share" + i).classList.add("d-none")
                    }
                  }}>  <i class="fas fa-ellipsis-v"></i></a>
                </div>
              </div>
            </div>
          })}
        </div>

      </div>
        {this.state.invitePopUp && <div class="modal fade show" style={{ display: "block" }} >
          <div class="modal-dialog">
            <div class="modal-content modalPopUp">
              <div class="modal-header borderNone">
                <h5 class="modal-title">Invite Link</h5>
                <button type="button" class="popupClose" data-dismiss="modal" onClick={() => this.setState({ invitePopUp: false })}><i class="fas fa-times"></i></button>
              </div>
              <div class="modal-body">
                <div className="modal_tab_share">
                  <button className="active">Share With Social Media</button>
                  <button onClick={() => this.setState({ tomatoShare: !this.state.tomatoShare, invitePopUp: false })}>Share With Tomatomedical</button>
                </div>
                <div className={this.state.copyLinkEnable != true ? "" : " d-none"}>
                  <div class="form-group">
                    <h5>Valid till :</h5>
                    <input class="inp-cbx" id="validLink" type="checkbox" checked={this.state.isChecked} style={{ display: "none" }} onChange={this.toggleChange} />
                    <label class="cbx" for="validLink"><span><svg width="12px" height="10px" viewBox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span><span>Link is Valid for 24 Hours only</span></label></div>
                  <button type="button" class="commonBtn float-right" onClick={() => this.setState({ copyLinkEnable: true }, () => this.generateValidUrl())}>Create</button>
                </div>
                <div className={this.state.copyLinkEnable == true ? "" : "d-none"}>
                  <h6>Link sharing is on</h6>

                  <div class="share-input">
                    <input type="text" id="inviteLink" value={this.state.shareUrl} placeholder="https://www.tomato.colanonline.com" readonly />
                    <button onClick={this.copyToClipboard}>Copy</button>
                  </div>
                  <div class="share_social_icon">
                    <ul>
                      <li>
                        <FacebookShareButton url={this.state.shareUrl} >
                          <FacebookIcon size={32} round={true} />
                        </FacebookShareButton>
                      </li>
                      <li>
                        <WhatsappShareButton url={this.state.shareUrl} >
                          <WhatsappIcon size={32} round={true} />
                        </WhatsappShareButton>
                      </li>
                      <li>
                        <EmailShareButton url={this.state.shareUrl} >
                          <EmailIcon size={32} round={true} />
                        </EmailShareButton>
                      </li>
                    </ul>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>}

        {this.state.tomatoShare && <div class="modal fade show" style={{ display: "block" }} >
          <div class="modal-dialog">
            <div class="modal-content modalPopUp">
              <div class="modal-header borderNone">
                <h5 class="modal-title">With in Tomato</h5>
                <button type="button" class="popupClose" onClick={() => this.setState({ tomatoShare: !this.state.tomatoShare })} ><i class="fas fa-times"></i></button>
              </div>
              <div class="modal-body">
                <h6>Doctor Name</h6>
                <div class="form-group">
                  {/* <input type="text" class="form-control" id="usr" name="email" value={this.state.filterName} onChange={(e) => this.setState({ receiverEmailId: e.target.value })} /> */}
                  <input type="text" class="form-control" id="usr" name="filterName" value={this.state.filterName} onChange={(e) => {
                    let value = e.target.value;
                    this.setState({ [e.target.name]: value }, () => {
                      let dummy = this.state.filterDoctorListcopy.slice()
                      let res = dummy.filter((each) => {

                        return each.firstName.includes(value)
                      })
                      this.setState({ filterDoctorList: res })
                    })


                  }} />

                </div>
                <div class="card_list_modal">
                  {this.state.filterDoctorList.length > 0 && this.state.filterDoctorList.map((each) =>
                    <div className="card_list_li">
                      {/* <a onClick={() => this.setState({ filterName: each.firstName + " " + each.email })}> */}
                      <a onClick={() => this.setState({ receiverEmailId: each.email, filterName: each.firstName + " " + each.email })}>

                        <label><i class="fas fa-user-alt"></i> {each.firstName}</label>
                        <label><i class="fas fa-envelope"></i>  {each.email}</label>
                        <label><i class="fas fa-mobile-alt"></i> {each.mobileNo}</label>
                        <label><i class="fas fa-map-marker-alt"></i> {each.address + each.address2}</label>
                      </a>
                    </div>
                  )}
                </div>
                <button class="send_btn" onClick={this.handleEmailValidation}><i class="far fa-paper-plane"></i> Send</button>
              </div>
            </div>
          </div>
        </div>}
      </div>)
  }
}
export default MyDrive;