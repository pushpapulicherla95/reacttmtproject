import URL from '../../asset/configUrl';
import axios from 'axios';
import actionType from '../productcategory/actionType';
//CREATE PRODUCT
export const createproductcategory = createproduct => dispatch => {
    const configs = {
        method: 'post',
        url: URL.CREATE_PRODUCTCATEGORY,
        data: createproduct,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.CREATE_PRODUCTCATEGORY_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.CREATE_PRODUCTCATEGORY_FAILURE,
            payload: err
        })
    })
}
export const listproductcategory = listproduct => dispatch => {
    const configs = {
        method: 'get',
        url: URL.LIST_PRODUCTCATEGORY,
        data: listproduct,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.LIST_PRODUCTCATEGORY_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.LIST_PRODUCTCATEGORY_FAILURE,
            payload: err
        })
    })
}
//updatecategory
export const updateproductcategory = updatecategory => dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDATE_PRODUCTCATEGORY,
        data: updatecategory,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.UPDATE_PRODUCTCATEGORY_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.UPDATE_PRODUCTCATEGORY_FAILURE,
            payload: err
        })
    })
}
//deletecategory
export const deleteproductcategory = deletecategory => dispatch => {
    const configs = {
        method: 'post',
        url: URL.DELETE_PRODUCTCATEGORY,
        data: deletecategory,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.DELETE_PRODUCTCATEGORY_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.DELETE_PRODUCTCATEGORY_FAILURE,
            payload: err
        })
    })
}