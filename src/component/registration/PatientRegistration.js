import React, { Component } from 'react';
import { connect } from "react-redux";
import { register } from "../../service/login/action";
import validator from "validator";
import { NavLink } from "react-router-dom";

class PatientRegistration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            lastName: "",
            email: "",
            address: "",
            password: "",
            confirmpassword: "",
            errors: {
                firstName: "",
                lastName: "",
                email: "",
                address: "",
                password: "",
                confirmpassword: ""
            },
            firstNameValid: false,
            lastNameValid: false,
            emailValid: false,
            addressValid: false,
            passwordValid: false,
            confirmpasswordValid: false,
            formValid: false,
        }
    }
    handleChange = (e) => {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value }, () => {
            this.validateField(name, value);
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const { firstName, lastName, email, address, password } = this.state;
        const patientregister = {
            firstName: firstName,
            lastName: lastName,
            email: email,
            mobileNo: "",
            dataOfBirth: "",
            address: address,
            address2: "",
            password: password,
            signedFormName: "",
            signedForm: "",
            cityId: 1,
            latitude: "",
            langitude: ""

        }
        this.props.register(patientregister);
    }

    componentWillReceiveProps = nextProps => {
        if (nextProps.registerDetails.status == "success") {
            this.props.history.push("/login/patient");

        }
    }
    //validation
    validateField(fieldName, value) {
        const { errors, firstNameValid, lastNameValid, emailValid, addressValid, passwordValid, confirmpasswordValid, confirmpassword, password } = this.state;
        let emailvalid = emailValid;
        let firstNamevalid = firstNameValid;
        let lastNamevalid = lastNameValid;
        let passwordvalid = passwordValid;
        let addressvalid = addressValid;
        let confirmpasswordvalid = confirmpasswordValid;
        let fieldValidationErrors = errors;
        switch (fieldName) {
            case "email":
                emailvalid = value.match(
                    /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i
                );
                fieldValidationErrors.email = emailvalid
                    ? ""
                    : "Please enter valid email address.";
                break;
            case "firstName":
                firstNamevalid = !(value.length < 3 || value.length > 20);
                fieldValidationErrors.firstName = firstNamevalid
                    ? ""
                    : "First Name should be 3 to 20 characters";
                break;
            case "lastName":
                lastNamevalid = !(value.length < 3 || value.length > 20);
                fieldValidationErrors.lastName = lastNamevalid
                    ? ""
                    : "Last Name should be 3 to 20 characters";
                break;
            case "password":
                passwordvalid = value.length !== 0
                fieldValidationErrors.password = passwordvalid
                    ? ""
                    : "Please enter valid password.";
                break;
            case "address":
                addressvalid = !(value.length < 1);
                fieldValidationErrors.address = addressvalid
                    ? ""
                    : "Please enter the address";
                break;
            // case "password":
            // passwordvalid =
            //   value.length >= 8 &&
            //   value.match(
            //     /(?=.*[_!@#$%^&*-])(?=.*\d)(?!.*[.\n])(?=.*[a-z])(?=.*[A-Z])^.{8,}$/
            //   );
            // fieldValidationErrors.password = passwordvalid
            //   ? ""
            //   : "Must contain at least one uppercase letter, one lowercase letter, one number, one special character and a minimum of 8 characters";
            // break;
            case "confirmpassword":
                confirmpasswordvalid = validator.equals(value, password);
                fieldValidationErrors.confirmpassword = confirmpasswordvalid
                    ? ""
                    : "Password does not match";
                break;
        }
        if (fieldName === "password") {
            if (confirmpassword !== "") {
                if (value !== confirmpassword) {
                    fieldValidationErrors.confirmpassword = "Password does not match.";
                } else {
                    fieldValidationErrors.confirmpassword = "";
                }
            }
        } else if (fieldName === "confirmpassword") {
            if (value !== password) {
                fieldValidationErrors.confirmpassword = "Password does not match.";
            } else {
                fieldValidationErrors.confirmpassword = "";
            }
        }
        this.setState({
            errors: fieldValidationErrors,
            emailValid: emailvalid,
            passwordValid: passwordvalid,
            firstNameValid: firstNamevalid,
            lastNameValid: lastNamevalid,
            addressValid: addressvalid,
            confirmpasswordValid: confirmpasswordvalid
        },
            this.validateForm
        )
    }
    //
    validateForm() {
        const {
            firstNameValid, lastNameValid, emailValid, addressValid, passwordValid, confirmpasswordValid
        } = this.state;
        this.setState({
            formValid:
                lastNameValid &&
                firstNameValid &&
                emailValid &&
                addressValid &&
                passwordValid &&
                confirmpasswordValid,




        });
      
    }
    render() {
        const { firstName, lastName, email, address, password, confirmpassword, errors,formValid } = this.state;
        return (
            <React.Fragment>
                <div className='loginInner registerInner d-block' >
                    <div className="loginFormContent">
                        <h4 className="popupTitle">Register as a patient</h4>
                        <form >
                            <div className="row">
                                <div className="col-xl-6  col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div className="form-group emailIcon">
                                        <label>E-Mail* </label>
                                        <input type="text" className="form-control commonInput" onChange={this.handleChange} name="email" value={email} placeholder="Email" />
                                        <div style={{ color: "red" }}>{errors.email}</div>

                                    </div>
                                    <div className="form-group pass">
                                        <label>Password* </label>
                                        <input type="password" className="form-control commonInput" onChange={this.handleChange} name="password" value={password} placeholder="Password" />
                                        <div style={{ color: "red" }}>{errors.password}</div>
                                    </div>
                                    <div className="form-group pass">
                                        <label>Confirm Password* </label>
                                        <input type="password" className="form-control commonInput" onChange={this.handleChange} name="confirmpassword" value={confirmpassword} placeholder="Confirm Password" />
                                        <div style={{ color: "red" }}>{errors.confirmpassword}</div>
                                    </div>
                                </div>
                                <div className="col-xl-6  col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div className="form-group name">
                                        <label>First Name </label>
                                        <input type="text" className="form-control commonInput" onChange={this.handleChange} name="firstName" value={firstName} placeholder="First Name" />
                                        <div style={{ color: "red" }}>{errors.firstName}</div>

                                    </div>
                                    <div className="form-group name">
                                        <label>Last Name </label>
                                        <input type="text" className="form-control commonInput" onChange={this.handleChange} name="lastName" value={lastName} placeholder="Last Name" />
                                        <div style={{ color: "red" }}>{errors.lastName}</div>

                                    </div>
                                    <div className="form-group address">
                                        <label>From of Address </label>
                                        <textarea className="form-control" name="address" onChange={this.handleChange} value={address} placeholder="From of Address"></textarea>
                                        <div style={{ color: "red" }}>{errors.address}</div>

                                    </div>
                                </div>
                                <div className="col-xl-12  col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div className="form-group">
                                        <input className="inp-cbx" id="cbx" type="checkbox" style={{ display: "none" }} />
                                        <label className="cbx" htmlFor="cbx"><span>
                                            <svg width="12px" height="10px" viewBox="0 0 12 10">
                                                <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                            </svg></span><span>I have read the <a href="">terms of use and the data privacy statement</a> and accept them.</span></label>
                                    </div>
                                    <div className="form-group">
                                        <input className="inp-cbx" id="cbx1" type="checkbox" style={{ display: "none" }} />
                                        <label className="cbx" htmlFor="cbx1"><span>
                                            <svg width="12px" height="10px" viewBox="0 0 12 10">
                                                <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                            </svg></span><span>I have read the <a href="">general terms and conditions</a> and accept them.</span></label>
                                    </div>

                                    <div className="form-group text-center">
                                        <button type="button" className="btn loginBtn" disabled={!formValid} onClick={this.handleSubmit}>Submit</button>
                                    </div>

                                    <p className="linkToReg">Already have an account ?
                                    <NavLink to="/login/patient"> Login Here</NavLink>
                                    </p>
                                </div>

                            </div>

                        </form>
                    </div>
                    <div className="socialLogin">
                        <h4 className="popupTitle">Register with Social Network  </h4>
                        <ul>
                            <li>
                                <a href="" title="Facebook" className="fb">
                                    <img src="../images/facebook-logo.png" />
                                </a>
                            </li>
                            <li>
                                <a href="" title="Google Plus" className="google">
                                    <img src="../images/googlePlus.png" />
                                </a>
                            </li>
                            <li>
                                <a href="" title="Linked In" className="linked">
                                    <img src="../images/linkedin-logo1.png" />
                                </a>
                            </li>
                            <li>
                                <a href="" title="Instagram" className="insta">
                                    <img src="../images/instagram.png" />
                                </a>
                            </li>
                            <li>
                                <a href="" title="We Chat" className="wechat">
                                    <img src="../images/wechat.png" />
                                </a>
                            </li>
                            <li>
                                <a href="" title="Amazon" className="amazon">
                                    <img src="../images/amazon.png" />
                                </a>
                            </li>
                            <li>
                                <a href="" title="Flickr" className="flickr">
                                    <img src="../images/flickr-.png" />
                                </a>
                            </li>
                            <li>
                                <a href="" title="Twitter" className="twit">
                                    <img src="../images/twitter-logo.png" />
                                </a>
                            </li>
                            <li>
                                <a href="" title="Yahoo" className="yahoo">
                                    <img src="../images/yahoo-big-logo.png" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </React.Fragment>

        )
    }
}
const mapStateToProps = state => ({
    registerDetails: state.loginReducer.registerDetails
});

const mapDispatchToProps = dispatch => ({
    register: patientregister => dispatch(register(patientregister))
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PatientRegistration);
