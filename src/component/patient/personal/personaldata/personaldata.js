import React, { Component } from "react";
import {
  addPersonaldata,
  getPersonaldata
} from "../../../../service/healthrecord/action";
import { getUploadFiles, handleFileDelete } from "../../../../service/upload/action";
import { connect } from "react-redux";
import axios from "axios";
import { onlyAlphabets, onlyNumbers, onlyDate } from "../KeyValidation";
import URL from "../../../../asset/configUrl";
import { toastr } from "react-redux-toastr";
import { awsPersonalFiles, } from "../../../../service/common/action";

class personaldata extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 1,
      tabone: true,
      tabtwo: false,
      familyName: "",
      firstName: "",
      title: "",
      email: "",
      dob: "",
      pob: "",
      addressPrivate: "",
      addressJob: "",
      privateNo: "",
      workNo: "",
      mobileNo: "",
      gender: "",
      reletionship: "",
      patientFindingListData: "",
      uploadPopup: false,
      name: "",
      fileName: "",
      fileType: "",
      fileURL: "",
      errors: {
        familyName: "",
        firstName: "",
        title: "",
        email: "",
        addressPrivate: "",
        addressjob: "",
        gender: "",
      },
      firstNameValid: false,
      familyNameValid: false,
      emailValid: false,
      titleValid: false,
      addressjobValid: false,
      addressPrivateValid: false,
      genderValid: false,
      formValid: false,

    };
  }



  validateField(fieldName, value) {
    const { errors, firstNameValid, familyNameValid, emailValid, titleValid, aaddressjobValid, addressPrivateValid, genderValid } = this.state;
    let emailvalid = emailValid;
    let firstNamevalid = firstNameValid;
    let familyNamevalid = familyNameValid;
    let titlevalid = titleValid;
    let addressjobvalid = aaddressjobValid;
    let addressPrivatevalid = addressPrivateValid;
    let gendervalid = genderValid;
    let fieldValidationErrors = errors;
    switch (fieldName) {
      case "email":
        emailvalid = value.match(
          /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i
        );
        fieldValidationErrors.email = emailvalid
          ? ""
          : "Please enter valid email address.";
        break;
      case "firstName":
        firstNamevalid = !(value.length < 3 || value.length > 20);
        fieldValidationErrors.firstName = firstNamevalid
          ? ""
          : "First Name should be 3 to 20 characters";
        break;
      case "familyName":
        familyNamevalid = !(value.length < 3 || value.length > 20);
        fieldValidationErrors.familyName = familyNamevalid
          ? ""
          : "Last Name should be 3 to 20 characters";
        break;
      case "title":
        titlevalid = value.length !== 0
        fieldValidationErrors.title = titlevalid
          ? ""
          : "Title should be 1 to 100 characters.";
        break;
      case "addressjob":
        addressjobvalid = !(value.length < 1);
        fieldValidationErrors.addressjob = addressjobvalid
          ? ""
          : "Please enter the address";
        break;
      case "addressPrivate":
        addressPrivatevalid = !(value.length < 1);
        fieldValidationErrors.addressPrivate = addressPrivatevalid
          ? ""
          : "Please enter the address";
        break;
      case "gender":
        gendervalid = !(value.length < 1);
        fieldValidationErrors.gender = gendervalid
          ? ""
          : "Please enter the gender";
        break;

    }
    this.setState({
      errors: fieldValidationErrors,
      emailValid: emailvalid,
      firstNameValid: firstNamevalid,
      familyNameValid: familyNamevalid,
      titleValid: titlevalid,
      genderValid: gendervalid,
      addressjobValid: addressjobvalid,
      addressPrivateValid: addressPrivatevalid
    },
      this.ValidationField
    )
  }
  ValidationField = () => {
    const {
      firstNameValid,
      familyNameValid,
      title,
      email,
      dob,
      gender,
      pob,
      addressPrivate,
      addressjob,
      privateno,
      workno,
      mobileno,
      genderValid, emailValid,
      reletionship, errors
    } = this.state;
    this.setState({
      formValid:
        gender

    });

    let allKeyLength = Object.keys(errors).length
    let allTrueArray = []
    Object.keys(errors).map(val => {
      errors[val] && allTrueArray.push(true)
    })
  

  };

  handleAwsFiles = value => {


    // window.location.href = "https://data.tomatomedical.com/patientpersonal/"+value;

    window.open("https://data.tomatomedical.com/patientpersonal/" + value, '_blank');
  };

  trigger = () => {
    this.setState({ uploadPopup: !this.state.uploadPopup });
  };
  handleChangePopup = e => {
    var reader = new FileReader();
    var file = e.target.files[0];
    let name = file.name.split('.')[0]
    let type = "." + file.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileURL: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(file);
  };
  popupSubmit = () => {
    var phrase = this.state.fileURL;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);
    var { userInfo } = this.props.loginDetails;
    const payload = {
      uid: userInfo.userId,
      z: match[1],
      fileName: this.state.fileName,
      fileType: this.state.fileType,
      name: this.state.name,
      category: "personal"
    };
    this.setState({ uploadPopup: false });

    axios
      .post(URL.FILEPERSONAL_UPLOAD, payload)
      .then(response => {
        response.data.status === "success"
          ? toastr.success("Message", response.data.message)
          : toastr.error("Message", response.data.message);
        if (response.data.status === "success") {
          var { userInfo } = this.props.loginDetails;
          const uploadFiles = {
            uid: userInfo.userId,
            category: "personal"
          };
          this.props.getUploadFiles(uploadFiles);
          this.setState({ uploadPopup: false });
          this.setState({
            name: "",
            fileName: "",
            fileType: "",
            fileURL: ""
          });
        }
      })
      .catch(error => {
        // this.setState({ uploadPopup: true });
      });
  };

  componentWillMount() {
    const { userInfo } = this.props.loginDetails;

    const listpersonal = {
      uid: userInfo.userId
    };
    const uploadFiles = { uid: userInfo.userId, category: "personal" };
    this.props.getUploadFiles(uploadFiles);
    this.props.getPersonaldata(listpersonal);
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.getpersonalDetails &&
      nextProps.getpersonalDetails.personal != null &&
      nextProps.getpersonalDetails.personal != 0
    ) {
      const listpersonal = nextProps.getpersonalDetails.personal;
      let patientFindingListData = JSON.parse(listpersonal);
      this.setState({
        familyName: patientFindingListData.familyName,
        firstName: patientFindingListData.firstName,
        title: patientFindingListData.title,
        email: patientFindingListData.email,
        dob: patientFindingListData.dob,
        pob: patientFindingListData.pob,
        addressPrivate: patientFindingListData.addressPrivate,
        addressJob: patientFindingListData.addressJob,
        privateNo: patientFindingListData.privateNo,
        workNo: patientFindingListData.workNo,
        mobileNo: patientFindingListData.mobileNo,
        gender: patientFindingListData.gender,
        reletionship: patientFindingListData.reletionship
      });
    }
  }

  tabOne = () => {
    this.setState({ tabone: true, tabtwo: false });
  };
  tabTwo = () => {
    this.setState({ tabone: false, tabtwo: true });
  };
  // handleChange = (e, index) => {
  //   this.setState({ [e.target.name]: e.target.value },()=>{this.validateField()});
  // };


  handleChange = e => {
    e.preventDefault();
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };

  // handleChange = e => {
  //   let name = e.target.name;
  //   let value = e.target.value;

  //   this.setState(
  //     {
  //       [e.target.name]: e.target.value 
  //     },()=>{this.ValidationField()},

  //     () => {
  //       if (
  //         name !== "mobileno" &&
  //         name !== "privateno" &&
  //         name !== "gender" &&
  //         name !== "reletionship" &&
  //         name !== "addressprivate" &&
  //         name !== "addressjob" &&
  //         name !== "privateno" &&
  //         name !== "workno"
  //       ) {
  //         let data = validate(name, value);
  //         this.setState({
  //           [name + "Valid"]: data.errorValid,
  //           [name + "Error"]: data.errorMessage
  //         });
  //       }
  //     }
  //   );
  // };
  handleChanges = e => {
    this.setState({ [e.target.name]: e.target.value }, () => { this.validateField() });
  };
  handleData = () => {
    const {
      familyName,
      firstName,
      title,
      email,
      dob,
      pob,
      addressPrivate,
      addressJob,
      privateNo,
      workNo,
      mobileNo,
      gender,
      reletionship
    } = this.state;
    const { userInfo } = this.props.loginDetails;
    const personalData = {
      userId: userInfo.userId,
      personal: {
        familyName: familyName,
        firstName: userInfo.firstName,
        title: title,
        email: userInfo.email,
        dob: dob,
        pob: pob,
        addressPrivate: addressPrivate,
        addressJob: addressJob,
        privateNo: privateNo,
        workNo: workNo,
        mobileNo: mobileNo,
        gender: gender,
        reletionship: reletionship
      }

    };
    this.props.addPersonaldata(personalData);
  };

  render() {
    const {
      tabone,
      tabtwo,
      familyName,
      familyNameError,
      fristName,
      firstNameError,
      title,
      titleError,
      pobError,
      email,
      emailError,
      dob,
      pob,
      gender,
      addressPrivate,
      addressJob,
      privateno,
      workno,
      mobileno,
      patientFindingListData,
      errors,
      formValid
    } = this.state;
    const { userInfo } = this.props.loginDetails;
    return (
      <div className="" id="main-content">
        <div className="wrapper">
          <div className="container-fluid">
            <div className="body-content">
              <div className="tomCard">
                <div className="tomCardHead">
                  <h5 className="float-left">PERSONAL DATA</h5>
                </div>

                <div className="tab-menu-content">
                  <div class="tab-menu-title">
                    <ul>
                      <li className={this.state.tabone ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabOne}>
                          <p>Personal Data</p>
                        </a>
                      </li>
                      <li className={this.state.tabtwo ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabTwo}>
                          <p>Attached Files</p>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>

                {tabone && (
                  <div className="tab-menu-content-section">
                    <div
                      id="content-1"
                      className={tabone ? "d-block" : "d-none"}
                    >
                      <div className="row">
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div className="form-group">
                            <label>Family Name</label>
                            <input
                              type="text"
                              className="form-control"
                              name="familyName"
                              onChange={this.handleChange}
                              value={familyName}
                              onKeyPress={onlyAlphabets}
                            />
                            {/* <div style={{ color: "red" }}>{errors.familyName}</div> */}


                          </div>
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div className="form-group">
                            <label>First Name</label>
                            <input
                              type="text"
                              className="form-control"
                              name="firstName"
                              onChange={this.handleChange}
                              value={userInfo.firstName}
                              onKeyPress={onlyAlphabets} readOnly
                            />
                            {/* <div style={{ color: "red" }}>{errors.fristName}</div> */}
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div className="form-group">
                            <label>Title</label>
                            <input
                              type="text"
                              className="form-control"
                              name="title"
                              onChange={this.handleChange}
                              value={title}
                              onKeyPress={onlyAlphabets}
                            />
                            {/* <div style={{ color: "red" }}>{errors.title}</div> */}
                          </div>
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div className="form-group">
                            <label>Date of birth</label>
                            <input
                              type="date"
                              className="form-control"
                              name="dob"
                              onChange={this.handleChange}
                              value={this.state.dob}
                              onKeyPress={onlyDate}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div className="form-group">
                            <label>Place of birth</label>
                            <input
                              type="text"
                              className="form-control"
                              name="pob"
                              onChange={this.handleChange}
                              value={pob}
                              onKeyPress={onlyAlphabets}
                            />
                            <div style={{ color: "red" }}>{pobError}</div>
                          </div>
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div className="form-group">
                            <label>Gender <sup>*</sup> </label>
                            <select
                              className="form-control"
                              name="gender"
                              value={this.state.gender}
                              onChange={this.handleChange}
                            >
                              <option value="">--Select Gender--</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div className="form-group">
                            <label>Relationship</label>
                            <select
                              className="form-control"
                              name="reletionship"
                              value={this.state.reletionship}
                              onChange={this.handleChange}
                            >
                              <option value="">--Select Relationship--</option>
                              <option value="Single">Single</option>
                              <option value="Married">Married</option>
                              <option value="Seperated">Seperated</option>
                              <option value="Divorced">Divorced</option>
                              <option value="Widowed">Widowed</option>
                            </select>
                          </div>
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div className="form-group">
                            <label>Email </label>
                            <input
                              type="email"
                              className="form-control"
                              name="email"
                              onChange={this.handleChange}
                              value={userInfo.email} readOnly
                            />{" "}
                            {/* <div style={{ color: "red" }}>{errors.email}</div> */}
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        {/* <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div className="form-group">
                            <label>Address Private (Street,ZIP,City)</label>
                            <textarea
                              rows="3"
                              className="form-control"
                              name="addressPrivate"
                              onChange={this.handleChange}
                              value={this.state.addressPrivate}
                            />
                          </div>
                          
                        </div> */}
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div className="form-group">
                            <label>
                              Address Private (Street,ZIP,City)
                            </label>
                            <textarea
                              rows="3"
                              className="form-control"
                              name="addressPrivate"
                              onChange={this.handleChange}
                              value={this.state.addressPrivate}
                            />
                          </div>
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div className="form-group">
                            <label>
                              Address Job (Street, ZIP/Postcode, City)
                            </label>
                            <textarea
                              rows="3"
                              className="form-control"
                              name="addressJob"
                              onChange={this.handleChange}
                              value={this.state.addressJob}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div className="form-group">
                            <label>Private Phone number</label>
                            <input
                              type="text"
                              className="form-control"
                              name="privateNo"
                              onChange={this.handleChange}
                              value={this.state.privateNo}
                              onKeyPress={onlyNumbers} maxlength="15"
                            />
                          </div>
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div className="form-group">
                            <label>Work Phone Number</label>
                            <input
                              type="text"
                              className="form-control"
                              name="workNo"
                              onChange={this.handleChange}
                              value={this.state.workNo}
                              onKeyPress={onlyNumbers} maxlength="15"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div className="form-group">
                            <label>Mobile number</label>
                            <input
                              type="text"
                              className="form-control"
                              name="mobileNo"
                              onChange={this.handleChange}
                              value={this.state.mobileNo}
                              onKeyPress={onlyNumbers} maxlength="15"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12">
                          <button
                            type="button"
                            className="commonBtn float-right"
                            onClick={this.handleData}
                            disabled={!formValid}
                          >
                            Save
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                {tabtwo && (
                  <div id="content-2" className={tabtwo ? "d-block" : "d-none"}>
                    <button
                      type="button"
                      className="commonBtn2 float-right mb-2"
                      onClick={this.trigger}
                    >
                      <i className="fa fa-plus" />
                      <span> Upload new files </span>{" "}
                    </button>
                    <div className="table-responsive">
                      <table className="table table-hover">
                        <thead className="thead-default">
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>FileName</th>
                            <th>Action</th>

                          </tr>
                        </thead>
                        <tbody>

                          {this.props.getFilesDetails.fileDetails &&
                            this.props.getFilesDetails.fileDetails.map(
                              (each, i) => (
                                <tr>
                                  <td data-label="#">{i + 1}</td>
                                  <td data-label="Name"> {each.name}</td>
                                  <td data-label="FileName"> <span >{each.fileName}</span></td>

                                  <td data-label="Action">
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.handleAwsFiles(each.fileUrl)
                                      }
                                    >
                                      <i class="fas fa-download" />
                                    </button>
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.props.handleFileDelete(each.id, each.fileUrl, "personal")
                                      }
                                    >
                                      <i class="fas fa-trash" />
                                    </button>
                                  </td>


                                </tr>
                              )
                            )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>

        <div
          className={this.state.uploadPopup ? "modal d-block" : "modal"}
          id="myModal"
        >
          <div class="modal-dialog">
            <div class="modal-content modalPopUp">
              <div class="modal-header borderNone">
                <h5 class="modal-title">Upload File</h5>
                <button type="button" className="close" onClick={this.trigger}>
                  <i class="fas fa-times" />
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="form-group">
                        <label className="form-label">File name</label>
                        <input
                          className="form-control"
                          type="text"
                          id="name"
                          value={this.state.name}
                          name="name"
                          onChange={this.handleChanges}
                        />
                        <span />
                      </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="fileUp">
                        <label
                          for="fileUp1"
                          class="commonBtn text-center upload"
                        >
                          Upload File
                        </label>
                        <input
                          type="file"
                          id="fileUp1"
                          onChange={this.handleChangePopup}
                          style={{ display: "none" }}
                        />
                        {/* <p style={{ textAlign: "center" }}>hsfkjhsfj</p> */}
                      </div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-sm-12 mt-2">
                      <button
                        type="button"
                        class="commonBtn float-right"
                        disabled={!this.state.fileURL}
                        onClick={this.popupSubmit}
                      >
                        Submit
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  getpersonalDetails: state.healthrecordReducer.getpersonalDetails,
  loginDetails: state.loginReducer.loginDetails,
  getFilesDetails: state.uploadReducer.getFilesDetails
});

const mapDispatchToProps = dispatch => ({
  addPersonaldata: personalData => dispatch(addPersonaldata(personalData)),
  getPersonaldata: listpersonal => dispatch(getPersonaldata(listpersonal)),
  getUploadFiles: payload => dispatch(getUploadFiles(payload)),
  handleFileDelete: (id, fileUrl, type) => dispatch(handleFileDelete(id, fileUrl, type))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(personaldata);
