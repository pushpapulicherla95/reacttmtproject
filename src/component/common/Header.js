import React, { Component } from 'react';

class Header extends Component {

    render() {
        return (
            <header class="header">
            <div class="brand">
                <a href="#" class="logo">
                    <img src="../images/logo.jpg"/> </a>
                <div class="sidebar-toggle-box">
                    <i class="fa fa-bars"></i>
                </div>
            </div>
            <div class="top-nav">

                    <div class="dropdown float-right mr-2">
                            <a href="login.html" title="Logout" class="logout-icon">
                                <i class="fas fa-power-off"></i>
                            </a>
        
                        </div>
            </div>
        </header>
        )
    }

}
export default Header;