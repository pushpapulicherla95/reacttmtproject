import React, { Component } from 'react';
import axios from 'axios';

export default class EmployeeEdit extends Component {
  constructor(props) {
    super(props);

    // this.onChangeFirstName = this.onChangeFirstName.bind(this);
    // this.onChangeLastName = this.onChangeLastName.bind(this);
    // this.onChangeMobileNumber = this.onChangeMobileNumber.bind(this);
    // this.onChangeEmail = this.onChangeEmail.bind(this);
    // this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      first_name: '',
      last_name: '',
      mobile_no: '',
      email: ''
    }
  }
  handleChange = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    this.setState({
      [name] :  value
  })
}

  componentDidMount() {
    // console.log("sjnfb");
    axios.get('http://localhost:4000/API/employee/getById/:id' + this.props.match.params.id)
      .then(response => {
        
        this.setState({
          first_name: response.data.first_name,
          last_name: response.data.last_name,
          mobile_no: response.data.mobile_no,
          email: response.data.email
        });
      })
      .catch(function (error) {
        console.log(error);
      })
  }

  // onChangeFirstName(e) {
  //   this.setState({
  //     first_name: e.target.value
  //   });
  // }
  // onChangeLastName(e) {
  //   this.setState({
  //     last_name: e.target.value
  //   })
  // }
  // onChangeMobileNumber(e) {
  //   this.setState({
  //     mobile_no: e.target.value
  //   })
  // }
  // onChangeEmail(e) {
  //   this.setState({
  //     email: e.target.value
  //   })
  // }

  onSubmit(e) {
    e.preventDefault();
    const obj = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      mobile_no: this.state.mobile_no,
      email: this.state.email

    };
    axios.post('http://localhost:4000/API/employee/update/' + this.props.match.params.id, obj)
      .then(res => console.log(res.data));

    this.props.history.push('/');
  }

  render() {
    const {   first_name,
      last_name,
      mobile_no,
      email } = this.state;

      console.log("Hey I am from state ..:",this.state)
    return (
      <div style={{ marginTop: 10 }}>
        <h3 align="center">Update Business</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>First Name:  </label>
            <input
              type="text"
              className="form-control"
              name="first_name"
              value={first_name}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label>Last Name: </label>
            <input type="text"
              className="form-control"
              name="last_name"
                      value={last_name}
                      onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label>Mobile Number: </label>
            <input type="text"
              className="form-control"
              name="mobile_no"
              value={mobile_no}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label>Email: </label>
            <input type="text"
              className="form-control"
              name="email"
              value={email}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <input type="submit"
              value="Update Employee"
              className="btn btn-primary" />
          </div>
        </form>
      </div>
    )
  }
}
// export default EmployeeEdit;
// import React, { Component } from 'react';


// class EmployeeEdit extends Component {
//   render() {
//     return (
//       <div className="App">
//       <b><h1>Welcome to Tomato Medical</h1></b>

//       </div>
//     );
//   }
// }
// export default EmployeeEdit;