import actionType from "../patient/actionType";
import URL from '../../asset/configUrl';
import axios from "axios";
import {getPersonaldata} from "../healthrecord/action";
import { toastr } from 'react-redux-toastr';
import { toastrOptions } from "../../component/common/toasteroptions";
export const addPatientHealthData = addPatient => dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_PATIENT_HEALTH_RECORD,
        data: addPatient,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const userinfo = {
        "uid": addPatient.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPatientHealthData(userinfo))
            dispatch({
                type: actionType.ADD_PATIENT_HEALTH_RECORD_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_PATIENT_HEALTH_RECORD_FAILURE,
            payload: err
        })
    })
}
export const updatePatientHealthData = updatePatient => dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDTAE_PATIENT_HEALTH_RECORD,
        data: updatePatient,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        
    }
    const userinfo = {
        "uid": updatePatient.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPatientHealthData(userinfo))
            dispatch({
                type: actionType.UPDATE_PATIENT_HEALTH_RECORD_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.UPDTAE_PATIENT_HEALTH_RECORD_FAILURE,
            payload: err
        })
    })
}

export const getPatientHealthData = (userinfo) => dispatch => {

    const configs = {
        method: 'post',
        url: URL.GET_PATIENT_HEALTH_RECORD,
        data: userinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.GET_PATIENT_HEALTH_RECORD_SUCCESS,
                payload: res.data
            })
        }
        //     toastr.success('Message', res.data.message, toastrOptions);
        // } else if (res.data.status == "failure") {
        //     toastr.error('error', "Error", toastrOptions);
        // }
    })
        .catch(err => {
            dispatch({
                type: actionType.GET_PATIENT_HEALTH_RECORD_FAILURE,
                error: err
            })
        })
}
export const deletePatientHealthData = (deletepatient) => dispatch => {

    const configs = {
        method: 'post',
        url: URL.DELETE_PATIENT_HEALTH_RECORD,
        data: deletepatient,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const userinfo = {
        "uid": deletepatient.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPatientHealthData(userinfo))
            dispatch({
                type: actionType.DELETE_PATIENT_HEALTH_RECORD_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    })
        .catch(err => {
            dispatch({
                type: actionType.DELETE_PATIENT_HEALTH_RECORD_FAILURE,
                error: err
            })
        })
}
//ACCIDENTS
//Add
export const createAccidentData = createaccident => dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_ACCIDENT,
        data: createaccident,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const listaccident = {
        "uid": createaccident.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(listAccidentData(listaccident))

            dispatch({
                type: actionType.ADD_ACCIDENT_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_ACCIDENT_FAILURE,
            payload: err
        })
    })
}
//List
export const listAccidentData = listaccident => dispatch => {
    const configs = {
        method: 'post',
        url: URL.GET_ACCIDENT,
        data: listaccident,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.GET_ACCIDENT_SUCCESS,
                payload: res.data
            })
        }
        //     toastr.success('Message', res.data.message, toastrOptions);
        // } 
        // else if (res.data.status == "failure") {
        //     toastr.error('error', "Error", toastrOptions);
        // }
    }).catch(err => {
        dispatch({
            type: actionType.GET_ACCIDENT_FAILURE,
            payload: err
        })
    })
}
//update
export const updateAccidentData = updateaccident => dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDATE_ACCIDENT,
        data: updateaccident,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.UPDATE_ACCIDENT_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.UPDATE_ACCIDENT_FAILURE,
            payload: err
        })
    })
}
//delete
export const deleteAccidentData = deleteaccident => dispatch => {
    const configs = {
        method: 'post',
        url: URL.DELETE_ACCIDENT,
        data: deleteaccident,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const listaccident = {
        "uid": deleteaccident.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(listAccidentData(listaccident))

            dispatch({
                type: actionType.DELETE_ACCIDENT_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.DELETE_ACCIDENT_FAILURE,
            payload: err
        })
    })
}
//Finding
//add
export const createFindingData = createfinding=> dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_FINDING,
        data: createfinding,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const listfinding = {
        "uid": createfinding.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(listFindingData(listfinding))


            dispatch({
                type: actionType.ADD_FINDING_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_FINDING_FAILURE,
            payload: err
        })
    })
}
//list
export const listFindingData = listfinding => dispatch => {
    const configs = {
        method: 'post',
        url: URL.GET_FINDING,
        data: listfinding,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.GET_FINDING_SUCCESS,
                payload: res.data
            })
        }
        //     toastr.success('Message', res.data.message, toastrOptions);
        // } else if (res.data.status == "failure") {
        //     toastr.error('error', "Error", toastrOptions);
        // }
    }).catch(err => {
        dispatch({
            type: actionType.GET_FINDING_FAILURE,
            payload: err
        })
    })
}
//update
export const updateFindingData = updatefinding => dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDATE_FINDING,
        data: updatefinding,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            
            dispatch({
                type: actionType.UPDATE_FINDING_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.UPDATE_FINDING_FAILURE,
            payload: err
        })
    })
}
//delete

export const deleteFindingData = deletefinding => dispatch => {
    const configs = {
        method: 'post',
        url: URL.DELETE_FINDING,
        data: deletefinding,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const listfinding = {
        "uid": deletefinding.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(listFindingData(listfinding))


            dispatch({
                type: actionType.DELETE_FINDING_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.DELETE_FINDING_FAILURE,
            payload: err
        })
    })
}
//Diagnosis/Therapy 
//ADD
export const createTherapyData = createtherapy=> dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_THERAPY,
        data:createtherapy,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const listtherapy = {
        "uid": createtherapy.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(listTherapyData(listtherapy))


            dispatch({
                type: actionType.ADD_THERAPY_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_THERAPY_FAILURE,
            payload: err
        })
    })
}
//list
export const listTherapyData = listtherapy => dispatch => {
    const configs = {
        method: 'post',
        url: URL.GET_THERAPY,
        data: listtherapy,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.GET_THERAPY_SUCCESS,
                payload: res.data
            })
        //     toastr.success('Message', res.data.message, toastrOptions);
        // } else if (res.data.status == "failure") {
        //     toastr.error('error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_THERAPY_FAILURE,
            payload: err
        })
    })
}
//update
export const updateTherapyData = updatetherapy => dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDATE_THERAPY,
        data: updatetherapy,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            
            dispatch({
                type: actionType.UPDATE_THERAPY_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.UPDATE_THERAPY_FAILURE,
            payload: err
        })
    })
}
//delete

export const deleteTherapyData = deletetherapy=> dispatch => {
    const configs = {
        method: 'post',
        url: URL.DELETE_THERAPY,
        data: deletetherapy,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const listtherapy = {
        "uid": deletetherapy.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(listTherapyData(listtherapy))


            dispatch({
                type: actionType.DELETE_THERAPY_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.DELETE_THERAPY_FAILURE,
            payload: err
        })
    })
}
//curriculum add
export const addcurriculumData = addcurriculum=> dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_CURRICULUM,
        data: addcurriculum,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.ADD_CURRICULUM_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_CURRICULUM_FAILURE,
            payload: err
        })
    })
}
//addcurricullum education
export const addeducationData = addeducation=> dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_EDUCATION,
        data: addeducation,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const listpersonaldatainfo = {
        "uid": addeducation.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(listpersonaldatainfo))

            dispatch({
                type: actionType.ADD_EDUCATION_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_EDUCATION_FAILURE,
            payload: err
        })
    })
}
//addpreviousjob
export const addpreviousjobData = addpreviousjob=> dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_PREVIOUSJOB,
        data: addpreviousjob,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getHospitaldata = {
        "uid": addpreviousjob.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getHospitaldata))
            dispatch({
                type: actionType.ADD_PREVIOUSJOB_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_PREVIOUSJOB_FAILURE,
            payload: err
        })
    })
}
export const updateeducationData = updateeducation=> dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDATE_EDUCATION,
        data: updateeducation,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getHospitaldata = {
        "uid": updateeducation.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getHospitaldata))

            dispatch({
                type: actionType.UPDATE_EDUCATION_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.UPDATE_EDUCATION_FAILURE,
            payload: err
        })
    })
}
//deletejob
export const deletepreviousjobData = deletepreviousjob=> dispatch => {
    const configs = {
        method: 'post',
        url: URL.DELETE_PREVIOUSJOB,
        data: deletepreviousjob,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getHospitaldata = {
        "uid": deletepreviousjob.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getHospitaldata))
            dispatch({
                type: actionType.DELETE_PREVIOUSJOB_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.DELETE_PREVIOUSJOB_FAILURE,
            payload: err
        })
    })
}
//deleteeducationData
export const deleteeducationData = deleteeducation=> dispatch => {
    const configs = {
        method: 'post',
        url: URL.DELETE_EDUCATION,
        data: deleteeducation,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getHospitaldata = {
        "uid": deleteeducation.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getHospitaldata))
            dispatch({
                type: actionType.DELETE_EDUCATION_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.DELETE_EDUCATION_FAILURE,
            payload: err
        })
    })
}
//updatejob
export const updatepreviousjobData = updatepreviousjob=> dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDATE_PREVIOUSJOB,
        data: updatepreviousjob,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getHospitaldata = {
        "uid": updatepreviousjob.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getHospitaldata))
            dispatch({
                type: actionType.UPDATE_PREVIOUSJOB_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.UPDATE_PREVIOUSJOB_FAILURE,
            payload: err
        })
    })
}
//addlanguage
export const addlanguageData = addlanguage=> dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_LANGUAGE,
        data: addlanguage,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getHospitaldata = {
        "uid": addlanguage.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getHospitaldata))
            dispatch({
                type: actionType.ADD_LANGUAGE_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_LANGUAGE_FAILURE,
            payload: err
        })
    })
}
//updateanguage
export const updatelanguageData = updatelanguage=> dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDATE_LANGUAGE,
        data: updatelanguage,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getHospitaldata = {
        "uid": updatelanguage.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getHospitaldata))
            dispatch({
                type: actionType.UPDATE_LANGUAGE_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.UPDATE_LANGUAGE_FAILURE,
            payload: err
        })
    })
}
export const deletelanguageData = deletelanguage=> dispatch => {
    const configs = {
        method: 'post',
        url: URL.DELETE_LANGUAGE,
        data: deletelanguage,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getHospitaldata = {
        "uid": deletelanguage.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getHospitaldata))
            dispatch({
                type: actionType.DELETE_LANGUAGE_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.DELETE_LANGUAGE_FAILURE,
            payload: err
        })
    })
}
//adddoctor
export const adddoctorData = adddoctor=> dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_DOCTOR,
        data: adddoctor,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getHospitaldata = {
        "uid": adddoctor.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getHospitaldata))
            dispatch({
                type: actionType.ADD_DOCTOR_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_DOCTOR_FAILURE,
            payload: err
        })
    })
}
//updatedoctor
export const updatedoctorData = updatedoctor=> dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDATE_DOCTOR,
        data: updatedoctor,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getHospitaldata = {
        "uid": updatedoctor.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getHospitaldata))
            dispatch({
                type: actionType.UPDATE_DOCTOR_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.UPDATE_DOCTOR_FAILURE,
            payload: err
        })
    })
}
//deletedoctor
export const deletedoctorData = deletedoctor=> dispatch => {
    const configs = {
        method: 'post',
        url: URL.DELETE_DOCTOR,
        data: deletedoctor,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getHospitaldata = {
        "uid": deletedoctor.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getHospitaldata))
            dispatch({
                type: actionType.DELETE_DOCTOR_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.DELETE_DOCTOR_FAILURE,
            payload: err
        })
    })
}