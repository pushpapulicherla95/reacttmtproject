import React, { Component } from "react";
import { connect } from "react-redux";
import { listPatientInfo,updateDoctorfeedback } from "../../service/appointment/action";

class MyPatientList extends Component {
  constructor(props) {
    super(props);
    this.state = {

      anamnesis: "",
      objectives: "",
      diagnosis: "",
      treatment: "",

    };
  }
  
  componentWillReceiveProps=(nextprops)=>{
    if(!nextprops)
    {
      const { userInfo } = this.props.loginDetails
      const listinfo = {
        doctorId: userInfo.userId
      }
      this.props.listPatientInfo(listinfo);
    }
  }

  componentWillMount = () => {
    const { listpatientDetails } = this.props
    //location.state.patientId
    const { userInfo } = this.props.loginDetails
    const listinfo = {
      doctorId: userInfo.userId
    }
    this.props.listPatientInfo(listinfo);

    const patientDetails = listpatientDetails[this.props.location.state.index]
    this.setState({
      anamnesis: patientDetails.anamnesis,
      objectives: patientDetails.objectives,
      diagnosis: patientDetails.diagnosis,
      treatment: patientDetails.treatment
      
    });
  }
  handleChange = (e) => {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value })
  }

  updateFeedback = (e) => {
    const {anamnesis,objectives,diagnosis,treatment} = this.state;
    const { listpatientDetails } = this.props
    const patientDetails = listpatientDetails[this.props.location.state.index]
   

     let feedBack = {
        doctorId : patientDetails.doctorId,
        patientId :patientDetails.patientId,
        anamnesis:anamnesis,
        objectives:objectives,
        diagnosis:diagnosis,
        treatment:treatment,
        teleMedicineId:patientDetails.id
     }
   this.props.updateDoctorfeedback(feedBack);
   

}
// componentWillReceiveProps=()=> {
//   console.log(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,",this.props)
// console.log("/////////////////",this.props.listpatientDetails)
//   const { listpatientDetails } = this.props
//   // if (
//   //   nextProps.listpatientDetails &&
//   //   nextProps.listpatientDetails != null &&
//   //   nextProps.listpatientDetails != 0
//   // ) {

//     const patientDetails = listpatientDetails[this.props.location.state.index]
//     this.setState({
//       anamnesis: patientDetails.anamnesis,
//       objectives: patientDetails.objectives,
//       diagnosis: patientDetails.diagnosis,
//       treatment: patientDetails.treatment
      
//     });
//   }


  render() {
    const { anamnesis, objectives, diagnosis, treatment } = this.state;
    // console.log(">>>>>>>>>>>>>>>>>>>>", this.props)
    const { listpatientDetails } = this.props
    // console.log("listpatientDetailslistpatientDetailslistpatientDetails", listpatientDetails[this.props.location.state.index])
    // console.log("myPatient", this.props.loginDetails)

    const patientDetails = listpatientDetails[this.props.location.state.index]
    return (
      <div className="inner-pageNew">
        <section>
          <header class="header">
            <div class="brand">
              <a href="" class="backArrow">
                <i class="fas fa-chevron-left" />
              </a>
              <div class="mobileLogo">
                <a href="" class="logo">
                  <img src="../images/logo.jpg" />{" "}
                </a>
              </div>
            </div>

          </header>

          <aside class="sideMenu">
            <ul class="sidemenuItems">
              <li>
                <a href="" title="Personal">
                  <img src="../images/icon2-color.png" />
                </a>
                <div className="menu-text">
                  <span>Personal</span>
                </div>
              </li>
              <li>
                <a href="" title="My Schedules">
                  <img src="images/app-icon-color.png" />
                </a>
                <div class="menu-text">
                  <span>My Schedules</span>
                </div>
              </li>
              <li>
                <a href="" title="Booked Appoinments">
                  <img src="images/book-icon-color.png" />
                </a>
                <div class="menu-text">
                  <span>Booked Appointments</span>
                </div>
              </li>
              <li>
                <a href="" title="My Patients">
                  <img src="images/patient-icon-color.png" />
                </a>
                <div class="menu-text">
                  <span>My Patients</span>
                </div>
              </li>
              <li>
                <a href="" title="Manage ADs">
                  <img src="images/icon-manage-color.png" />
                </a>
                <div class="menu-text">
                  <span>Manage ADs</span>
                </div>
              </li>

            </ul>
          </aside>
          <div class="mainMenuSide" id="main-content">
            <div class="wrapper">
              <div class="container-fluid">
                <div class="body-content">
                  <div class="row">


                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                      <div class="doSection card">
                        <div class="row">

                          <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="doctorImage">
                              <img src="https://images.pexels.com/photos/1239291/pexels-photo-1239291.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=650&w=940" /></div>
                            <div class="doc-title">
                              <p class="title-name">  {patientDetails && patientDetails.patientName}</p>


                            </div>
                            <div class="docButton">
                              <ul>
                                <li class="clr10025" id="appointment"><a title="Get Appointment">  <span>Telemedicine data</span></a></li>
                                <li class="clr10027" id="profile"><a title="View Profile"> <span>QR Code</span></a></li>


                              </ul>
                            </div>
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-12 col-12">

                            <div class="doctor-appoitment" id="contant1">
                              <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                  <div class="patDetails">
                                   
                                    <h5>Anamnesis</h5>
                                    <hr />

                                    <textarea class="form-control" rows="5" name="anamnesis" value={anamnesis} onChange={this.handleChange}/>
                                    <div class="float-right pat-detailsBtn">
                                      {/* <button onClick={this.updateFeedback}>Save</button> */}
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                  <div class="patDetails">

                                    <h5>Objectives</h5>
                                    <hr />

                                    <textarea class="form-control" rows="5" name="objectives" value={objectives} onChange={this.handleChange} />
                                    <div class="float-right pat-detailsBtn">
                                      {/* <button onClick={this.updateFeedback}>Save</button> */}
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                  <div class="patDetails">

                                    <h5>Diagnosis</h5>
                                    <hr />
                                    <textarea class="form-control" rows="5" name="diagnosis" value={diagnosis} onChange={this.handleChange} />
                                    <div class="float-right pat-detailsBtn">
                                      {/* <button onClick={this.updateFeedback}>Save</button> */}
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                  <div class="patDetails">
                                   

                                    <h5>Treatment</h5>
                                    <hr />

                                    <textarea class="form-control" rows="5" name="treatment" value={treatment} onChange={this.handleChange} />
                                    <div class="float-right pat-detailsBtn">
                                      <button onClick={this.updateFeedback}>Save</button>
                                    </div>
                                  </div>
                                </div>

                              </div>
                            </div>


                          </div>
                        </div>


                      </div>

                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div> */}

          </section>
      </div>

    )
  }
}
const mapStateToProps = state => ({
  loginDetails: state.loginReducer.loginDetails,
  listpatientDetails: state.appointmentReducer.listpatientDetails,
});

const mapDispatchToProps = dispatch => ({
  listPatientInfo: (listinfo) => dispatch(listPatientInfo(listinfo)),
  updateDoctorfeedback:(updateinfo)=>dispatch(updateDoctorfeedback(updateinfo))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyPatientList);