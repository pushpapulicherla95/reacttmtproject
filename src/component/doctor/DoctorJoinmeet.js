import React, { Component } from 'react';
import { connect } from 'react-redux';
import socketIOClient from "socket.io-client";
import URL from '../../asset/configUrl';
import { chatHistoryAPICall } from "../../service/appointment/action";
import Loader from "react-loader-spinner";
import { updateFeedback } from "../../service/appointment/action";

var SocketIOFileUpload = require('socketio-file-upload');


// variables
var roomNumber,doctorName;
var localStream;
var remoteStream;
var rtcPeerConnection;
var iceServers = {
    'iceServers': [
        { 'urls': 'stun:stun.services.mozilla.com' },
        { 'urls': 'stun:stun.l.google.com:19302' },
        { 'urls': 'stun:stun1.l.google.com:19302' },
        { 'urls': 'stun:stun2.l.google.com:19302' },
        { 'urls': 'stun:stun3.l.google.com:19302' },
        { 'urls': 'stun:stun4.l.google.com:19302' },
        { 'urls': 'stun:stun.services.mozilla.org' }
    ]
}
var streamConstraints = { audio: true, video: true };
var isCaller = false;

var socket = socketIOClient(URL.SOCKET_URL);
var uploader = new SocketIOFileUpload(socket, {
	chunkSize: 1024 * 1000
});

// Do something on upload progress:
uploader.addEventListener("progress", function(event){
    var percent = event.bytesLoaded / event.file.size * 100;
    console.log("File is : ", percent.toFixed(2), " : percent loaded");
});

// Do something when a file is uploaded:
uploader.addEventListener("complete", function(event){
    console.log("In client file upload complete : status : ",event.success);
    console.log("In client file upload complete : file name : ",event.file.name);
    socket.emit('chat-message', {
        filename:event.file.name,
        file:event.file,
        room: roomNumber,
        type:"doctor",
        name:doctorName,
        patientId:localStorage.getItem("patientId"),
        doctorId:localStorage.getItem("doctorId"),
        isfile:true
    });
});

socket.on('chat-message', function (event) {
    console.log("in chat meesage : client : room number : ",event.room,"type : ",event.type,"name : ",event.name)
    var ul = document.getElementById("chatMessagesId");
    var li = document.createElement("li");

    if(event.type === "doctor"){
        li.setAttribute("class", "outgoing-msg float-right");
    }else{
        li.setAttribute("class", "incoming-msg float-left");
    }
    var ptag = document.createElement("p");
    ptag.innerHTML=event.name;
    var ptag1 = document.createElement("p");
    if(event.isfile){
        console.log("in chat message : client : is file : ",event.isfile)
        ptag1.innerHTML=event.filename;
    }else{
        console.log("in chat message : client : msg : ",event.msg)
        ptag1.innerHTML=event.msg;
    }
    
    // var p = document.createTextNode(event.name);
    // var p1 = document.createTextNode(event.msg);
    li.appendChild(ptag1);
    li.appendChild(ptag);
    ul.appendChild(li);
});

socket.on('created', function (room) {
    console.log("in client created ")
    navigator.mediaDevices.getUserMedia(streamConstraints).then(function (stream) {
        localStream = stream;
        document.getElementById("localVideo").srcObject = stream;
        isCaller = true;
    }).catch(function (err) {
        console.log('An error ocurred when accessing media devices', err);
    });
});

socket.on('joined', function (room) {
    console.log("in client joined ")
    navigator.mediaDevices.getUserMedia(streamConstraints).then(function (stream) {
        localStream = stream;
        document.getElementById("localVideo").srcObject = stream;
        socket.emit('ready', roomNumber);
    }).catch(function (err) {
        console.log('An error ocurred when accessing media devices', err);
    });
});

socket.on('candidate', function (event) {
    console.log("in client candidate ")
    var candidate = new RTCIceCandidate({
        sdpMLineIndex: event.label,
        candidate: event.candidate
    });
    rtcPeerConnection.addIceCandidate(candidate);
});

socket.on('ready', function () {
    console.log("in client ready : isCaller", isCaller)
    if (isCaller) {
        rtcPeerConnection = new RTCPeerConnection(iceServers);
        rtcPeerConnection.onicecandidate = onIceCandidate;
        rtcPeerConnection.ontrack = onAddStream;
        rtcPeerConnection.addTrack(localStream.getTracks()[0], localStream);
        rtcPeerConnection.addTrack(localStream.getTracks()[1], localStream);
        rtcPeerConnection.createOffer()
            .then(sessionDescription => {
                rtcPeerConnection.setLocalDescription(sessionDescription);
                socket.emit('offer', {
                    type: 'offer',
                    sdp: sessionDescription,
                    room: roomNumber
                });
            })
            .catch(error => {
                console.log(error)
            })
    }
});

socket.on('offer', function (event) {
    console.log("in client offer : isCaller : ", isCaller)
    if (!isCaller) {
        rtcPeerConnection = new RTCPeerConnection(iceServers);
        rtcPeerConnection.onicecandidate = onIceCandidate;
        rtcPeerConnection.ontrack = onAddStream;
        rtcPeerConnection.addTrack(localStream.getTracks()[0], localStream);
        rtcPeerConnection.addTrack(localStream.getTracks()[1], localStream);
        rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(event));
        rtcPeerConnection.createAnswer()
            .then(sessionDescription => {
                rtcPeerConnection.setLocalDescription(sessionDescription);
                socket.emit('answer', {
                    type: 'answer',
                    sdp: sessionDescription,
                    room: roomNumber
                });
            })
            .catch(error => {
                console.log(error)
            })
    }
});

socket.on('answer', function (event) {
    rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(event));
})

function onIceCandidate(event) {
    console.log("in client onIceCandidate ")
    if (event.candidate) {
        console.log('sending ice candidate');
        socket.emit('candidate', {
            type: 'candidate',
            label: event.candidate.sdpMLineIndex,
            id: event.candidate.sdpMid,
            candidate: event.candidate.candidate,
            room: roomNumber
        })
    }
}

function onAddStream(event) {
    console.log("onAddStream")
    document.getElementById("remoteVideo").srcObject = event.streams[0];
    remoteStream = event.stream;
}


class DoctorJoinmeet extends Component {

    constructor(props) {
        super(props);

        this.state = {
            chat: '',
            enableVideo: true,
            enableAudio: true,
            enableCall: true,
            interChange: true,
            chatHistoryList:[],
            openfeedback:false,
        }
      
    }

    navBack = () => {
        console.log("vdvsvdvdvgscgc i am coming back")
        this.props.history.push('/dashboard/patient/appointment')
    }

    componentWillMount = () => {
        console.log("inside component willmount",this.props.loginDetails);
        // const {userInfo}=this.props.loginDetails;
        const historyJSON =
        {
           meetingRoom: localStorage.getItem("meetingRoom")
        }
        console.log("historyJSON : ",historyJSON)
        this.props.chatHistoryMethod(historyJSON);
    }

    componentWillReceiveProps = () => {
        console.log("component will receive props : ",this.props.listChatHistory)
        const { status,chatHistory } = this.props.listChatHistory
        console.log("component will receive props : ",chatHistory)
        this.setState({ chatHistoryList: chatHistory })
   
    }

    componentDidMount() {
        roomNumber = localStorage.getItem("meetingRoom")
        doctorName = localStorage.getItem("doctorName")
        //  let backUrl = this.props.location.state.backUrl
        //document.jitsiMeet(meetingRoom,doctorName, this.navBack);
        console.log('Joining meeting : ',roomNumber,"doctorName",doctorName)
        socket.emit('create or join', roomNumber)
        uploader.listenOnInput(document.getElementById("siofu_input"));
    }

    videoMuteGo = () => {
        localStream.getVideoTracks()[0].enabled = !(localStream.getVideoTracks()[0].enabled);
        this.setState({ enableVideo: !this.state.enableVideo }, () => {
            console.log("videoMuteGo.........:", this.state.enableVideo, "room-number", roomNumber)
        })
    };

    audioMuteGo = () => {
        localStream.getAudioTracks()[0].enabled = !(localStream.getAudioTracks()[0].enabled);
        this.setState({ enableAudio: !this.state.enableAudio }, () => {
            console.log("audioMuteGo.........:", this.state.enableAudio, "room-number", roomNumber)
        })
    };

    endMeetingGo = () => {
        document.getElementById("localVideo").srcObject = null;
        localStream = null;
        socket.emit('stop', {
            type: 'stop',
            room: roomNumber
        })
        this.setState({ openfeedback: true });
    };
  

    modalPopUpClose = () => {
        this.setState({ openfeedback: false });
    }
    fullScreenGo = () => {
        if (document.getElementById("remoteVideo").requestFullscreen) {
            document.getElementById("remoteVideo").requestFullscreen();
        } else if (document.getElementById("remoteVideo").mozRequestFullScreen) {
            document.getElementById("remoteVideo").mozRequestFullScreen(); // Firefox
        } else if (document.getElementById("remoteVideo").webkitRequestFullscreen) {
            document.getElementById("remoteVideo").webkitRequestFullscreen(); // Chrome and Safari
        }
    }

    interChangeVideo = () => {
        console.log("i am calling interchanging video")
        this.setState({
            interChange: !this.state.interChange
        })
    }

    chatGo = () =>{
        this.setState({ chat: this.state.chat }, () => {
            console.log("chatGo.........:", this.state.chat, "room-number", roomNumber)
            //socket.emit('chat-message', this.state.chat,roomNumber,"doctor",doctorName);
            socket.emit('chat-message', {
                msg: this.state.chat,
                room: roomNumber,
                type:"doctor",
                name:doctorName,
                patientId:localStorage.getItem("patientId"),
                doctorId:localStorage.getItem("doctorId"),
                isfile:false
            });
            this.setState({ chat: ''});
        })
    }    

    handleChange = (e) =>{
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }
  

    handleFeedback = () => {
        const {anamnesis,objectives,diagnosis,treatment} = this.state;

         let feedBack = {
            doctorId : localStorage.getItem("doctorId"),
            patientId : localStorage.getItem("patientId"),
            anamnesis:anamnesis,
            objectives:objectives,
            diagnosis:diagnosis,
            treatment:treatment

         }
         console.log("feedBack",feedBack);
       this.props.updateFeedback(feedBack);
    this.props.history.goBack();
      this.setState({ openfeedback: false });
    }
    render() {
        const { isLoading } = this.props
        const { chatHistoryList } = this.state;
        const { anamnesis, objectives, diagnosis, treatment } = this.state;

        const { enableVideo, enableAudio, enableCall, interChange,chat } = this.state;
        console.log("In render enableVideo", enableVideo, "interChange : ", interChange);
        let enableVideoClass = enableVideo ? 'video' : 'video not-allow';
        let enableAudioClass = enableAudio ? 'mic' : 'mic not-allow';
        let enableCallClass = enableCall ? 'answer not-allow' : 'answer';
        console.log("In render enableVideoClass", enableVideoClass);
        return (
            <React.Fragment>
            {isLoading && (
                <div className="pop-mod">
                    <div className="loder-back">
                        <Loader
                            type="Circles"
                            color="#00BFFF"
                            height="100"
                            width="100"
                        />
                    </div>
                </div>
            )} 

            <div class="mainMenuSide" id="main-content">
                <div class="wrapper">
                    <div class="container-fluid">
                        <div class="body-content">

                            <div class="row mt-3">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                    {!this.state.openfeedback && <div class="chat-section">
                                        <div class="video-section" onClick={this.interChangeVideo}>
                                            <video id={interChange ? "remoteVideo" : "localVideo"} autoPlay></video>
                                        </div>
                                        <div class="myVideo" onClick={this.interChangeVideo}>
                                            <video id={interChange ? "localVideo" : "remoteVideo"} autoPlay muted></video>
                                        </div>
                                        <div class="control-section">


                                            <div class="ctrlBtn">
                                                <ul>
                                                    <li><button onClick={this.videoMuteGo} className={enableVideoClass}></button></li>
                                                    <li><button onClick={this.audioMuteGo} className={enableAudioClass}></button></li>
                                                    <li><button onClick={this.fullScreenGo} className="resize"></button></li>
                                                    <li><button onClick={this.endMeetingGo} className={enableCallClass}></button></li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>}
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                    <div class="txtchat">
                                        <h5>Chat</h5>
                                        <hr />
                                        
                                           <ul class="chatBody" id="chatMessagesId">
                                           {
                                                    chatHistoryList && chatHistoryList.map((each,i) => (
                                                        <li key={i} className={each.type== 'doctor' ? 'outgoing-msg float-right' : 'incoming-msg float-left'}>
                                                         
                                                             <p>{each.msg}</p>  <p>{each.name}</p> 
                                                                
                                                        </li>
                                                        )) }
                                           </ul>
                                        
                                        <div class="chatInput">
                                            <input
                                                type="text"
                                                className="form-control commonInput"
                                                placeholder="Chat Here"
                                                name="chat"
                                                value={chat}
                                                onChange={this.handleChange}
                                            />
                                            <input type="file" id="siofu_input" />
                                            <button className="sendBtn" onClick={this.chatGo}><i class="fas fa-paper-plane chatIcon"></i></button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        </div>
                        </div>
                        </div>
                        {/* update feedback */}
                        
                <div className={this.state.openfeedback ? "modal show d-block" : "modal"} >
                    <div class="modal-dialog">
                        <div class="modal-content modalPopUp">
                            <div class="modal-header borderNone">
                                <h5 class="modal-title">Update Feedback</h5>
                                <button type="button" class="popupClose" data-dismiss="modal" onClick={this.modalPopUpClose}><i class="fas fa-times"></i></button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12">

                                            <div class="form-group">
                                                <label class="commonLabel">Anamnesis</label>
                                                <textarea className="form-control" name="anamnesis" value={anamnesis} onChange={this.handleChange}></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Objectives</label>
                                                <textarea className="form-control" name="objectives" value={objectives} onChange={this.handleChange}></textarea>

                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Diagnosis</label>
                                                <textarea className="form-control" name="diagnosis" value={diagnosis} onChange={this.handleChange}></textarea>

                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Treatment</label>
                                                <textarea className="form-control" name="treatment" value={treatment} onChange={this.handleChange}></textarea>

                                            </div>
                                        </div>

                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <button type="button" class="commonBtn float-right" onClick={this.handleFeedback}>Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                        </React.Fragment>

        )
    }
}
const mapStateToProps = state => ({
    listChatHistory: state.appointmentReducer.listChatHistory,
    isLoading: state.loginReducer.isLoading,
    loginDetails: state.loginReducer.loginDetails,
});

const mapDispatchToProps = dispatch => ({
    chatHistoryMethod: (historyInfo) => dispatch(chatHistoryAPICall(historyInfo)),
    updateFeedback:(feedBack)=>dispatch(updateFeedback(feedBack))

});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DoctorJoinmeet);