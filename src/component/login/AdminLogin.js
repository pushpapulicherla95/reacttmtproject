import React, { Component } from "react";
import { connect } from "react-redux";
import { userLogin, loginAdmin } from "../../service/login/action";
import { NavLink } from "react-router-dom";

class AdminLogin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            submitted: false
        };
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log("sdsdsd");
        let logininfo = {
            userName: this.state.email,
            password: this.state.password
        };

        this.props.loginAdmin(logininfo);

    }

    componentWillUnmount(){

    }
    componentWillReceiveProps = nextProps => {
        console.log("nextProps>>",nextProps)
        if (nextProps.loginDetails.status == "success") {
            this.props.history.push("/adminmainmenu");
        }
    }


    render() {
        const { email, password } = this.state;

        return (
            <div>
                <section className="loginSection">
                    <a href="/" className="logoDiv">
                        <img src="../images/logo.jpg" />
                    </a>
                    <div className="loginInner">
                        <div className="loginFormContent hideSidebar">
                            <h4 className="popupTitle">Login</h4>
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-group emailIcon">
                                    <input
                                        type="text"
                                        className="form-control commonInput"
                                        placeholder="Email"
                                        name="email"
                                        value={email}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group pass">
                                    <input
                                        type="password"
                                        className="form-control commonInput"
                                        placeholder="Password"
                                        name="password"
                                        value={password}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group ">
                                    <NavLink to="/login/forgot" className="forgotPass text-right">
                                        Forgot Password ?
                                     </NavLink>
                                </div>
                                <div className="form-group">
                                    <a href="javascript:void(0)">
                                        <button type="submit" className="btn loginBtn" >Login</button>
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails
});

const mapDispatchToProps = dispatch => ({
    userLogin: userinfo => dispatch(userLogin(userinfo)),
    loginAdmin: logininfo => dispatch(loginAdmin(logininfo))
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AdminLogin);
