import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import AdditionalInsurance from '../../patient/insurance/AdditionalInsurance'
import HealthInsurance from '../../patient/insurance/HealthInsurance'
import InsuranceAccident from '../../patient/insurance/InsuranceAccident'

class InsuranceInnerRouting extends Component {
  render() {
    return (
      <Switch>
        <Redirect path="/insurence" exact to="/insurence/healthInsurance" />
        <Route path="/insurance/additionalInsurance" component={AdditionalInsurance} />
        <Route path="/insurance/healthInsurance" component={HealthInsurance} />
        <Route path="/insurance/insuranceAccident" component={InsuranceAccident} />
      </Switch>
    );
  }
}

export default InsuranceInnerRouting;
