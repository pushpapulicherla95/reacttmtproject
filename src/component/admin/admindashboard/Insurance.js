import React,{Component} from "react";

class Insurance extends Component{

    render(){
        return(
            <div className="mainMenuSide" id="main-content">
            <div className="wrapper">
                <div className="container-fluid">
                    <div className="body-content">
                    

                <div className="search-result-section">
                    
                    <div className="search-card2">
                        <img src="images/placeholder-image.png"/>
                        <div className="doc-title">
                                <p className="title-name"> Insurance Company</p>
                                <p> +49 9876543210</p>

                                </div>
                                <div className="patientView">
                                    <button><i className="far fa-eye"></i></button>
                                    <button className="approveBtn"><i className="fas fa-check"></i></button>
                                </div>
                    </div>        
                </div>
                        
                    </div>
                </div>
            </div>

        </div>

        )
    }
}
export default Insurance;