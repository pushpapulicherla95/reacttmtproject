
import React, { Component } from 'react';
import { withRouter } from "react-router";
class AsideMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            submenuType: "Personal"
        }

    }
    render() {
        return (
            <aside className="sideMenu">
                <ul className="sidemenuItems">
                    <li><a href="javascript:void(0)" onClick={() => this.props.history.push({ pathname: "/submenu", state: { submenuType: "Personal" } })} title="Personal"><img src="../images/icon2-color.png" /></a><div className="menu-text"><span>Personal</span></div></li>
                    <li><a href="javascript:void(0)" onClick={() => this.props.history.push({ pathname: "/submenu", state: { submenuType: "Telemedicine" } })} title="Telemedicine"><img src="../images/icon5-color.png" /></a><div className="menu-text"><span>Telemedicine</span></div></li>
                    <li><a href="javascript:void(0)" onClick={() => this.props.history.push({ pathname: "/submenu", state: { submenuType: "HealthRecord" } })} title="Health Records"><img src="../images/icon1-color.png" /></a><div className="menu-text"><span>Health Records</span></div></li>
                    <li><a href="javascript:void(0)" title="Pharmacy"><img src="../images/icon4-color.png" /></a> <div className="menu-text"><span>Pharmacy</span></div></li>
                    <li><a href="javascript:void(0)" onClick={() => this.props.history.push({ pathname: "/submenu", state: { submenuType: "MedicalSpecialist" } })} title="Find Your medical specialist"><img src="../images/icon7-color.png" /></a><div className="menu-text"><span>Find Your medical specialist</span></div></li>
                    {/* <li><a href="/mainmenu" title="Services"><img src="../images/icon7-color.png" /></a><div className="menu-text"><span>Services</span></div></li> */}
                    <li><a onClick={() => this.props.history.push({ pathname: "/submenu", state: { submenuType: "shareData" } })} title="Share Data"><img src="../images/icon6-color.png" /></a><div className="menu-text"><span>Share data</span></div></li>
                </ul>
            </aside>
        )
    }
}

export default withRouter(AsideMenu);
