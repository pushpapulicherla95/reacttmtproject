import React from "react";
import {
  healthinsurance,
  getAllinsurance
} from "../../../service/insurance/action";
import { getUploadFiles, handleFileDelete } from "../../../service/upload/action";
import axios from "axios";
import { connect } from "react-redux";
import URL from "../../../asset/configUrl";
import { toastr } from "react-redux-toastr";
import { awsPersonalFiles } from "../../../service/common/action";

import { onlyNumbers, onlyAlphabets } from "../personal/KeyValidation";
class HealthInsurance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      healthName: "",
      address: "",
      policyNumber: "",
      additionalInsurance: "",
      phone: "",
      typeInsurance: "",
      insurance: "",
      tab: true,
      uploadPopup: false,
      name: "",
      fileName: "",
      fileType: "",
      fileURL: "",
      index: 1,
      tabone: true,
      tabtwo: false,
      errors: {
        policyNumber: ""
      },
      policyNumberValid: false,
      formValid: false
    };
  }
  validateField(fieldName, value) {
    const { errors, policyNumberValid } = this.state;
    let policyNumbervalid = policyNumberValid;

    let fieldValidationErrors = errors;
    switch (fieldName) {
      case "policyNumber":
        policyNumbervalid = value.match(
          /^[0-9a-zA-Z]+$/
        );
        fieldValidationErrors.policyNumber = policyNumbervalid
          ? ""
          : "Please enter only AlphaNumeric.";
        break;


    }
    this.setState({
      errors: fieldValidationErrors,
      policyNumberValid: policyNumbervalid,

    },
      this.ValidationField
    )
  }
  ValidationField = () => {
    const {
      healthName,
      policyNumberValid,
    } = this.state
    this.setState({
      formValid:

        policyNumberValid




    });

  }
  handleAwsFiles = value => {
    window.open("https://data.tomatomedical.com/patientpersonal/" + value, '_blank');

  };
  tabOne = () => {
    this.setState({ tabone: true, tabtwo: false });
  };
  tabTwo = () => {
    this.setState({ tabone: false, tabtwo: true });
  };
  handleChangePopup = e => {
    var reader = new FileReader();
    var file = e.target.files[0];
    let name = file.name.split('.')[0]
    let type = "." + file.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileURL: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(file);
  };
  popupSubmit = () => {
    var phrase = this.state.fileURL;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);
    var { userInfo } = this.props.loginDetails;
    const payload = {
      uid: userInfo.userId,
      z: match[1],
      fileName: this.state.fileName,
      fileType: this.state.fileType,
      name: this.state.name,
      category: "healthinsurance"
    };
    this.setState({ uploadPopup: false });
    axios
      .post(URL.FILEHEALTHINSURANCE_UPLOAD, payload)
      .then(response => {
        response.data.status === "success"
          ? toastr.success("Message", response.data.message)
          : toastr.error("Message", response.data.message);
        if (response.data.status === "success") {
          var { userInfo } = this.props.loginDetails;
          const uploadFiles = {
            uid: userInfo.userId,
            category: "healthinsurance"
          };
          this.props.getUploadFiles(uploadFiles);

          this.setState({
            name: "",
            fileName: "",
            fileType: "",
            fileURL: ""
          });
          this.setState({ uploadPopup: false });
        }
      })
      .catch(error => {
        // this.setState({ uploadPopup: true });
      });
  };
  componentDidMount = () => {
    const { userInfo } = this.props.loginDetails;
    this.props.getAllinsurance(userInfo.userId);
    const uploadFiles = { uid: userInfo.userId, category: "healthinsurance" };
    this.props.getUploadFiles(uploadFiles);
  };

  componentWillReceiveProps(nextProps) {
    const data = this.props.healthInsurance;
    if (data) {
      this.setState({
        healthName: data.name,
        address: data.address,
        policyNumber: data.policyNumber,
        additionalInsurance: data.additionalInsurance,
        phone: data.phone,
        typeInsurance: data.type,
        insurance: data.ownInsurance
      });
    }
  }

  handleTab = () => {
    this.setState({ tab: !this.state.tab }, () => {
      const { userInfo } = this.props.loginDetails;
      this.props.getAllinsurance(userInfo.userId);
    });
  };

  handleChange = e => {
    e.preventDefault();
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };


  handleSubmit = e => {
    e.preventDefault();
    const { userInfo } = this.props.loginDetails;
    const payload = {
      userId: userInfo.userId,
      healthInsurance: {
        name: this.state.healthName,
        address: this.state.address,
        phone: this.state.phone,
        additionalInsurance: this.state.additionalInsurance,
        policyNumber: this.state.policyNumber,
        ownInsurance: this.state.insurance,
        type: this.state.typeInsurance
      }
    };

    this.props.healthinsurance(payload);
  };
  render() {
    const { tabone, tabtwo, formValid, errors } = this.state;
    return (
      <div className="" id="main-content">
        <div className="wrapper">
          <div className="container-fluid">
            <div className="body-content">
              <div className="tomCard">
                <div className="tomCardHead">
                  <h5 class="float-left">Insurances</h5>
                </div>

                <div class="tab-menu-content">
                  <div class="tab-menu-title">
                    <ul>
                      <li class="menu1 active" onClick={this.tabOne}>
                        <a href="#">
                          <p>Health Insurance Company</p>
                        </a>
                      </li>

                      <li class="menu2" onClick={this.tabTwo}>
                        <a href="#">
                          <p>Attached Files</p>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                {tabone && (
                  <div className="tab-menu-content-section">
                    <div
                      id="content-1"
                      className={tabone ? "d-block" : "d-none"}
                    >
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Name</label>
                            <input
                              type="text"
                              class="form-control"
                              name="healthName"
                              value={this.state.healthName}
                              onChange={this.handleChange}
                            />
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Address (Street, ZIP/Postcode, City)</label>
                            <input
                              type="text"
                              class="form-control"
                              name="address"
                              value={this.state.address}
                              onChange={this.handleChange}
                            />
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Phone Number </label>
                            <input
                              type="text"
                              class="form-control"
                              name="phone"
                              value={this.state.phone}
                              onChange={this.handleChange}
                              onKeyPress={onlyNumbers} maxLength="15"
                            />


                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Additional Insurance</label>
                            <input
                              type="text"
                              class="form-control"
                              name="additionalInsurance"
                              value={this.state.additionalInsurance}
                              onChange={this.handleChange}
                            />
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Policy Number<sup>*</sup></label>
                            <input
                              type="text"
                              class="form-control"
                              name="policyNumber"

                              value={this.state.policyNumber}
                              onChange={this.handleChange}
                            />
                            <div style={{ color: "red" }}>{errors.policyNumber}</div>
                          </div>


                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>
                              Own insurance or Insured as a dependant
                            </label>
                            <select
                              class="form-control"
                              name="insurance"
                              value={this.state.insurance}
                              onChange={this.handleChange}
                            >
                              <option
                                data-label="Please select"
                                value=""
                                selected="true"
                              >
                                Please select
                              </option>
                              <option
                                data-label="Privately insured"
                                value="Privatelyinsured"
                              >
                                Privately insured
                              </option>
                              <option
                                data-label="As necessary"
                                value="Asnecessary"
                              >
                                As necessary
                              </option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Type of Insurance</label>
                            <select
                              class="form-control"
                              name="typeInsurance"
                              value={this.state.typeInsurance}
                              onChange={this.handleChange}
                            >
                              <option
                                data-label="Please select"
                                value=""
                                selected="true"
                              >
                                Please select
                              </option>
                              <option
                                data-label="medicare/ medicaid/ NHS "
                                value="medicare"
                              >
                                medicare/ medicaid/ NHS{" "}
                              </option>
                              <option
                                data-label="Private (full cover)"
                                value="Private(full cover)"
                              >
                                Private (full cover)
                              </option>
                              <option
                                data-label="Private (limited cover)"
                                value="Private(limited cover)"
                              >
                                Private (limited cover)
                              </option>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-12">
                          <button
                            type="button"
                            class="commonBtn float-right"
                            onClick={this.handleSubmit} disabled={!formValid}
                          >
                            Save
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                )}

                {tabtwo && (
                  <div id="content-1" className={tabtwo ? "d-block" : "d-none"}>
                    <button
                      type="button"
                      class="commonBtn2 float-right mb-2"
                      id="upBtn"
                      onClick={() =>
                        this.setState({
                          uploadPopup: !this.state.uploadPopup
                        })
                      }
                    >
                      {/* <svg
                        class="svg-inline--fa fa-plus fa-w-14"
                        aria-hidden="true"
                        focusable="false"
                        data-prefix="fa"
                        data-icon="plus"
                        role="img"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 448 512"
                        data-fa-i2svg=""
                      >
                        <path
                          fill="currentColor"
                          d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"
                        />
                      </svg>{" "} */}

                      <i className="fa fa-plus" />
                      <span> Upload new files</span>            </button>
                    <input
                      type="file"
                      id="file1"
                      name="file1"
                      style={{ display: "none" }}
                    />
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead class="thead-default">
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>FileName</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.props.getFilesDetails.fileDetails &&
                            this.props.getFilesDetails.fileDetails.map(
                              (each, i) => (
                                <tr>
                                  <td data-label="#">{i + 1}</td>
                                  <td data-label="Name"> {each.name}</td>
                                  <td data-label="FileName"> {each.fileName}</td>
                                  <td data-label="Action">
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.handleAwsFiles(each.fileUrl)
                                      }
                                    >
                                      <i class="fas fa-download" />
                                    </button>
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.props.handleFileDelete(each.id, each.fileUrl, "healthinsurance")
                                      }
                                    >
                                      <i class="fas fa-trash" />
                                    </button>
                                  </td>
                                </tr>
                              )
                            )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>

        {
          <div
            className={this.state.uploadPopup ? "modal d-block" : "modal"}
            id="myModal4"
          >
            <div class="modal-dialog">
              <div class="modal-content modalPopUp">
                <div class="modal-header borderNone">
                  <h5 class="modal-title">Upload File</h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    onClick={() =>
                      this.setState({ uploadPopup: !this.state.uploadPopup })
                    }
                  >
                    <i class="fas fa-times" />
                  </button>
                </div>
                <div class="modal-body">
                  <form>
                    <div class="row">
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="form-group">
                          <label className="form-label">File name</label>
                          <input
                            name="name"
                            className="form-control"
                            type="text"
                            id="name"
                            value={this.state.name}
                            onChange={this.handleChange}
                          />
                          <span />
                        </div>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="fileUp">
                          <label
                            for="fileUp1"
                            class="commonBtn text-center upload"
                          >
                            Upload File
                          </label>
                          <input
                            type="file"
                            id="fileUp1"
                            onChange={this.handleChangePopup}
                            style={{ display: "none" }}
                          />
                          {/* <p style={{ textAlign: "center" }}>hsfkjhsfj</p> */}
                        </div>
                      </div>

                      <div class="col-md-12 col-lg-12 col-sm-12 mt-2">
                        <button
                          type="button"
                          class="commonBtn float-right"
                          onClick={this.popupSubmit} disabled={!this.state.fileURL}
                        >
                          Submit
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}
const mapStateToProps = state => ({
  healthInsurance: state.insuranceReducer.healthinsuranceDetails,
  insuranceAccident: state.insuranceReducer.insuranceAccident,
  additionalInsurance: state.insuranceReducer.additionalInsurance,
  getFilesDetails: state.uploadReducer.getFilesDetails,
  loginDetails: state.loginReducer.loginDetails
});

const mapDispatchToProps = dispatch => ({
  healthinsurance: payload => dispatch(healthinsurance(payload)),
  getAllinsurance: payload => dispatch(getAllinsurance(payload)),
  getUploadFiles: payload => dispatch(getUploadFiles(payload)),
  handleFileDelete: (id, fileUrl, type) => dispatch(handleFileDelete(id, fileUrl, type))

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HealthInsurance);
