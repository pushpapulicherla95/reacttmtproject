import React, { Component } from "react";
import { connect } from "react-redux";
import { listPatientInfo } from "../../service/appointment/action";
import { userInfo } from "os";
class MyPatients extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    componentWillReceiveProps=()=>{
        // console.log("listpatientDetails", this.props.listpatientDetails);

    }

    componentDidMount = () => {

     
        const{userInfo}=this.props.loginDetails;
        const listinfo = {
            doctorId: userInfo.userId ||""
        }
        this.props.listPatientInfo(listinfo);

    }

    render() {
        const {listpatientDetails}=this.props;
        return (

            <div className="mainMenuSide" id="main-content">
                <div className="wrapper">
                    <div className="container-fluid">
                        <div className="body-content">


                            <div className="search-result-section">
                           
                               {(listpatientDetails && listpatientDetails.length > 0)? ( listpatientDetails.map((each,i)=>

                                <div className="search-card2">
                                    {/* <img src="https://images.pexels.com/photos/1239291/pexels-photo-1239291.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=650&w=940" /> */}
                                    <div className="doc-title">
                                        <p className="title-name"> {each.patientName}</p>
                                        <p> +49 9876543210</p>

                                    </div>
                                    <div className="patientView">
                                        <button key ={i} onClick={()=>this.props.history.push( {pathname:'/listpatient',state:{doctorId:each.doctorId,patientId:each.patientId,index:i}})}><i class="far fa-eye"></i></button>
                                    </div>
                               </div> ))
                             : (
                                  <tr>No Patient list available</tr>
                                )}
                                {/* <div className="search-card2">
                            <img src="https://images.pexels.com/photos/736716/pexels-photo-736716.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=650&w=940"/>
                            <div className="doc-title">
                                    <p className="title-name"> John Doe</p>
                                    <p> +49 9876543210</p>
    
                                    </div>
                                    <div className="patientView">
                                        <button onclick="location.href='listPatientDetails.html'">View</button>
                                    </div>
                        </div> */}



                            </div>

                        </div>
                    </div>
                </div>

            </div>



        )
    }
}
const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails,
    listpatientDetails: state.appointmentReducer.listpatientDetails,
});

const mapDispatchToProps = dispatch => ({
    listPatientInfo: (listinfo) => dispatch(listPatientInfo(listinfo)),

});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyPatients);