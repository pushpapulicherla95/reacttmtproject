import React, { Component } from "react";
import { Switch, Route, Redirect } from 'react-router-dom';
import PersonalData from "../../patient/personal/personaldata/personaldata";
import Addallergy from "../../patient/healthrecords/medicines/Addallery";
import Accident from "../../patient/healthrecords/medicines/Accident";
import Finding from "../../patient/healthrecords/medicines/Finding";
import HospitalLength from "../../patient/healthrecords/medicines/HospitalLength";
import Therapy from "../../patient/healthrecords/medicines/Therapy";
import Immunization from  "../../patient/healthrecords/medicines/Immunization";
import Medicine from "../../patient/healthrecords/medicines/Medicine";
class HealthRecordRoute extends Component {
    render() {
        return (
            <Switch>
                <Redirect path="/medicine" exact to="/medicine/data" /> 
                <Route path="/medicine/data" component={Medicine} />
                <Route path="/medicine/allergies" component={Addallergy} />
                <Route path="/medicine/accident" component={Accident} />
                <Route path="/medicine/finding" component={Finding} />
                <Route path="/medicine/hospital" component={HospitalLength} />
                <Route path="/medicine/therapy" component={Therapy} />
                <Route path="/medicine/immunization" component={Immunization} />

            </Switch>
        )
    }
}
export default HealthRecordRoute;