import React, { Component } from "react";
import { connect } from "react-redux";
import { change } from "../../service/login/action";
import { withRouter } from "react-router";

class Changepsw extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      resetcode: "",
      password: "",
      submitted: false,
      errors: {
        email: "",
        resetcode: "",
        password:""
      },
      emailValid: false,
      passwordValid: false,
      resetcodeValid:false,
      formValid: false,
     
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  }
  validateField(fieldName, value) {
    const { errors, emailValid, passwordValid ,resetcodeValid } = this.state;
    let emailvalid = emailValid;
    let passwordvalid = passwordValid;
    let resetcodevalid=resetcodeValid;
    let fieldValidationErrors = errors;
    switch (fieldName) {
      case "email":
        emailvalid = value.match(
          /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i
        );
        fieldValidationErrors.email = emailvalid
          ? ""
          : "Please enter valid email address.";
        break;
      case "password":
        passwordvalid = value.length !== 0
        fieldValidationErrors.password = passwordvalid
          ? ""
          : "Please enter valid password.";
        break;
        case "resetcode":
        resetcodevalid = value.length !== 0
        fieldValidationErrors.resetcode = resetcodevalid
          ? ""
          : "Please enter OTP.";
        break;

      default:
        break;
    }
    this.setState({
      errors: fieldValidationErrors,
         emailValid: emailvalid,
         passwordValid: passwordvalid,
         resetcodeValid:resetcodevalid
    },
    this.validateForm
    );
  }
  validateForm() {
    const {
      emailValid,passwordValid,resetcodeValid
    } = this.state;
    this.setState({
      formValid:
        emailValid &&
        passwordValid &&
        resetcodeValid




    });

  }
  componentWillReceiveProps = () => {
    const { staus } = this.props.changepswDetails;
    console.log("changepswDetails",this.staus)
    if (staus === "success") {
      this.props.history.push("/");
    }else{
      this.props.history.push("/login/newPassword")
    }
  };

  handleSubmit(e) {
    e.preventDefault();

    let changeinfo = {
      email: this.state.email,
      resetcode: this.state.resetcode,
      password: this.state.password
    };

    this.props.change(changeinfo);
    this.props.history.push("/");
  }

  render() {
    const { email, resetcode, password ,formValid,errors} = this.state;
    return (
      
      <section className="loginSection">
        <a href="/" className="logoDiv"><img src="../images/logo.jpg" /></a>
        <div className="loginInner">
          <div className="loginFormContent hideSidebar">
            <h4 className="popupTitle">Change Password</h4>
            <form onSubmit={this.handleSubmit}>
              <div className="form-group emailIcon">
                <input

                  id="validation-email"
                  className="form-control commonInput"
                  placeholder="Enter your Email"
                  name="email"
                  type="text"
                  value={email}
                  onChange={this.handleChange}
                />
                <div style={{ color: "red" }}>{errors.email}</div>

              </div>
              <div className="form-group emailIcon">
                <input

                  id="validation-email"
                  className="form-control commonInput"
                  placeholder="Enter your Reset Code"
                  name="resetcode"
                  type="text"
                  value={resetcode}
                  onChange={this.handleChange}
                />
                <div style={{ color: "red" }}>{errors.resetcode}</div>

              </div>
              <div className="form-group emailIcon">
                <input

                  id="validation-email"
                  className="form-control commonInput"
                  placeholder="Enter your Reset password"
                  name="password"
                  type="confirmpsw"
                  value={password}
                  onChange={this.handleChange}
                />
                <div style={{ color: "red" }}>{errors.password}</div>

              </div>
              <div className="form-group">
                <a href="#">
                  <button
                    type="submit"
                    className="btn loginBtn"
                    onClick={this.handleSubmit} disabled={!formValid}
                  >
                    Submit
                  </button>
                </a>
              </div>
            </form>
          </div>
        </div>
      </section>
    );
  }
}
const mapStateToProps = state => ({
  changepswDetails: state.loginReducer.changepswDetails
});

const mapDispatchToProps = dispatch => ({
  change: changeinfo => dispatch(change(changeinfo))
});
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Changepsw)
);
