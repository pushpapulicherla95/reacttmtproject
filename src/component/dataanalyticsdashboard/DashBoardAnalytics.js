import React from "react";
// import LandingHeader from "../common/LandingHeader";
// import $ from "jquery";
import axios from "axios";
import _ from "lodash";
// import { viewProfile } from "../../service/dashboard/action";
import DataListYourCard from "./DataListYourCard";
import DataListPublicCard from "./DataListPublicCard";
import CreatableSelect from "react-select/lib/Creatable";
import moment from 'moment';
import sample from './sample.csv';
import { toastr } from "react-redux-toastr";
import Select from 'react-select'
import TableText from "./TableText";
class DashBoardAnalytics extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      uploadPop: false,
      sampleFilePopup: false,
      type: "puplic",
      showSideMenu: false,
      viewProfile: false,
      viewReport: false,
      viewReportFilter: false,
      listReport: [],
      selectedCompareByList: [],
      selectedCompareWithList: [],
      yourData: false,
      publicData: true,
      columnFieldList: [],
      compareWithFieldList: [],
      rowFieldList: [],
      selectedFieldName: "",
      selectedColumnName: "",
      selectedRowName: "",
      compareColumnName: "",
      result: [],
      inputValue: "",
      value: [],
      quickViewList: [],
      quickViewParticular: [],
      fileName: "",
      fileDescription: "",
      filetitle: "",
      fileLicence: "",
      tags: [],
      name: "",
      fileUrl: "",
      file_size: "",
      invalid: "",
      filterList: [],
      tab: "tab1",
      tab2compareBy: [],
      tab2comparewith: [],
      tab2compareByColumnName: "",
      tab2comparewithColumnName: "",
      tab2columnFieldList: [],
      tab2columnField: "",
      independentValues: [],
      tableList: []
    };
  }

  Validate = () => {
    var _validFileExtensions = [".csv"];
    var arrInputs = document.getElementsByTagName("input");
    for (var i = 0; i < arrInputs.length; i++) {
      var oInput = arrInputs[i];
      if (oInput.type == "file") {
        var sFileName = oInput.value;
        if (sFileName.length > 0) {
          var blnValid = false;
          for (var j = 0; j < _validFileExtensions.length; j++) {
            var sCurExtension = _validFileExtensions[j];
            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
              blnValid = true;
              break;
            }
          }
          if (!blnValid) {
            return false;
          }
        }
      }
    }

    return true;
  }

  textInnova = () => {
    axios
      .get("http://192.168.2.30:8000/tomatomedical/API/testAnovaquickview")
      .then(response => {
        this.setState({ tab2compareBy: response.data.dependentValues, tab2comparewith: response.data.independentValues });
      });
  }

  componentWillMount() {
    this.listReport();
    this.quickViewInfo(this.state.type)
    this.textInnova()
  }

  quickView = (quickView) => {
    this.setState({ viewProfile: !this.state.viewProfile, quickViewParticular: quickView });
  };


  listReport = () => {
    axios
      .get("http://192.168.2.30:8000/tomatomedical/API/uniqueKeys")
      .then(response => {
        this.setState({ listReport: response.data });
      });
  };

  handletab2Submit = () => {

    const configs = {
      method: 'post',
      url: 'http://192.168.2.30:8000/tomatomedical/API/testANOVA',
      data: {
        "dependent_value": this.state.tab2compareByColumnName,
        "independent_values": this.state.independentValues

      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }
    axios(configs)
      .then(response => {
        this.setState({ tab: "tab3", viewReportFilter: false, tableList: response.data })
        console.log("respkps e", response.data)
        // this.setState({ quickViewList: response.data.lists, filterList: response.data.lists });
      });
  }


  quickViewInfo = (type) => {
    const configs = {
      method: 'post',
      url: 'http://192.168.2.30:8000/tomatomedical/API/quickview',
      data: { "uid": 101, "view": type },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }
    axios(configs)
      .then(response => {
        this.setState({ quickViewList: response.data.lists, filterList: response.data.lists });
      });
  }

  searchFilter = () => {
    let searchValue = document.getElementById("search").value.toLowerCase().replace(/\s+/g, '');
    if (searchValue) {
      let updatedList = this.state.filterList.slice()
      updatedList = updatedList.filter((each) => {
        let tags = each.tags.length > 0 && each.tags.filter((val) => val.toLowerCase().includes(searchValue))
        return each.title && each.title.toLowerCase().includes(searchValue) || each.name && each.name.toLowerCase().includes(searchValue) || tags.length != 0
      })
      this.setState({ quickViewList: updatedList })
    } else {
      this.setState({ quickViewList: this.state.filterList })
    }
  }

  handleChange = (event) => {
    let name = event.target.name;
    let value = event.target.value;

    this.setState({ [name]: value })

    if (event.target.files) {
      let file = event.target.files[0];
      let name1 = file.name.split('.')[0]
      let type = "." + file.name.split('.')[1]
      let reader = new FileReader()
      reader.onload = () => {
        this.setState({
          fileUrl: reader.result,
          fileName: name1,
          fileType: type,
          file_size: file.size
        }, () => { this.setState({ invalid: this.Validate() }) });
      };
      reader.readAsDataURL(file);
    }
  }

  handleSubmit = () => {
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(this.state.fileUrl);
    axios.post('http://192.168.2.30:8000/tomatomedical/API/dataAnalystFileUpload', {
      "name": this.state.name,
      "title": this.state.filetitle,
      "description": this.state.fileDescription,
      "tags": this.state.tags,
      "file_size": this.state.file_size,
      "file_type": this.state.fileType,
      "created_date": moment().format('YYYY-MM-DD'),
      "user_id": "101",
      "license": this.state.fileLicence,
      "fileUrl": match[1]
    })
      .then((response) => {
        this.quickViewInfo("self")
        toastr.success(response.data.message);
        this.setState({ uploadPop: false })
      })
      .catch((error) => {
        this.setState({ uploadPop: true })
        toastr.error(error.response.data.message);
      })
  }

  onSideMenuClick = () =>
    this.setState({ showSideMenu: !this.state.showSideMenu });

  render() {

    var compareWithFieldList = this.state.compareWithFieldList.slice();
    if (this.state.selectedColumnName && this.state.compareColumnName) {
      if (this.state.compareColumnName === this.state.selectedColumnName) {
        compareWithFieldList = _.reject(
          this.state.compareWithFieldList,
          a => a.tablename == this.state.selectedFieldName
        );
      }
    }

    const { quickViewParticular } = this.state
    return (
      <body class="inner-pageNew">
        <header className="header">
          <div className="brand">
            <a href="Dashboard.html" className="backArrow">
              <i className="fas fa-chevron-left"></i>
            </a>
            <div className="mobileLogo">
              <a
                href="javascript:void(0)"
                onClick={() =>
                  this.props.history.push({ pathname: "/hospitalmainmenu" })
                }
                className="logo"
              >
                <img src="../images/logo.jpg" />{" "}
              </a>
            </div>
          </div>
          <div className="top-nav">
            <div className="dropdown float-right mr-2">
              <a className="dropdown-toggle" data-toggle="dropdown">
                Tomato Medical
              </a>
              <div className="dropdown-menu">
                <a className="dropdown-item" href="#">
                  <i className="fa fa-edit icon-head-menu"></i>
                  <label className="head-menu">My account</label>
                </a>
                <a className="dropdown-item" href="#">
                  Setting
                </a>
                <a className="dropdown-item" href="#">
                  Logout
                </a>
              </div>
            </div>
          </div>
        </header>
        <div class="body-content">
          <div class="container-fluid">
            {this.state.tab != "tab3" ? <div class="data_analysis">
              <div class="row">
                <div>
                  <div class="profile_card" id="myid1">
                    <div class="mobileBackArrow" onclick="closeSide()">
                      <i class="fas fa-arrow-left"></i>
                    </div>
                    <div class="profile_image">
                      <img src="../images/adult-doctor-girl-355934.jpg" />
                    </div>
                    <div class="personal_details">
                      <p class="title">Organistaion Name</p>
                      <a href="javascript:void(0);" title="Logout">
                        <i class="fas fa-sign-out-alt"></i>
                      </a>
                    </div>
                    <div class="per_full_details">
                      <label>Email</label>
                      <h6>Companyname@gmail.com</h6>
                      <label>Phone</label>
                      <h6>+91 654 784 547</h6>
                      <label>Address</label>
                      <h6>71 Pilgrim Avenue Chevy Chase, MD 20815</h6>
                      <a
                        href="javascript:void(0);"
                        class="edit_icon_pencil"
                        data-toggle="modal"
                        data-target="#addEntry"
                      >
                        <i class="fas fa-pencil-alt"></i>
                      </a>
                    </div>
                    <div class="mobile-profile-icon">
                      <i class="fas fa-user-cog" onclick="openSide()"></i>
                    </div>
                  </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="total_count">
                    <div class="row">
                      <div class="col-lg-3 col-md-3 col-sm-12 col-12 ">
                        <div class="card_new bg-c-yellow">
                          <div class="left_sec_icon">
                            <i class="fas fa-users fs-25"></i>
                            <h6>Patient</h6>
                          </div>
                          <div class="right_sec_icon text-center">
                            <span class="count">200</span>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-12 col-12 ">
                        <div class="card_new bg-c-green">
                          <div class="left_sec_icon">
                            <i class="fas fa-user-md fs-25"></i>
                            <h6>Doctor</h6>
                          </div>
                          <div class="right_sec_icon text-center">
                            <span class="count">100</span>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-12 col-12 ">
                        <div class="card_new bg-c-pink ">
                          <div class="left_sec_icon">
                            <i class="fas fa-hospital fs-25"></i>
                            <h6>Hospital</h6>
                          </div>
                          <div class="right_sec_icon text-center">
                            <span class="count">50</span>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-12 col-12 ">
                        <div class="card_new bg-c-lite-green">
                          <div class="left_sec_icon">
                            <i class="fas fa-cloud-download-alt fs-25"></i>
                            <h6>Download</h6>
                          </div>
                          <div class="right_sec_icon text-center">
                            <span class="count">20</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row mt-2 mb-2">
                <div class="col-lg-5 col-md-6 col-sm-12 col-12 ">
                  <div class="chart_section mt-2 mb-2">
                    <div class="heading_title">
                      <p>Symptoms Charts</p>
                    </div>
                    <div class="chart_area">
                      <img src="images/charts_1.png" />
                    </div>
                  </div>
                  <div class="chart_section mt-2 mb-2">
                    <div class="heading_title">
                      <p>ChatBot data</p>
                    </div>
                    <div class="chart_area">
                      <img src="images/charts_1.png" />
                    </div>
                  </div>
                </div>

                <div class="col-lg-7 col-md-6 col-sm-12 col-12">
                  <div class="chart_sec mt-2 mb-2">
                    <div class="heading_title">
                      <div class="tabCard">
                        <div class="tabOne">
                          <ul>
                            <li
                              class={this.state.publicData && "active"}
                              onClick={() =>
                                this.setState({
                                  publicData: true,
                                  yourData: false,
                                  type: "puplic"
                                }, () => this.quickViewInfo(this.state.type))
                              }
                            >
                              Public
                            </li>
                            <li
                              class={this.state.yourData && "active"}
                              onClick={() =>
                                this.setState({
                                  publicData: false,
                                  yourData: true,
                                  type: 'self'
                                }, () => this.quickViewInfo(this.state.type))
                              }
                            >
                              Your Datasets
                            </li>
                            <li>
                              <input type="text" placeholder="Search" id="search" onChange={this.searchFilter} />{" "}
                              <i class="fas fa-search ser-icon-22"></i>
                            </li>
                          </ul>
                        </div>
                        <div class="tabTwo">
                          <button type="button" onClick={() => this.setState({ sampleFilePopup: !this.state.sampleFilePopup })}>
                            <i class="fas fa-cloud-upload-alt"></i>
                            <span> Upload</span>
                          </button>
                          {/* <a href={sample}>Click here Sample CSV File</a> */}
                        </div>
                      </div>

                      <div class="clearfix"></div>
                    </div>
                    {this.state.yourData && (
                      <DataListYourCard quickView={this.quickView} quickViewList={this.state.quickViewList} />
                    )}
                    {this.state.publicData && (
                      <DataListPublicCard quickView={this.quickView} quickViewList={this.state.quickViewList} />
                    )}
                  </div>
                </div>
              </div>
            </div> : <TableText tableList={this.state.tableList} />}
          </div>
        </div>

        <div class="modal" id="addEntry">
          <div class="modal-dialog">
            <div class="modal-content modalPopUp">
              <div class="modal-header borderNone">
                <h5 class="modal-title">Edit Profile</h5>
                <button type="button" class="popupClose" data-dismiss="modal">
                  <i class="fas fa-times"></i>
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="form-group">
                        <label class="commonLabel">Organisation Name</label>
                        <input type="text" class="form-control" />
                      </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="form-group">
                        <label class="commonLabel">Email</label>
                        <input type="text" class="form-control" />
                      </div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="form-group">
                        <label class="commonLabel">Phone</label>
                        <input type="text" class="form-control" />
                      </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="form-group">
                        <label class="commonLabel">Address</label>
                        <input type="text" class="form-control" />
                      </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <button type="button" class="commonBtn float-right">
                        Update
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        {this.state.viewProfile && quickViewParticular && (
          <div class="modal show" id="Quickinfo" style={{ display: "block" }}>
            <div class="modal-dialog">
              <div class="modal-content mod-data">
                <div class="modal-header borderNone newHeader">
                  <div class="image-model">
                    <img src="https://images.pexels.com/photos/211050/pexels-photo-211050.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" />
                  </div>
                  <div class="NewQuickheading">
                    <h5 class="modal-title">{quickViewParticular.title + " " + quickViewParticular.name}</h5>
                    <p class="sub-heading">
                      Airbnb listings and metrics in NYC, NY, USA (2019)
                    </p>
                  </div>
                  <button
                    type="button"
                    class="popupClosenew"
                    onClick={this.quickView}
                  >
                    <i class="fas fa-times"></i>
                  </button>
                </div>
                <div class="modal-body">
                  <form>
                    <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <p class="title600">Metadata</p>
                        <ul>
                          <li>
                            <i class="far fa-calendar"></i>
                            <span> Created date : {moment(quickViewParticular.created_date).format("MM-DD-YYYY")}</span>
                          </li>
                          <li>
                            <i class="far fa-calendar"></i>
                            <span> Last updated :{moment(quickViewParticular.modified_date).format("MM-DD-YYYY")}</span>
                          </li>
                          <li>
                            <i class="far fa-calendar"></i>
                            <span> Licence :{quickViewParticular.license}</span>
                          </li>
                          <li>
                            <i class="fas fa-tags"></i>
                            <span>
                              {" "}
                              Tags :{quickViewParticular.tags.map(val => "" + val + ',')}
                            </span>
                          </li>

                        </ul>
                      </div>

                      <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <p class="title600">Files</p>
                        <ul>
                          <li>
                            <i class="fas fa-list"></i>
                            <span> Total size : {quickViewParticular.file_size}</span>
                          </li>
                          <li>
                            <i class="fas fa-file"></i>
                            <span> {quickViewParticular.file_type}</span>
                          </li>
                        </ul>
                      </div>
                      <div class="col-lg-12col-md-12 col-sm-12 col-12">
                        <p class="title600">Description</p>
                        <p>
                          {quickViewParticular.description}
                        </p>
                      </div>

                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <button
                          type="button"
                          class="commonBtn float-right"
                          //   data-toggle="modal"
                          //   data-target="#Reportfilter"
                          onClick={() =>
                            this.setState({
                              viewProfile: false,
                              viewReportFilter: true
                            })
                          }
                        >
                          {" "}
                          View Report
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>)}
        {this.state.viewReportFilter && (
          <div
            class="modal show"
            id="Reportfilter"
            style={{ display: "block" }}
          >
            <div class="modal-dialog width900">
              <div class="modal-content mod-data">
                <div class="modal-header borderNone newHeader">
                  <div class="NewQuickheading">
                    <h5 class="modal-title">Report Filter</h5>
                  </div>
                  <button
                    type="button"
                    class="popupClosenew"
                    // data-dismiss="modal"
                    onClick={() => this.setState({ viewReportFilter: false })}
                  >
                    <i class="fas fa-times"></i>
                  </button>
                </div>
                <div class="modal-body">
                  <form>
                    <div class="tab-menu-content-section borderNone">
                      <div class="row">
                        <div class="col-12 p-lg-0">
                          <button type="button" className={this.state.tab == "tab1" ? "model_button_tab active" : "model_button_tab"} onClick={() => this.setState({ tab: "tab1" })}>Chart </button>
                          <button type="button" className={this.state.tab == "tab2" ? "model_button_tab active" : "model_button_tab"} onClick={() => this.setState({ tab: "tab2" })}>Analysis of varience</button>
                        </div>
                        {this.state.tab == "tab1" &&
                          <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="row">
                              <div class="col-lg-6 col-md-6 col-sm-12 col-12 p-lg-0 ">
                                <div class="listSection bor-new">
                                  <h5>
                                    {" "}
                                    <label>Compare By</label>
                                  </h5>
                                  <div class="form-group">
                                    <label>
                                      Column Name <sup>*</sup>
                                    </label>

                                    <select
                                      class="form-control"
                                      value={this.state.selectedColumnName}
                                      onChange={e => {
                                        const selectedIndex =
                                          e.target.selectedIndex - 1;
                                        this.setState(
                                          {
                                            selectedColumnName: e.target.value
                                          },
                                          () => {
                                            this.setState({
                                              columnFieldList: this.state.listReport[
                                                selectedIndex
                                              ].value
                                            });
                                          }
                                        );
                                      }}
                                    >
                                      <option>--Select Category--</option>
                                      {this.state.listReport &&
                                        this.state.listReport.map(val => {
                                          return (
                                            <option value={val.name}>
                                              {val.name}
                                            </option>
                                          );
                                        })}
                                    </select>
                                  </div>
                                  <div class="table-select">
                                    <div class="form-group">
                                      <label>
                                        Column Field <sup>*</sup>
                                      </label>
                                      <select
                                        class="form-control"
                                        value={this.state.selectedFieldName}
                                        onChange={e => {
                                          const selectedIndex =
                                            e.target.selectedIndex - 1;

                                          this.setState(
                                            { selectedFieldName: e.target.value },
                                            () => {
                                              this.setState({
                                                rowFieldList: this.state
                                                  .columnFieldList[selectedIndex]
                                                  .values
                                              });
                                            }
                                          );
                                        }}
                                      >
                                        <option>--Select Category--</option>
                                        {this.state.columnFieldList &&
                                          this.state.columnFieldList.map(val => (
                                            <option value={val.tablename}>
                                              {val.tablename}
                                            </option>
                                          ))}
                                      </select>
                                    </div>
                                    <div class="form-group">
                                      <label>
                                        Row field <sup>*</sup>
                                      </label>

                                      <select
                                        value={this.state.selectedRowName}
                                        class="form-control"
                                        onChange={e =>
                                          this.setState({
                                            selectedRowName: e.target.value
                                          })
                                        }
                                      >
                                        <option>--Select Category--</option>
                                        {this.state.rowFieldList &&
                                          this.state.rowFieldList.map(val => (
                                            <option value={val}>{val}</option>
                                          ))}
                                      </select>
                                    </div>
                                    <div class="form-group">
                                      <button
                                        type="button"
                                        class="btn bg-warning"
                                        onClick={() => {
                                          let addedArray = this.state.selectedCompareByList.slice();

                                          let payload = this.state.result.slice();
                                          if (
                                            this.state.selectedFieldName &&
                                            this.state.selectedRowName
                                          ) {
                                            payload.push({
                                              value: {
                                                column_name: this.state
                                                  .selectedFieldName,
                                                row_values: this.state.selectedRowName
                                              }
                                            });
                                          } else {
                                            payload.push({
                                              value: {
                                                column_name: this.state
                                                  .selectedFieldName
                                              }
                                            });
                                          }
                                          payload = _.uniqWith(payload, _.isEqual);
                                          this.setState({ result: payload });
                                          addedArray.push({
                                            column_name: this.state.selectedFieldName,
                                            row_values: this.state.selectedRowName
                                          });
                                          addedArray = _.uniqWith(
                                            addedArray,
                                            _.isEqual
                                          );
                                          this.setState({
                                            selectedCompareByList: addedArray
                                          });
                                        }}
                                        disabled={
                                          !(
                                            this.state.selectedFieldName ||
                                            this.state.selectedRowName
                                          )
                                        }
                                      >
                                        Add
                                </button>
                                    </div>
                                  </div>
                                  <div className="itemsList">
                                    <div className="Listitles">
                                      <span>Column Name</span>
                                      <span>Row Name</span>
                                      <span>Action</span>
                                    </div>
                                    <ul>
                                      {this.state.selectedCompareByList.length > 0
                                        ? this.state.selectedCompareByList.map(
                                          (each, i) => (
                                            <li>
                                              <span>{each.column_name}</span>
                                              <span>{each.row_values}</span>
                                              <span onClick={() => this.setState({ selectedCompareByList: this.state.selectedCompareByList.filter((item, index) => { if (index !== i) { return item; } }) })}>
                                                <i className="fa fa-times"></i>
                                              </span>
                                            </li>
                                          )
                                        )
                                        : <h5 style={{ textAlign: "center" }}>"No selected values"</h5>}
                                    </ul>
                                  </div>
                                </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-12 col-12  p-lg-0 ">
                                <div class="listSection bor-new2" >
                                  <h5>
                                    {" "}
                                    <label>Compare with</label>
                                  </h5>
                                  <div class="form-group">
                                    <label>
                                      Column Name <sup>*</sup>
                                    </label>

                                    <select
                                      class="form-control"
                                      value={this.state.compareColumnName}
                                      onChange={e => {
                                        const selectedIndex =
                                          e.target.selectedIndex - 1;

                                        this.setState(
                                          {
                                            compareColumnName: e.target.value
                                          },
                                          () => {
                                            this.setState({
                                              compareWithFieldList: this.state.listReport[
                                                selectedIndex
                                              ].value
                                            });
                                          }
                                        );
                                      }}
                                    >
                                      <option>--Select Category--</option>
                                      {this.state.listReport &&
                                        this.state.listReport.map(val => {
                                          return (
                                            <option value={val.name}>
                                              {val.name}
                                            </option>
                                          );
                                        })}
                                    </select>
                                  </div>
                                  <div class="table-select">
                                    <div class="form-group">
                                      <label>
                                        Column Field <sup>*</sup>{" "}
                                      </label>
                                      <select
                                        class="form-control"
                                        onChange={e =>
                                          this.setState({
                                            comparewithColumnName: e.target.value
                                          })
                                        }
                                      >
                                        <option>--Select Category--</option>
                                        {compareWithFieldList &&
                                          compareWithFieldList.map(val => (
                                            <option value={val.tablename}>
                                              {val.tablename}
                                            </option>
                                          ))}
                                      </select>
                                    </div>

                                    <div class="form-group">
                                      <button
                                        type="button"
                                        class="btn bg-warning"
                                        onClick={() => {
                                          let addedArray = this.state.selectedCompareWithList.slice();
                                          addedArray.push(
                                            this.state.comparewithColumnName
                                          );
                                          addedArray = _.uniqBy(addedArray);
                                          this.setState({
                                            selectedCompareWithList: addedArray
                                          });
                                        }}
                                        disabled={!this.state.comparewithColumnName}
                                      >
                                        Add
                                </button>
                                    </div>
                                  </div>
                                  <div className="itemsList">
                                    <div className="Listitles">
                                      <span>Column Name</span>
                                      <span>Row Name</span>
                                      <span>Action</span>
                                    </div>
                                    <ul>
                                      {this.state.selectedCompareWithList.length > 0
                                        ? this.state.selectedCompareWithList.map(
                                          (each, i) => (
                                            <li>
                                              <span>{each}</span>

                                              <span
                                                onClick={() => {
                                                  this.setState({
                                                    selectedCompareWithList: this.state.selectedCompareWithList.filter((item, index) => { if (index !== i) { return item; } })
                                                  });
                                                }}
                                              >
                                                <i className="fa fa-times"></i>
                                              </span>
                                            </li>
                                          )
                                        )
                                        : <h5>"No selected values"</h5>}
                                    </ul>
                                  </div>
                                </div>
                              </div>

                              <div class="col-md-12 col-lg-12 col-sm-12 mt-3 text-center">
                                <button
                                  type="button"
                                  class="commonBtn"
                                  disabled={
                                    !(
                                      this.state.comparewithColumnName &&
                                      this.state.selectedFieldName
                                    )
                                  }
                                  onClick={() => {
                                    const payload = {
                                      compare_by: this.state.result,
                                      compare_with: this.state.selectedCompareWithList
                                    };
                                    this.setState({ viewReportFilter: false }, () =>
                                      this.props.history.push({
                                        pathname: "/chart",
                                        state: payload
                                      })
                                    );
                                  }}
                                >
                                  {" "}
                                  Compare{" "}
                                </button>
                              </div>
                            </div>
                          </div>}


                        {this.state.tab == "tab2" &&
                          <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="row">
                              <div class="col-lg-6 col-md-6 col-sm-12 col-12  p-lg-0 ">
                                <div class="listSection bor-new">
                                  <h5>
                                    {" "}
                                    <label>Compare By</label>
                                  </h5>
                                  <div class="form-group">
                                    <label>
                                      Column Name <sup>*</sup>
                                    </label>

                                    <select
                                      class="form-control"
                                      value={this.state.tab2compareByColumnName}
                                      onChange={(e) => this.setState({ tab2compareByColumnName: e.target.value })}
                                    >

                                      <option>--Select Category--</option>
                                      {
                                        this.state.tab2compareBy.map((each, i) => <option value={each}>{each}</option>)
                                      }
                                    </select>
                                  </div>

                                </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-12 col-12  p-lg-0 ">
                                <div class="listSection bor-new2">
                                  <h5>
                                    {" "}
                                    <label>Compare with</label>
                                  </h5>
                                  <div class="form-group">
                                    <label>
                                      Column Name <sup>*</sup>
                                    </label>

                                    <select
                                      class="form-control"
                                      value={this.state.tab2comparewithColumnName}
                                      onChange={(e) => {

                                        this.setState({ tab2comparewithColumnName: e.target.value }, () => {
                                          let dummy = this.state.tab2comparewith.slice()
                                          let res = dummy.filter((each, i) => {
                                            return each.name == this.state.tab2comparewithColumnName
                                          })
                                          let result = []
                                          res[0].value.map(each => result.push({ label: each, value: each }))
                                          this.setState({ tab2columnFieldList: result })
                                        })
                                      }}
                                    >
                                      <option>--Select Category--</option>
                                      {
                                        this.state.tab2comparewith.map((each, i) => <option value={each.name}>{each.name}</option>)

                                      }
                                    </select>
                                  </div>
                                  <div class="table-select">
                                    <div class="form-group">
                                      <label>
                                        Column Field <sup>*</sup>{" "}
                                      </label>

                                      <Select
                                        isMulti
                                        onChange={(val) => {
                                          let res = []
                                          val.map(each => {
                                            if (res.indexOf(each.value) == -1) {
                                              res.push(each.value)
                                            }
                                          })
                                          this.setState({ independentValues: res })
                                        }}
                                        options={this.state.tab2columnFieldList}
                                      />
                                    </div>
                                  </div>

                                </div>
                              </div>

                              <div class="col-md-12 col-lg-12 col-sm-12 mt-3 text-center">
                                <button
                                  type="button"
                                  class="commonBtn"
                                  disabled={!this.state.independentValues.length > 0 || !this.state.tab2compareByColumnName}
                                  onClick={this.handletab2Submit}
                                >
                                  {" "}
                                  Compare{" "}
                                </button>
                              </div>
                            </div>
                          </div>}
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        )}
        {this.state.uploadPop && <div class="modal show" style={{ display: "block" }} >
          <div class="modal-dialog">
            <div class="modal-content modalPopUp">
              <div class="modal-header borderNone">
                <h5 class="modal-title">Add Dataset</h5>
                <button type="button" class="popupClose" onClick={() => { this.setState({ uploadPop: !this.state.uploadPop }) }}>
                  <i class="fas fa-times"></i>
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="form-group">
                        <label class="commonLabel">Name</label>
                        <input
                          type="text"
                          class="form-control"
                          name="name"
                          value={this.state.name}
                          onChange={this.handleChange}
                        />
                      </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="form-group">
                        <label class="commonLabel">Title</label>
                        <input
                          type="text"
                          class="form-control"
                          name="filetitle"
                          value={this.state.filetitle}
                          onChange={this.handleChange}
                        />
                      </div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="form-group">
                        <label class="commonLabel">Description</label>
                        <textarea
                          class="form-control"
                          name="fileDescription"
                          value={this.state.fileDescription}
                          onChange={this.handleChange}
                        ></textarea>
                      </div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="form-group">
                        <label class="commonLabel">Licence</label>
                        <textarea
                          class="form-control"
                          name="fileLicence"
                          value={this.state.fileLicence}
                          onChange={this.handleChange}
                        ></textarea>
                      </div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="form-group">
                        <label class="commonLabel">Tags</label>
                        <CreatableSelect
                          components={
                            {
                              DropdownIndicator: () => null,
                              IndicatorSeparator: () => null
                            }
                          }
                          isClearable
                          isMulti
                          onChange={(e) => {
                            e.map((each, i) => {
                              let result = this.state.tags.slice();
                              result.push(each.value)
                              this.setState({ tags: result })
                            })

                          }}
                        />
                      </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="form-group">
                        <label class="commonLabel">Upload Files</label>
                        <input type="file" name="fileUpload" value={this.state.fileUpload} onChange={this.handleChange} />
                        {this.state.fileUrl && this.state.invalid == false && <div style={{ color: "red" }}>sorry only .csv are allowed  </div>}
                      </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <button type="button" class="commonBtn float-right" disabled={!this.state.invalid} onClick={this.handleSubmit}>
                        Upload
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>}
        {this.state.sampleFilePopup && <div class="modal show" style={{ display: "block" }} >
          <div class="modal-dialog">
            <div class="modal-content modalPopUp">
              <div class="modal-header borderNone">
                <h5 class="modal-title">Sample CSV Format</h5>
                <button class="popupClose" onClick={() => this.setState({ sampleFilePopup: !this.state.sampleFilePopup })}>
                  <i class="fas fa-times"></i>
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="form-group">
                        <p>Please upload the CSV in the following format <a href={sample} download="sample.csv">Click here to download</a> Sample CSV File</p>

                      </div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <button type="button" class="commonBtn float-right" onClick={() => { this.setState({ uploadPop: !this.state.uploadPop, sampleFilePopup: false }) }}>
                        Next
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>}
      </body>
    );
  }
}

export default DashBoardAnalytics;

