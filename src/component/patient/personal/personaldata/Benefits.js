import React, { Component } from "react";
import {
  addBenefitsdata,
  getPersonaldata
} from "../../../../service/healthrecord/action";
import { connect } from "react-redux";
import $ from "jquery";
import axios from "axios";
import { getUploadFiles, handleFileDelete } from "../../../../service/upload/action";
import URL from "../../../../asset/configUrl";
import { toastr } from "react-redux-toastr";
import { awsPersonalFiles } from "../../../../service/common/action";
import { onlyAlphabets, onlyNumbers, onlyDate } from "../KeyValidation";

class Benefits extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 1,
      tabone: true,
      tabtwo: false,
      pensionAge: "",
      dataPensionStart: "",
      pensionWorkAccident: "",
      startDatePensionWorkAccident: "",
      disabilityInPercent: "",
      uploadPopup: false,
      name: "",
      fileName: "",
      fileType: "",
      fileURL: "",
      formValid: false
    };
  }
  ValidationField = () => {
    const {
      pensionAge,
      dataPensionStart,
      pensionWorkAccident,
      startDatePensionWorkAccident,
      disabilityInPercent, } = this.state
    this.setState({
      formValid:
        pensionAge &&
        dataPensionStart

    });

  }
  handleAwsFiles = value => {
    window.open("https://data.tomatomedical.com/patientpersonal/" + value, '_blank');

  };
  handleChangePopup = e => {
    var reader = new FileReader();
    var file = e.target.files[0];
    let name = file.name.split('.')[0]
    let type = "." + file.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileURL: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(file);
  };
  popupSubmit = () => {
    var phrase = this.state.fileURL;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);
    var { userInfo } = this.props.loginDetails;
    const payload = {
      uid: userInfo.userId,
      z: match[1],
      fileName: this.state.fileName,
      fileType: this.state.fileType,
      name: this.state.name,
      category: "benefits"
    };
    this.setState({ uploadPopup: false });
    axios
      .post(URL.FILEBENIFIT_UPLOAD, payload)
      .then(response => {
        response.data.status === "success"
          ? toastr.success("Message", response.data.message)
          : toastr.error("Message", response.data.message);
        if (response.data.status === "success") {
          var { userInfo } = this.props.loginDetails;
          const uploadFiles = {
            uid: userInfo.userId,
            category: "benefits"
          };
          this.props.getUploadFiles(uploadFiles);
          this.setState({ uploadPopup: false });
          this.setState({
            name: "",
            fileName: "",
            fileType: "",
            fileURL: ""
          });
        }
      })
      .catch(error => {
        // this.setState({ uploadPopup: true });
        this.setState({
          name: "",
          fileName: "",
          fileType: "",
          fileURL: ""
        });
      });
  };
  trigger = () => {

    this.setState({ uploadPopup: !this.state.uploadPopup }, () => {
      this.emptystate();
    });
  };
  emptystate = () => {
    this.setState({
      name: "",
      fileName: "",
      fileType: "",
      fileURL: ""
    })
  }
  componentWillMount() {
    const { userInfo } = this.props.loginDetails;

    const listpersonal = {
      uid: userInfo.userId
    };
    this.props.getPersonaldata(listpersonal);

    const uploadFiles = {
      uid: userInfo.userId,
      category: "benefits"
    };
    this.props.getUploadFiles(uploadFiles);
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.getpersonalDetails &&
      nextProps.getpersonalDetails.benefits != null &&
      nextProps.getpersonalDetails.benefits != 0
    ) {
      const listpersonal = nextProps.getpersonalDetails.benefits;
      let patientFindingListData = JSON.parse(listpersonal);

      this.setState({
        pensionAge: patientFindingListData.pensionAge,
        dataPensionStart: patientFindingListData.dataPensionStart,
        pensionWorkAccident: patientFindingListData.pensionWorkAccident,
        startDatePensionWorkAccident:
          patientFindingListData.startDatePensionWorkAccident,
        disabilityInPercent: patientFindingListData.disabilityInPercent
      });
    }
  }
  tabOne = () => {
    this.setState({ tabone: true, tabtwo: false });
  };
  tabTwo = () => {
    this.setState({ tabone: false, tabtwo: true });
  };
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value }, () => this.ValidationField());
  };

  handleData = () => {
    const {
      pensionAge,
      dataPensionStart,
      pensionWorkAccident,
      startDatePensionWorkAccident,
      disabilityInPercent
    } = this.state;
    const { userInfo } = this.props.loginDetails;
    const benefitsdata = {
      userId: userInfo.userId,
      benefits: {
        pensionAge: pensionAge,
        dataPensionStart: dataPensionStart,
        pensionWorkAccident: pensionWorkAccident,
        startDatePensionWorkAccident: startDatePensionWorkAccident,
        disabilityInPercent: disabilityInPercent
      }
    };

    this.props.addBenefitsdata(benefitsdata);
  };
  render() {
    const {
      tabone,
      tabtwo,
      pensionAge,
      dataPensionStart,
      pensionWorkAccident,
      startDatePensionWorkAccident,
      disabilityInPercent, formValid
    } = this.state;

    return (
      <div className="" id="main-content">
        <div className="wrapper">
          <div className="container-fluid">
            <div className="body-content">
              <div className="tomCard">
                <div className="tomCardHead">
                  <h5 className="float-left">BENEFITS DATA</h5>
                </div>

                <div className="tab-menu-content">
                  <div class="tab-menu-title">
                    <ul>
                      <li className={this.state.tabone ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabOne}>
                          <p>Benefits</p>
                        </a>
                      </li>
                      <li className={this.state.tabtwo ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabTwo}>
                          <p>Attached Files</p>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>

                {tabone && (
                  <div className="tab-menu-content-section">
                    <div
                      id="content-1"
                      className={tabone ? "d-block" : "d-none"}
                    >
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Pension age <sup>*</sup></label>
                            <select
                              class="form-control"
                              name="pensionAge"
                              onChange={this.handleChange}
                              value={this.state.pensionAge}
                            >
                              <option>--Select Pension age--</option>
                              <option data-label="Yes" value="Yes">
                                Yes
                              </option>
                              <option data-label="No" value="No">
                                No
                              </option>
                            </select>
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Date Pension start/started <sup>*</sup></label>
                            <input
                              type="date"
                              class="form-control"
                              name="dataPensionStart"
                              onChange={this.handleChange}
                              value={this.state.dataPensionStart}
                              onKeyPress={onlyDate}
                            />
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Pension (Work Accident)</label>
                            <select
                              class="form-control"
                              name="pensionWorkAccident"
                              onChange={this.handleChange}
                              value={this.state.pensionWorkAccident}
                            >
                              <option>--SelectPension (Work Accident)--</option>
                              <option data-label="Yes" value="Yes">
                                Yes
                              </option>
                              <option data-label="No" value="No">
                                No
                              </option>
                            </select>
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Start date of Pension (Work Accident)</label>
                            <input
                              type="date"
                              class="form-control"
                              name="startDatePensionWorkAccident"
                              onChange={this.handleChange}
                              value={this.state.startDatePensionWorkAccident}
                              onKeyPress={onlyDate}
                            />
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Disability in Percent</label>
                            <input
                              type="text"
                              class="form-control"
                              name="disabilityInPercent"
                              onChange={this.handleChange}
                              value={this.state.disabilityInPercent}
                            />
                          </div>
                        </div>
                        {/* <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                                    <div class="form-group">
                                                        <label>Degree of disability
                                                                   <sup>*</sup>
                                                        </label>
                                                        <input type="text" class="form-control" />
                                                    </div>
                                                </div> */}
                      </div>

                      <div class="row">
                        <div class="col-12">
                          <button
                            type="button"
                            class="commonBtn float-right"
                            onClick={this.handleData} disabled={!formValid}
                          >
                            Save
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                {tabtwo && (
                  <div id="content-2" className={tabtwo ? "d-block" : "d-none"}>
                    <button
                      type="button"
                      className="commonBtn2 float-right mb-2"
                      onClick={this.trigger}
                    >
                      <i className="fa fa-plus" />
                      <span> Upload new files</span>                  </button>
                    <div className="table-responsive">
                      <table className="table table-hover">
                        <thead className="thead-default">
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>FileName</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.props.getFilesDetails.fileDetails &&
                            this.props.getFilesDetails.fileDetails.map(
                              (each, i) => (
                                <tr>
                                  <td data-label="#">{i + 1}</td>
                                  <td data-label="Name">{each.name}</td>
                                  <td data-label="FileName"> {each.fileName}</td>
                                  <td data-label="Action">
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.handleAwsFiles(each.fileUrl)
                                      }
                                    >
                                      <i class="fas fa-download" />
                                    </button>
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.props.handleFileDelete(each.id, each.fileUrl, "benefits")
                                      }
                                    >
                                      <i class="fas fa-trash" />
                                    </button>
                                  </td>
                                </tr>
                              )
                            )}

                        </tbody>
                      </table>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        {
          <div
            className={this.state.uploadPopup ? "modal d-block" : "modal"}
            id="myModal"
          >
            <div class="modal-dialog">
              <div class="modal-content modalPopUp">
                <div class="modal-header borderNone">
                  <h5 class="modal-title">Upload File</h5>
                  <button
                    type="button"
                    className="close"
                    onClick={this.trigger}
                  >
                    <i class="fas fa-times" />
                  </button>
                </div>
                <div class="modal-body">
                  <form>
                    <div class="row">
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="form-group">
                          <label className="form-label">File name</label>
                          <input
                            className="form-control"
                            type="text"
                            name="name"
                            value={this.state.name}
                            onChange={this.handleChange}
                          />
                          <span />
                        </div>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="fileUp">
                          <label
                            for="fileUp1"
                            class="commonBtn text-center upload"
                          >
                            Upload File
                          </label>
                          <input
                            type="file"
                            id="fileUp1"
                            style={{ display: "none" }}
                            onChange={this.handleChangePopup}
                          />
                          {/* <p style={{ textAlign: "center" }}>hsfkjhsfj</p> */}
                        </div>
                      </div>

                      <div class="col-md-12 col-lg-12 col-sm-12 mt-2">
                        <button
                          type="button"
                          class="commonBtn float-right"
                          onClick={this.popupSubmit} disabled={!this.state.fileURL}
                        >
                          Submit
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  getpersonalDetails: state.healthrecordReducer.getpersonalDetails,
  loginDetails: state.loginReducer.loginDetails,
  getFilesDetails: state.uploadReducer.getFilesDetails
});

const mapDispatchToProps = dispatch => ({
  addBenefitsdata: benefitsdata => dispatch(addBenefitsdata(benefitsdata)),
  getPersonaldata: listpersonal => dispatch(getPersonaldata(listpersonal)),
  getUploadFiles: payload => dispatch(getUploadFiles(payload)),
  handleFileDelete: (id, fileUrl, type) => dispatch(handleFileDelete(id, fileUrl, type))

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Benefits);
