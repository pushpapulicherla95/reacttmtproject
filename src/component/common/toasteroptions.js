
export const toastrOptions = {
  timeOut: 3000, // by setting to 0 it will prevent the auto close
 // icon: (<myCustomIconOrAvatar />), // You can add any component you want but note that the width and height are 70px ;)
  onShowComplete: () => console.log('SHOW: animation is done'),
  onHideComplete: () => console.log('HIDE: animation is done'),
  onCloseButtonClick: () => console.log('Close button was clicked'),
  onToastrClick: () => console.log('Toastr was clicked'),
  showCloseButton: true, // false by default
  closeOnToastrClick: true, // false by default, this will close the toastr when user clicks on it
//   component: ( // this option will give you a func 'remove' as props
//     // <MyCustomComponent myProp="myValue">
//     //   <span>Hello, World!</span>
//     // </MyCustomComponent>
//   )
}


