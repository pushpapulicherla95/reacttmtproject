import React, { Component } from "react";
import Personal from "../../doctor/Personal";
import BoookedAppointments from "../../doctor/BookedAppointments";
import MySchedule from "../../doctor/MySchedule";
import DoctorJoinmeet from "../../doctor/DoctorJoinmeet";
import { connect } from "react-redux";
import MyPatients from "../../doctor/MyPatients";
import Logout from '../../common/Logout';
import { withRouter } from "react-router";
import Manageads from "../../doctor/MangeAD";
import OfflineAppointments from "../../doctor/OfflineAppointments";
import ShareDataMenu from '../../dashboard/SubMenu/ShareDataMenu'
class DoctorRouting extends Component {
    constructor(props) {
        super(props);
        this.state = {
            submenuType: "Personal"
        }
    }
    componentWillReceiveProps(nextProps) {

    }
    handleTab(type) {
        const { doctorProfile } = this.props;
        if (doctorProfile && !(doctorProfile.docInformation.specialistId == "" || doctorProfile.docInformation.specialistId == null)) {
            if (type == "Home") {
                this.props.history.push({ pathname: '/doctormainmenu' })
            } else {
                this.props.history.push({ pathname: '/docdashboard', state: { submenuType: type } })

            }
        }

    }
    render() {
        const { submenuType } = this.props.location.state || this.state;
        const { doctorProfile } = this.props;
        console.log("this.statte.subtype",this.state.submenuType,this.props)
        return (
            <div className="inner-pageNew">
                <section>
                    <header className="header">
                        <div className="brand">
                            <a href="javascript:void(0);" onClick={() => this.handleTab("Home")}  className="backArrow">
                                <i className="fas fa-chevron-left"></i>
                            </a>
                            <div className="mobileLogo">
                                <a href="javascript:void(0);" onClick={() => this.handleTab("Home")}  className="logo">
                                    <img src="../images/logo.jpg" /> </a></div>

                        </div>
                        <div className="top-nav">

                            <div className="dropdown float-right mr-2" >
                                <a className="dropdown-toggle" data-toggle="dropdown">
                                    Doctor Name
                            </a>
                                <Logout />
                            </div>
                        </div>
                    </header>
                    <aside className="sideMenu">
                        <ul className="sidemenuItems">
                            <li><a href="javascript:void(0);" onClick={() => this.handleTab("Personal")} title="Personal"><img src="../images/icon2-color.png" /></a><div className="menu-text"><span>Personal</span></div></li>
                            <li><a href="javascript:void(0);" onClick={() => this.handleTab("MySchedule")} title="My Schedules"><img src="images/app-icon-color.png" /></a><div className="menu-text"><span>My Schedules</span></div></li>
                            <li><a href="javascript:void(0);" onClick={() => this.handleTab("Appointments")} title="Appointment"><img src="images/book-icon-color.png" /></a><div className="menu-text"><span>Booked Appointments</span></div></li>
                            <li><a href="javascript:void(0);" onClick={() => this.handleTab("OfficeMeeting")} title="OfficeMeeting"><img src="images/book-icon-color.png" /></a><div className="menu-text"><span>Booked Office Appointments</span></div></li>

                            <li><a href="javascript:void(0);" onClick={() => this.handleTab("Patients")} title="Appointment" title="My Patients"><img src="images/patient-icon-color.png" /></a><div className="menu-text"><span>My Patients</span></div></li>
                            <li><a href="javascript:void(0);" onClick={() => this.handleTab("Manageads")} title="Manage ADs"><img src="images/icon-manage-color.png" /></a><div className="menu-text"><span>Manage ADs</span></div></li>
                            <li><a href="javascript:void(0);" onClick={() => this.handleTab("Communicator")} title="Manage ADs"><img src="images/icon-manage-color.png" /></a><div className="menu-text"><span>Communicator</span></div></li>


                        </ul>
                    </aside>
                    {submenuType == "Personal" && <Personal history={this.props.history} />}
                    {submenuType == "Appointments" && <BoookedAppointments history={this.props.history} />}
                    {submenuType == "OfficeMeeting" && <OfflineAppointments history={this.props.history} />}
                    {submenuType == "MySchedule" && <MySchedule history={this.props.history} />}
                    {submenuType == "DoctorJoinmeet" && <DoctorJoinmeet history={this.props.history} />}
                    {submenuType == "Patients" && <MyPatients history={this.props.history} details={this.props.loginDetails} />}
                    {submenuType == "Manageads" && <Manageads {...this.props} />}
                    {submenuType == "Communicator" && <ShareDataMenu {...this.props} />}

                </section>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails,
    testData: state.loginReducer.testData,
    doctorProfile: state.doctorReducer.doctorProfile,

})

const mapDispatchToprops = dispatch => ({
})
export default connect(
    mapStateToProps,
    mapDispatchToprops
)(withRouter(DoctorRouting));