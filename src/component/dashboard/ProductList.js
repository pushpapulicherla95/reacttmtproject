import React, { Component } from 'react';
import { connect } from 'react-redux'
import { createproductinfo, listproductinfo, updateproductinfo, deleteproductinfo } from '../../service/pharmacy/action';
class ProductList extends Component {


    state = {


        modalPop1: false,
      
        editproduct: {},
        productName: "",
        productTitle: "",
        productUses: "",
        productDescription: "",
        userid: "",
        productPrize: "",
        addproductpop:false,
        productId:"",
    }
    handleChange = (e) => {
        e.preventDefault();
        console.log("sdsdsdsd", e.target.value, e.target.name);
        this.setState({ [e.target.name]: e.target.value })
    }
    componentWillMount = () => {

        //listproduct
        const listproduct = {
            "userid": "2"
        }
        this.props.listproductinfo(listproduct);
        console.log("dmfjkhsdfkjhghfgshdifghsdhksdkjbf", listproduct)
    }
    addPharmacyproduct = (e) => {

        this.setState({ addproductpop: true});

    }

    modalPopUpClose = () => {
        this.setState({ addproductpop: false });
        this.setState({ modalPop1: false });

    }
    modalPopupOpen = (e) => {
    console.log("kdshfjkhdgfhf",e)
        this.setState({ modalPop1: true, editproduct: e });
        this.setState({
            productName :e.productName, productTitle:e.productTitle, productUses:e.productUses, productDescription:e.productDescription, userid:e.userid,productPrize:e.productPrize
        })
      

    }

    productSubmit = e => {
        const { productName, productTitle, productUses, productDescription, productId, productPrize } = this.state;
        const createproduct = {
            productName: productName,
            productTitle: productTitle,
            productUses: productUses,
            productDescription: productDescription,
            productId: productId,
            productPrize: productPrize

        }
        this.props.createproductinfo(createproduct);
        this.setState({addproductpop : false})
    }
    delete = (event) => {
        console.log("event",event);

        const deleteproduct =
        {
          "productId" : "5"
        }

        this.props.deleteproductinfo(deleteproduct);

    }
    updateproduct = () => {
        const { productName, productTitle, productUses, productDescription, productId, productPrize } = this.state;
        const updateproduct = {
            productName: productName,
            productTitle: productTitle,
            productUses: productUses,
            productDescription: productDescription,
            productId: '2',
          
            productPrize: productPrize

        }
        console.log("updateproduct", updateproduct);
        this.props.updateproductinfo(updateproduct);
        this.setState({modalPop1 : false})
    }
    render() {
        const { productName, productTitle, productUses, productDescription, userid, productPrize } = this.state;
        const { pharmacylist } = this.props.listproductDetails;
        return (
            <div className="cui-layout-content">
                <div className="cui-utils-content">
                    <div className="cui-utils-title mb-3"><strong className="text-uppercase font-size-16">List</strong></div>
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="card">
                                <div className="card-header">
                                    <div className="cui-utils-title"><strong>Doctor List</strong>
                                        <button type="button" className="btn commonBtn float-right" onClick={() => this.addPharmacyproduct()} data-toggle="modal" data-target="#myModal"><i className="fa fa-plus"></i> Add Pharmacy Product</button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead class="thead-default">
                                                <tr>
                                                    {/* <td>Name</td> */}
                                                    <th>ProductName</th>
                                                    <th>ProductTitle</th>
                                                    <th>ProductUses</th>
                                                    <th>ProductDescription</th>
                                                    <th>productPrize </th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    pharmacylist && pharmacylist.map(each => (
                                                        <tr>
                                                            <td data-label="productName">{each.productName}</td>
                                                            <td data-label="productTitle">{each.productTitle}</td>
                                                            <td data-label="productUses">{each.productUses}</td>
                                                            <td data-label="productDescription ">{each.productDescription}</td>
                                                            <td data-label="productPrize">{each.productPrize} </td>

                                                            <td data-label="Action"><a title="Edit" class="editBtn" onClick={(e) => this.modalPopupOpen(each)}><span id={5} class="fa fa-edit"></span></a>
                                                                <a title="Delete" class="deleteBtn" onClick={e =>
                                                                    window.confirm("Are you sure you wish to delete this item?") &&
                                                                    this.delete(e)
                                                                }><i class="fa fa-trash" data-toggle="modal" data-target="#myModal2"></i></a></td>
                                                        </tr>))
                                                }

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={this.state.addproductpop ? "modal show d-block" : "modal"} >
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Add Schedule</h4>
                                <button type="button" className="close" data-dismiss="modal" onClick={this.modalPopUpClose}>&times;</button>

                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label className="form-label">ProductName</label>
                                        <input className="form-control" name="productName" onChange={this.handleChange} value={productName} type="text" /><span ></span>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-label">ProductTitle</label>
                                        <input id="validation-email" className="form-control" name="productTitle" onChange={this.handleChange} type="text" value={productTitle} /><span ></span>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-label">ProductUses</label>
                                        <input id="validation-email" className="form-control" name="productUses" onChange={this.handleChange} type="text" value={productUses} /><span ></span>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-label">ProductDescription</label>
                                        <input id="validation-email" className="form-control" name="productDescription" onChange={this.handleChange} type="text" value={productDescription} /><span ></span>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-label">productPrize</label>
                                        <input id="validation-email" className="form-control" name="productPrize" onChange={this.handleChange} type="text" value={productPrize} /><span ></span>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-label">Userid</label>
                                        <input id="validation-email" className="form-control" name="userid" onChange={this.handleChange} type="text" value={userid} /><span ></span>
                                    </div>
                                    <div className="form-actions">
                                        <button type="button" className="btn commonBtn mr-3" onClick={this.productSubmit}>Submit</button>

                                    </div>

                                </form>
                            </div>
                           
                        </div>
                    </div>
                </div>

                <div class={this.state.modalPop1 ? "modal show d-block" : "modal"} id="edit_btn1">
                    <div class="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Edit </h4>
                                <button type="button" className="close" data-dismiss="modal" onClick={this.modalPopUpClose}>&times;</button>

                            </div>
                            <div className="modal-body">
                                <div className="form-group"><label className="form-label">ProductName</label>
                                    <input type="text" className="form-control" placeholder="Enter your Email" name="ProductName" onChange={this.handleChange} Value={this.state.editproduct.productName} />
                                </div>
                                <div className="form-group"><label className="form-label">ProductTitle</label>
                                    <input className="form-control" placeholder="Enter your Email" name="ProductTitle" Value={this.state.editproduct.productTitle} onChange={this.handleChange} type="text" />
                                </div>
                                <div className="form-group"><label className="form-label">ProductUses</label>
                                    <input className="form-control" placeholder="Enter your Email" name="ProductUses" Value={this.state.editproduct.productUses} onChange={this.handleChange} type="text" />
                                </div>
                                <div className="form-group"><label className="form-label">ProductDescription</label>
                                    <input className="form-control" placeholder="Enter your Email" name="ProductDescription" onChange={this.handleChange} Value={this.state.editproduct.productDescription} type="text" />
                                </div>
                                <div className="form-group"><label className="form-label">productPrize</label>
                                    <input className="form-control" placeholder="Enter your Email" name="productPrize" onChange={this.handleChange} Value={this.state.editproduct.productPrize} type="text" />
                                </div>




                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn commonBtn" onClick={this.updateproduct} >Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({

    createproductDetails: state.pharmacyReducer.createproductDetails,
    listproductDetails: state.pharmacyReducer.listproductDetails,
    updateproductDetails: state.pharmacyReducer.updateproductDetails,
    deleteproductDetails: state.pharmacyReducer.deleteproductDetails
});

const mapDispatchToProps = dispatch => ({

    createproductinfo: (createproduct) => dispatch(createproductinfo(createproduct)),
    listproductinfo: (listproduct) => dispatch(listproductinfo(listproduct)),
    updateproductinfo: (updateproduct) => dispatch(updateproductinfo(updateproduct)),
    deleteproductinfo: (deleteproduct) => dispatch(deleteproductinfo(deleteproduct))
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductList);