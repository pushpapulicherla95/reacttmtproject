import URL from "../../asset/configUrl";
import axios from "axios";
import actionType from "../pharmacy/actionType";
//CREATE PRODUCT
export const createproductinfo = createproduct => dispatch => {
  const configs = {
    method: "post",
    url: URL.CREATE_PRODUCT,
    data: createproduct,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(configs)
    .then(res => {
      if (res.status === 200) {
        dispatch({
          type: actionType.CREATE_PRODUCT_SUCCESS,
          payload: res.data
        });
      }
    })
    .catch(err => {
      dispatch({
        type: actionType.CREATE_PRODUCT_FAILURE,
        payload: err
      });
    });
};
//LIST PRODUCT
export const listproductinfo = listproduct => dispatch => {
  const configs = {
    method: "get",
    url: URL.LIST_PRODUCT,
    data: listproduct,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(configs)
    .then(res => {
      const { data, config, headers } = res;
      if (res.status === 200) {
        dispatch({
          type: actionType.LIST_PRODUCT_SUCCESS,
          payload: res.data
        });
      }
    })
    .catch(err => {
      dispatch({
        type: actionType.LIST_PRODUCT_FAILURE,
        payload: err
      });
    });
};
export const updateproductinfo = updateproduct => dispatch => {
  const configs = {
    method: "post",
    url: URL.UPDATE_PRODUCT,
    data: updateproduct,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(configs)
    .then(res => {
      if (res.status === 200) {
        dispatch({
          type: actionType.UPDATE_PRODUCT_SUCCESS,
          payload: res.data
        });
      }
    })
    .catch(err => {
      dispatch({
        type: actionType.UPDATE_PRODUCT_FAILURE,
        payload: err
      });
    });
};
export const deleteproductinfo = deleteproduct => dispatch => {
  const configs = {
    method: "post",
    url: URL.DELETE_PRODUCT,
    data: deleteproduct,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(configs)
    .then(res => {
      // const {data,config,headers} =res

      if (res.status === 200) {
        dispatch({
          type: actionType.DELETE_PRODUCT_SUCCESS,
          payload: res.data
        });
      }
    })
    .catch(err => {
      dispatch({
        type: actionType.DELETE_PRODUCT_FAILURE,
        payload: err
      });
    });
};
