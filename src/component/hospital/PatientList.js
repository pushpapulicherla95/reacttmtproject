import React, { Component } from "react";
import DoctorList from "../../component/hospital/DoctorList";
import { addHospitalPatient, getHospitalPatientList, updateHospitalPatient } from "../../service/hospital/action";
import { connect } from "react-redux";
import { onlyNumbers } from "../patient/personal/KeyValidation";

class PatientList extends Component {
    constructor(props) {
        super(props);
        var loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
        const { userInfo } = loginDetails;
        var userId = userInfo.userId || "";
        this.state = {
            uid: "",
            firstName: "",
            lastName: "",
            mobileNo: "",
            email: "",
            password: "",
            fileType: "",
            fileUrl: "",
            gender: "",
            address: "",
            userId: userId,
            patientTab: "List",
            patientListData: [],
            totalPatientList: [],
            updatePatient: false,
            seachText: "",
            patientId: "",
            errors: {
                firstName: "",
                lastName: "",
                mobileNo: "",
                email: "",
                password: "",
                gender: "",
                address: "",
                fileUrl: "",
                patientId: ""

            },
            firstNameValid: false,
            lastNameValid: false,
            passwordValid: false,
            mobileNoValid: false,
            addressValid: false,
            genderValid: false,
            emailValid: false,
            fileUrlValid: false,
            patientIdValid: false
        }
    }


    validateField(fieldName, value) {

        const { errors, firstNameValid, lastNameValid, passwordValid, mobileNoValid, fileUrlValid, addressValid, genderValid, emailValid,patientIdValid} = this.state;
        let firstNamevalid = firstNameValid;
        let lastNamevalid = lastNameValid;
        let passwordvalid = passwordValid;
        let mobileNovalid = mobileNoValid;
        let fileUrlvalid = fileUrlValid;
        let addressvalid = addressValid;
        let gendervalid = genderValid;
        let emailvalid = emailValid;
        let patientIdvalid = patientIdValid;
        let fieldValidationErrors = errors;
        switch (fieldName) {
            case "firstName":
                firstNamevalid = !(value.length < 3 || value.length > 20);
                fieldValidationErrors.firstName = firstNamevalid
                    ? ""
                    : "First Name should be 3 to 20 characters";
                break;
            case "lastName":
                lastNamevalid = !(value.length < 3 || value.length > 20);
                fieldValidationErrors.lastName = lastNamevalid
                    ? ""
                    : "Last Name should be 3 to 20 characters";
                break;
            case "password":
                passwordvalid = !(value.length < 3 || value.length > 20);
                fieldValidationErrors.password = passwordvalid
                    ? ""
                    : "Last Name should be 3 to 20 characters";
                break;
            case "email":
                emailvalid = value.match(
                    /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i
                );
                fieldValidationErrors.email = emailvalid
                    ? ""
                    : "Please enter valid email address.";
                break;
            case "mobileNo":
                mobileNovalid = !(value.length < 1);
                fieldValidationErrors.mobileNo = mobileNovalid
                    ? ""
                    : "Please enter phone number";
                break;
            case "fileUrl":
                // fileURLvalid = value.length ?true:false 
                fileUrlvalid = value.size > 50000 && value.size <= 1000000
                fieldValidationErrors.fileUrl = fileUrlvalid
                    ? ""
                    : "File size should be less than 1MB.";
                break;
            case "address":
                addressvalid = !(value.length < 3 || value.length > 300);
                fieldValidationErrors.address = addressvalid
                    ? ""
                    : "Please enter your Address";
                break;
            case "gender":
                gendervalid = !(value.length < 1);
                fieldValidationErrors.gender = gendervalid
                    ? ""
                    : "Please select Gender";
                break;
            case "patientId":
                patientIdvalid = !(value.length < 1);
                fieldValidationErrors.patientId = patientIdvalid
                    ? ""
                    : "Please Enter PatientId";
                break;

        }
        this.setState({
            errors: fieldValidationErrors,
            firstNameValid: firstNamevalid,
            lastNameValid: lastNamevalid,
            passwordValid: passwordvalid,
            emailValid: emailvalid,
            addressValid: addressvalid,
            genderValid: gendervalid,
            mobileNoValid: mobileNovalid,
            fileUrlValid: fileUrlvalid

        },
            this.validateForm()
        )
    }
    //
    validateForm() {
        const {
            firstNameValid, lastNameValid, passwordValid, mobileNoValid, fileUrlValid, addressValid, genderValid, emailValid
        } = this.state;
        this.setState({
            formValid:
                firstNameValid &&
                lastNameValid &&
                passwordValid &&
                mobileNoValid &&
                addressValid &&
                fileUrlValid&&
                genderValid &&
                emailValid 
             

        });
       

    }
    componentWillMount() {
        const info = {
            uid: this.state.userId
        }
        this.props.getHospitalPatientList(info);
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.hospitalPatientList.status != "failure") {
            const { hospitalPatientList } = nextProps.hospitalPatientList;
            this.setState({ patientListData: hospitalPatientList, totalPatientList: hospitalPatientList })

        } else {
            let listPatient = [];
            this.setState({ patientListData: listPatient, totalPatientList: listPatient })

        }
    }

    handleChange = (e) => {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value }, () => {
            this.validateField(name, value);
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const { firstName, lastName, mobileNo, email, password, fileType, fileUrl, gender, address, uid,patientId} = this.state;

        const updateInfo = {
            uid: uid,
            firstName: firstName,
            lastName: lastName,
            mobileNo: mobileNo,
            address: address,
            userId: this.state.userId,
            patientId:patientId

        }

        if (this.state.updatePatient) {
            this.props.updateHospitalPatient(updateInfo).then(response=>{
                if(response == "success"){
                    this.setState({ updatePatient: false, patientTab: "List" })

                }
            }).catch(err=>{

            })
        } else {
            var phrase = this.state.fileUrl;
            var myRegexp = /base64,(.*)/;
            var match = myRegexp.exec(phrase);
            const savedata = {
                uid: this.state.userId,
                firstName: firstName,
                lastName: lastName,
                mobileNo: mobileNo,
                email: email,
                password: password,
                fileType: fileType,
                fileUrl: fileUrl,
                gender: gender,
                address: address,
                // fileUrl: match[1],
                patientId:patientId
            }
            this.props.addHospitalPatient(savedata).then(res=>{
                if(res == "success"){
                    this.setState({ updatePatient: false, patientTab: "List" })

                }
            }).catch(err=>{

            })
        }
    }

    handleChangePopup = e => {
        var reader = new FileReader();
        const name = e.target.name;

        var value = e.target.files[0];

        reader.onload = () => {
            this.setState({
                fileUrl: reader.result,
                fileName: value.name,
                fileType: value.type
            }, () => {
                this.validateField(name, value);
            });
        };
        reader.readAsDataURL(value);
    };
    handleMenu(name) {
        this.setState({ patientTab: name, updatePatient: false }, this.emptyStateValues);

    }
    emptyStateValues = () => {

        this.setState({
            firstName: "",
            lastName: "",
            mobileNo: "",
            address: "",
            gender: "",
            updatePatient: false,
            email: "",
            password: "",
            uid: "",
            patientId:""
        })
    }
    viewPatient = (data) => {
        const { firstName, lastName, mobileNo, address, gender, uid, email, password ,patientId} = data;
        this.setState({
            firstName: firstName,
            lastName: lastName,
            mobileNo: mobileNo,
            address: address,
            gender: gender,
            patientTab: "Add",
            email: email,
            password: password,
            uid: uid,
            updatePatient: true,
            patientId:patientId

        })
    }
    searchPatient = (e) => {
        e.preventDefault();
        const { patientListData, totalPatientList } = this.state;
        const childArrays = totalPatientList.filter((data) => {
            return data.firstName.includes(e.target.value) || data.mobileNo.includes(e.target.value)
        })
        this.setState({ patientListData: childArrays })
    }
    render() {
        const { firstName, lastName, mobileNo, email, password, fileType, fileUrl, gender, address, patientTab, patientListData, errors,
            formValid, seachText, patientId } = this.state;
        return (
            <div className="mainMenuSide" id="main-content">
                <div className="wrapper">
                    <div className="container-fluid">
                        <div className="body-content">
                            <div className="newAdmin">
                                <div className="row mt-2">
                                    <div className="col-12">
                                        {patientTab == "Add" && <button className="newcommonBtn1" onClick={e => this.handleMenu("List")} ><i className="fas fa-list"></i> Patients  List</button>}
                                        {patientTab == "List" && <button className="newcommonBtn1" onClick={e => this.handleMenu("Add")}><i className="fas fa-plus"></i>  Add Patients</button>}
                                        {patientTab == "List"&& <div className="searchInput_new"><input type="text" name="seachText" onChange={this.searchPatient} placeholder="Search by name,phone" /><i class="fas fa-search icn_sea" ></i></div>}

                                    </div>
                                </div>
                                {patientTab == "Add" && <div className="row mt-2" id="contant-1" >
                                    <div className="col-12">
                                        <div className="admin_pro">
                                            <form>
                                                <div className="row">
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Firstname</label>
                                                            <input type="text" className="form-control input_h_35 boxnone" name="firstName" value={firstName} onChange={this.handleChange} readOnly={this.state.updatePatient ? "readonly" : ""}/>
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.firstName}</div>

                                                    </div>
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Lastname</label>
                                                            <input type="text" className="form-control input_h_35 boxnone" name="lastName" value={lastName} onChange={this.handleChange} readOnly={this.state.updatePatient ? "readonly" : ""}/>
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.lastName}</div>

                                                    </div>
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Email</label>
                                                            <input type="text" className="form-control input_h_35 boxnone" name="email" value={email} readOnly={this.state.updatePatient ? "readonly" : ""} onChange={this.handleChange} />
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.email}</div>

                                                    </div>

                                                </div>
                                                <div className="row">

                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Password</label>
                                                            <input type="password" className="form-control input_h_35 boxnone" name="password" value={password} readOnly={this.state.updatePatient ? "readonly" : ""} onChange={this.handleChange} />
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.password}</div>

                                                    </div>
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Mobile</label>

                                                            <input type="text" className="form-control input_h_35 boxnone" name="mobileNo" value={mobileNo} onChange={this.handleChange} onKeyPress={onlyNumbers} />
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.mobileNo}</div>

                                                    </div>
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Gender</label>
                                                            <select className="form-control input_h_35 boxnone" name="gender" value={gender} onChange={this.handleChange}>
                                                            <option value="">--select gender--</option>
                                                                <option>Male</option>
                                                                <option>Female</option>
                                                            </select>
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.gender}</div>

                                                    </div>
                                                </div>


                                                <div className="row">
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Address</label>
                                                            <textarea className="form-control boxnone" name="address" value={address} onChange={this.handleChange}></textarea>
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.address}</div>

                                                    </div>
                                                    <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel">Patient Id</label>

                                                            <input type="text" className="form-control input_h_35 boxnone" name="patientId" value={patientId} onChange={this.handleChange} onKeyPress={onlyNumbers} />
                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.patientId}</div>

                                                    </div>
                                                    {!this.state.updatePatient && <div className="col-md-4 col-lg-4 col-sm-12 col-12">
                                                        <div className="form-group">
                                                            <label className="commonLabel d-block">Picture Upload</label>
                                                            <input type="file" className="input_h_35 boxnone" name="fileUrl" onChange={this.handleChangePopup} />
                                                           

                                                        </div>
                                                        <div style={{ color: "red" }}>{errors.fileUrl}</div>
                                                    </div>}
                                                </div>

                                                <div className="row">
                                                    <div className="col-12 text-center">

                                                        {!this.state.updatePatient && <button type="button" className="newBtnsave" onClick={this.handleSubmit} disabled={!formValid}> Save</button>}
                                                        {this.state.updatePatient && <button type="button" className="newBtnsave" onClick={this.handleSubmit}> Update</button>}
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                }
                                {patientTab == "List" && <div className="row" id="contant-2">
                                    <div className="search-result-section">
                                        {patientListData.length > 0 ? patientListData.map((value, i) =>
                                            <div className="search-card2" key={i}>
                                                <img src="https://images.pexels.com/photos/1239291/pexels-photo-1239291.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=650&w=940" />
                                                <div className="doc-title">
                                                    <p className="title-name"> {value.firstName}</p>
                                                    <p> {value.mobileNo}</p>
                                                </div>
                                                <div className="patientView">
                                                    <button onClick={e => this.viewPatient(value)}><i className="far fa-eye"></i></button>
                                                </div>
                                            </div>
                                        ) : <div className="doc-title"><p> No Patientlist</p></div>
                                        }


                                    </div>
                                </div>}

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        )
    }

}
const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails,
    hospitalPatientList: state.hospitalReducer.hospitalPatientList
})

const mapDispatchToprops = dispatch => ({
    addHospitalPatient: (savedata) => dispatch(addHospitalPatient(savedata)),
    getHospitalPatientList: (info) => dispatch(getHospitalPatientList(info)),
    updateHospitalPatient: (updateInfo) => dispatch(updateHospitalPatient(updateInfo))
})

export default connect(
    mapStateToProps,
    mapDispatchToprops
)(PatientList);

