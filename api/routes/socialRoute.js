const express = require('express');
const socialLoginRouter = express.Router();
const { socialLogin } = require('../controller/login/sociallogin.api');

socialLoginRouter.route('/login')
    .post(socialLogin)
module.exports = socialLoginRouter;