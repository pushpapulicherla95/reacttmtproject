import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter,HashRouter} from 'react-router-dom';
import App from './view/App';
import serviceWorker from './serviceWorker';
import {createStore,applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import persistedReducer from './store/index'
import ReduxToastr from 'react-redux-toastr'
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css'
import { persistStore } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react';  
import ChatBot from  './../src/component/landing/ChatBot1';

// PWA implementation code

// import "../src/css/bootstrap.css";
// import '../src/css/style.css';
// import "../src/css/core.css";
// import "./css/card.css";
// import "./css/dropdown.css";
// import "./css/topBar.css";
// import "./css/utilites.css";
// import "./css/menuLeft.css";
// import "./css/responsive.css";
// import "./css/font-awesome.min.css";

// import "../src/js/jquery-3.4.1.min.js";
// import "../src/js/jquery.js";
// import "../src/js/popper.min.js";
// import "../src/js/bootstrap.js";
// import "../src/js/fontawesome.js";
// import "../src/js/external_api.js";
import dotenv from "dotenv";
       dotenv.config();

const store = applyMiddleware(thunk)(createStore)(persistedReducer)
const persistor = persistStore(store, { storage: sessionStorage });

ReactDOM.render(
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <BrowserRouter>
      
        <App />
      </BrowserRouter>
    </PersistGate>

    <div>
      <ReduxToastr
        timeOut={4000}
        newestOnTop={false}
        preventDuplicates
        position="top-right"
        transitionIn="fadeIn"
        transitionOut="fadeOut"
        progressBar
        closeOnToastrClick
      />
    </div>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker();
