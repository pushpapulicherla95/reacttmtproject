import React, { Component } from "react";
import { addPatientHealthData, updatePatientHealthData } from "../../service/patient/action";
import { connect } from "react-redux";
class PatientList extends Component {

    state = {
        uid:"",
        medictation:[],
        allergies:[],
        height:"",
        weight : "",
        healthBackground:"",
        addpatientpop:false
    }
    handleChange = (e) => {
        e.preventDefault();
        console.log("sdsdsdsd", e.target.value, e.target.name);
        this.setState({ [e.target.name]: e.target.value })
    }
    addPatientpopUp = (e) => {

        this.setState({ addpatientpop: true});

    }
    modalPopUpClose = () => {
        this.setState({ addpatientpop: false });
       

    }

    render() {
        const{ uid,   medictation,allergies, height,weight,healthBackground  }=this.state;
        return (



            <div className="cui-layout-content">
                <div className="cui-utils-content">
                    <div className="cui-utils-title mb-3"><strong className="text-uppercase font-size-16">List</strong></div>
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="card">
                                <div className="card-header">
                                    <div className="cui-utils-title"><strong>Patient List</strong>
                                        <button type="button" className="btn commonBtn float-right" data-toggle="modal" data-target="#myModal" onClick={() => this.addPatientpopUp()} ><i className="fa fa-plus"></i> Add Patient</button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead class="thead-default">
                                                <tr>
                                                    
                                                    <th>Medictation</th>
                                                    <th>Allergies</th>
                                                    <th>Height</th>
                                                    <th>Weight </th>
                                                    <th>HealthBackground</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            {/* <tbody>
                                                {
                                                    pharmacylist && pharmacylist.map(each => (
                                                        <tr>
                                                            <td data-label="productName">{each.productName}</td>
                                                            <td data-label="productTitle">{each.productTitle}</td>
                                                            <td data-label="productUses">{each.productUses}</td>
                                                            <td data-label="productDescription ">{each.productDescription}</td>
                                                            <td data-label="productPrize">{each.productPrize} </td>

                                                            <td data-label="Action"><a title="Edit" class="editBtn" onClick={(e) => this.modalPopupOpen(each)}><span id={5} class="fa fa-edit"></span></a>
                                                                <a title="Delete" class="deleteBtn" onClick={e =>
                                                                    window.confirm("Are you sure you wish to delete this item?") &&
                                                                    this.delete(e)
                                                                }><i class="fa fa-trash" data-toggle="modal" data-target="#myModal2"></i></a></td>
                                                        </tr>))
                                                }

                                            </tbody> */}
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={this.state.addpatientpop ? "modal show d-block" : "modal"} >
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Add Schedule</h4>
                                <button type="button" className="close" data-dismiss="modal" onClick={this.modalPopUpClose}>&times;</button>

                            </div>
                            <div className="modal-body">
                                <form>
                                <div className="form-group">
                                        <label className="form-label">UserId</label>
                                        <input id="validation-email" className="form-control" name="uid" onChange={this.handleChange} type="text" value={uid} /><span ></span>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-label">Medictation</label>
                                        <div className="checkbox">
                                    <label className="cui-utils-control cui-utils-control-checkbox">
                                        <input type="checkbox" checked=""/><span className="cui-utils-control-indicator"></span></label>
                                </div>
                                <div className="checkbox">
                                    <label className="cui-utils-control cui-utils-control-checkbox">
                                        <input type="checkbox" checked=""/><span className="cui-utils-control-indicator"></span></label>
                                </div>
                                <div className="checkbox">
                                    <label className="cui-utils-control cui-utils-control-checkbox">
                                        <input type="checkbox" checked=""/><span className="cui-utils-control-indicator"></span></label>
                                </div>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-label">Allergies</label>
                                        <div className="checkbox">
                                    <label className="cui-utils-control cui-utils-control-checkbox">
                                        <input type="checkbox" checked=""/><span className="cui-utils-control-indicator"></span></label>
                                </div>
                                <div className="checkbox">
                                    <label className="cui-utils-control cui-utils-control-checkbox">
                                        <input type="checkbox" checked=""/><span className="cui-utils-control-indicator"></span></label>
                                </div>
                                <div className="checkbox">
                                    <label className="cui-utils-control cui-utils-control-checkbox">
                                        <input type="checkbox" checked=""/><span className="cui-utils-control-indicator"></span></label>
                                </div>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-label">Height</label>
                                        <input id="validation-email" className="form-control" name="height" onChange={this.handleChange} type="text" value={height} /><span ></span>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-label">Weight</label>
                                        <input id="validation-email" className="form-control" name="weight" onChange={this.handleChange} type="text" value={weight} /><span ></span>
                                    </div>
                                    <div className="form-group">
                                        <label className="form-label">HealthBackground</label>
                                        <input id="validation-email" className="form-control" name="healthBackground" onChange={this.handleChange} type="text" value={healthBackground} /><span ></span>
                                    </div>
                                  
                                   
                                    <div className="form-actions">
                                        <button type="button" className="btn commonBtn mr-3" >Submit</button>

                                    </div>

                                </form>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => ({

    addpatientDetails: state.patientReducer.addpatientDetails,
    updatepatientDetails: state.patientReducer.addpatientDetails,

});

const mapDispatchToProps = dispatch => ({

    addPatientHealthData: (addPatient) => dispatch(addPatientHealthData(addPatient)),
    updatePatientHealthData: (updatePatient) => dispatch(updatePatientHealthData(updatePatient)),

});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PatientList);