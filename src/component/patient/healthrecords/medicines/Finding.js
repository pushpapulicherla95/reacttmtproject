import React, { Component } from "react";
import { connect } from "react-redux";
import {
  createFindingData,
  listFindingData,
  updateFindingData,
  deleteFindingData
} from "../../../../service/patient/action";
import { getUploadFiles,handleHealthDataDelete } from "../../../../service/upload/action";

import Select from "react-select";
import axios from "axios";
import URL from '../../../../asset/configUrl'
import { awsFiles } from '../../../../service/common/action'

class Finding extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 1,
      tabone: true,
      tabtwo: false,
      find: "",
      comment: "",
      title: "",
      addfindingpop: false,
      editfinding: false,
      editDetails: {},
      patientFindingListData: [],
      uploadPopup: false,
      name: "",
      fileName: "",
      fileType: "",
      fileURL: "",
      errors: {
        title: "",
        comment: ""
      },
      titleValid: false,
      commentValid: false,
      firstValid: false
    };
  }

  handleAwsFiles = value => {
    awsFiles(value).then(response => {
      window.location.href = response;
    });
  };

  handleChangePopup = e => {
    var reader = new FileReader();
    var file = e.target.files[0];
    let name = file.name.split('.')[0]
    let type = "." + file.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileURL: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(file);
  };
  popupSubmit = () => {
    var phrase = this.state.fileURL;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);
    var { userInfo } = this.props.loginDetails;
    const payload = {
      uid: userInfo.userId,
      z: match[1],
      fileName: this.state.fileName,
      fileType: this.state.fileType,
      name: this.state.name,
      category: "finding"
    };
    axios
      .post(URL.FILEFINDING_UPLOAD, payload)
      .then(response => {
        if (response.data.status === "success") {
          var { userInfo } = this.props.loginDetails;
          const uploadFiles = {
            uid: userInfo.userId,
            category: "finding"
          };
          this.props.getUploadFiles(uploadFiles);
          this.setState({ uploadPopup: false });
        }
      })
      .catch(error => {
        this.setState({ uploadPopup: true });
      });
  };

  tabOne = () => {
    this.setState({ tabone: true, tabtwo: false });
  };
  tabTwo = () => {
    this.setState({ tabone: false, tabtwo: true });
  };
  handleChange = e => {
    e.preventDefault();
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };

  //validation
  validateField(fieldName, value) {
    const { errors, titleValid, commentValid } = this.state;

    let titlevalid = titleValid;
    let commentvalid = commentValid;
    let fieldValidationErrors = errors;
    switch (fieldName) {
      case "title":
        titlevalid = !(value.length < 3 || value.length > 20);
        fieldValidationErrors.title = titlevalid
          ? ""
          : "Title should be 3 to 20 characters";
        break;
      case "comment":
        commentvalid = !(value.length < 3 || value.length > 20);
        fieldValidationErrors.comment = commentvalid
          ? ""
          : "Comment should be 3 to 20 characters";
        break;
    }
    this.setState(
      {
        errors: fieldValidationErrors,
        titleValid: titlevalid,
        commentValid: commentvalid
      },
      this.validateForm
    );
  }

  validateForm() {
    const { titleValid, commentValid } = this.state;
    this.setState({
      firstValid: titleValid && commentValid
    });
  }

  findingpop = () => {
    this.setState({ addfindingpop: true }, () => {
      this.emptystate();
    });
  };
  modalPopUpClose = () => {
    this.setState({
      addfindingpop: false,
      editfinding: false
    });
  };
  handleFinding = () => {
    const { title, find, comment } = this.state;
    const { userInfo } = this.props.loginDetails;

    const createfinding = {
      uid: userInfo.userId,
      findings: [
        {
          title: title,
          find: find,
          comment: comment
        }
      ]
    };

    this.props.createFindingData(createfinding);

    this.setState({
      addfindingpop: false
    });
  };
  componentWillMount() {
    const { userInfo } = this.props.loginDetails;

    const listfinding = {
      uid: userInfo.userId
    };
    // var { userInfo } = this.props.loginDetails;
    const uploadFiles = {
      uid: userInfo.userId,
      category: "finding"
    };
    this.props.getUploadFiles(uploadFiles);
    this.props.listFindingData(listfinding);
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.getfindinglist &&
      nextProps.getfindinglist.findings != null &&
      nextProps.getfindinglist.findings != 0
    ) {
      const listfinding = nextProps.getfindinglist.findings;
      let parsedContent = JSON.parse(listfinding);
      let patientFindingListData = parsedContent.findings;

      this.setState({
        patientFindingListData,
        addfindingpop: false
      });
    }
  }

  modalPopupOpen = e => {
    this.setState({ editfinding: true, editDetails: e });
    this.setState({
      title: e.title,
      find: e.find,
      comment: e.comment
    });
  };
  updateFinding = () => {
    const { title, find, comment } = this.state;
    const { userInfo } = this.props.loginDetails;
    const updatefinding = {
      uid: userInfo.userId,
      findings: [
        {
          title: title,
          find: find,
          comment: comment
        }
      ]
    };

    this.props.updateFindingData(updatefinding);
    this.setState({ editfinding: false });
  };
  deleteFinding = e => {
    const { userInfo } = this.props.loginDetails;

    const deletefinding = {
      uid: userInfo.userId,
      findings: [
        {
          title: e.title,
          find: e.find,
          comment: e.comment
        }
      ]
    };
    this.props.deleteFindingData(deletefinding);
  };
  emptystate = () => {
    this.setState({
      find: "",
      comment: "",
      title: ""
    });
  };

  render() {
    const {
      tabone,
      tabtwo,
      comment,
      title,
      find,
      addfindingpop,
      patientFindingListData,
      editfinding,
      firstValid,
      errors
    } = this.state;
    return (
      <React.Fragment>
        <div className="" id="main-content">
          <div className="wrapper">
            <div className="container-fluid">
              <div className="body-content">
                <div className="tomCard">
                  <div className="tomCardHead">
                    <h5 className="float-left">Finding</h5>
                  </div>

                  <div className="tab-menu-content">
                    <div class="tab-menu-title">
                      <ul>
                        <li
                          className={
                            this.state.tabone ? "menu1 active" : ""
                          }
                        >
                          <a href="#" onClick={this.tabOne}>
                            <p>List Of Finding</p>
                          </a>
                        </li>
                        <li
                          className={
                            this.state.tabtwo ? "menu1 active" : ""
                          }
                        >
                          <a href="#" onClick={this.tabTwo}>
                            <p>Attached Files</p>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>

                  {tabone && (
                    <div className="tab-menu-content-section">
                      <div
                        id="content-1"
                        className={tabone ? "d-block" : "d-none"}
                      >
                        <button
                          type="button"
                          onClick={this.findingpop}
                          className="commonBtn2 float-right mb-2"
                        >
                          <i className="fa fa-plus" /> Add New Entry
                         </button>
                        <div className="table-responsive">
                          <table className="table table-hover">
                            <thead className="thead-default">
                              <tr>
                                <th>Title</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {patientFindingListData.length > 0 ? (
                                patientFindingListData.map((each, i) => (
                                  <tr key={i}>
                                    <td data-label="startDate">
                                      {each.title}
                                    </td>

                                    <td data-label="Action">
                                      <a
                                        title="Edit"
                                        className="editBtn"
                                        onClick={e =>
                                          this.modalPopupOpen(each)
                                        }
                                      >
                                        <span
                                          id={5}
                                          className="fa fa-edit"
                                        />
                                      </a>
                                      <a
                                        title="Delete"
                                        className="deleteBtn"
                                        onClick={e =>
                                          this.deleteFinding(each)
                                        }
                                      >
                                        <i
                                          className="fa fa-trash"
                                          data-toggle="modal"
                                          data-target="#myModal2"
                                        />
                                      </a>
                                    </td>
                                  </tr>
                                ))
                              ) : (
                                  <tr>No data available</tr>
                                )}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  )}

                  {tabtwo && (
                    <div
                      id="content-2"
                      className={tabtwo ? "d-block" : "d-none"}
                    >
                      <button
                        type="button"
                        className="commonBtn2 float-right mb-2"
                        onClick={() =>
                          this.setState({
                            uploadPopup: !this.state.uploadPopup
                          })
                        }
                      >
                        <i className="fa fa-plus" /> <span> Upload new files</span>
                      </button>
                      <div className="table-responsive">
                        <table className="table table-hover">
                          <thead className="thead-default">
                            <tr>
                              <th>#</th>
                              <th>Name</th>
                            <th>FileName</th>
                            <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.props.getFilesDetails.fileDetails &&
                              this.props.getFilesDetails.fileDetails.map(
                                (each, i) => (
                                  <tr>
                                    <td>{i + 1}</td>
                                    <td>{each.name}</td>
                                    <td data-label="FileName"> {each.fileName}</td>

                                    <td>
                                      <button
                                        class="download_btn"
                                        onClick={() =>
                                          this.handleAwsFiles(each.fileUrl)
                                        }
                                      >
                                        <i class="fas fa-download" />{" "}
                                        
                                      </button>
                                      <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.props.handleHealthDataDelete(each.id, each.fileUrl, "finding")
                                      }
                                    >
                                      <i class="fas fa-trash" />
                                    </button>
                                    </td>
                                  </tr>
                                )
                              )}
                            {/* <tr>
                              <td data-label="#">1</td>
                              <td data-label="Name">Eye Care Test Report</td>
                              <td data-label="Type">png</td>
                              <td data-label="Size">87.9 kB</td>
                              <td data-label="Date">16.05.2019 11:56</td>
                              <td data-label="Action">
                                <a
                                  title="Edit"
                                  className="editBtn"
                                  data-toggle="modal"
                                  data-target="#myModal"
                                >
                                  <i className="fa fa-edit" />
                                </a>
                                <a
                                  title="Delete"
                                  className="deleteBtn"
                                  onClick={e => this.deletePatient()}
                                >
                                  <i
                                    className="fa fa-trash"
                                    data-toggle="modal"
                                    data-target="#myModal2"
                                  />
                                </a>
                              </td>
                            </tr> */}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  )}
                </div>

                <div
                  className={addfindingpop ? "modal d-block" : "modal"}
                  id="myModal"
                >
                  <div class="modal-dialog">
                    <div class="modal-content modalPopUp">
                      <div class="modal-header borderNone">
                        <h5 class="modal-title">Create New Entry</h5>
                        <button
                          type="button"
                          className="close"
                          data-dismiss="modal"
                          onClick={this.modalPopUpClose}
                        >
                          <i class="fas fa-times" />
                        </button>
                      </div>
                      <div class="modal-body">
                        <form>
                          <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Title for list entry (e.g. Meniscus
                                  damage) <sup>*</sup>
                                </label>
                                <input
                                  className="form-control"
                                  name="title"
                                  onChange={this.handleChange}
                                  value={title}
                                  type="text"
                                />
                                <div style={{ color: "red" }}>
                                  {errors.title}
                                </div>

                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Type of finding
                                </label>
                                <input
                                  className="form-control"
                                  name="find"
                                  onChange={this.handleChange}
                                  value={find}
                                  type="text"
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Comment <sup>*</sup>
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="comment"
                                  onChange={this.handleChange}
                                  type="text"
                                  value={comment}
                                />
                                <div style={{ color: "red" }}>
                                  {errors.comment}
                                </div>
                              </div>
                            </div>

                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <button
                                type="button"
                                class="commonBtn float-right"
                                onClick={this.handleFinding}
                                disabled={!firstValid}
                              >
                                Add
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  className={editfinding ? "modal d-block" : "modal"}
                  id="myModal"
                >
                  <div class="modal-dialog">
                    <div class="modal-content modalPopUp">
                      <div class="modal-header borderNone">
                        <h5 class="modal-title">Edit Accident</h5>
                        <button
                          type="button"
                          className="close"
                          onClick={this.modalPopUpClose}
                        >
                          <i class="fas fa-times" />
                        </button>
                      </div>
                      <div class="modal-body">
                        <form>
                          <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Title for list entry (e.g. Meniscus
                                  damage)
                                </label>
                                <input
                                  className="form-control"
                                  name="title"
                                  onChange={this.handleChange}
                                  defaultValue={
                                    this.state.editDetails.title
                                  }
                                  type="text"
                                  readOnly
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Type of finding
                                </label>
                                <input
                                  className="form-control"
                                  name="find"
                                  onChange={this.handleChange}
                                  defaultValue={this.state.editDetails.find}
                                  type="text"
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Comment
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="comment"
                                  onChange={this.handleChange}
                                  type="text"
                                  defaultValue={
                                    this.state.editDetails.comment
                                  }
                                />
                                <span />
                              </div>
                            </div>

                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <button
                                type="button"
                                class="commonBtn float-right"
                                onClick={this.updateFinding}
                              >
                                Update
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            className={this.state.uploadPopup ? "modal d-block" : "modal"}
            id="myModal"
          >
            <div class="modal-dialog">
              <div class="modal-content modalPopUp">
                <div class="modal-header borderNone">
                  <h5 class="modal-title">Upload File</h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    onClick={() =>
                      this.setState({
                        uploadPopup: !this.state.uploadPopup
                      })
                    }
                  >
                    <i class="fas fa-times" />
                  </button>
                </div>
                <div class="modal-body">
                  <form>
                    <div class="row">
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="form-group">
                          <label className="form-label">File name</label>
                          <input
                            className="form-control"
                            type="text"
                            name="name"
                            onChange={this.handleChange}
                          />
                          <span />
                        </div>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="fileUp">
                          <label
                            for="fileUp1"
                            class="commonBtn text-center upload"
                          >
                            Upload File
                          </label>
                          <input
                            type="file"
                            id="fileUp1"
                            style={{ display: "none" }}
                            onChange={this.handleChangePopup}
                          />
                          {/* <p style={{ textAlign: "center" }}>hsfkjhsfj</p> */}
                        </div>
                      </div>

                      <div class="col-md-12 col-lg-12 col-sm-12 mt-2">
                        <button
                          type="button"
                          class="commonBtn float-right"
                          onClick={this.popupSubmit} disabled={!this.state.fileURL}
                        >
                          Add
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  addfindinglist: state.patientReducer.addfindinglist,
  getfindinglist: state.patientReducer.getfindinglist,
  updatefindinglist: state.patientReducer.updatefindinglist,
  deletefindinglist: state.patientReducer.deletefindinglist,
  loginDetails: state.loginReducer.loginDetails,
  getFilesDetails: state.uploadReducer.getFilesDetails
});

const mapDispatchToProps = dispatch => ({
  createFindingData: createfinding =>
    dispatch(createFindingData(createfinding)),
  listFindingData: listfinding => dispatch(listFindingData(listfinding)),
  updateFindingData: updatefinding =>
    dispatch(updateFindingData(updatefinding)),
  deleteFindingData: deletefinding =>
    dispatch(deleteFindingData(deletefinding)),
  getUploadFiles: payload => dispatch(getUploadFiles(payload)),
  handleHealthDataDelete: (id, fileUrl, type) => dispatch(handleHealthDataDelete(id, fileUrl, type))

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Finding);
