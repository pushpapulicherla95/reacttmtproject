import React, { Component } from 'react';

class HealthRecordMenu extends Component {
    render() {
        return (
            <div className="mainMenuSide" id="main-content">
                <div className="wrapper">
                    <div className="container-fluid">
                        <div className="body-content">
                            <div className="row">
                                <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig"> 
                                    <a href="javaScript:void(0);" onClick={() => this.props.history.push({pathname:'/medicine',state:{submenuType:"HealthRecord"}})} className="card bg-c submenuIcon">
                                        <div className="card-img1 submenuIconImg ">
                                            <img src="images/personal-color.png" />
                                        </div>
                                        <div className="card-title">
                                            <h5>Medicines</h5>
                                        </div>
                                    </a>
                                </div>
                                <div className="col-lg-3 col-md-3 col-sm-6 col-6 padLeft">
                                <a href="javaScript:void(0);" onClick={() => this.props.history.push({pathname:'/diseases/newdiseases'})} className="card bg-c submenuIcon">
                                        <div className="card-img1 submenuIconImg">
                                            <img src="images/curriculum-color.png" />
                                        </div>
                                        <div className="card-title">
                                            <h5>Diseases</h5>
                                        </div>
                                    </a>
                                </div>

                                {/* <div className="col-lg-3 col-md-3 col-sm-6 col-6 padLeft">
                                    <a href="javaScript:void(0);" onClick={() => this.props.history.push({pathname:'/curriculam/newcurriculam'})} className="card bg-c submenuIcon">
                                        <div className="card-img1 submenuIconImg">
                                            <img src="images/curriculum-color.png" />
                                        </div>
                                        <div className="card-title">
                                            <h5>Curriculum</h5>
                                        </div>
                                    </a>
                                </div> */}

                                {/* <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                                    <a href="javascript:void(0)"  onClick={() => this.props.history.push({pathname:'/symptoms/symptoms'})}className="card bg-c submenuIcon">
                                        <div className="card-img1 submenuIconImg">
                                            <img src="images/doctors-color.png" />
                                        </div>
                                        <div className="card-title">
                                            <h5>Symptoms</h5>
                                        </div>
                                    </a>
                                </div> */}
                                <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                                    <a href="javascript:void(0)"  onClick={() => this.props.history.push({pathname:'/symptoms/assesment'})}className="card bg-c submenuIcon">
                                        <div className="card-img1 submenuIconImg">
                                            <img src="images/doctors-color.png" />
                                        </div>
                                        <div className="card-title">
                                            <h5>Assessment</h5>
                                        </div>
                                    </a>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}
export default HealthRecordMenu;

