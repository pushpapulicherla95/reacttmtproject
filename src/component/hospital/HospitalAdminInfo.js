import React, { Component } from "react";
import { connect } from "react-redux";
import { getCountry, getState, getCity, getArea, getPostalcode } from "../../service/doctor/action";
import { getAdminInfo, updateAdminInfo, uploadProfilepic } from "../../service/hospital/action";
import AWS from "aws-sdk";

class HospitalAdminInfo extends Component {
    constructor(props) {
        super(props);
        var loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
        const { userInfo } = loginDetails;
        var userId = userInfo.userId || "";
        this.state = {
            firstName: userInfo.firstName || "",
            lastName: userInfo.lastName || "",
            gender: "",
            email: "",
            mobileNo: "",
            selectedAreaOption: "",
            citys: "",
            area: "",
            country: "",
            state: "",
            pincode: "",
            states: "",
            areas: "",
            stateList: [],
            selectedState: "",
            lists: "",
            city: "",
            selectedStateOption: "",
            selectedCityOption: "",
            selectedOption: "",
            userId: userId,
            hospitalName: "",
            address: "",
            fileType: "",
            fileUrl: "",
            profilePic: userInfo.profilePic,

        }
    }
    componentWillMount() {
        const userdetails = {
            "user_id": this.state.userId
        }
        const info = {
            "uid": this.state.userId
        }
        this.props.getAdminInfo(info)
        // this.props.getCountry();
        // this.props.removeState();
        // this.props.removeCity();
        const { profilePic } = this.state;
        AWS.config.update({
            accessKeyId: process.env.REACT_APP_ACCESSKEY,
            secretAccessKey: process.env.REACT_APP_SECRETACCESSKEY,
            region: process.env.REACT_APP_REGION
        });
        var s3 = new AWS.S3();
        var data = {
            Bucket: "tomato-medical-bucket",
            Key: `profilepicture/${profilePic}`
        };
        s3.getSignedUrl("getObject", data, async (err, data) => {
            if (err) {
                console.log("Error uploading data: ", err);
            } else {
                console.log("succesfully uploaded the image!", data);

                (await data) && this.setState({ fileUrl: data });
            }
        });

    }
    handleOption = (e) => {
        const { selectedOption } = this.state
        this.setState({ selectedOption: e.target.value })
        this.setState({ selectedOption: selectedOption })
    }
    handleChange = (e) => {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value });
    };
    selectCountry = (e) => {
        this.setState({ selectedOption: e.target.value }, () => {
            let payload = {
                country: this.state.selectedOption
            }
            this.props.getState(payload)
        })


    }
    selectState = (e) => {
        this.setState({ selectedStateOption: e.target.value }, () => {
            let payload = {
                state: this.state.selectedStateOption
            }
            this.props.getCity(payload)
        })


    }
    selectCity = (e) => {
        this.setState({ selectedCityOption: e.target.value }, () => {
            let payload = {
                city: this.state.selectedCityOption
            }
            this.props.getArea(payload)
        })


    }
    selectArea = (e) => {
        this.setState({ selectedAreaOption: e.target.value }, () => {
            let payload = {
                area: this.state.selectedAreaOption
            }
            this.props.getPostalcode(payload)
        })


    }


    componentWillReceiveProps(nextProps) {

        if (nextProps.hospitalAdminInfo && (nextProps.hospitalAdminInfo != this.props.hospitalAdminInfo) && nextProps.hospitalAdminInfo != null) {
            const {
                email
                , mobileNo, area, pinCode, country, city, state, id, hospitalName, address, gender
            } = nextProps.hospitalAdminInfo;

            this.setState({
                email
                , mobileNo, hospitalId: id, selectedAreaOption: area, pincode: pinCode, hospitalName, selectedOption: country, selectedCityOption: city, selectedStateOption: state, address, gender
            }, () => {

                this.props.getArea({ city })
                this.props.getState({ country })
                this.props.getCity({ state })
                this.props.getPostalcode({ area })
            },
            )
        }
    }
    handleSubmit = (e) => {

        const {
            mobileNo,
            hospitalName,
            email,
            address,
            gender,
            firstName,
            lastName,
            selectedStateOption,
            selectedCityOption,
            pincode, selectedOption, selectedAreaOption, hospitalId
        } = this.state;
        const saveInfo = {
            "uid": this.state.userId,
            hospitalId: hospitalId,
            email: email,
            address: address,
            gender: gender,
            firstName: firstName,
            lastName: lastName,
            city: selectedCityOption,
            area: selectedAreaOption,
            country: selectedOption,
            pincode: pincode,
            state: selectedStateOption,
            mobileNum: mobileNo,
            hospitalName: hospitalName
        }
        this.props.updateAdminInfo(saveInfo);
    }
    handleChangePopup = e => {
        var reader = new FileReader();
        const name = e.target.name;

        var value = e.target.files[0];

        reader.onload = () => {
            this.setState({
                fileUrl: reader.result,
                fileName: value.name,
                fileType: value.type
            });
        };
        reader.readAsDataURL(value);
    }
    submitUpload = (e) => {
        e.preventDefault();
        const { fileType, fileUrl, userId } = this.state;
        var phrase = this.state.fileUrl;
        var myRegexp = /base64,(.*)/;
        var match = myRegexp.exec(phrase);
        const filedata = {
            uid: userId,
            fileType: fileType,
            fileUrl: match[1],
        }
        this.props.uploadProfilepic(filedata);
    }
    render() {
        const lists = this.props.listcountryDetails && this.props.listcountryDetails.lists;
        const city = this.props.listcityDetails && this.props.listcityDetails.city;
        const states = this.props.liststateDetails && this.props.liststateDetails.states;
        const area = this.props.listareaDetails && this.props.listareaDetails.area;
        const postalcode = this.props.listpostalcodeDetails && this.props.listpostalcodeDetails.postalcode;
        const { firstName, lastName, email, mobileNo, pincode, selectedOption, selectedAreaOption, selectedStateOption,
            selectedCityOption, hospitalName, address, fileUrl } = this.state;
        return (
            <div className="mainMenuSide" id="main-content"><div className="wrapper"><div className="container-fluid"><div className="body-content"><div className="row">
                <div className="innerCard">
                    <div className="innerCardHead">
                        <h5>Basic Information</h5>
                    </div>
                    <div className="innerCardBody">
                        <div className="row">
                            <div className="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12">
                                <div className="imagePreview">
                                    <label className="uploadBtn"><svg className="svg-inline--fa fa-upload fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="upload" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M296 384h-80c-13.3 0-24-10.7-24-24V192h-87.7c-17.8 0-26.7-21.5-14.1-34.1L242.3 5.7c7.5-7.5 19.8-7.5 27.3 0l152.2 152.2c12.6 12.6 3.7 34.1-14.1 34.1H320v168c0 13.3-10.7 24-24 24zm216-8v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h136v8c0 30.9 25.1 56 56 56h80c30.9 0 56-25.1 56-56v-8h136c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path></svg>
                                        <input type="file" className="input_h_35 boxnone" name="fileUrl" onChange={this.handleChangePopup} /></label>
                                    <img src={fileUrl ? fileUrl : "../images/profile.png"} alt="your image" />
                                </div>
                                <button type="button" className="commonBtn text-center upload" onClick={this.submitUpload}>Upload Image</button>
                            </div>
                            <div className="col-xl-9 col-lg-8 col-md-8 col-sm-12 col-12">
                                <div className="row">
                                    <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                                        <div className="form-group">
                                            <label>First Name <sup>*</sup></label>
                                            <input type="text" className="form-control" name="firstName" defaultValue={firstName} readOnly="readonly" />
                                            {/* <div style={{ color: "red" }}>{errors.firstName}</div> */}
                                        </div>
                                    </div>
                                    <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                                        <div className="form-group">
                                            <label>Last Name <sup>*</sup></label>
                                            <input type="text" className="form-control" name="lastName" defaultValue={lastName} readOnly="readonly" />
                                            {/* <div style={{ color: "red" }}>{errors.lastName}</div> */}
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <div className="form-group">
                                            <label>
                                                Gender<sup>*</sup>
                                            </label>
                                            <select
                                                className="form-control"
                                                name="gender"
                                                value={this.state.gender}
                                                onChange={this.handleChange}
                                            >
                                                <option value="">--Select Gender--</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                            {/* <div style={{ color: "red" }}>{errors.gender}</div> */}
                                        </div>
                                    </div>
                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <div className="form-group">
                                            <label>Email Id <sup>*</sup></label>
                                            <input type="text" className="form-control" name="email" defaultValue={email} readOnly="readonly" />
                                            {/* <div style={{ color: "red" }}>{errors.email}</div> */}
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <div className="form-group">
                                            <label>Mobile No <sup>*</sup></label>
                                            <input type="text" className="form-control" name="mobileNo" value={mobileNo} onChange={this.handleChange} />
                                            {/* <div style={{ color: "red" }}>{errors.mobileNo}</div> */}
                                        </div>
                                    </div>
                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <div className="form-group">
                                            <label className="form-label">Country<sup>*</sup></label>
                                            <select className="form-control" name="country" value={this.state.selectedOption} onChange={this.selectCountry}>
                                                <option value="" disabled>---Select Country ----</option>
                                                {
                                                    lists && lists.map((each, i) => (

                                                        <option key={i} value={each.code}>  {each.country}</option>))
                                                }
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <div className="form-group">
                                            <label className="form-label">State<sup>*</sup></label>
                                            <select className="form-control" name="state" value={this.state.selectedStateOption} onChange={this.selectState}>

                                                <option value="" disabled>---Select State ----</option>
                                                {
                                                    states && states.map((each, i) => (
                                                        <option value={each} key={i}> {each}</option>))
                                                }
                                            </select>
                                        </div>


                                    </div>
                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">

                                        <div className="form-group">
                                            <label className="form-label">City<sup>*</sup></label>
                                            <select className="form-control" name="city" value={this.state.selectedCityOption} onChange={this.selectCity}>
                                                <option value="" disabled>---Select City ----</option>
                                                {
                                                    city && city.map((each, i) => (

                                                        <option key={i} value={each}>  {each}</option>))
                                                }
                                            </select>
                                        </div>

                                    </div>
                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <div className="form-group">
                                            <label className="form-label">Area<sup>*</sup></label>
                                            <select className="form-control" name="areas" value={this.state.selectedAreaOption} onChange={this.selectArea}>
                                                <option value="" disabled>---Select Area ----</option>
                                                {
                                                    area && area.map((each, i) => (

                                                        <option key={i} value={each}> {each}</option>))
                                                }
                                            </select>
                                        </div>

                                    </div>
                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">

                                        <div className="form-group">
                                            <label className="form-label">Pincode<sup>*</sup></label>
                                            <select className="form-control" name="pincode" value={pincode} onChange={this.handleChange}>
                                                <option value="" disabled>---Select Pincode ----</option>
                                                {
                                                    postalcode && postalcode.map((each, i) => (

                                                        <option key={i} value={each}> {each}</option>))
                                                }
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">

                                        <div className="form-group">
                                            <label className="form-label">Hospital Name<sup>*</sup></label>
                                            <input type="textarea" className="form-control" name="hospitalName" value={hospitalName} onChange={this.handleChange} />
                                        </div>
                                    </div>
                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">

                                        <div className="form-group">
                                            <label className="form-label">Address<sup>*</sup></label>
                                            <textarea className="form-control" name="address" value={address} onChange={this.handleChange} />
                                        </div>
                                    </div>
                                </div>
                                <button type="button" className="commonBtn float-right" onClick={this.handleSubmit}  >Save</button>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            </div></div></div></div>

        )
    }
}
const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails,
    listcountryDetails: state.doctorReducer.listcountryDetails,
    liststateDetails: state.doctorReducer.liststateDetails,
    listcityDetails: state.doctorReducer.listcityDetails,
    listareaDetails: state.doctorReducer.listareaDetails,
    listpostalcodeDetails: state.doctorReducer.listpostalcodeDetails,
    hospitalAdminInfo: state.hospitalReducer.hospitalAdminInfo
})

const mapDispatchToprops = dispatch => ({
    getCountry: () => dispatch(getCountry()),
    getState: (state) => dispatch(getState(state)),
    getArea: (area) => dispatch(getArea(area)),
    getCity: (city) => dispatch(getCity(city)),
    removeState: () => dispatch({
        type: "GET_STATE_SUCCESS",
        payload: { liststateDetails: { states: [] } }
    }),
    getPostalcode: (postalcode) => dispatch(getPostalcode(postalcode)),
    getAdminInfo: (info) => dispatch(getAdminInfo(info)),
    updateAdminInfo: (saveInfo) => dispatch(updateAdminInfo(saveInfo)),
    uploadProfilepic: (filedata) => dispatch(uploadProfilepic(filedata))

})

export default connect(
    mapStateToProps,
    mapDispatchToprops
)(HospitalAdminInfo);
