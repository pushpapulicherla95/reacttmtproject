import {combineReducers} from "redux";
import { persistStore, persistReducer } from 'redux-persist'
import createEncryptor from 'redux-persist-transform-encrypt'
import storage from 'redux-persist/lib/storage/session'
import loginReducer from '../service/login/reducer';
import dashboardReducer  from '../service/dashboard/reducer';
import doctorReducer from '../service/doctor/reducer';
import patientReducer  from '../service/patient/reducer';
import medicalSpecialistReducer from "../service/medicalSpecialist/reducer";

import pharmacyReducer from '../service/pharmacy/reducer';
import jitsiReducer from '../service/jitsi/reducer';
import appointmentReducer from '../service/appointment/reducer';
import commonReducer from '../service/common/reducer';
import {reducer as toastrReducer} from 'react-redux-toastr';
import categoryReducer from '../service/productcategory/reducer';
import healthrecordReducer from "../service/healthrecord/reducer";
import insuranceReducer from "../service/insurance/reducer";
import uploadReducer from '../service/upload/reducer';
import adminPatientReducer from "../service/admin/Patient/reducer";
import textRoboReducer from "../service/admin/TextRobo/reducer";
import hospitalReducer from "../service/hospital/reducer";

const encryptor = createEncryptor({
  secretKey: 'tomatato-medical',
  onError: function (error) {
    // Handle the error.
  }
})
const persistConfig = {
  key: "root",
  //transforms: [encryptor],
  storage
}

const rootReducer = combineReducers({
  loginReducer,
  dashboardReducer,
  doctorReducer,
  patientReducer,
  pharmacyReducer,
  jitsiReducer,
  appointmentReducer,
  toastr: toastrReducer,
  commonReducer,
  categoryReducer,
  healthrecordReducer,
  insuranceReducer,
  medicalSpecialistReducer,
  uploadReducer,
  adminPatientReducer,
  textRoboReducer,
  hospitalReducer
});

const persistedReducer = persistReducer(persistConfig , rootReducer)

export default persistedReducer;