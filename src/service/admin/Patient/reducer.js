import actionType from "../Patient/actionType";

const initialState = {
    adminPatientList: "",
    adminDoctorList: "",
    viewPatientDetails: "",
    adminProfileDetails: "",
    hospitalList:"",
    logdataDetails:"",
    adminprofilepicture:"",
}
const adminPatientReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.GET_PATIENT_LIST_SUCCESS:
            return {
                ...state,
                adminPatientList: action.payload
            }
        case actionType.GET_PATIENT_LIST_FAILURE:
            return {
                ...state,
                adminPatientList: action.error
            }
        case actionType.GET_DOCTOR_LIST_SUCCESS:
            return {
                ...state,
                adminDoctorList: action.payload
            }
        case actionType.GET_DOCTOR_LIST_FAILURE:
            return {
                ...state,
                adminDoctorList: action.error
            }
        case actionType.APPROVE_DOCTOR_SUCCESS:
            return {
                ...state
            }
        case actionType.APPROVE_DOCTOR_FAILURE:
            return {
                ...state
            }
        case actionType.VIEW_PATIENT_SUCCESS:
            return {
                ...state,
                viewPatientDetails: action.payload
            }
        case actionType.VIEW_PATIENT_FAILURE:
            return {
                ...state,
                viewPatientDetails: action.error
            }
        case actionType.GET_ADMIN_PROFILE_SUCCESS:
            return {
                ...state,
                adminProfileDetails: action.payload
            }
        case actionType.GET_ADMIN_PROFILE_FAILURE:
            return {
                ...state,
                adminProfileDetails: action.error
            }
        case actionType.UPDATE_ADMIN_PROFILE_SUCCESS:
            return {
                ...state
            }
        case actionType.UPDATE_ADMIN_PROFILE_FAILURE:
            return {
                ...state
            }
        case actionType.GET_HOSPITAL_LIST_SUCCESS:
         return {
             ...state,
             hospitalList:action.payload
         }
         case actionType.GET_HOSPITAL_LIST_FAILURE:
         return{
            ...state,
            hospitalList:action.error 
         }
         case actionType.GET_LOGDATA_SUCCESS:
            return {
                ...state,
                logdataDetails: action.payload
            }
        case actionType.GET_LOGDATA_FAILURE:
            return {
                ...state,
                logdataDetails: action.error
            }
            case actionType.UPLOAD_ADMIN__PROFILE_PIC_SUCCESS:
                return {
                    ...state,
                    adminprofilepicture: action.payload
                }
            case actionType.UPLOAD_ADMIN__PROFILE_PIC_FAILURE:
                return {
                    ...state,
                    adminprofilepicture: action.error
                }
            //adminprofilepicture
        default: return state;
    }
}
export default adminPatientReducer;