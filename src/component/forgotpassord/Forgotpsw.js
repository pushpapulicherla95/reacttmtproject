import React, { Component } from "react";
import { connect } from "react-redux";
import { forgot } from "../../service/login/action";
class Forgotpsw extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      errors: {
        email: "",

      },
      emailValid: false,

      formValid: false,
      submitted: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  validateField(fieldName, value) {
    const { errors, emailValid } = this.state;
    let emailvalid = emailValid;

    let fieldValidationErrors = errors;
    switch (fieldName) {
      case "email":
        emailvalid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        fieldValidationErrors.email = emailvalid
          ? ""
          : "Please enter valid email address.";
        break;

      default:
        break;
    }
    this.setState({
      errors: fieldValidationErrors,
      emailValid: emailvalid,

    },
      this.validateForm
    );
  }
  validateForm() {
    const {
      emailValid
    } = this.state;
    this.setState({
      formValid:
        emailValid




    });

  }
  componentWillReceiveProps(nextprops)
  {
    console.log("status", this.props.forgotpswDetails);
  const{status}=  this.props.forgotpswDetails;
    if(status === "success")
    {
      this.props.history.push("/login/newPassword");
    }

  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    let forgotinfo = {
      email: this.state.email
    };

    this.props.forgot(forgotinfo);

  }

  render() {
    const { email, errors, formValid } = this.state;

    return (
      // <div className="cui-login">
      //   <div className="cui-login-header">
      //     <div className="row">
      //       <div className="col-lg-6">
      //         <div className="cui-login-header-logo">
      //           <a href="javascript:void(0);">
      //             <img src="images/logo.jpg" alt="tomato Logo" />
      //           </a>
      //         </div>
      //       </div>
      //       <div className="col-lg-6" />
      //     </div>
      //   </div>
      //   <div className="cui-login-block">
      //     <div className="row">
      //       <div className="col-xl-12">
      //         <div className="cui-login-block-inner">
      //           <div className="cui-login-block-form">
      //             <h4 className="text-uppercase text-center">
      //               <strong>Forgot password</strong>
      //             </h4>
      //             <br />
      //             <form id="form-validation" name="form-validation">
      //               <div className="form-group">
      //                 <label className="form-label">Email</label>
      //                 <input
      //                   id="validation-email"
      //                   className="form-control"
      //                   placeholder="Enter your Email"
      //                   name="email"
      //                   type="text"
      //                   value={email}
      //                   onChange={this.handleChange}
      //                 />
      //                 <span />
      //               </div>
      //               <div className="form-actions">
      //                 <button
      //                   type="submit"
      //                   className="btn commonBtn mr-3"
      //                   onClick={this.handleSubmit}
      //                 >
      //                   SUBMIT
      //                 </button>
      //               </div>
      //             </form>
      //           </div>
      //         </div>
      //       </div>
      //     </div>
      //   </div>
      //   <div className="cui-login-footer text-center" />
      // </div>
      <section className="loginSection">
        <a href="/" className="logoDiv"><img src="../images/logo.jpg" /></a>
        <div className="loginInner">
          <div className="loginFormContent hideSidebar">
            <h4 className="popupTitle">Forgot Password</h4>
            <form onSubmit={this.handleSubmit}>
              <div className="form-group emailIcon">
                <input

                  id="validation-email"
                  className="form-control commonInput"
                  placeholder="Enter your Email"
                  name="email"
                  type="text"
                  value={email}
                  onChange={this.handleChange}
                />
                <div style={{ color: "red" }}>{errors.email}</div>
              </div>


              <div className="form-group">
                <a href="#">
                  <button
                    type="submit"
                    className="btn loginBtn"
                    onClick={this.handleSubmit} disabled={!formValid}
                  >
                    Submit
                    </button>
                </a>
              </div>

            </form>
          </div>


        </div>
      </section>
    );
  }
}
const mapStateToProps = state => ({
  forgotpswDetails: state.loginReducer.forgotpswDetails
});

const mapDispatchToProps = dispatch => ({
  forgot: forgotinfo => dispatch(forgot(forgotinfo))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Forgotpsw);
