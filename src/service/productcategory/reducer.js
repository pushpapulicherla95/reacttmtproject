import actionType from '../productcategory/actionType';
const initialState = {
    
    createproductcategoryDetails:"",
    listproductcategoryDetails:"",
    updateproductcategoryDetails:"",
    deleteproductcategoryDetails:"",

    
    }
    const categoryReducer = (state = initialState, action) => {
    
        switch (action.type) {
            case actionType.CREATE_PRODUCTCATEGORY_SUCCESS:
            return {
                ...state,
                createproductcategoryDetails:action.payload,
            
            }
        case actionType.CREATE_PRODUCTCATEGORY_FAILURE:
            return {
                ...state,
                createproductcategoryDetails:action.payload,

            }
            case actionType.LIST_PRODUCTCATEGORY_SUCCESS:
            return {
                ...state,
                listproductcategoryDetails:action.payload,
            
            }
        case actionType.LIST_PRODUCTCATEGORY_FAILURE:
            return {
                ...state,
                listproductcategoryDetails:action.payload,

            }
            case actionType.UPDATE_PRODUCTCATEGORY_SUCCESS:
            return {
                ...state,
                updateproductcategoryDetails:action.payload,
            
            }
        case actionType.UPDATE_PRODUCTCATEGORY_FAILURE:
            return {
                ...state,
                updateproductcategoryDetails:action.payload,

            }
            case actionType.DELETE_PRODUCTCATEGORY_SUCCESS:
            return {
                ...state,
                deleteproductcategoryDetails:action.payload,
            
            }
        case actionType.DELETE_PRODUCTCATEGORY_FAILURE:
            return {
                ...state,
                deleteproductcategoryDetails:action.payload,

            }
        
        default:
            return state;
    }
}
export default categoryReducer;