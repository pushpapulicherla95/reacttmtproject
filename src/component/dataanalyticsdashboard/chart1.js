import React from "react";
import Chart from "chart.js";
import axios from "axios";
import _ from 'lodash'
class Chart1 extends React.Component {
  constructor() {
    super();
    this.state = {
      chartList: []
    };
  }

  componentDidMount = () => {
    this.chartData()
  };

  chartData = () => {
    const payload = {
      compare_by: [
        {
          value: {
            column_name: "personal_gender",
            row_values: "M"
          }
        },
        {
          value: {
            column_name: "personal_gender"
          }
        }
      ],
      compare_with: ["allergies_name", "medication_name"]
    };
    axios
      .post("http://192.168.2.30:8000/tomatomedical/API/columns", payload)
      .then(response => {
        console.log("response.data", response.data);
        this.setState({chartList:[].concat.apply([],response.data)},()=>{
            this.renderChart();
        });
      });
  };

  renderChart = () => {
 
  
  this.state.chartList.map((each, i) => {
  console.log("each",each);
    
    let data1 = [];
    let data2 = [];
    let data3 = [];
 
        each.field.map((val)=>{
            data1.push(val.fieldname);
            data2.push(val.fieldcount);
            data3.push(val.count);
        })  
      
          console.log(data2, data1, data3);

                             var canvas = document.getElementById("barchart"+i);
                               var ctx = canvas.getContext("2d");

                               // Global Options:
                               Chart.defaults.global.defaultFontColor = "dodgerblue";
                               Chart.defaults.global.defaultFontSize = 16;

                               // Data with datasets options
                               let label=data3
                               var data = {
                                 labels: data1,
                                 datasets: [
                                   {
                                     label: each.tablename,
                                     fill: true,
                                     //  backgroundColor: [
                                     //    "moccasin",
                                     //    "saddlebrown",
                                     //    "lightpink"
                                     //  ],
                                     data: data2
                                   }
                                 ]
                               };

                               console.log("label", data);
                               var array = [];
                               var options = {
                                 tooltips: {
                                   callbacks: {
                                     label: tooltipItem => {
                                       console.log(
                                         "fsdf==",
                                         label[tooltipItem.index]
                                       );
                                       return (
                                         "$" +
                                         Number(tooltipItem.yLabel) +
                                         Object.keys(label[tooltipItem.index]) +
                                         ":" +
                                         Object.values( label[tooltipItem.index])
                                       );
                                     }
                                   }
                                 },
                                 responsive: false,
                                 scales: {
                                   yAxes: [
                                     {
                                       stacked: true,
                                       gridLines: {
                                         display: true,
                                         color: "rgba(255,99,132,0.2)"
                                       }
                                     }
                                   ],
                                   xAxes: [
                                     {
                                       gridLines: {
                                         display: false
                                       }
                                     }
                                   ]
                                 }
                               };

                               // Chart declaration:
                               var myBarChart = new Chart(ctx, {
                                 type: "bar",
                                 data: data,
                                 options: options
                               });
                           })

  };

  render() {
    return (
      <div class="col-sm-4">
          {this.state.chartList&&this.state.chartList.map((each,i)=>
          {
            console.log("fs",each.length)
         return (<canvas
          id={"barchart" + i}
          style={{
            height: "100%",
            width: "100%",
            border: "1px dotted red "
          }}
          ></canvas>)})}
      </div>
    );
  }

}
export default Chart1;





// componentDidMount() {
//   var canvas = document.getElementById("barChart");
//   var ctx = canvas.getContext("2d");

//   // Global Options:
//   Chart.defaults.global.defaultFontColor = "dodgerblue";
//   Chart.defaults.global.defaultFontSize = 16;

//   // Data with datasets options
//   let label=["female","male","malegg","femalengj"]
//   var data = {
//     labels: ["Vanilla", "Chocolate", "Strawberry", "hfghjf"],
//     datasets: [
//       {
//         label: "Ice Cream Prices ",
//         fill: true,
//         backgroundColor: ["moccasin", "saddlebrown", "lightpink"],
//         data: [11, 9, 4, 5]
//       }
//     ]
//   };
//   var array = [];
//   var options = {
//     tooltips: {
//       callbacks: {
//         label: (tooltipItem)=> {
//           console.log("fsdf==", tooltipItem);
//           return "$" + Number(tooltipItem.yLabel) + label[tooltipItem.index];
//         }
//       }
//     },
//     title: {
//       display: true,
//       text: "Ice Cream Truck",
//       position: "bottom"
//     },
//     scales: {
//       yAxes: [
//         {
//           ticks: {
//             beginAtZero: true
//           }
//         }
//       ]
//     }
//   };

//   // Chart declaration:
//   var myBarChart = new Chart(ctx, {
//     type: "bar",
//     data: data,
//     options: options
//   });

// }

// render() {
//   return (
//      <div class="container">
//   <br />
//   <div class="row">
//     <div class="col-md-1"></div>
//     <div class="col-md-10">
//       <canvas id="barChart"></canvas>
//     </div>
//     <div class="col-md-1"></div>
//   </div>
// </div>
//   );
// }