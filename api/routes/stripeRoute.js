const express = require('express');
const stripeRouter = express.Router();
const {
    getSavedCardList,
    createCard, updateSavedCard,
    deleteSavedCard,
    createplans,
    createPayment
} = require('../controller/payment/stripe.api');
const {
  listallPlans,
  deletePlans,
  updatePlans,
  retrivePlans,
  createPlans
} = require("../controller/payment/stripe.plans");
const {
 cardPayment,
  bankPayment
} = require("../controller/payment/stripe.payment");
const {
  createCustomer,
  createCharge,
  refundCharge
} = require("../controller/payment/stripe.charge");


//user saved cards
stripeRouter.route('/createCard')
    .post(createCard)
stripeRouter.route('/getSavedCardList')
    .get(getSavedCardList)
stripeRouter.route('/updateSavedCard')
    .post(updateSavedCard)
stripeRouter.route('/deleteSavedCard')
    .post(deleteSavedCard);
// stripeRouter.route("/createPayment")
//     .post(createPayment);


 //user plans   
stripeRouter.route("/createPlans")
    .post(createPlans);
stripeRouter.route("/retrivePlans/:id")
    .get(retrivePlans);
stripeRouter.route("/updatePlans/:id")
    .post(updatePlans);
stripeRouter.route("/listallPlans")
    .get(listallPlans);
stripeRouter.route("/deletePlans/:id")
    .delete(deletePlans);


//payment API   
stripeRouter.route("/cardPayment")
     .post(cardPayment);
stripeRouter.route("/bankPayment")
     .post(bankPayment);


// charge and creating customer
stripeRouter.route("/createCustomer")
     .post(createCustomer);
stripeRouter.route("/createCharge")
     .post(createCharge);
stripeRouter.route("/refundCharge")
     .post(refundCharge);

module.exports = stripeRouter;