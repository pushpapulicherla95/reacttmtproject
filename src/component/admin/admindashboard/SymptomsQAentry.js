
import React, { Component } from "react";
import axios from "axios";
import _ from 'lodash';
import URL from "../../../asset/configUrl"
import { toastr } from "react-redux-toastr";

class SymptomsQAentry extends Component {
    constructor(props) {
        super(props)
        this.state = {
            allSymptomApi: [],
            masterList: [],
            groupObjectMaster:[],
            openPopup: false,
            openPopupSummary:false,
            singleOptionData:"",
            lastIdValue : "",
            listAllPreviousSymptoms : [],
            selectedMainId:"",
            openPopupAddSymptom: false,
            symptomNameFull:"",
            valueInputData: {

            }
        }
        this.toasterOptions = {
            timeOut: 2000,
            newestOnTop: true,
            position: 'top-right',
            transitionIn: 'bounceIn',
            transitionOut: 'bounceOut',
            progressBar: false,
            closeOnToastrClick: false,
          }
        this.timeout = 0;
    }

    componentWillMount() {
        this.getAllSymptoms()
        this.setState({lastIdValue:localStorage.getItem("lastId")})
    }

    componentDidMount(){
      
    }

    getAllSymptoms = () => {
        this.state.selectedMainId && axios.get(URL.GET_ALL_OPTION+"/"+this.state.selectedMainId).then((response) => {
            if (response.data.listCategory.length == 0) {
                const payload = {
                    "categoryId": this.state.selectedMainId,
                    "categoryName": this.state.selectedMainSymptomName,
                    "question": "",
                    "options":[],
                    "parentCategoryId": 0,
                    "mainCid": +this.state.selectedMainId,
                    "status": "",
                    "causes": "",
                    "summary":""
                }
        
                const configs = {
                    method: 'post',
                    url: URL.UPDATE_OPTION,
                    data: payload,
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                }
                axios(configs).then((response) => {
                    this.getAllSymptoms()
                    this.reloadMainSymptomNameList()
                })
            }
            else {
               this.splictCategoryJson(response.data.listCategory)
            }
        })
        this.reloadMainSymptomNameList()
    }
    reloadMainSymptomNameList = () => {
        axios.get(URL.LIST_ALL_ADDED_SYMPTOM_TREE).then((response) => {
            const prevSymptoms = response.data.symptoms || [];
            const prevSymptomsWithKeys = prevSymptoms.map((val, index) => {
                return {
                    keyLetter: val.symptomsName.charAt(0).toLowerCase(),
                    ...val
                }
            })
            this.setState({ listAllPreviousSymptoms: prevSymptomsWithKeys })
        })
    }
    onUpdateOption = (singleOption, mainCid , cause ) => {

        const keyCate = singleOption.category_id + "" + singleOption.parent_category_id + "cate"
        const keyQues = singleOption.category_id + "" + singleOption.parent_category_id + "ques"
        const keyOption = singleOption.category_id + "" + singleOption.parent_category_id + "opti"

        let parsedJson = JSON.parse(singleOption.options)

        const payload = {
            "categoryId": singleOption.category_id,
            "categoryName": this.state.valueInputData[keyCate],
            "question": this.state.valueInputData[keyQues],
            "options": this.state.valueInputData[keyOption],
            "parentCategoryId": singleOption.parent_category_id,
            "mainCid": mainCid,
            "status": singleOption.status,
            "causes": cause || "",
            "summary":cause || ""
        }

        const configs = {
            method: 'post',
            url: URL.UPDATE_OPTION,
            data: payload,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        }

        axios(configs).then((response) => {
        
        })

    }

    initialAddOption=()=>{
        const payload = {
            "categoryName": this.state.symptomNameFull,
            "question": "",
            "options": [],
            "parentCategoryId": 0,
            "mainCid": 0,
            "status":"end",
            "causes":"",
            "summary":""
        }

        const configs = {
            method: 'post',
            url: URL.ADD_OPTION,
            data: payload,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        }

        axios(configs).then((response) => {
            this.getAllSymptoms()
        })
    }

    nonInitialAddOption = (singleOption, mainCid) =>{
        const keyCate = singleOption.category_id + "" + singleOption.parent_category_id + "cate"
        const keyQues = singleOption.category_id + "" + singleOption.parent_category_id + "ques"
        let arrayOption = [];

        const  payload = {
            "categoryName": this.state.valueInputData[keyQues],
            "question": "",
            "options": [],
            "parentCategoryId": singleOption.category_id,
            "mainCid": mainCid,
            "status":"end",
            "causes":"",
            "summary":""
        }

        let parentField = this.state.masterList.filter((data) => {
            return singleOption.category_id == data.category_id
        })

        const parentOption = parentField[0]
        const parentKeyCate = parentOption.category_id + "" + parentOption.parent_category_id + "cate"
        let parsedJson = JSON.parse(parentOption.options)
        let oldArray = parsedJson.options;
        oldArray.push("Type Here")

        const payloadUpdate = {
            "categoryId": parentOption.category_id,
            "categoryName": parentOption.category_name,
            "question": parentOption.question,
            "options": oldArray,
            "parentCategoryId": parentOption.parent_category_id,
            "mainCid": parentOption.main_cid,
            "status":"continue",
            "causes":"",
            "summary":""
        }

        const configs = {
            method: 'post',
            url: URL.ADD_OPTION,
            data: payload,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        }

        const configs2 = {
            method: 'post',
            url: URL.UPDATE_OPTION,
            data: payloadUpdate,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        }

        if (this.state.valueInputData[keyQues]) {
            axios(configs2).then((response) => {
                axios(configs).then((response) => {
                    axios.get(URL.GET_ALL_OPTION+"/"+this.state.selectedMainId).then((response) => {
                        this.splictCategoryJson(response.data.listCategory)
                        if (document.getElementById(parentKeyCate)) {
                            try{
                                document.getElementById(parentKeyCate).style.display = "block"
                                document.getElementById(parentKeyCate + "icon").classList.remove("fa-chevron-right")
                                document.getElementById(parentKeyCate + "icon").classList.add("fa-chevron-down")
                            }catch(e){

                            }
             
                        }
                    })
                })
            })     
        }else{
            toastr.warning("Please enter question",{   timeOut: 2000,
                newestOnTop: true,
                position: 'top-right',
                transitionIn: 'bounceIn',
                transitionOut: 'bounceOut',
                progressBar: false,
                closeOnToastrClick: false,})
        }
        
    }

    onAddOption = (singleOption, mainCid, initial) => {
        initial ? this.initialAddOption() : this.nonInitialAddOption(singleOption,mainCid)
    }


    onDeleteOption=(singleOption,index)=>{
        let parentField = this.state.masterList.filter((data) => {
            return singleOption.parent_category_id == data.category_id
        })
        const parentOption = parentField[0];

        if(parentOption){

            const payloadDelete={
                "categoryId" : singleOption.category_id
            }
            let parsedJson = JSON.parse(parentOption.options)
            let oldArray = parsedJson.options;

            oldArray = oldArray.filter((value,indexInter)=>{
                  return index != indexInter;
            })

            const payloadUpdate = {
                "categoryId": parentOption.category_id,
                "categoryName": parentOption.category_name,
                "question": parentOption.question,
                "options": oldArray,
                "parentCategoryId": parentOption.parent_category_id,
                "mainCid": parentOption.main_cid,
                "status":"continue",
                "causes":"",
                "summary":""
            }
            const configsDelete = {
                method: 'post',
                url: URL.DELETE_OPTION,
                data: payloadDelete,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }
            const configsUpdate = {
                method: 'post',
                url: URL.UPDATE_OPTION,
                data: payloadUpdate,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }

            axios(configsUpdate).then((response) => {
                axios(configsDelete).then(() => {
                    this.getAllSymptoms()
                })
            })
        }else{
            const payloadDelete = {
                "categoryId" : singleOption.category_id
            }

            const configsDelete = {
                method: 'post',
                url: URL.DELETE_OPTION,
                data: payloadDelete,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }
                axios(configsDelete).then(() => {
                     this.setState({selectedMainId:"",keyLetterSelected:"",selectedMainSymptomName:""},()=>{
                        this.getAllSymptoms()
                     })
                })
            
        }
       
    }

    onChangeInput(symptomData, mainCid, e) {
        localStorage.setItem("lastId",e.target.name)
        let valueInputData = this.state.valueInputData
        valueInputData[e.target.name] = e.target.value
        this.setState({
            valueInputData
        }, () => {
            if (this.timeout) clearTimeout(this.timeout);
            this.timeout = setTimeout(() => {
                this.onUpdateOption(symptomData, mainCid)
                const childArrays = this.state.masterList.filter((data) => {
                    return symptomData.category_id == data.parent_category_id
                })
                childArrays.map((data)=>{
                    this.onUpdateOption(data, mainCid)
                })
            }, 300);
        })
    }

    onChangeOption(singleOption, mainCid, indexOption, e) {
        const keyOption = singleOption.category_id + "" + singleOption.parent_category_id + "opti"
        this.state.valueInputData[keyOption][indexOption] = e.target.value
        this.setState({
            valueInputData: this.state.valueInputData
        }, () => {
            if (this.timeout) clearTimeout(this.timeout);
            this.timeout = setTimeout(() => {
                this.onUpdateOption(singleOption, mainCid)
            }, 300);
        })
    }

    treeDisplay=(id)=>{
        try{
            if( document.getElementById(id).style.display == "none"){
                document.getElementById(id).style.display = "block"
                document.getElementById(id+"icon").classList.remove("fa-chevron-right")
                document.getElementById(id+"icon").classList.add("fa-chevron-down")
            }else{
                document.getElementById(id).style.display = "none"
                document.getElementById(id+"icon").classList.remove("fa-chevron-down")
                document.getElementById(id+"icon").classList.add("fa-chevron-right")
            }
        }catch(e){

        }
    
    }

    viewRow = (optionArray, index, orderMain) => {
        return (
            <div class="datawidthResponsive">
                <button className="addSummary" onClick={() => {
                    this.setState({ openPopup: true ,singleOptionData : optionArray , causeValue : optionArray.summary})
                }}>Add Summary</button>
                <ul class="chatData">
                    {this.viewOptionView(optionArray, orderMain, optionArray.category_id)}
                </ul>
            </div>
        )
    }

    viewOptionView = (optionArray, orderMain, mainCid) => {
        let viewToDisplay = []
        if (optionArray.child && !orderMain) {
            optionArray.child.map((singleOption, index) => {
                const keyCate = singleOption.category_id + "" + singleOption.parent_category_id + "cate"
                const keyQues = singleOption.category_id + "" + singleOption.parent_category_id + "ques"
                const keyOption = singleOption.category_id + "" + singleOption.parent_category_id + "opti"

                if (this.state.valueInputData[keyCate] == undefined) {
                    this.state.valueInputData[keyCate] = singleOption.category_name
                }
                if (this.state.valueInputData[keyQues] == undefined) {
                    this.state.valueInputData[keyQues] = singleOption.question
                }
                let arrayOption = []
                let parsedJson = JSON.parse(singleOption.options)
                if (parsedJson.options) {
                    arrayOption = parsedJson.options
                }
                if (!this.state.valueInputData[keyOption]) {
                    this.state.valueInputData[keyOption] = arrayOption
                }
                let parentField = this.state.masterList.filter((data) => {
                    return singleOption.parent_category_id == data.category_id
                })
                const parentOption = parentField[0]
                const parentKeyOption = parentOption.category_id + "" + parentOption.parent_category_id + "opti"
                const parentKeyQues = parentOption.category_id + "" + parentOption.parent_category_id + "ques"
                const optionData = this.state.valueInputData[parentKeyOption].length != 0 ? this.state.valueInputData[parentKeyOption][index] : ""
                if(optionData){
                    this.state.valueInputData[keyCate] = this.state.valueInputData[parentKeyQues] + " " + optionData
                }else{
                    this.state.valueInputData[keyCate] = this.state.valueInputData[parentKeyQues]
                }
     
                viewToDisplay.push(
                    // <li>
                        <ul class="chatData">
                            <li>
                                <div class="row mt-2 mb-2 ml-2 clr-hover">
                                    <div class="col-lg-3 col-md-4 col-sm-12 col-12">
                                        <div class="d-flex">
                                        <button type="button" onClick={() => { this.treeDisplay(keyCate) }} class=" tranBtn  "> <i id={keyCate+"icon"} class="fas fa-chevron-right"></i></button>
                                            {/* <button type="button" onClick={() => { this.onAddOption(singleOption, mainCid) }} class=" tranBtn  "> <i class="fas fa-plus-circle"></i></button> */}
                                            {/* <input type="text" disabled={!orderMain} name={keyCate} onChange={this.onChangeInput.bind(this,singleOption,mainCid)} value={this.state.valueInputData[keyCate]} placeholder="Category" /></div> */}
                                            <input type="text" className="pd-right25" name={keyCate} onChange={this.onChangeInput.bind(this, singleOption, mainCid)} value={this.state.valueInputData[keyCate]} placeholder="Category" /></div>
                                           <div class="Qusicon50" onClick={()=>{
                                               this.setState({
                                                   openPopup: true,
                                                   singleOptionData : singleOption,
                                                   causeValue : singleOption.causes || ""
                                                })
                                            }}>
                                                {singleOption.causes &&
                                                    <a href="javascript:void(0);" title="Edit Cause" className="greenqa">
                                                        <i class="fas fa-edit"></i></a>
                                                }
                                                {!singleOption.causes &&
                                                    <a href="javascript:void(0);" title="Add Cause" className="redqa">
                                                        <i class="fas fa-plus"></i></a>
                                                }
                                           </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-12 col-12">
                                        <input type="text" name={keyQues} onChange={this.onChangeInput.bind(this, singleOption, mainCid)} value={this.state.valueInputData[keyQues]} placeholder="Question" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                        <div class="addOption">
                                            <ul>
                                                {/* {arrayOption && arrayOption.map((value,indexOption) => {
                                                return (
                                                    <li><input type="text" onChange={this.onChangeOption(this,singleOption,mainCid,indexOption)} value={this.state.valueInputData[keyOption][indexOption]}  placeholder="Option" /></li>
                                                )
                                            })} */}

                                                {arrayOption && arrayOption.map((value, indexOption) => {
                                                    return (
                                                        <li><input type="text" onChange={this.onChangeOption.bind(this, singleOption, mainCid, indexOption)} value={this.state.valueInputData[keyOption][indexOption]} placeholder="Option" /></li>
                                                    )
                                                })}

                                                {arrayOption.length == 0 &&
                                                    <li><input type="text" disabled placeholder="No Option" /></li>
                                                }
                                            </ul>
                                            <div  class="addIconOption">
                                            <span onClick={() => { this.onAddOption(singleOption, mainCid) }}><i  class="fas fa-plus-circle"></i></span>
                                            {arrayOption.length ==0 && <span onClick={()=> this.onDeleteOption(singleOption,index)}><i  class="fas fa-trash" style={{marginLeft:"10px"}}></i></span>}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li id={keyCate} style={{display:"none"}}>
                                {singleOption.child && this.viewOptionView(singleOption, null, mainCid)}
                            </li>
                           
                        </ul>
                    // </li>
                )
            })
        }
        else {
            let arrayOption = null
            let parsedJson = JSON.parse(optionArray.options)

            arrayOption = parsedJson.options

            const keyCate = optionArray.category_id + "" + optionArray.parent_category_id + "cate"
            const keyQues = optionArray.category_id + "" + optionArray.parent_category_id + "ques"
            const keyOption = optionArray.category_id + "" + optionArray.parent_category_id + "opti"

            if (this.state.valueInputData[keyCate] == undefined) {
                this.state.valueInputData[keyCate] = optionArray.category_name
            }
            if (this.state.valueInputData[keyQues] == undefined) {
                this.state.valueInputData[keyQues] = optionArray.question
            }
            if (!this.state.valueInputData[keyOption]) {
                this.state.valueInputData[keyOption] = arrayOption
            }
            viewToDisplay.push(
                // <li>
                    <ul class="chatData ml-100minus" >
                        <li class="clickhideshow">
                            <div class="row mt-2 mb-2 ml-2 clr-hover">
                                <div class="col-lg-3 col-md-4 col-sm-12 col-12">
                                    <div class="d-flex">
                                    <button type="button" onClick={() => { this.treeDisplay(keyCate) }} class=" tranBtn  "> <i id={keyCate+"icon"} class="fas fa-chevron-right"></i></button>
                                        {/* <button type="button" onClick={() => { this.onAddOption(optionArray, mainCid ,true) }} class=" tranBtn  "> <i class="fas fa-plus-circle"></i></button> */}
                                        <input type="text" name={keyCate} onChange={this.onChangeInput.bind(this, optionArray, mainCid)} value={this.state.valueInputData[keyCate]} placeholder="Category" /></div>
                                </div>
                                <div class="col-lg-3 col-md-4 col-sm-12 col-12">
                                    <input type="text" name={keyQues} onChange={this.onChangeInput.bind(this, optionArray, mainCid)} value={this.state.valueInputData[keyQues]} placeholder="Question" />
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="addOption">
                                        <ul>
                                            {arrayOption && arrayOption.map((value, indexOption) => {
                                                return (
                                                    <li><input type="text" onChange={this.onChangeOption.bind(this, optionArray, mainCid, indexOption)} value={this.state.valueInputData[keyOption][indexOption]} placeholder="Option" /></li>
                                                )
                                            })}
                                            {arrayOption.length == 0 &&
                                                <li><input type="text" disabled placeholder="No Option" /></li>
                                            }
                                        </ul>
                                        <div  class="addIconOption">
                                            <span onClick={() => { this.onAddOption(optionArray, mainCid) }}><i class="fas fa-plus-circle"></i></span>
                                            {arrayOption.length ==0 && <span onClick={()=> this.onDeleteOption(optionArray)}><i class="fas fa-trash" style={{marginLeft:"10px"}}></i></span>}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li id={keyCate} style={{display:"none"}}>
                            {optionArray.child && this.viewOptionView(optionArray, null, mainCid)}
                        </li>
                    </ul>
                // </li>
            )
        }
        return viewToDisplay
    }

    recurseArray = (itemArray, groupObject) => {
        let newItemArray = itemArray.map((data) => {
            let symptopmData = Object.assign({}, data)
            if (groupObject[symptopmData.category_id]) {
                symptopmData.child = this.recurseArray(groupObject[symptopmData.category_id], groupObject)
            }
            return symptopmData
        })
        return newItemArray
    }

    splictCategoryJson = (list,parentId) => {
        let groupObject = _.groupBy(list, "parent_category_id")
        let originalTopSymptomArray = _.filter(list, (value) => { return value.parent_category_id == 0 })
        let topSymptomArray = originalTopSymptomArray.map((data) => {
            let newObject = Object.assign({}, data)
            if (groupObject[newObject.category_id]) {
                newObject.child = this.recurseArray(groupObject[newObject.category_id], groupObject)
            }
            return newObject
        });
        this.setState({
            allSymptomApi: topSymptomArray,
            masterList: list,
            groupObjectMaster : groupObject
        })
    }
    modalSyptoms=()=>{
        return (
            <div class="modal d-block show" >
                <div class="modal-dialog">
                    <div class="modal-content modalPopUp">
                        <div class="modal-header borderNone">
                            <h5 class="modal-title">{this.state.singleOptionData.parent_category_id != 0 ?  "Add Cause" : "Add Summary"}</h5>
                            <button type="button" class="popupClose" onClick={()=>{
                                this.setState({ openPopup: false })
                            }}><i class="fas fa-times"></i></button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="form-group">
                                        <label class="commonLabel clr1002612" >Category : <strong color="black">{this.state.singleOptionData && this.state.singleOptionData.category_name}</strong></label>
                                            <textarea value={this.state.causeValue && this.state.causeValue } onChange={(e)=>{
                                                this.setState({causeValue:e.target.value})
                                            }} style={{height:"200px"}} class="form-control" name="description"  ></textarea>
                                        </div>
                                    </div>
                                   <div class="col-md-12 col-lg-12 col-sm-12 mt-3 text-center">
                                        <button type="button" class="commonBtn" onClick={()=>{
                                            this.onUpdateOption(this.state.singleOptionData,this.state.singleOptionData.main_cid,this.state.causeValue)
                                            this.setState({
                                                openPopup : false,
                                                causeValue : ""
                                            },()=>{
                                                toastr.success("Message","Successfully Updated.", this.toasterOptions);

                                                setTimeout(()=>{
                                                    this.getAllSymptoms()
                                                },1000)
                                             
                                            })
                                        }} >Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>)
    }

    addSymptomNamePopUp=()=>{
        return (
            <div class="modal d-block show" >
                <div class="modal-dialog">
                    <div class="modal-content modalPopUp">
                        <div class="modal-header borderNone">
                            <h5 class="modal-title">Add Symptom Name</h5>
                            <button type="button" class="popupClose" onClick={()=>{
                                this.setState({ openPopupAddSymptom: false })
                            }}><i class="fas fa-times"></i></button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="form-group">
                                            <textarea value={this.state.symptomNameFull} onChange={(e)=>this.setState({symptomNameFull:e.target.value})} style={{height:"100px"}} class="form-control" name="description"  ></textarea>
                                        </div>
                                    </div>
                                   <div class="col-md-12 col-lg-12 col-sm-12 mt-3 text-center">
                                        <button type="button" class="commonBtn" disabled={!this.state.symptomNameFull}
                                            onClick={() => {
                                                this.setState({ openPopupAddSymptom: false }, () => {
                                                    this.onAddOption(null, null, true)
                                                    const charAtOne = this.state.symptomNameFull.charAt(0).toLowerCase()
                                                    this.setState({ prevLetter: charAtOne },()=>{
                                                        setTimeout(()=>{
                                                            this.setState({ prevLetter: "" })
                                                        },2500)
                                                    })
                                                })
                                              
                                            }}
                                        >Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>)
    }

    render() {
        const { allSymptomApi, listAllPreviousSymptoms, keyLetterSelected = "", selectedMainSymptomName = "", openPopupAddSymptom ,prevLetter="" } = this.state;
        const symptomListGroup = _.groupBy(listAllPreviousSymptoms,"keyLetter");
        const litsSelectedLetter = (keyLetterSelected && symptomListGroup[keyLetterSelected]) || []
        return (
            <div>
                <div class="mainMenuSide" id="main-content">
                    {this.state.openPopup && this.modalSyptoms()}
                    {openPopupAddSymptom && this.addSymptomNamePopUp()}
                    <div class="wrapper">
                        <div class="container-fluid">
                            <div class="body-content">
                                <div class="tomCard">
                                    <div class="tomCardHead">
                                        <h5 class="float-left">Symptoms QA</h5>
                                        <button type="button" onClick={() => this.setState({ openPopupAddSymptom: true, symptomNameFull:""})} class="commonBtn float-right">Add New Symptom</button>
                                    </div>

                                    <ul className="filterAlphabets">  {Array(26).fill(1).map((val, index) => {
                                        const alphaCharacter = String.fromCharCode(65 + index)
                                        const charCheck = _.has(symptomListGroup, alphaCharacter.toLowerCase())
                                        const showBlink = alphaCharacter.toLowerCase() == prevLetter
                                        return <li>
                                            <a id={"blinking"+alphaCharacter.toLowerCase()} className={`${!charCheck ? 'disabledChar' :""} ${showBlink ? 'blinking' :""} `} href="javascript:void(0);" onClick={() => this.setState({ keyLetterSelected: alphaCharacter.toLowerCase(),selectedMainId:"",selectedMainSymptomName:""})} >{alphaCharacter} </a>
                                        </li>
                                    })}   </ul>
                                    {!selectedMainSymptomName &&  <div className="search-list">
                                        <h2>{keyLetterSelected && keyLetterSelected.toUpperCase()}</h2>
                                        <ul>
                                            {litsSelectedLetter.map((val) =>
                                                <li onClick={() => this.setState({ selectedMainId: val.symptomsId, selectedMainSymptomName: val.symptomsName }, () => this.getAllSymptoms())}> &#35; {val.symptomsName} </li>
                                            )}</ul>
                                    </div>}

                                    {selectedMainSymptomName && <div class="tomCardBody">
                                        {allSymptomApi.map((value, index) => {
                                            return (
                                                <div class="innerCard" style={{borderRadius: "3px"}}>
                                                    <div class="chatDataEntry">
                                                        {this.viewRow(value, index, true)}
                                                    </div>
                                                </div>
                                            )
                                        })}
                                    </div>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        )
       
    }
}
export default SymptomsQAentry;
