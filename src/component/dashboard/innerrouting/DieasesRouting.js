import React, { Component } from "react";
import { Switch, Route, Redirect } from 'react-router-dom';
import NewDiseases from "../../patient/healthrecords/Diseases/NewDiseases";
class DieasesRouting extends Component {
    render() {
        return (
            <Switch>
                {/* <Redirect path="/medicine" exact to="/medicine/data" />  */}
                <Route path="/diseases/newdiseases" component={NewDiseases} />
          

            </Switch>
        )
    }
}
export default DieasesRouting;