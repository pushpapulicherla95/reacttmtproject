
import URL from '../../asset/configUrl';
import axios from 'axios';
import actiontype from '../../service/jitsi/actionType'

export const joinJitsiMeet = jitsiInfo => dispatch => {

    const configs = {
        method: 'post',
        url: URL.USER_LOGIN,
        data: jitsiInfo,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            'Access-Control-Allow-Origin': ' http://127.0.0.1:3000',
        },

    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actiontype.JITSI_SUCCESS,
                payload: res.data
            })
        }
    })
        .catch(err => {
            dispatch({
                type: actiontype.JITSI_FAILURE,
                error: err
            })
        })
}
