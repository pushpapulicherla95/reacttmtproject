import React , {Component} from 'react';
import { NavLink  } from 'react-router-dom';
import { withRouter } from "react-router";
import LandingHeader from '../common/LandingHeader';
class Partner extends Component{
    componentDidMount() {
        window.scrollTo(0, 0)
    }
    render(){
        return(
<>
<LandingHeader/>
<section class="MbeBannerSection"></section>
	<section class="banner d-flex justify-content-center align-items-center">
		<div class="container">
			<div class="bannerContent  flex-column">
				<h1>Welcome to
					<span>tomato Medical </span>
				</h1>
				<p>Dr. med. Lemberger, a German Orthopedic and Trauma Surgeon, created this outstanding solution for patients worldwide.
					He thinks that everybody worldwide should have access to medical knowledge and therapy. </p>
		
			</div>
		</div>
	</section>
	<div class="emergencyAlert">
		<a href="" > <img src="images/emergencyCall.png"/> </a>
	</div>
	<section class="ourServices">
		<div class="container">
			<h1 class="titles text-center">Our Services
				<span>For You</span>
			</h1>
			<div class="serviceList">
				<div class="row">
					<div class="col-lg-4 col-md-6 col-sm-12 col-12 paddingLeft0">
						<div class="serviceBg">
							<div class="serviceIcon">
								<img src="images/telemedicine-icon.png" />
							</div>
							<h4>Telemedicine</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s </p>
							<button type="button" class="readmore">Start Here</button>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12 col-12 paddingLeft0">
						<div class="serviceBg">
							<div class="serviceIcon">
								<img src="images/healthRecord.png" />
							</div>
							<h4>health record </h4>
							<p>You can store your data safely and have worldwide access to your data and translations in multiple languages worldwide,
								emergency data... </p>
							<button type="button" class="readmore">Start Here</button>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12 col-12 paddingLeft0">
						<div class="serviceBg">
							<div class="serviceIcon">
								<img src="images/emerancy.png" />
							</div>
							<h4>emergency services</h4>
							<p>GPS alarm via email and SMS. Dead man alarm sets off alarm automatically when smartphone doesn´t move any longer.Can
								actively transfer ... </p>
							<button type="button" class="readmore">Start Here</button>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12 col-12 paddingLeft0">
						<div class="serviceBg">
							<div class="serviceIcon">
								<img src="images/big-data-register.png" />
							</div>
							<h4>Big data Register
							</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s </p>
							<button type="button" class="readmore">Start Here</button>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12 col-12 paddingLeft0">
						<div class="serviceBg">
							<div class="serviceIcon">
								<img src="images/hospital.png" />
							</div>
							<h4>find medical expert </h4>
							<p>Find the right specialist for your health by specialty and location and make an online reservation for hospitals,
								doctors, therapists and all kind of caregivers. </p>
							<button type="button" class="readmore">Start Here</button>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12 col-12 paddingLeft0">
						<div class="serviceBg">
							<div class="serviceIcon">
								<img src="images/sales-funnel.png" />
							</div>
							<h4>Sales funnel</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
							<button type="button" class="readmore">Start Here</button>
						</div>
					</div>
				</div>
			</div>
		</div>


	</section>
	
 <div class="scrollTop">
			<a href="javascript:void(0)">
				<i class="fa fa-arrow-up" aria-hidden="true"></i>
			</a>
		</div> 
	<section class="downloadApp">
		<div class="container">
			<div class="downloadappContent">
				<h2>Click here to get your free medical emergency app with
					<b>save storage</b> of your health data
				</h2>
				<button type="button" class="commonBtn">Download Now</button>
			</div>
		</div>
	</section>
	<section class="faqSection">
		<div class="container">
			<h1 class="titles text-center">Frequently asked
				<span>questions</span>
			</h1>
			<div class="faqContent">
				<dl>
					<dt>
						<span>
							<i class="fas fa-chevron-right"></i>
						</span> Can I download the app for android AND Apple/IOS?</dt>
						<dd>Yes, Press <a href="">Here</a> to download the app. You can also download it from Google Play or Apple App Store.</dd>
					<dt>
						<span>
							<i class="fas fa-chevron-right"></i>
						</span> Are my data/information save?</dt>
					<dd>Yes. All data are encrypted and protected according to european law (DSGVO/GDPR) and we are in the process to get HIPPA clearance and ISO 27001 certification).</dd>
					<dt>
						<span>
							<i class="fas fa-chevron-right"></i>
						</span> What are the requirements to use the app?</dt>
					<dd>You can use the app on every PC, Mac, Android and IOS device with a web browser.</dd>
					<dt>
						<span>
							<i class="fas fa-chevron-right"></i>
						</span> What are the benefits, when I use the app?</dt>
					<dd>You can check your medical symptoms any time, if you are not sure, call a telemedicine doctor, find a real doctor for
						your problem and make an appointment online, use medication intake & vaccination reminder, shop for medication and
						products, save your life with with our multilingual emergency GPS-services, store and manage your personal medical
						data and exchange them with your doctors to avoid double exams, use different languages when you are abroad for your
						medical communication.</dd>
				</dl>
				<div class="text-center mb-3">
					<button type="button" class="commonBtn text-center">Try it now for free</button>
				</div>
			</div>
		</div>
	</section>
<footer>
          <div class="footerLink">
            <div class="container">
              <ul>
                <li>
                  <NavLink to="/aboutus" >About </NavLink>
                </li>
                <li>
                <NavLink to="/partners" >For Partners </NavLink>
                </li>
                <li>
                <NavLink to="/contact" >Contact </NavLink>
                </li>
                <li>
                <NavLink to="/datasecurity" >Data Security </NavLink>
                </li>
                <li>
                <NavLink to="/privacypolicy" > Privacy Policy </NavLink>
                </li>
                <li>
                <NavLink to="/imprint">Imprint </NavLink>
                </li>
              </ul>
            </div>
          </div>

          <div class="footerCopyRights">
            <div class="container">
              <ul>
                <li>
                  <a href="">
                    <i class="fab fa-facebook-f" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-google-plus-g" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-instagram" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-twitter" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-linkedin-in" />
                  </a>
                </li>
              </ul>
              <p>Copyrights @ 2019, All Rights Reserved</p>
            </div>
          </div>
        </footer>
</>
        );
    }
}
export default Partner;