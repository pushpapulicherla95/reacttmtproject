import URL from "../../asset/configUrl";
import axios from "axios";
import actionType from "../../service/healthrecord/actionType";
import { getPatientHealthData } from "../patient/action";
import { toastr } from 'react-redux-toastr';
import { toastrOptions } from "../../component/common/toasteroptions";
export const addHospitaldata = (hospitalinfo) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_HOSPITAL,
        data: hospitalinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getHospitaldata = {
        "uid": hospitalinfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPatientHealthData(getHospitaldata))
            dispatch({
                type: actionType.ADD_HOSPITAL_DATA_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_HOSPITAL_DATA_FAILURE,
            payload: err
        })
    })

}

export const deleteHospitaldata = (hospitalinfo) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.DELETE_HOSPITAL,
        data: hospitalinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getHospitaldata = {
        "uid": hospitalinfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPatientHealthData(getHospitaldata))
            dispatch({
                type: actionType.DELETE_HOSPITAL_DATA_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.DELETE_HOSPITAL_DATA_FAILURE,
            payload: err
        })
    })
}
export const editHospitaldata = (hospitaleditinfo) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.EDIT_HOSPITAL,
        data: hospitaleditinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getHospitaldata = {
        "uid": hospitaleditinfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPatientHealthData(getHospitaldata))
           dispatch({
                type: actionType.EDIT_HOSPITAL_DATA_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.EDIT_HOSPITAL_DATA_FAILURE,
            payload: err
        })
    })
}
//Immunization
export const addImmunizationdata = (immunizationinfo) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_IMMUNIZATION,
        data: immunizationinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getImmunizationdata = {
        "uid": immunizationinfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPatientHealthData(getImmunizationdata))
            dispatch({
                type: actionType.ADD_IMMUNIZATION_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_IMMUNIZATION_FAILURE,
            payload: err
        })
    })

}

export const deleteImmunizationdata = (immunizationinfo) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.DELETE_IMMUNIZATION,
        data: immunizationinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getImmunizationdata = {
        "uid": immunizationinfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPatientHealthData(getImmunizationdata))
            dispatch({
                type: actionType.DELETE_IMMUNIZATION_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.DELETE_IMMUNIZATION_FAILURE,
            payload: err
        })
    })
}
export const editImmunizationdata = (immunizationinfo) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDATE_IMMUNIZATION,
        data: immunizationinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getImmunizationdata = {
        "uid": immunizationinfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPatientHealthData(getImmunizationdata))
            dispatch({
                type: actionType.EDIT_IMMUNIZATION_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.EDIT_IMMUNIZATION_FAILURE,
            payload: err
        })
    })
}
//personalData
export const addPersonaldata = (personaldatainfo) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_PERSONALDATA,
        data: personaldatainfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getImmunizationdata = {
        "uid":personaldatainfo.userId
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            console.log("personal",getImmunizationdata)
            dispatch(getPersonaldata(getImmunizationdata))
            dispatch({
                type: actionType.ADD_PERSONALDATA_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    })
        .catch(err => {
            dispatch({
                type: actionType.ADD_PERSONALDATA_FAILURE,
                payload: err
            })
        })

}
//Coredata
export const addCoredata = (coredatainfo) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_COREDATA,
        data: coredatainfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getImmunizationdata = {
        "uid":coredatainfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getImmunizationdata))
            dispatch({
                type: actionType.ADD_COREDATA_SUCCESS,
                payload: res.data
            })

            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_COREDATA_FAILURE,
            payload: err
        })
    })

}
//job
export const addJobdata = (jobdatainfo) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_JOBDATA,
        data: jobdatainfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getImmunizationdata = {
        "uid":jobdatainfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getImmunizationdata))
            dispatch({
                type: actionType.ADD_JOBDATA_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_JOBDATA_FAILURE,
            payload: err
        })
    })

}
//Emergency
export const addEmergencydata = (emergencydatainfo) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_EMERGENCY,
        data: emergencydatainfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getImmunizationdata = {
        "uid":emergencydatainfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getImmunizationdata))
            dispatch({
                type: actionType.ADD_EMERGENCY_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_EMERGENCY_FAILURE,
            payload: err
        })
    })

}
//Benefits
export const addBenefitsdata = (benefitsdatainfo) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_BENEFITS,
        data: benefitsdatainfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getImmunizationdata = {
        "uid":benefitsdatainfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getImmunizationdata))
            dispatch({
                type: actionType.ADD_BENEFITS_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_BENEFITS_FAILURE,
            payload: err
        })
    })

}
//List
export const getPersonaldata = (listpersonaldatainfo) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.LIST_PERSONALDATA,
        data: listpersonaldatainfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    // const getImmunizationdata = {
    //     "uid":listpersonaldatainfo.uid
    // }
    axios(configs).then((res) => {

        if (res.status === 200) {
            // dispatch(getPatientHealthData(getImmunizationdata))           
             dispatch({
                type: actionType.GET_PERSONALDATA_SUCCESS,
                payload: res.data
            })
        //     toastr.success('Message', res.data.message, toastrOptions);
        // } else if (res.data.status == "failure") {
        //     toastr.error('error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_PERSONALDATA_FAILURE,
            payload: err
        })
    })

}
//Mediciens
export const addMedicine = (medicalinfo) => dispatch => {

    const configs = {
        method: 'post',
        url: URL.ADD_MEDICINE,
        data: medicalinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const userinfo = {
        "uid": medicalinfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPatientHealthData(userinfo))

            dispatch({
                type: actionType.ADD_MEDICINE_SUCCESS,
                payload: res.data
            })

            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_MEDICINE_FAILURE,
            payload: err
        })
    })

}

//Edit Medical Record
export const editMedicaldata = (medicalinfo) => dispatch => {

    const configs = {
        method: 'post',
        url: URL.EDIT_MEDICINE,
        data: medicalinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getHospitaldata = {
        "uid": medicalinfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPatientHealthData(getHospitaldata))
            dispatch({
                type: actionType.EDIT_MEDICINE_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.EDIT_MEDICINE_FAILURE,
            payload: err
        })
    })

}
//list of length
export const edithospitaldata = (medicalinfo) => dispatch => {

    const configs = {
        method: 'post',
        url: URL.EDIT_HOSPITAL,
        data: medicalinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getHospitaldata = {
        "uid": medicalinfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPatientHealthData(getHospitaldata))
            dispatch({
                type: actionType.EDIT_HOSPITAL_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.EDIT_HOSPITAL_FAILURE,
            payload: err
        })
    })

}

//DELETE MEDICAL DATA

export const deleteMedicaldata = (deleteinfo) => dispatch => {

    const configs = {
        method: 'post',
        url: URL.DELETE_MEDICINE,
        data: deleteinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
    const getHospitaldata = {
        "uid": deleteinfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPatientHealthData(getHospitaldata))
            dispatch({
                type: actionType.DELETE_MEDICINE_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.DELETE_MEDICINE_FAILURE,
            payload: err
        })
    })
}

// export const diseasesList=()=>dispatch=>{
//     axios.get("https://api1.tomatomedical.com/tomatomedical/API/patient/medicaldata/allpersonaldata/diseaselist")
//     .then(response=>{
//         dispatch({
//           type: actionType.DISEASESLIST_SUCCESS,
//           payload: response.data.therapists
//         });
//     })
//     .catch(error=>{
//          dispatch({
//            type: actionType.DISEASESLIST_FAILURE,
//            payload: error
//          });
//     })
// }
//
export const diseasesList = (diseaseslist) => dispatch => {

    const configs = {
        method: 'get',
        url: URL.LIST_DIEASESDATA,
        data: diseaseslist,
        // headers: {
        //     'Content-Type': 'application/x-www-form-urlencoded'
        // }
    }
  
    axios.get(URL.LIST_DIEASESDATA).then((res) => {
        if (res.status === 200) {
            console.log("res",res)
            dispatch({
                type: actionType.DISEASESLIST_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.DISEASESLIST_FAILURE,
            payload: err
        })
    })
}
//
export const familyList=()=>dispatch=>{
    axios.get("https://api1.tomatomedical.com/tomatomedical/API/patient/medicaldata/allpersonaldata/diseaselistfamily")
    .then(response=>{
       dispatch({
         type: actionType.FAMILYLIST_SUCCESS,
         payload: response.data.therapists
       });
        
    })
    .catch(error=>{
        dispatch({
          type: actionType.FAMILYLIST_FAILURE,
          payload: error.response.data
        });
    })
}

export const adddiseases=(payload)=>dispatch=>{
    axios.post("https://api1.tomatomedical.com/tomatomedical/API/patient/medicaldata/allpersonaldata/otherdiseases/add",payload)
    .then(response=>{
       dispatch({
         type: actionType.ADDDISEASESLIST_SUCCESS,
         payload: response.data
       });
        
    })
    .catch(error=>{
        dispatch({
          type: actionType.ADDDISEASESLIST_FAILURE,
          payload: error.response.data
        });
    })
}

export const editdiseases=(payload)=>dispatch=>{
    axios.post("https://api1.tomatomedical.com/tomatomedical/API/patient/medicaldata/allpersonaldata/otherdiseases/edit",payload)
    .then(response=>{
       dispatch({
         type: actionType.EDITDISEASESLIST_SUCCESS,
         payload: response.data
       });
        
    })
    .catch(error=>{
        dispatch({
          type: actionType.EDITDISEASESLIST_FAILURE,
          payload: error.response.data
        });
    })
}

export const deletediseases=(payload)=>dispatch=>{
    axios.post("https://api1.tomatomedical.com/tomatomedical/API/patient/medicaldata/allpersonaldata/otherdiseases/delete",payload)
    .then(response=>{
       dispatch({
         type: actionType.DELETEDISEASESLIST_SUCCESS,
         payload: response.data
       });
        
    })
    .catch(error=>{
        dispatch({
          type: actionType.DELETEDISEASESLIST_FAILURE,
          payload: error.response.data
        });
    })
}


export const addupdateDiseases=(payload)=>dispatch=>{
    axios.post("https://api1.tomatomedical.com/tomatomedical/API/patient/healthrecord/diseasefamily/addupdate",payload)
    .then(response=>{
       dispatch({
         type: actionType.ADDUPDATEDISEASES_SUCCESS,
         payload: response.data
       });
        
    })
    .catch(error=>{
        dispatch({
          type: actionType.ADDUPDATEDISEASES_FAILURE,
          payload: error.response.data
        });
    })
}
//sports
export const addSportsdata = (createsports) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_SPORTDATA,
        data: createsports,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getImmunizationdata = {
        "uid":createsports.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getImmunizationdata))
            dispatch({
                type: actionType.ADD_SPORTDATA_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    })
        .catch(err => {
            dispatch({
                type: actionType.ADD_SPORTDATA_FAILURE,
                payload: err
            })
        })

}
export const updateSportsdata = (updatesports) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDATE_SPORTDATA,
        data: updatesports,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getImmunizationdata = {
        "uid":updatesports.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getImmunizationdata))
            dispatch({
                type: actionType.UPDATE_SPORTDATA_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    })
        .catch(err => {
            dispatch({
                type: actionType.UPDATE_SPORTDATA_FAILURE,
                payload: err
            })
        })

}
export const deleteSportsdata = (deletesports) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.DELETE_SPORTDATA,
        data: deletesports,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getImmunizationdata = {
        "uid":deletesports.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getImmunizationdata))
            dispatch({
                type: actionType.DELETE_SPORTDATA_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    })
        .catch(err => {
            dispatch({
                type: actionType.DELETE_SPORTDATA_FAILURE,
                payload: err
            })
        })

}
//preganencies
export const addPregnanciesdata = (pregnanciesdatainfo) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_PRGNANCIESDATA,
        data: pregnanciesdatainfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getImmunizationdata = {
        "uid":pregnanciesdatainfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getImmunizationdata))
            dispatch({
                type: actionType.ADD_PRGNANCIESDATA_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    })
        .catch(err => {
            dispatch({
                type: actionType.ADD_PRGNANCIESDATA_FAILURE,
                payload: err
            })
        })

}
export const addListOfPrgnancies = (pregnancieslistinfo) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.ADD_PRGNANCIESLIST,
        data: pregnancieslistinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getImmunizationdata = {
        "uid":pregnancieslistinfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getImmunizationdata))
            dispatch({
                type: actionType.ADD_PRGNANCIESLIST_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    })
        .catch(err => {
            dispatch({
                type: actionType.ADD_PRGNANCIESLIST_FAILURE,
                payload: err
            })
        })

}
export const updateListOfPrgnancies = (updatepregnancieslist) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDATE_PRGNANCIESLIST,
        data: updatepregnancieslist,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getImmunizationdata = {
        "uid":updatepregnancieslist.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getImmunizationdata))
            dispatch({
                type: actionType.UPDATE_PRGNANCIESLIST_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    })
        .catch(err => {
            dispatch({
                type: actionType.UPDATE_PRGNANCIESLIST_FAILURE,
                payload: err
            })
        })

}
export const deleteListOfPrgnancies = (deletepregnancieslist) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.DELETE_PRGNANCIESLIST,
        data: deletepregnancieslist,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getImmunizationdata = {
        "uid":deletepregnancieslist.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getPersonaldata(getImmunizationdata))
            dispatch({
                type: actionType.DELETE_PRGNANCIESLIST_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "Error", toastrOptions);
        }
    })
        .catch(err => {
            dispatch({
                type: actionType.DELETE_PRGNANCIESLIST_FAILURE,
                payload: err
            })
        })

}

