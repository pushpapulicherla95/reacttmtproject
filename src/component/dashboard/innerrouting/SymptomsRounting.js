import React, { Component } from "react";
import { Switch, Route, Redirect } from 'react-router-dom';
// import Symptoms from "../../patient/healthrecords/Symptoms/Symptoms";
import Assesment from "../../patient/healthrecords/Assesment/Assesment";
class SymptomsRounting extends Component {
    render() {
        return (
          <Switch>
            {/* <Redirect path="/medicine" exact to="/medicine/data" />  */}
            {/* <Route path="/symptoms/symptoms" component={Symptoms} /> */}
            <Route path="/symptoms/assesment" component={Assesment  } />
          </Switch>
        );
    }
}
export default SymptomsRounting;