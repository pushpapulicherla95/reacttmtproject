import actionType from "../hospital/actionType";
const initialState = {
    hospitalAdminInfo: "",
    hospitalDoctorList: "",
    hospitalPatientList:"",
    profilePicInfo:""
}
const hospitalReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.GET_HOSPITAL_ADMININFO_SUCCESS:
            return {
                ...state,
                hospitalAdminInfo: action.payload
            }
        case actionType.GET_HOSPITAL_ADMININFO_FAILURE:
            return {
                ...state,
                hospitalAdminInfo: action.error
            }
        case actionType.UPDATE_HOSPITAL_ADMININFO_SUCCESS:
            return {
                ...state
            }
        case actionType.UPDATE_HOSPITAL_ADMININFO_FAILURE:
            return {
                ...state
            }
        case actionType.ADD_HOSPITAL_DOCTOR_SUCCESS:
            return {
                ...state
            }
        case actionType.ADD_HOSPITAL_DOCTOR_FAILURE:
            return {
                ...state
            }
        case actionType.GET_HOSPITAL_DOCTOR_SUCCESS:
            return {
                ...state,
                hospitalDoctorList: action.payload
            }
        case actionType.GET_HOSPITAL_DOCTOR_FAILURE:
            return {
                ...state,
                hospitalDoctorList: action.error
            }
        case actionType.UPDTAE_HOSPITAL_DOCTOR_SUCCESS:
            return {
                ...state
            }
        case actionType.UPDTAE_HOSPITAL_DOCTOR_FAILURE:
            return {
                ...state
            }
        case actionType.ADD_HOSPITAL_PATIENT_SUCCESS:
            return {
                ...state
            }
        case actionType.ADD_HOSPITAL_PATIENT_FAILURE:
            return {
                ...state
            }
        case actionType.GET_HOSPITAL_PATIENT_SUCCESS:
            return {
                ...state,
                hospitalPatientList: action.payload
            }
        case actionType.GET_HOSPITAL_PATIENT_FAILURE:
            return {
                ...state,
                hospitalPatientList: action.error
            }
        case actionType.UPDTAE_HOSPITAL_PATIENT_SUCCESS:
            return {
                ...state
            }
        case actionType.UPDTAE_HOSPITAL_PATIENT_FAILURE:
            return {
                ...state
            }
            case actionType.UPLOAD_PROFILEPIC_SUCCESS:
                return{
                    ...state,
                    profilePicInfo:action.payload
                }
            case actionType.UPLOAD_PROFILEPIC_SUCCESS:
            return{
                ...state,
                profilePicInfo:action.error
            }
        default:
            return state;
    }
}
export default hospitalReducer;