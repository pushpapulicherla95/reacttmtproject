import axios from "../../../asset/axios";
import URL from "../../../asset/configUrl";
import moment from 'moment'
export const onlyNumbers = (e) => {
  const re = /[0-9]+/g;
  if (!re.test(e.key)) {
    e.preventDefault();
  }
}

export const onlyAlphabets = (e) => {
  const re = /^[a-zA-Z ]*$/;
  if (!re.test(e.key)) {
    e.preventDefault();
  }
}
export const Alphanumeric= (e) => {
  const re =/[a-zA-Z0-9]/;
  if (!re.test(e.key)) {
    e.preventDefault();
  }
}
//                    /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i
export const emailFormat = (e) => {
  const re = /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i;
  if (!re.test(e.key)) {
    e.preventDefault();
  }
}
export const onlyDate = (e) => {
  const re = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
  if (!re.test(e.key)) {
    e.preventDefault();
  }
}
export const NumbersAndDot = (e) => {

  var rgx = /^[0-9]*\.?[0-9]*$/

  if (!rgx.test(e.key)) {
    e.preventDefault();
  }
}

export const Geolocation = () => {
  // const Geo = window.navigator && window.navigator.geolocation;
  // console.log("inside ;location",window.navigator.geolocation)
  navigator.geolocation.getCurrentPosition((position) => {

    const userid = sessionStorage.getItem("loginDetails");
    const ID = userid && JSON.parse(userid);
    const datenew = moment(position.coords.timestamp).format("DD-MM-YYYY h:mm")
    const positions = {
      lat: position.coords.latitude,
      lon: position.coords.longitude,
      userId: ID && ID.userInfo && ID.userInfo.userId,
      timestamp: datenew,
    }

    // const configs = {
    //   method: 'post',
    //   // url: URL.GEO_LOCATION,
    //   url: 'https://tomato.colan.in:9001/tomatomedical/API/savelatlon',
    //   data: positions,
    //   headers: {
    //     'Content-Type': 'application/x-www-form-urlencoded'
    //   },
    // }
    // //  console.log("inside",position)

    // fetch(configs).then((res) => {
    //   if (res.status === 200) {
    //     console.log("successfully retrived location", res)
    //   }
    // }).catch(err => {
    //   console.log("ERROR", err)
    // })

    fetch('https://tomato.colan.in:9001/tomatomedical/API/savelatlon', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: JSON.stringify(positions)
    })
      .then(function (res) {
        return res.json();
      })
      .then(function (data) {
        console.log(JSON.stringify(data))
      })

  }, (e) => {

    console.log("location error", e)
  }, {
      enableHighAccuracy: true
    })

}