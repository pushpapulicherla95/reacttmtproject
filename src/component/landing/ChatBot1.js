import React from "react";
import axios from "axios";
import QuestionAnswer from "./QuestionAnswer";
import Loader from "react-loader-spinner";
import URL from "../../asset/configUrl.js";
import ChatBotForm from "./ChatBotForm.js";

const start = [
  { name: "Hai there to help you Just start a symptoms assessment." },
  { options: ["Start symptoms assessment", "Track my symptoms"] }
];

const step2 = [
  { name: "Who is the assessement for? " },
  { options: ["Myself", "Someoneless"] }
];

class ChatBot1 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      viewType: props.viewStep ? props.viewStep : "step1",
      symptomsList: [],
      filterSymptoms: [],
      selectedSymptoms: "",
      symtomDescriptionView: false,
      nextSymptoms: "",
      symptomsTrack: [],
      selectedOption: "",
      question: "",
      text: "",
      child_viewType: 1,
      child_steps: "step",
      child_type: "step1",
      parentId: 0,
      assessmentId: "",
      symptoms: ""
    };
  }

  componentWillReceiveProps(newprops) {
    const value = newprops.continue && JSON.parse(newprops.continue.openReport).question;
    const selected = value && value[value.length - 1];
    if (newprops.viewStep != null) {
      this.setState({
        viewType: newprops.viewStep,
        selectedSymptoms: selected.question + " " + selected.selectedOption,
        question: selected.question,
        selectedOption: selected.selectedOption,
        assessmentId: newprops.continue.assesmentId,
        parentId: newprops.continue.parentId,
        symptoms: newprops.continue.symptoms
      });

    }
  }

  symptomsList = () => {
    axios.get(URL.LIST_CHAT).then(response => {
      this.setState({ symptomsList: response.data.symptoms });
    });
  };

  loadingActive = () => {
    this.setState({ loading: true });
  };
  loadingDeActive = () => {
    this.setState({ loading: false });
  };
  handleFilter = e => {
    const { symptomsList } = this.state;
    this.setState({ [e.target.name]: e.target.value });
    if (e.target.value.length > 0) {
      this.setState({
        filterSymptoms: symptomsList.filter((each, i) => {
          return each.symptomsName.includes(e.target.value);
        })
      });
    } else {
      this.setState({ filterSymptoms: symptomsList });
    }
  };

  componentDidMount() {
    this.symptomsList();
  }

  stepIncrement = () => {
    let inc = this.state.child_viewType + 1;
    this.setState({ child_type: "step" + inc, child_viewType: inc }, () => {
      if (inc == 6) {
        this.setState({ viewType: "step2" });
      }
    });
  };

  switchCaseRender = () => {
    const { name } = start[0];
    const { options } = start[1];
    const name1 = step2[0].name;
    const options1 = step2[1].options;
    switch (this.state.viewType) {
      case "step1":
        return (
          <ChatBotForm
            child_type={this.state.child_type}
            stepIncrement={this.stepIncrement}
            loadingActive={this.loadingActive}
            loadingDeActive={this.loadingDeActive}
          />
        );
        break;
      case "step2":
        return (
          <div className="chat-conversion">
            <div className="chatQuestion">{name && <p>{name}</p>}</div>
            <div className="chatButton">
              <ul>
                {/* <li>
                  <button>Less than one day</button>
                </li> */}
                {options.map(each => (
                  <li>
                    <button
                      type="button"
                      className={
                        this.state.text == each ? "dummy active" : "dummy"
                      }
                      onClick={() => {
                        this.setState({ loading: true });
                        this.setState({ text: each });
                        setTimeout(() => {
                          this.setState({ loading: false });
                          this.setState({ viewType: "step3" });
                        }, 1000);
                      }}
                    >
                      {each}
                    </button>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        );
      case "step3":
        return (
          <div className="chat-conversion">
            <div className="chatQuestion">{name1 && <p>{name1}</p>}</div>
            <div className="chatButton">
              <ul>
                {/* <li>
                  <button>Less than one day</button>
                </li> */}
                {options1.map(each => (
                  <li>
                    <button
                      type="button"
                      className={
                        this.state.text == each ? "dummy active" : "dummy"
                      }
                      onClick={() => {
                        this.setState({ text: each });
                        this.setState({ loading: true });
                        setTimeout(() => {
                          this.setState({ loading: false });
                          this.setState({ viewType: "step4" });
                        }, 1000);
                      }}
                    >
                      {each}
                    </button>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        );
        break;
      case "step4":
        return (
          <div className="chat-conversion">
            <div className="chatQuestion">
              {" "}
              <p>Let's start with the symptom that's troubling you the most.</p>
            </div>
            <div className="chatButton">
              <input
                type="text"
                name="selectedSymptoms"
                value={this.state.selectedSymptoms}
                placeholder="eg:Headache"
                onChange={this.handleFilter}
              />
            </div>
            {this.state.filterSymptoms.length != 0 &&
              !this.state.symtomDescriptionView && (
                <div className="searchList">
                  <ul>
                    {this.state.filterSymptoms.map(each => {
                      return (
                        <li
                          style={{ cursor: "pointer" }}
                          key={each.symptomsId}
                          onClick={() => {
                            this.setState(
                              {
                                selectedSymptoms: each.symptomsName,
                                symtomDescriptionView: true
                              },
                              () => {
                                sessionStorage.setItem(
                                  "symptoms",
                                  this.state.selectedSymptoms
                                );
                              }
                            );
                          }}
                        >
                          {each.symptomsName}
                        </li>
                      );
                    })}

                    {/* <li>result2</li> */}
                  </ul>
                </div>
              )}
            <div className="chatresultsection">
              {this.state.symtomDescriptionView && (
                <div className="chatresult">
                  <h4>{this.state.selectedSymptoms}</h4>
                  <p>Lorem ipsom lorem ipsome</p>
                  <div className="chatButton">
                    <ul>
                      <li>
                        {" "}
                        <button
                          className={
                            this.state.text == this.state.selectedSymptoms
                              ? "dummy active"
                              : "dummy"
                          }
                          onClick={() => {
                            this.setState({
                              text: this.state.selectedSymptoms
                            });
                            this.setState({ loading: true });
                            setTimeout(() => {
                              this.setState({ loading: false });
                              let newArray = this.state.symptomsTrack.slice();
                              newArray.push(this.state.selectedSymptoms);
                              this.setState({
                                viewType: "step5",
                                symptomsTrack: newArray
                              });
                            }, 1000);
                          }}
                        >
                          Select Symptoms
                        </button>
                      </li>
                    </ul>
                  </div>
                </div>
              )}
            </div>
          </div>
        );
        break;
      case "step5":
        return (
          <QuestionAnswer
            changeSelectedSymptom={(name, value) =>
              this.changeSelectedSymptom(name, value)
            }
            selectedSymptoms={this.state.selectedSymptoms}
            selectedOption={this.state.selectedOption}
            question={this.state.question}
            loading={this.state.loading}
            loadTrue={() => this.loadingActive()}
            loadFalse={() => this.loadingDeActive()}
            parentId={this.state.parentId}
            assessmentId={this.state.assessmentId}
            symptoms={this.state.symptoms}
          />
        );
    }
  };

  handleBack = e => {
    const { symptomsTrack } = this.state;
    let newarray = symptomsTrack.slice();
    this.setState(
      {
        symptomsTrack: newarray
      },
      () => {
        this.setState(
          {
            selectedSymptoms: symptomsTrack[symptomsTrack.length - 2]
          },
          () => {
            newarray.pop();
            this.setState({ symptomsTrack: newarray });
          }
        );
      }
    );
  };

  changeSelectedSymptom = (each, value) => {
    const name = each + " " + value;
    let newArray = this.state.symptomsTrack.slice();
    newArray.push(name);
    this.setState({
      selectedSymptoms: name,
      symptomsTrack: newArray,
      question: each,
      selectedOption: value
    });
  };

  render() {
    const { handleChatbot, open } = this.props;
    console.log("this.state", this.state)
    return (
      <div
        className={open ? "chat-wrap d-block" : "chat-wrap d-none"}
        id="chatBody"
      >
        {this.state.loading && (
          <div className="chatLoader">
            <Loader
              type="ThreeDots"
              color="#f30a0a"
              height="10"
              width="100"
              center
            />
          </div>
        )}

        <div className="chat-head">
          {this.state.symptomsTrack.length > 1 && (
            <button
              type="button"
              class="chat_back"
              onClick={() => this.handleBack()}
            >
              <i class="fas fa-chevron-left" />
            </button>
          )}
          <img src="../images/logo.jpg" />
          {/* {this.state.viewType == "step4" && <button onClick={this.handleBack}>back</button>} */}
          <button
            type="button"
            className="chat_close"
            onClick={this.props.handleChatbot}
          >
            <i id="cls-chat" className="fa fa-times" aria-hidden="true" />
          </button>
        </div>
        {this.switchCaseRender()}
      </div>
    );
  }
}
export default ChatBot1;
