import React, { Component } from "react";


class ViewMedical extends Component {

    render() {
        return (
            <div class="mainMenuSide" id="main-content">
                <div class="wrapper">
                    <div class="container-fluid">
                        <div class="body-content">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="doSection card">
                                        <div class="row">

                                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                                <div class="doctorImage">
                                                    <img src="../images/adult-doctor-girl-355934.jpg" /></div>

                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">

                                                <div class="DocDetails">
                                                    <iframe src="pdf/DoctorDoc.pdf" class="pdfobject-container"></iframe>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }

}
export default ViewMedical;