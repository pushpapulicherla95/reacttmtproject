import React from "react";
import { NavLink  } from 'react-router-dom';

class LandingHeader extends React.Component{
    render(){
        return(
            <header>
            {/* <MultipleDatePicker
              onSubmit={dates => console.log("selected date", dates)}
            /> */}
            <nav class="navbar navbar-expand-md navbg">
              <div class="container">
                <a href="#" class="navbar-brand">
                  <img src={require("../../images/logo.jpg")}/>
                </a>
                {/* <!-- <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
  <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
  <div class="navbar-nav ml-auto"> 
      <a href="#" class="nav-item nav-link">Home</a> 
        <a href="#" class="nav-item nav-link">About Us</a> 
       <a href="#" class="nav-item nav-link">Our Services</a> 
        <a href="#" class="nav-item nav-link">For Partners</a> 
        <a href="#" class="nav-item nav-link">Chat with a doctor</a>
   </div> 
   <div class="navbar-nav ml-auto">		<a href="" class="nav-item nav-link" data-toggle="modal" data-target="#myModal">Login / Register</a></div> 
  </div> --> */}
                <div class="loginInfoandLang">
                  <div class="navbar-nav">
                    {/* <!-- <a href="login.html" class="nav-item nav-link" >Login</a> --> */}
  
                    <div class="dropdown float-right mr-2">
                      <a class="dropdown-toggle" data-toggle="dropdown">
                        Login
                      </a>
  
                      <div class="dropdown-menu">
                        <NavLink to="/login/patient" class="dropdown-item" >
                          <i class="fas fa-user-plus" />
                          <label class="head-menu">Patient</label>
                        </NavLink>
                        <NavLink
                          class="dropdown-item"
                          to="/login/healthcareprovider"
                        >
                          <i class="fas fa-user-md" />
                          <label class="head-menu">
                            Health care provider
                          </label>
                        </NavLink>
                      </div>
                    </div>
                  </div>
                  <div class="navbar-nav">
                    <a href="/register/patient" class="nav-item nav-link">
                      Register
                    </a>
                  </div>
                  <div class="languageSel">
                    <span>Language :</span>
                    <select class="">
                      <option>EN</option>
                    </select>
                  </div>
                </div>
              </div>
            </nav>
          </header>
        )
    }
}

export default LandingHeader; 