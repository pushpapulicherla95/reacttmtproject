import URL from "../../asset/configUrl";
import axios from "axios";
import actionType from "../upload/actionType";

export const getUploadFiles = payload => dispatch => {
  const config = {
    method: "post",
    url: URL.GET_UPLOADFILES,
    data: payload,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(config)
    .then(response => {
      console.log("upoad action ", response)
      dispatch({
        type: actionType.UPLOADFILES_SUCCESS,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch({
        type: actionType.UPLOADFILES_FAILURE
        // error:error.response.data
      });
    });
};


export const handleFileDelete = (id, fileUrl, type) => dispatch => {
  const login = JSON.parse(sessionStorage.getItem("loginDetails"))
  
  const uploadFiles = {
    uid: login.userInfo.userId,
    category: type
  };
  const config = {
    method: "post",
    url: URL.DELETE_PERSONALDATA_UPLOADFILES,
    data: { "id": id, "fileUrl": fileUrl },
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(config)
    .then(response => {
      dispatch({
        type: actionType.UPLOADFILES_DELETE_SUCCESS,
        payload: response.data
      });
     dispatch(getUploadFiles(uploadFiles))
    })
    .catch(error => {
      dispatch({
        type: actionType.UPLOADFILES_DELETE_FAILURE
        // error:error.response.data
      });
    });
};

export const handleHealthDataDelete = (id, fileUrl, type) => dispatch => {
  const login = JSON.parse(sessionStorage.getItem("loginDetails"))
  
  const uploadFiles = {
    uid: login.userInfo.userId,
    category: type
  };
  const config = {
    method: "post",
    url: URL.DELETE_HEALTHREDATA_UPLOADFILES,
    data: { "id": id, "fileUrl": fileUrl },
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(config)
    .then(response => {
      dispatch({
        type: actionType.UPLOADFILES_DELETE_SUCCESS,
        payload: response.data
      });
     dispatch(getUploadFiles(uploadFiles))
    })
    .catch(error => {
      dispatch({
        type: actionType.UPLOADFILES_DELETE_FAILURE
        // error:error.response.data
      });
    });
};