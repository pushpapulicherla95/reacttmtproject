import React, { Component } from "react";
import { doctormanageAds, deleteDocManagead, updateDocManagead, getDocManageadList } from "../../service/doctor/action";
import { connect } from "react-redux";
import AWS from "aws-sdk";
import validator from "validator";

class Manageads extends Component {

    constructor(props) {
        super(props)
        this.state = {
            addNewAds: false,
            healthCareProviderId: props.loginDetails ? props.loginDetails.userInfo.userId : "",
            title: "",
            symptoms: "",
            description: "",
            webLinks: "",
            fileName: "",
            fileType: "",
            fileURL: "",

            manageAdsList: "",
            updateAdd: true,
            updateid: "",
            errors: {
                title: "",
                symptoms: "",
                description: "",
                webLinks: "",
                fileURL: "",

            },
            titleValid: false,
            symptomsValid: false,
            fileURLValid: false,
            descriptionValid: false,
            webLinksValid: false,

        }
    }

    componentWillMount() {
        let listinfo = {
            uid: this.state.healthCareProviderId
        }
        this.props.getDocManageadList(listinfo);
    }
    componentDidMount() {
     
    }
    componentWillReceiveProps(nextProps) {
        if ( (nextProps.doctorManageAddList != this.props.doctorManageAddList) && (nextProps.doctorManageAddList && nextProps.doctorManageAddList.manageAdsList)) {
            const manageAdsList = nextProps.doctorManageAddList.manageAdsList
            const updatedManageList =  manageAdsList.map((value)=>{
                const newObject = Object.assign({},value)
                this.getImagePath(value.fileUrl).then((data)=>{
                    newObject.imgSrc = data
                }) 
                return newObject
            })
            this.setState({ manageAdsList: manageAdsList },()=>{
                setTimeout(()=>{
                    this.setState({ manageAdsList: updatedManageList })
                },600)
            })

        }

    }
    openModalPop = () => {
        this.setState({ addNewAds: true, updateAdd: false }, () => {
            this.emptystate();
        });
    };

    closeModalPop = () => {
        this.setState({ addNewAds: false, updateAdd: false }, () => {
            this.emptystate();
        })

    }

    handleChange = (e) => {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value }, () => {
            this.validateField(name, value);
        });
    };

    handleChangePopup = e => {
        var reader = new FileReader();
        const name = e.target.name;

        var value = e.target.files[0];

        reader.onload = () => {
            this.setState({
                fileURL: reader.result,
                fileName: value.name,
                fileType: value.type
            }, () => {
                this.validateField(name, value);
            });
        };
        reader.readAsDataURL(value);
    };

    submitAdd = (e) => {
        e.preventDefault();
        const { title, symptoms, description, webLinks, healthCareProviderId } = this.state;
        var phrase = this.state.fileURL;
        var myRegexp = /base64,(.*)/;
        var match = myRegexp.exec(phrase);
        const saveInfo = {
            healthCareProviderId: healthCareProviderId,
            typeInfo: "2",
            title: title,
            symptoms: symptoms,
            description: description,
            webLinks: webLinks,
            fileUrl: match[1],
            fileName: this.state.fileName,
            fileType: this.state.fileType
        }
        this.props.doctormanageAds(saveInfo);
        this.setState({
            title: "",
            symptoms: "",
            description: "",
            webLinks: "",
            fileURL: "",
            addNewAds: false
        })
    }
    emptystate = () => {
        this.setState({
            fileURL: "",
            title: "",
            symptoms: "",
            description: "",
            webLinks: "",
        })
    }
    //
    validateField(fieldName, value) {
        // titleValid: false,
        // symptomsValid: false,
        // descriptionValid: false,
        // webLinksValid: false,
        const { errors, titleValid, symptomsValid, descriptionValid, webLinksValid, fileURLValid } = this.state;
        let titlevalid = titleValid;
        let symptomsvalid = symptomsValid;
        let descriptionvalid = descriptionValid;
        let webLinksvalid = webLinksValid;
        let fileURLvalid = fileURLValid;

        let fieldValidationErrors = errors;
        switch (fieldName) {
            case "title":
                titlevalid = !(value.length < 3 || value.length > 20);
                fieldValidationErrors.title = titlevalid
                    ? ""
                    : "Title should be 3 to 20 characters";
                break;
            case "symptoms":
                symptomsvalid = !(value.length < 3 || value.length > 20);
                fieldValidationErrors.symptoms = symptomsvalid
                    ? ""
                    : "Symptoms should be 3 to 20 characters";
                break;
            case "description":
                descriptionvalid = !(value.length < 3 || value.length > 20);
                fieldValidationErrors.description = descriptionvalid
                    ? ""
                    : "Description should be 3 to 20 characters";
                break;
            case "fileURL":
                // fileURLvalid = value.length ?true:false 
                fileURLvalid = value.size > 50000 && value.size <= 1000000
                fieldValidationErrors.fileURL = fileURLvalid
                    ? ""
                    : "File size should be less than 1MB.";
                break;
            case "webLinks":
                webLinksvalid = value.match(
                    /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/
                );
                fieldValidationErrors.webLinks = webLinksvalid
                    ? ""
                    : "WebLinks should be proper.";
                break;

        }
        this.setState({
            errors: fieldValidationErrors,
            titleValid: titlevalid,
            symptomsValid: symptomsvalid,
            descriptionValid: descriptionvalid,
            webLinksValid: webLinksvalid,
            fileURLValid: fileURLvalid,

        },
            this.validateForm
        )
    }
    //
    validateForm() {
        const {
            titleValid, symptomsValid, descriptionValid, webLinksValid, fileURLValid
        } = this.state;
        this.setState({
            formValid:
                titleValid &&
                symptomsValid &&
                descriptionValid &&
                fileURLValid &&
                webLinksValid,

        });

    }

    //
    deleteAdd = (value) => {
        let deleteInfo = {
            id: value.id,
            uid: this.state.healthCareProviderId
        }
        this.props.deleteDocManagead(deleteInfo);
    }
    handleViewManageAdd = (viewdata) => {
        this.setState({
            title: viewdata.title,
            symptoms: viewdata.symptoms,
            description: viewdata.description,
            webLinks: viewdata.webLinks,
            fileURL: viewdata.imgSrc,
            addNewAds: true,
            updateid: viewdata.id,
            updateAdd: true
        })

    }
    handleUpdateAdd = (e) => {                         
        e.preventDefault();
        const { title, symptoms, description, webLinks, healthCareProviderId, updateid, fileURL } = this.state;
        let editInfo = {
            healthCareProviderId: healthCareProviderId,
            typeInfo: "2",
            id: updateid,
            fileURL: "jjj",
            title: title,
            symptoms: symptoms,
            description: description,
            webLinks: webLinks
        }
        this.props.updateDocManagead(editInfo);
        this.setState({
            addNewAds: false
        })
    }
    getImagePath = async (filename) => {
      const imageUrl = await new Promise((resolve,reject)=>{
            AWS.config.update({
                accessKeyId: process.env.REACT_APP_ACCESSKEY,
                secretAccessKey: process.env.REACT_APP_SECRETACCESSKEY,
                region: process.env.REACT_APP_REGION
            });
            var s3 = new AWS.S3();
            var data = {
                Bucket: "tomato-medical-bucket",
                Key: "manageads/"+filename
            };
    
            s3.getSignedUrl("getObject", data, async (err, data) => {
                if (err) {
                    console.log("Error uploading data: ", err);
                } else {
                    console.log("succesfully uploaded the image!", data);
                    resolve(data)
                    // (await data) && this.setState({ img: data });
                }
            });
        })
     
      return imageUrl
    }
    render() {
        const { title, symptoms, description, webLinks, fileURL, manageAdsList, errors, formValid } = this.state;
        return (
            <React.Fragment>
                <div class="mainMenuSide" id="main-content">
                    <div class="wrapper">
                        <div class="container-fluid">
                            <div class="body-content">

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 col-sm-12 col-6 padRig">
                                        <div class="adSection p-3">
                                            <a class="addNewAds" href="javascript:void(0);" onClick={this.openModalPop}>
                                                <i class="fas fa-plus-circle"></i>
                                            </a>
                                        </div>
                                    </div>
                                    {manageAdsList.length > 0 ? manageAdsList.map((each, i) => (
                                        <div class="col-lg-3 col-md-4 col-sm-12 col-6 padLeft">
                                            <div class="adSection">
                                                <img src={each.imgSrc && each.imgSrc} id="imagePreview" />
                                                <div onClick={() => this.deleteAdd(each)} >

                                                    <label for="imageUpload" id="upBtn">
                                                        <i class="far fa-trash-alt"></i>
                                                    </label></div>
                                                <div class="Upload-Ad-Btn">

                                                    <button class="adPost" data-target="#myModal" data-toggle="modal" onClick={() => this.handleViewManageAdd(each)}>View</button>
                                                </div>
                                            </div>
                                        </div>
                                    )) : ""
                                    }



                                </div>



                            </div>
                        </div>
                    </div>

                </div>
                {this.state.addNewAds &&
                    <div class="modal d-block show" >
                        <div class="modal-dialog">
                            <div class="modal-content modalPopUp">
                                <div class="modal-header borderNone">
                                    <h5 class="modal-title">Manage Ads <sup>*</sup></h5>
                                    <button type="button" class="popupClose" onClick={this.closeModalPop}><i class="fas fa-times"></i></button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-sm-12">
                                                <div class="imageUploadSection mb-2" >
                                                    <input
                                                        type="file" name="fileURL"
                                                        onChange={this.handleChangePopup}
                                                        className="hide_file_input"
                                                    />
                                                    <img src={fileURL ? fileURL : "images/upimage.png"} />
                                                    <div style={{ color: "red" }}>{errors.fileURL}</div>
                                                </div>

                                            </div>
                                            <div class="col-md-12 col-lg-12 col-sm-12">
                                                <div class="form-group">
                                                    <label class="commonLabel clr1002612">Add Title <sup>*</sup></label>
                                                    <input type="text" class="form-control" name="title" value={title} onChange={this.handleChange} />
                                                    <div style={{ color: "red" }}>{errors.title}</div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-lg-12 col-sm-12">
                                                <div class="form-group">
                                                    <label class="commonLabel clr1002612">Select Symptoms <sup>*</sup></label>
                                                    <select class="form-control" name="symptoms" value={symptoms} onChange={this.handleChange}>
                                                        <option>Select Symptoms</option>
                                                        <option value="fever">fever</option>
                                                        <option value="headache">headache</option>
                                                    </select>
                                                    <div style={{ color: "red" }}>{errors.symptoms}</div>

                                                </div>
                                            </div>
                                            <div class="col-md-12 col-lg-12 col-sm-12">
                                                <div class="form-group">
                                                    <label class="commonLabel clr1002612 ">Add Description <sup>*</sup></label>
                                                    <textarea class="form-control" name="description" value={description} onChange={this.handleChange}></textarea>
                                                    <div style={{ color: "red" }}>{errors.description}</div>

                                                </div>
                                            </div>

                                            <div class="col-md-12 col-lg-12 col-sm-12">
                                                <div class="form-group">
                                                    <label class="commonLabel clr1002612" >Web links <sup>*</sup></label>
                                                    <input type="text" class="form-control" name="webLinks" value={webLinks} onChange={this.handleChange} />
                                                    <div style={{ color: "red" }}>{errors.webLinks}</div>

                                                </div>
                                            </div> <div class="col-md-12 col-lg-12 col-sm-12 mt-3 text-center">
                                                {!this.state.updateAdd && <button type="button" class="commonBtn" onClick={this.submitAdd} disabled={!formValid} >Publish</button>}
                                                {this.state.updateAdd && <button type="button" class="commonBtn" onClick={this.handleUpdateAdd} >Update</button>}
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </React.Fragment>
        )
    }

}

const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails,
    doctorManageAddList: state.doctorReducer.doctorManageAddList
})

const mapDispatchToprops = dispatch => ({
    doctormanageAds: saveinfo => dispatch(doctormanageAds(saveinfo)),
    deleteDocManagead: deleteinfo => dispatch(deleteDocManagead(deleteinfo)),
    getDocManageadList: listinfo => dispatch(getDocManageadList(listinfo)),
    updateDocManagead: editinfo => dispatch(updateDocManagead(editinfo))

})
export default connect(
    mapStateToProps,
    mapDispatchToprops
)(Manageads);