import React, { Component } from "react";
import { connect } from "react-redux";
import { viewProfile, editProfile } from "../../service/dashboard/action";
import axios from "axios";

class Viewprofile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      mobileNo: "",
      address: "",
      dateOfBirth: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange = e => {
    let name = e.target.name;
    let value = e.target.value;
    this.setState({
      [name]: value
    });
  };
  handleSubmit(e) {
    e.preventDefault();

    let editinfo = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      mobileNo: this.state.mobileNo,
      address: this.state.address,
      dateOfBirth: this.state.dateOfBirth
    };

    this.props.editProfile(editinfo);
  }

  componentWillMount() {
    const profileinfo = {
      email: "colan@gmail.com"
    };

    this.props.viewProfile(profileinfo);
  }
  render() {
    const {
      firstName,
      lastName,
      email,
      mobileNo,
      address,
      dateOfBirth
    } = this.props.viewprofileDetails;
    return (
      <div className="modal-content">
        <div className="modal-header">
          <h4 className="modal-title">Edit </h4>
          <button type="button" className="close" data-dismiss="modal">
            &times;
          </button>
        </div>
        <div className="modal-body">
          <div className="form-group">
            <label className="form-label">Frist name</label>
            <input
              type="text"
              className="form-control"
              placeholder="Enter your Email"
              name="firstName"
              onChange={this.handleChange}
              defaultValue={firstName}
            />
          </div>
          <div className="form-group">
            <label className="form-label">last name</label>
            <input
              className="form-control"
              placeholder="Enter your Email"
              name="lastName"
              defaultValue={lastName}
              onChange={this.handleChange}
              type="text"
            />
          </div>
          <div className="form-group">
            <label className="form-label">Email</label>
            <input
              className="form-control"
              placeholder="Enter your Email"
              name="email"
              defaultValue={email}
              onChange={this.handleChange}
              type="text"
            />
          </div>
          <div className="form-group">
            <label className="form-label">Mobile</label>
            <input
              className="form-control"
              placeholder="Enter your Email"
              name="mobileNo"
              onChange={this.handleChange}
              defaultValue={mobileNo}
              type="text"
            />
          </div>

          <div className="form-group">
            <label className="form-label">Address</label>
            <input
              className="form-control"
              placeholder="Enter your Email"
              name="address"
              defaultValue={address}
              onChange={this.handleChange}
              type="text"
            />
          </div>

          <div className="form-group">
            <label className="form-label">Date of brith</label>
            <input
              className="form-control"
              placeholder="Enter your Email"
              name="address"
              defaultValue={dateOfBirth}
              onChange={this.handleChange}
              type="text"
            />
          </div>
        </div>
        <div className="modal-footer">
          <button
            type="button"
            className="btn commonBtn"
            onClick={this.handleSubmit}
          >
            Close
          </button>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  viewprofileDetails: state.dashboardReducer.viewprofileDetails,
  editprofileDetails: state.dashboardReducer.editprofileDetails
});

const mapDispatchToProps = dispatch => ({
  viewProfile: profileinfo => dispatch(viewProfile(profileinfo)),
  editProfile: editinfo => dispatch(editProfile(editinfo))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Viewprofile);
