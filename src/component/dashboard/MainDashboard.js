import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { NavLink } from "react-router-dom";
import URL from "../../asset/configUrl";
import axios from "axios";
import AWS from "aws-sdk";
import { symptomMainList } from "../../service/admin/TextRobo/action";
import { changepsw } from "../../service/login/action"
import { uploadProfilepic } from "../../service/hospital/action";
import validator from "validator";

class MainDashboard extends Component {
  constructor(props) {
    super(props);
    var loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
    const { userInfo } = loginDetails;
    var userId = userInfo.userId || "";
    this.state = {
      qrCode: null,
      allSymptomList: [],
      selectedSymptomData: {},
      userId: userId,
      qrCode: null,
      tabone: true,
      tabtwo: false,
      tabthree: false,
      fileUrl: "",
      fileName: "",
      fileType: "",
      openModel: false,
      profilePic: userInfo.profilePic,
      currentpassword: "",
      password: "",
      confirmpassword: "",
      errors: {
        password: "",
        confirmpassword: "",
        currentpassword: ""
      },
      passwordValid: false,
      confirmpasswordValid: false,
      currentpasswordValid: false,
      formValid: false,
      randomNumber: Math.random(),
      fileImgUrl: "",
      imageHash: Date.now()

    };
  }

  validateQrcode = () => {
    const { userInfo } = JSON.parse(sessionStorage.getItem("loginDetails"));
    axios.get(URL.VALID_QRCODE + userInfo.userId).then(response => {
      if (response.data.message === "valid QRcode") {
        this.setState({ qrCode: URL.BUCKETURL + "QRCode/" + userInfo.userId + "_qrcode.png" });
      }
    });
  };

  handleLogout = () => {

    sessionStorage.clear("loginDetails");
    this.props.removeProfilePicInfo({ type: "UPLOAD_PROFILEPIC_SUCCESS", payload: "" })
    this.props.history.push("/");
  };
  onSymptomInputChange = (e) => {

    const { symptomMainListData } = this.props;
    const val = e.target.value;
    this.setState({ symptomName: e.target.value }, () => {
      const upDatedallListSymptom = symptomMainListData.filter((each) => {
        return each.symptomsName && each.symptomsName.toLowerCase().includes(val && val.toLowerCase());
      })
      this.setState({ allSymptomList: val ? upDatedallListSymptom : [] })
    })
  }

  componentWillMount() {
    this.props.symptomMainList()
    AWS.config.update({
      accessKeyId: process.env.REACT_APP_ACCESSKEY,
      secretAccessKey: process.env.REACT_APP_SECRETACCESSKEY,
      region: process.env.REACT_APP_REGION
    });
    const { profilePic } = this.state;
    if (this.props.profilePicInfo) {
      this.setState({ profilePic: this.props.profilePicInfo.profilePic })
      this.getprofileImage(this.props.profilePicInfo.profilePic);

    } else {
      this.getprofileImage(profilePic);

    }
    const { userInfo } = this.props.loginDetails;
    this.setState({ fileImgUrl: URL.BUCKETURL + `profilepicture/${userInfo.userId}_.png?${this.state.imageHash}` })
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.profilePicInfo != nextProps.profilePicInfo) {
      const { profilePic } = nextProps.profilePicInfo;
      console.log("nextProps", nextProps);
      // this.setState({ profilePic: nextProps.profilePicInfo.profilePic });
      console.log("profilePic", profilePic);
      const { userInfo } = this.props.loginDetails;
      this.setState({ fileImgUrl: "", imageHash: Date.now() }, () => {
        setTimeout(() => {
          this.setState({ fileImgUrl: URL.BUCKETURL + `profilepicture/${userInfo.userId}_.png?${this.state.imageHash}` })
        }, 3000)

      })
      this.getprofileImage(profilePic);
    }
  }

  getprofileImage = (profilePic) => {
    var data = URL.BUCKETURL + `profilepicture/${profilePic}`;
    this.setState({ fileUrl: data });
    // this.validateQrcode();
    console.log("fileUrl", this.state.fileUrl);
  }

  componentWillMount = () => {
    this.validateQrcode();
  }

  generateQRcode = () => {
    const { userInfo } = JSON.parse(sessionStorage.getItem("loginDetails"));
    const configs = {
      method: "post",
      url: URL.GENERATE_QRCODE,
      data: { uid: userInfo.userId },
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };
    axios(configs)
      .then(response => {

        if (response.data.status == "success") {

          this.setState({ qrCode: URL.BUCKETURL + "QRCode/" + userInfo.userId + "_qrcode.png" });

        }
      })
      .catch(error => {
        console.log("errr", error);
      });
  };



  tabOne = () => {
    this.setState({
      tabone: true,
      tabtwo: false,
      tabthree: false
    });
  };

  tabTwo = () => {
    this.setState({
      tabone: false,
      tabtwo: true,
      tabthree: false
    });
  };

  tabThree = () => {
    this.setState({
      tabone: false,
      tabtwo: false,
      tabthree: true
    });
  };
  handleChangePopup = e => {
    var reader = new FileReader();

    var value = e.target.files[0];
    let name = value.name.split('.')[0]
    let type = "." + value.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileUrl: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(value);
  }
  submitUpload = (e) => {
    e.preventDefault();
    const { fileType, fileUrl, userId, fileName, profilePic } = this.state;
    var phrase = this.state.fileUrl;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);

    const filedata = {
      uid: userId,
      fileType: fileType,
      fileName: fileName,
      fileUrl: match[1],
    }
    this.props.uploadProfilepic(filedata);
    this.getprofileImage(profilePic);
    this.setState({
      openModel: false,
      fileUrl: "",
      fileName: "",
      fileType: "",
    })

  }
  modalPopClose = () => {
    this.setState({ openModel: !this.state.openModel });
  }
  openModelPop = () => {
    this.setState({ openModel: true });

  }
  //
  changePassword = () => {

    const { email, password, currentpassword, confirmpassword } = this.state;
    const { userInfo } = this.props.loginDetails;
    let changepswinfo = {
      email: userInfo.email,

      password: password,

    };

    this.props.changepsw(changepswinfo);
    this.setState({
      openModel: false,
      currentpassword: "",
      password: "",
      confirmpassword: "",
    })

  }
  handleChange = (e) => {
    e.preventDefault();
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };
  //Validation
  validateField(fieldName, value) {
    const { errors, passwordValid, confirmpasswordValid, currentpasswordValid, confirmpassword, password } = this.state;

    let passwordvalid = passwordValid;
    let currentpasswordvalid = currentpasswordValid;
    let confirmpasswordvalid = confirmpasswordValid;
    let fieldValidationErrors = errors;
    switch (fieldName) {

      case "currentpassword":
        currentpasswordvalid = !(value.length < 3 || value.length > 20);
        fieldValidationErrors.currentpassword = currentpasswordvalid
          ? ""
          : "Password should be 3 to 20 characters";
        break;

      case "password":
        passwordvalid = value.length !== 0
        fieldValidationErrors.password = passwordvalid
          ? ""
          : "Please enter valid password.";
        break;

      // case "password":
      // passwordvalid =
      //   value.length >= 8 &&
      //   value.match(
      //     /(?=.*[_!@#$%^&*-])(?=.*\d)(?!.*[.\n])(?=.*[a-z])(?=.*[A-Z])^.{8,}$/
      //   );
      // fieldValidationErrors.password = passwordvalid
      //   ? ""
      //   : "Must contain at least one uppercase letter, one lowercase letter, one number, one special character and a minimum of 8 characters";
      // break;
      case "confirmpassword":
        confirmpasswordvalid = validator.equals(value, password);
        fieldValidationErrors.confirmpassword = confirmpasswordvalid
          ? ""
          : "Password does not match";
        break;
    }
    if (fieldName === "password") {
      if (confirmpassword !== "") {
        if (value !== confirmpassword) {
          fieldValidationErrors.confirmpassword = "Password does not match.";
        } else {
          fieldValidationErrors.confirmpassword = "";
        }
      }
    } else if (fieldName === "confirmpassword") {
      if (value !== password) {
        fieldValidationErrors.confirmpassword = "Password does not match.";
      } else {
        fieldValidationErrors.confirmpassword = "";
      }
    }
    this.setState({
      errors: fieldValidationErrors,
      passwordValid: passwordvalid,
      confirmpasswordValid: confirmpasswordvalid,
      currentpasswordValid: currentpasswordvalid
    },
      this.validateForm
    )
  }
  //
  validateForm() {
    const {
      passwordValid, confirmpasswordValid, currentpasswordValid
    } = this.state;
    this.setState({
      formValid:
        currentpasswordValid &&
        passwordValid &&
        confirmpasswordValid,




    });

  }

  render() {
    const { userInfo } = this.props.loginDetails;

    const { tabone, tabtwo, tabthree, userId, openModel, profilePic, fileUrl, currentpassword, password, errors, formValid, fileImgUrl } = this.state;
    console.log("state", this.state)
    return (
      <React.Fragment>
        {this.state.randomNumber && <div className="inner-pageNew">
          <header className="header">
            <div className="brand">
              <a href="/" className="logo">
                <img src="images/logo.jpg" />{" "}
              </a>
            </div>
            <div className="top-nav">
              {/* <div className="searchBox float-right " >
                <input type="text" className="searchTxt" placeholder="Search Symptom Checker" />
                <a href="#" className="searchBtn">
                  <i className="fas fa-search"></i>
                </a>
              </div> */}

              <div className="languageSel text-right mb-2">
                <span>Language :</span>
                <select className="">
                  <option>EN</option>
                </select>
              </div>
            </div>
          </header>

          <div className="body-content">
            <div className="container-fluid">
              <div className="row">
                <div className="col-lg-3 col-md-3 col-sm-12 col-12 bg-purple-card">
                  <div className="card user-profile-card">
                    <div className="row">
                      <div className="col-6">
                        <a href="javaScript:void(0);" onClick={this.openModelPop}

                        >
                          {/* <img src={`https://data.tomatomedical.com/profilepicture/${userInfo.userId}_.png`}/> */}
                          <img src={fileImgUrl} />
                          {/* <img src= {userInfo && userInfo.profilePic != "" && userInfo.profilePic
                        } /> */}

                          {/* <img src={this.state.fileUrl ? this.state.fileUrl :"../images/no-profile-pic-icon-5.png" }  /> */}
                        </a>
                        {userInfo && userInfo.firstName != "" && (
                          <h5>{userInfo.firstName}</h5>
                        )}
                        {userInfo && userInfo.firstName == "patient" && (
                          <p>Patient</p>
                        )}
                      </div>
                      <div className="col-6">
                        <div className="QRcode">
                          {/* <a
                            rel="nofollow"
                            href=""
                            border="0"
                            style={{ cursor: "default" }}
                          > */}
                          <img
                            src={
                              this.state.qrCode == null
                                ? "images/Dummy-Qr.png"
                                : this.state.qrCode
                            }
                            className="QRIcon_Sec"
                            onClick={() => this.generateQRcode()}
                          />
                          {/* </a> */}
                        </div>
                      </div>
                    </div>

                    <div className="set-section">
                      <ul>
                        <li>
                          <a href="javaScript:void(0);" onClick={this.openModelPop}
                            data-toggle="modal" data-target="#settingModal"
                          >
                            <i className="fas fa-cog" />
                          </a>
                        </li>
                        <li>
                          <a
                            href="javaScript:void(0);"
                            onClick={() =>
                              this.props.history.push({
                                pathname: "/subscription"
                              })
                            }
                          >
                            <i className="fas fa-shopping-cart" />
                          </a>
                        </li>
                        <li>
                          <a
                            href="javaScript:void(0);"
                            onClick={this.handleLogout}
                          >
                            <i className="fas fa-power-off" />
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-12 mobileSearch large-screen ">
                    <input type="text" type="text"
                      class="form-control"
                      placeholder="Search Symptom Checker"
                      onChange={(e) => {
                        this.onSymptomInputChange(e)
                      }}
                      value={this.state.symptomName && this.state.symptomName} placeholder="Search Symptom Checker" />
                    {this.state.allSymptomList.length != 0 && <div className="searchResultSuggestion slightchangedashboard">
                      <ul>
                        {this.state.allSymptomList.map((each) =>
                          <li style={{ textAlign: "left" }} onClick={() => this.setState({ selectedSymptomData: each, symptomName: each.symptomsName, allSymptomList: [] }, () => {
                            this.props.history.push({ pathname: "/symptomcheckerQA", state: { ...this.state.selectedSymptomData } })
                          })}> {each.symptomsName}  </li>
                        )}
                      </ul>
                    </div>}
                    <div className="searchIcon">
                      <i className="fas fa-search" />
                    </div>
                  </div>
                </div>


                <div className="col-lg-9 col-md-9 col-sm-12 col-12">
                  <div className="row">
                    <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                      <a
                        href="javaScript:void(0);"
                        onClick={() =>
                          this.props.history.push({
                            pathname: "/submenu",
                            state: { submenuType: "Personal" }
                          })
                        }
                        className="card bg-c"
                      >
                        <div className="card-img1">
                          <img src="images/personal-color.png" />
                        </div>
                        <div className="card-title">
                          <h5>Personal</h5>
                        </div>
                      </a>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-6 col-6 padLeft">
                      <a
                        href="javaScript:void(0);"
                        onClick={() =>
                          this.props.history.push({
                            pathname: "/submenu",
                            state: { submenuType: "Telemedicine" }
                          })
                        }
                        className="card bg-c"
                      >
                        <div className="card-img1">
                          <img src="images/telemedicine-color.png" />
                        </div>
                        <div className="card-title">
                          <h5>Telemedicine</h5>
                        </div>
                      </a>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                      <a
                        href="javaScript:void(0);"
                        onClick={() =>
                          this.props.history.push({
                            pathname: "/submenu",
                            state: { submenuType: "HealthRecord" }
                          })
                        }
                        className="card bg-c"
                      >
                        <div className="card-img1">
                          <img src="images/health-color.png" />
                        </div>
                        <div className="card-title">
                          <h5>Health Records</h5>
                        </div>
                      </a>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-6 col-6 padLeft">
                      <NavLink to=" " className="card bg-c">
                        <div className="card-img1">
                          <img src="images/pharma-color.png" />
                        </div>
                        <div className="card-title">
                          <h5>Pharmacy / Supply</h5>
                        </div>
                      </NavLink>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                      <a
                        href="javaScript:void(0);"
                        onClick={() =>
                          this.props.history.push({
                            pathname: "/submenu",
                            state: { submenuType: "MedicalSpecialist" }
                          })
                        }
                        className="card bg-c"
                      >
                        <div className="card-img1">
                          <img src="../images/icon7-color.png" />
                        </div>
                        <div className="card-title">
                          <h5>Find Your Medical Specialist</h5>
                        </div>
                      </a>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-6 col-6 padLeft">
                      <NavLink to=" " className="card bg-c">
                        <div className="card-img1">
                          <img src="images/service-color.png" />
                        </div>
                        <div className="card-title">
                          <h5>Services</h5>
                        </div>
                      </NavLink>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                      <a href="javaScript:void(0);"
                        className="card bg-c" onClick={() =>
                          this.props.history.push({
                            pathname: "/submenu",
                            state: { submenuType: "shareData" }
                          })
                        }>
                        <div className="card-img1">
                          <img src="images/share-color.png" />
                        </div>
                        <div className="card-title">
                          <h5>Communicatore</h5>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {openModel && <div className="modal d-block show" >
            <div className="modal-dialog">
              <div className="modal-content modalPopUp">
                <div className="modal-header borderNone">
                  <h5 className="modal-title">Setting</h5>
                  <button type="button" className="popupClose" onClick={this.modalPopClose} ><i className="fas fa-times"></i></button>
                </div>
                <div className="modal-body">
                  <form>
                    <div className="tomCardBody">
                      <div className="tab-menu-content">

                        <div className="tab-menu-title">
                          <ul>
                            <li className={this.state.tabone ? "menu1 active" : ""}>
                              <a href="#" onClick={this.tabOne}>
                                <p>Profile</p>
                              </a></li>
                            <li className={this.state.tabtwo ? "menu1 active" : ""}>
                              <a href="#" onClick={this.tabTwo}>
                                <p>Change Password</p>
                              </a></li>
                            <li className={this.state.tabthree ? "menu1 active" : ""}>
                              <a href="#" onClick={this.tabThree}>
                                <p>Delete Account</p>
                              </a></li>
                          </ul>
                        </div>

                        <div className="tab-menu-content-section">
                          {tabone &&
                            <div id="pcontent-1" >

                              <div className="row">
                                <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                  <div className="form-group">
                                    <div className="profile_model_image">

                                      <img src={fileImgUrl} />
                                    </div>
                                  </div>
                                </div>
                                <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                  <div className="form-group">
                                    <label>Upload Profile {tabone} <sup>*</sup></label>
                                    <input type="file" className="form-control" name="fileUrl" onChange={this.handleChangePopup} />
                                  </div>
                                </div>

                              </div>
                              <div className="row">
                                <div className="col-12">
                                  <button type="button" className="commonBtn float-right" onClick={this.submitUpload}>Update</button></div>
                              </div>
                            </div>}
                          {tabtwo &&
                            <div id="pcontent-2"  >

                              <div className="row">
                                <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                  <div className="form-group">
                                    <label>Current Password <sup>*</sup></label>
                                    <input type="text" className="form-control" name="currentpassword" value={currentpassword} onChange={this.handleChange}
                                    />
                                  </div>
                                  <div style={{ color: "red" }}>{errors.currentpassword}</div>

                                </div>
                                <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                  <div className="form-group">
                                    <label>New Password <sup>*</sup></label>
                                    <input type="text" name="password" value={password} className="form-control" onChange={this.handleChange}
                                    />
                                  </div>
                                  <div style={{ color: "red" }}>{errors.password}</div>

                                </div>
                                <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                  <div className="form-group">
                                    <label>Confirm Password <sup>*</sup></label>
                                    <input type="text" name="confirmpassword" value={this.state.confirmpassword} className="form-control" onChange={this.handleChange} />
                                  </div>
                                  <div style={{ color: "red" }}>{errors.confirmpassword}</div>

                                </div>

                              </div>
                              <div className="row">
                                <div className="col-12">
                                  <button type="button" className="commonBtn float-right" onClick={this.changePassword} disabled={!formValid} >Update</button></div>
                              </div>
                            </div>
                          }

                          {tabthree &&


                            <div id="pcontent-3"  >

                              <div className="row">
                                <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                  <div className="form-group">
                                    <p className="mb-0">Are you sure you want to delete <b>Tomato Medical Account</b>?
                                                                        </p>
                                    <p>All the Records and Medical Document will be deleted and cannot be undone.</p>
                                  </div>
                                </div>



                              </div>
                              <div className="row">
                                <div className="col-12">
                                  <button type="button" className="commonBtn float-right">Delete</button></div>
                              </div>
                            </div>
                          }


                        </div>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
          }
        </div>}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  loginDetails: state.loginReducer.loginDetails,
  symptomMainListData: (state.textRoboReducer.symptomMainListData && state.textRoboReducer.symptomMainListData.symptomsList) || [],
  profilePicInfo: state.hospitalReducer.profilePicInfo,
  changeuserPswDetails: state.loginReducer.changeuserPswDetails
});

const mapDispatchToProps = dispatch => ({
  symptomMainList: () => dispatch(symptomMainList()),
  uploadProfilepic: (filedata) => dispatch(uploadProfilepic(filedata)),
  changepsw: (changepswinfo) => dispatch(changepsw(changepswinfo)),
  removeProfilePicInfo: () => dispatch({
    type: "UPLOAD_PROFILEPIC_SUCCESS",
    payload: ""
  })
});
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(MainDashboard)
);
