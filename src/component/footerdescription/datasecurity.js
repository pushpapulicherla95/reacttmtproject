import React , {Component} from 'react';
import { NavLink  } from 'react-router-dom';
import { withRouter } from "react-router";
import LandingHeader from '../common/LandingHeader';
class Datasecurity extends Component{
  componentDidMount() {
    window.scrollTo(0, 0)
}
    render(){
        return(
<div>	
<LandingHeader/>
<section class="datasec-banner about-banner banner d-flex justify-content-center align-items-center">
		<div class="container">
			<div class="bannerContent  flex-column">
				<h1>
					<span>Data Security </span>
				</h1>
			
			</div>
		</div>
	</section>
	<div class="emergencyAlert">
		<a href="" > <img src="images/emergencyCall.png"/> </a>
	</div>
<section class="about-content data_security_content">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-12 col-12">
				<img src="images/data-security-content.png"/>
				
			</div>
			<div class="col-lg-8 col-md-8 col-sm-12 col-12 d-flex justify-content-center flex-column">
				<p class="static_para">
					Data security means protecting digital data, such as those in a database, from destructive forces and from the unwanted actions of unauthorized users, such as a cyberattack or a data breach.
				</p>
				<p class="static_para">
					All app Data on the mobile device and in the cloud/back end are encrypted in the top of the line encryption standard AES-256. Data transfers use the SSL-protocol. All servers are based in Germany.
				</p>
			</div>
		</div>
		</div>	
</section>
<footer>
          <div class="footerLink">
            <div class="container">
              <ul>
                <li>
                  <NavLink to="/aboutus" >About </NavLink>
                </li>
                <li>
                <NavLink to="/partners" >For Partners </NavLink>
                </li>
                <li>
                <NavLink to="/contact" >Contact </NavLink>
                </li>
                <li>
                <NavLink to="/datasecurity" >Data Security </NavLink>
                </li>
                <li>
                <NavLink to="/privacypolicy" > Privacy Policy </NavLink>
                </li>
                <li>
                <NavLink to="/imprint">Imprint </NavLink>
                </li>
              </ul>
            </div>
          </div>

          <div class="footerCopyRights">
            <div class="container">
              <ul>
                <li>
                  <a href="">
                    <i class="fab fa-facebook-f" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-google-plus-g" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-instagram" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-twitter" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-linkedin-in" />
                  </a>
                </li>
              </ul>
              <p>Copyrights @ 2019, All Rights Reserved</p>
            </div>
          </div>
        </footer>
</div>

        );
    }
}
export default  Datasecurity;