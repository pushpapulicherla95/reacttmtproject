import actionType from '../common/actionType';


const initialState = {
    emailResponse:"",
}

const commonReducer = (state = initialState, action) => {
    switch (action.type) {
        
        case actionType.SEND_EMAIL_SUCCESS:
            return {
                ...state,
                emailResponse: action.payload,
            }
        case actionType.SEND_EMAIL_FAILURE:
            return {
                ...state,
                emailResponse: action.payload,
            }
            
        default:
            return state;
    }
}
export default commonReducer;