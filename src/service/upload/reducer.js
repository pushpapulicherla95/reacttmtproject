import actionType from "../upload/actionType";
const initialState = {
  getFilesDetails: ""
};

const uploadReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.UPLOADFILES_SUCCESS:
      return {
        ...state,
        getFilesDetails: action.payload
      };
    case actionType.UPLOADFILES_FAILURE:
      return {
        ...state,
        getFilesDetails: action.error
      };
      case actionType.UPLOADFILES_DELETE_SUCCESS:
      return {
        ...state,
        deleteFilesDetails: action.payload
      };
    case actionType.UPLOADFILES_DELETE_FAILURE:
      return {
        ...state,
        deleteFilesDetails: action.error
      };
    default:
      return state;
  }
};
export default uploadReducer;
