import React, { Component } from "react";
import {
  addEmergencydata,
  getPersonaldata
} from "../../../../service/healthrecord/action";
import { connect } from "react-redux";
import $ from "jquery";
import { getUploadFiles, handleFileDelete } from "../../../../service/upload/action";
import { onlyAlphabets, onlyNumbers, onlyDate } from "../KeyValidation";
import URL from "../../../../asset/configUrl";
import { toastr } from "react-redux-toastr";
import { awsPersonalFiles } from "../../../../service/common/action";

import axios from "axios";

class emergency extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 1,
      tabone: true,
      tabtwo: false,
      contactedPerson: "",
      address: "",
      emergencyContactPerson1: "",
      emergencyContactPerson2: "",
      note: "",
      bloodGroup: "",
      rhesusFactor: "",
      donor: "",
      patientsDecree: "",
      patientsDecreeName: "",
      patientsDecreeAddress: "",
      patientsDecreePhone: "",
      patientsDecreeMail: "",
      uploadPopup: false,
      name: "",
      fileName: "",
      fileType: "",
      fileURL: "",
      emergencyContactPersonphone: "",
      emergencyContactPersonEmail: "",
      emergencyContactPersonAddress: "",
      emergencyContactPerson2Phone: "",
      emergencyContactPerson2Email: "",
      emergencyContactPerson2Address: "",
      donerName: "",
      donerPhone: "",
      donerEmail: "",
      donerName2: "",
      donerPhone2: "",
      donerEmail2: "",
      donerName3: "",
      donerPhone3: "",
      donerEmail3: "",
      error: {},
      Valid: false
    };
  }
  handleAwsFiles = value => {
    window.open("https://data.tomatomedical.com/patientpersonal/" + value, '_blank');

  };
  ValidationField = () => {
    const {
      contactedPerson,
      address,
      emergencyContactPerson1,
      emergencyContactPerson2,
      note,
      bloodGroup,
      rhesusFactor,
      donor,
      patientsDecree,
      patientsDecreeName,
      patientsDecreeAddress,
      patientsDecreePhone,
      patientsDecreeMail, emergencyContactPersonphone,
      emergencyContactPersonEmail,
      emergencyContactPersonAddress,
      emergencyContactPerson2Phone,
      emergencyContactPerson2Email,
      emergencyContactPerson2Address,
      donerName,
      donerPhone,
      donerEmail,
      donerName2,
      donerPhone2,
      donerEmail2,
      donerName3,
      donerPhone3,
      donerEmail3 } = this.state
    this.setState({
      Valid:
        // address||
        // contactedPerson||
        emergencyContactPerson1
      // emergencyContactPerson2||
      // note||
      // bloodGroup||
      // rhesusFactor||
      // donor||
      // patientsDecree||
      // patientsDecreeName||
      // patientsDecreeAddress||
      // patientsDecreePhone||
      // patientsDecreeMail||  emergencyContactPersonphone||
      // emergencyContactPersonEmail||
      // emergencyContactPersonAddress||
      // emergencyContactPerson2Phone||
      // emergencyContactPerson2Email||
      // emergencyContactPerson2Address||
      // donerName||
      // donerPhone||
      // donerEmail||
      // donerName2||
      // donerPhone2||
      // donerEmail2||
      // donerName3||
      // donerPhone3||
      // donerEmail3
    });


  }
  handleChangePopup = e => {
    var reader = new FileReader();
    var file = e.target.files[0];
    let name = file.name.split('.')[0]
    let type = "." + file.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileURL: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(file);
  };
  popupSubmit = () => {
    var phrase = this.state.fileURL;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);
    var { userInfo } = this.props.loginDetails;
    const payload = {
      uid: userInfo.userId,
      z: match[1],
      fileName: this.state.fileName,
      fileType: this.state.fileType,
      name: this.state.name,
      category: "emergency"
    };
    this.setState({ uploadPopup: false });

    axios
      .post(URL.FILEEMERGENCY_UPLOAD, payload)
      .then(response => {
        response.data.status === "success"
          ? toastr.success("Message", response.data.message)
          : toastr.error("Message", response.data.message);
        if (response.data.status === "success") {
          var { userInfo } = this.props.loginDetails;
          const uploadFiles = {
            uid: userInfo.userId,
            category: "emergency"
          };
          this.props.getUploadFiles(uploadFiles);
          this.setState({ uploadPopup: false });
          this.setState({
            name: "",
            fileName: "",
            fileType: "",
            fileURL: ""
          });
        }
      })
      .catch(error => {
        // this.setState({ uploadPopup: true });
      });
  };

  trigger = () => {
    this.setState({ uploadPopup: !this.state.uploadPopup });
  };

  tabOne = () => {
    this.setState({ tabone: true, tabtwo: false });
  };
  tabTwo = () => {
    this.setState({ tabone: false, tabtwo: true });
  };
  handleChange = e => {

    const error = this.state.error
    if (e.target.name == "emergencyContactPersonEmail" || e.target.name == "emergencyContactPerson2Email" || e.target.name == "emergencyContactPerson2Email" || e.target.name == "donerEmail" || e.target.name == "donerEmail2" || e.target.name == "donerEmail3" || e.target.name == "patientsDecreeMail") {
      const re = /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i;
      if (e.target.value && !re.test(e.target.value)) {
        error[e.target.name] = "Please enter the valid email Id"
      } else {
        error[e.target.name] = ""
      }
    }
    this.setState({
      error
    }, () => {
      if (error.emergencyContactPersonEmail || error.emergencyContactPerson2Email || error.emergencyContactPerson2Email || error.donerEmail || error.donerEmail2 || error.donerEmail3 || error.patientsDecreeMail) {
        this.setState({ Valid: false })
      } else {
        this.setState({ Valid: true }, () => this.ValidationField())
      }
    })

    this.setState({ [e.target.name]: e.target.value });

  };

  componentWillMount() {
    const { userInfo } = this.props.loginDetails;
    const listpersonal = {
      uid: userInfo.userId
    };

    const uploadFiles = {
      uid: userInfo.userId,
      category: "emergency"
    };
    this.props.getUploadFiles(uploadFiles);
    this.props.getPersonaldata(listpersonal);
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.getpersonalDetails &&
      nextProps.getpersonalDetails.emergency != null &&
      nextProps.getpersonalDetails.emergency != 0
    ) {
      const listpersonal = nextProps.getpersonalDetails.emergency;
      let patientFindingListData = JSON.parse(listpersonal);

      this.setState({
        emergencyContactPerson1: patientFindingListData.emergencyContactPerson1,
        emergencyContactPerson2: patientFindingListData.emergencyContactPerson2,
        emergencyContactPersonphone: patientFindingListData.emergencyContactPersonphone,
        emergencyContactPersonEmail: patientFindingListData.emergencyContactPersonphone,
        emergencyContactPersonAddress: patientFindingListData.emergencyContactPersonAddress,
        emergencyContactPerson2Phone: patientFindingListData.emergencyContactPerson2Phone,
        emergencyContactPerson2Email: patientFindingListData.emergencyContactPerson2Email,
        emergencyContactPerson2Address: patientFindingListData.emergencyContactPerson2Address,
        donerName: patientFindingListData.donerName,
        donerPhone: patientFindingListData.donerPhone,
        donerEmail: patientFindingListData.donerEmail,
        donerName2: patientFindingListData.donerName2,
        donerEmail2: patientFindingListData.donerEmail2,
        donerPhone2: patientFindingListData.donerPhone2,
        donerName3: patientFindingListData.donerName3,
        donerEmail3: patientFindingListData.donerEmail3,
        donerPhone3: patientFindingListData.donerPhone3,
        bloodGroup: patientFindingListData.bloodGroup,
        donor: patientFindingListData.donor,
        patientsDecree: patientFindingListData.patientsDecree,
        patientsDecreeAddress: patientFindingListData.patientsDecreeAddress,
        patientsDecreeName: patientFindingListData.patientsDecreeName,
        patientsDecreePhone: patientFindingListData.patientsDecreePhone,
        patientsDecreeMail: patientFindingListData.patientsDecreeMail
      });
    }
  }
  handleData = () => {
    const {
      emergencyContactPerson1,
      emergencyContactPerson2,
      emergencyContactPersonphone,
      emergencyContactPersonEmail,
      emergencyContactPersonAddress,
      emergencyContactPerson2Phone,
      emergencyContactPerson2Email,
      emergencyContactPerson2Address,
      donerName,
      donerPhone,
      donerEmail,
      donerName2,
      donerEmail2,
      donerPhone2,
      donerName3,
      donerEmail3,
      donerPhone3,
      bloodGroup,
      donor,
      patientsDecree,
      patientsDecreeAddress,
      patientsDecreeName,
      patientsDecreePhone,
      patientsDecreeMail
    } = this.state;
    const { userInfo } = this.props.loginDetails;
    const emergencyData = {
      userId: userInfo.userId,
      emergency: {

        emergencyContactPerson1: emergencyContactPerson1,
        emergencyContactPerson2: emergencyContactPerson2,
        emergencyContactPersonphone: emergencyContactPersonphone,
        emergencyContactPersonEmail: emergencyContactPersonphone,
        emergencyContactPersonAddress: emergencyContactPersonAddress,
        emergencyContactPerson2Phone: emergencyContactPerson2Phone,
        emergencyContactPerson2Email: emergencyContactPerson2Email,
        emergencyContactPerson2Address: emergencyContactPerson2Address,
        donerName: donerName,
        donerPhone: donerPhone,
        donerEmail: donerEmail,
        donerName2: donerName2,
        donerEmail2: donerEmail2,
        donerPhone2: donerPhone2,
        donerName3: donerName3,
        donerEmail3: donerEmail3,
        donerPhone3: donerPhone3,
        bloodGroup: bloodGroup,
        donor: donor,
        patientsDecree: patientsDecree,
        patientsDecreeAddress: patientsDecreeAddress,
        patientsDecreeName: patientsDecreeName,
        patientsDecreePhone: patientsDecreePhone,
        patientsDecreeMail: patientsDecreeMail
      }
    };

    this.props.addEmergencydata(emergencyData);
  };
  render() {
    const {
      tabone,
      tabtwo,
      emergencyContactPerson1,
      emergencyContactPerson2,
      emergencyContactPersonphone,
      emergencyContactPersonEmail,
      emergencyContactPersonAddress,
      emergencyContactPerson2Phone,
      emergencyContactPerson2Email,
      emergencyContactPerson2Address,
      donerName,
      donerPhone,
      donerEmail,
      donerName2,
      donerEmail2,
      donerPhone2,
      donerName3,
      donerEmail3,
      donerPhone3,
      bloodGroup,
      donor,
      patientsDecree,
      patientsDecreeAddress,
      patientsDecreeName,
      patientsDecreePhone,
      patientsDecreeMail, error, Valid
    } = this.state;
    return (
      <div className="" id="main-content">
        <div className="wrapper">
          <div className="container-fluid">
            <div className="body-content">
              <div className="tomCard">
                <div className="tomCardHead">
                  <h5 className="float-left">EMERGENCY DATA</h5>
                </div>

                <div className="tab-menu-content">
                  <div class="tab-menu-title">
                    <ul>
                      <li className={this.state.tabone ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabOne}>
                          <p>Emergency</p>
                        </a>
                      </li>
                      <li className={this.state.tabtwo ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabTwo}>
                          <p>Attached Files</p>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>

                {tabone && (
                  <div className="tab-menu-content-section">
                    <div
                      id="content-1"
                      className={tabone ? "d-block" : "d-none"}
                    >
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label />
                            Emergency Contact Person 1 <sup>*</sup>
                            <input
                              type="text"
                              class="form-control"
                              name="emergencyContactPerson1"
                              onChange={this.handleChange}
                              value={emergencyContactPerson1}
                              onKeyPress={onlyAlphabets}
                            />
                          </div>
                          <div class="form-group">
                            <label />
                            Phone / Mobile
                            <input
                              type="text"
                              class="form-control"
                              name="emergencyContactPersonphone"
                              onChange={this.handleChange}
                              value={emergencyContactPersonphone}
                              onKeyPress={onlyNumbers}
                              maxLength="15"
                            />
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label />
                            Email
                            <input
                              type="text"
                              class="form-control"
                              name="emergencyContactPersonEmail"
                              onChange={this.handleChange}
                              value={emergencyContactPersonEmail}
                            />
                          </div>
                          <div style={{ color: "red" }}>{error.emergencyContactPersonEmail}</div>
                          <div class="form-group">
                            <label />
                            Address (Street, ZIP/Postcode, City)
                            <input
                              type="text"
                              class="form-control"
                              name="emergencyContactPersonAddress"
                              onChange={this.handleChange}
                              value={emergencyContactPersonAddress}
                            />
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label />
                            Emergency Contact Person2
                            <input
                              type="text"
                              class="form-control"
                              name="emergencyContactPerson2"
                              onChange={this.handleChange}
                              value={emergencyContactPerson2}
                              onKeyPress={onlyAlphabets}
                            />
                          </div>
                          <div class="form-group">
                            <label />
                            Phone / Mobile
                            <input
                              type="text"
                              class="form-control"
                              name="emergencyContactPerson2Phone"
                              onChange={this.handleChange}
                              value={emergencyContactPerson2Phone}
                              onKeyPress={onlyNumbers} maxLength="15"
                            />
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label />
                            Email
                            <input
                              type="text"
                              class="form-control"
                              name="emergencyContactPerson2Email"
                              onChange={this.handleChange}
                              value={emergencyContactPerson2Email}
                            />
                          </div>
                          <div style={{ color: "red" }}>{error.emergencyContactPerson2Email}</div>

                          <div class="form-group">
                            <label />
                            Address (Street, ZIP/Postcode, City)
                            <input
                              type="text"
                              class="form-control"
                              name="emergencyContactPerson2Address"
                              onChange={this.handleChange}
                              value={emergencyContactPerson2Address}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row mt-5 mb-2">
                        <div className="col-12">
                          <hr />
                        </div>
                      </div>


                      <div className="row">
                        <div className="col-12">
                          <div className="tomCardHead">
                            <h5 className="float-left">Patient's Decree</h5>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label />
                            Name Contact (Patient's Decree)
                            <input type="text"
                              class="form-control"
                              name="patientsDecreeName"
                              onChange={this.handleChange}
                              value={patientsDecreeName}
                              onKeyPress={onlyAlphabets}
                            />
                          </div>

                          <div class="form-group">
                            <label>Address (Patient's Decree)</label>
                            <input
                              type="text"
                              class="form-control"
                              name="patientsDecreeAddress"
                              onChange={this.handleChange}
                              value={patientsDecreeAddress}
                            />
                          </div>
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>E-Mail (Patient's Decree)</label>
                            <input
                              type="text"
                              class="form-control"
                              name="patientsDecreeMail"
                              onChange={this.handleChange}
                              value={patientsDecreeMail}
                            />
                          </div>
                          <div style={{ color: "red" }}>{error.patientsDecreeMail}</div>


                          <div class="form-group">
                            <label>Phone number (Patient's Decree)</label>
                            <input
                              type="text"
                              class="form-control"
                              name="patientsDecreePhone"
                              onChange={this.handleChange}
                              value={patientsDecreePhone}
                              onKeyPress={onlyNumbers} maxLength="15"
                            />
                          </div>
                        </div>
                      </div>

                      <div className="row mt-5 mb-2">
                        <div className="col-12">
                          <hr />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12">
                          <div className="tomCardHead">
                            <h5 className="float-left">Add Doner</h5>
                          </div>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-lg-4 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label />
                            Name
                            <input type="text"
                              class="form-control"
                              name="donerName"
                              onChange={this.handleChange}
                              value={donerName}
                              onKeyPress={onlyAlphabets}
                            />
                          </div>


                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Phone / Mobile</label>
                            <input
                              type="text"
                              class="form-control"
                              name="donerPhone"
                              onChange={this.handleChange}
                              value={donerPhone} onKeyPress={onlyNumbers} maxLength="15"
                            />
                          </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>E-Mail</label>
                            <input
                              type="text"
                              class="form-control"
                              name="donerEmail"
                              onChange={this.handleChange}
                              value={donerEmail}
                            />
                          </div>
                          <div style={{ color: "red" }}>{error.donerEmail}</div>
                        </div>


                      </div>
                      <div className="row">
                        <div className="col-lg-4 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label />
                            Name
                            <input type="text"
                              class="form-control"
                              name="donerName2"
                              onChange={this.handleChange}
                              value={donerName2}
                              onKeyPress={onlyAlphabets}
                            />
                          </div>


                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Phone / Mobile</label>
                            <input
                              type="text"
                              class="form-control"
                              name="donerPhone2"
                              onChange={this.handleChange}
                              value={donerPhone2}
                              onKeyPress={onlyNumbers} maxLength="15"
                            />
                          </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>E-Mail</label>
                            <input
                              type="text"
                              class="form-control"
                              name="donerEmail2"
                              onChange={this.handleChange}
                              value={donerEmail2}
                            />
                          </div>
                          <div style={{ color: "red" }}>{error.donerEmail2}</div>

                        </div>


                      </div>
                      <div className="row">
                        <div className="col-lg-4 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label />
                            Name
                            <input type="text"
                              class="form-control"
                              name="donerName3"
                              onChange={this.handleChange}
                              value={donerName3}
                              onKeyPress={onlyAlphabets}
                            />
                          </div>


                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Phone / Mobile</label>
                            <input
                              type="text"
                              class="form-control"
                              name="donerPhone3"
                              onChange={this.handleChange}
                              value={donerPhone3} onKeyPress={onlyNumbers} maxLength="15"
                            />
                          </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>E-Mail</label>
                            <input
                              type="text"
                              class="form-control"
                              name="donerEmail3"
                              onChange={this.handleChange}
                              value={donerEmail3}
                            />
                          </div>
                          <div style={{ color: "red" }}>{error.donerEmail3}</div>

                        </div>


                      </div>

                      <div className="row mt-5 mb-2">
                        <div className="col-12">
                          <hr />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12">
                          <div className="tomCardHead">
                            <h5 className="float-left">Blood Group</h5>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label />
                            Blood Group
                            <select
                              class="form-control"
                              name="bloodGroup"
                              onChange={this.handleChange}
                              value={bloodGroup}
                            >
                              <option>--Select Blood Group --</option>

                              <option data-label="A+" value="A+">
                                A+
                              </option>
                              <option data-label="O+" value="O+">
                                O+
                              </option>
                              <option data-label="B+" value="B+">
                                B+
                              </option>
                              <option data-label="AB+" value="AB+">
                                AB+
                              </option>
                              <option data-label="A-" value="A-">
                                A-
                              </option>
                              <option data-label="O-" value="O-">
                                O-
                              </option>
                              <option data-label="B-" value="B-">
                                B-
                              </option>
                              <option data-label="AB-" value="AB-">
                                AB-
                              </option>
                            </select>
                          </div>


                        </div>
                      </div>
                      {/* <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label />
                            Address (Street, ZIP/Postcode, City)
                            <input
                              type="text"
                              class="form-control"
                              name="address"
                              onChange={this.handleChange}
                              value={this.state.address}
                            />
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Note</label>
                            <input
                              type="date"
                              class="form-control"
                              name="note"
                              onChange={this.handleChange}
                              value={this.state.note}
                              onKeyPress={onlyDate}
                            />
                          </div>
                        </div>
                      </div> */}
                      {/* <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label />
                            Emergency Contact Person2
                            <input
                              type="text"
                              class="form-control"
                              name="emergencyContactPerson2"
                              onChange={this.handleChange}
                              value={this.state.emergencyContactPerson2}
                              onKeyPress={onlyAlphabets}
                            />
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label />
                            Blood Group
                            <select
                              class="form-control"
                              name="bloodGroup"
                              onChange={this.handleChange}
                              value={this.state.bloodGroup}
                            >
                              <option>--Select Blood Group --</option>

                              <option data-label="0" value="0">
                                0
                              </option>
                              <option data-label="A" value="A">
                                A
                              </option>
                              <option data-label="B" value="B">
                                B
                              </option>
                              <option data-label="AB" value="AB">
                                AB
                              </option>
                              <option data-label="Unknown" value="Unknown">
                                Unknown
                              </option>
                            </select>
                          </div>
                        </div>
                      </div> */}
                      {/* <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label />
                            Rhesus factor
                            <select
                              class="form-control"
                              name="rhesusFactor"
                              onChange={this.handleChange}
                              value={this.state.rhesusFactor}
                            >
                              <option>--Select Rhesus factor --</option>

                              <option data-label="Positive" value="Positive">
                                Positive
                              </option>
                              <option data-label="Negative" value="Negative">
                                Negative
                              </option>
                              <option data-label="Unknown" value="Unknown">
                                Unknown
                              </option>
                            </select>
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Donor</label>
                            <select
                              class="form-control"
                              name="donor"
                              onChange={this.handleChange}
                              value={this.state.donor}
                            >
                              <option>--Select Donor --</option>

                              <option data-label="Yes" value="Yes">
                                Yes
                              </option>
                              <option data-label="No" value="No">
                                No
                              </option>
                            </select>
                          </div>
                        </div>
                      </div> */}
                      {/* <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label />
                            Patient's Decree
                            <select
                              class="form-control"
                              name="patientsDecree"
                              onChange={this.handleChange}
                              value={this.state.patientsDecree}
                            >
                              <option>--Select Patient's Decree --</option>

                              <option data-label="Yes" value="Yes">
                                Yes
                              </option>
                              <option data-label="No" value="No">
                                No
                              </option>
                            </select>
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label />
                            Name Contact (Patient's Decree)
                            <textarea
                              rows="3"
                              class="form-control"
                              name="patientsDecreeName"
                              onChange={this.handleChange}
                              value={this.state.patientsDecreeName}
                              onKeyPress={onlyAlphabets}
                            />
                          </div>
                        </div>
                      </div> */}
                      {/* <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Phone number (Patient's Decree)</label>
                            <input
                              type="text"
                              class="form-control"
                              name="patientsDecreePhone"
                              onChange={this.handleChange}
                              value={this.state.patientsDecreePhone}
                              onKeyPress={onlyNumbers}
                            />
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>E-Mail (Patient's Decree)</label>
                            <input
                              type="text"
                              class="form-control"
                              name="patientsDecreeMail"
                              onChange={this.handleChange}
                              value={this.state.patientsDecreeMail}
                            />
                          </div>
                        </div>
                      </div> */}
                      {/* <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Address (Patient's Decree)</label>
                            <input
                              type="text"
                              class="form-control"
                              name="patientsDecreeAddress"
                              onChange={this.handleChange}
                              value={this.state.patientsDecreeAddress}
                            />
                          </div>
                        </div>
                      </div> */}
                      <div class="row">
                        <div class="col-12">
                          <button
                            type="button"
                            class="commonBtn float-right"
                            onClick={this.handleData} disabled={!Valid}
                          >
                            Save
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                {tabtwo && (
                  <div id="content-2" className={tabtwo ? "d-block" : "d-none"}>
                    <button
                      type="button"
                      className="commonBtn2 float-right mb-2"
                      onClick={this.trigger}
                    >
                      <i className="fa fa-plus" />
                      <span> Upload new files</span>                 </button>
                    <div className="table-responsive">
                      <table className="table table-hover">
                        <thead className="thead-default">
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>FileName</th>

                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.props.getFilesDetails.fileDetails &&
                            this.props.getFilesDetails.fileDetails.map(
                              (each, i) => (
                                <tr>
                                  <td data-label="#">{i + 1}</td>
                                  <td data-label="Name">{each.name}</td>
                                  <td data-label="FileName"> {each.fileName}</td>
                                  <td data-label="Action">
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.handleAwsFiles(each.fileUrl)
                                      }
                                    >
                                      <i class="fas fa-download" />
                                    </button>
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.props.handleFileDelete(each.id, each.fileUrl, "emergency")
                                      }
                                    >
                                      <i class="fas fa-trash" />
                                    </button>
                                  </td>
                                </tr>
                              )
                            )}
                          {/* <tr>
                            <td data-label="#">1</td>
                            <td data-label="Name">Eye Care Test Report</td>
                            <td data-label="Type">png</td>
                            <td data-label="Size">87.9 kB</td>
                            <td data-label="Date">16.05.2019 11:56</td>
                            <td data-label="Action">
                              <a
                                title="Edit"
                                className="editBtn"
                                data-toggle="modal"
                                data-target="#myModal"
                              >
                                <i className="fa fa-edit" />
                              </a>
                              <a
                                title="Delete"
                                className="deleteBtn"
                                onClick={e => this.deletePatient()}
                              >
                                <i
                                  className="fa fa-trash"
                                  data-toggle="modal"
                                  data-target="#myModal2"
                                />
                              </a>
                            </td>
                          </tr> */}
                        </tbody>
                      </table>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        {
          <div
            className={this.state.uploadPopup ? "modal d-block" : "modal"}
            id="myModal"
          >
            <div class="modal-dialog">
              <div class="modal-content modalPopUp">
                <div class="modal-header borderNone">
                  <h5 class="modal-title">Upload File</h5>
                  <button
                    type="button"
                    className="close"
                    onClick={this.trigger}
                  >
                    <i class="fas fa-times" />
                  </button>
                </div>
                <div class="modal-body">
                  <form>
                    <div class="row">
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="form-group">
                          <label className="form-label">File name</label>
                          <input
                            className="form-control"
                            type="text"
                            name="name"
                            value={this.state.name}
                            onChange={this.handleChange}
                          />
                          <span />
                        </div>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="fileUp">
                          <label
                            for="fileUp1"
                            class="commonBtn text-center upload"
                          >
                            Upload File
                          </label>
                          <input
                            type="file"
                            id="fileUp1"
                            style={{ display: "none" }}
                            onChange={this.handleChangePopup}
                          />
                          <p style={{ textAlign: "center" }} />
                        </div>
                      </div>

                      <div class="col-md-12 col-lg-12 col-sm-12 mt-2">
                        <button
                          type="button"
                          class="commonBtn float-right"
                          onClick={this.popupSubmit} disabled={!this.state.fileURL}
                        >
                          submit
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  getpersonalDetails: state.healthrecordReducer.getpersonalDetails,
  loginDetails: state.loginReducer.loginDetails,
  getFilesDetails: state.uploadReducer.getFilesDetails
});

const mapDispatchToProps = dispatch => ({
  addEmergencydata: emergencyData => dispatch(addEmergencydata(emergencyData)),
  getPersonaldata: listpersonal => dispatch(getPersonaldata(listpersonal)),
  getUploadFiles: payload => dispatch(getUploadFiles(payload)),
  handleFileDelete: (id, fileUrl, type) => dispatch(handleFileDelete(id, fileUrl, type))

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(emergency);
