import React, { Component } from "react";
import { getAdminProfile, updateAdminProfile } from "../../../service/admin/Patient/action";
import { connect } from "react-redux";
// import { uploadProfilepic } from "../../service/hospital/action";
import { uploadProfilepic } from "../../../service/hospital/action"
class AdminProfile extends Component {

    constructor(props) {
        super(props);
        const { loginDetails } = this.props;
        const { adminInfo } = loginDetails;
        this.state = {
            firstName: "",
            lastName: "",
            email: "",
            gender: "",
            mobileNo: "",
            aboutMe: "",
            id: adminInfo.id
        }

    }

    componentWillMount = () => {

        const profiledata = {
            id: this.state.id
        }
        this.props.getAdminProfile(profiledata);
    }
    componentWillReceiveProps = (nextProps) => {
        console.log("nextProps", nextProps);
        if (nextProps.adminProfileDetails) {
            const { adminProfileDetails } = nextProps;
            this.setState({
                firstName: adminProfileDetails.firstName,
                lastName: adminProfileDetails.lastName,
                email: adminProfileDetails.email,
                gender: adminProfileDetails.gender,
                mobileNo: adminProfileDetails.mobileNo,
                aboutMe: adminProfileDetails.aboutMe
            })

        }

    }
    handleChange = (e) => {
        e.preventDefault();
        this.setState({ [e.target.name]: e.target.value });
    }
    handleSubmit = (e) => {
        e.preventDefault();
        const { firstName, lastName, gender, email, mobileNo, aboutMe } = this.state;
        const editInfo = {
            id: this.state.id,
            firstName: firstName,
            lastName: lastName,
            gender: gender,
            email: email,
            mobileNo: mobileNo,
            aboutMe: aboutMe
        }

        this.props.updateAdminProfile(editInfo);
    }
    setGender(event) {
        this.setState({ [event.target.name]: event.target.value });
    }
    render() {
        console.log("name", this.props);
        const { firstName, lastName, gender, email, mobileNo, aboutMe } = this.state;
        return (
            <div className="mainMenuSide" id="main-content">
                <div className="wrapper">
                    <div className="container-fluid">
                        <div className="body-content">
                            <div className="tomCard">
                                <div className="tomCardHead">
                                    <h5 className="float-left">My Profile</h5>
                                </div>

                                <div className="tomCardBody">
                                    <div className="innerCard">
                                        <div className="innerCardHead">
                                            <h5>Basic Information</h5>
                                        </div>
                                        <div className="innerCardBody">
                                            <div className="row">
                                                {/* <div className="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12">
                                                    <div className="imagePreview">
                                                        <label className="uploadBtn"><svg className="svg-inline--fa fa-upload fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="upload" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M296 384h-80c-13.3 0-24-10.7-24-24V192h-87.7c-17.8 0-26.7-21.5-14.1-34.1L242.3 5.7c7.5-7.5 19.8-7.5 27.3 0l152.2 152.2c12.6 12.6 3.7 34.1-14.1 34.1H320v168c0 13.3-10.7 24-24 24zm216-8v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h136v8c0 30.9 25.1 56 56 56h80c30.9 0 56-25.1 56-56v-8h136c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path></svg><input type="file" /></label>
                                                        <img src="../images/profile.png" alt="your image" />
                                                    </div>
                                                    <button type="button" className="commonBtn text-center upload">Upload Image</button>
                                                </div> */}
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div className="row">
                                                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                                                            <div className="form-group">
                                                                <label>First Name</label>
                                                                <input type="text" className="form-control" name="firstName" value={firstName} onChange={this.handleChange} />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                                                            <div className="form-group">
                                                                <label>Last Name</label>
                                                                <input type="text" className="form-control" name="lastName" value={lastName} onChange={this.handleChange} />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                            
                                                                {/* <div className="genderInfo">
                                                                    <input type="radio" id={"male"} name="gender" checked ={gender === "male"} value="gender"    onChange={this.setGender.bind(this)}/>
                                                                    <label htmlFor={"male"}>Male</label>
                                                                </div>
                                                                <div className="genderInfo">
                                                                    <input type="radio" id={"female"} name="gender" checked ={gender === "female"} value="gender"    onChange={this.setGender.bind(this)}/>
                                                                    <label htmlFor={"female"}>Female</label>
                                                                </div> */}
                                                                <div class="form-group">
                                                                    <label>Gender</label>
                                                                    <div class="genderInfo">
                                                                        <input
                                                                            type="radio"
                                                                            id={"cloudlink1"}
                                                                            name="gender"
                                                                            value="male"
                                                                            checked={gender === "male"}
                                                                            onChange={this.setGender.bind(this)}
                                                                        />
                                                                        <label htmlFor={"cloudlink1"}>Male</label>
                                                                    </div>
                                                                    <div class="genderInfo">
                                                                        <input
                                                                            type="radio"
                                                                            id={"emailzip1"}
                                                                            name="gender"
                                                                            value="female"
                                                                            checked={gender === "female"}
                                                                            onChange={this.setGender.bind(this)}
                                                                        />
                                                                        <label htmlFor={"emailzip1"}>Female</label>
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                            <div className="form-group">
                                                                <label>Email Id</label>
                                                                <input type="text" className="form-control" name="email" value={email} />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                            <div className="form-group">
                                                                <label>Mobile No</label>
                                                                <input type="text" className="form-control" name="mobileNo" value={mobileNo} onChange={this.handleChange} />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="innerCard">
                                        <div className="innerCardHead">
                                            <h5>About Me</h5>
                                        </div>
                                        <div className="innerCardBody">
                                            <div className="row">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div className="form-group">
                                                        <textarea className="form-control" name="aboutMe" value={aboutMe} onChange={this.handleChange}></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <button type="button" className="commonBtn float-right" onClick={this.handleSubmit}>Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails,
    adminProfileDetails: state.adminPatientReducer.adminProfileDetails
})
const mapDispatchToProps = dispatch => ({
    getAdminProfile: (profiledata) => dispatch(getAdminProfile(profiledata)),
    updateAdminProfile: (editInfo) => dispatch(updateAdminProfile(editInfo))
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AdminProfile);