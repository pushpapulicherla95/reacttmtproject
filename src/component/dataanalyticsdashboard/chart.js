import React, { Component } from "react";
import Chart from "chart.js";
import axios from "axios";
import jsPDF from 'jspdf'
import $ from 'jquery'
class BarChart extends Component {
  constructor() {
    super();
    this.state = {
      options: null,
      filterEnable: false,
      filterOptions: [],
      selectedOption: [],
      filter: "",
      updatedfilter: []
    };
  }

  // componentWillMount = () => {
  //   const payload = this.props.location.state;
  //   this.getDetails(payload);
  // };

  // download = () => {
  //   var pdf = new jsPDF('p', 'pt', 'a4');
  //   console.log("document.getElementById('chart-section')", document.getElementById('chartBody'))
  //   pdf.addHTML(document.getElementById('chartBody'), () => {
  //     pdf.save('web.pdf');
  //   });
  // }

  handleChange = event => {
    var updatedList = this.state.updatedfilter;
    updatedList = updatedList.filter(item => {
      return (
        item.tablename
          .toLowerCase()
          .search(event.target.value.toLowerCase()) !== -1
      );
    });
    this.setState({ options: updatedList });
  };
  componentDidMount = () => {
    const payload = this.props.location.state;
    //  compare_by: [
    //    {
    //      value: {
    //        column_name: "personal_gender",
    //        row_values: "M"
    //      }
    //    },
    //    {
    //      value: {
    //        column_name: "personal_gender"
    //      }
    //    }
    //  ],
    //  compare_with: ["allergies_name", "medication_name"]
    this.getDetails(payload);
  };
  getRandomColor = () => {
    var colors = [
      "#ffb3b3",
      "#78d3b2",
      " #78d3d3",
      "#cdcd65",
      " #af9fe0",
      "#e6b2bf",
      "#CD5C5C",
      "#F08080",
      "#FA8072",
      "#E9967A",
      "#FFA07A"
    ];
    var rand = Math.floor(Math.random() * colors.length);
    return colors[rand];
  };
  renderChart = () => {

    this.state.selectedOption.map((each, i) => {
      let colorReturn = this.getRandomColor();
      let data1 = [];
      let data2 = [];
      let data3 = [];

      each.field.map(val => {
        data1.push(val.fieldname);
        data2.push(val.fieldcount);
        data3.push(val.count);
      });
      var canvas = document.getElementById("myChart" + i);
      var ctx = canvas.getContext("2d");
      let label = data3;
      var data = {
        labels: data1,
        datasets: [
          {
            label: each.tablename,
            fill: true,
            backgroundColor: colorReturn,
            borderColor: colorReturn,
            borderWidth: 2,
            hoverBackgroundColor: colorReturn,
            hoverBorderColor: colorReturn,
            data: data2
          }
        ]
      };

      var array = [];
      var options = {
        tooltips: {
          callbacks: {
            label: tooltipItem => {
              return (
                "Total" +
                " : " +
                Number(tooltipItem.yLabel) +
                " " +
                Object.keys(label[tooltipItem.index]) +
                " : " +
                Object.values(label[tooltipItem.index])
              );
            }
          }
        },
        scaleFontColor: "red",
        scaleFontSize: 10,
        responsive: false,

        scales: {
          yAxes: [
            {
              stacked: true,
              gridLines: {
                display: true,
                color: "rgba(0,0,0,0.1)"
              }
            }
          ],
          xAxes: [
            {
              gridLines: {
                display: false,
                color: "rgba(0,0,0,0.1)"
              }
            }
          ],
          fontColor: "#000"
        },

        legend: {
          labels: {
            fontColor: "black",
            fontSize: 11,
            fontFamily: "Arial, sans-serif"
          }
        }
      };
      var myBarChart = new Chart(ctx, {
        type: "bar",
        data: data,
        options: options
      });
    });
  };

  getDetails = async payload => {
    axios
      .post("http://192.168.2.30:8000/tomatomedical/API/columns", payload)
      .then(response => {
        let option = [].concat.apply([], response.data);
        this.setState(
          { options: option, selectedOption: option, updatedfilter: option },
          () => {
            this.renderChart();
          }
        );
      });
  };
  conditionChecked = demo1 => {
    this.setState({ filterOptions: demo1 }, () => {
      if (demo1.length == 0) {
        this.setState({ selectedOption: this.state.options }, () => {
          this.renderChart();
        });
      } else {
        this.setState({ selectedOption: demo1 }, () => {
          this.renderChart();
        });
      }
    });
  };

  filterChart = (each, index) => {
    var element = document.getElementById("myDIV" + index);
    element.classList.toggle("active");
    let demo1 = this.state.filterOptions;
    if (demo1.indexOf(each) == -1) {
      demo1.push(each);
      this.conditionChecked(demo1);
    } else {
      demo1.splice(demo1.indexOf(each), 1);
      this.setState({ filterOptions: demo1 }, () => {
        this.conditionChecked(demo1);
      });
    }
  };

  render() {
    return (
      <div id="chartBody">
        <div class="inner-pageNew" id="content">
          <div id="editor"></div>
          <header className="header">
            <div className="brand">
              <a href="Dashboard.html" className="backArrow">
                <i className="fas fa-chevron-left"></i>
              </a>
              <div className="mobileLogo">
                <a
                  href="javascript:void(0)"
                  onClick={() =>
                    this.props.history.push({
                      pathname: "/hospitalmainmenu"
                    })
                  }
                  className="logo"
                >
                  <img src="../images/logo.jpg" />{" "}
                </a>
              </div>
            </div>
            <div className="top-nav">
              <div className="dropdown float-right mr-2">
                <a className="dropdown-toggle" data-toggle="dropdown">
                  Tomato Medical
              </a>
                <div className="dropdown-menu">
                  <a className="dropdown-item" href="#">
                    <i className="fa fa-edit icon-head-menu"></i>
                    <label className="head-menu">My account</label>
                  </a>
                  <a className="dropdown-item" href="#">
                    Setting
                </a>
                  <a className="dropdown-item" href="#">
                    Logout
                </a>
                </div>
              </div>
            </div>
          </header>

          <div class="body-content">
            <div class="container-fluid">
              <div className="row mt-2">
                <div className="col-12">
                  <div class="listCardsearch position-relative">
                    <h5>Charts Section</h5>
                    <button id="cmd" onClick={() => window.print()}>download</button>

                    <div
                      className="filtericon"
                      onClick={() =>
                        this.setState({
                          filterEnable: !this.state.filterEnable
                        })
                      }
                    >
                      <i class="fas fa-filter"></i>
                      <label>Filter</label>

                    </div>

                    <div
                      class={
                        this.state.filterEnable
                          ? "list-box d-block"
                          : "list-box d-none"
                      }
                    >
                      <input
                        type="text"
                        placeholder="Search"
                        onChange={this.handleChange}
                      />
                      <ul>
                        {this.state.options &&
                          this.state.options.map((each, i) => (
                            <li
                              id={"myDIV" + i}
                              value={each.tablename}
                              onClick={() => this.filterChart(each, i)}
                            >
                              {each.tablename}
                              <span></span>
                            </li>
                          ))}
                      </ul>
                    </div>

                  </div>
                </div>
              </div>
              <div className="chart-section">
                {this.state.selectedOption &&
                  this.state.selectedOption.map((each, i) => (
                    // <canvas id={"myChart"+i} width="400" height="200"></canvas>
                    //               <div style={{textAlign: "center",
                    // fontWeight: "600"}}>
                    // {each.tablename}
                    <div style={{ padding: "16px" }} id="chart">
                      {/* <div class="chartDownload">
                     <button><i class="fas fa-download"></i> Click to Download</button>
                      </div> */}
                      <canvas
                        id={"myChart" + i}
                        style={{
                          height: "100%",
                          width: "100%",
                          border: "1px dotted red "
                        }}
                      />

                    </div>
                    // </div>
                  ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default BarChart;
