import React , {Component} from 'react';
import { NavLink  } from 'react-router-dom';
import { withRouter } from "react-router";
import LandingHeader from '../common/LandingHeader';
class Contact extends Component{
  componentDidMount() {
    window.scrollTo(0, 0)
}
    render(){
        return(
<div>	
<LandingHeader/>
    <section class="contact-banner about-banner banner d-flex justify-content-center align-items-center">
		<div class="container">
			<div class="bannerContent  flex-column">
				<h1>
					<span>Contact </span>
				</h1>
			
			</div>
		</div>
	</section>
	<div class="emergencyAlert">
		<a href="" > <img src="images/emergencyCall.png"/> </a>
	</div>
<section class="about-content">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-12">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2229.38580883623!2d13.005357015686666!3d49.20524317932294!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xadcc8c7887f01642!2stomatomedical!5e1!3m2!1sen!2sin!4v1571324434822!5m2!1sen!2sin" width="100%" height="350" frameBorder="0" style={{border:"0"}} allowFullScreen=""></iframe>
				
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-12">
				<div class="contact_form_area">
				<h3>Contact Data</h3>
				<ul>
						<li>
								<i class="fas fa-map-marker-alt"></i>
								<p>tomatomedical international UG (haftungsbeschränkt)
							Dr. med. Lemberger 
							Schwarzhölzlstr. 1 
							93474 Arrach </p>
						</li>
						<li>	<i class="fas fa-mobile-alt"></i>
							<p>+499943943375 
								(Monday-Friday: 9:00 a.m. − 5:00 p.m.) </p>
							
						</li>
						<li><i class="far fa-envelope"></i><p>info@tomatomedical.com</p></li>
						<li><i class="fas fa-globe-asia"></i> <p><a href="https://www.tomatomedical.com" title="tomatomedical Homepage" target="_blank">www.tomatomedical.com</a></p></li>
					</ul>
			</div>
			</div>
		</div>
		</div>	
</section>
<footer>
          <div class="footerLink">
            <div class="container">
              <ul>
                <li>
                  <NavLink to="/aboutus" >About </NavLink>
                </li>
                <li>
                <NavLink to="/partners" >For Partners </NavLink>
                </li>
                <li>
                <NavLink to="/contact" >Contact </NavLink>
                </li>
                <li>
                <NavLink to="/datasecurity" >Data Security </NavLink>
                </li>
                <li>
                <NavLink to="/privacypolicy" > Privacy Policy </NavLink>
                </li>
                <li>
                <NavLink to="/imprint">Imprint </NavLink>
                </li>
              </ul>
            </div>
          </div>

          <div class="footerCopyRights">
            <div class="container">
              <ul>
                <li>
                  <a href="">
                    <i class="fab fa-facebook-f" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-google-plus-g" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-instagram" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-twitter" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-linkedin-in" />
                  </a>
                </li>
              </ul>
              <p>Copyrights @ 2019, All Rights Reserved</p>
            </div>
          </div>
        </footer>
</div>

        );
    }
}
export default Contact;