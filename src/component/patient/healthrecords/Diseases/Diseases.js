import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";

import {
  diseasesList,
  familyList,
  adddiseases,
  editdiseases,
  deletediseases,
  addupdateDiseases
} from "../../../../service/healthrecord/action";
class Diseases extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 1,
      tabone: true,
      tabtwo: false,
      tabthree: false,
      tabfour: false,
      viewType: "personal",
      radioValue: "",
      selected: "",
      name: "",
      title: "",
      description: "",
      otherDisease: "",
      result: []
    };
  }
  tabOne = () => {
    this.setState({
      tabone: true,
      tabtwo: false,
      tabthree: false,
      tabfour: false
    });
  };
  tabTwo = () => {
    this.setState({
      tabone: false,
      tabtwo: true,
      tabthree: false,
      tabfour: false
    });
  };
  tabThree = () => {
    this.setState({
      tabone: false,
      tabtwo: false,
      tabthree: true,
      tabfour: false,
      type: ""
    });
  };

  adddiseasespop = () => {
    this.setState({ adddiseases: true });
  };

  modalPopUpClose = () => {
    this.setState({
      adddiseases: false
    });
  };

  handleSelect = e => {
    this.setState({ viewType: e.target.value });
  };

  // getDiseaseList = () => {
  //   axios
  //     .post(
  //       "https://tomato.colan.in:9001/tomatomedical/API/patient/health/retrive",
  //       { uid: 9 }
  //     )
  //     .then(response => {
  //       this.setState({
  //         otherDisease: JSON.parse(response.data.otherDiseases)
  //       });
  //     });
  // };

  // handleDelete = each => {
  //   const payload = {
  //     uid: 9,
  //     otherDiseases: [
  //       {
  //         name: each.name,
  //         title: each.title
  //       }
  //     ]
  //   };
  //   this.getDiseaseList();
  //   this.props.deletediseases(payload);
  // };

  // componentWillMount() {
  //   this.props.diseasesList();
  //   this.props.familyList();
  //   this.getDiseaseList();
  // }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  // handleSubmit = e => {
  //   e.preventDefault();
  //   if (this.state.type == "edit") {
  //     const payload = {
  //       uid: 9,
  //       otherDiseases: [
  //         {
  //           name: this.state.name,
  //           title: this.state.title,
  //           description: this.state.description
  //         }
  //       ]
  //     };
  //     this.setState({ adddiseases: false });
  //     this.getDiseaseList();
  //     this.props.editdiseases(payload);
  //   } else {
  //     const payload = {
  //       uid: 9,
  //       otherDiseases: [
  //         {
  //           name: this.state.name,
  //           title: this.state.title,
  //           description: this.state.description
  //         }
  //       ]
  //     };
  //     this.setState({ adddiseases: false });
  //     this.getDiseaseList();
  //     this.props.adddiseases(payload);
  //   }
  // };

  // handleRadio = (e, name) => {
  //   var res = this.state.result.filter(value => {
  //     return !value[name];
  //   });

  //   res.push({ [name]: e.target.value });
  //   this.setState({ radioValue: e.target.value });
  // };

  // handleSave = e => {
  //   e.preventDefault();
  //   const payload = {
  //     uid: 9,
  //     diseasesFamily: this.state.result
  //   };
  //   this.props.addupdateDiseases(payload);
  // };

  render() {
    const { tabone, tabtwo, tabthree, adddiseases, viewType } = this.state;
    const { therapistsfamily, therapistsDiesese } = this.props;
    const { otherDiseases } = this.state.otherDisease;
    // console.log("this.srarrw", this.state.result);
    return (
      <div className="mainMenuSide" id="main-content">
        <div className="wrapper">
          <div className="container-fluid">
            <div className="body-content">
              <div className="tomCard">
                <div className="tomCardHead">
                  <h5 className="float-left">Diseases </h5>
                </div>
                <div className="tab-menu-content">
                  <div className="tab-menu-title">
                    <ul>
                      <li className={this.state.tabone ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabOne}>
                          <p>Diseases</p>
                        </a>
                      </li>
                      <li className={this.state.tabtwo ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabTwo}>
                          <p>Other Diseases</p>
                        </a>
                      </li>
                      <li className={this.state.tabthree ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabThree}>
                          <p> Attached Files</p>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                {tabone && (
                  <div className="tab-menu-content-section">
                    <div
                      id="content-1"
                      className={tabone ? "d-block" : "d-none"}
                    >
                      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row">
                          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="form-group">
                              <label>Selection</label>
                              <select
                                class="form-control"
                                onClick={this.handleSelect}
                              >
                                <option value="personal">Personal</option>
                                <option value="family">Family</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="table-responsive">
                        <table className="table table-hover">
                          <thead className="thead-default">
                            <tr>
                              <th>Diseases</th>
                              <th>Status</th>
                            </tr>
                          </thead>
                          {/* <tbody>
                            {viewType == "personal"
                              ? therapistsDiesese &&
                                therapistsDiesese.map((each, i) => (
                                  <tr key={i}>
                                    <td data-label="Disease">{each.name}</td>
                                    <td data-label="status">
                                      <div class="diseaseList">
                                        <div class="diseaseQue">
                                          <input
                                            type="radio"
                                            id={"yes" + i}
                                            name={each.name}
                                            value="yes"
                                            onChange={e =>
                                              this.handleRadio(e, each.name)
                                            }
                                          />
                                          <label for={"yes" + i}>Yes</label>
                                        </div>
                                        <div class="diseaseQue">
                                          <input
                                            type="radio"
                                            id={"no" + i}
                                            name={each.name}
                                            value="no"
                                            onChange={e =>
                                              this.handleRadio(e, each.name)
                                            }
                                          />
                                          <label for={"no" + i}>No</label>
                                        </div>
                                        <div class="diseaseQue">
                                          <input
                                            type="radio"
                                            id={"unknown" + i}
                                            name={each.name}
                                            value="unknown"
                                            onChange={e =>
                                              this.handleRadio(e, each.name)
                                            }
                                          />
                                          <label for={"unknown" + i}>
                                            Unknown
                                          </label>
                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                                ))
                              : therapistsfamily &&
                                therapistsfamily.map((each, i) => (
                                  <tr>
                                    {
                                      <td data-label="Disease">
                                        {each.family}
                                      </td>
                                    }
                                    <td data-label="status">
                                      <div class="diseaseList">
                                        <div class="diseaseQue">
                                          <input
                                            type="radio"
                                            id={"yes" + i}
                                            name={each.family}
                                            onChange={e =>
                                              this.handleRadio(e, each.family)
                                            }
                                          />
                                          <label for={"yes" + i}>Yes</label>
                                        </div>
                                        <div class="diseaseQue">
                                          <input
                                            type="radio"
                                            id={"no" + i}
                                            name={each.family}
                                            onChange={e =>
                                              this.handleRadio(e, each.family)
                                            }
                                          />
                                          <label for={"no" + i}>No</label>
                                        </div>
                                        <div class="diseaseQue">
                                          <input
                                            type="radio"
                                            id={"unknown" + i}
                                            name={each.family}
                                            onChange={e =>
                                              this.handleRadio(e, each.family)
                                            }
                                          />
                                          <label for={"unknown" + i}>
                                            Unknown
                                          </label>
                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                                ))}
                          </tbody> */}
                        </table>
                      </div>
                    </div>
                    {/* <button onClick={this.handleSubmit}> save</button> */}
                  </div>
                )}
                {tabtwo && (
                  <div className="tab-menu-content-section">
                    <div
                      id="content-1"
                      className={tabtwo ? "d-block" : "d-none"}
                    >
                      <button
                        type="button"
                        onClick={this.adddiseasespop}
                        className="commonBtn2 float-right mb-2"
                      >
                        <i className="fa fa-plus" /> Add New Entry
                      </button>
                      <div className="table-responsive">
                        <table className="table table-hover">
                          <thead className="thead-default">
                            <tr>
                              <th>Name</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            {otherDiseases &&
                              otherDiseases.map((each, i) => (
                                <tr>
                                  <td data-label="#">{i + 1}</td>
                                  <td data-label="Name">{each.name}</td>
                                  <td data-label="Action">
                                    <a
                                      title="Edit"
                                      class="editBtn"
                                      /* data-toggle="modal"
                                      data-target="#myModal" */
                                      onClick={() => {
                                        this.setState({
                                          adddiseases: !this.state.adddiseases,
                                          name: each.name,
                                          title: each.title,
                                          description: each.description,
                                          type: "edit"
                                        });
                                      }}
                                    >
                                      <i class="fa fa-edit" />
                                    </a>
                                    <a
                                      title="Delete"
                                      class="deleteBt"
                                      onClick={() => this.handleDelete(each)}
                                    >
                                      <i class="fa fa-trash" />
                                    </a>
                                  </td>
                                </tr>
                              ))}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                )}
              </div>

              <div
                className={adddiseases ? "modal d-block" : "modal"}
                id="myModal"
              >
                <div className="modal-dialog">
                  <div className="modal-content modalPopUp">
                    <div className="modal-header borderNone">
                      <h5 className="modal-title">Add Diseases</h5>
                      <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        onClick={this.modalPopUpClose}
                      >
                        <i className="fas fa-times" />
                      </button>
                    </div>
                    <div className="modal-body">
                      <form onSubmit={this.handleSubmit}>
                        <div className="row">
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">
                                Title for list entry (e.g. Headache)
                              </label>
                              <input
                                type="text"
                                class="form-control"
                                name="name"
                                value={this.state.name}
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">Name</label>
                              <input
                                type="text"
                                class="form-control"
                                name="title"
                                value={this.state.title}
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">Description</label>
                              <input
                                type="text"
                                class="form-control"
                                name="description"
                                value={this.state.description}
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <button type="submit" class="commonBtn float-right">
                              Create
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              {tabthree && (
                <div id="content-2" className={tabthree ? "d-block" : "d-none"}>
                  <button
                    type="button"
                    className="commonBtn2 float-right mb-2"
                    onClick={() =>
                      this.setState({
                        uploadPopup: !this.state.uploadPopup
                      })
                    }
                  >
                    <i className="fa fa-plus" /> <span> Upload new files</span>
                  </button>
                  <div className="table-responsive">
                    <table className="table table-hover">
                      <thead className="thead-default">
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          {/* <th>Type</th>
                              <th>Size</th>
                              <th>Date</th>
                              <th>Action</th> */}
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td data-label="#"> + 1</td>
                          <td data-label="Name">
                            {" "}
                            <a target="_blank">upload</a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  therapistsDiesese: state.healthrecordReducer.therapistsDiesese,
  therapistsfamily: state.healthrecordReducer.therapistsfamily
});

const mapDispatchToProps = dispatch => ({
  diseasesList: () => dispatch(diseasesList()),
  familyList: () => dispatch(familyList()),
  adddiseases: payload => dispatch(adddiseases(payload)),
  editdiseases: payload => dispatch(editdiseases(payload)),
  deletediseases: payload => dispatch(deletediseases(payload)),
  addupdateDiseases: payload => dispatch(addupdateDiseases(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Diseases);
