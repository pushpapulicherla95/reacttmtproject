import React, { Component } from "react";
import { connect } from "react-redux";
import { Logdatas } from "../../../service/admin/Patient/action"
import { filter as _filter } from 'lodash';
import { onlyNumbers, onlyAlphabets, onlyDate } from "../../patient/personal/KeyValidation";
import moment from 'moment'

class Logdata extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 1,
            tabone: true,
            tabtwo: false,
            id: "",
            searchdate:moment().format("DD-MM-YYYY"),
            logLists: [],
            mainScheduleList: []
        };
    } 
    // searchLogdata = () => {
    //    const {searchdate}=this.state;
    //     const { logList } = this.props.logdataDetails
    //     var result = [];
    //     console.log("loglist",logList)
    //     result = _filter(logList,  (list)=> {
    //         return list[1] === searchdate;
    //     })  
    //     console.log("loglist",result)  
    //         this.setState({logList: result })
    //        console.log("nbsdvfbndsf",result)
    // }
    searchLogdata = (e) => {
        const searchdate=e.target.value;
         const { logLists,mainScheduleList } = this.state;
        var result = [];
        result = _filter(mainScheduleList, function (data) {
        const searchdates=    moment(searchdate).format("DD-MM-YYYY")
            console.log("dshfjgdsfjdfhgjhgf,,,,,,,,,,,,,",data[1],searchdates)
            return data[1].includes(searchdates);
        })
        this.setState({ logLists: result })
        console.log("success",result)
    }

    tabOne = () => {
        this.setState({ tabone: true, tabtwo: false });
    };
    tabTwo = () => {
        this.setState({ tabone: false, tabtwo: true });
    };

    componentDidMount = () => {
        this.props.Logdatas(10);

    }
    componentWillReceiveProps = (nextProps) => {
        console.log("nextprops",nextProps)
        if (
            nextProps.logdataDetails &&
            nextProps.logdataDetails.logList != null &&
            nextProps.logdataDetails.logList != 0
          ){
            const { logList } = nextProps.logdataDetails;
    // console.log("dmfdgjdfgjhdjhgdfgdg........", logList)
            this.setState({ logLists: logList ,mainScheduleList:logList })
           
          }
        
      

    }
    handleChange = (e) => {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        this.props.Logdatas(value)
        console.log("values",value)
        this.setState({
            [name]: value
        });
    };
    handleChanges = (e) => {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;

        this.setState({
            [name]: value
        });

    };
    render() {
        const {
            tabone,
            tabtwo,
            id,
            searchdate,logLists,
            

        } = this.state;
        console.log("iam coming////////////////", logLists)
        const { logList } = this.props.logdataDetails
        return (
            <React.Fragment>
                <div className="mainMenuSide" id="main-content">
                    <div className="wrapper">
                        <div className="container-fluid">
                            <div className="body-content">
                                <div className="tomCard">
                                    <div className="tomCardHead">
                                        <h5 className="float-left">Log Data</h5>
                                    </div>

                                    <div className="tab-menu-content">
                                        <div class="tab-menu-title">
                                            <ul>
                                                <li className={this.state.tabone ? "menu1 active" : ""}>
                                                    <a href="javascript:void(0);" onClick={this.tabOne}>
                                                        <p>Log List</p>
                                                    </a>
                                                </li>
                                                <li className={this.state.tabtwo ? "menu1 active" : ""}>
                                                    <a href="javascript:void(0);" onClick={this.tabTwo}>
                                                        <p>Deleted Log </p>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div className="tab-menu-content-section">

                                        {tabone && (
                                            <div
                                                id="content-1"
                                                className={tabone ? "d-block" : "d-none"}
                                            >
                                                <div className="entries">
                                                    <span>Show</span>
                                                    <select
                                                        name="id"
                                                        value={this.state.id}
                                                        onChange={this.handleChange}>
                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select>
                                                    <span> entries</span>
                                                    <div className="search_log">
                                                    <input type="date" class="searchInputDate"  onChange={(e)=>this.searchLogdata(e)} onKeyPress={onlyDate} />

                                                        <div class="search_log_icon">
                                                            <i class="fas fa-search"></i>
                                                        </div>
                                                      
                                                    </div>
                                                </div>

                                                <div className="table-responsive">
                                                    <table className="table table-hover">
                                                        <thead className="thead-default">
                                                            <tr>
                                                                <th>Date</th>
                                                                <th>Time</th>
                                                                <th>API Name</th>
                                                                <th>Error</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {logLists ? (
                                                                logLists.map((each, i) => (
                                                                    <tr key={i}>
                                                                        <td data-label="Date">
                                                                            {each[1]}
                                                                        </td>
                                                                        <td data-label="Time">
                                                                            {each[2]}
                                                                        </td>
                                                                        <td data-label="Description">
                                                                            {each[3]}
                                                                        </td>
                                                                        <td data-label="Date">
                                                                            {each[4]}
                                                                        </td>

                                                                        <td data-label="Action">

                                                                            <a
                                                                                title="Delete"
                                                                                className="deleteBtn"

                                                                            >
                                                                                <i
                                                                                    className="fa fa-trash"
                                                                                    data-toggle="modal"
                                                                                    data-target="#myModal2"
                                                                                />
                                                                            </a>
                                                                            <a title="share" class="deleteBtn"><i class="fas fa-share"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                ))
                                                            ) : (
                                                                    <tr>No data available</tr>
                                                                )}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        )}





                                        {
                                            tabtwo && (
                                                <div
                                                    id="content-1"
                                                    className={tabtwo ? "d-block" : "d-none"}
                                                >


                                                    <div className="table-responsive">
                                                        <table className="table table-hover">
                                                            <thead className="thead-default">
                                                                <tr>

                                                                    <th>Deleted Logs</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {/* {patientFindingListData.length > 0 ? (
                                                             patientFindingListData.map((each, i) => (
                                                                 <tr key={i}>
                                                                     <td data-label="startDate">
                                                                         {each.title}
                                                                     </td>

                                                                     <td data-label="Action">
                                                                         <a
                                                                             title="Edit"
                                                                             className="editBtn"
                                                                             onClick={e =>
                                                                                 this.modalPopupOpen(each)
                                                                             }
                                                                         >
                                                                             <span
                                                                                 id={5}
                                                                                 className="fa fa-edit"
                                                                             />
                                                                         </a>
                                                                         <a
                                                                             title="Delete"
                                                                             className="deleteBtn"
                                                                             onClick={e =>
                                                                                 this.deleteFinding(each)
                                                                             }
                                                                         >
                                                                             <i
                                                                                 className="fa fa-trash"
                                                                                 data-toggle="modal"
                                                                                 data-target="#myModal2"         
                                                                             />
                                                                         </a>
                                                                     </td>
                                                                 </tr>
                                                             ))
                                                         ) : (
                                                                 <tr>No data available</tr>
                                                             )} */}
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            )}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({

    loginDetails: state.loginReducer.loginDetails,
    getFilesDetails: state.uploadReducer.getFilesDetails,
    logdataDetails: state.adminPatientReducer.logdataDetails
});

const mapDispatchToProps = dispatch => ({
    Logdatas: id => dispatch(Logdatas(id))

});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Logdata);