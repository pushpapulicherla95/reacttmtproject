import actionType from "../TextRobo/actionType";
import {toastr} from "react-redux-toastr" ;
import URL from "../../../asset/configUrl";
import {toastrOptions} from "../../../component/common/toasteroptions";
import axios from "axios";
const toasteroptionsNew = {
    timeOut: 1000,
    newestOnTop: true,
    position: 'top-right',
    transitionIn: 'bounceIn',
    transitionOut: 'bounceOut',
    progressBar: false,
    closeOnToastrClick: false
}
export const addTextSymptom = (info) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.SYMPTOMS_CHECKER_ADD,
        data: info,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    return axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.ADD_SYMPTOMS_SUCCESS,
                payload: res.data
            })
            dispatch(getTextSymptomList())
            toastr.success("Message", res.data.message, toastrOptions);
            return Promise.resolve()
        } else if (res.data.status == "failure") {
            toastr.error("error", res.data.message, toastrOptions);
            return Promise.reject()
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_SYMPTOMS_FAILURE,
            error: err
        })
        toastr.error("errr", toastrOptions);
        return Promise.reject()
    })
}

export const getTextSymptomList = (info) => dispatch => {
    const configs = {
        method: 'get',
        url: URL.SYMPTOMS_CHECKER_LIST,
        data: {},
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.LIST_SYMPTOMS_SUCCESS,
                payload: res.data
            })
           // toastr.success("Message", res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error("error", res.data.message, toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.LIST_SYMPTOMS_FAILURE,
            error: err
        })
        toastr.error("errr", toastrOptions);
    })
}

export const searchSymptoms = (info) => dispatch => { 
    const configs = {
        method: 'post',
        url: URL.SEARCH_SYMPTOMS,
        data: info,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.SEARCH_SYMPTOMS_SUCCESS,
                payload: res.data
            })
            //toastr.success("Message", res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error("error", res.data.message, toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.SEARCH_SYMPTOMS_FAILURE,
            error: err
        })
        toastr.error("Error on retriving symptom data", toastrOptions);
    })
}

export const updateSymptoms = (info) => dispatch => { 
    const configs = {
        method: 'post',
        url: URL.UPDATE_CAUSE,
        data: info,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
    axios(configs).then((res) => {
        const toasteroptions = {
            timeOut: 1000,
            newestOnTop: true,
            position: 'top-right',
            transitionIn: 'bounceIn',
            transitionOut: 'bounceOut',
            progressBar: false,
            closeOnToastrClick: false
        }
        toastr.success("Message", res.data.message,toasteroptions);
    }).catch(err => {   
        toastr.error("errr", toastrOptions);
    })
}

export const symptomMainList = () => dispatch => { 
    const configs = {
        method: 'get',
        url: URL.SYMPTOM_MAIN_LIST,
        data: {},
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    }
    axios(configs).then((res) => {
        dispatch({
            type : actionType.SYMPTOM_MAIN_LIST_SUCESS,
            payload : res.data
        })
    }).catch(err => {   
        dispatch({
            type : actionType.SYMPTOM_MAIN_LIST_SUCESS,
            payload : err
        })
    })
}

export const updateSymptomMain = (payload) => dispatch => { 
    const configs = {
        method: 'post',
        url: URL.UPDATE_SYMPTOM_MAIN,
        data: payload,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    }
    axios(configs).then((res) => {

    }).catch(err => {   

    })
}

//Normal Function
export const updateSymptomMainFunction = (payload ,callback) => { 
    const configs = {
        method: 'post',
        url: URL.UPDATE_SYMPTOM_MAIN,
        data: payload,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    }
    axios(configs).then((res) => {
        callback(res.data)
    }).catch(err => {   

    })
}

export const getQuestionAndOptionList = (payload) => dispatch => { 
    const configs = {
        method: 'post',
        url: URL.GET_ALL_QUESTION_OPTION,
        data: payload,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    }
    axios(configs).then((res) => {
        dispatch({
            type : actionType.QUESTION_AND_OPTION_LIST_SUCCESS,
            payload : res.data
        })
    }).catch(err => {   
        dispatch({
            type : actionType.QUESTION_AND_OPTION_LIST_SUCCESS,
            payload : err
        })
    })
}

export const updateQuestion = (payload) => dispatch => { 
    const configs = {
        method: 'post',
        url: URL.UPDATE_QUESTION,
        data: payload,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    }
    axios(configs).then((res) => {
     
    }).catch(err => {   
       
    })
}

//Normal function
export const updateOptionFunction = (payload , callback) => { 
    const configs = {
        method: 'post',
        url: URL.UPDATE_OPTION_SYMPTOM,
        data: payload,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    }
    axios(configs).then((res) => {
       callback(res.data)
    }).catch(err => {   
       
    })
}

export const deleteOption = (payload) => dispatch => { 
    const configs = {
        method: 'post',
        url: URL.DELETE_OPTION_SYMPTOM,
        data: payload,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    }
    axios(configs).then((res) => {
        dispatch(getQuestionAndOptionList({ symptomsId: payload.symptomsId })) 
    }).catch(err => {   
       
    })
}

export const deleteQuestion = (payload) => dispatch => { 
    const configs = {
        method: 'post',
        url: URL.DELETE_QUESTION,
        data: payload,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    }
    axios(configs).then((res) => {
        dispatch(getQuestionAndOptionList({ symptomsId: payload.symptomsId })) 
    }).catch(err => {   
       
    })
}