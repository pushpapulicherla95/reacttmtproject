//healthcareregister
import React, { Component } from "react";
import { getUserType, healthcareRegister, hospitalRegister } from "../../service/login/action";
import { connect } from "react-redux";
import validator from "validator";
import { NavLink } from "react-router-dom";

class HealthCareRegistration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userList: [],
      userTypeId: "",
      email: "",
      password: "",
      firstName: "",
      lastName: "",
      confirmpassword: "",
      errors: {
        firstName: "",
        lastName: "",
        email: "",
        userTypeId: "",
        password: "",
        confirmpassword: ""
      },
      firstNameValid: false,
      lastNameValid: false,
      emailValid: false,
      userTypeIdValid: false,
      passwordValid: false,
      confirmpasswordValid: false,
      firstValid: false,
      fileName: "",
      fileUrl: ""
    };
  }
  componentWillMount() {
    this.props.getUserType();
  }
  componentWillReceiveProps(nextProps) {
    console.log("nextProps>>>",nextProps);
    if (
      nextProps.userTypeList &&
      nextProps.userTypeList.list != null &&
      nextProps.userTypeList.list.length != 0
    ) {
      this.setState({ userList: nextProps.userTypeList.list });
    }
    if (
      nextProps.registerDetails &&
      nextProps.registerDetails != "" &&
      nextProps.registerDetails.status == "success"
    ) {
      this.props.history.push("/login/healthcareprovider");
    }
  }

  fileupload = e => {
    var f = e.target.files[0]; // FileList object
    var reader = new FileReader();
    var base64String;
    // Closure to capture the file information.
    reader.onload = (theFile => {
      return e => {
        var binaryData = e.target.result;
        //Converting Binary Data to base 64
        base64String = window.btoa(binaryData);
        console.log("base64String", base64String);
        this.setState({
          fileName: f.name,
          fileUrl: base64String
        });
      };
    })(f);
    // Read in the image file as a data URL.
    reader.readAsBinaryString(f);
  };

  handleChange = e => {
    e.preventDefault();
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { firstName, lastName, email, password, userTypeId } = this.state;
    const registerinfo = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password,
      userTypeId: parseInt(userTypeId) || "",
      fileName: this.state.fileName,
      z: this.state.fileUrl
    };
    if (userTypeId == "3") {
      console.log("userTypeId>>>",userTypeId);
      let info = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password,
        userTypeId: parseInt(userTypeId) || "",
      }
      this.props.hospitalRegister(info);

    }else{
      this.props.healthcareRegister(registerinfo);

    }
  };
  //validation
  validateField(fieldName, value) {
    const {
      errors,
      firstNameValid,
      lastNameValid,
      emailValid,
      userTypeIdValid,
      passwordValid,
      confirmpasswordValid,
      confirmpassword,
      password
    } = this.state;
    let emailvalid = emailValid;
    let firstNamevalid = firstNameValid;
    let lastNamevalid = lastNameValid;
    let passwordvalid = passwordValid;
    let userTypeIdvalid = userTypeIdValid;
    let confirmpasswordvalid = confirmpasswordValid;
    let fieldValidationErrors = errors;
    switch (fieldName) {
      case "email":
        emailvalid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        fieldValidationErrors.email = emailvalid
          ? ""
          : "Please enter valid email address.";
        break;
      case "firstName":
        firstNamevalid = !(value.length < 3 || value.length > 20);
        fieldValidationErrors.firstName = firstNamevalid
          ? ""
          : "First Name should be 3 to 20 characters";
        break;
      case "lastName":
        lastNamevalid = !(value.length < 3 || value.length > 20);
        fieldValidationErrors.lastName = lastNamevalid
          ? ""
          : "Last Name should be 3 to 20 characters";
        break;
      case "password":
        passwordvalid = value.length !== 0;
        fieldValidationErrors.password = passwordvalid
          ? ""
          : "Please enter valid password.";
        break;
      case "userTypeId":
        userTypeIdvalid = !(value.length < 1);
        fieldValidationErrors.userTypeId = userTypeIdvalid
          ? ""
          : "Please select the userTypeId";
        break;
      // case "password":
      // passwordvalid =
      //   value.length >= 8 &&
      //   value.match(
      //     /(?=.*[_!@#$%^&*-])(?=.*\d)(?!.*[.\n])(?=.*[a-z])(?=.*[A-Z])^.{8,}$/
      //   );
      // fieldValidationErrors.password = passwordvalid
      //   ? ""
      //   : "Must contain at least one uppercase letter, one lowercase letter, one number, one special character and a minimum of 8 characters";
      // break;
      case "confirmpassword":
        confirmpasswordvalid = validator.equals(value, password);
        fieldValidationErrors.confirmpassword = confirmpasswordvalid
          ? ""
          : "Password does not match";
        break;
    }
    if (fieldName === "password") {
      if (confirmpassword !== "") {
        if (value !== confirmpassword) {
          fieldValidationErrors.confirmpassword = "Password does not match.";
        } else {
          fieldValidationErrors.confirmpassword = "";
        }
      }
    } else if (fieldName === "confirmpassword") {
      if (value !== password) {
        fieldValidationErrors.confirmpassword = "Password does not match.";
      } else {
        fieldValidationErrors.confirmpassword = "";
      }
    }
    this.setState(
      {
        errors: fieldValidationErrors,
        emailValid: emailvalid,
        passwordValid: passwordvalid,
        firstNameValid: firstNamevalid,
        lastNameValid: lastNamevalid,
        confirmpasswordValid: confirmpasswordvalid
      },
      this.validateForm
    );
  }
  validateForm() {
    const {
      firstNameValid,
      lastNameValid,
      emailValid,
      passwordValid,
      confirmpasswordValid
    } = this.state;
    this.setState({
      firstValid:
        lastNameValid &&
        firstNameValid &&
        emailValid &&
        passwordValid &&
        confirmpasswordValid
    });
  }
  render() {
    const {
      userList,
      email,
      firstName,
      lastName,
      password,
      confirmpassword,
      userTypeId,
      firstValid,
      errors
    } = this.state;
    // console.log("yji.satate",this.state)
    return (
      <React.Fragment>
        <div className="loginInner registerInner d-flex">
          <div className="loginFormContent">
            <h4 className="popupTitle">Register as a HealthCare Provider</h4>
            <form>
              <div className="row">
                <div className="col-xl-6  col-lg-6 col-md-6 col-sm-12 col-12">
                  <div className="form-group emailIcon">
                    <label>E-Mail* </label>
                    <input
                      type="text"
                      className="form-control commonInput"
                      name="email"
                      value={email}
                      onChange={this.handleChange}
                      placeholder="Email"
                    />
                    <div style={{ color: "red" }}>{errors.email}</div>
                  </div>
                  <div className="form-group pass">
                    <label>Password* </label>
                    <input
                      type="password"
                      className="form-control commonInput"
                      name="password"
                      value={password}
                      onChange={this.handleChange}
                      placeholder="Password"
                    />
                    <div style={{ color: "red" }}>{errors.password}</div>
                  </div>
                  <div className="form-group pass">
                    <label>Confirm Password* </label>

                    <input
                      type="password"
                      className="form-control commonInput"
                      name="confirmpassword"
                      value={confirmpassword}
                      onChange={this.handleChange}
                      placeholder="Confirm Password"
                    />
                    <div style={{ color: "red" }}>{errors.confirmpassword}</div>
                  </div>
                </div>
                <div className="col-xl-6  col-lg-6 col-md-6 col-sm-12 col-12">
                  <div className="form-group name">
                    <label>First Name </label>
                    <input
                      type="text"
                      className="form-control commonInput"
                      name="firstName"
                      value={firstName}
                      onChange={this.handleChange}
                      placeholder="First Name"
                    />
                    <div style={{ color: "red" }}>{errors.firstName}</div>
                  </div>

                  <div className="form-group name">
                    <label>Last Name </label>
                    <input
                      type="text"
                      className="form-control commonInput"
                      name="lastName"
                      value={lastName}
                      onChange={this.handleChange}
                      placeholder="Last Name"
                    />
                    <div style={{ color: "red" }}>{errors.lastName}</div>
                  </div>

                  <div className="form-group ">
                    <label>User Type</label>

                    <select
                      className="form-control commonInput"
                      name="userTypeId"
                      onChange={this.handleChange}
                      id="sel1"
                    >
                      <option>select </option>
                      {userList.length > 0 &&
                        userList.map((link, i) => (
                          <option
                            name="userTypeId"
                            key={i}
                            value={link.userTypeId}
                          >
                            {link.userType}
                          </option>
                        ))}
                    </select>
                    <div style={{ color: "red" }}>{errors.userTypeId}</div>
                  </div>
                </div>

                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div className="form-group">
                    <p className="cont">
                      As care provider or cost unit we need documents of your
                      personal qualification. Please attach the proper
                      documents. We'll check the documents, before we open your
                      account.
                    </p>
                    <div className="upload-btn">
                      <label className="commonBtn text-center upload">
                        Upload Files
                        <input
                          type="file"
                          id="files"
                          accept=".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf"
                          onChange={this.fileupload}
                        />
                      </label>
                    </div>
                    <div className="form-group">
                      <input
                        className="inp-cbx"
                        id="cbxh2"
                        type="checkbox"
                        style={{ display: " none" }}
                      />
                      <label className="cbx" htmlFor="cbxh2">
                        <span>
                          <svg width="12px" height="10px" viewBox="0 0 12 10">
                            <polyline points="1.5 6 4.5 9 10.5 1" />
                          </svg>
                        </span>
                        <span>
                          I'm interested to offer services on the
                          tomatomedical-platform. When you accept this, we'll
                          send you further infos.
                        </span>
                      </label>
                    </div>
                    <div className="form-group">
                      <input
                        className="inp-cbx"
                        id="cbxh"
                        type="checkbox"
                        style={{ display: " none" }}
                      />
                      <label className="cbx" htmlFor="cbxh">
                        <span>
                          <svg width="12px" height="10px" viewBox="0 0 12 10">
                            <polyline points="1.5 6 4.5 9 10.5 1" />
                          </svg>
                        </span>
                        <span>
                          I have read the{" "}
                          <a href="">
                            terms of use and the data privacy statement{" "}
                          </a>
                          and accept them.
                        </span>
                      </label>
                    </div>
                    <div className="form-group">
                      <input
                        className="inp-cbx"
                        id="cbxh1"
                        type="checkbox"
                        style={{ display: " none" }}
                      />
                      <label className="cbx" htmlFor="cbxh1">
                        <span>
                          <svg width="12px" height="10px" viewBox="0 0 12 10">
                            <polyline points="1.5 6 4.5 9 10.5 1" />
                          </svg>
                        </span>
                        <span>
                          I have read the{" "}
                          <a href="">general terms and conditions</a> and accept
                          them.
                        </span>
                      </label>
                    </div>
                    <div className="form-group text-center">
                      <button
                        type="button"
                        className="btn loginBtn"
                        onClick={this.handleSubmit}
                        disabled={!firstValid}
                      >
                        Submit
                      </button>
                    </div>

                    <p className="linkToReg">
                      Already have an account ?
                      <NavLink to="/login/healthcareprovider">
                        Login Here
                      </NavLink>
                    </p>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  userTypeList: state.loginReducer.userTypeList,
  registerDetails: state.loginReducer.registerDetails
});

const mapDispatchToprops = dispatch => ({
  healthcareRegister: registerinfo =>
    dispatch(healthcareRegister(registerinfo)),
  getUserType: () => dispatch(getUserType()),
  hospitalRegister: info => dispatch(hospitalRegister(info))
});

export default connect(
  mapStateToProps,
  mapDispatchToprops
)(HealthCareRegistration);
