
var path = require('path')
const express = require('express');
//const code = require('../config/CountryCodes');
const stripeRoute = require('./stripeRoute');
const subscriptionRoute = require('./subscriptionRoute');
const emailRouter = require('./emailRoute');
const smsRoute = require('./smsRoute');
const webhookRoute = require('./webhookRoute')
const socialRoute = require('./socialRoute')
const chatRoute = require('./chatRoute')

// const {
//     pick: _pick,
//     map: _map,
//     partialRight: _partialRight,
// } = require("lodash");

// const morgan = require('morgan')
const routes = (app) => {

   // app.use(morgan(':date[web] :method :url :response-time :res[header]'))
    // app.use(morgan(':date[web] :method :url :response-time :res[Origin]', {
    //     skip: function (req, res) {
    //         return res.statusCode < 400
    //     }, stream: process.stderr
    // }));

    // app.use(morgan(':date[iso] :method :url :response-time :res[Origin]', {
    //     skip: function (req, res) {
    //         return res.statusCode >= 400
    //     }, stream: process.stdout
    // }));
    
    app.use('/API/subscription',checkAuth,subscriptionRoute)
    app.use('/API/webhook',webhookRoute)
    app.use('/API/payment',stripeRoute)
    app.use("/API/email", emailRouter);
    app.use('/API/sms',smsRoute)
    app.use('/API/social',socialRoute)
    app.use('/API/chat',chatRoute)
    // if(process.env.NODE_ENV === 'production') {
    //     const root = path.join(__dirname, '../build');
    //     app.use(express.static(root));
    //     app.use('/',
    //       (req, res) => {
    //         res.sendFile(path.resolve(__dirname, root, 'index.html'))
    //       })
    // }  
}

const handleInit = async (req, res) => {
    const result = _map(code, _partialRight(_pick, ["name", "alpha2Code", "callingCodes", "currencies"]));

    const options = {
        // domain: 'localhost',
        maxAge: 1000 * 60 * 6000, // would expire after 15 minutes
        httpOnly: true, // The cookie only accessible by the web server
        // signed: true // Indicates if the cookie should be signed
    }

    // Set cookie
    res
        .cookie("test", "xeni", options) //
        .status(200)
        .send(result);
};

const checkAuth = (req, res, next) => {
    const secretCode = req.headers['secret-code'];
    if (!secretCode || secretCode !== process.env.CLIENT_SECRET_CODE) {
        return res.json({
            status: false,
            message: 'authentication failed'
        });
    } else {
        next()
    }
};

module.exports = routes;