
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { register } from '../../service/login/action'
import Login from '../login/Login';
import axios from 'axios';
import { NavLink } from "react-router-dom";

class Registration extends Component {
    constructor(props) {
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            mobileNo: '',
            password: '',
            confirmpassword: '',
            userIdType: '',
            cityId: '',
            state: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }
    handleSubmit(e) {
        e.preventDefault();
        let registerinfo = {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            mobileNo: this.state.mobileNo,
            password: this.state.password,
           confirmpassword: this.state.confirmpassword,
           userIdType: "1",
           cityId: "2",
           state: this.state.state
        };
        this.props.register(registerinfo);
     
    }
    render() {
        const { firstName, lastName, email, mobileNo, password, confirmpassword, userIdType, cityId, state } = this.state;
        return (

            <div className="cui-login">
                <div className="cui-login-header">
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="cui-login-header-logo">
                                <a href="javascript:void(0);"><img src="images/logo.jpg" alt="tomato Logo" /></a>
                            </div>
                        </div>
                        <div className="col-lg-6"></div>
                    </div>
                </div>
                <div className="cui-login-block">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="cui-login-block-inner">
                                <div className="cui-login-block-form">
                                    <h4 className="text-uppercase text-center"><strong>Sign Up</strong></h4>
                                    <br />
                                    <form id="form-validation" name="form-validation">
                                        <div className="form-group">
                                            <label className="form-label">Frist Name</label><sup>*</sup>
                                            <input id="validation-email" className="form-control" placeholder="Enter your firstname" name="firstName" type="text" value={firstName} onChange={this.handleChange} /></div>
                                        <div className="form-group">
                                            <label className="form-label">Last Name</label><sup>*</sup>
                                            <input id="validation-email" className="form-control" placeholder="Enter your last name" name="lastName" type="text" value={lastName} onChange={this.handleChange} /></div>
                                        <div className="form-group">
                                            <label className="form-label">Email</label><sup>*</sup>
                                            <input id="validation-email" className="form-control" placeholder="Enter your Email" name="email" type="text" value={email} onChange={this.handleChange} /></div>
                                        <div className="form-group">
                                            <label className="form-label">Mobile</label><sup>*</sup>
                                            <input id="validation-email" className="form-control" placeholder="Enter your Email" name="mobileNo" type="text" value={mobileNo} onChange={this.handleChange} /></div>
                                        <div className="form-group">
                                            <label className="form-label">Password</label><sup>*</sup>
                                            <input id="validation-password" className="form-control password" name="password" type="password" placeholder="Password" value={password} onChange={this.handleChange} /></div>
                                        <div className="form-group">
                                            <label className="form-label">Confirm Password</label><sup>*</sup>
                                            <input id="validation-password" className="form-control password" name="confirmpassword" type="password" placeholder="confirmpassword" value={confirmpassword} onChange={this.handleChange} /></div>
                                        <div className="form-group">
                                            <label className="form-label">User Type </label><sup>*</sup>
                                            <select className="form-control" name="userIdType" value={userIdType} onChange={this.handleChange} >
                                                <option>--Select User Type--</option>
                                                <option>Patient</option>
                                                <option>Doctor</option>
                                                <option>Nursing home</option>
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">City</label><sup>*</sup>
                                            <select className="form-control" name="cityId" value={cityId} onChange={this.handleChange} >
                                                <option>--Select City--</option>
                                                <option>City 1</option>
                                                <option>City 2</option>
                                                <option>City 3</option>
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label">State</label><sup>*</sup>
                                            <select className="form-control" name="state" value={state} onChange={this.handleChange}>
                                                <option>--Select State--</option>
                                                <option>State 1</option>
                                                <option>State 2</option>
                                                <option>State 3</option>
                                            </select>
                                        </div>
                                        <div className="form-actions">
                                            <button type="button" className="btn commonBtn mr-3" onClick={this.handleSubmit}>Sign Up</button>
                                            <NavLink to="/register/patient">Back to Login</NavLink>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="cui-login-footer text-center">
                </div>
            </div>

        )
    }
}
const mapStateToProps = state => ({
    registerDetails: state.loginReducer.registerDetails

});

const mapDispatchToProps = dispatch => ({
    register: (registerinfo) => dispatch(register(registerinfo)),

});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Registration);