import React from "react";
import { getSearchDoctorDetails } from "../../../service/medicalSpecialist/action";
import { connect } from "react-redux";
import { bookAppointmentAPICall } from "../../../service/appointment/action";
import Logout from '../../common/Logout'
import { filter as _filter } from 'lodash';
import moment from 'moment'

class MedicalSpecialList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      docSearchResult: "",
      docSearchFilter: "",
      showProfile: false,
      showSchedule: true,
      dateFilter: "Today"
    }
  }
  showProfilepop = () => {
    this.setState({ showProfile: true, showSchedule: false });

  }
  showSchedulepop = () => {
    this.setState({ showProfile: false, showSchedule: true });

  }
  componentWillMount() {

    const { userInfo } = this.props.loginDetails;

    if (this.props.location.state) {
      const { doctorSpecialistId, doctorSpecialistName } = this.props.location.state;
      const searchdata = {
        specialistId: doctorSpecialistId,
        specialistName: doctorSpecialistName
      }
      this.props.getSearchDoctorDetails(searchdata);

    }
  }
  componentWillReceiveProps = nextProps => {
    const { doctorSpecialistId, doctorSpecialistName } = this.props.location.state;
   console.log("logindetalis",this.props.loginDetails)
    if (nextProps.searchDoctorDetails) {
      const { therapists } = nextProps.searchDoctorDetails;
      console.log(
        "...................................", therapists
      )
      var terapistResult = therapists && _filter(therapists, function (list) {

        return doctorSpecialistName == list.firstName;
      })

      var result = (terapistResult) && _filter(terapistResult[0].schedules, function (list) {
        let todaydate = moment(new Date()).format("YYYY-MM-DD");
        return list.appointmentDate == todaydate;
      })
      console.log(result, terapistResult[0])
      this.setState({
        docSearchResult: terapistResult,
        docSearchFilter: result.map(val => { return { ...val, doctorEmail: terapistResult[0] && terapistResult[0].email } })
      })
    }
  }
  bookAppointment = (e) => {
    console.log("e", e);
    const { doctorSpecialistId, doctorSpecialistName } = this.props.location.state;
    const email = this.props.loginDetails.userInfo.email;
    const { userInfo } = this.props.loginDetails;
    const appointmentInfo = {
      scheduleId: e.scheduleId,
      patientId: userInfo.userId,
      doctorId: e.doctorId,
      status: "booked",
      scheduleType: e.meetingType,
      specialistId: doctorSpecialistId,
      specialistName: doctorSpecialistName,
      appointmentDate: e.appointmentDate,
      endTime: e.endTime,
      startTime: e.startTime,
      email: email,
      doctorMail: e.doctorEmail

    };
    this.props.bookAppointmentAPICall(appointmentInfo);
  }

  handleDateFilter = (day) => {
    const { docSearchResult } = this.state;
    let todaydate = moment(new Date()).format("YYYY-MM-DD");
    var new_date = moment(new Date()).add(7, 'days');

    this.setState({ dateFilter: day })
    var result = (docSearchResult && docSearchResult[0].schedules) && _filter(docSearchResult[0].schedules, function (list) {
      if (day == "Today") {
        return list.appointmentDate == todaydate;
      } else {
        return list.appointmentDate < new_date.format;
      }

    })
    this.setState({
      docSearchFilter: result
    })
  }
  render() {
    const { docSearchResult, showProfile, showSchedule, docSearchFilter, dateFilter } = this.state;
    const { userInfo } = this.props.loginDetails;
    console.log("profilepic", userInfo.profilePic);
    console.log("profilepic");


    return (
      <div className="inner-pageNew">
        <section>
          <header className="header">
            <div className="brand">
              <a href="" className="backArrow">
                <i className="fas fa-chevron-left" />
              </a>
              <div className="mobileLogo">
                <a href="" className="logo">
                  <img src="../images/logo.jpg" />{" "}
                </a>
              </div>
            </div>
            <div className="top-nav">
              <div className="dropdown float-right mr-2">
                <a className="dropdown-toggle" data-toggle="dropdown">
                  Patient Name
              </a>
                <Logout />

              </div>
            </div>
          </header>

          <aside className="sideMenu">
            <ul className="sidemenuItems">
              <li>
                <a href="javascript:void(0)" onClick={() =>
                  this.props.history.push({
                    pathname: "/submenu",
                    state: { submenuType: "Personal" }
                  })
                } title="Personal">
                  <img src="../images/icon2-color.png" />
                </a>
                <div className="menu-text">
                  <span>Personal</span>
                </div>
              </li>
              <li>
                <a href="javascript:void(0)" onClick={() =>
                  this.props.history.push({
                    pathname: "/submenu",
                    state: { submenuType: "Telemedicine" }
                  })
                } title="Telemedicine">
                  <img src="../images/icon5-color.png" />
                </a>
                <div className="menu-text">
                  <span>Telemedicine</span>
                </div>
              </li>
              <li>
                <a href="javascript:void(0)" onClick={() =>
                  this.props.history.push({
                    pathname: "/submenu",
                    state: { submenuType: "HealthRecord" }
                  })
                } title="Health Records">
                  <img src="../images/icon1-color.png" />
                </a>
                <div className="menu-text">
                  <span>Health Records</span>
                </div>
              </li>
              <li>
                <a href="javascript:void(0)" title="Pharmacy">
                  <img src="../images/icon4-color.png" />
                </a>
                <div className="menu-text">
                  <span>Pharmacy</span>
                </div>
              </li>
              <li>
                <a href="javascript:void(0)" onClick={() =>
                  this.props.history.push({
                    pathname: "/submenu",
                    state: { submenuType: "MedicalSpecialist" }
                  })
                } title="Services">
                  <img src="../images/icon7-color.png" />
                </a>
                <div className="menu-text">
                  <span>Find Your Medical Specialist</span>
                </div>
              </li>
              <li>
                <a href="javascript:void(0)" onClick={() =>
                  this.props.history.push({
                    pathname: "/submenu",
                    state: { submenuType: "MedicalSpecialist" }
                  })
                } title="Services">
                  <img src="../images/icon7-color.png" />
                </a>
                <div className="menu-text">
                  <span>Services</span>
                </div>
              </li>
              <li>
                <a href="javascript:void(0)" title="Share Data">
                  <img src="../images/icon6-color.png" />
                </a>
                <div className="menu-text">
                  <span>Share data</span>
                </div>
              </li>
            </ul>
          </aside>
          <div className="mainMenuSide" id="main-content">
            <div className="wrapper">
              <div className="container-fluid">
                <div className="body-content">
                  <div className="row">
                    <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                      <div className="doSection card">
                        <div className="row w-100">
                          <div className="col-lg-3 col-md-3 col-sm-12 col-12">
                            <div className="doctorImage">
                              <img src={`https://data.tomatomedical.com/profilepicture/${userInfo.userId}_.png`} />
                            
                            </div>
                            <div className="doc-title">
                              <p clasName="title-name">Dr.{docSearchResult && docSearchResult[0].firstName}</p>
                              <p>{docSearchResult && docSearchResult[0].qualificaion} </p>
                            </div>
                            <div className="docButton">
                              <ul>
                                <li className="clr10025" id="appointment">
                                  <a title="Get Appointment"
                                    // onClick={() => this.setState({ showProfile: !showProfile, showSchedule: !showSchedule })}
                                    onClick={this.showSchedulepop}
                                  >
                                    <i className="far fa-calendar-alt" />{" "}
                                    <span>Book</span>
                                  </a>
                                </li>
                                <li className="clr10027" id="profile">
                                  <a title="View Profile"
                                    //  onClick={() => this.setState({ showProfile: !showProfile, showSchedule: !showSchedule })}
                                    onClick={this.showProfilepop}
                                  >
                                    {" "}
                                    <i className="fas fa-user-tie" />{" "}
                                    <span>Profile</span>
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div className="col-lg-9 col-md-9 col-sm-12 col-12">
                            {showSchedule && <div className="doctor-appoitment">
                              <div className="doc-title">
                                <div className="date-section">
                                  <div className="prev-arrow" id="slider1prev" onClick={e => this.handleDateFilter("Today")} >
                                    {" "}
                                    <i className="fas fa-chevron-left" />
                                  </div>
                                  <div className="day-section">
                                    <p className="text" id="first_one">
                                      {dateFilter}
                                    </p>
                                    <p
                                      className="text"
                                      id="second_one"
                                      style={{ display: "none" }}
                                    >
                                      Weekly
                                  </p>

                                  </div>
                                  <div className="next-arrow" id="slider1next" onClick={e => this.handleDateFilter("Weekly")}>
                                    {" "}
                                    <i className="fas fa-chevron-right" />
                                  </div>
                                </div>
                                <table className="table docTable">
                                  <thead>
                                    <tr>
                                      <th>Time slot</th>
                                      <th>Date</th>
                                      <th>Type</th>
                                      <th>Appointment</th>

                                    </tr>
                                  </thead>
                                  <tbody>
                                    {
                                      (docSearchFilter.length > 0) ?
                                        (docSearchFilter.map((value, key) => (
                                          <tr key={key}>
                                            <td data-label="Time slot">
                                              {value.startTime}-{value.endTime}
                                            </td>
                                            <td>{value.appointmentDate}</td>
                                            <td>{value.meetingType}</td>
                                            <td data-label="Appointment">
                                              <a className={value.status == "NA" ? "disabled" : ""} onClick={e => this.bookAppointment(value)}>Book</a>
                                            </td>
                                          </tr>
                                        ))) : <tr>No schedule List</tr>
                                    }

                                  </tbody>
                                </table>
                              </div>
                            </div>}

                            {showProfile && <div className="tomCard1" >
                              <div className="tomCardBody">
                                <div className="tab-menu-content">
                                  <div className="content1">
                                    <div className="row">
                                      <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div className="doc-title">
                                          <p>
                                            <span className="title-name clr10022">
                                              clinicName:
                                          </span>{" "}
                                            {docSearchResult && docSearchResult[0].clinicName}{" "}                                          </p>

                                          <p>
                                            <span className="title-name clr10022">
                                              Phone:
                                          </span>{" "}
                                            {docSearchResult && docSearchResult[0].phone}{" "}
                                          </p>
                                          <p>
                                            <span className="title-name clr10022">
                                              Address:
                                          </span>{" "}
                                            {docSearchResult && docSearchResult[0].address}
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <hr />
                                  <div className="content-2">
                                    <div className="row">
                                      <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div className="doc-title">
                                          <h6 className="title-name">
                                            Experience
                                        </h6>
                                          <p>
                                            <span className="title-name clr10022">
                                              {docSearchResult && docSearchResult[0].experience}
                                            </span>
                                          </p>
                                          <h6 className="title-name">
                                            About
                                        </h6>
                                          <p>
                                            {docSearchResult && docSearchResult[0].description}
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            }
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  searchDoctorDetails: state.medicalSpecialistReducer.searchDoctorDetails,
  loginDetails: state.loginReducer.loginDetails,
})
const mapDispatchToProps = dispatch => ({
  getSearchDoctorDetails: searchdata => dispatch(getSearchDoctorDetails(searchdata)),
  bookAppointmentAPICall: appointmentInfo => dispatch(bookAppointmentAPICall(appointmentInfo))
})
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MedicalSpecialList);
