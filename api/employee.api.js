const express = require('express');
const businessRoutes = express.Router();


businessRoutes.get('/', function (req, res) {
    connection.query('select * from test', function (error, results, fields) {
       if (error) throw error;
       res.end(JSON.stringify(results));
     });
 });
  
 // api to get a single employee data
 businessRoutes.get('/getById/:id', function (req, res) {
    console.log(req);
    connection.query('select * from test where id=?', [req.params.id], function (error, results, fields) {
       if (error) throw error;
       res.end(JSON.stringify(results));
     });
 });
  
 // api to create a new record into mysql database
 businessRoutes.post('/create', function (req, res) {
    var postData  = req.body;
    connection.query('INSERT INTO test SET ?', postData, function (error, results, fields) {
       if (error) throw error;
       res.end(JSON.stringify(results));
     });
 });
  
 //api to update record into mysql database
 businessRoutes.put('/update', function (req, res) {
    connection.query('UPDATE `test` SET `first_name`=?,`last_name`=?,`mobile_no`=?,`email`=? where `id`=?', [req.body.first_name,req.body.last_name, req.body.mobile_no, req.body.email, req.params.id], function (error, results, fields) {
       if (error) throw error;
       res.end(JSON.stringify(results));
     });
 });
  
 //api to delete record from mysql database
 businessRoutes.delete('/deleteById/:id', function (req, res) {
    console.log(req.body);
    connection.query('DELETE FROM test WHERE test.id=?', [req.params.id], function (error, results, fields) {
       if (error) throw error;
       res.end('Record has been deleted!');
     });
 });

 module.exports = businessRoutes;
