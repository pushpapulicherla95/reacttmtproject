import React, { Component } from 'react';
import { connect } from 'react-redux';
import socketIOClient from "socket.io-client";
import URL from '../../../../asset/configUrl';
import { chatHistoryAPICall } from "../../../../service/appointment/action";
import Loader from "react-loader-spinner";
import { updateFeedback } from "../../../../service/appointment/action";

var SocketIOFileUpload = require('socketio-file-upload');

// variables
var roomNumber, patientName;
var localStream;
var remoteStream;
var rtcPeerConnection;
var iceServers = {
    'iceServers': [
        { 'urls': 'stun:stun.services.mozilla.com' },
        { 'urls': 'stun:stun.l.google.com:19302' },
        { 'urls': 'stun:stun1.l.google.com:19302' },
        { 'urls': 'stun:stun2.l.google.com:19302' },
        { 'urls': 'stun:stun3.l.google.com:19302' },
        { 'urls': 'stun:stun4.l.google.com:19302' },
        { 'urls': 'stun:stun.services.mozilla.org' }
    ]
}
var streamConstraints = { audio: true, video: true };
var isCaller = false;

var socket = socketIOClient(URL.SOCKET_URL);
var uploader = new SocketIOFileUpload(socket, {
    chunkSize: 1024 * 1000
});

// Do something on upload progress:
uploader.addEventListener("progress", function (event) {
    var percent = event.bytesLoaded / event.file.size * 100;
});

// Do something when a file is uploaded:
uploader.addEventListener("complete", function (event) {
    socket.emit('chat-message', {
        filename: event.file.name,
        file: event.file,
        room: roomNumber,
        type: "patient",
        name: patientName,
        patientId: localStorage.getItem("patientId"),
        doctorId: localStorage.getItem("doctorId"),
        isfile: true
    });
});


socket.on('chat-message', function (event) {
    var ul = document.getElementById("chatMessagesId");
    var li = document.createElement("li");
    if (event.type === "patient") {
        li.setAttribute("class", "outgoing-msg float-right ");
    } else {
        li.setAttribute("class", "incoming-msg float-left");
    }
    var ptag = document.createElement("p");
    ptag.innerHTML = event.name;
    var ptag1 = document.createElement("p");
    if (event.isfile) {
        ptag1.innerHTML = event.filename + "<a srcObject=" + event.file + " download=" + event.filename + ">Download " + event.filename + "</a>";
    } else {
        ptag1.innerHTML = event.msg;
    }
    // var p = document.createTextNode(event.name);
    // var p1 = document.createTextNode(event.msg);
    li.appendChild(ptag1);
    li.appendChild(ptag);
    // li.appendChild(document.createTextNode(event.name+" : "+event.msg));
    ul.appendChild(li);
});

socket.on('created', function (room) {
    navigator.mediaDevices.getUserMedia(streamConstraints).then(function (stream) {
        localStream = stream;
        document.getElementById("localVideo").srcObject = stream;
        isCaller = true;
    }).catch(function (err) {
    });
});

socket.on('joined', function (room) {
    navigator.mediaDevices.getUserMedia(streamConstraints).then(function (stream) {
        localStream = stream;
        document.getElementById("localVideo").srcObject = stream;
        socket.emit('ready', roomNumber);
    }).catch(function (err) {
    });
});

socket.on('candidate', function (event) {
    var candidate = new RTCIceCandidate({
        sdpMLineIndex: event.label,
        candidate: event.candidate
    });
    rtcPeerConnection.addIceCandidate(candidate);
});

socket.on('ready', function () {
    if (isCaller) {
        rtcPeerConnection = new RTCPeerConnection(iceServers);
        rtcPeerConnection.onicecandidate = onIceCandidate;
        rtcPeerConnection.ontrack = onAddStream;
        rtcPeerConnection.addTrack(localStream.getTracks()[0], localStream);
        rtcPeerConnection.addTrack(localStream.getTracks()[1], localStream);
        rtcPeerConnection.createOffer()
            .then(sessionDescription => {
                rtcPeerConnection.setLocalDescription(sessionDescription);
                socket.emit('offer', {
                    type: 'offer',
                    sdp: sessionDescription,
                    room: roomNumber
                });
            })
            .catch(error => {
            })
    }
});

socket.on('offer', function (event) {
    if (!isCaller) {
        rtcPeerConnection = new RTCPeerConnection(iceServers);
        rtcPeerConnection.onicecandidate = onIceCandidate;
        rtcPeerConnection.ontrack = onAddStream;
        rtcPeerConnection.addTrack(localStream.getTracks()[0], localStream);
        rtcPeerConnection.addTrack(localStream.getTracks()[1], localStream);
        rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(event));
        rtcPeerConnection.createAnswer()
            .then(sessionDescription => {
                rtcPeerConnection.setLocalDescription(sessionDescription);
                socket.emit('answer', {
                    type: 'answer',
                    sdp: sessionDescription,
                    room: roomNumber
                });
            })
            .catch(error => {
            })
    }
});

socket.on('answer', function (event) {
    rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(event));
})

function onIceCandidate(event) {
    if (event.candidate) {
        socket.emit('candidate', {
            type: 'candidate',
            label: event.candidate.sdpMLineIndex,
            id: event.candidate.sdpMid,
            candidate: event.candidate.candidate,
            room: roomNumber
        })
    }
}

function onAddStream(event) {
    document.getElementById("remoteVideo").srcObject = event.streams[0];
    remoteStream = event.stream;
}


class Joinmeet extends Component {

    constructor(props) {
        super(props);

        this.state = {
            chat: '',
            enableVideo: true,
            enableAudio: true,
            enableCall: true,
            interChange: true,
            chatHistoryList: [],
            openfeedback: false,

        }

    }

    navBack = () => {
        this.props.history.push('/dashboard/patient/appointment')
    }

    componentWillMount = () => {
        const { userInfo } = this.props.loginDetails;
        const historyJSON =
        {
            meetingRoom: localStorage.getItem("meetingRoom")
        }
        this.props.chatHistoryMethod(historyJSON);
    }

    componentWillReceiveProps = () => {
        const { status, chatHistory } = this.props.listChatHistory
        this.setState({ chatHistoryList: chatHistory })

    }

    componentDidMount() {
        roomNumber = localStorage.getItem("meetingRoom")
        patientName = localStorage.getItem("patientName")
        //  let backUrl = this.props.location.state.backUrl
        //document.jitsiMeet(meetingRoom,patientName, this.navBack);
        socket.emit('create or join', roomNumber)
        uploader.listenOnInput(document.getElementById("siofu_input"));
    }

    videoMuteGo = () => {
        localStream.getVideoTracks()[0].enabled = !(localStream.getVideoTracks()[0].enabled);
        this.setState({ enableVideo: !this.state.enableVideo }, () => {
        })
    };

    audioMuteGo = () => {
        localStream.getAudioTracks()[0].enabled = !(localStream.getAudioTracks()[0].enabled);
        this.setState({ enableAudio: !this.state.enableAudio }, () => {
        })
    };

    chatGo = () => {
        this.setState({ chat: this.state.chat }, () => {
            socket.emit('chat-message', {
                msg: this.state.chat,
                room: roomNumber,
                type: "patient",
                name: patientName,
                patientId: localStorage.getItem("patientId"),
                doctorId: localStorage.getItem("doctorId"),
                isfile: false
            });
            this.setState({ chat: '' });
        })
    }

    endMeetingGo = () => {
        document.getElementById("localVideo").srcObject = null;
        localStream = null;
        socket.emit('stop', {
            type: 'stop',
            room: roomNumber
        })
        this.setState({ openfeedback: true });
    };


    fullScreenGo = () => {
        if (document.getElementById("remoteVideo").requestFullscreen) {
            document.getElementById("remoteVideo").requestFullscreen();
        } else if (document.getElementById("remoteVideo").mozRequestFullScreen) {
            document.getElementById("remoteVideo").mozRequestFullScreen(); // Firefox
        } else if (document.getElementById("remoteVideo").webkitRequestFullscreen) {
            document.getElementById("remoteVideo").webkitRequestFullscreen(); // Chrome and Safari
        }
    }

    interChangeVideo = () => {
        this.setState({
            interChange: !this.state.interChange
        })
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }


    modalPopUpClose = () => {
        this.setState({ openfeedback: false });
         this.props.history.goBack();
    }
    //

    handleFeedback = () => {
        const { anamnesis, objectives, diagnosis, treatment } = this.state;

        let feedBack = {
            "doctorId": "1",
            "patientId": "9",
            anamnesis: anamnesis,
            objectives: objectives,
            diagnosis: diagnosis,
            treatment: treatment

        }
        this.props.updateFeedback(feedBack);
        // this.props.history.goBack();
        this.setState({ openfeedback: false });
    }
    render() {
        const { anamnesis, objectives, diagnosis, treatment } = this.state;
        const { isLoading } = this.props
        const { enableVideo, enableAudio, enableCall, interChange, chat } = this.state;
        const { chatHistoryList } = this.state;


        let enableVideoClass = enableVideo ? 'video' : 'video not-allow';
        let enableAudioClass = enableAudio ? 'mic' : 'mic not-allow';
        let enableCallClass = enableCall ? 'answer not-allow' : 'answer';
        return (
            <React.Fragment>
                {isLoading && (
                    <div className="pop-mod">
                        <div className="loder-back">
                            <Loader
                                type="Circles"
                                color="#00BFFF"
                                height="100"
                                width="100"
                            />
                        </div>
                    </div>
                )}
                <div class="mainMenuSide" id="main-content">
                    <div class="wrapper">
                        <div class="container-fluid">
                            <div class="body-content">

                                <div class="row mt-3">
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                                        {!this.state.openfeedback && <div class="chat-section">
                                            <div class="video-section" onClick={this.interChangeVideo}>
                                                <video id={interChange ? "remoteVideo" : "localVideo"} autoPlay></video>
                                            </div>
                                            <div class="myVideo" onClick={this.interChangeVideo}>
                                                <video id={interChange ? "localVideo" : "remoteVideo"} autoPlay muted></video>
                                            </div>
                                            <div class="control-section">


                                                <div class="ctrlBtn">
                                                    <ul>

                                                        <li><button onClick={this.videoMuteGo} className={enableVideoClass}></button></li>
                                                        <li><button onClick={this.audioMuteGo} className={enableAudioClass}></button></li>
                                                        <li><button onClick={this.fullScreenGo} className="resize"></button></li>
                                                        <li><button onClick={this.endMeetingGo} className={enableCallClass}></button></li>
                                                    </ul>

                                                </div>
                                            </div>
                                        </div>}
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                                        <div class="txtchat">
                                            <h5>Chat</h5>
                                            <hr />

                                            <ul class="chatBody" id="chatMessagesId">
                                                {
                                                    chatHistoryList && chatHistoryList.map((each, i) => (
                                                        <li key={i} className={each.type == 'patient' ? 'outgoing-msg float-right' : 'incoming-msg float-left'}>

                                                            <p>{each.msg}</p>  <p>{each.name}</p>

                                                        </li>
                                                    ))}
                                            </ul>

                                            <div class="chatInput">
                                                <input
                                                    type="text"
                                                    className="form-control commonInput"
                                                    placeholder="Chat Here"
                                                    name="chat"
                                                    value={chat}
                                                    onChange={this.handleChange}
                                                />
                                                <input type="file" id="siofu_input" />
                                                <button className="sendBtn" onClick={this.chatGo}><i class="fas fa-paper-plane chatIcon"></i></button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div></div></div></div>
                {/* feedback form */}
                <div className={this.state.openfeedback ? "modal show d-block" : "modal"} >
                    <div class="modal-dialog modalCenterScreen">
                        <div class="modal-content modalPopUp">
                            <div class="modal-header borderNone">
                                <h5 class="modal-title">Feedback</h5>
                                <button type="button" class="popupClose" data-dismiss="modal" onClick={this.modalPopUpClose}><i class="fas fa-times"></i></button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="col-md-12 col-lg-12 col-sm-12">

                                        <div class="form-group">
                                            <label class="commonLabel">Please wait Doctor feedback</label>
                                        </div>
                                    </div>
                                    {/* <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12">

                                            <div class="form-group">
                                                <label class="commonLabel">Anamnesis</label>
                                                <textarea className="form-control" name="anamnesis" value={anamnesis} onChange={this.handleChange}></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Objectives</label>
                                                <textarea className="form-control" name="objectives" value={objectives} onChange={this.handleChange}></textarea>

                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Diagnosis</label>
                                                <textarea className="form-control" name="diagnosis" value={diagnosis} onChange={this.handleChange}></textarea>

                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Treatment</label>
                                                <textarea className="form-control" name="treatment" value={treatment} onChange={this.handleChange}></textarea>

                                            </div>
                                        </div>

                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <button type="button" class="commonBtn float-right" onClick={this.handleFeedback}>Submit</button>
                                        </div>
                                    </div> */}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </React.Fragment>

        )
    }
}
const mapStateToProps = state => ({
    listChatHistory: state.appointmentReducer.listChatHistory,
    isLoading: state.loginReducer.isLoading,
    loginDetails: state.loginReducer.loginDetails,
});

const mapDispatchToProps = dispatch => ({
    chatHistoryMethod: (historyInfo) => dispatch(chatHistoryAPICall(historyInfo)),
    updateFeedback: (feedBack) => dispatch(updateFeedback(feedBack))
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Joinmeet);