import React from 'react';
import axios from 'axios';
import URL from '../../../asset/configUrl'
class ShareWith extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            emailList: [],
            enableList: false,
            getshareWithList: []
        }
    }

    componentWillMount = () => {
        this.getEmailList()
    }

    getEmailList = () => {
        const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
        const configs = {
            method: 'post',
            url: URL.SHAREWITH_RECEIVERLIST,
            data: { "uid": loginDetails.userInfo.userId },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }
        axios(configs)
            .then(response => this.setState({ emailList: response.data.senderEmailList }))
    }

    getshareWithList = (email) => {
        const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
        const configs = {
            method: 'post',
            url: URL.SHAREWITH_SENDERLIST,
            data: { "uid": loginDetails.userInfo.userId, "senderEmailId": email },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }
        axios(configs)
            .then(response => this.setState({ getshareWithList: response.data.receiverListBySenderEmail }))
    }

    render() {
        const { emailList, getshareWithList } = this.state
        return (<div>
            <div class="share_card_new">
                <div class="share_card_title">
                    <h5>Shared With</h5>
                    <div class="share_card_search">
                        <input type="text" placeholder="Search Drive" />
                        <div class="search_icon_input">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                </div>
                {!this.state.enableList && <div className="email_list_share">
                    {emailList.map((each, i) => <div class="lsit_share_card">
                        <label><i class="fas fa-envelope"></i> {each}</label>
                        <button onClick={() => this.setState({ enableList: true }, () => this.getshareWithList(each))}><i class="far fa-eye"></i></button>
                    </div>)}
                </div>}

                <div class="file_list_share">
                    {this.state.enableList && getshareWithList.map((each, i) => {
                        return each.fileUrl.map((val) =>
                            <div class="file_list_card">
                                <div class="file_format"> <img src="images/pdf_icon.png" />
                                    <div id="share" class="share_option d-none " >
                                        <ul>
                                            <li><button><i class="fas fa-cloud-download-alt"></i> Download</button></li>
                                            <li><button data-toggle="modal" data-target="#shareModal"><i class="fas fa-share-alt"></i> Share</button></li>
                                            <li><button><i class="fas fa-trash"></i> Remove</button></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="file_details">
                                    <label>{val}</label>
                                    <div class="menu">
                                        <a>  <i class="fas fa-ellipsis-v"></i></a>
                                    </div>
                                </div>
                            </div>)
                    })}
                </div>
            </div>
        </div>)
    }
}
export default ShareWith