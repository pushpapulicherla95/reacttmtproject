import React from "react";
import { withRouter } from 'react-router';
import axios from 'axios'
import URL from '../../asset/configUrl'
class Logout extends React.Component {
  handleLogout = () => {
    const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
    const configs = {
      method: 'post',
      url: URL.USER_LOGOUT,
      data: {
        "uid": loginDetails.userInfo.userId,
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }
    axios(configs)
      .then(response => {
        sessionStorage.clear("loginDetails");
        this.props.history.push("/");
      })

  }
  render() {
    return (
      <div className="dropdown-menu">
        <a className="dropdown-item" href="#">
          <i className="fa fa-edit icon-head-menu" />
          <label className="head-menu"> My account</label>
        </a>
        <a className="dropdown-item" href="#">
          Setting
        </a>
        <a className="dropdown-item" onClick={this.handleLogout}>
          Logout
        </a>
      </div>
    );
  }
}
export default withRouter(Logout);
