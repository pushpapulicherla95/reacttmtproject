import React from 'react';
import axios from 'axios';
import Lottie from 'react-lottie';
import { reject } from 'q';
import MyDrive from './MyDrive'
import ShareWith from './SharedWith'
import ShareBy from './SharedBy';
import Inbox from './Inbox'
import {
  FacebookShareButton,
  WhatsappShareButton,
  EmailShareButton,
  FacebookIcon,
  EmailIcon,
  WhatsappIcon,
  TwitterIcon,
  TwitterShareButton,
  LinkedinIcon,
  LinkedinShareButton,
} from 'react-share';
import URL from '../../../asset/configUrl'

import animationData from '../../admin/admindashboard/lotties/loader-upload.json'

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice'
  }
};

class ShareDataMenu extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      uploadEnable: false,
      fileUploadStatus: [],
      myDriveList: [],
      NavbarType: "myDrive",
      invitePopUp: false
    }
  }
  componentWillMount = () => {
    this.myDrivehandle()
  }

  myDrivehandle = () => {
    const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
    const configs = {
      method: 'post',
      url: URL.FILELIST_SHAREDATA,
      data:{
        "uid": loginDetails.userInfo.userId
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }
    axios(configs)
      .then(response => this.setState({ myDriveList: response.data.fileList }))
  }


  myDriveListDelete = (fileName, i) => {
    const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
    const configs = {
      method: 'post',
      url: URL.FILEDELETE_SHAREDATA,
      data:{
        "uid": loginDetails.userInfo.userId,
        "fileName": fileName
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }
    axios(configs)
      .then(response => {
        // document.getElementById("share" + i).classList.add("d-none")
        this.myDrivehandle()
      })
  }

  handleChange = (e) => {
    let file = e.target.files
    let fileuploadpromise = []

    Array.from(file).map(async (each) => {
      await fileuploadpromise.push(this.submit(each))
    })

    Promise.all(fileuploadpromise)
      .then(res => setTimeout(() => {
        this.setState({ loading: false })
        this.myDrivehandle()
      }, 1500))
  }

  submit = (fileObject) => {
    const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
    this.setState({ uploadEnable: true, loading: true })
    return new Promise((resolve, reject) => {
      let reader = new FileReader()
      var value = fileObject
      var file = fileObject.name.split(".");
      this.state.fileUploadStatus.push({ fileName: file[0], type: file[1], status: "pending", response: null })
      this.setState({ fileUploadStatus: this.state.fileUploadStatus }, () => {
        reader.readAsDataURL(value)
        reader.onload = () => {
          var match = /base64,(.*)/.exec(reader.result);
          
          const configs = {
            method: 'post',
            url: URL.FILEUPLOAD_SHAREDATA,
            data:{
            uid: loginDetails.userInfo.userId,
            fileType: "." + file[1],
            fileUrl: match[1],
            fileName: file[0]
          },
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
          }
         

          axios(configs)
            .then(response => {
              let ind = this.state.fileUploadStatus.findIndex(val => val.fileName == file[0])
              this.state.fileUploadStatus[ind] = {
                fileName: file[0], type: file[1],
                status: response.data.status,
                response: response.data.message
              }
              this.setState({ fileUploadStatus: this.state.fileUploadStatus })
              resolve({ response: response.data, fileName: file[0], type: file[1] })
            }
            )
        };
      })
    })
  }

  copyToClipboard = () => {
    var textBox = document.getElementById("inviteLink");
    textBox.select();
    document.execCommand("copy");
  }


  render() {
    // console.log("sdfsdfdsffdsf", this.state)
    const { NavbarType } = this.state
    return (
      <div>
        <section>
          <aside class="shareSidebar">
            <div class="sidebar_menu">
              <div class="btn-up-share">
                <button class="file_upload_share"> <label style={{ margin: "0" }} for="upload"><i class="fas fa-cloud-upload-alt"></i> Upload</label>
                  <input type="file" style={{ display: "none" }} id="upload" multiple onChange={this.handleChange} onClick={() => this.setState({ fileUploadStatus: [], uploadEnable: false })} />
                </button>
              </div>
              <ul>
                <li onClick={() => this.setState({ NavbarType: "myDrive" })} className={NavbarType == "myDrive" && "active"}> <i class="fas fa-hdd"></i> My Drive</li>
                <li onClick={() => this.setState({ NavbarType: "Sharedwithme" })} className={NavbarType == "Sharedwithme" && "active"}><i class="fas fa-user-friends"></i> Shared with me</li>
                <li onClick={() => this.setState({ NavbarType: "Sharedbyme" })} className={NavbarType == "Sharedbyme" && "active"}><i class="fas fa-share-square"></i> Shared by me</li>
                <li onClick={() => this.setState({ NavbarType: "Inbox" })} className={NavbarType == "Inbox" && "active"}><i class="fas fa-retweet"></i> Notification</li>
                {/* <li data-toggle="modal" data-target="#shareModal3"><i class="fas fa-user-plus"></i> Invite Doctors</li> */}
                <li onClick={() => this.setState({ invitePopUp: true })} className={this.state.invitePopUp && "active"}><i class="fas fa-user-plus"></i> Invite Doctors</li>

              </ul>
            </div>
          </aside>
          <div class="" id="main-content">
            <div class="wrapper">
              <div class="container-fluid">
                <div class="body-content">
                  {this.state.NavbarType == "myDrive" && <MyDrive myDriveListDelete={this.myDriveListDelete} myDriveList={this.state.myDriveList} />}
                  {this.state.NavbarType == "Sharedwithme" && <ShareWith />}
                  {this.state.NavbarType == "Sharedbyme" && <ShareBy />}
                  {this.state.NavbarType == "Inbox" && <Inbox />}
                </div>
              </div>
            </div>


            {this.state.uploadEnable && <div class="upload_list_area">
              <div class="uploading_title">
                <label>{this.state.fileUploadStatus.length} items uploading</label>
                <a onClick={() => { this.setState({ uploadEnable: false, loading: false, fileUploadStatus: [] }) }}><i class="fas fa-times"></i></a>
              </div>
              <div class="upload_file_list">
                <ul>
                  {this.state.fileUploadStatus.map((each) => <li>
                    <img src={each.type == "pdf" ?
                      "images/pdf_icon.png" : each.type == "zip"
                        ? "images/zip_icon.png" : "images/doc_icon.png"} />
                    <div class="up_file_details">
                      <div class="up_file_name">
                        <h6>{each.fileName}</h6>
                        <div class="status">
                          {this.state.loading && <Lottie options={defaultOptions}
                            height={40}
                            width={40}
                          />}
                          {each.status == "failure" && this.state.loading == false && <span className="error_text_status">{each.response}</span>}
                          {this.state.loading == false && <i class={each.status == "success" ? "far fa-thumbs-up success" : "far fa-thumbs-down failed"}></i>}
                        </div>
                      </div>
                    </div>
                  </li>)}
                </ul>
              </div>
            </div>}
          </div>

        </section>

        <div class="modal" id="deleteModal">
          <div class="modal-dialog">
            <div class="modal-content modalPopUp">
              <div class="modal-header borderNone">
                <h5 class="modal-title">Delete</h5>
                <button type="button" class="popupClose" data-dismiss="modal"><i class="fas fa-times"></i></button>
              </div>
              <div class="modal-body">
                <h4 class="deleteMsg"><i class="fas fa-info-circle"></i> Are you sure you want to delete this
                                record</h4>
                <button type="text" class="commonBtn smallBtn float-right" data-dismiss="modal">No</button>
                <button type="text" class="commonBtn smallBtn float-right mr-2">Yes</button>

              </div>
            </div>
          </div>
        </div>
        <div class="modal">
          <div class="modal-dialog">
            <div class="modal-content modalPopUp">
              <div class="modal-header borderNone">
                <h5 class="modal-title">Sharable Link</h5>
                <button type="button" class="popupClose" data-dismiss="modal" ><i class="fas fa-times"></i></button>
              </div>
              <div class="modal-body">
                <h6>Link sharing is on</h6>
                <div class="share-input">
                  <input type="text" placeholder="https://www.tomato.colanonline.com" readonly />
                  <button>Copy</button>
                </div>
                <div class="share_social_icon">
                  <ul>
                    <li><button title="within tomato" data-toggle="modal" data-target="#shareModal2"><img src="images/tomato_share.png" /></button></li>
                    <li><button title="Facebook"><img src="images/facebook_share.png" /></button></li>
                    <li><button title="What's App "><img src="images/whatsapp_share.png" /></button></li>
                    <li><button title="Gmail"><img src="images/google_share.png" /></button></li>
                  </ul>
                </div>

              </div>
            </div>
          </div>
        </div>


        {this.state.invitePopUp && <div class="modal fade show" style={{ display: "block" }} >
          <div class="modal-dialog">
            <div class="modal-content modalPopUp">
              <div class="modal-header borderNone">
                <h5 class="modal-title">Invite Link</h5>
                <button type="button" class="popupClose" data-dismiss="modal" onClick={() => this.setState({ invitePopUp: false })}><i class="fas fa-times"></i></button>
              </div>
              <div class="modal-body">
                <h6>Link sharing is on</h6>
                <div class="share-input">
                  <input type="text" id="inviteLink" value="https://app.tomatomedical.com/register/patient" placeholder="https://www.tomato.colanonline.com" readonly />
                  <button onClick={this.copyToClipboard}>Copy</button>
                </div>
                <div class="share_social_icon">
                  <ul>
                    {/* <li><button title="within tomato" data-toggle="modal" data-target="#shareModal2"><img src="images/tomato_share.png" /></button></li>
                    <li><button title="Facebook"><img src="images/facebook_share.png" /></button></li>
                    <li><button title="What's App "><img src="images/whatsapp_share.png" /></button></li>
                    <li><button title="Gmail"><img src="images/google_share.png" /></button></li> */}
                    {/* <FacebookShareButton url="http://localhost:3000/register/patient"/> */}
                    <li>
                      <FacebookShareButton url="https://app.tomatomedical.com/register/patient" >
                        <FacebookIcon size={32} round={true} />
                      </FacebookShareButton>
                    </li>
                    <li>
                      <WhatsappShareButton url="https://app.tomatomedical.com/register/patient" >
                        <WhatsappIcon size={32} round={true} />
                      </WhatsappShareButton>
                    </li>
                    <li>
                      <EmailShareButton url="https://app.tomatomedical.com/register/patient" >
                        <EmailIcon size={32} round={true} />
                      </EmailShareButton>
                    </li>
                    <li>
                      <TwitterShareButton url="https://app.tomatomedical.com/register/patient" >
                        <TwitterIcon size={32} round={true} />
                      </TwitterShareButton>
                    </li>
                    <li>
                      <LinkedinShareButton url="https://app.tomatomedical.com/register/patient" >
                        <LinkedinIcon size={32} round={true} />
                      </LinkedinShareButton>
                    </li>

                  </ul>

                </div>
              </div>
            </div>

          </div>
        </div>}


        <div class="modal" id="shareModal2" >
          <div class="modal-dialog">
            <div class="modal-content modalPopUp">
              <div class="modal-header borderNone">
                <h5 class="modal-title">With in Tomato</h5>
                <button type="button" class="popupClose" data-dismiss="modal"><i class="fas fa-times"></i></button>
              </div>
              <div class="modal-body">
                <h6>Email</h6>
                <div class="form-group">
                  <input type="text" class="form-control" id="usr" />
                </div>
                <h6>Message</h6>
                <div class="form-group">
                  <textarea class="form-control" rows="2" id="comment"></textarea>
                </div>
                <button class="send_btn"><i class="far fa-paper-plane"></i> Send</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default ShareDataMenu