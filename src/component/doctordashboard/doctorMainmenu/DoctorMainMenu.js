import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { getDoctorProfile } from "../../../service/doctor/action";
import AWS from "aws-sdk";
import validator from "validator";
// import { changepsw } from "../../service/login/action"
import { changepsw } from "../../../service/login/action";
import { uploadProfilepic } from "../../../service/hospital/action"
import axios from 'axios';
import URL from '../../../asset/configUrl'
class DoctorMainMenu extends Component {
  constructor(props) {
    super(props);
    var loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
    const { userInfo } = loginDetails;
    var userId = userInfo.userId || "";
    this.state = {
      userId: userId,
      specialistId: "",
      profilePic: userInfo.profilePic,
      tabone: true,
      tabtwo: false,
      tabthree: false,
      fileUrl: "",
      fileName: "",
      fileType: "",
      openModel: false,
      profilePic: userInfo.profilePic,
      currentpassword: "",
      password: "",
      confirmpassword: "",
      errors: {
        password: "",
        confirmpassword: "",
        currentpassword: ""
      },
      passwordValid: false,
      confirmpasswordValid: false,
      currentpasswordValid: false,
      formValid: false,
      randomNumber: Math.random(),
      fileImgUrl: "",
      imageHash: Date.now()
    }
  }


  componentWillMount() {
    // this.props.symptomMainList()
    AWS.config.update({
      accessKeyId: process.env.REACT_APP_ACCESSKEY,
      secretAccessKey: process.env.REACT_APP_SECRETACCESSKEY,
      region: process.env.REACT_APP_REGION
    });
    const { profilePic } = this.state;
    if (this.props.profilePicInfo) {
      this.setState({ profilePic: this.props.profilePicInfo.profilePic })
      this.getprofileImage(this.props.profilePicInfo.profilePic);

    } else {
      this.getprofileImage(profilePic);

    }
    const { userInfo } = this.props.loginDetails;
    this.setState({ fileImgUrl: `https://data.tomatomedical.com/profilepicture/${userInfo.userId}_.png?${this.state.imageHash}` })
  }

  // componentWillMount() {
  //     const userdetails = {
  //         "user_id": this.state.userId
  //     }
  //     this.props.getDoctorProfile(userdetails);
  //     const { profilePic } = this.state;
  //     AWS.config.update({
  //         accessKeyId: process.env.REACT_APP_ACCESSKEY,
  //         secretAccessKey: process.env.REACT_APP_SECRETACCESSKEY,
  //         region: process.env.REACT_APP_REGION
  //     });

  //     if (this.props.profilePicInfo) {
  //       this.setState({ profilePic: this.props.profilePicInfo.profilePic })
  //       this.getprofileImage(this.props.profilePicInfo.profilePic);

  //     } else {
  //       this.getprofileImage(profilePic);

  //     }
  //     var s3 = new AWS.S3();
  //     var data = {
  //         Bucket: "tomato-medical-bucket",
  //         Key: `profilepicture/${profilePic}`
  //     };
  //     s3.getSignedUrl("getObject", data, async (err, data) => {
  //         if (err) {
  //             console.log("Error uploading data: ", err);
  //         } else {
  //             console.log("succesfully uploaded the image!", data);

  //             (await data) && this.setState({ img: data });
  //         }
  //     });

  // }
  // componentWillReceiveProps(nextProps) {
  //     if (nextProps.doctorProfile && nextProps.doctorProfile.docInformation && (nextProps.doctorProfile.docInformation.specialistId == "" || nextProps.doctorProfile.docInformation.specialistId == null)) {

  //         this.props.history.push({ pathname: '/docdashboard', state: { submenuType: "Personal" } })
  //         // onClick={() => this.props.history.push({ pathname: '/docdashboard', state: { submenuType: "Personal" } })}
  //     }
  //     if (nextProps.profilePicInfo) {
  //         console.log("success upload")
  //         const { profilePic } = nextProps.profilePicInfo
  //         this.setState({ profilePic: nextProps.profilePicInfo.profilePic });
  //         this.getprofileImage(profilePic);
  //       }

  // }
  componentWillReceiveProps = (nextProps) => {
    if (nextProps.doctorProfile && nextProps.doctorProfile.docInformation && (nextProps.doctorProfile.docInformation.specialistId == "" || nextProps.doctorProfile.docInformation.specialistId == null)) {

      this.props.history.push({ pathname: '/docdashboard', state: { submenuType: "Personal" } })
      //         // onClick={() => this.props.history.push({ pathname: '/docdashboard', state: { submenuType: "Personal" } })}
    }
    if (this.props.profilePicInfo != nextProps.profilePicInfo) {
      const { profilePic } = nextProps.profilePicInfo;
      console.log("nextProps", nextProps);
      // this.setState({ profilePic: nextProps.profilePicInfo.profilePic });
      console.log("profilePic", profilePic);
      const { userInfo } = this.props.loginDetails;
      this.setState({ fileImgUrl: "", imageHash: Date.now() }, () => {
        setTimeout(() => {
          this.setState({ fileImgUrl: `https://data.tomatomedical.com/profilepicture/${userInfo.userId}_.png?${this.state.imageHash}` })
        }, 3000)

      })
      this.getprofileImage(profilePic);
    }
  }
  getprofileImage = (profilePic) => {

    var data = `https://data.tomatomedical.com/profilepicture/${profilePic}`;
    this.setState({ fileUrl: data });
    // this.validateQrcode();
    console.log("fileUrl", this.state.fileUrl);
  }


  handleLogout = () => {
    const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
    const configs = {
      method: 'post',
      url: URL.USER_LOGOUT,
      data: {
        "uid": loginDetails.userInfo.userId,
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    }
    axios(configs)
      .then(response => {
        sessionStorage.clear("loginDetails");
        this.props.history.push("/");
      })
    this.props.removeProfilePicInfo({ type: "UPLOAD_PROFILEPIC_SUCCESS", payload: "" })
  };


  tabOne = () => {
    this.setState({
      tabone: true,
      tabtwo: false,
      tabthree: false
    });
  };

  tabTwo = () => {
    this.setState({
      tabone: false,
      tabtwo: true,
      tabthree: false
    });
  };

  tabThree = () => {
    this.setState({
      tabone: false,
      tabtwo: false,
      tabthree: true
    });
  };
  handleChangePopup = e => {
    var reader = new FileReader();

    var value = e.target.files[0];
    let name = value.name.split('.')[0]
    let type = "." + value.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileUrl: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(value);
  }
  submitUpload = (e) => {
    e.preventDefault();
    const { fileType, fileUrl, userId, fileName, profilePic } = this.state;
    var phrase = this.state.fileUrl;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);

    const filedata = {
      uid: userId,
      fileType: fileType,
      fileName: fileName,
      fileUrl: match[1],
    }
    this.props.uploadProfilepic(filedata);
    this.getprofileImage(profilePic);
    this.setState({
      openModel: false,
      fileUrl: "",
      fileName: "",
      fileType: "",
    })

  }
  modalPopClose = () => {
    this.setState({ openModel: !this.state.openModel });
  }
  openModelPop = () => {
    this.setState({ openModel: true });

  }
  changePassword = () => {

    const { email, password, currentpassword, confirmpassword } = this.state;
    const { userInfo } = this.props.loginDetails;
    let changepswinfo = {
      email: userInfo.email,
      password: password,

    };

    this.props.changepsw(changepswinfo);
    this.setState({
      openModel: false,
      currentpassword: "",
      password: "",
      confirmpassword: "",
    })

  }

  validateField(fieldName, value) {
    const { errors, passwordValid, confirmpasswordValid, currentpasswordValid, confirmpassword, password } = this.state;

    let passwordvalid = passwordValid;
    let currentpasswordvalid = currentpasswordValid;
    let confirmpasswordvalid = confirmpasswordValid;
    let fieldValidationErrors = errors;
    switch (fieldName) {

      case "currentpassword":
        currentpasswordvalid = !(value.length < 3 || value.length > 20);
        fieldValidationErrors.currentpassword = currentpasswordvalid
          ? ""
          : "Password should be 3 to 20 characters";
        break;

      case "password":
        passwordvalid = value.length !== 0
        fieldValidationErrors.password = passwordvalid
          ? ""
          : "Please enter valid password.";
        break;

      // case "password":
      // passwordvalid =
      //   value.length >= 8 &&
      //   value.match(
      //     /(?=.*[_!@#$%^&*-])(?=.*\d)(?!.*[.\n])(?=.*[a-z])(?=.*[A-Z])^.{8,}$/
      //   );
      // fieldValidationErrors.password = passwordvalid
      //   ? ""
      //   : "Must contain at least one uppercase letter, one lowercase letter, one number, one special character and a minimum of 8 characters";
      // break;
      case "confirmpassword":
        confirmpasswordvalid = validator.equals(value, password);
        fieldValidationErrors.confirmpassword = confirmpasswordvalid
          ? ""
          : "Password does not match";
        break;
    }
    if (fieldName === "password") {
      if (confirmpassword !== "") {
        if (value !== confirmpassword) {
          fieldValidationErrors.confirmpassword = "Password does not match.";
        } else {
          fieldValidationErrors.confirmpassword = "";
        }
      }
    } else if (fieldName === "confirmpassword") {
      if (value !== password) {
        fieldValidationErrors.confirmpassword = "Password does not match.";
      } else {
        fieldValidationErrors.confirmpassword = "";
      }
    }
    this.setState({
      errors: fieldValidationErrors,
      passwordValid: passwordvalid,
      confirmpasswordValid: confirmpasswordvalid,
      currentpasswordValid: currentpasswordvalid
    },
      this.validateForm
    )
  }
  //
  validateForm() {
    const {
      passwordValid, confirmpasswordValid, currentpasswordValid
    } = this.state;
    this.setState({
      formValid:
        currentpasswordValid &&
        passwordValid &&
        confirmpasswordValid,

    });

  }
  handleChange = (e) => {
    e.preventDefault();
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };

  render() {
    const { loginDetails, firstName } = this.props;
    const { specialistId, tabone, tabtwo, tabthree, openModel, profilePic, fileUrl, currentpassword, fileImgUrl, password, errors, formValid } = this.state;
    const { userInfo } = this.props.loginDetails
    const { docInformation } = this.props.doctorProfile;
    console.log("profilePic fileUrl>>>", fileUrl)
    console.log("profilePic profilePic", profilePic);
    console.log("dsasszsfd", fileImgUrl)
    console.log("state", this.state)

    return (
      <React.Fragment>
        {this.state.randomNumber &&
          <div className="inner-pageNew">

            <header className="header">
              <div className="brand">
                <a href="javascript:void(0);" onClick={() => this.props.history.push({ pathname: '/doctormainmenu', ...this.props })} className="logo">
                  <img src="../images/logo.jpg" /> </a>

              </div>
              <div className="top-nav" style={{ display: "none" }}>

                <div className="searchBox float-right " >

                  <input type="text" className="searchTxt" placeholder="Search Symptom Checker" />
                  <a href="#" className="searchBtn">
                    <i className="fas fa-search"></i>
                  </a>
                </div>

              </div>
              <div className="languageSel text-right mb-2" style={{ marginTop: "20px;margin-right:10px" }}>
                <span>Language :</span>
                <select className="">
                  <option>EN</option>
                </select>
              </div>
            </header>

            <div className="body-content">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-lg-3 col-md-3 col-sm-12 col-12 bg-purple-card">
                    <div className="card user-profile-card">
                      <div className="row">
                        <div className="col-12">
                          <a href="javaScript:void(0);" onClick={this.openModelPop}
                          >
                            {/* <img src={this.state.fileUrl  ? this.state.fileUrl  :"../images/no-profile-pic-icon-5.png" }  /> */}
                            {/* <img src={`https://data.tomatomedical.com/profilepicture/${userInfo.profilePic}`}/>  */}
                            <img src={fileImgUrl} />

                          </a>
                          <h5>Doctor Name</h5>
                          <p>{userInfo.firstName}</p>
                        </div>

                      </div>
                      <div className="set-section">
                        <ul>
                          <li><a href="javaScript:void(0);" onClick={this.openModelPop}><i className="fas fa-cog"></i></a></li>
                          <li><a href="" title="Subscription"><i className="fas fa-shopping-cart"></i></a></li>
                          <li><a href="" onClick={this.handleLogout}><i className="fas fa-power-off"></i></a></li>
                        </ul>
                      </div>

                    </div>

                  </div>

                  <div className="col-lg-9 col-md-9 col-sm-12 col-12">
                    <div className="row">
                      <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                        <a href="javascript:void(0);" onClick={() => this.props.history.push({ pathname: '/docdashboard', state: { submenuType: "Personal" } })} className="card bg-c">
                          <div className="card-img1">
                            <img src="../images/personal-color.png" />
                          </div>
                          <div className="card-title">
                            <h5>Personal</h5>
                          </div>
                        </a>
                      </div>
                      <div className="col-lg-3 col-md-3 col-sm-6 col-6 padLeft">
                        <a href="javascript:void(0);" onClick={() => this.props.history.push({ pathname: '/docdashboard', state: { submenuType: "MySchedule" } })} className="card bg-c">
                          <div className="card-img1">
                            <img src="../images/appointment.png" />
                          </div>
                          <div className="card-title">
                            <h5>My Availability</h5>
                          </div>
                        </a>
                      </div>
                      <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                        <a href="javascript:void(0);" onClick={() => this.props.history.push({ pathname: '/docdashboard', state: { submenuType: "Appointments" } })} className="card bg-c">
                          <div className="card-img1">
                            <img src="../images/Booking.png" />
                          </div>
                          <div className="card-title">
                            <h5>Booked Telemedicine</h5>
                          </div>
                        </a>
                      </div>
                      <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                        <a href="javascript:void(0);" onClick={() => this.props.history.push({ pathname: '/docdashboard', state: { submenuType: "OfficeMeeting" } })} className="card bg-c">
                          <div className="card-img1">
                            <img src="../images/Booking.png" />
                          </div>
                          <div className="card-title">
                            <h5>Booked office meeting</h5>
                          </div>
                        </a>
                      </div>
                      <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                        <a href="javascript:void(0);" onClick={() => this.props.history.push({ pathname: '/docdashboard', loginDetails: loginDetails, state: { submenuType: "Patients" } })} className="card bg-c">
                          <div className="card-img1">
                            <img src="../images/myPatient.png" />
                          </div>
                          <div className="card-title">
                            <h5>My Patients</h5>
                          </div>
                        </a>
                      </div>
                      <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                        <a href="javascript:void(0);" className="card bg-c" onClick={() => this.props.history.push({ pathname: '/docdashboard', loginDetails: loginDetails, state: { submenuType: "Manageads" } })}>
                          <div className="card-img1">
                            <img src="../images/ads.PNG" />
                          </div>
                          <div className="card-title">
                            <h5>Manage Ads</h5>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                    <a href="javascript:void(0);" className="card bg-c" onClick={() => this.props.history.push({ pathname: '/docdashboard', loginDetails: loginDetails, state: { submenuType: "Communicator" } })}>
                      <div className="card-img1">
                        <img src="../images/ads.PNG" />
                      </div>
                      <div className="card-title">
                        <h5>Communicator</h5>
                      </div>
                    </a>
                  </div>
                </div>

              </div>
            </div>

            {openModel && <div className="modal d-block show" >
              <div className="modal-dialog">
                <div className="modal-content modalPopUp">
                  <div className="modal-header borderNone">
                    <h5 className="modal-title">Setting</h5>
                    <button type="button" className="popupClose" onClick={this.modalPopClose} ><i className="fas fa-times"></i></button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="tomCardBody">
                        <div className="tab-menu-content">

                          <div className="tab-menu-title">
                            <ul>
                              <li className={this.state.tabone ? "menu1 active" : ""}>
                                <a href="#" onClick={this.tabOne}>
                                  <p>Profile</p>
                                </a></li>
                              <li className={this.state.tabtwo ? "menu1 active" : ""}>
                                <a href="#" onClick={this.tabTwo}>
                                  <p>Change Password</p>
                                </a></li>
                              <li className={this.state.tabthree ? "menu1 active" : ""}>
                                <a href="#" onClick={this.tabThree}>
                                  <p>Delete Account</p>
                                </a></li>
                            </ul>
                          </div>

                          <div className="tab-menu-content-section">
                            {tabone &&
                              <div id="pcontent-1" >

                                <div className="row">
                                  <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div className="form-group">
                                      <div className="profile_model_image">

                                        <img src={fileImgUrl} />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div className="form-group">
                                      <label>Upload Profile {tabone} <sup>*</sup></label>
                                      {/* <label class="label_btn_upload" for="btn_img">Browse</label> */}
                                      <input type="file" className="form-control" name="fileUrl" id="btn_img" onChange={this.handleChangePopup} />
                                    </div>
                                  </div>

                                </div>
                                <div className="row">
                                  <div className="col-12">
                                    <button type="button" className="commonBtn float-right" onClick={this.submitUpload}>Update</button></div>
                                </div>
                              </div>}
                            {tabtwo &&
                              <div id="pcontent-2"  >

                                <div className="row">
                                  <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div className="form-group">
                                      <label>Current Password <sup>*</sup></label>
                                      <input type="text" className="form-control" name="currentpassword" value={currentpassword} onChange={this.handleChange}
                                      />
                                    </div>
                                    <div style={{ color: "red" }}>{errors.currentpassword}</div>

                                  </div>
                                  <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div className="form-group">
                                      <label>New Password <sup>*</sup></label>
                                      <input type="text" name="password" value={password} className="form-control" onChange={this.handleChange}
                                      />
                                    </div>
                                    <div style={{ color: "red" }}>{errors.password}</div>

                                  </div>
                                  <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div className="form-group">
                                      <label>Confirm Password <sup>*</sup></label>
                                      <input type="text" name="confirmpassword" value={this.state.confirmpassword} className="form-control" onChange={this.handleChange} />
                                    </div>
                                    <div style={{ color: "red" }}>{errors.confirmpassword}</div>

                                  </div>

                                </div>
                                <div className="row">
                                  <div className="col-12">
                                    <button type="button" className="commonBtn float-right" onClick={this.changePassword} disabled={!formValid} >Update</button></div>
                                </div>
                              </div>
                            }

                            {tabthree &&


                              <div id="pcontent-3"  >

                                <div className="row">
                                  <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div className="form-group">
                                      <p className="mb-0">Are you sure you want to delete <b>Tomato Medical Account</b>?
                                                                        </p>
                                      <p>All the Records and Medical Document will be deleted and cannot be undone.</p>
                                    </div>
                                  </div>



                                </div>
                                <div className="row">
                                  <div className="col-12">
                                    <button type="button" className="commonBtn float-right">Delete</button></div>
                                </div>
                              </div>
                            }


                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            }
          </div>}
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  loginDetails: state.loginReducer.loginDetails,
  doctorProfile: state.doctorReducer.doctorProfile,
  profilePicInfo: state.hospitalReducer.profilePicInfo,



})

const mapDispatchToprops = dispatch => ({
  getDoctorProfile: userdetails => dispatch(getDoctorProfile(userdetails)),
  uploadProfilepic: (filedata) => dispatch(uploadProfilepic(filedata)),
  changepsw: (changepswinfo) => dispatch(changepsw(changepswinfo)),

  removeProfilePicInfo: () => dispatch({
    type: "UPLOAD_PROFILEPIC_SUCCESS",
    payload: ""
  })


})
export default connect(
  mapStateToProps,
  mapDispatchToprops
)(withRouter(DoctorMainMenu));
