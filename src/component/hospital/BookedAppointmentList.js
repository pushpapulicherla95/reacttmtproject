import React, { Component } from "react";
import { getHospitalDoctorList } from "../../service/hospital/action";
import { connect } from "react-redux";
import { doctorAppointmentListAPICall } from "../../service/appointment/action";
import _ from 'lodash';
import moment from 'moment';

class BookedAppontments extends Component {
    constructor(props) {
        super(props);
        var loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
        const { userInfo } = loginDetails;
        var userId = userInfo.userId || "";
        this.state = {
            userId: userId,
            doctorList: [],
            appointmentType: "Doclist",
            doctorAppointmentList: [],
            officeMeetingList: []
        }
        this.allInterval = []
    }
    componentWillMount() {
        const info = {
            uid: this.state.userId
        }
        this.props.getHospitalDoctorList(info)
    }
    componentWillReceiveProps(nextProps) {
        console.log("nextProps hospital appontment>>>", nextProps);
        if (nextProps.hospitalDoctorList && nextProps.hospitalDoctorList.hospitalDoctorList) {
            this.setState({ doctorList: nextProps.hospitalDoctorList.hospitalDoctorList })

        }
        if (nextProps.listDoctorAppointment && nextProps.listDoctorAppointment.appointmentDoctorLists) {

            const { appointmentDoctorLists } = nextProps.listDoctorAppointment;
            let result = appointmentDoctorLists.length > 0 && _.filter(appointmentDoctorLists, (value) => {
                return value.scheduleType == "video call";
            })
            let filterResult = appointmentDoctorLists.length > 0 && _.filter(appointmentDoctorLists, (value) => {
                return value.scheduleType == "office meeting";
            })
            this.setState({ doctorAppointmentList: result, officeMeetingList: filterResult })

        }  

    }
  
    componentWillUnmount = () => {
        this.allInterval.map((data) => {
            clearInterval(data)
        })
    }
    openJitseeMeet = (data) => {
        let meetingRoom = data.meetingRoom;
        let doctorName = data.doctorName;
        localStorage.setItem("meetingRoom", meetingRoom);
        localStorage.setItem("doctorName", doctorName);
        localStorage.setItem("doctorId", data.doctorId);
        localStorage.setItem("patientId", data.patientId);

        this.props.history.push({ pathname: '/docdashboard', state: { submenuType: "DoctorJoinmeet" } })
    }

    // openFeedbackPop = () => {
    //     this.setState({ openfeedback: true });
    // }

    // modalPopUpClose = () => {
    //     this.setState({ openfeedback: false });
    // }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }
    getTelemedicine = (data) => {
        console.log("data>>>>>", data.userInfoId);
        this.setState({ appointmentType: "Online" })
        // getTelemedicine
        const doctorInfo = {
            "doctorId": data.userInfoId
        }
        this.props.doctorAppListMethod(doctorInfo);

    }
    getOfficeMeeting = (data) => {
        this.setState({ appointmentType: "Offline" });
        const doctorInfo = {
            "doctorId": data.userInfoId
        }
        this.props.doctorAppListMethod(doctorInfo);

    }
    render() {
        const { doctorList, appointmentType, doctorAppointmentList, officeMeetingList } = this.state;
        console.log("doctorAppointmentList",doctorAppointmentList);
        return (
            <div>
                <div className="mainMenuSide" id="main-content">
                    <div className="wrapper">
                        <div className="container-fluid">
                            <div className="body-content">
                                <div className="tomCard">
                                    <div className="tomCardHead">
                                        <h5 className="float-left">Appointment</h5>

                                    </div>

                                    <div className="tab-menu-content tomCardBody">

                                        <div className="tab-menu-title">
                                            <ul>
                                                <li className="menu1 active"><a href="JavaScript:Void(0);">
                                                    <p>List of Appointment </p>
                                                </a></li>


                                            </ul>
                                        </div>
                                        {/* hospital doctors list */}
                                        {appointmentType == "Doclist" && <div className="tab-menu-content-section">
                                            <div id="content-1" style={{ display: "block" }}>

                                                <div className="table-responsive">
                                                    {doctorList.length > 0 ?<table className="table table-hover">
                                                        <thead className="thead-default">
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Name</th>
                                                                <th>Specialist</th>
                                                                <th>Contact</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {doctorList.length > 0 && doctorList.map((value, i) =>
                                                                <tr key={i}>
                                                                    <td data-label="#">{i + 1}</td>
                                                                    <td data-label="Name">{value.firstName}</td>
                                                                    <td data-label="Date">{value.specialListName}</td>
                                                                    <td data-label="Timings">{value.phone}</td>
                                                                    <td data-label="Action">
                                                                        <a title="View Appoinments" className="commonBtn2 mr-2" data-toggle="modal" onClick={() => this.getTelemedicine(value)}>View Telemedicine</a>
                                                                        <a title="View Appoinments" className="commonBtn2 " data-toggle="modal" onClick={() => this.getTelemedicine(value)}>View OfficeMeeting</a>
                                                                    </td>
                                                                </tr>
                                                            )
                                                            }

                                                        </tbody>
                                                    </table>:"No Booked Appointment List"
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                        }
                                        {/* Booked Telemedicine */}
                                        {appointmentType == "Online" &&
                                            <div className="tab-menu-content-section">
                                                <div id="content-1" style={{ display: "block" }}>

                                                    <div className="table-responsive">
                                                    { doctorAppointmentList.length > 0 ? <table className="table table-hover">
                                                            <thead className="thead-default">
                                                                <tr>
                                                                    <th>Appointment Date</th>
                                                                    <th>Timings</th>
                                                                    <th>Patient Name</th>
                                                                    <th>Status</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {
                                                                    doctorAppointmentList && doctorAppointmentList.map((each, i) => {
                                                                        // console.log(moment(each.appointmentDate).isAfter())
                                                                        let disabled;

                                                                        if (moment().format("YYYY-MM-DD") == each.appointmentDate) {
                                                                            disabled = false;
                                                                            var newInterval = setInterval(() => {
                                                                                const currentTime = moment();
                                                                                const startTime = moment(each.startTime, "HH:mm ");
                                                                                const endTime = moment(each.endTime, "HH:mm");
                                                                                const amIBetween = currentTime.isBetween(startTime, endTime);
                                                                                const beforeCheck = currentTime.isBefore(startTime)
                                                                                const afterCheck = currentTime.isAfter(endTime)
                                                                                if (document.getElementById("meeting" + i)) {
                                                                                    if (amIBetween) {
                                                                                        document.getElementById("meeting" + i).textContent = "Join Meeting"
                                                                                        document.getElementById("meeting" + i).disabled = false

                                                                                    } else if (beforeCheck) {
                                                                                        let duration = moment.duration(startTime.diff(currentTime))
                                                                                        let hours = parseInt(duration.asHours());
                                                                                        let minutes = parseInt(duration.asMinutes()) % 60;
                                                                                        let seconds = parseInt(duration.asSeconds()) % 60;

                                                                                        let timeString = hours + ':' + minutes + ':' + seconds + ''

                                                                                        document.getElementById("meeting" + i).textContent = "Meeting in " + timeString
                                                                                        document.getElementById("meeting" + i).disabled = true

                                                                                    } else if (afterCheck) {
                                                                                        document.getElementById("meeting" + i).textContent = "Meeting Ended"
                                                                                        document.getElementById("meeting" + i).disabled = true
                                                                                    }
                                                                                }

                                                                            }, 100)
                                                                            this.allInterval.push(newInterval)
                                                                        } else {
                                                                            disabled = true;
                                                                            const currentTime = moment();
                                                                            const beforeCheck = moment(each.appointmentDate).isAfter(currentTime)
                                                                            if (document.getElementById("meeting" + i)) {
                                                                                if (beforeCheck) {

                                                                                    document.getElementById("meeting" + i).textContent = "Yet To Start"

                                                                                } else {

                                                                                    document.getElementById("meeting" + i).textContent = "Meeting Ended"

                                                                                }
                                                                            }

                                                                        }

                                                                        return (
                                                                            <tr key={i}>
                                                                                <td data-label="Appointment Date">{each.appointmentDate}</td>
                                                                                <td data-label="Start Time">{each.startTime} - {each.endTime}</td>
                                                                                <td data-label="Patient Name">{each.patientName}</td>
                                                                                <td data-label="status">{each.status} </td>
                                                                                <td data-label="Action"><button id={"meeting" + i} title="Join Meeting" disabled={disabled} onClick={() => this.openJitseeMeet(each)} className="commonBtn2 ">Join Meeting</button></td>


                                                                            </tr>
                                                                        )

                                                                    })
                                                                }

                                                            </tbody>
                                                        </table> : "No Booked Appointment List"
                                                            }
                                                    </div>
                                                </div>
                                            </div>}
                                    </div>

                                    {/* Booked offline meetings */}
                                    {appointmentType == "Offline" && <div className="tab-menu-content-section">
                                        <div id="content-1" style={{ display: "block" }}>

                                            <div className="table-responsive">
                                                <table className="table table-hover">
                                                    <thead className="thead-default">
                                                        <tr>
                                                            <th>Appointment Date</th>
                                                            <th>Timings</th>
                                                            <th>Patient Name</th>
                                                            <th>Status</th>
                                                            <th>Appointment Type</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {
                                                            officeMeetingList && officeMeetingList.map((each, i) => {
                                                                let disabled;

                                                                if (moment().format("YYYY-MM-DD") == each.appointmentDate) {
                                                                    disabled = false;
                                                                    var newInterval = setInterval(() => {
                                                                        const currentTime = moment();
                                                                        const startTime = moment(each.startTime, "HH:mm ");
                                                                        const endTime = moment(each.endTime, "HH:mm");
                                                                        const amIBetween = currentTime.isBetween(startTime, endTime);
                                                                        const beforeCheck = currentTime.isBefore(startTime)
                                                                        const afterCheck = currentTime.isAfter(endTime)
                                                                        if (document.getElementById("meeting" + i)) {
                                                                            if (amIBetween) {
                                                                                document.getElementById("meeting" + i).textContent = "Join Meeting"
                                                                                document.getElementById("meeting" + i).disabled = false

                                                                            } else if (beforeCheck) {
                                                                                let duration = moment.duration(startTime.diff(currentTime))
                                                                                let hours = parseInt(duration.asHours());
                                                                                let minutes = parseInt(duration.asMinutes()) % 60;
                                                                                let seconds = parseInt(duration.asSeconds()) % 60;

                                                                                let timeString = hours + ':' + minutes + ':' + seconds + ''

                                                                                document.getElementById("meeting" + i).textContent = "Meeting in " + timeString
                                                                                document.getElementById("meeting" + i).disabled = true

                                                                            } else if (afterCheck) {
                                                                                document.getElementById("meeting" + i).textContent = "Meeting Ended"
                                                                                document.getElementById("meeting" + i).disabled = true
                                                                            }
                                                                        }

                                                                    }, 100)
                                                                    this.allInterval.push(newInterval)
                                                                } else {
                                                                    disabled = true;
                                                                    const currentTime = moment();
                                                                    const beforeCheck = moment(each.appointmentDate).isAfter(currentTime)
                                                                    if (document.getElementById("meeting" + i)) {
                                                                        if (beforeCheck) {

                                                                            document.getElementById("meeting" + i).textContent = "Yet To Start"

                                                                        } else {

                                                                            document.getElementById("meeting" + i).textContent = "Meeting Ended"

                                                                        }
                                                                    }

                                                                }

                                                                return (
                                                                    <tr key={i}>
                                                                        <td data-label="Appointment Date">{each.appointmentDate}</td>
                                                                        <td data-label="Start Time">{each.startTime} - {each.endTime}</td>
                                                                        <td data-label="Patient Name">{each.patientName}</td>
                                                                        <td data-label="status">{each.status} </td>
                                                                        <td data-label="Action">{each.scheduleType}</td>


                                                                    </tr>
                                                                )

                                                            })
                                                        }

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                  }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        )
    }
}
const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails,
    hospitalDoctorList: state.hospitalReducer.hospitalDoctorList,
    listDoctorAppointment: state.appointmentReducer.listDoctorAppointment

})

const mapDispatchToprops = dispatch => ({
    getHospitalDoctorList: (info) => dispatch(getHospitalDoctorList(info)),
    doctorAppListMethod: (doctorInfo) => dispatch(doctorAppointmentListAPICall(doctorInfo))

})

export default connect(
    mapStateToProps,
    mapDispatchToprops
)(BookedAppontments);

