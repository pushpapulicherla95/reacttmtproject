import React, { Component } from 'react';
import PatientAppointments from "../../patient/telemedicine/appointment/Appointment";
import MyDoctor from "../../patient/telemedicine/appointment/MyDoctor";
import JoinMeet from "../../patient/telemedicine/appointment/Joinmeet";
import { Switch, Route, Redirect } from 'react-router-dom';

class TelemedicineRouting extends Component {
    render() {
        return (
            <Switch>
                <Redirect path="/telemedicine" exact to="/telemedicine/patientappointments" />

                <Route path="/telemedicine/patientappointments" component={PatientAppointments} />
                <Route path="/telemedicine/joinmeeting" component={JoinMeet} />
                <Route path="/telemedicine/mydoctor" component={MyDoctor} />



            </Switch>
        )
    }
}

export default TelemedicineRouting;