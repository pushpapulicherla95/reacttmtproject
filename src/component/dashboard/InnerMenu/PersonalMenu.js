import React, { Component } from 'react';
import PersonalRouting from "../innerrouting/PersonalRouting";
import HealthRecordRoute from "../innerrouting/HealthRecordRouting";
import Logout from '../../common/Logout'
import { connect } from "react-redux";
import { getPersonaldata } from "../../../service/healthrecord/action";
import AsideMenu from "../../common/AsideMenu"

class DashboardInnerMenu extends Component {
    constructor(props) {
        super(props);

    }
    handleChange = (path) => {
        const clientWidth = window.innerWidth;
        if (clientWidth < 479) {
            document.getElementById("submenuHideOnMobile").style.display = "none";
        }
        this.props.history.push(path)
    }
    handleArrow = () => {
        const clientWidth = window.innerWidth;
        if (clientWidth < 479) {
            if (document.getElementById("submenuHideOnMobile").style.display == "none") {
                document.getElementById("submenuHideOnMobile").style.display = "block";
            } else {
                this.props.history.push("/submenu");
                document.getElementById("submenuHideOnMobile").style.display = "none";
            }
        }
    }
    componentWillMount() {
        const { userInfo } = this.props.loginDetails;
    
        const listpersonal = {
          uid: userInfo.userId
        };
      

        this.props.getPersonaldata(listpersonal);
      }
    componentWillReceiveProps(nextprops){
        console.log("dskjfhkjdhgkjfdghlkdfnglkhb",nextprops)
        // const listpersonal = nextProps.getpersonalDetails.personal;

    }
    render() {
        // const gender = 
        return (
            <div className="inner-pageNew">
                <section>
                    <header className="header">
                        <div className="brand">
                            <a href="javascript:void(0);" className="backArrow" onClick={this.handleArrow }>
                                <i className="fas fa-chevron-left" ></i>
                            </a>
                            <div className="mobileLogo">
                                <a href="/mainmenu" className="logo">
                                    <img src="../images/logo.jpg" /> </a>
                            </div>

                        </div>
                        <div className="top-nav">

                            <div className="dropdown float-right mr-2" >
                                <a className="dropdown-toggle" data-toggle="dropdown">
                                    Patient Name
                            </a>
                                <Logout/>
                            </div>
                        </div>
                    </header>

                    <AsideMenu/>
                    {/* <aside className="sideMenu">
                        <ul className="sidemenuItems">
                            <li><a href="/mainmenu" onClick={() => this.props.history.push("/personal/data")} title="Personal"><img src="../images/icon2-color.png" /></a><div className="menu-text"><span>Personal</span></div></li>
                            <li><a href="/mainmenu"  title="Telemedicine" ><img src="../images/icon5-color.png" /></a><div className="menu-text"><span>Telemedicine</span></div></li>
                            <li><a href="/mainmenu" title="Health Records"><img src="../images/icon1-color.png" /></a><div className="menu-text"><span>Health Records</span></div></li>
                            <li><a href="/mainmenu" title="Pharmacy"><img src="../images/icon4-color.png" /></a> <div className="menu-text"><span>Pharmacy</span></div></li>
                            <li><a href="/mainmenu" title="Find Your medical specialist"><img src="../images/icon7-color.png" /></a><div className="menu-text"><span>Find Your medical specialist</span></div></li>

                            <li><a href="/mainmenu" title="Services"><img src="../images/icon7-color.png" /></a><div className="menu-text"><span>Services</span></div></li>
                            <li><a href="/mainmenu" title="Share Data"><img src="../images/icon6-color.png" /></a><div className="menu-text"><span>Share data</span></div></li>
                        </ul>

                    </aside> */}
                    <div className="inner-subMenu" id="submenuHideOnMobile">
                        <ul className="sid-subMenuitems">
                            <li><a title="Personal" onClick={() => this.handleChange("/personal/data")}>Personal</a></li>
                            <li><a title="Core Data" onClick={() => this.handleChange("/personal/coredata")}>Core Data</a></li>
                            <li><a title="Job" onClick={() => this.handleChange("/personal/job")}>Job</a></li>
                            <li><a title="Emergency" onClick={() => this.handleChange("/personal/emergency")}>Emergency</a></li>
                            <li><a title="Benefits" onClick={() => this.handleChange("/personal/benifit")}>Benefits</a></li>
                            <li><a title="Sports" onClick={() =>  this.handleChange("/personal/sports")}>Sports</a></li>
                            {this.props.gender && this.props.gender == "Female" &&       <li><a title="pregnancies" onClick={() =>  this.handleChange("/personal/pregancies")}>Pregnancies</a></li>}
                        </ul>
                    </div>
                    <PersonalRouting />
                </section>
            </div >
        )
    }

}
const mapStateToProps = state => {
    const personal = state.healthrecordReducer.getpersonalDetails && state.healthrecordReducer.getpersonalDetails.personal;
    // if (typeof personal !== 'undefined') {
       const personalDataJson = personal && JSON.parse(personal) ;
      
    return {
        loginDetails: state.loginReducer.loginDetails,
        getpersonalDetails: state.healthrecordReducer.getpersonalDetails,
        gender : personalDataJson && personalDataJson.gender
    }
//}
};

const mapDispatchToProps = dispatch => ({
    getPersonaldata: listpersonal => dispatch(getPersonaldata(listpersonal)),

});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DashboardInnerMenu);


