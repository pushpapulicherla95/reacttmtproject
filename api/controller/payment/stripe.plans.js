
const mysql = require("mysql");
const stripe = require("stripe")("sk_test_ieJg7gzaeD6CeCtDIsCTo2HX004YvQBPTT");

// Creating new Plans

const createPlans =  (request, response) => {
  const product={ name: "Gold", type: "service" }
  
  stripe.products.create(product, (err, product) => {
    console.log("payload", product);
    
    const paylod = {
      amount: 5000,
      interval: "month",
      product: {name:product.id}, 
      currency: "USD"
    };
    console.log("payload",paylod)
    stripe.plans.create(paylod, (err, plan) => {
      if (err) {
        response.json({
          status: false,
          message: "failure",
          data: err
        });
      } else {
        // insertDate(err, plan);
        return response.json({
          status: true,
          message: "success",
          data: plan
        });
      }
    });
  });

};
//  particular plans retrive
const retrivePlans = (request, response) => {
  const id = request.params.id;
  stripe.plans.retrieve(id, (err, plan) => {
    if (err) {
      response.json({
        status: false,
        message: "failure",
        data: err
      });
    } else {
      response.json({
        status: true,
        message: "success",
        data: plan
      });
    }
  });
};

//update the plans
const updatePlans = (request, response) => {
  const id= request.params.id
 
  stripe.plans.update(id, request.body,(err, plan) => {
      if (err) {
        response.json({
          status: false,
          message: "failure",
          data: err
        });
      } else {
        response.json({
          status: true,
          message: "success",
          data: plan
        });
      }
    }
  );
};

//Deleting the plans
const deletePlans = (request, response) => {
  const id = request.params.id;
  stripe.plans.del(id, (err, confirmation) => {
    if (err) {
      response.json({
        status: false,
        message: "failure",
        data: err
      });
    } else {
      response.json({
        status: true,
        message: "success",
        data: confirmation
      });
    }
  });
};

//Listing all plans
const listallPlans = (request, response) => {
  stripe.plans.list({ limit: 3 }, (err, plans) => {
    if (err) {
      response.json({
        status: false,
        message: "failure",
        data: err
      });
    } else {
      response.json({
        status: true,
        message: "success",
        data: plans
      });
    }
  });
};

insertDate=(err,plan)=>{
  console.log("plamm",  plan);
 pool.execute(
     "INSERT INTO subscription_plans(`plan_id`,`plan_name`,`amount`,`created`,`currency`,`interval`,`product`) values(?,?,?,?,?,?,?)",
     [
       plan.id,
       plan.object,
       plan.amount,
       plan.created,
       plan.currency,
       plan.interval,
       plan.product
     ]
   )
   .then(response => console.log("Response", response))
   .catch(error => {
     console.log("error", error);
   });
}

module.exports = {
  listallPlans,
  deletePlans,
  updatePlans,
  retrivePlans,
  createPlans
};

