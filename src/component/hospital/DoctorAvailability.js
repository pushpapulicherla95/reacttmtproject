import React, { Component } from "react";
import { connect } from "react-redux";
import { getHospitalDoctorList } from "../../service/hospital/action";
import { createDoctorSchedule, listScheduleInfo, updateDoctorSchedule } from "../../service/doctor/action";
import MultipleDatePicker from 'react-multiple-datepicker';
import moment from 'moment';
import { onlyNumbers, onlyAlphabets, onlyDate } from "../patient/personal/KeyValidation";
import { filter as _filter } from 'lodash';

class DoctorAvailability extends Component {
    constructor(props) {
        super(props);
        var loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
        const { userInfo } = loginDetails;
        var userId = userInfo.userId || "";
        this.state = {
            userId: userId,
            doctorList: [],
            startTime: "",
            endTime: "",
            meetingType: "",
            appointmentDate: "",
            addAvailabilityPop: false,
            doctorId: "",
            scheduleLists: [],
            totalScheduleList: [],
            viewschedule: false,
            status: "A",
            editSchedulePop: false,
            errors: {
                appointmentDate: "",
                startTime: "",
                endTime: "",
                meetingType: ""
            },
            appointmentDateValid: false,
            startTimeValid: false,
            endTimeValid: false,
            endTimeValid: false,
            meetingTypeValid: false,
            formValid: false,
            searchdate: moment().format("YYYY-MM-DD"),

        }
    }
    componentWillMount() {
        const date = new Date().getDate();
        console.log("new date",moment().format("YYYY-MM-DD"));
        this.setState({
            date: date
        });
        const info = {
            uid: this.state.userId
        }
        this.props.getHospitalDoctorList(info)
    }
    componentWillReceiveProps(nextProps) {
        console.log("nextProps>>>",nextProps);
        if (nextProps.hospitalDoctorList && nextProps.hospitalDoctorList.hospitalDoctorList) {
            this.setState({ doctorList: nextProps.hospitalDoctorList.hospitalDoctorList })

        }
        if (nextProps.listscheduleDetails && nextProps.listscheduleDetails.scheduleinfo) {
            const { scheduleinfo } = nextProps.listscheduleDetails;

            this.setState({ scheduleLists: scheduleinfo, totalScheduleList: scheduleinfo })

        }
    }
    modalPopupOpen = (event) => {

        this.setState({
            addAvailabilityPop: true,
            doctorId: event.userInfoId

        });
    }
    handleChange = (e) => {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value }, () => {
            this.validateField(name, value);
        })
    }
    modalPopUpClose = () => {
        this.setState({
            addAvailabilityPop: false,
            editSchedulePop: false,
            doctorId: ""

        });
    }
    handleSelect = (day) => {

        this.setState({ appointmentDate: day }, () => {
            this.validateField("appointmentDate", day);
        })
    }
    viewAvailability = (data) => {
        const scheduleinfo = {
            uid: data.userInfoId,
            appointmentDate:moment().format("YYYY-MM-DD")
        }
        this.setState({ viewschedule: true })
        this.setState({ doctorId: data.userInfoId })
        this.props.listScheduleInfo(scheduleinfo);
    }
    editSchedule = (event) => {
        this.setState({
            viewschedule: true,
            editSchedulePop: true,
            editdate: event.appointmentDate,
            status: event.status,
            startTime: event.startTime,
            endTime: event.endTime,
            meetingType: event.meetingType,
            scheduleId: event.scheduleId,
            doctorId: event.userId
        });
    }

    validateField(fieldName, value) {
        const { errors, appointmentDateValid, startTimeValid, endTimeValid, appointmentDate, startTime, endTime, meetingType, meetingTypeValid } = this.state;

        let appointmentTest = appointmentDateValid;
        let startTimeTest = startTimeValid;
        let endTimeTest = endTimeValid;
        let meetingTypeTest = meetingTypeValid;
        let fieldValidationErrors = errors;


        switch (fieldName) {
            case "appointmentDate":
                appointmentTest = appointmentDate.length > 0;
                fieldValidationErrors.appointmentDate = appointmentTest ? "" : "Please Select Valid appontmentdate"
                break;
            case "startTime":
                startTimeTest = startTime.length > 0;
                fieldValidationErrors.startTime = startTimeTest ? "" : "Please Select Valid Starttime"
                break;
            case "endTime":
                endTimeTest = endTime.length > 0;
                fieldValidationErrors.endTime = endTimeTest ? "" : "Please Select Valid Endtime"
                break;
            case "meetingType":
                meetingTypeTest = meetingType.length > 0;
                fieldValidationErrors.meetingType = meetingTypeTest ? "" : "Please Select Meetingtype"
                break;

        }
        this.setState({
            errors: fieldValidationErrors,
            appointmentDateValid: appointmentTest,
            startTimeValid: startTimeTest,
            endTimeValid: endTimeTest,
            meetingTypeValid: meetingTypeTest
        },
            this.validateForm
        )
    }
    validateForm() {
        const { appointmentDateValid, startTimeValid, endTimeValid, meetingTypeValid } = this.state;
        this.setState({
            formValid: appointmentDateValid && startTimeValid && endTimeValid && meetingTypeValid
        })

    }
    handleSchedule = () => {
        let appointment = [];
        const { appointmentDate, status, startTime, endTime, meetingType, doctorId } = this.state;
        appointmentDate.length > 0 && appointmentDate.map((each, i) => {
            appointment.push(moment(each).format("YYYY-MM-DD"))
        })
        const allListSchedulesArray = this.props.listscheduleDetails && this.props.listscheduleDetails.scheduleinfo;
        let validDate = true;
        (allListSchedulesArray && allListSchedulesArray.length > 0) && allListSchedulesArray.map((data) => {

            if (appointment.includes(data.appointmentDate)) {
                const currentStartTime = moment(startTime, "HH:mm ");
                const startTimeMoment = moment(data.startTime, "HH:mm ");
                const endTimeMoment = moment(data.endTime, "HH:mm");
                const amIBetween = currentStartTime.isBetween(startTimeMoment, endTimeMoment);
                if (amIBetween) {
                    const errors = this.state.errors;
                    errors.appointmentDate = `Please choose different timing for ${data.appointmentDate}`
                    validDate = false
                    this.setState({
                        errors: errors
                    }, () => {
                        setTimeout(() => {
                            errors.appointmentDate = ""
                            this.setState({
                                errors: errors
                            })
                        }, 1000)
                    })
                }
            }
        });

        if (validDate) {
            const doctorSchedule = {
                doctorId: doctorId,
                startTime: startTime,
                endTime: endTime,
                appointmentDate: appointment,
                status: "A",
                meetingType: meetingType
            }
            this.props.createDoctorSchedule(doctorSchedule);

            this.setState({
                addSchedulepop: false,
                startTime: "",
                endTime: "",
                appointmentDate: "",
                status: "",
                meetingType: "",
                appointment: [],
                addAvailabilityPop: false,
                doctorId: ""

            });
        }
    }
    //updateSchedule
    updateSchedule = () => {

        const { status, startTime, endTime, meetingType, scheduleId, editdate, doctorId } = this.state;

        const updateSchedule = {
            uid: doctorId,
            scheduleId: scheduleId,
            startTime: startTime,
            endTime: endTime,
            appointmentDate: editdate,
            status: status,
            meetingType: meetingType
        }
        this.props.updateDoctorSchedule(updateSchedule);
        this.setState({ editSchedulePop: false });
        const scheduleinfo = {
            "uid": doctorId
        }
        this.props.listScheduleInfo(scheduleinfo);
        this.setState({ doctorId: "" })
    }
    dateSearch = (e) => {
        const { scheduleLists, totalScheduleList ,searchdate} = this.state;
        // const result = _filter(totalScheduleList, function (list) {

        //     return list.appointmentDate == searchdate;
        // })
        // this.setState({ scheduleLists: result.length == 0 ? totalScheduleList : result })
        const scheduleinfo = {
            uid: this.state.doctorId,
            appointmentDate:moment(searchdate).format("YYYY-MM-DD")
        }
        this.props.listScheduleInfo(scheduleinfo);
        this.setState({searchdate:""});
    }
    render() {
        const { doctorList, startTime, endTime, scheduleLists, editdate, meetingType, status, formValid, errors, date ,searchdate,totalScheduleList} = this.state;
        return (
            <div>
                <div className="mainMenuSide" id="main-content">
                    <div className="wrapper">
                        <div className="container-fluid">
                            <div className="body-content">
                                <div className="tomCard">
                                    <div className="tab-menu-content tomCardBody">

                                        <div className="tab-menu-title">
                                            <ul>
                                                <li className="menu1 active"><a href="JavaScript:Void(0);">
                                                    <p>List of Availablity </p>
                                                </a></li>
                                            </ul>
                                        </div>
                                        {this.state.viewschedule && <div className="tab-menu-content-section">
                                            <div id="content-1" style={{ display: "block" }}>

                                                <div className="row">
                                                    <div className="col-lg-6 col-md-3 col-sm-12 col-12">
                                                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                                                        <div className="searchAvailablity mobile-width-100" >
                                                            <input type="date" className="searchInputDate" name="searchdate" value={searchdate} onChange={this.handleChange} onKeyPress={onlyDate} />
                                                            <div className="calenderIcon"><i className="fas fa-calendar-alt"></i></div>
                                                        </div>
                                                        <div>
                                                            <button type="button" class="commonBtn2 mobile-width-100" onClick={this.dateSearch}> <i class="fa fa-search docPlus"></i> Search</button>
                                                        </div>
                                                        <div>
                                                            <button type="button" class="commonBtn2 mobile-width-100" onClick={() => this.setState({ scheduleLists: totalScheduleList, searchdate: "" })}> <i class="fa fa-eye docPlus"></i> Show all list</button>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="row mt-3 mb-3">
                                                    {
                                                        scheduleLists.length > 0 ? scheduleLists.map((each, i) => (
                                                            <div key={i} className="col-lg-3 col-md-3 col-sm-4 col-6 padRig">
                                                                <div className="availablityCard">
                                                                    <div className="cartType">
                                                                        <p className="video">{each.meetingType}</p>
                                                                    </div>
                                                                    <div className="cardTime">
                                                                        <p>{each.appointmentDate} &nbsp;&nbsp;({each.startTime} - {each.endTime})</p>
                                                                    </div>
                                                                    <div className="cardAction">
                                                                        <div className="availableEdit">
                                                                            <a href="javascript:void(0)" title="Edit" onClick={() => this.editSchedule(each)}><i className="far fa-edit"></i> <span>Edit</span></a>
                                                                        </div>
                                                                        {each.status == "A" && <div className="availableStatus">
                                                                            <a href="" title="Available">  <i className="fas fa-check"></i> <span>Available</span></a>

                                                                        </div>}
                                                                        {each.status == "NA" && <div className="availableDelete">
                                                                            <a href="javascript:void(0)" title="Not Avaliable"><i className="fas fa-times"></i> <span>Not Avaliable</span></a>
                                                                        </div>}

                                                                        {/* <div class="availableDelete">
                                                                            <a href="javascript:void(0)" title="Delete" onClick={(e) => this.deleteScheduleopen(each)}><i class="far fa-trash-alt"></i></a>
                                                                        </div> */}

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        )) : <div>No availability </div>
                                                    }

                                                </div>
                                            </div>
                                        </div>
                                        }
                                        {!this.state.viewschedule && <div className="tab-menu-content-section">
                                            <div id="content-1" style={{ display: "block" }}>
                                                <div className="row mt-3 mb-3">
                                                    <div className="table-responsive">
                                                        {doctorList.length > 0 ? <table className="table table-hover">
                                                            <thead className="thead-default">
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Name</th>
                                                                    <th>Specialist</th>
                                                                    <th>Contact</th>
                                                                    <th>Action</th>
                                                                    {/* <th>View Availablity</th> */}
                                                                </tr>

                                                            </thead>
                                                            <tbody>
                                                                {doctorList.length > 0 && doctorList.map((value, i) =>
                                                                    <tr key={i}>
                                                                        <td data-label="#">{i + 1}</td>
                                                                        <td data-label="Name">{value.firstName}</td>
                                                                        <td data-label="Date">{value.specialListName}</td>
                                                                        <td data-label="Timings">{value.phone}</td>
                                                                        <td data-label="Action">
                                                                            <a title="View Appoinments" className="commonBtn2 mr-2" data-toggle="modal" data-target="#myModal1" onClick={() => this.modalPopupOpen(value)}>Add Availability</a>
                                                                            <a title="View Appoinments" className="commonBtn2 " data-toggle="modal" data-target="#myModal1" onClick={() => this.viewAvailability(value)}>View Availability</a>
                                                                        </td>
                                                                        {/* <td data-label="Availablity"><a title="View Appoinments" className="commonBtn2 " data-toggle="modal" data-target="#myModal1" onClick={() => this.viewAvailability(value)}>View Availability</a></td> */}
                                                                    </tr>
                                                                )
                                                                }
                                                            </tbody>
                                                        </table> : "No Availability list"
                                                        }
                                                    </div>

                                                </div>
                                            </div>


                                        </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* add doctors availability */}
                <div className={this.state.addAvailabilityPop ? "modal show d-block" : "modal"}>
                    <div className="modal-dialog">
                        <div className="modal-content modalPopUp">
                            <div className="modal-header borderNone">
                                <h5 className="modal-title">Add  Availablity</h5>
                                <button type="button" className="popupClose" data-dismiss="modal" onClick={this.modalPopUpClose}><i className="fas fa-times"></i></button>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="row">

                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group position-relative datePickerinput">
                                                <label class="commonLabel d-block">Date</label>
                                                {/* <input type="text" class="form-control"  name="appointmentDate" /> */}
                                                <MultipleDatePicker minDate={new Date()}
                                                    onSubmit={dates => this.handleSelect(dates)} />
                                                <div style={{ color: "red" }}>{errors.appointmentDate}</div>
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <div className="form-group">
                                                <label className="commonLabel">Start Time</label>
                                                <input type="time" className="form-control" name="startTime" onChange={this.handleChange} value={startTime} />
                                                <div style={{ color: "red" }}>{errors.startTime}</div>
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <div className="form-group">
                                                <label className="commonLabel">End Time</label>
                                                <input type="time" className="form-control" name="endTime" onChange={this.handleChange} value={endTime} />
                                                <div style={{ color: "red" }}>{errors.endTime}</div>
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <div className="form-group">
                                                <label className="commonLabel">Type</label>
                                                <select className="form-control" onChange={this.handleChange} name="meetingType">
                                                    <option value="">Select Meeting Type</option>
                                                    <option value="video call">Video Call</option>
                                                    <option value="office meeting">Office Meeting</option>
                                                </select>
                                                <div style={{ color: "red" }}>{errors.meetingType}</div>
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <div className="form-group">
                                                <label className="commonLabel">Availablity</label>
                                                <select className="form-control" name="status" onChange={this.handleChange}>
                                                    <option value=""> Select Available</option>
                                                    <option value="A">Available</option>
                                                    <option value="NA">Not Available</option>
                                                </select>

                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <button type="button" className="commonBtn float-right" disabled={!formValid} onClick={this.handleSchedule}>Create</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Edit pop */}
                <div className={this.state.editSchedulePop ? "modal show d-block" : "modal"} id="edit_btn">
                    <div className="modal-dialog">
                        <div className="modal-content modalPopUp">
                            <div className="modal-header borderNone">
                                <h5 className="modal-title">Add  Availablity</h5>
                                <button type="button" className="popupClose" data-dismiss="modal" onClick={this.modalPopUpClose}><i className="fas fa-times"></i></button>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="row">
                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <div className="form-group position-relative datePickerinput">
                                                <label className="commonLabel d-block">Date</label>
                                                <input type="date" value={editdate} onChange={this.handleChange} min={moment().format("YYYY-MM-DD")} name="editdate" onKeyPress={onlyNumbers} minDate={date} />
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <div className="form-group">
                                                <label className="commonLabel">Start Time</label>
                                                <input type="time" className="form-control" name="startTime" onChange={this.handleChange} value={startTime} />
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <div className="form-group">
                                                <label className="commonLabel">End Time</label>
                                                <input type="time" className="form-control" name="endTime" onChange={this.handleChange} value={endTime} />
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <div className="form-group">
                                                <label className="commonLabel">Type</label>
                                                <select className="form-control" onChange={this.handleChange} value={meetingType} name="meetingType">
                                                    <option value="video call">Video Call</option>
                                                    <option value="office meeting">Office Meeting</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <div className="form-group">
                                                <label className="commonLabel">Availablity</label>
                                                <select className="form-control" name="status" value={status} onChange={this.handleChange}>
                                                    <option value="A">Available</option>
                                                    <option value="NA">Not Available</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-md-12 col-lg-12 col-sm-12">
                                            <button type="button" className="commonBtn float-right" onClick={this.updateSchedule}>Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal" id="myModal2">
                    <div className="modal-dialog">
                        <div className="modal-content modalPopUp">
                            <div className="modal-header borderNone">
                                <h5 className="modal-title">Delete</h5>
                                <button type="button" className="popupClose" data-dismiss="modal"><i className="fas fa-times"></i></button>
                            </div>
                            <div className="modal-body">
                                <h4 className="deleteMsg"><i className="fas fa-info-circle"></i> Are you sure you want to delete this record</h4>
                                <button type="text" className="commonBtn smallBtn float-right" data-dismiss="modal">No</button>
                                <button type="text" className="commonBtn smallBtn float-right mr-2">Yes</button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails,
    hospitalDoctorList: state.hospitalReducer.hospitalDoctorList,
    listscheduleDetails: state.doctorReducer.listscheduleDetails,

})

const mapDispatchToprops = dispatch => ({
    getHospitalDoctorList: (info) => dispatch(getHospitalDoctorList(info)),
    createDoctorSchedule: (doctorSchedule) => dispatch(createDoctorSchedule(doctorSchedule)),
    listScheduleInfo: (scheduleinfo) => dispatch(listScheduleInfo(scheduleinfo)),
    updateDoctorSchedule: (updateSchedule) => dispatch(updateDoctorSchedule(updateSchedule))



})

export default connect(
    mapStateToProps,
    mapDispatchToprops
)(DoctorAvailability);
