import React , {Component} from 'react';
import { NavLink  } from 'react-router-dom';
import { withRouter } from "react-router";
import LandingHeader from '../common/LandingHeader';
class Aboutus extends Component{
    componentDidMount() {
        window.scrollTo(0, 0)
    }
    render(){
        return(
<>
<LandingHeader/>
<section class="about-banner d-flex justify-content-center align-items-center">
		<div class="container">
			<div class="bannerContent bg-transperent flex-column">
				<h1>
					<span>About </span>
				</h1>
				
			</div>
		</div>
	</section>
	<div class="emergencyAlert">
		<a href="" > <img src="images/emergencyCall.png"/> </a>
	</div>
<section class="about-content">
	<div class="container">
		<div class="row">
				<div class="col-lg-9 col-md-9 col-sm-9 col-12">
				
						<p class="static_para">in my profession as a doctor (10 years orthopedist in Bad Kötzting in the Bavarian Forest) I have often struggled with missing or inaccurate data. Since I´m mainly active as a shoulder surgeon, I need X-ray and MRI images, as well as findings of pre-treating physicians.</p>
						<p class="static_para">Because the data streams in the medical system do not work as they should, we developed a patient centered medical platform and mobile apps for IPHONE and ANDROID with cloud for the first healthcare market that will revolutionize the medical system. Meanwhile we developed a huge multilingual platform with translations and added a lot of important, very useful emergency features like SMS with GPS.</p>
						<p class="static_para">A legal clarification is made operational. The app is designed for patients. But it also offers opportunities for interaction with Medical Professionals. The app can be extended to the completely digital recipe with QR-code, where patients can participate and benefit from having no smartphone itself (e.g. the elderly)</p>
						
					</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-12">
					<img class="img-fluid about-hospital" src="images/photo-lemberger.jpg"/>
				</div>

		</div>
		<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-12">
						<p class="static_para">Best wishes,</p>
						<p style={{margin:"0"}}><img class="img-fluid" src="images/signing-lemberger.png" width="240px"/></p>
						<p class="static_para">
								Dr. med. Matthias Lemberger</p>
								<p class="static_para">Specialist in orthopedics and trauma surgery, acupuncture, chiropractic, emergency medicine, sports medicine, inpatient and outpatient surgeries, D-physician professional association.</p>
					</div>
					</div>
		</div>	
</section>
<footer>
          <div class="footerLink">
            <div class="container">
              <ul>
                <li>
                  <NavLink to="/aboutus" >About </NavLink>
                </li>
                <li>
                <NavLink to="/partners" >For Partners </NavLink>
                </li>
                <li>
                <NavLink to="/contact">Contact </NavLink>
                </li>
                <li>
                <NavLink to="/datasecurity">Data Security </NavLink>
                </li>
                <li>
                <NavLink to="/privacypolicy" > Privacy Policy </NavLink>
                </li>
                <li>
                <NavLink to="/imprint">Imprint </NavLink>
                </li>
              </ul>
            </div>
          </div>

          <div class="footerCopyRights">
            <div class="container">
              <ul>
                <li>
                  <a href="">
                    <i class="fab fa-facebook-f" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-google-plus-g" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-instagram" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-twitter" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-linkedin-in" />
                  </a>
                </li>
              </ul>
              <p>Copyrights @ 2019, All Rights Reserved</p>
            </div>
          </div>
        </footer>
</>
        );
    }
}
export default Aboutus;