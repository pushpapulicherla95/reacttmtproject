import React, { Component } from "react";
import { connect } from "react-redux";
import { getPatientHealthData } from "../../../../service/patient/action";
import { getUploadFiles,handleHealthDataDelete } from "../../../../service/upload/action";
import { toastr } from "react-redux-toastr";
import {
  addMedicine,
  deleteMedicaldata,
  editMedicaldata
} from "../../../../service/healthrecord/action";
import axios from "axios";
import URL from "../../../../asset/configUrl";
import { awsFiles } from "../../../../service/common/action";

class AddPatient extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 1,
      name: "",
      dose: "",
      title: "",
      medicine: "",
      patientHealthListData: [],
      tabone: true,
      tabtwo: false,
      addmedicine: false,
      editmedicine: false,
      editDetails: {},
      uploadPopup: false,
      name: "",
      fileName: "",
      fileType: "",
      fileURL: "",
      errors: {
        title: "",
        name: ""
      },
      titleValid: false,
      nameValid: false,
      firstValid: false
    };
  }

  handleChangePopup = e => {
    var reader = new FileReader();
    var file = e.target.files[0];
    let name = file.name.split('.')[0]
    let type = "." + file.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileURL: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(file);
  };
  popupSubmit = () => {
    var phrase = this.state.fileURL;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);
    var { userInfo } = this.props.loginDetails;
    const payload = {
      uid: userInfo.userId,
      z: match[1],
      fileName: this.state.fileName,
      fileType: this.state.fileType,
      name: this.state.name,
      category: "Medicine"
    };
    axios
      .post(URL.FILEMEDITATION_UPLOAD, payload)
      .then(response => {
        response.data.status === "success"
          ? toastr.success("Message", response.data.message)
          : toastr.error("Message", response.data.message);
        if (response.data.status === "success") {
          var { userInfo } = this.props.loginDetails;
          const uploadFiles = {
            uid: userInfo.userId,
            category: "Medicine"
          };
          this.props.getUploadFiles(uploadFiles);
          this.setState({ uploadPopup: false });
        }
      })
      .catch(error => {
        this.setState({ uploadPopup: true });
      });
  };

  // trigger = () => {
  //   this.setState({ uploadPopup: !this.state.uploadPopup });
  // };
  componentWillMount() {
    const { userInfo } = this.props.loginDetails;

    const userinfo = {
      uid: userInfo.userId
    };

    // var { userInfo } = this.props.loginDetails;
    const uploadFiles = { uid: userInfo.userId, category: "Medicine" };
    this.props.getUploadFiles(uploadFiles);
    this.props.getPatientHealthData(userinfo);
  }
  componentWillReceiveProps(nextProps) {

    if (
      nextProps.patientHealthList &&
      nextProps.patientHealthList.medictation != null &&
      nextProps.patientHealthList.medictation != 0
    ) {
      const medicineList = nextProps.patientHealthList.medictation;
      let parsedContent = JSON.parse(medicineList);

      let patientHealthListData = parsedContent.medication;
      this.setState({
        patientHealthListData,
        addallergiespop: false
      });
    }
  }

  handleChange = e => {
    e.preventDefault();
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };
  validateField(fieldName, value) {
    const { errors, titleValid, nameValid } = this.state;

    let titlevalid = titleValid;
    let namevalid = nameValid;
    let fieldValidationErrors = errors;
    switch (fieldName) {
      case "title":
        titlevalid = !(value.length < 3 || value.length > 20);
        fieldValidationErrors.title = titlevalid
          ? ""
          : "Title should be 3 to 20 characters";
        break;
      case "name":
        namevalid = !(value.length < 3 || value.length > 20);
        fieldValidationErrors.name = namevalid
          ? ""
          : "Name should be 3 to 20 characters";
        break;
    }
    this.setState(
      {
        errors: fieldValidationErrors,
        titleValid: titlevalid,
        nameValid: namevalid
      },
      this.validateForm
    );
  }

  validateForm() {
    const { titleValid, nameValid } = this.state;
    this.setState({
      firstValid: titleValid && nameValid
    });
  }

  tabOne = () => {
    this.setState({ tabone: true, tabtwo: false });
  };
  tabTwo = () => {
    this.setState({ tabone: false, tabtwo: true });
  };
  addallergiespop = () => {
    this.setState({ addmedicine: true }, () => {
      this.emptystate();
    });
  };

  modalPopUpClose = () => {
    this.setState({
      addmedicine: false,
      editmedicine: false
    });
  };
  modalPopupOpen = e => {
    this.setState({
      name: e.name,
      title: e.title,
      dose: e.dose,
      medicine: e.medicine,
      editmedicine: true
    });
  };
  //update
  updateAllery = () => {
    const { name, title, dose, medicine } = this.state;
    const { userInfo } = this.props.loginDetails;

    const editmedicine = {
      uid: userInfo.userId,
      medication: [
        {
          name: name,
          title: title,
          trigger: dose,
          description: medicine
        }
      ]
    };

    this.props.editMedicaldata(editmedicine);
    this.setState({ editmedicine: false });
  };

  deleteMedicine = e => {
    const { userInfo } = this.props.loginDetails;

    const deletepatient = {
      uid: userInfo.userId,
      medication: [
        {
          name: e.name,
          title: e.title,
          trigger: e.dose,
          description: e.medicine
        }
      ]
    };
    this.props.deleteMedicaldata(deletepatient);
  };
  handleMedicine = () => {
    const { name, title, dose, medicine } = this.state;
    const { userInfo } = this.props.loginDetails;

    const medicineInfo = {
      uid: userInfo.userId,
      medication: [
        {
          name: name,
          title: title,
          dose: dose,
          medicine: medicine
        }
      ]
    };

    this.props.addMedicine(medicineInfo);
    this.setState({
      addmedicine: false
    });
  };
  emptystate = () => {
    this.setState({
      name: "",
      title: "",
      dose: "",
      medicine: ""
    });
  };

  handleAwsFiles = value => {
    awsFiles(value).then(response => {
      window.location.href = response;
    });
  };

  render() {
    const {
      tabone,
      tabtwo,
      patientHealthListData,
      addmedicine,
      name,
      title,
      dose,
      editmedicine,
      firstValid,
      errors
    } = this.state;
    return (
      <React.Fragment>
        <div className="" id="main-content">
          <div className="wrapper">
            <div className="container-fluid">
              <div className="body-content">
                <div className="tomCard">
                  <div className="tomCardHead">
                    <h5 className="float-left">Medicines </h5>
                  </div>
                  <div className="tab-menu-content">
                    <div className="tab-menu-title">
                      <ul>
                        <li className={this.state.tabone ? "menu1 active" : ""}>
                          <a href="#" onClick={this.tabOne}>
                            <p>List of Medicines</p>
                          </a>
                        </li>
                        <li className={this.state.tabtwo ? "menu1 active" : ""}>
                          <a href="#" onClick={this.tabTwo}>
                            <p>Attached Files</p>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  {tabone && (
                    <div className="tab-menu-content-section">
                      <div
                        id="content-1"
                        className={tabone ? "d-block" : "d-none"}
                      >
                        <button
                          type="button"
                          onClick={this.addallergiespop}
                          className="commonBtn2 float-right mb-2"
                        >
                          <i className="fa fa-plus" /> Add New Entry
                         </button>
                        <div className="table-responsive">
                          <table className="table table-hover">
                            <thead className="thead-default">
                              <tr>
                                <th>Name</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {patientHealthListData.length > 0 ? (
                                patientHealthListData.map((each, i) => (
                                  <tr key={i}>
                                    <td data-label="startDate">{each.name}</td>
                                    <td data-label="Action">
                                      <a
                                        title="Edit"
                                        className="editBtn"
                                        onClick={e => this.modalPopupOpen(each)}
                                      >
                                        <span id={5} className="fa fa-edit" />
                                      </a>
                                      <a
                                        title="Delete"
                                        className="deleteBtn"
                                        onClick={e => this.deleteMedicine(each)}
                                      >
                                        <i
                                          className="fa fa-trash"
                                          data-toggle="modal"
                                          data-target="#myModal2"
                                        />
                                      </a>
                                    </td>
                                  </tr>
                                ))
                              ) : (
                                <tr>No data available</tr>
                              )}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  )}

                  {tabtwo && (
                    <div
                      id="content-2"
                      className={tabtwo ? "d-block" : "d-none"}
                    >
                      <button
                        type="button"
                        className="commonBtn2 float-right mb-2"
                        onClick={() =>
                          this.setState({
                            uploadPopup: !this.state.uploadPopup
                          })
                        }
                      >
                        <i className="fa fa-plus" /> <span> Upload new files</span>
                      </button>
                      <div className="table-responsive">
                        <table className="table table-hover">
                          <thead className="thead-default">
                            <tr>
                              <th>#</th>
                              <th>Name</th>
                            <th>FileName</th>
                            <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.props.getFilesDetails.fileDetails &&
                              this.props.getFilesDetails.fileDetails.map(
                                (each, i) => (
                                  <tr>
                                    <td data-label="#">{i + 1}</td>
                                    <td data-label="Name">{each.name}</td>
                                    <td data-label="FileName"> {each.fileName}</td>

                                    <td data-label="Action">
                                      <button
                                        class="download_btn"
                                        onClick={() =>
                                          this.handleAwsFiles(each.fileUrl)
                                        }
                                      >
                                        <i class="fas fa-download" /> 
                                      </button>
                                      <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.props.handleHealthDataDelete(each.id, each.fileUrl, "Medicine")
                                      }
                                    >
                                      <i class="fas fa-trash" />
                                    </button>
                                    </td>
                                  </tr>
                                )
                              )}
                            {/* <tr>
                              <td data-label="#">1</td>
                              <td data-label="Name">
                                Eye Care Test Report
                              </td>
                              <td data-label="Type">png</td>
                              <td data-label="Size">87.9 kB</td>
                              <td data-label="Date">16.05.2019 11:56</td>
                              <td data-label="Action">
                                <a
                                  title="Edit"
                                  className="editBtn"
                                  data-toggle="modal"
                                  data-target="#myModal"
                                >
                                  <i className="fa fa-edit" />
                                </a>
                                <a
                                  title="Delete"
                                  className="deleteBtn"
                                  onClick={e => this.deletePatient()}
                                >
                                  <i
                                    className="fa fa-trash"
                                    data-toggle="modal"
                                    data-target="#myModal2"
                                  />
                                </a>
                              </td>
                            </tr> */}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  )}
                </div>

                <div
                  className={addmedicine ? "modal d-block" : "modal"}
                  id="myModal"
                >
                  <div className="modal-dialog">
                    <div className="modal-content modalPopUp">
                      <div className="modal-header borderNone">
                        <h5 className="modal-title">Add Medicines</h5>
                        <button
                          type="button"
                          className="close"
                          data-dismiss="modal"
                          onClick={this.modalPopUpClose}
                        >
                          <i className="fas fa-times" />
                        </button>
                      </div>
                      <div className="modal-body">
                        <form>
                          <div className="row">
                            <div className="col-md-12 col-lg-12 col-sm-12">
                              <div className="form-group">
                                <label className="form-label">
                                  Title for list entry (e.g. Heart drug){" "}
                                  <sup>*</sup>
                                </label>
                                <input
                                  className="form-control"
                                  name="title"
                                  onChange={this.handleChange}
                                  value={title}
                                  type="text"
                                />
                                <div style={{ color: "red" }}>
                                  {errors.title}
                                </div>
                                <span />
                              </div>
                            </div>
                            <div className="col-md-12 col-lg-12 col-sm-12">
                              <div className="form-group">
                                <label className="form-label">
                                  Name <sup>*</sup>
                                </label>
                                <input
                                  className="form-control"
                                  name="name"
                                  onChange={this.handleChange}
                                  value={name}
                                  type="text"
                                />
                                <div style={{ color: "red" }}>
                                  {errors.name}
                                </div>
                                <span />
                              </div>
                            </div>
                            <div className="col-md-12 col-lg-12 col-sm-12">
                              <div className="form-group">
                                <label className="form-label">
                                  Frequency of medicine intake
                                </label>
                                <select
                                  className="form-control"
                                  name="medicine"
                                  onChange={this.handleChange}
                                >
                                  <option value="1x perday">1x perday</option>
                                  <option value="2x perday">2x perday</option>
                                </select>{" "}
                              </div>
                            </div>
                            <div className="col-md-12 col-lg-12 col-sm-12">
                              <div className="form-group">
                                <label className="form-label">Dose</label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="dose"
                                  onChange={this.handleChange}
                                  type="text"
                                  value={dose}
                                />
                                <span />
                              </div>
                            </div>
                            <div className="col-md-12 col-lg-12 col-sm-12">
                              <button
                                type="button"
                                className="commonBtn float-right"
                                onClick={this.handleMedicine}
                                disabled={!firstValid} 
                              >
                                Add
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  className={editmedicine ? "modal d-block" : "modal"}
                  id="myModal"
                >
                  <div className="modal-dialog">
                    <div className="modal-content modalPopUp">
                      <div className="modal-header borderNone">
                        <h5 className="modal-title">Edit medicines</h5>
                        <button
                          type="button"
                          className="close"
                          onClick={this.modalPopUpClose}
                        >
                          <i className="fas fa-times" />
                        </button>
                      </div>
                      <div className="modal-body">
                        <form>
                          <div className="row">
                            <div className="col-md-12 col-lg-12 col-sm-12">
                              <div className="form-group">
                                <label className="form-label">
                                  Title for list entry (e.g. Heart drug)
                                </label>
                                <input
                                  className="form-control"
                                  name="title"
                                  onChange={this.handleChange}
                                  defaultValue={this.state.title}
                                  type="text"
                                  readOnly
                                />
                                <span />
                              </div>
                            </div>
                            <div className="col-md-12 col-lg-12 col-sm-12">
                              <div className="form-group">
                                <label className="form-label">Name</label>
                                <input
                                  className="form-control"
                                  name="name"
                                  onChange={this.handleChange}
                                  defaultValue={this.state.name}
                                  type="text"
                                />
                                <span />
                              </div>
                            </div>
                            <div className="col-md-12 col-lg-12 col-sm-12">
                              <div className="form-group">
                                <label className="form-label">
                                  Frequency of medicine intake
                                </label>
                                <select
                                  className="form-control"
                                  name="medicine"
                                  onChange={this.handleChange}
                                >
                                  <option value="1x perday">1x perday</option>
                                  <option value="2x perday">2x perday</option>
                                </select>{" "}
                              </div>
                            </div>
                            <div className="col-md-12 col-lg-12 col-sm-12">
                              <div className="form-group">
                                <label className="form-label">Dose</label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="dose"
                                  onChange={this.handleChange}
                                  type="text"
                                  value={this.state.dose}
                                />
                                <span />
                              </div>
                            </div>
                            <div className="col-md-12 col-lg-12 col-sm-12">
                              <button
                                type="button"
                                className="commonBtn float-right"
                                onClick={this.updateAllery}
                              >
                                Update
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            className={this.state.uploadPopup ? "modal d-block" : "modal"}
            id="myModal"
          >
            <div class="modal-dialog">
              <div class="modal-content modalPopUp">
                <div class="modal-header borderNone">
                  <h5 class="modal-title">Upload File</h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    onClick={() =>
                      this.setState({
                        uploadPopup: !this.state.uploadPopup
                      })
                    }
                  >
                    <i class="fas fa-times" />
                  </button>
                </div>
                <div class="modal-body">
                  <form>
                    <div class="row">
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="form-group">
                          <label className="form-label">File name</label>
                          <input
                            name="name"
                            className="form-control"
                            type="text"
                            onChange={this.handleChange}
                          />
                          <span />
                        </div>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="fileUp">
                          <label
                            for="fileUp1"
                            class="commonBtn text-center upload"
                          >
                            Upload File
                          </label>
                          <input
                            type="file"
                            id="fileUp1"
                            style={{ display: "none" }}
                            onChange={this.handleChangePopup}
                          />
                          {/* <p style={{ textAlign: "center" }}>hsfkjhsfj</p> */}
                        </div>
                      </div>

                      <div class="col-md-12 col-lg-12 col-sm-12 mt-2">
                        <button
                          type="button"
                          class="commonBtn float-right"
                          onClick={this.popupSubmit} disabled={!this.state.fileURL} 
                        >
                          Add
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  patientHealthList: state.patientReducer.patientHealthList,
  loginDetails: state.loginReducer.loginDetails,
  getFilesDetails: state.uploadReducer.getFilesDetails
});

const mapDispatchToProps = dispatch => ({
  addMedicine: medicineInfo => dispatch(addMedicine(medicineInfo)),
  editMedicaldata: editmedicine => dispatch(editMedicaldata(editmedicine)),
  getPatientHealthData: userinfo => dispatch(getPatientHealthData(userinfo)),
  deleteMedicaldata: deletepatient =>
    dispatch(deleteMedicaldata(deletepatient)),
  getUploadFiles: payload => dispatch(getUploadFiles(payload)),
  handleHealthDataDelete: (id, fileUrl, type) => dispatch(handleHealthDataDelete(id, fileUrl, type))

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddPatient);
