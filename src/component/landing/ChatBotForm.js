import React, { Component } from "react";

class ChatBotForm extends Component {
  constructor(props) {
    super(props);
    this.demo = React.createRef();
    this.state = {
      viewType: 1,
      steps: "step",
      type: "step1",
      name: "",
      email: "",
      weight: "",
      height: "",
      dateofBirth: "",
      gender: "",
      sugar: "",
      bloodPressure: "",
      loading: false
    };
  }

  handleChange = e => {
    var x = e.target.tagName;
    
    this.setState({ [e.target.name]: e.target.value }, () => {
      if (x === "BUTTON") {
        this.props.loadingActive();
        setTimeout(() => {
          this.props.stepIncrement();
          this.props.loadingDeActive();
        }, 1000);
      }
    });
  };

  formRendering = () => {
    switch (this.props.child_type) {
      case "step1":
        return (
          <div className="chat-conversion ">
            <div className="chatQuestion">
              <p>Name</p>
              <input
                type="text"
                name="name"
                ref={this.demo}
                value={this.state.name}
                onChange={this.handleChange}
              />
            </div>
            <div className="chatQuestion">
              <p>Email</p>
              <input
                type="text"
                name="email"
                value={this.state.email}
                onChange={this.handleChange}
              />
            </div>
            <div className="chatQuestion">
              <p>DOB</p>
              <input
                type="text"
                name="dateofBirth"
                value={this.state.dateofBirth}
                onChange={this.handleChange}
              />
            </div>
            <div className="chatQuestion">
              <button onClick={this.handleChange}>Next</button>
            </div>
          </div>
        );
        break;
      case "step2":
        return (
          <div className="chat-conversion align-none">
            <div className="chatQuestion">
              <p>Weight</p>
              <input
                type="text"
                name="weight"
                value={this.state.weight}
                onChange={this.handleChange}
              />
            </div>
            <div className="chatQuestion">
              <p>Height</p>
              <input
                type="text"
                name="height"
                value={this.state.height}
                onChange={this.handleChange}
              />
            </div>
            <div className="chatQuestion">
              <button onClick={this.handleChange}>Next</button>
            </div>
          </div>
        );
        break;
      case "step3":
        return (
          <div className="chat-conversion">
            <div className="chatQuestion">
              <p>Gender</p>
            </div>
            <div className="chatButton">
              <ul>
                <li>
                  <button
                    type="button"
                    className={
                      this.state.gender === "male" ? "dummy active" : "dummy"
                    }
                    name="gender"
                    value="male"
                    id="gender"
                    onClick={this.handleChange}
                    /* onClick={this.props.stepIncrement} */
                  >
                    Male
                  </button>
                </li>
                <li>
                  <button
                    type="button"
                    className={
                      this.state.gender === "female" ? "dummy active" : "dummy"
                    }
                    name="gender"
                    value="female"
                    id="gender"
                    onClick={this.handleChange}
                    /* onClick={this.props.stepIncrement} */
                  >
                    Female
                  </button>
                </li>
              </ul>
            </div>
          </div>
        );
        break;
      case "step4":
        return (
          <div className="chat-conversion">
            <div className="chatQuestion">
              <p>Blood Pressure</p>
            </div>
            <div className="chatButton">
              <ul>
                <li>
                  <button
                    type="button"
                    className={
                      this.state.bloodPressure === "yes"
                        ? "dummy active"
                        : "dummy"
                    }
                    name="bloodPressure"
                    value="yes"
                    onClick={this.handleChange}
                  >
                    Yes
                  </button>
                </li>
                <li>
                  <button
                    type="button"
                    className={
                      this.state.bloodPressure === "no"
                        ? "dummy active"
                        : "dummy"
                    }
                    name="bloodPressure"
                    value="no"
                    onClick={this.handleChange}
                  >
                    No
                  </button>
                </li>
              </ul>
            </div>
          </div>
        );
        break;
      case "step5":
        return (
          <div className="chat-conversion">
            <div className="chatQuestion">
              <p>High sugar</p>
            </div>
            <div className="chatButton">
              <ul>
                <li>
                  <button
                    type="button"
                    className={
                      this.state.sugar === "yes" ? "dummy active" : "dummy"
                    }
                    name="sugar"
                    value="yes"
                    onClick={this.handleChange}
                  >
                    Yes
                  </button>
                </li>
                <li>
                  <button
                    type="button"
                    className={
                      this.state.sugar === "no" ? "dummy active" : "dummy"
                    }
                    name="sugar"
                    value="no"
                    onClick={this.handleChange}
                  >
                    No
                  </button>
                </li>
              </ul>
            </div>
          </div>
        );
        break;
    }
  };
  render() {
    return <React.Fragment>{this.formRendering()}</React.Fragment>;
  }
}
export default ChatBotForm;
