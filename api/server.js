const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const PORT = 4000;
const cors = require('cors');
//const mysql = require('mysql')
const mysql = require('mysql2/promise');
var http = require("http");
const routes = require('./routes');
var siofu = require("socketio-file-upload")
var Client = require('node-rest-client').Client;
var client = new Client();
const { chatSave,chatHistory } = require('./controller/chat/chat.api');
// const  { sendSMS } = require('../api/controller/sms/sms.api.js')
    const accountSid = "AC384635163";
    const authToken = "2893635153";
    const clientId = require("twilio")(accountSid, authToken);
var cron = require("node-schedule");

// var rule = new cron.RecurrenceRule();
// rule.second = 00;
// cron.scheduleJob(rule, ()=> {


//     clientId.messages
//       .create({
//         body:
//           "This is the ship that made the Kessel Run in fourteen parsecs?",
//         from: "+15017122661",
//         to: "+919003718687"


//       })
//       .then(message => console.log(message.sid));
//   console.log(new Date(), "The 30th second of the minute.");
// });

// const connection = mysql.createConnection({
//     connectionLimit : 100,
//     host:'192.168.2.64',
//     port:3306,
//     user:'admin',
//     password:'Colan@123',
//     database:'tomato_medical'  
// })

// connection.connect(function(err) {
//   if (!err) {
//     console.log("Database is connected ... nn");
//   } else {
//     console.log("Error connecting database ... nn",err);
//   }
// });

// global.connection = connection;

const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'tomato_medical',
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
});

global.pool = pool;

//socket io file upload
app.use(siofu.router)

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//To prevent errors from Cross Origin Resource Sharing, we will set 
//our headers to allow CORS with middleware like so:
// app.use(function (req, res, next) {
//     res.setHeader('Access-Control-Allow-Origin', '*');
//     res.setHeader('Access-Control-Allow-Credentials', 'true');
//     res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
//     res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
//     //and remove cacheing so we get the most recent appointments
//     res.setHeader('Cache-Control', 'no-cache');
//     next();
// });

routes(app)

// New code - START
// const https = require('https');
// // const httpsSocket = https.Server(app);
const fs = require('fs')
// const httpsOptions = {
//     key: fs.readFileSync('cert.key'),
//     cert: fs.readFileSync('cert.pem')
// }
var server = http.createServer({
    key: fs.readFileSync('cert.key'),
    cert: fs.readFileSync('cert.pem'),
    requestCert: false,
    rejectUnauthorized: false
}, app);

// New code - END

// app.listen(PORT, function(){
//   console.log('Server is running on Port:',PORT);
// });


server.listen(PORT, () => {
    console.log('server running at ' + PORT)
});

var io = require('socket.io').listen(server);

// signaling
io.on('connection', function (socket) {

    console.log('a user connected');
    console.log('client connected...', socket.id)

    var uploader = new siofu();
    uploader.dir = "D:\\Socket-File-Uploads\\";
    uploader.listen(socket);

    //Uploader Start
    uploader.on('start', function (event) {
        console.log('Uploader start : ');
    });

    // Do something when a file is saved:
    uploader.on("saved", function (event) {
        console.log("In file saved method : file name : ", event.file.name);
    });

    // Error handler:
    uploader.on("error", function (event) {
        console.log("Error from uploader", event);
    });

    //Whenever someone disconnects this piece of code executed
    socket.on('disconnect', function () {
        console.log('A user disconnected');
        console.log('client disconnect...', socket.id);
    });

    socket.on('chat-message', function (event) {
        console.log("in chat meesage : server : msg : ", event.msg, "file name : ", event.filename, "room number : ", event.room, "type : ", event.type, "name : ", event.name)
        //socket.emit('chat-message', msg,room,type,name);
        var message="";
        if(!event.isfile){
            message=event.msg
        }
        const reqJSON = {
            "patientId": event.patientId,
            "doctorId": event.doctorId,
            "message": message,
            "meetingRoom": event.room,
            "isfile": event.isfile,
            "sentBy": event.type
        }
        chatSave(reqJSON)
        .then(res=>{
            io.to(event.room).emit('chat-message', event);
            console.log('response database: ',res);
        }).catch(error=>{
            console.log('error in saving database : ',error);
        });
    });

    socket.on('create or join', function (room) {
        console.log('create or join to room ', room);
        var myRoom = io.sockets.adapter.rooms[room] || { length: 0 };
        var numClients = myRoom.length;
        console.log(room, ' has ', numClients, ' clients');

        if (numClients == 0) {
            socket.join(room);
            socket.emit('created', room);
        } else if (numClients == 1) {
            socket.join(room);
            socket.emit('joined', room);
        } else {
            socket.emit('full', room);
        }



    });

    socket.on('leave', function (room) {
        console.log('leave ', room);
        try {
            console.log('[socket]', 'leave room :', room);
            socket.leave(room);

        } catch (e) {
            console.log('[error]', 'leave room :', e);

        }

    });

    socket.on('rejoin', function (room) {
        console.log('rejoin to room ', room);
        socket.leave(room);

        var myRoom = io.sockets.adapter.rooms[room] || { length: 0 };
        var numClients = myRoom.length;

        console.log(room, 'rejoin has ', numClients, ' clients');

        socket.join(room);
        socket.emit('joined', room);

    });

    socket.on('stop', function (event) {
        socket.leave(event.room);
        socket.broadcast.to(event.room).emit('stop', event);
    });

    socket.on('ready', function (room) {
        socket.broadcast.to(room).emit('ready');
    });

    socket.on('candidate', function (event) {
        socket.broadcast.to(event.room).emit('candidate', event);
    });

    socket.on('offer', function (event) {
        socket.broadcast.to(event.room).emit('offer', event.sdp);
    });

    socket.on('leaveTest', function (event) {
        socket.broadcast.to(event.room).emit('leaveTest', event.sdp);
    });

    socket.on('answer', function (event) {
        socket.broadcast.to(event.room).emit('answer', event.sdp);
    });

});

