import React from "react";
import axios from "axios";
import { withRouter } from "react-router";
import URL from "../../asset/configUrl.js";
import moment from "moment";

class QuestionAnswer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      question: "",
      options: [],
      assesmentId: "",
      text: "",
      parentId: 0,
      uid: "",
      symptoms: ""
    };
  }
  componentWillReceiveProps(nextProps, nextState) {
    if (nextProps.selectedSymptoms != this.props.selectedSymptoms) {
      this.questions(nextProps);
    }
  }
  handleuserassesment = async payload => {

    const config = {
      method: "post",
      url: URL.ASSESSMENT_CHAT,
      data: payload,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    }
    // await axios.post(URL.ASSESSMENT_CHAT, payload)
    await axios(config)
      .then(response => {
        console.log("res-->", response.data);
      });
  };

  handleOpenReport = () => {
    if (sessionStorage.getItem("loginDetails")) {
      this.props.history.push("/symptoms/assesment");
    } else {
      this.props.history.push("/login/patient", "chatBot");
    }
  };

  questions = nextProps => {
    console.log("nexporps", nextProps)
    const user = JSON.parse(sessionStorage.getItem("loginDetails"));
    const config = {
      method: "post",
      url: URL.CHAT,
      data: {
        uid: user == null ? 0 : user.userInfo.userId,
        chatInputQ: nextProps.selectedSymptoms,
        assesmentId:
          nextProps.assessmentId != ""
            ? nextProps.assessmentId
            : this.state.assesmentId,
        parentId:
          nextProps.parentId > 0 ? nextProps.parentId : this.state.parentId
      },
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };
    axios(config)
      // .post(URL.CHAT, {
      //   uid: user == null ? 0 : user.userInfo.userId,
      //   chatInputQ: nextProps.selectedSymptoms,
      //   assesmentId:
      //     nextProps.assessmentId != ""
      //       ? nextProps.assessmentId
      //       : this.state.assesmentId,
      //   parentId:
      //     nextProps.parentId > 0 ? nextProps.parentId : this.state.parentId
      // })
      .then(response => {
        if (response.data.status == "continue") {
          const { options } = JSON.parse(response.data.options);
          const {
            question = null,
            status = null,
            parentId = null
          } = response.data;
          localStorage.setItem("assesmentId", response.data.assesmentId);
          this.setState(
            {
              uid:
                response.data.uid === undefined
                  ? this.state.uid
                  : response.data.uid
            },
            () => {
              const payload = {
                uid: user == null ? this.state.uid : user.userInfo.userId,
                assesmentId: response.data.assesmentId,
                question: [
                  {
                    question: this.props.question,
                    selectedOption: this.props.selectedOption,
                    possibleCauses: response.data.possibleCauses
                  }
                ],
                status: status,
                symptoms: nextProps.symptoms
                  ? nextProps.symptoms
                  : sessionStorage.getItem("symptoms"),
                dateTime: moment(new Date(Date.now())).format(
                  "YYYY-MM-DD HH:mm"
                ),
                parentId:
                  nextProps.parentId > 0
                    ? nextProps.parentId
                    : this.state.parentId
                // data.summary
              };
              this.handleuserassesment(payload);
              this.setState({
                question: question,
                options: options,
                status: status,
                assesmentId: response.data.assesmentId,
                parentId: parentId
              });
            }
          );
        } else {
          this.setState({ status: response.data.status });
        }
      })
      .catch(error => { });
  };

  componentDidMount = () => {
    console.log("sdf0000000000", this.props)
    this.questions(this.props);
  };
  render() {
    const { handleChatbot } = this.props;
    console.log("this.state.wuestionanswer ", this.state)
    return (
      <div>
        {this.state.status == "continue" && (
          <div className="chat-conversion">
            {/* <input type="hidden" id="text" value={this.state.text} /> */}
            <div className="chatQuestion">{this.state.question}</div>
            <div className="chatButton">
              <ul>
                {/* <li>
                  <button>Less than one day</button>
                </li> */}
                {this.state.options.map(each => (
                  <li>
                    <button
                      className={
                        this.state.text == each + this.state.question
                          ? "dummy active"
                          : "dummy"
                      }
                      id={each}
                      onClick={e => {
                        this.setState({ text: each + this.state.question });
                        this.props.loadTrue();
                        setTimeout(() => {
                          this.props.loadFalse();
                          this.props.changeSelectedSymptom(
                            this.state.question,
                            each
                          );
                        }, 1000);
                      }}
                      type="button"
                    >
                      {each}
                    </button>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        )}
        {this.state.status == "end" && (
          <div>
            <div className="chatQuestion">Thanks for your Assessment</div>
            <p style={{ textAlign: "center" }}>
              Please find your Final Reports
            </p>
            <div style={{ textAlign: "center" }}>
              <button
                className="open_Report_btn"
                type="button"
                /* onClick={() => this.props.history.push("/symptoms/assesment")} */
                onClick={() => this.handleOpenReport()}
              >
                open Report
              </button>
            </div>
          </div>
        )}
      </div>
    );
  }
}
export default withRouter(QuestionAnswer);
