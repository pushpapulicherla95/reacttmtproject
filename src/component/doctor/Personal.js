import React, { Component } from "react";
import { getUserType } from "../../service/login/action";
import { connect } from "react-redux";
import { getDoctorProfile, saveDoctorProfile, getCountry, getState, getCity, getArea, getPostalcode } from "../../service/doctor/action";
import { getClinic } from "../../service/medicalSpecialist/action";
import { onlyNumbers } from "../patient/personal/KeyValidation";
import { uploadProfilepic } from "../../service/hospital/action";
import AWS from "aws-sdk";

class Personal extends Component {
    constructor(props) {
        super(props)
        var loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
        const { userInfo } = loginDetails;
        var userId = userInfo.userId || "";
        this.state = {
            qualification: "",
            specialist: "",
            phone: "",
            experience: "",
            email: userInfo.email || "",
            address: "",
            gender: "",
            description: "",
            firstName: userInfo.firstName || "",
            lastName: userInfo.lastName || "",
            clinicName: "",
            clinicPhone: "",
            clinicAdd: "",
            clinicEmail: "",
            specialistList: [],
            userId: userId,
            selectedAreaOption: "",
            citys: "",
            area: "",
            country: "",
            state: "",
            pincode: "",
            states: "",
            areas: "",
            stateList: [],
            selectedState: "",
            lists: "",
            city: "",
            selectedStateOption: "",
            selectedCityOption: "",
            selectedOption: "",
            errors: {
                qualification: "",
                specialist: "",
                phone: "",
                experience: "",
                email: "",
                address: "",
                gender: "",
                description: "",
                firstName: "",
                lastName: "",
                clinicName: "",
                clinicPhone: "",
                clinicAdd: "",
                clinicEmail: "",
                citys: "",
                area: "",
                country: "",
                state: "",
                pincode: "",
            },
            qualificationValid: false,
            specialistValid: false,
            emailValid: false,
            phoneValid: false,
            addressValid: false,
            experienceValid: false,
            genderValid: false,
            descriptionValid: false,
            firstNameValid: false,
            lastNameValid: false,
            clinicNameValid: false,
            clinicPhoneValid: false,
            clinicAddValid: false,
            clinicEmailValid: false,
            cityValid: false,
            areaValid: false,
            countryValid: false,
            stateValid: false,
            pincodeValid: false,
            formValid: false,
            profilePic: userInfo.profilePic,
            fileType: "",
            fileUrl: ""
        }
    }
    //validation
    validateField(fieldName, value) {
        const { errors,
            qualificationValid,
            specialistValid,
            emailValid,
            phoneValid,
            addressValid,
            experienceValid,
            genderValid,
            descriptionValid,
            firstNameValid,
            lastNameValid,
            clinicNameValid,
            clinicPhoneValid,
            clinicAddValid,
            clinicEmailValid,
            cityValid,
            areaValid,
            countryValid,
            stateValid,
            pincodeValid, } = this.state;
        let emailvalid = emailValid;
        let firstNamevalid = firstNameValid;
        let lastNamevalid = lastNameValid;
        let phonevalid = phoneValid;
        let addressvalid = addressValid;
        let experiencevalid = experienceValid;
        let gendervalid = genderValid;
        let descriptionvalid = descriptionValid;
        let specialistvalid = specialistValid;
        let clinicNamevalid = clinicNameValid;
        let clinicPhonevalid = clinicPhoneValid;
        let clinicAddvalid = clinicAddValid;
        let clinicEmailvalid = clinicEmailValid;
        let cityvalid = cityValid;
        let areavalid = areaValid;
        let countryvalid = countryValid;
        let statevalid = stateValid;
        let pincodevalid = pincodeValid;
        let qualificationvalid = qualificationValid;

        let fieldValidationErrors = errors;
        switch (fieldName) {
            case "email":
                emailvalid = value.match(
                    /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i
                );
                fieldValidationErrors.email = emailvalid
                    ? ""
                    : "Please enter valid email address.";
                break;
            case "firstName":
                firstNamevalid = !(value.length < 3 || value.length > 20);
                fieldValidationErrors.firstName = firstNamevalid
                    ? ""
                    : "First Name should be 3 to 20 characters";
                break;
            case "lastName":
                lastNamevalid = !(value.length < 3 || value.length > 20);
                fieldValidationErrors.lastName = lastNamevalid
                    ? ""
                    : "Last Name should be 3 to 20 characters";
                break;
            case "qualification":
                qualificationvalid = !(value.length < 3 || value.length > 300);
                fieldValidationErrors.qualification = qualificationvalid
                    ? ""
                    : "Qualification  should be 3 to 20 characters";
                break;
            case "specialist":
                specialistvalid = !(value.length < 1);
                fieldValidationErrors.specialist = specialistvalid
                    ? ""
                    : "Please select the Specialist";
                break;
            case "phone":
                phonevalid = !(value.length < 1);
                fieldValidationErrors.phone = phonevalid
                    ? ""
                    : "Please enter phone number";
                break;
            case "experience":
                experiencevalid = !(value.length < 3 || value.length > 300);
                fieldValidationErrors.experience = experiencevalid
                    ? ""
                    : "Please enter your Exprience";
                break;

            case "address":
                addressvalid = !(value.length < 3 || value.length > 300);
                fieldValidationErrors.address = addressvalid
                    ? ""
                    : "Please enter your Address";
                break;
            case "gender":
                gendervalid = !(value.length < 1);
                fieldValidationErrors.gender = gendervalid
                    ? ""
                    : "Please select Gender";
                break;
            case "description":
                descriptionvalid = !(value.length < 3 || value.length > 500);
                fieldValidationErrors.description = descriptionvalid
                    ? ""
                    : "Please enter your Description ";
                break;
            case "clinicName":
                clinicNamevalid = !(value.length < 3 || value.length > 50);;
                fieldValidationErrors.clinicName = clinicNamevalid
                    ? ""
                    : "Please enter your Clinic Name ";
                break;
            case "clinicPhone":
                clinicPhonevalid = !(value.length < 1);
                fieldValidationErrors.clinicPhone = clinicPhonevalid
                    ? ""
                    : "Please enter your Clinic Phone";
                break;
            case "clinicAddvalid":
                clinicAddvalid = !(value.length < 3 || value.length > 300);
                fieldValidationErrors.clinicAdd = clinicAddvalid
                    ? ""
                    : "Please enter your clinic Address";
                break;
            case "clinicEmail":
                clinicEmailvalid = value.match(
                    /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i
                );
                fieldValidationErrors.clinicEmail = clinicEmailvalid
                    ? ""
                    : "Please enter valid email address.";
                break;
            case "citys":
                cityValid = !(value.length < 3 || value.length > 50);
                fieldValidationErrors.citys = cityValid
                    ? ""
                    : "Please enter your City";
                break;
            case "area":
                areavalid = !(value.length < 1);
                fieldValidationErrors.area = areavalid
                    ? ""
                    : "Please enter your Area ";
                break;
            case "country":
                countryvalid = !(value.length < 1);
                fieldValidationErrors.country = countryvalid
                    ? ""
                    : "Please enter your Country ";
                break;
            case "state":
                statevalid = !(value.length < 1);
                fieldValidationErrors.state = statevalid
                    ? ""
                    : "Please enter your State";
                break;
            case "pincode":
                pincodevalid = !(value.length < 1);
                fieldValidationErrors.pincode = pincodevalid
                    ? ""
                    : "Please enter your Pincode";
                break;
        }

        this.setState({
            errors: fieldValidationErrors,
            emailValid: emailvalid,
            firstNameValid: firstNamevalid,
            lastNameValid: lastNamevalid,
            phoneValid: phonevalid,
            addressValid: addressvalid,
            experienceValid: experiencevalid,
            genderValid: gendervalid,
            descriptionValid: descriptionvalid,
            specialistValid: specialistvalid,
            clinicNameValid: clinicNamevalid,
            clinicPhoneValid: clinicPhonevalid,
            clinicAddValid: clinicAddvalid,
            clinicEmailValid: clinicEmailvalid,
            cityValid: cityValid,
            areaValid: areavalid,
            countryValid: countryvalid,
            stateValid: statevalid,
            pincodeValid: pincodevalid,
            qualificationValid: qualificationValid,
        },
            this.validateForm
        )
    }
    validateForm() {
        const {
            qualificationValid,
            specialistValid,
            emailValid,
            phoneValid,
            addressValid,
            experienceValid,
            genderValid,
            descriptionValid,
            firstNameValid,
            lastNameValid,
            clinicNameValid,
            clinicPhoneValid,
            clinicAddValid,
            clinicEmailValid,
            cityValid,
            areaValid,
            countryValid,
            stateValid,
            pincodeValid,
        } = this.state;
        this.setState({
            formValid:
                qualificationValid &&
                specialistValid &&
                emailValid &&
                phoneValid &&
                experienceValid &&
                genderValid &&
                firstNameValid &&
                lastNameValid &&
                cityValid &&
                areaValid &&
                countryValid &&
                stateValid &&
                pincodeValid





        }, () => {
            let {

                qualificationValid,
                specialistValid,
                emailValid,
                phoneValid,
                experienceValid,
                genderValid,
                firstNameValid,
                lastNameValid,
                cityValid,
                areaValid,
                countryValid,
                stateValid,
                pincodeValid,
            } = this.state;


            let validatingData = {
                qualificationValid,
                specialistValid,
                emailValid,
                phoneValid,
                experienceValid,
                genderValid,
                firstNameValid,
                lastNameValid,
                cityValid,
                areaValid,
                countryValid,
                stateValid,
                pincodeValid,
            }

        });
    }
    componentWillMount() {
        this.props.getUserType();
        const userdetails = {
            "user_id": this.state.userId
        }

        this.props.getCountry();
        this.props.getDoctorProfile(userdetails);
        this.props.getClinic();
        this.props.removeState();
        // this.props.removeCity();

        //getprofile pic
        const { profilePic } = this.state;
        AWS.config.update({
            accessKeyId: process.env.REACT_APP_ACCESSKEY,
            secretAccessKey: process.env.REACT_APP_SECRETACCESSKEY,
            region: process.env.REACT_APP_REGION
        });
        var s3 = new AWS.S3();
        var data = {
            Bucket: "tomato-medical-bucket",
            Key: `profilepicture/${profilePic}`
        };
        s3.getSignedUrl("getObject", data, async (err, data) => {
            if (err) {
                console.log("Error uploading data: ", err);
            } else {
                console.log("succesfully uploaded the image!", data);

                (await data) && this.setState({ fileUrl: data });
            }
        });

    }
    handleOption = (e) => {
        const { selectedOption } = this.state
        this.setState({ selectedOption: e.target.value })
        this.setState({ selectedOption: selectedOption })
    }

    componentWillReceiveProps(nextProps) {
        // console.log(".................",this.props.listareaDetails)

        // if (nextProps.liststateDetails && nextProps.liststateDetails.states.length > 0) {
        //     this.setState({ states: nextProps.liststateDetails.states })

        // }

        if (nextProps.doctorProfile && (nextProps.doctorProfile != this.props.doctorProfile) && nextProps.doctorProfile.docInformation != null) {
            const {
                address, clinicEmail, clinicAdd, clinicName, clinicPhone, description, email, experience, firstName, gender, lastName
                , phone, qualification, specialistId, userInfoId, area, pincode, country, city, state
            } = nextProps.doctorProfile.docInformation;

            this.setState({
                address, clinicEmail, clinicAdd, clinicName, clinicPhone, description, email, experience, firstName, gender, lastName
                , phone, qualification, specialistId, userInfoId, selectedAreaOption: area, pincode: pincode, selectedOption: country, selectedCityOption: city, selectedStateOption: state, specialist: specialistId,
            }, () => {
                if (this.state.specialistId === "") {
                    this.setState({
                        firstNameValid: true,
                        emailValid: true,
                        lastNameValid: true,
                    })
                } else {
                    this.validateupdate();
                }

                this.props.getArea({ city })
                this.props.getState({ country })
                this.props.getCity({ state })
                this.props.getPostalcode({ area })


            },
            )
        }
        if (nextProps.clinicDetails && nextProps.clinicDetails.therapists.length > 0 && nextProps.clinicDetails.therapists.length != null) {
            this.setState({ specialistList: nextProps.clinicDetails.therapists })

        }

    }
    handleChange = (e) => {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value }, () => {
            this.validateField(name, value);
        });
    };

    validateupdate = () => {
        this.setState({
            formValid: true,
            qualificationValid: true,
            specialistValid: true,
            phoneValid: true,
            addressValid: true,
            experienceValid: true,
            genderValid: true,
            descriptionValid: true,
            firstNameValid: true,
            emailValid: true,
            lastNameValid: true,
            clinicNameValid: true,
            clinicPhoneValid: true,
            clinicAddValid: true,
            clinicEmailValid: true,
            cityValid: true,
            areaValid: true,
            countryValid: true,
            stateValid: true,
            pincodeValid: true,

        })

    }

    selectCountry = (e) => {
        this.setState({ selectedOption: e.target.value }, () => {
            let payload = {
                country: this.state.selectedOption
            }
            this.props.getState(payload)
        })


    }

    selectState = (e) => {
        this.setState({ selectedStateOption: e.target.value }, () => {
            let payload = {
                state: this.state.selectedStateOption
            }
            this.props.getCity(payload)
        })


    }
    selectCity = (e) => {
        this.setState({ selectedCityOption: e.target.value }, () => {
            let payload = {
                city: this.state.selectedCityOption
            }
            this.props.getArea(payload)
        })


    }
    selectArea = (e) => {
        this.setState({ selectedAreaOption: e.target.value }, () => {
            let payload = {
                area: this.state.selectedAreaOption
            }
            this.props.getPostalcode(payload)
        })


    }

    handleSubmit = (e) => {
        var loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
        const {
            qualification,
            specialist,
            phone,
            experience,
            email,
            address,
            gender,
            description,
            firstName,
            lastName,
            clinicName,
            clinicPhone,
            clinicAdd,
            clinicEmail,
            selectedStateOption,
            selectedCityOption,
            citys, areas, country, state, pincode, selectedOption, selectedAreaOption
        } = this.state;
        const saveInfo = {
            userInfoId: this.state.userId,
            specialist: specialist,
            phone: phone,
            qualification: qualification,
            email: email,
            address: address,
            gender: gender,
            description: description,
            firstName: firstName,
            lastName: lastName,
            clinicName: clinicName,
            clinicPhone: clinicPhone,
            clinicAdd: clinicAdd,
            clinicEmail: clinicEmail,
            experience: experience,
            city: selectedCityOption,
            area: selectedAreaOption,
            country: selectedOption,
            pincode: pincode,
            state: selectedStateOption
        }
        this.props.saveDoctorProfile(saveInfo);
    }
    handleChangePopup = e => {
        var reader = new FileReader();
        const name = e.target.name;

        var value = e.target.files[0];
        reader.onload = () => {
            this.setState({
                fileUrl: reader.result,
                fileName: value.name,
                fileType: value.type
            });
        };
        reader.readAsDataURL(value);
    }
    submitUpload = (e) => {
        e.preventDefault();
        const { fileType, fileUrl, userId } = this.state;
        var phrase = this.state.fileUrl;
        var myRegexp = /base64,(.*)/;
        var match = myRegexp.exec(phrase);
        const filedata = {
            uid: userId,
            fileType: fileType,
            fileUrl: match[1],
        }
        this.props.uploadProfilepic(filedata);
    }
    render() {
        const lists = this.props.listcountryDetails && this.props.listcountryDetails.lists;
        const city = this.props.listcityDetails && this.props.listcityDetails.city;
        const states = this.props.liststateDetails && this.props.liststateDetails.states;
        const area = this.props.listareaDetails && this.props.listareaDetails.area;
        const postalcode = this.props.listpostalcodeDetails && this.props.listpostalcodeDetails.postalcode;

        const {
            errors,
            formValid,
            selectedState,
            stateList,
            qualification,
            specialist,
            phone,
            experience,
            email,
            address,
            gender,
            areas,
            description,
            firstName,
            lastName,
            clinicName,
            clinicPhone,
            clinicAdd,
            clinicEmail, citys, country, state, pincode, selectedOption, selectedStateOption,
            specialistList, fileUrl } = this.state;
        return (
            <div className="mainMenuSide" id="main-content">
                <div className="wrapper">
                    <div className="container-fluid">
                        <div className="body-content">
                            <div className="tomCard">
                                <div className="tomCardHead">
                                    <h5 className="float-left">My Profile</h5>
                                </div>
                                <div className="tomCardBody">
                                    <div className="innerCard">
                                        <div className="innerCardHead">
                                            <h5>Basic Information</h5>
                                        </div>
                                        <div className="innerCardBody">
                                            <div className="row">
                                                {/* <div className="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12">
                                                    <div className="imagePreview">
                                                        <label className="uploadBtn"><svg className="svg-inline--fa fa-upload fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="upload" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M296 384h-80c-13.3 0-24-10.7-24-24V192h-87.7c-17.8 0-26.7-21.5-14.1-34.1L242.3 5.7c7.5-7.5 19.8-7.5 27.3 0l152.2 152.2c12.6 12.6 3.7 34.1-14.1 34.1H320v168c0 13.3-10.7 24-24 24zm216-8v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h136v8c0 30.9 25.1 56 56 56h80c30.9 0 56-25.1 56-56v-8h136c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path></svg>
                                                            <input type="file" className="input_h_35 boxnone" name="fileUrl" onChange={this.handleChangePopup} /></label>
                                                        <img src={fileUrl ? fileUrl : "../images/profile.png"} alt="your image" />
                                                    </div>
                                                    <button type="button" className="commonBtn text-center upload" onClick={this.submitUpload}>Upload Image</button>
                                                </div> */}
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div className="row">
                                                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                                                            <div className="form-group">
                                                                <label>First Name <sup>*</sup></label>
                                                                <input type="text" className="form-control" name="firstName" defaultValue={firstName} readOnly="readonly" />
                                                                <div style={{ color: "red" }}>{errors.firstName}</div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                                                            <div className="form-group">
                                                                <label>Last Name <sup>*</sup></label>
                                                                <input type="text" className="form-control" name="lastName" defaultValue={lastName} readOnly="readonly" />
                                                                <div style={{ color: "red" }}>{errors.lastName}</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                            <div className="form-group">
                                                                <label>
                                                                    Gender<sup>*</sup>
                                                                </label>
                                                                <select
                                                                    className="form-control"
                                                                    name="gender"
                                                                    value={this.state.gender}
                                                                    onChange={this.handleChange}
                                                                >
                                                                    <option value="">--Select Gender--</option>
                                                                    <option value="Male">Male</option>
                                                                    <option value="Female">Female</option>
                                                                </select>
                                                                <div style={{ color: "red" }}>{errors.gender}</div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                            <div className="form-group">
                                                                <label>Email Id <sup>*</sup></label>
                                                                <input type="text" className="form-control" name="email" defaultValue={email} readOnly="readonly" />
                                                                <div style={{ color: "red" }}>{errors.email}</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                            <div className="form-group">
                                                                <label>Mobile No <sup>*</sup></label>
                                                                <input type="text" className="form-control" name="phone" value={phone} onChange={this.handleChange} onKeyPress={onlyNumbers} maxlength="15" />
                                                                <div style={{ color: "red" }}>{errors.phone}</div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                            <div className="form-group">
                                                                <label className="form-label">Country<sup>*</sup></label>
                                                                <select className="form-control" name="country" value={this.state.selectedOption} onChange={this.selectCountry}>
                                                                    <option value="" disabled>---Select Country ----</option>
                                                                    {
                                                                        lists && lists.map((each, i) => (

                                                                            <option key={i} value={each.code}>  {each.country}</option>))
                                                                    }
                                                                </select>
                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                            <div className="form-group">
                                                                <label className="form-label">State<sup>*</sup></label>
                                                                <select className="form-control" name="state" value={this.state.selectedStateOption} onChange={this.selectState}>

                                                                    <option value="" disabled>---Select State ----</option>
                                                                    {
                                                                        states && states.map((each, i) => (
                                                                            <option value={each} key={i}> {each}</option>))
                                                                    }
                                                                </select>
                                                            </div>


                                                        </div>
                                                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">

                                                            <div className="form-group">
                                                                <label className="form-label">City<sup>*</sup></label>
                                                                <select className="form-control" name="city" value={this.state.selectedCityOption} onChange={this.selectCity}>
                                                                    <option value="" disabled>---Select City ----</option>
                                                                    {
                                                                        city && city.map((each, i) => (

                                                                            <option key={i} value={each}>  {each}</option>))
                                                                    }
                                                                </select>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                            <div className="form-group">
                                                                <label className="form-label">Area<sup>*</sup></label>
                                                                <select className="form-control" name="areas" value={this.state.selectedAreaOption} onChange={this.selectArea}>
                                                                    <option value="" disabled>---Select Area ----</option>
                                                                    {
                                                                        area && area.map((each, i) => (

                                                                            <option key={i} value={each}> {each}</option>))
                                                                    }
                                                                </select>
                                                            </div>

                                                        </div>
                                                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">

                                                            <div className="form-group">
                                                                <label className="form-label">Pincode<sup>*</sup></label>
                                                                <select className="form-control" name="pincode" value={pincode} onChange={this.handleChange}>
                                                                    <option value="" disabled>---Select Pincode ----</option>
                                                                    {
                                                                        postalcode && postalcode.map((each, i) => (

                                                                            <option key={i} value={each}> {each}</option>))
                                                                    }
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="innerCard">
                                        <div className="row">
                                            <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                <div className="innerCardHead">
                                                    <h5>About Me</h5>
                                                </div>
                                                <div className="innerCardBody">
                                                    <div className="row">
                                                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div className="form-group">
                                                                <textarea className="form-control" name="description" value={description} onChange={this.handleChange}></textarea>
                                                                <div style={{ color: "red" }}>{errors.description}</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                <div className="innerCardHead">
                                                    <h5>Education</h5>
                                                </div>
                                                <div className="innerCardBody">
                                                    <div className="row">
                                                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div className="form-group">
                                                                <textarea className="form-control" name="qualification" value={qualification} onChange={this.handleChange}></textarea>

                                                                <div style={{ color: "red" }}>{errors.qualification}</div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="innerCard">
                                        <div className="row">
                                            <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                <div className="innerCardHead">
                                                    <h5>Experience <sup>*</sup></h5>
                                                </div>
                                                <div className="innerCardBody">
                                                    <div className="row">
                                                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div className="form-group">
                                                                <textarea className="form-control" name="experience" value={experience} onChange={this.handleChange}></textarea>
                                                                <div style={{ color: "red" }}>{errors.experience}</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                <div className="innerCardHead">
                                                    <h5>Address</h5>
                                                </div>
                                                <div className="innerCardBody">
                                                    <div className="row">
                                                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div className="form-group">
                                                                <textarea className="form-control" name="address" value={address} onChange={this.handleChange}></textarea>
                                                                <div style={{ color: "red" }}>{errors.address}</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="innerCard">
                                        <div className="innerCardHead">
                                            <h5>Specializations <sup>*</sup></h5>
                                        </div>
                                        <div className="innerCardBody">
                                            <div className="row">
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                    <div className="form-group">
                                                        <label>Specializations</label>
                                                        {/* <select type="text" className="form-control">
                                                            <option>Select Specializations</option>
                                                        </select> */}
                                                        <select className="form-control" name='specialist' value={specialist} onChange={this.handleChange}>
                                                            {specialistList.length > 0 && specialistList.map((link, i) =>
                                                                <option key={i} value={link.id} >{link.name}</option>
                                                            )
                                                            }
                                                        </select>
                                                        <div style={{ color: "red" }}>{errors.specialist}</div>
                                                    </div>
                                                </div>
                                                {/* <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                    <div className="form-group">
                                                        <label>Services</label>
                                                        <select type="text" className="form-control">
                                                            <option>Select Services</option>
                                                        </select>
                                                    </div>
                                                </div> */}
                                            </div>

                                        </div>
                                    </div>
                                    <div className="innerCard">
                                        <div className="innerCardHead">
                                            <h5> Clinic Or Hospital Info</h5>
                                        </div>
                                        <div className="innerCardBody">
                                            <div className="row">
                                                <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                    <div className="form-group">
                                                        <label>Name</label>
                                                        <input type="text" className="form-control" name="clinicName" value={clinicName} onChange={this.handleChange} />
                                                        <div style={{ color: "red" }}>{errors.clinicName}</div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                    <div className="form-group">
                                                        <label>Phone No</label>
                                                        <input type="text" className="form-control" name="clinicPhone" value={clinicPhone} onChange={this.handleChange} onKeyPress={onlyNumbers} maxlength="15" />
                                                        <div style={{ color: "red" }}>{errors.clinicPhone}</div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                    <div className="form-group">
                                                        <label>Email</label>
                                                        <input type="text" className="form-control" name="clinicEmail" value={clinicEmail} onChange={this.handleChange} />
                                                        <div style={{ color: "red" }}>{errors.clinicEmail}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                    <div className="form-group">
                                                        <label>Address</label>
                                                        <textarea className="form-control" name="clinicAdd" value={clinicAdd} onChange={this.handleChange}></textarea>
                                                        <div style={{ color: "red" }}>{errors.clinicAdd}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" className="commonBtn float-right" onClick={this.handleSubmit} disabled={!formValid} >Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails,
    userTypeList: state.loginReducer.userTypeList,
    doctorProfile: state.doctorReducer.doctorProfile,
    clinicDetails: state.medicalSpecialistReducer.clinicDetails,
    listcountryDetails: state.doctorReducer.listcountryDetails,
    liststateDetails: state.doctorReducer.liststateDetails,
    listcityDetails: state.doctorReducer.listcityDetails,
    listareaDetails: state.doctorReducer.listareaDetails,
    listpostalcodeDetails: state.doctorReducer.listpostalcodeDetails,
})

const mapDispatchToprops = dispatch => ({
    getUserType: () => dispatch(getUserType()),
    getDoctorProfile: userdetails => dispatch(getDoctorProfile(userdetails)),
    saveDoctorProfile: saveInfo => dispatch(saveDoctorProfile(saveInfo)),
    getClinic: () => dispatch(getClinic()),
    getCountry: () => dispatch(getCountry()),
    getState: (state) => dispatch(getState(state)),
    getArea: (area) => dispatch(getArea(area)),
    getCity: (city) => dispatch(getCity(city)),
    removeState: () => dispatch({
        type: "GET_STATE_SUCCESS",
        payload: { liststateDetails: { states: [] } }
    }),
    getPostalcode: (postalcode) => dispatch(getPostalcode(postalcode)),
    // removeCity: () => dispatch({
    //     type: "GET_CITY_SUCCESS",
    //     payload: { listcityDetails: { city: [] } }
    // })
    uploadProfilepic: (filedata) => dispatch(uploadProfilepic(filedata))

})

export default connect(
    mapStateToProps,
    mapDispatchToprops
)(Personal);
