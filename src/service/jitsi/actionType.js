const actionType = {
    JITSI_SUCCESS: "JITSI_SUCCESS",
    JITSI_FAILURE: "JITSI_FAILURE"
}

export default actionType;