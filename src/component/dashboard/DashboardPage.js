import React, { Component } from 'react';

class DashboardPage extends Component {
    render() {
        return (
            <section id="main-content" class="marginLeftRes">
                <div class="wrapper">
                    <div class="container-fluid">
                        <div class="tomCard">
                            <div class="tomCardHead">
                                <h5 class="text-center">Welcome to tomatomedical</h5>
                                <hr />                        </div>
                            <div class="tomCardBody">
                                <div class="tab-menu-content">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                            <h6>Hello User Mail Id</h6>
                                            <label style={{font_weight: "300;font-size:16px"}}>Welcome to your personal medical cloud</label>

                                            <p>Please select a category from the menu on the left to manage your medical data.</p>
                                            <br />
                                            <p>Or choose one of the following functions for exchanging your medical data in a secure way with other users: </p>
                                            <ul>
                                                <li><a href="Sharedata.html" style={{color:"red", font_weight:"300"}}>Send files to other users</a></li>
                                                <li><a href="Sharedata.html" style="color:red; font-weight:300">Receive files from other users</a></li>
                                                <li><a href="Sharedata.html" style="color:red; font-weight:300">Share your data with other users</a></li>
                                                <li><a href="Sharedata.html" style="color:red; font-weight:300">View data that others have shared with you</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                            <h6 style="text-align: center">Current cloud storage usage</h6>
                                            <div class="graph-section">

                                            </div>
                                            <p>You have currently stored 3 files with a total of 666.7 kB in your tomatomedical cloud.</p>
                                            <p>Remaining available cloud storage: 2.0 GB</p>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        )
    }

}
export default DashboardPage;