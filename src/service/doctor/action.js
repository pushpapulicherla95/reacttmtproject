import URL from '../../asset/configUrl';
import actionType from '../doctor/actionType';
import { toastr } from 'react-redux-toastr';
import { toastrOptions } from "../../component/common/toasteroptions";
import axios from "axios";

export const insertDoctorInfo = doctorinfo => dispatch => {
    const configs = {
        method: 'post',
        url: URL.INSERT_DOCTOR_INFO,
        data: doctorinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.ADD_DOCTOR_INFO_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.ADD_DOCTOR_INFO_FAILURE,
            payload: err
        })
    })
}
//Show Doctor List
export const listDoctorInfo = listinfo => dispatch => {
    const configs = {
        method: 'post',
        url: URL.LIST_DOCTOR_INFO,
        data: listinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.LIST_DOCTOR_INFO_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.LIST_DOCTOR_INFO_FAILURE,
            payload: err
        })
    })
}
//List Doctor Schedule
export const listScheduleInfo = (scheduleinfo) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.LIST_SCHEDULE_INFO,
        data: scheduleinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            localStorage.setItem('scheduleLists', JSON.stringify(res.data.scheduleLists));
            dispatch({
                type: actionType.LIST_SCHEDULE_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.LIST_SCHEDULE_FAILURE,
            payload: err
        })
    })
}
export const createDoctorSchedule = doctorSchedule => dispatch => {
    const configs = {
        method: 'post',
        url: URL.CREATE_SCHEDULE,
        data: doctorSchedule,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },

    }
    const scheduleinfo = {
        "uid": doctorSchedule.doctorId
    }
console.log("dhfjgkfdhgjk",scheduleinfo)
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(listScheduleInfo(scheduleinfo))
            dispatch({
                type: actionType.CREATE_SCHEDULE_SUCCESS,
                payload: res.data
            })
            toastr.success('SUCCESS', res.data.message, toastrOptions);
        
           }   else if (res.data.status == "failure") {
                toastr.error('Error', "Error", toastrOptions);
            }  
    }).catch(err => {
        dispatch({
            type: actionType.CREATE_SCHEDULE_FAILURE,
            payload: err
        })
    })
}
//DELETE_SCHEDULE
export const deleteSchedule = deleteinfo => dispatch => {

    const deldata = {
        scheduleId: deleteinfo.scheduleId
    }
    const configs = {
        method: 'post',
        url: URL.DELETE_SCHEDULE,
        data: deldata,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const scheduleinfo = {
        "uid": deleteinfo.userId
    }
    axios(configs).then((res) => {
        dispatch(listScheduleInfo(scheduleinfo))
        dispatch({
            type: actionType.DELETE_SCHEDULE_SUCCESS,
            payload: res.data
        })


    }).catch(err => {
        dispatch({
            type: actionType.DELETE_SCHEDULE_FAILURE,
            error: err
        })

    })

}
//Update Schedule
export const updateDoctorSchedule = updateSchedule => dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDATE_SCHEDULE,
        data: updateSchedule,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const scheduleinfo = {
        "uid": updateSchedule.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(listScheduleInfo(scheduleinfo))
            dispatch({
                type: actionType.UPDATE_SCHEDULE_SUCCESS,
                payload: res.data
            })
            toastr.success('Message', res.data.message, toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.UPDATE_SCHEDULE_FAILURE,
            payload: err
        })
        toastr.error('error', "error while updating data", toastrOptions);
    })
}
//GET DOCTOR PROFILE
export const getDoctorProfile = updateSchedule => dispatch => {
    const configs = {
        method: 'post',
        url: URL.GET_DOC_PROFILE,
        data: updateSchedule,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.GET_DOC_PROFILE_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_DOC_PROFILE_FAILURE,
            error: err
        })
    })
}
//GET COUNTRY
export const getCountry = country => dispatch => {
    const configs = {
        method: 'get',
        url: URL.GET_COUNTRY,
        data: country,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.GET_COUNTRY_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_COUNTRY_FAILURE,
            error: err
        })
    })
}
//getstateby country
export const getState = state => dispatch => {
    const configs = {
        method: 'post',
        url: URL.GET_STATE,
        data: state,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {

            dispatch({
                type: actionType.GET_STATE_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_STATE_FAILURE,
            error: err
        })
    })
}
//city
export const getCity = city => dispatch => {
    const configs = {
        method: 'post',
        url: URL.GET_CITY,
        data: city,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {

            dispatch({
                type: actionType.GET_CITY_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_CITY_FAILURE,
            error: err
        })
    })
}
//Area
export const getArea = area => dispatch => {
    const configs = {
        method: 'post',
        url: URL.GET_AREA,
        data: area,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {

            dispatch({
                type: actionType.GET_AREA_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_AREA_FAILURE,
            error: err
        })
    })
}
//getPostalcode

export const getPostalcode = postalcode => dispatch => {
    const configs = {
        method: 'post',
        url: URL.GET_POSTALCODE,
        data: postalcode,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {

            dispatch({
                type: actionType.GET_POSTALCODE_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_POSTALCODE_FAILURE,
            error: err
        })
    })
}
//SAVE DOCTOR PROFILE 
export const saveDoctorProfile = updateSchedule => dispatch => {
    const configs = {
        method: 'post',
        url: URL.SAVE_DOC_PROFILE,
        data: updateSchedule,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const getProfile = {
        "user_id": updateSchedule.userInfoId
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getDoctorProfile(getProfile))
            dispatch({
                type: actionType.SAVE_DOC_PROFILE_SUCCESS,
                payload: res.data
            })
            toastr.success('Message', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', res.data.message, toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.SAVE_DOC_PROFILE_FAILURE,
            error: err
        })
    })
}

//DOCTOR MANAGEADDS ADD
export const doctormanageAds = saveinfo => dispatch => {
    const configs = {
        method: 'post',
        url: URL.DOCTOR_MANAGE_ADS,
        data: saveinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    let getlist = {
        uid: saveinfo.healthCareProviderId
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getDocManageadList(getlist))
            dispatch({
                type: actionType.DOCTOR_MANAGE_ADD_SUCCESS,
                payload: res.data
            })
            toastr.success('Message', "Adds added successfully", toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.success('Message', res.data.message, toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.DOCTOR_MANAGE_ADD_FAILURE,
            error: err
        })
    })
}
//DOCTOR MANAGEADDS LIST
export const getDocManageadList = listinfo => dispatch => {
    const configs = {
        method: 'post',
        url: URL.DOC_MANAGEADS_LIST,
        data: listinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.DOCTOR_MANAGE_LIST_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.DOCTOR_MANAGE_LIST_FAILURE,
            error: err
        })
    })
}

//DOCTOR MANAGEADDS EDIT

export const updateDocManagead = editinfo => dispatch => {

    const configs = {
        method: 'post',
        url: URL.DOC_MANAGEADS_EDIT,
        data: editinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
    let listinfo = {
        uid: editinfo.healthCareProviderId
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getDocManageadList(listinfo))
            dispatch({
                type: actionType.DOCTOR_MANAGE_EDIT_SUCCESS,
                payload: res.data
            })
            toastr.success('Message', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', "updation failed", toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.DOCTOR_MANAGE_EDIT_FAILURE,
            error: err
        })
    })
}

//DOCTOR MANAGEADDS DELETE
export const deleteDocManagead = deleteinfo => dispatch => {
    const configs = {
        method: 'post',
        url: URL.DOC_MANAGEADS_DELETE,
        data: deleteinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
    let info = {
        uid: deleteinfo.uid
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getDocManageadList(info))
            dispatch({
                type: actionType.DOCTOR_MANAGE_DELETE_SUCCESS,
                payload: res.data
            })
            toastr.success('Message', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('error', res.data.message, toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.DOCTOR_MANAGE_DELETE_FAILURE,
            error: err
        })
    })

}

// //show Patient List
// export const listPatientInfo = listinfo => dispatch => {
//     const configs = {
//         method: 'post',
//         url: URL.LIST_PATIENT_INFO,
//         data: listinfo,
//         headers: {
//             'Content-Type': 'application/x-www-form-urlencoded'
//         },
//     }
//     axios(configs).then((res) => {
//         if (res.status === 200) {
//     console.log("iam a patient List",res)
//             dispatch({
//                 type: actionType.LIST_PATIENT_INFO_SUCCESS,
//                 payload: res.data
//             })
//         }
//     }).catch(err => {
//         dispatch({
//             type: actionType.LIST_PATIENT_INFO_FAILURE,
//             payload: err
//         })
//     })
// }
