import React, { Component } from "react";
import { getDoctorList, approveDoctor } from "../../../service/admin/Patient/action";
import { connect } from "react-redux";
import { filter as _filter } from 'lodash';

class MedicalSpecialist extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mainAdminDocList: [],
            adminDoctorList: [],
            nameSearch: "",
            dateSearch: ""
        }
    }
    componentWillMount() {
        this.props.getDoctorList();
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.adminDoctorList && nextProps.adminDoctorList.doctorList.length > 0) {
            const { doctorList } = nextProps.adminDoctorList;
            this.setState({ adminDoctorList: doctorList, mainAdminDocList: doctorList })
        }
    }
    handleDoctorApproval = (e, approvedStatus) => {
        const status = {
            uid: e.id,
            approval: approvedStatus,
            userName:"Doctor"
        }
        this.props.approveDoctor(status);
    }
    handleChange = (e) => {
        e.preventDefault();
        this.setState({ [e.target.name]: e.target.value })
    }
    handleNameSearch = (e) => {
        e.preventDefault();
        var result = [];
        const { nameSearch, mainAdminDocList } = this.state;

        this.setState({ [e.target.name]: e.target.value })
        if (!e.target.value) {
            this.setState({ adminDoctorList: mainAdminDocList })
        } else {
            result = _filter(mainAdminDocList, function (list) {

                if (list.firstName != "" || list.firstName != undefined) {
                    return list.firstName.includes(nameSearch);
                }

            })
            this.setState({ adminDoctorList: result })

        }

    }
    handleSearch = (e) => {
        e.preventDefault();
        const { mainAdminDocList, dateSearch } = this.state;
        var datesearchResult = [];
        
        if (!dateSearch) {
            this.setState({ adminDoctorList: mainAdminDocList })

        } else {
            datesearchResult = _filter(mainAdminDocList, function (list) {
                if (list.createdDate != null || list.createdDate != undefined) {
                    return list.createdDate.includes(dateSearch);
                }

            })
            this.setState({ adminDoctorList: datesearchResult })
        }



    }
    render() {
        const { adminDoctorList, nameSearch, dateSearch } = this.state;
        const { userInfo } = this.props.loginDetails
        console.log("profileinfo")
        return (
            <div className="mainMenuSide">
                <div className="wrapper">
                    <div className="container-fluid">
                        <div className="body-content">
                            <div class="search-section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                                            <div class="search-doctor">
                                                <input type="text" placeholder="Name" name="nameSearch" value={nameSearch} onChange={this.handleNameSearch} />
                                                <i class="fas fa-user search-icon "></i>
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col-md-6 col-sm-12 col-12">
                                            <div class="search-doctor">
                                                <input type="date" name="dateSearch" placeholder="Date Search" value={dateSearch} onChange={this.handleChange} />
                                                <i class="far fa-calendar-alt search-icon "></i>

                                            </div>
                                        </div>
                                        <div class="search-doctor-btn col-lg-2 col-12">
                                            <button class="searchDocBtn" disabled="" onClick={this.handleSearch}>Search</button>
                                        </div>
                                    </div></div></div>

                            <div className="search-result-section">

                                {adminDoctorList ? adminDoctorList.map((each, i) => (
                                    <div key={i} className="search-card2">
                                        <img src="../images/no-profile-pic-icon-5.jpg" />
                                        <div className="doc-title">
                                            <p className="title-name">{each.firstName}{each.lastName}</p>
                                            <p> {each.mobileNo}</p>

                                        </div>
                                        <div className="patientView">
                                            <button className="viewDoc" onClick={() => this.props.history.push({ pathname: '/admindashboard', state: { submenuType: "ViewMedical" } })} ><i className="far fa-eye"></i></button>
                                            {each.approvedStatus == "pending" && <button className="approveBtn" onClick={(e) => this.handleDoctorApproval(each, "approved")}><i className="fas fa-check"></i></button>}
                                            {each.approvedStatus == "approved" && <button className="RejectBtn" onClick={() => this.handleDoctorApproval(each, "pending")}><i class="fas fa-times"></i></button>}
                                        </div>
                                    </div>)) : <div></div>}
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        )
    }
}
const mapStateToProps = state => ({
    adminDoctorList: state.adminPatientReducer.adminDoctorList,
    loginDetails: state.loginReducer.loginDetails,

})
const mapDispatchToProps = dispatch => ({
    getDoctorList: () => dispatch(getDoctorList()),
    approveDoctor: (status) => dispatch(approveDoctor(status))

})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MedicalSpecialist);