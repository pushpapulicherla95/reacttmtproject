import actionType from "../Patient/actionType";
import axios from 'axios';
import URL from "../../../asset/configUrl";
import { toastr } from "react-redux-toastr";
import { toastrOptions } from '../../../component/common/toasteroptions';
import { getHospitalDoctorList } from "../../hospital/action";

export const getPatient = (info) => dispatch => {
    const configs = {
        method: 'get',
        url: URL.ADMIN_PATIENT_LIST,
        data: info,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.GET_PATIENT_LIST_SUCCESS,
                payload: res.data
            })
            toastr.success("Message", "Patient List Retrived Successfully", toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error("error", res.data.message, toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_PATIENT_LIST_FAILURE,
            error: err
        })
        toastr.error("errr", toastrOptions);
    })
}
export const getDoctorList = () => dispatch => {
    const configs = {
        method: 'get',
        url: URL.ADMIN_DOCTOR_LIST,
        data: "",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {

        if (res.status === 200) {
            dispatch({
                type: actionType.GET_DOCTOR_LIST_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_DOCTOR_LIST_FAILURE,
            error: err
        })
    })
}

export const approveDoctor = (status) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.APPROVE_DOCTOR,
        data: status,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const info = {
        uid: status.hospitalId || 0
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            if (status.userName == "Doctor") {
                dispatch(getDoctorList());
            } else if (status.userName == "Hospital") {
                dispatch(getHospitalList());
            }
            dispatch(getHospitalDoctorList(info))
            dispatch({
                type: actionType.APPROVE_DOCTOR_SUCCESS,
                payload: res.data
            })
            toastr.success("Message", res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error("error", res.data.message, toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.APPROVE_DOCTOR_FAILURE,
            error: err
        })
        toastr.error("errr", toastrOptions);
    })

}
//Approve Patient
export const approvePatient = (payload) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.APPROVE_DOCTOR,
        data: payload,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        dispatch(getPatient())
    })
}

//View patient details
export const viewDetails = (viewdata) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.EDIT_PATIENT_DETAILS,
        data: viewdata,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }

    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.VIEW_PATIENT_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.VIEW_PATIENT_FAILURE,
            error: err
        })
    })

}

//Get adminprofile
export const getAdminProfile = (viewdata) => dispatch => {

    const configs = {
        method: 'post',
        url: URL.GET_ADMIN_PROFILE,
        data: viewdata,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.GET_ADMIN_PROFILE_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_ADMIN_PROFILE_FAILURE,
            error: err
        })
    })
}

//UPDATE/ADD ADMIN PROFILE
export const updateAdminProfile = (viewdata) => dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPDTAE_ADMIN_PROFILE,
        data: viewdata,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    const info = {
        id: viewdata.id
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch(getAdminProfile(info))
            dispatch({
                type: actionType.UPDATE_ADMIN_PROFILE_SUCCESS,
                payload: res.data
            })
            toastr.success("Message", res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error("error", res.data.message, toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.UPDATE_ADMIN_PROFILE_FAILURE,
            error: err
        })
        toastr.error("errr", toastrOptions);
    })
}

//ADMIN HOSPITAL LIST
export const getHospitalList = (viewdata) => dispatch => {
    const configs = {
        method: 'get',
        url: URL.ADMIN_HOSPITAL_LIST,
        data: "",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }

    axios(configs).then((res) => {
        if (res.status === 200) {
            // dispatch(getAdminProfile(info))
            dispatch({
                type: actionType.GET_HOSPITAL_LIST_SUCCESS,
                payload: res.data
            })
            toastr.success("Message", "Hospital List Retrieved Successfully", toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error("error", res.data.message, toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_HOSPITAL_LIST_FAILURE,
            error: err
        })
        toastr.error("errr", toastrOptions);
    })
}
//Logdata
export const Logdatas = (id) => dispatch => {
    const configs = {
        method: 'get',
        url: URL.GET_LOGDATA + id,

        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }

    axios(configs).then((res) => {

        console.log("jdshgfjd", res)
        if (res.status === 200) {

            dispatch({
                type: actionType.GET_LOGDATA_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.GET_LOGDATA_FAILURE,
            error: err
        })
        toastr.error("Error", toastrOptions);
    })
}
//uploadfiles

export const uploadAdminProfilepic = info => dispatch => {
    const configs = {
        method: 'post',
        url: URL.UPLOAD_ADMIN__PROFILE_PIC,
        data: info,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }

    axios(configs).then((res) => {
        if (res.status === 200) {
            // dispatch(getHospitalPatientList(listdata))
            dispatch({
                type: actionType.UPLOAD_ADMIN__PROFILE_PIC_SUCCESS,
                payload: res.data
            })
            toastr.success('Success', res.data.message, toastrOptions);
        } else if (res.data.status == "failure") {
            toastr.error('Error', res.data.message, toastrOptions);
        }
    }).catch(err => {
        dispatch({
            type: actionType.UPLOAD_ADMIN__PROFILE_PIC_FAILURE,
            error: err
        })
    })

}
