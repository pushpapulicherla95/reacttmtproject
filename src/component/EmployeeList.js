import React, { Component } from 'react';
import { BrowserRouter as Link } from 'react-router-dom';
import TableRow from './TableRow';

import axios from 'axios';
class EmployeeList extends Component {

    constructor(props) {
        super(props);
        this.state = { employee: []
         };

    
    }
    componentDidMount(){
        axios.get('http://localhost:4000/API/employee')
          .then(response => {
            this.setState({ employee: response.data });
          })
          .catch(function (error) {
            console.log(error);
          })
      }
    tabRow() {
        return this.state.employee.map(function (object, i) {
            return <TableRow obj={object} key={i} />;
        });
    }

    render() {
        return (<div>
            <div>
                <h3 align="center">Employee List</h3>
                <table className="table table-striped" style={{ marginTop: 20 }}>
                    <thead>
                        <tr>
                            {/* <th>Id</th> */}
                            <th>FirstName</th>
                            <th>LastName</th>
                            <th>MobileNumber</th>
                            <th>Email</th>
                            <th colSpan="2">Action</th>

                        </tr>
                    </thead>
                    <tbody>
                       {this.tabRow()}
                    </tbody>
                </table>
            </div>

        </div>);
    }
}


export default EmployeeList;