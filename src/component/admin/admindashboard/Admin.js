import React, { Component } from "react";
import AdminProfile from "../admindashboard/Adminprofile";
import Patients from "../admindashboard/Patients";
import MedicalSpecialist from "../admindashboard/MedicalSpecialist";
import Pharma from "../admindashboard/Pharma";
import Insurance from "../admindashboard/Insurance";
import ViewPatient from "../admindashboard/PatientView";
import ViewMedical from "../admindashboard/ViewMedical";
import SymptomsQAentry from "../admindashboard/SymptomsQAentry";
import Textrobot from "../admindashboard/TextRobot";
import HospitalList from "../admindashboard/HospitalList";
import Logdata from "../admindashboard/Logdata"
class AdminMenuRoute extends Component {
constructor(props){
    super(props);
    this.state = {
        submenuType:"" 
    }
}
    render() {
        const { submenuType } = this.props.location.state || this.state;

        return (
            <div className="inner-pageNew">
                <section>
                    <header className="header">
                        <div className="brand">
                            <a href="adminDashboard.html" className="backArrow">
                                <i className="fas fa-chevron-left"></i>
                            </a>
                            <div className="mobileLogo">
                                <a href="javascript:void(0)"  onClick={() => this.props.history.push("/adminmainmenu")} className="logo">
                                    <img src="../images/logo.jpg" /> </a></div>

                        </div>
                        <div className="top-nav">

                            <div className="dropdown float-right mr-2" >
                                <a className="dropdown-toggle" data-toggle="dropdown">
                                    Admin
                            </a>
                                <div className="dropdown-menu">
                                    <a className="dropdown-item" href="#"><i className="fa fa-edit icon-head-menu"></i><label className="head-menu"> My account</label></a>
                                    <a className="dropdown-item" href="#">Setting</a>
                                    <a className="dropdown-item" href="#">Logout</a>
                                </div>
                            </div>
                        </div>
                    </header>

                    <aside className="sideMenu">
                        <ul className="sidemenuItems">
                            <li><a href="javascript:void(0)" onClick={() => this.props.history.push({pathname:'/admindashboard',state:{submenuType:"AdminProfile"}})} title="My Profile"><img src="../images/icon2-color.png" /></a><div className="menu-text"><span>Personal</span></div></li>
                            <li><a href="javascript:void(0)" onClick={() => this.props.history.push({pathname:'/admindashboard',state:{submenuType:"AdminPatient"}})} title="Patients"><img src="images/patient-icon-color.png" /></a><div className="menu-text"><span>My Patients</span></div></li>
                            <li><a href="javascript:void(0)" onClick={() => this.props.history.push({pathname:'/admindashboard',state:{submenuType:"Hospitalist"}})} title="hospital list"><img src="images/patient-icon-color.png" /></a><div className="menu-text"><span>Hospital List</span></div></li>
                            <li><a href="javascript:void(0)" onClick={() => this.props.history.push({pathname:'/admindashboard',state:{submenuType:"AdminMedical"}})} title="Doctors"><img src="images/icon-doctor-color.png" /></a><div className="menu-text"><span>My Doctors</span></div></li>
                            <li><a href="javascript:void(0)" onClick={() => this.props.history.push({pathname:'/admindashboard',state:{submenuType:"AdminPharma"}})}  title="Pharmacy"><img src="../images/icon4-color.png" /></a><div className="menu-text"><span>Pharmacy</span></div></li>
                            <li><a href="javascript:void(0)" onClick={() => this.props.history.push({pathname:'/admindashboard',state:{submenuType:"AdminInsurance"}})}  title="Insurance"><img src="images/icon-insurance-color.png" /></a><div className="menu-text"><span>Insurance</span></div></li>
                            <li><a href="javascript:void(0)" onClick={() => this.props.history.push({pathname:'/admindashboard',state:{submenuType:"Logdata"}})}  title="Insurance"><img src="images/icon-insurance-color.png" /></a><div className="menu-text"><span>Log Data</span></div></li>

                        </ul>
                    </aside>
                  {submenuType == "AdminProfile" && <AdminProfile {...this.props}/>}
                  {submenuType == "AdminPatient" && <Patients history={this.props.history}/>}
                  {submenuType == "AdminMedical" && <MedicalSpecialist  history={this.props.history}/>}
                  {submenuType == "AdminPharma" && <Pharma/>}
                  {submenuType == "AdminInsurance" && <Insurance/>}
                  {submenuType == "ViewProfile" && <ViewPatient  {...this.props}/>}
                  {submenuType == "ViewMedical" && <ViewMedical/>}
                  {submenuType == "ViewSymptomsQAentry" && <SymptomsQAentry/>}
                  {submenuType == "Textrobot" && <Textrobot/>}
                  {submenuType == "Hospitalist" && <HospitalList/>}
                {submenuType == "Logdata" && <Logdata/>
}
                </section>
            </div>

        )
    }
}
export default AdminMenuRoute;