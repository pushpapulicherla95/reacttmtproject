const express = require('express');
const emailRouter = express.Router();
const { sendEmail } = require('../controller/email/email.api');
const { sendRegisterMail, sendForgetMail, sendBookingAppoinmentMail, sendRegisterMailHealthCare } = require('../controller/email/mailtemplate');

emailRouter.route('/send')
    .post(sendEmail)
emailRouter.route('/register')
    .post(sendRegisterMail)
emailRouter.route('/registerHealthCare')
    .post(sendRegisterMailHealthCare)
emailRouter.route('/forget')
    .post(sendForgetMail)
emailRouter.route('/mailBookingAppoinment')
    .post(sendBookingAppoinmentMail)
module.exports = emailRouter;