import React from "react";
import Logout from '../common/Logout';
import AsideMenu from "../common/AsideMenu";
class Subscription extends React.Component {
  handleLogout = () => {

  }
  render() {
    return (
      <div class="inner-pageNew">
        <div>
          <header class="header">
            <div class="brand">
              <a href="Dashboard.html" class="backArrow">
                <i class="fas fa-chevron-left" />
              </a>
              <div class="mobileLogo">
                <a href="Dashboard.html" class="logo">
                  <img src="images/logo.jpg" />{" "}
                </a>
              </div>
            </div>
            <div class="top-nav">
              <div class="dropdown float-right mr-2">
                <a class="dropdown-toggle" data-toggle="dropdown">
                  Patient Name
                </a>
                <Logout />
              </div>
            </div>
          </header>
          <AsideMenu />
          {/* <aside class="sideMenu">
            <ul class="sidemenuItems">
              <li>
                <a href="personalData.html" title="Personal">
                  <img src="images/icon2-color.png" />
                </a>
                <div class="menu-text">
                  <span>Personal</span>
                </div>
              </li>
              <li>
                <a href="" title="Telemedicine">
                  <img src="images/icon5-color.png" />
                </a>
                <div class="menu-text">
                  <span>Telemedicine</span>
                </div>
              </li>
              <li>
                <a href="" title="Health Records">
                  <img src="images/icon1-color.png" />
                </a>
                <div class="menu-text">
                  <span>Health Records</span>
                </div>
              </li>
              <li>
                <a href="" title="Pharmacy">
                  <img src="images/icon4-color.png" />
                </a>{" "}
                <div class="menu-text">
                  <span>Pharmacy</span>
                </div>
              </li>
              <li>
                <a href="" title="Services">
                  <img src="images/icon7-color.png" />
                </a>
                <div class="menu-text">
                  <span>Services</span>
                </div>
              </li>
              <li>
                <a href="shareData.html" title="Share Data">
                  <img src="images/icon6-color.png" />
                </a>
                <div class="menu-text">
                  <span>Share data</span>
                </div>
              </li>
            </ul>
          </aside> */}
          <div class="mainMenuSide" id="main-content">
            <div class="wrapper">
              <div class="container-fluid">
                <div class="body-content">
                  <div class="row mt-3">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                      <div class="subscription">
                        <h5>Tomato medical</h5>
                        <h6 class="fw600">Starter</h6>
                        <div class="subAmount ">
                          <p>
                            2.99 <span>€</span>
                            <span class="subspan2">3 Month Plan</span>
                          </p>
                        </div>
                        <div class="subfeature">
                          <ul>
                            <li>1Gb Cloud Storage </li>

                            <li>24/7 support</li>
                          </ul>
                        </div>
                        <div class="subBtn">
                          <button class="clrsub1Btn">Purchase</button>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                      <div class="subscription">
                        <h5>Tomato medical</h5>
                        <h6 class="fw600">Basic</h6>
                        <div class="subAmount">
                          <p>
                            12.99 <span>€</span>
                            <span class="subspan2">Monthly Plan</span>
                          </p>
                        </div>
                        <div class="subfeature">
                          <ul>
                            <li>100 Gb Cloud Storage </li>

                            <li>24/7 support</li>
                          </ul>
                        </div>
                        <div class="subBtn">
                          <button class="clrsub2Btn">Purchase</button>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                      <div class="subscription ">
                        <div class="subHead">
                          <h5>Tomato medical</h5>
                          <h6 class="fw600">Premium</h6>
                        </div>
                        <div class="subAmount">
                          <p>
                            26.99 <span>€</span>
                            <span class="subspan2">Monthly Plan</span>
                          </p>
                        </div>
                        <div class="subfeature">
                          <ul>
                            <li>1000 Gb Cloud Storage </li>

                            <li>24/7 support</li>
                          </ul>
                        </div>
                        <div class="subBtn">
                          <button class="clrsub3Btn">Purchase</button>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                      <div class="subscription ">
                        <h5>Tomato medical</h5>
                        <h6 class="fw600">Unlimited</h6>
                        <div class="subAmount">
                          <p>
                            99.99 <span>€</span>
                            <span class="subspan2">Monthly Plan</span>
                          </p>
                        </div>
                        <div class="subfeature">
                          <ul>
                            <li>Unlimited Cloud Storage </li>

                            <li>24/7 support</li>
                          </ul>
                        </div>
                        <div class="subBtn">
                          <button class="clrsub4Btn">Purchase</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Subscription;
