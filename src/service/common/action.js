import URL from "../../asset/configUrl";
import axios from "../../asset/axios";
import actionType from "../common/actionType";
import { toastr } from "react-redux-toastr";
import { toastrOptions } from "../../component/common/toasteroptions";
import { showIsLoading, hideIsLoading } from "../login/action";
import AWS from "aws-sdk";
//Send Email
export const sendEmailAPICall = emailInfo => dispatch => {
  dispatch(showIsLoading());
  const configs = {
    method: "post",
    url: URL.SEND_EMAIL,
    data: emailInfo,
      headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
  };
  //axios.post(URL.SEND_EMAIL,emailInfo).then((res) => {
  axios(configs)
    .then(res => {
      if (res.status === 200 && res.data.status === "success") {
        dispatch({
          type: actionType.SEND_EMAIL_SUCCESS,
          payload: res.data
        });
        toastr.success("Email status", res.data.message, toastrOptions);
      } else {
        dispatch({
          type: actionType.SEND_EMAIL_FAILURE,
          payload: res.data
        });
        toastr.error("Email status", res.data.message, toastrOptions);
      }
      dispatch(hideIsLoading());
    })
    .catch(err => {
      dispatch({
        type: actionType.SEND_EMAIL_FAILURE,
        payload: err
      });
      toastr.error("Email status", "Server Problem", toastrOptions);
      dispatch(hideIsLoading());
    });
};

//Send SMS
export const sendSMSAPICall = smsInfo => dispatch => {
  dispatch(showIsLoading());
  const configs = {
    method: "post",
    url: URL.SEND_SMS,
    data: smsInfo,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  //axios.post(URL.SEND_SMS,smsInfo).then((res) => {
  axios(configs)
    .then(res => {
      if (res.status === 200 && res.data.status === "success") {
        dispatch({
          type: actionType.SEND_SMS_SUCCESS,
          payload: res.data
        });
        toastr.success("SMS status", res.data.message, toastrOptions);
      } else {
        dispatch({
          type: actionType.SEND_SMS_FAILURE,
          payload: res.data
        });
        toastr.error("SMS status", res.data.message, toastrOptions);
      }
      dispatch(hideIsLoading());
    })
    .catch(err => {
      dispatch({
        type: actionType.SEND_SMS_FAILURE,
        payload: err
      });
      toastr.error("SMS status", "Server Problem", toastrOptions);
      dispatch(hideIsLoading());
    });
};

export const awsFiles = files => {
  return new Promise((resolve, reject) => {
    AWS.config.update({
      accessKeyId: process.env.REACT_APP_ACCESSKEY,
      secretAccessKey: process.env.REACT_APP_SECRETACCESSKEY,
      region: process.env.REACT_APP_REGION
    });
    var s3 = new AWS.S3();
    var data = {
      Bucket: "tomato-medical-bucket",
      Key: "patienthealthdata/" + files
    };
    s3.getSignedUrl("getObject", data, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
        console.log("succesfully uploaded the image!", data);
      }
    });
  });
};

export const awsPersonalFiles = files => {
  return new Promise((resolve, reject) => {
    AWS.config.update({
      accessKeyId: process.env.REACT_APP_ACCESSKEY,
      secretAccessKey: process.env.REACT_APP_SECRETACCESSKEY,
      region: process.env.REACT_APP_REGION
    });
    var s3 = new AWS.S3();
    var data = {
      Bucket: "tomato-medical-bucket",
      Key: "patientpersonal/" + files
    };
    s3.getSignedUrl("getObject", data, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
        console.log("succesfully uploaded the image!", data);
      }
    });
  });
};


export const handleFileDelete = files => {
  return new Promise((resolve, reject) => {
    AWS.config.update({
      accessKeyId: process.env.REACT_APP_ACCESSKEY,
      secretAccessKey: process.env.REACT_APP_SECRETACCESSKEY,
      region: process.env.REACT_APP_REGION
    });
    var s3 = new AWS.S3();
    var data = {
      Bucket: "tomato-medical-bucket",
      Key: "patientpersonal/" + files
    };
    s3.getSignedUrl("getObject", data, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
        console.log("succesfully uploaded the image!", data);
      }
    });
  });
};

