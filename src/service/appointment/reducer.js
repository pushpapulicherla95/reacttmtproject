import actionType from '../appointment/actionType';


const initialState = {
    listPatientAppointment: "",
    listDoctorAppointment: "",
    listChatHistory: "",
    listpatientDetails:"",
    listdoctorDetails:""
}

const appointmentReducer = (state = initialState, action) => {
    switch (action.type) {

        case actionType.LIST_APPOINTMENT_SUCCESS:
            return {
                ...state,
                listPatientAppointment: action.payload,
            }
        case actionType.LIST_APPOINTMENT_FAILURE:
            return {
                ...state,
                listPatientAppointment: action.payload,
            }
        case actionType.LIST_DOC_APPOINTMENT_SUCCESS:
            return {
                ...state,
                listDoctorAppointment: action.payload,
            }
        case actionType.LIST_DOC_APPOINTMENT_FAILURE:
            return {
                ...state,
                listDoctorAppointment: action.payload,
            }
        case actionType.CHAT_HISTORY_SUCCESS:
            return {
                ...state,
                listChatHistory: action.payload,
            }
        case actionType.CHAT_HISTORY_FAILURE:
            return {
                ...state,
                listChatHistory: action.payload,
            }
        case actionType.UPDATE_FEEDBACK_SUCCESS:
            return {
                ...state
            }
        case actionType.UPDATE_FEEDBACK_FAILURE:
            return {
                ...state
            }
            case actionType.LIST_PATIENT_INFO_SUCCESS:
            // console.log("listpatientDetails",action.payload)
            return {
                ...state,
                listpatientDetails: action.payload,
            }
        case actionType.LIST_PATIENT_INFO_FAILURE:
            return {
                ...state,
                listpatientDetails: action.payload,

            }
            case actionType.UPDATE_DOCTOR_FEEDBACK_SUCCESS:
            return {
                ...state
            }
        case actionType.UPDATE_DOCTOR_FEEDBACK_FAILURE:
            return {
                ...state
            }
            case actionType.LIST_DOCTOR_INFO_SUCCESS:
            // console.log("listpatientDetails",action.payload)
            return {
                ...state,
                listdoctorDetails: action.payload,
            }
        case actionType.LIST_DOCTOR_INFO_FAILURE:
            return {
                ...state,
                listdoctorDetails: action.payload,

            }
        default:
            return state;
    }
}
export default appointmentReducer;