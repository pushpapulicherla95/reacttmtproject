import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import ChatBot from "./ChatBot1";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { symptomMainList  } from "../../service/admin/TextRobo/action";
import LandingHeader from '../common/LandingHeader'
// import MultipleDatePicker from "react-multiple-datepicker";

class Landing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      allSymptomList:[],
      selectedSymptomData:{}
    };
  }

  handleChatbot = () => {
    this.setState({ open: !this.state.open },);
  };

  componentWillMount(){
    this.props.symptomMainList()
  }

  onSymptomInputChange=(e)=>{
    const { symptomMainListData } = this.props;
    const val = e.target.value;
    this.setState({ symptomName: e.target.value }, () => {
      const upDatedallListSymptom = symptomMainListData.filter((each) => {
        return each.symptomsName && each.symptomsName.toLowerCase().includes(val && val.toLowerCase() );
      })
      this.setState({ allSymptomList: val ? upDatedallListSymptom : [] })
    })
  }
  render() {
    return (
      <React.Fragment>
        <LandingHeader/>
        <section class="MbeBannerSection" />
        <section class="banner d-flex justify-content-center align-items-center">
          <div class="container">
            <div class="bannerContent  flex-column">
              <h1>
                Welcome to
                 <span> tomato Medical </span>
              </h1>
              <p>
                Dr. med. Lemberger, a German Orthopedic and Trauma Surgeon,
                created this outstanding solution for patients worldwide. He
                thinks that everybody worldwide should have access to
                medical knowledge and therapy.{" "}
              </p>
              {/* <!-- <button type="button" class="commonBtn">View More</button>  --> */}
              <div class="symptomcheckdiv">
                <a class="expend" aria-expanded="false">
                  Tell us about your medical problem
                </a>
                <div id="symptomChecker">
                  <div class="form-group searchSymptom">
                    <input
                      type="text"
                      class="form-control"
                      placeholder="Search Symptom Checker"
                      onChange={(e)=>{
                         this.onSymptomInputChange(e)
                      }}
                      value={this.state.symptomName && this.state.symptomName}
                    />
                    {this.state.allSymptomList.length !=0 && <div className="searchResultSuggestion slightchangedashboard">
                      <ul>
                        {this.state.allSymptomList.map((each) =>
                          <li style={{textAlign:"left"}} onClick={()=>this.setState({selectedSymptomData:each,symptomName:each.symptomsName,allSymptomList:[]})}> {each.symptomsName}  </li>
                        )}
                      </ul>
                    </div>}

                  </div>
                  <button type="button" class="commonBtn searchBtn" onClick={()=> 
                    this.state.symptomName && this.props.history.push({ pathname: "/symptomcheckerQA",state:{...this.state.selectedSymptomData}})}>
                    Search
                  </button>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div class="emergencyAlert lognemer">
          <a href="">
            {" "}
            <img src={require("../../images/emergencyCall.png")} />          </a>
        </div>
        <div class="live-chat">
          <button
            type="button"
            id="chatBtn"
            class="livechatbtn positionfixed"
            onClick={this.handleChatbot}
          >
            <img src={require("../../images/chat-icon.png")} alt="Live Chat" />
          </button>
          <ChatBot
            handleChatbot={this.handleChatbot}
            open={this.state.open}
          />
        </div>
        <section class="ourServices">
          <div class="container">
            <h1 class="titles text-center">
              Our Services
              <span> For You</span>
            </h1>
            <div class="serviceList">
              <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12 col-12 paddingLeft0">
                  <div class="serviceBg">
                    <div class="serviceIcon">
                      <img src={require("../../images/symptomchecker.png")} />
                    </div>
                    <h4>Check your medical symptoms</h4>
                    <p>
                      Check your medical symptoms and medical problems
                      without a doctors. Our smart artificial intelligence
                      has hugh underlying medical data resources.{" "}
                    </p>
                    <button type="button" class="readmore">
                      Start Here
                    </button>
                  </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-12 paddingLeft0">
                  <div class="serviceBg">
                    <div class="serviceIcon">
                      <img src={require("../../images/healthRecord.png")} />
                    </div>
                    <h4>health record </h4>
                    <p>
                      You can store your data safely and have worldwide
                      access to your data and translations in multiple
                      languages worldwide, emergency data...{" "}
                    </p>
                    <button type="button" class="readmore">
                      Start Here
                    </button>
                  </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-12 paddingLeft0">
                  <div class="serviceBg">
                    <div class="serviceIcon">
                      <img src={require("../../images/emerancy.png")} />
                    </div>
                    <h4>emergency services</h4>
                    <p>
                      GPS alarm via email and SMS. Dead man alarm sets off
                      alarm automatically when smartphone doesn´t move any
                      longer.Can actively transfer ...{" "}
                    </p>
                    <button type="button" class="readmore">
                      Start Here
                    </button>
                  </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-12 paddingLeft0">
                  <div class="serviceBg">
                    <div class="serviceIcon">
                      <img src={require("../../images/chatIcon.png")} />
                    </div>
                    <h4>chat with a doctor </h4>
                    <p>
                      Contact registered platform doctors to have a
                      telemedicine consultation or invite them to become a
                      platform partner, if they are not already{" "}
                    </p>
                    <button type="button" class="readmore">
                      Start Here
                    </button>
                  </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-12 paddingLeft0">
                  <div class="serviceBg">
                    <div class="serviceIcon">
                      <img src={require("../../images/hospital.png")} />
                    </div>
                    <h4>find medical expert </h4>
                    <p>
                      Find the right specialist for your health by specialty
                      and location and make an online reservation for
                      hospitals, doctors, therapists and all kind of
                      caregivers.{" "}
                    </p>
                    <button onClick={()=>{
                      this.props.history.push({pathname:'/submenu',state:{submenuType:"MedicalSpecialist"}})
                    }} type="button" class="readmore">
                      Start Here
                    </button>
                  </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-12 paddingLeft0">
                  <div class="serviceBg">
                    <div class="serviceIcon">
                      <img src={require("../../images/shopMedical.png")} />
                    </div>
                    <h4>Shop medication and medical products</h4>
                    <p>
                      Shop for medication and all medical products and
                      devices you need. Our platform offers access to
                      pharmacies, medical supply stores ...
                    </p>
                    <button type="button" class="readmore">
                      Start Here
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div class="scrollTop">
          <a href="javascript:void(0)">
            <i class="fa fa-arrow-up" aria-hidden="true" />
          </a>
        </div>
        <section class="downloadApp">
          <div class="container">
            <div class="downloadappContent">
              <h2>
                Click here to get your free medical emergency app with
                  <b> save storage</b> of your health data
              </h2>
              <button type="button" class="commonBtn">
                Download Now
              </button>
            </div>
          </div>
        </section>
        <section class="faqSection">
          <div class="container">
            <h1 class="titles text-center">
              Frequently asked
              <span> questions</span>
            </h1>
            <div class="faqContent">
              <dl>
                <dt>
                  <span>
                    <i class="fas fa-chevron-right" />
                  </span>{" "}
                  Can I download the app for android and Apple/IOS?
                </dt>
                <dd>Yes</dd>
                <dt>
                  <span>
                    <i class="fas fa-chevron-right" />
                  </span>{" "}
                  Are my data/information safe?
                </dt>
                <dd>
                  Yes. All data are encrypted and protected according to
                  european law (DSGVO/GDPR) and we are in the process to get
                  HIPPA clearance and ISO 27001 certification).
                </dd>
                <dt>
                  <span>
                    <i class="fas fa-chevron-right" />
                  </span>{" "}
                  What are the requirements to use the app?
                </dt>
                <dd>
                  You can use the app on every PC, Mac, Android and IOS
                  device with a web browser.
                </dd>
                <dt>
                  <span>
                    <i class="fas fa-chevron-right" />
                  </span>{" "}
                  What are the benefits, when I use the app?
                </dt>
                <dd>
                  You can check your medical symptoms any time, if you are
                  not sure, call a telemedicine doctor, find a real doctor
                  for your problem and make an appointment online, use
                  medication intake & vaccination reminder, shop for
                  medication and products, save your life with with our
                  multilingual emergency GPS-services, store and manage your
                  personal medical data and exchange them with your doctors
                  to avoid double exams, use different languages when you
                  are abroad for your medical communication.
                </dd>
              </dl>
              <div class="text-center mb-3">
                <button type="button" class="commonBtn text-center">
                  Try it now for free
                </button>
              </div>
            </div>
          </div>
        </section>
        <footer>
          <div class="footerLink">
            <div class="container">
              <ul>
                <li>
                  <NavLink to="/aboutus" >About </NavLink>
                </li>
                <li>
                  <a href="">For Partners </a>
                </li>
                <li>
                <NavLink to="/contact" >Contact </NavLink>
                </li>
                <li>
                <NavLink to="/datasecurity" >Data Security </NavLink>
                </li>
                <li>
                  <a href="">Disclaimer </a>
                </li>
                <li>
                <NavLink to="/imprint">Imprint </NavLink>
                </li>
              </ul>
            </div>
          </div>

          <div class="footerCopyRights">
            <div class="container">
              <ul>
                <li>
                  <a href="">
                    <i class="fab fa-facebook-f" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-google-plus-g" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-instagram" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-twitter" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-linkedin-in" />
                  </a>
                </li>
              </ul>
              <p>Copyrights @ 2019, All Rights Reserved</p>
            </div>
          </div>
        </footer>

        <div class="modal" id="myModal">
          <div class="modal-dialog modalPopup">
            <div class="modal-content">
              <div class="modal-header borderNone">
                <button class="close" data-dismiss="modal">
                  <img src={require("../../images/close.png")} />
                </button>
              </div>
              <div class="modal-body" />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  symptomMainListData:   (state.textRoboReducer.symptomMainListData && state.textRoboReducer.symptomMainListData.symptomsList) || [] ,
})

const mapDispatchToProps = dispatch => ({
  symptomMainList: () => dispatch(symptomMainList()),
})

export default withRouter( connect(
  mapStateToProps,
  mapDispatchToProps
)(Landing));


