import React from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { getQuestionAndOptionList  } from "../../service/admin/TextRobo/action"
import _ from "lodash";
import LandingHeader from "../common/LandingHeader"

class QuestionAndOptions extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
           allOptionSelected : []
        }
    }

    componentWillMount(){
        if(this.props.location.state && this.props.location.state.symptomsId){
            this.props.getQuestionAndOptionList({ "symptomsId": this.props.location.state.symptomsId })
        }
    }

    onCheckBoxClick=(e,optionValue)=>{
        const newOptionArray = this.state.allOptionSelected.filter((data) => {
            return !e.target.checked ? !_.isEqual(data, optionValue) : true
        });
        e.target.checked && newOptionArray.push(optionValue)
        this.setState({
            allOptionSelected: newOptionArray
        })
    }

    render() {
        const { questionAndOptionList } = this.props;
        const routeState =  this.props.location.state;
        console.log("Elaiya Props",this.state)
        return (
            <body>
                <LandingHeader/>
                <div class="container searchContent mt-3 mb-3">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                            <div class="searchCard2">
                                <h6>Advertisment</h6>
                                <div class="Ad-secside">
                                    <ul>
                                        <li class="ads">	<div class="ad-secinner">
                                            <img src="images/ad_gif.gif" />
                                        </div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="searchCard">
                                <h5>{routeState.symptomsName && routeState.symptomsName}</h5>
                                <hr />
                                <p>Find possible causes of headaches based on specific factors. Check one or more factors on this page that apply to your symptom.</p>
                                {questionAndOptionList.map((each) =>
                                    <div class="checkSection">
                                        <h5>{each.question}</h5>
                                        <div class="inputChectSec">
                                            <ul>
                                                {each.options.map((optionValue) =>
                                                    <li><input type="checkbox" onClick={(e)=> this.onCheckBoxClick(e,optionValue)} /><label>{optionValue.option}</label></li>
                                                )}
                                            </ul>
                                        </div>
                                    </div>
                                )}
                                <div class="row mt-3">
                                    <div class="col-12">
                                        <button type="button" onClick={() =>

                                           this.props.history.push({pathname:"/symptomcauselist",
                                             state:{allOptionSelected:this.state.allOptionSelected,
                                             ...this.props.location.state}})} 
                                             
                                    class="commonBtn float-right" disabled={this.state.allOptionSelected.length == 0}>Find Causes</button></div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                            <div class="searchCard2">
                                <h6>Advertisment</h6>

                                <div class="Ad-secside">
                                    <div class="ad-secinner">
                                        <img src="images/ad_gif.gif" />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="scrollTop">
                    <a href="javascript:void(0)">
                        <i class="fa fa-arrow-up" aria-hidden="true"></i>
                    </a>
                </div>
                <footer>
                    <div class="footerLink">
                        <div class="container">
                            <ul>
                                <li>
                                    <a href="">About </a>
                                </li>
                                <li>
                                    <a href="">For Partners </a>
                                </li>
                                <li>
                                    <a href="">Contact </a>
                                </li>
                                <li>
                                    <a href="">Data Security </a>
                                </li>
                                <li>
                                    <a href="">Disclaimer </a>
                                </li>
                                <li>
                                    <a href="">Imprint </a>
                                </li>
                            </ul>

                        </div>
                    </div>

                    <div class="footerCopyRights">
                        <div class="container">
                            <ul>
                                <li>
                                    <a href="">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                            </ul>
                            <p>Copyrights @ 2019, All Rights Reserved</p>
                        </div>
                    </div>
                </footer>

                <div class="modal" id="myModal">
                    <div class="modal-dialog modalPopup">
                        <div class="modal-content">
                            <div class="modal-header borderNone">
                                <button class="close" data-dismiss="modal">
                                    <img src="images/close.png" />
                                </button>
                            </div>
                            <div class="modal-body">

                            </div>
                        </div>
                    </div>

                </div>
            </body>
        )
    }
}

  const mapStateToProps = state => ({
    questionAndOptionList : state.textRoboReducer.questionAndOptionData && state.textRoboReducer.questionAndOptionData.QuestionAndOption || [] 
  })
  
  const mapDispatchToProps = dispatch => ({
    getQuestionAndOptionList : (payload) => (dispatch(getQuestionAndOptionList(payload))),
  })
  
  export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
  )(QuestionAndOptions));
