import React, { Component } from "react";

class CityList extends Component {
  constructor(props) {
    super(props);
    this.state = { city: [] };

    fetch("http://localhost:4000/API/city ")
      .then(response => response.json())
      .then(city => this.setState({ city }));
  }
  render() {
    return (
      <div>
        <div>
          <h3 align="center">City List</h3>
          <table className="table table-striped" style={{ marginTop: 20 }}>
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>CountryCode</th>
                <th>District</th>
                <th>Population</th>
              </tr>
            </thead>
            <tbody>
              {this.state.city.map(citys => (
                <tr>
                  <td>{citys.ID}</td>
                  <td>{citys.Name}</td>
                  <td>{citys.CountryCode}</td>
                  <td>{citys.District}</td>
                  <td>{citys.Population}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

// export default CityList;
// render(){
//     return({

//     })
// }

// }
