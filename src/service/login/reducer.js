import actionType from '../../service/login/actionType';

const initialState = {
    userLoginStatus: null,
    loginDetails: "",
    registerDetails: "",
    forgotpswDetails: "",
    changepswDetails: "",
    changeuserPswDetails: "",
    isLoading: false,
    userTypeList: "",
    testData: ""
}

const loginReducer = (state = initialState, action) => {

    switch (action.type) {
        case actionType.LOGIN_SUCCESS:
            return {
                ...state,
                loginDetails: action.payload,
                userLoginStatus: action.loginStatus
            };
        case actionType.LOGIN_FAILURE:
            return {
                ...state,
                loginDetails: action.error
            }

        case actionType.REGISTER_SUCCESS:
            return {
                ...state,
                registerDetails: action.payload,
            }
        case actionType.REGISTER_FAILURE:
            return {
                ...state,
                registerDetails: action.error
            }
        case actionType.HEALTHCARE_REGISTER_SUCCESS:
            return {
                ...state,
                registerDetails: action.payload
            }
        case actionType.HEALTHCARE_REGISTER_FAILURE:
            return {
                ...state,
                registerDetails: action.error
            }
        case actionType.FORGOTPASSWORD_SUCCESS:
            console.log("snbnsa")
            return {
                ...state,
                forgotpswDetails: action.payload,
            }
        case actionType.FORGOTPASSWORD_FAILURE:
            return {
                ...state,
                forgotpswDetails: action.error
            }
        case actionType.CHANGEPASSWORD_SUCCESS:
            return {
                ...state,
                changepswDetails: action.payload,
            }
        case actionType.CHANGEPASSWORD_FAILURE:
            return {
                ...state,
                changepswDetails: action.error
            }
        case actionType.USERCHANGEPASSWORD_SUCCESS:
            return {
                ...state,
                changeuserPswDetails: action.payload,
            }
        case actionType.USERCHANGEPASSWORD_FAILURE:
            return {
                ...state,
                changeuserPswDetails: action.error
            }
        case actionType.SHOW_IS_LOADING:
            return {
                ...state,
                isLoading: true
            };
        case actionType.HIDE_IS_LOADING:
            return {
                ...state,
                isLoading: false
            };
        case actionType.GET_USERTYPE_SUCCESS:
            return {
                ...state,
                userTypeList: action.payload
            }
        case actionType.GET_USERTYPE_FAILURE:
            return {
                ...state,
                userTypeList: action.error
            }
        case actionType.REGISTER_MAIL_SUCCESS:
            return {
                ...state
            }
        case actionType.REGISTER_MAIL_FAILURE:
            return {
                ...state
            }
        case actionType.ADMIN_LOGIN_SUCCESS:
         return {
             ...state,
             loginDetails:action.payload
         }
        case actionType.ADMIN_LOGIN_FAILURE:
        return{
            ...state,
            loginDetails:action.error
        }
        case actionType.HOSPITAL_REGISTER_SUCCESS:
        return {
            ...state,
                registerDetails: action.payload,
        }
        case actionType.HOSPITAL_REGISTER_FAILURE:
        return {
            ...state,
            registerDetails: action.error,
        }
        default:
            return state;
    }

}



 

export default loginReducer;
