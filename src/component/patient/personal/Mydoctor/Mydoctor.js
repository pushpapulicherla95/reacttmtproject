import React, { Component } from "react";
import { connect } from "react-redux";
import { getPersonaldata } from "../../../../service/healthrecord/action";
import axios from "axios";
import {
  adddoctorData,
  deletedoctorData,
  updatedoctorData
} from "../../../../service/patient/action";
import { getUploadFiles, handleFileDelete } from "../../../../service/upload/action";
import Select from "react-select";
import URL from "../../../../asset/configUrl";
import { toastr } from "react-redux-toastr";
import { awsPersonalFiles } from "../../../../service/common/action";

import { onlyDate, onlyNumbers } from "../KeyValidation";
class Mydoctor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 1,
      name: "",
      title: "",
      address: "",
      phone: "",
      specialisation: "",
      education: [],
      tabone: true,
      tabtwo: false,
      addallergies: false,
      editallergies: false,
      editDetails: {},
      uploadPopup: false,
      name: "",
      fileName: "",
      fileType: "",
      fileURL: "",
      formValid: false
    };
  }
  //validation
  // validationField=()=>{
  //   const {name,
  //     title,
  //       address,
  //       phone,
  //       specialisation}=this.state
  //       this.setState({
  //         formValid:
  //         title||
  //         name||

  //         address||
  //         phone||
  //         specialisation||
  //         name
  //       })
  // }
  ValidationField = () => {
    const {
      name,
      title,
      address,
      phone,
      specialisation } = this.state
    this.setState({
      formValid:
        title




    });

  }

  handleAwsFiles = value => {
    window.open("https://data.tomatomedical.com/patientpersonal/" + value, '_blank');

  };

  handleChangePopup = e => {
    var reader = new FileReader();
    var file = e.target.files[0];
    let name = file.name.split('.')[0]
    let type = "." + file.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileURL: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(file);
  };
  popupSubmit = () => {
    var phrase = this.state.fileURL;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);
    var { userInfo } = this.props.loginDetails;
    const payload = {
      uid: userInfo.userId,
      z: match[1],
      fileName: this.state.fileName,
      fileType: this.state.fileType,
      name: this.state.name,
      category: "mydoctor"
    };
    this.setState({ uploadPopup: false });

    axios
      .post(URL.FILEMYDOCTOR_UPLOAD, payload)
      .then(response => {
        response.data.status === "success"
          ? toastr.success("Message", response.data.message)
          : toastr.error("Message", response.data.message);
        if (response.data.status === "success") {
          var { userInfo } = this.props.loginDetails;
          const uploadFiles = {
            uid: userInfo.userId,
            category: "mydoctor"
          };
          this.props.getUploadFiles(uploadFiles);
          this.setState({ uploadPopup: false });
          this.setState({
            name: "",
            fileName: "",
            fileType: "",
            fileURL: ""
          });

        }
      })
      .catch(error => {
        // this.setState({ uploadPopup: true });
      });
  };

  componentWillMount() {
    const { userInfo } = this.props.loginDetails;

    const listpersonal = {
      uid: userInfo.userId
    };
    this.props.getPersonaldata(listpersonal);
    // var { userInfo } = this.props.loginDetails;
    const uploadFiles = {
      uid: userInfo.userId,
      category: "mydoctor"
    };
    this.props.getUploadFiles(uploadFiles);
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.getpersonalDetails &&
      nextProps.getpersonalDetails.myDoctors != null &&
      nextProps.getpersonalDetails.myDoctors != 0
    ) {
      const Education = nextProps.getpersonalDetails.myDoctors;
      let education = JSON.parse(Education);
      this.setState({ education: education });
    }
  }
  handleSelect = e => {
    this.setState({ medicinefrequency: e });
  };
  handleChange = (e, index) => {

    this.setState({ [e.target.name]: e.target.value }, () => {
      this.ValidationField();
    });

  };

  tabOne = () => {
    this.setState({ tabone: true, tabtwo: false });
  };
  tabTwo = () => {
    this.setState({ tabone: false, tabtwo: true });
  };
  addallergiespop = () => {
    this.setState({ addallergies: true, formValid: false }, () => {
      this.emptystate();
    });
  };

  modalPopUpClose = () => {
    this.setState({
      addallergies: false,
      editallergies: false
    });
  };
  modalPopupOpen = e => {
    this.setState({ editallergies: true, editDetails: e });
    this.setState({
      name: e.name,
      title: e.title,
      address: e.address,
      specialisation: e.specialisation,
      phone: e.phone
    });
  };
  //update
  updateAllery = () => {
    const { name, title, address, specialisation, phone } = this.state;
    const { userInfo } = this.props.loginDetails;

    const updatedoctor = {
      uid: userInfo.userId,
      myDoctors: [
        {
          name: name,
          title: title,
          address: address,
          specialisation: specialisation,
          phone: phone
        }
      ]
    };

    this.props.updatedoctorData(updatedoctor);
    this.setState({ editallergies: false });
  };

  deletePatient = e => {
    const { userInfo } = this.props.loginDetails;

    const deletedoctor = {
      uid: userInfo.userId,
      myDoctors: [
        {
          name: e.name,
          title: e.title,
          address: e.address,
          specialisation: e.specialisation,
          phone: e.phone
        }
      ]
    };
    this.props.deletedoctorData(deletedoctor);
  };
  handleAllergy = () => {
    const { name, title, address, specialisation, phone } = this.state;
    const { userInfo } = this.props.loginDetails;

    const adddoctor = {
      uid: userInfo.userId,
      myDoctors: [
        {
          name: name,
          title: title,
          address: address,
          specialisation: specialisation,
          phone: phone
        }
      ]
    };

    this.props.adddoctorData(adddoctor);
    this.setState({
      addallergies: false
    });
  };
  emptystate = () => {
    this.setState({
      name: "",
      title: "",
      address: "",
      phone: "",
      specialisation: ""
    });
  };

  render() {
    const {
      tabone,
      tabtwo,
      addallergies,
      name,
      title,
      editallergies,
      address,
      phone,
      specialisation,
      education, formValid
    } = this.state;
    const { myDoctors } = education;
    return (
      <React.Fragment>
        <div className="mainMenuSide" id="main-content">
          <div className="wrapper">
            <div className="container-fluid">
              <div className="body-content">
                <div className="tomCard">
                  <div className="tomCardHead">
                    <h5 className="float-left">MyDoctor </h5>
                  </div>
                  <div className="tab-menu-content">
                    <div class="tab-menu-title">
                      <ul>
                        <li className={this.state.tabone ? "menu1 active" : ""}>
                          <a href="#" onClick={this.tabOne}>
                            <p>List of Doctors</p>
                          </a>
                        </li>
                        <li className={this.state.tabtwo ? "menu1 active" : ""}>
                          <a href="#" onClick={this.tabTwo}>
                            <p>Attached Files</p>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  {tabone && (
                    <div className="tab-menu-content-section">
                      <div
                        id="content-1"
                        className={tabone ? "d-block" : "d-none"}
                      >
                        <button
                          type="button"
                          onClick={this.addallergiespop}
                          className="commonBtn2 float-right mb-2"
                        >
                          <i className="fa fa-plus" /> Add New Entry
                        </button>
                        <div className="table-responsive">
                          <table className="table table-hover">
                            <thead className="thead-default">
                              <tr>
                                <th>Title</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {myDoctors && myDoctors.length > 0 ? (
                                myDoctors.map((each, i) => (
                                  <tr key={i}>
                                    <td data-label="startDate">{each.title}</td>
                                    <td data-label="Action">
                                      <a
                                        title="Edit"
                                        className="editBtn"
                                        onClick={e => this.modalPopupOpen(each)}
                                      >
                                        <span id={5} className="fa fa-edit" />
                                      </a>
                                      <a
                                        title="Delete"
                                        className="deleteBtn"
                                        onClick={e => this.deletePatient(each)}
                                      >
                                        <i
                                          className="fa fa-trash"
                                          data-toggle="modal"
                                          data-target="#myModal2"
                                        />
                                      </a>
                                    </td>
                                  </tr>
                                ))
                              ) : (
                                  <tr>No data available</tr>
                                )}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  )}

                  {tabtwo && (
                    <div
                      id="content-2"
                      className={tabtwo ? "d-block" : "d-none"}
                    >
                      <button
                        type="button"
                        className="commonBtn2 float-right mb-2"
                        onClick={() =>
                          this.setState({
                            uploadPopup: !this.state.uploadPopup
                          })
                        }
                      >
                        <i className="fa fa-plus" />
                        <span> Upload new files</span>                </button>
                      <div className="table-responsive">
                        <table className="table table-hover">
                          <thead className="thead-default">
                            <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>FileName</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.props.getFilesDetails.fileDetails &&
                              this.props.getFilesDetails.fileDetails.map(
                                (each, i) => (
                                  <tr>
                                    <td data-label="#">{i + 1}</td>
                                    <td data-label="Name">{each.name}</td>
                                    <td data-label="FileName"> {each.fileName}</td>

                                    <td data-label="Action">
                                      <button
                                        class="download_btn"
                                        onClick={() =>
                                          this.handleAwsFiles(each.fileUrl)
                                        }
                                      >
                                        <i class="fas fa-download" />
                                      </button>
                                      <button
                                        class="download_btn"
                                        onClick={() =>
                                          this.props.handleFileDelete(each.id, each.fileUrl, "mydoctor")
                                        }
                                      >
                                        <i class="fas fa-trash" />
                                      </button>
                                    </td>
                                  </tr>
                                )
                              )}

                          </tbody>
                        </table>
                      </div>
                    </div>
                  )}
                </div>

                <div
                  className={addallergies ? "modal d-block" : "modal"}
                  id="myModal"
                >
                  <div class="modal-dialog">
                    <div class="modal-content modalPopUp">
                      <div class="modal-header borderNone">
                        <h5 class="modal-title">Add Doctor</h5>
                        <button
                          type="button"
                          className="close"
                          data-dismiss="modal"
                          onClick={this.modalPopUpClose}
                        >
                          <i class="fas fa-times" />
                        </button>
                      </div>
                      <div class="modal-body">
                        <form>
                          <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Title for list entry (e.g. Dentist)<sup>*</sup>
                                </label>
                                <input
                                  className="form-control"
                                  name="title"
                                  onChange={this.handleChange}
                                  value={title}
                                  type="text"
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">Name</label>
                                <input
                                  className="form-control"
                                  name="name"
                                  onChange={this.handleChange}
                                  value={name}
                                  type="text"
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Address (Street, ZIP/Postcode, City)
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="address"
                                  onChange={this.handleChange}
                                  type="text"
                                  value={address}
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Phone Number
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="phone"
                                  onChange={this.handleChange}
                                  type="text"
                                  value={phone}
                                  onKeyPress={onlyNumbers} maxLength="15"
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div className="form-group">
                                <label>Specialisation</label>
                                <select
                                  className="form-control"
                                  name="specialisation"
                                  value={this.state.specialisation}
                                  onChange={this.handleChange}
                                >
                                  <option value="saw2">-- Please Select--</option>
                                  <option value="Anesthesia">Anesthesia</option>
                                  <option value="Occupational Medicine">
                                    Occupational Medicine
                                  </option>
                                  <option value="Orthopedics">
                                    Orthopedics
                                  </option>
                                  <option value="Psychiatry">Psychiatry</option>
                                  <option value="Surgery">Surgery</option>
                                  <option value="Pathology">Pathology</option>
                                  <option value="Rheumatology">
                                    Rheumatology
                                  </option>
                                  <option value="Radiology">Radiology</option>
                                  <option value="Urology">Urology</option>
                                  <option value="Dentist">Dentist</option>
                                  <option value="General Medicine">
                                    General Medicine
                                  </option>
                                  <option value="Diabetology">
                                    Diabetology
                                  </option>
                                  <option value="Internal Medicine">
                                    Internal Medicine
                                  </option>
                                  <option value="Dialysis">Dialysis</option>
                                  <option value="Dermatology">
                                    Dermatology
                                  </option>
                                  <option value="Aviation medicine">
                                    Aviation medicine
                                  </option>
                                  <option value="Obstetrics/Gynecology">
                                    Obstetrics/Gynecology
                                  </option>
                                  <option value="Hematology/Oncology">
                                    Hematology/Oncology
                                  </option>
                                  <option value="ENT">ENT</option>
                                  <option value="Pulmology">Pulmology</option>
                                  <option value="Cardiology">Cardiology</option>
                                  <option value="Pediatrics">Pediatrics</option>
                                  <option value="Nephrology">Nephrology</option>
                                  <option value="Neurology">Neurology</option>
                                  <option value="Neurosurgery">
                                    Neurosurgery
                                  </option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <button
                                type="button"
                                class="commonBtn float-right"
                                onClick={this.handleAllergy} disabled={!formValid}
                              >
                                Add
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  className={editallergies ? "modal d-block" : "modal"}
                  id="myModal"
                >
                  <div class="modal-dialog">
                    <div class="modal-content modalPopUp">
                      <div class="modal-header borderNone">
                        <h5 class="modal-title">Edit Doctor</h5>
                        <button
                          type="button"
                          className="close"
                          onClick={this.modalPopUpClose}
                        >
                          <i class="fas fa-times" />
                        </button>
                      </div>
                      <div class="modal-body">
                        <form>
                          <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Title for list entry (e.g. Dentist)
                                </label>
                                <input
                                  className="form-control"
                                  name="title"
                                  onChange={this.handleChange}
                                  defaultValue={this.state.editDetails.title} readOnly
                                  type="text"
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">Name</label>
                                <input
                                  className="form-control"
                                  name="name"
                                  onChange={this.handleChange}
                                  defaultValue={this.state.editDetails.name}
                                  type="text"
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Address (Street, ZIP/Postcode, City)
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="address"
                                  onChange={this.handleChange}
                                  type="text"
                                  defaultValue={this.state.editDetails.address}
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">Phone</label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="phone"
                                  onChange={this.handleChange}
                                  type="text"
                                  onKeyPress={onlyNumbers}
                                  defaultValue={this.state.editDetails.phone} maxLength="15"
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div className="form-group">
                                <label>Specialisation</label>
                                <select
                                  className="form-control"
                                  name="specialisation"
                                  value={this.state.specialisation}
                                  onChange={this.handleChange}
                                >
                                  <option>-- Please Select--</option>
                                  <option value="Anesthesia">Anesthesia</option>
                                  <option value="Occupational Medicine">
                                    Occupational Medicine
                                  </option>
                                  <option value="Orthopedics">
                                    Orthopedics
                                  </option>
                                  <option value="Psychiatry">Psychiatry</option>
                                  <option value="Surgery">Surgery</option>
                                  <option value="Pathology">Pathology</option>
                                  <option value="Rheumatology">
                                    Rheumatology
                                  </option>
                                  <option value="Radiology">Radiology</option>
                                  <option value="Urology">Urology</option>
                                  <option value="Dentist">Dentist</option>
                                  <option value="General Medicine">
                                    General Medicine
                                  </option>
                                  <option value="Diabetology">
                                    Diabetology
                                  </option>
                                  <option value="Internal Medicine">
                                    Internal Medicine
                                  </option>
                                  <option value="Dialysis">Dialysis</option>
                                  <option value="Dermatology">
                                    Dermatology
                                  </option>
                                  <option value="Aviation medicine">
                                    Aviation medicine
                                  </option>
                                  <option value="Obstetrics/Gynecology">
                                    Obstetrics/Gynecology
                                  </option>
                                  <option value="Hematology/Oncology">
                                    Hematology/Oncology
                                  </option>
                                  <option value="ENT">ENT</option>
                                  <option value="Pulmology">Pulmology</option>
                                  <option value="Cardiology">Cardiology</option>
                                  <option value="Pediatrics">Pediatrics</option>
                                  <option value="Nephrology">Nephrology</option>
                                  <option value="Neurology">Neurology</option>
                                  <option value="Neurosurgery">
                                    Neurosurgery
                                  </option>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <button
                                type="button"
                                class="commonBtn float-right"
                                onClick={this.updateAllery}
                              >
                                Update
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div
            className={this.state.uploadPopup ? "modal d-block" : "modal"}
            id="myModal"
          >
            <div class="modal-dialog">
              <div class="modal-content modalPopUp">
                <div class="modal-header borderNone">
                  <h5 class="modal-title">Upload File</h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    onClick={() =>
                      this.setState({ uploadPopup: !this.state.uploadPopup })
                    }
                  >
                    <i class="fas fa-times" />
                  </button>
                </div>
                <div class="modal-body">
                  <form>
                    <div class="row">
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="form-group">
                          <label className="form-label">File name</label>
                          <input

                            className="form-control"
                            type="text"
                            id="name"
                            name="name"
                            value={this.state.name}
                            onChange={this.handleChange}
                          />
                          <span />
                        </div>
                      </div>
                      <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="fileUp">
                          <label
                            for="fileUp1"
                            class="commonBtn text-center upload"
                          >
                            Upload File
                          </label>
                          <input
                            type="file"
                            id="fileUp1"
                            onChange={this.handleChangePopup}
                            style={{ display: "none" }}
                          />
                        </div>
                      </div>

                      <div class="col-md-12 col-lg-12 col-sm-12 mt-2">
                        <button
                          type="button"
                          class="commonBtn float-right"
                          onClick={this.popupSubmit} disabled={!this.state.fileURL}
                        >
                          Submit
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  getpersonalDetails: state.healthrecordReducer.getpersonalDetails,
  loginDetails: state.loginReducer.loginDetails,
  getFilesDetails: state.uploadReducer.getFilesDetails
});

const mapDispatchToProps = dispatch => ({
  adddoctorData: adddoctor => dispatch(adddoctorData(adddoctor)),
  deletedoctorData: deletedoctor => dispatch(deletedoctorData(deletedoctor)),
  updatedoctorData: updatedoctor => dispatch(updatedoctorData(updatedoctor)),
  getPersonaldata: listpersonal => dispatch(getPersonaldata(listpersonal)),
  getUploadFiles: payload => dispatch(getUploadFiles(payload)),
  handleFileDelete: (id, fileUrl, type) => dispatch(handleFileDelete(id, fileUrl, type))

});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Mydoctor);
