import actionType from "../medicalSpecialist/actionType";
import URL from "../../asset/configUrl";
import axios from "axios";

export const getClinic = () => dispatch => {
  axios
    .get(URL.GET_SPECIALIST)
    .then(res => {
      if (res.data.status == "success") {
        dispatch({
          type: actionType.SEARCHCLINIC_SUCCESS,
          payload: res.data
        });
      } else if (res.data.status == "failure") {
        // toastr.error('error', "Please Fill Valid Details", toastrOptions);
      }
    })
    .catch(error => {
      dispatch({
        type: actionType.SEARCHCLINIC_SUCCESS,
        error: error.response
      });
    });
};

export const getClinicDoctor = () => dispatch => {
  const configs = {
    method: "post",
    url: URL.GET_DOCTORLIST,
    data: {
      doctorId: 2
    },
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(configs)
    .then(res => {
      if (res.data.status == "success") {
        dispatch({
          type: actionType.CLINICDOCTOR_SUCCESS,
          payload: res.data
        });
      } else if (res.data.status == "failure") {
        // toastr.error('error', "Please Fill Valid Details", toastrOptions);
      }
    })
    .catch(error => {
      dispatch({
        type: actionType.SEARCHCLINIC_SUCCESS,
        error: error
      });
    });
};

export const getSearchDoctorDetails = searchdata => dispatch => {
  const config = {
    method: "post",
    url: URL.GET_SEARCH_DOC_DETAILS,
    data: searchdata,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };
  axios(config)
    .then(res => {
      if (res.data.status == "success") {
        dispatch({
          type: actionType.SEARCH_DOCTOR_DETAILS_SUCCESS,
          payload: res.data
        });
        // toastr.success('Message', res.data.message, toastrOptions);
      } else if (res.data.status == "failure") {
        // toastr.error('error', "Please Fill Valid Details", toastrOptions);
      }
    })
    .catch(error => {
      dispatch({
        type: actionType.SEARCH_DOCTOR_DETAILS_FAILURE,
        error: error
      });
      // toastr.error("errr", toastrOptions)
    });
};
