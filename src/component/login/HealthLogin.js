
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { userLogin } from '../../service/login/action'
import SocialButton from '../common/SocialButton'

import Registration from '../registration/registration';
import browserHistory from '../../view/App';
import { NavLink } from "react-router-dom";
import { toastr } from "react-redux-toastr";



class HealthLogin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      submitted: false,
      errors: {
        email: "",
        password: ""
      },
      emailValid: false,
      passwordValid: false,
      formValid: false,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }
  handleSubmit(e) {
    e.preventDefault();
    let userinfo = {
      email: this.state.email,
      password: this.state.password
    };

    this.props.userLogin(userinfo);
  }

  componentWillReceiveProps = nextProps => {
    console.log("nextprops", nextProps)
    if (nextProps.loginDetails.userInfo && nextProps.loginDetails.status == "success") {
      const { userInfo } = nextProps.loginDetails;
      console.log("userInfo", userInfo);
        if (userInfo.userType === "doctor") {
        this.props.history.push("/doctormainmenu");
      }else if(userInfo.userType === "hospital"){
        this.props.history.push("/hospitalmainmenu");
      }else
      {
          toastr.error("Error","Sorry this Doctor login");
      }
    }
  }

  handleSocialLogin = (user) => {
    console.log("handleSocialLogin : ", user)
    const socialUserInfo = {
      provider: user.provider,
      accessToken: user.token.accessToken,
      email: user.profile.email

    }
  }

  handleSocialLoginFailure = (err) => {
    console.error("handleSocialLoginFailure : ", err)
  }
  handleSocialLogout = (user) => {
    console.log("handleSocialLogout : ", user)
  }

  handleSocialLogoutFailure = (err) => {
    console.error("handleSocialLogoutFailure : ", err)
  }
  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  }
  // handleSubmit(e) {
  //   e.preventDefault();

  //   let userinfo = {
  //     email: this.state.email,
  //     password: this.state.password
  //   };
  //   this.props.userLogin(userinfo);
  // }
  
  //validation
  validateField(fieldName, value) {
    const { errors, emailValid, passwordValid } = this.state;
    let emailvalid = emailValid;
    let passwordvalid = passwordValid;
    let fieldValidationErrors = errors;
    switch (fieldName) {
      case "email":
        emailvalid = value.match(
          /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i
        );
        fieldValidationErrors.email = emailvalid
          ? ""
          : "Please enter valid email address.";
        break;
      case "password":
        passwordvalid = value.length !== 0
        fieldValidationErrors.password = passwordvalid
          ? ""
          : "Please enter valid password.";
        break;

      default:
        break;
    }
    this.setState({
      errors: fieldValidationErrors, emailValid: emailvalid, passwordValid: emailvalid
    })
  }
  render() {
    const { email, password, errors } = this.state;
    console.log("Hey I am from state ..:", this.props)
    return (
      <section className="loginSection">
        <a href="/" className="logoDiv"><img src="../images/logo.jpg" /></a>
        <div className="loginInner">
          <div className="loginFormContent hideSidebar">
            <h4 className="popupTitle">Login</h4>
            <form onSubmit={this.handleSubmit}>
              <div className="form-group emailIcon">
                <input
                  type="text"
                  className="form-control commonInput"
                  placeholder="Email"
                  name="email"
                  value={email}
                  onChange={this.handleChange}
                />
                <div style={{ color: "red" }}>{errors.email}</div>

              </div>
              <div className="form-group pass">
                <input
                  type="password"
                  className="form-control commonInput"
                  placeholder="Password"
                  name="password"
                  value={password}
                  onChange={this.handleChange}
                />
                <div style={{ color: "red" }}>{errors.password}</div>

              </div>
              <div className="form-group ">
                <NavLink to="/login/forgot" className="forgotPass text-right">
                  Forgot Password ?
                  </NavLink>
              </div>
              <div className="form-group">
                <a href="#">
                  <button
                    type="submit"
                    className="btn loginBtn"

                  >
                    Login
                    </button>
                </a>
              </div>
              <p className="linkToReg">
                Don't have an account ?
                  <NavLink to="/register/patient"> Register Here</NavLink>
              </p>
            </form>
          </div>


        </div>
      </section>


    )
  }
}
const mapStateToProps = state => ({
  loginDetails: state.loginReducer.loginDetails,
  userLoginStatus: state.loginReducer.userLoginStatus

});

const mapDispatchToProps = dispatch => ({
  userLogin: (userinfo) => dispatch(userLogin(userinfo)),

});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HealthLogin);


