import React, { Component } from "react";
import { connect } from "react-redux";
import { getPatient , approvePatient } from "../../../service/admin/Patient/action";
import { filter as _filter } from 'lodash';

class Patients extends Component {
    constructor(props) {
        super(props);
        this.state = {
            patientList: [],
            nameSearch: ""
        }
    }
    componentWillMount() {
        this.props.getPatient();
    }
    componentWillReceiveProps(nextProps) {

        if (nextProps.adminPatientList && nextProps.adminPatientList.patientList.length > 0) {
            const { patientList } = nextProps.adminPatientList;
            this.setState({ patientList: patientList, mainPatientList: patientList })
        }
    }
    handleNameSearch = (e) => {
        e.preventDefault();
        const { mainPatientList } = this.state;

        this.setState({ [e.target.name]: e.target.value })
        if (!e.target.value) {
            this.setState({ patientList: mainPatientList })
        }
    }
    handleSearch = (e) => {
        e.preventDefault();
        var result = [];
        const { nameSearch, mainPatientList } = this.state;
        result = _filter(mainPatientList, function (list) {

            if (list.firstName != "" || list.firstName != undefined) {
                return list.firstName.includes(nameSearch);
            }

        })
        this.setState({ patientList: result })
    }
    handleApprovePatient = (each, status) => {
       const payload = {
            "uid": each.id,
            "approval": status,
            "userName": "Username"
        }
        this.props.approvePatient(payload);
    }

    render() {
        const { patientList } = this.state;
        console.log(patientList)
        return (
            <div className="mainMenuSide" id="main-content">
                <div className="wrapper">
                    <div className="container-fluid">
                        <div className="body-content">
                            <div class="search-section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                                            <div class="search-doctor">
                                                <input type="text" placeholder="Patient Name" name="nameSearch" onChange={this.handleNameSearch} />
                                                <i class="fas fa-user search-icon "></i>
                                            </div>
                                        </div>
                                        <div class="search-doctor-btn col-lg-2 col-12">
                                            <button class="searchDocBtn" disabled="" onClick={this.handleSearch}>Search</button>
                                        </div>
                                    </div></div></div>
                            <div className="search-result-section">
                                {patientList ? patientList.map((each, i) => (
                                    <div key={i} className="search-card2">
                                        <img src="https://images.pexels.com/photos/1239291/pexels-photo-1239291.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=650&w=940" />
                                        <div className="doc-title">
                                            <p className="title-name"> {each.firstName} </p>
                                            <p> {each.mobileNo}</p>

                                        </div>
                                        <div className="patientView">
                                            <button onClick={() => this.props.history.push({ pathname: '/admindashboard', state: { submenuType: "ViewProfile", patientId: each.id } })}><i className="far fa-eye"></i></button>
                                            {each.approvedStatus == "approved" && <button title="Click to reject" className="RejectBtn" onClick={(e) => this.handleApprovePatient(each, "pending")}><i className="fas fa-times"></i>
                                            </button>}
                                            {each.approvedStatus == "pending" && <button title="Click to approve" className="approveBtn" onClick={(e) => this.handleApprovePatient(each, "approved")}><i className="fas fa-check"></i>
                                            </button>}
                                        </div>
                                    </div>)) : <div className="search-card2"><div className="doc-title">
                                        <p className="title-name">No patient list </p>


                                    </div> </div>}
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        )
    }
}
const mapStateToProps = state => ({
    adminPatientList: state.adminPatientReducer.adminPatientList,
    loginDetails: state.loginReducer.loginDetails
})
const mapDispatchToProps = dispatch => ({
    getPatient: () => dispatch(getPatient()),
    approvePatient: (payload) => dispatch(approvePatient(payload))
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Patients);