import React, { Component } from "react";
import { connect } from "react-redux";
import AWS from "aws-sdk";
import axios from 'axios'
import URL from '../../asset/configUrl';
class HospitalMainMenu extends Component {
    constructor(props) {
        super(props);
        var loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
        const { userInfo } = loginDetails;
        this.state = {
            profilePic: userInfo.profilePic
        }
    }
    handleLogout = () => {

        const loginDetails = JSON.parse(sessionStorage.getItem("loginDetails"))
        const configs = {
            method: 'post',
            url: URL.USER_LOGOUT,
            data: {
                "uid": loginDetails.userInfo.userId,
            },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }
        axios(configs)
            .then(response => {
                sessionStorage.clear("loginDetails");
                this.props.history.push("/");
            })

    }
    componentWillMount() {
        const { profilePic } = this.state;
        AWS.config.update({
            accessKeyId: process.env.REACT_APP_ACCESSKEY,
            secretAccessKey: process.env.REACT_APP_SECRETACCESSKEY,
            region: process.env.REACT_APP_REGION
        });
        var s3 = new AWS.S3();
        var data = {
            Bucket: "tomato-medical-bucket",
            Key: `profilepicture/${profilePic}`
        };
        s3.getSignedUrl("getObject", data, async (err, data) => {
            if (err) {
                console.log("Error uploading data: ", err);
            } else {
                console.log("succesfully uploaded the image!", data);

                (await data) && this.setState({ img: data });
            }
        });
    }
    render() {
        const { loginDetails } = this.props;
        return (
            <div className="inner-pageNew">

                <header className="header">
                    <div className="brand">
                        <a href="javascript:void(0);" className="logo">
                            <img src="../images/logo.jpg" /> </a>

                    </div>
                    <div className="top-nav" style={{ display: "none" }}>

                        <div className="searchBox float-right " >

                            <input type="text" className="searchTxt" placeholder="Search Symptom Checker" />
                            <a href="#" className="searchBtn">
                                <i className="fas fa-search"></i>
                            </a>
                        </div>

                    </div>
                    <div className="languageSel text-right mb-2" style={{ marginTop: "20px", marginRight: "10px" }}>
                        <span>Language :</span>
                        <select className="">
                            <option>EN</option>
                        </select>
                    </div>
                </header>

                <div className="body-content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-3 col-md-3 col-sm-12 col-12 bg-purple-card">
                                <div className="card user-profile-card">

                                    <div className="row">
                                        <div className="col-12">
                                            <img src={this.state.img} />
                                            <h5>{loginDetails.userInfo && loginDetails.userInfo.userType}</h5>
                                            <p>{loginDetails.userInfo && loginDetails.userInfo.firstName}</p>
                                        </div>

                                    </div>

                                    <div className="set-section">
                                        <ul>
                                            <li><a href=""><i className="fas fa-cog"></i></a></li>
                                            <li><a href="../Subscription.html"><i className="fas fa-shopping-cart"></i></a></li>
                                            <li><a href="javascript:void(0);" onClick={this.handleLogout}><i className="fas fa-power-off"></i></a></li>
                                        </ul>
                                    </div>

                                </div>

                            </div>

                            <div className="col-lg-9 col-md-9 col-sm-12 col-12">
                                <div className="row">

                                    <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                                        <a href="javascript:void(0);" onClick={() => this.props.history.push({ pathname: '/hospitaldashboard', state: { submenuType: "DoctorList" } })} className="card bg-c">
                                            <div className="card-img1">
                                                <img src="../images/doctors-color.png" />
                                            </div>
                                            <div className="card-title">
                                                <h5>Doctor's</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div className="col-lg-3 col-md-3 col-sm-6 col-6 padLeft">
                                        <a href="javascript:void(0);" onClick={() => this.props.history.push({ pathname: '/hospitaldashboard', state: { submenuType: "PatientList" } })} className="card bg-c">
                                            <div className="card-img1">
                                                <img src="../images/personal-color.png" />
                                            </div>
                                            <div className="card-title">
                                                <h5>Patient's</h5>
                                            </div>
                                        </a>
                                    </div>

                                    <div className="col-lg-3 col-md-3 col-sm-6 col-6 padRig">
                                        <a href="javascript:void(0);" onClick={() => this.props.history.push({ pathname: '/hospitaldashboard', state: { submenuType: "BookedAppointment" } })} className="card bg-c">
                                            <div className="card-img1">
                                                <img src="images/Booking.png" />
                                            </div>
                                            <div className="card-title">
                                                <h5>Booked Appointment</h5>
                                            </div>
                                        </a>
                                    </div>
                                    <div className="col-lg-3 col-md-3 col-sm-6 col-6 padLeft">
                                        <a href="javascript:void(0);" onClick={() => this.props.history.push({ pathname: '/hospitaldashboard', state: { submenuType: "DoctorAvailability" } })} className="card bg-c">
                                            <div className="card-img1">
                                                <img src="images/appointment.png" />
                                            </div>
                                            <div className="card-title">
                                                <h5>Doctors Availablity</h5>
                                            </div>
                                        </a>
                                    </div>

                                    <div className="col-lg-3 col-md-3 col-sm-6 col-6 padLeft">
                                        <a href="javascript:void(0);" onClick={() => this.props.history.push({ pathname: '/hospitaldashboard', state: { submenuType: "Hospitaladmininfo" } })} className="card bg-c">
                                            <div className="card-img1">
                                                <img src="images/appointment.png" />
                                            </div>
                                            <div className="card-title">
                                                <h5>Hospital Admin Info</h5>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>




            </div>
        )
    }
}
const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails
})

const mapDispatchToprops = dispatch => ({
})
export default connect(
    mapStateToProps,
    mapDispatchToprops
)(HospitalMainMenu);