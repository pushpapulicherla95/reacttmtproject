const { mailOptions, transporter } = require("../email/email.api");
const schedule = require('node-schedule');
const  moment =  require("moment");

const {
  registerSuccess,
  registerSuccessHealthCare,
  forgetPassword,
  bookingAppoinment
} = require("../email/htmltemplate");

const sendRegisterMail = async (req, res) => {
  const { email, firstName, healthCare } = req.body;
  await registerSuccess({ firstName: firstName , healthCare}).then(maildata => {
    //mail sending
    let mailOptions_info = {
      from: mailOptions.from,
      to: email,
      subject: "Tomato Medical Registration",
      html: maildata
    };

    // console.log("mailOptions_info", mailOptions_info);
    transporter.sendMail(mailOptions_info, (error, info) => {
      if (error) {
        res.status(400).json({
          status: "failur",
          message: "Mail sending error",
          data: error
        });
      } else {
        res.status(200).json({
          status: "success",
          message: "Registration mail sent successfully",
          data: ""
        });
      }
    });
  });
};

const sendRegisterMailHealthCare = async (req, res) => {
  const { email, firstName } = req.body;
  await registerSuccessHealthCare({ firstName: firstName }).then(maildata => {
    //mail sending
    let mailOptions_info = {
      from: mailOptions.from,
      to: email,
      subject: "Tomato Medical Registration",
      html: maildata
    };

    // console.log("mailOptions_info", mailOptions_info);
    transporter.sendMail(mailOptions_info, (error, info) => {
      if (error) {
        res.status(400).json({
          status: "failur",
          message: "Mail sending error",
          data: error
        });
      } else {
        res.status(200).json({
          status: "success",
          message: "Registration mail sent successfully",
          data: ""
        });
      }
    });
  });
};


const sendForgetMail = async (req, res) => {

  const { token, email } = req.body;
  await forgetPassword(token).then(response => {
    let mailOptions_info = {
      from: mailOptions.from,
      to: email,
      subject: "Tomato Medical OTP ",
      html: response
    };
    // console.log("mailOptions_info", mailOptions_info);
    transporter.sendMail(mailOptions_info, (error, info) => {
      if (error) {
        res.status(400).json({
          status: "failur",
          message: "Mail sending error",
          data: error
        });
      } else {
        res.status(200).json({
          status: "success",
          message: "OTP has been sent to your registered mail Id successfully",
          data: ""
        });
      }
    });
  });
};

const sendBookingAppoinmentMail = (req, res) => {
  const { email } = req.body;

  bookingAppoinment(req.body).then(response => {

    response.map((mailData) => {
      let mailOptions_info = {
        from: mailOptions.from,
        to: mailData.email,
        subject: "Tomato Medical Booking Appointment",
        html: mailData.template
      };

      transporter.sendMail(mailOptions_info, (error, info) => {
      });

      const { appointmentDate, startTime } = mailData;
      const appointmentDateMoment = moment(appointmentDate, 'YYYY-MM-DD')
      const day = appointmentDateMoment.format('DD')
      const month = appointmentDateMoment.format('MM')
      const hours = moment(startTime, "hh:mm").format("HH")
      const minutes = moment(startTime, "hh:mm").subtract(5, "minutes").format("mm")
      const weekDay = appointmentDateMoment.day()

      schedule.scheduleJob(`0 ${minutes} ${hours} ${day} ${month} ${weekDay}`, () => {
        let mailOptions_info = {
          from: mailOptions.from,
          to: mailData.email,
          subject: "Tomato Medical Booking Appointment",
          html: mailData.remaniderTemplate
        };
        transporter.sendMail(mailOptions_info, (error, info) => {
        });
      });
    })
    res.status(200).json({
      status: "success",
      message: "Appoinment Booked successfully",
      data: ""
    });
  }
  );
};

module.exports = {
  sendRegisterMail,
  sendForgetMail,
  sendBookingAppoinmentMail,
  sendRegisterMailHealthCare
};
