import React from 'react';
import { connect } from "react-redux";
import axios from 'axios';
import URL from '../../../asset/configUrl'
class ShareDataDownload extends React.Component {

    state = {
        submenuType: "Personal",
        shareDataList: []
    }
    handleArrow = () => {
        this.props.history.push("/mainmenu");
    }
    componentWillMount = () => {
        this.shareDataDownload()
    }

    shareDataDownload = () => {
        let parameters = new URLSearchParams(window.location.search)
        let uid = parameters.get("uid")
        let token = parameters.get("token")
        axios.get(URL.FILELINK_SHAREDATA + uid + "/" + token)
            .then(response => this.setState({ shareDataList: response.data.fileNames }))
    }

    render() {
        const { submenuType } = this.props.location.state || this.state;
        const { userInfo } = this.props.loginDetails;
        return (
            <div className="inner-pageNew">
                <section>
                    <header className="header">
                        <div className="brand">
                            <a href="javascript:void(0)" className="backArrow" onClick={this.handleArrow}>
                                <i className="fas fa-chevron-left" ></i>
                            </a>
                            <div className="mobileLogo">
                                <a href="javascript:void(0)" onClick={() => this.props.history.push({ pathname: 'mainmenu' })} className="logo">
                                    <img src="images/logo.jpg" /> </a></div>

                        </div>
                        <div className="top-nav">

                            <div className="dropdown float-right mr-2" >
                                {userInfo && userInfo.firstName != "" && <a className="dropdown-toggle" data-toggle="dropdown">
                                    {userInfo.firstName}
                                </a>}
                                <div className="dropdown-menu">
                                    <a className="dropdown-item" href="javascript:void(0);" onClick={() => this.props.history.push({ pathname: '/login/patient' })}>Logout</a>
                                </div>
                            </div>
                        </div>
                    </header>
                </section>

                <div class="wrapper">
                    <div class="container-fluid">
                        <div class="body-content">
                            {/* <div className="download_share_file">
                                    <div className="download_share_file_card">
                                        <i class="fas fa-cloud-download-alt"></i>
                                    </div>
                                </div> */}
                            <div class="file_list_share">
                                {this.state.shareDataList.map((each) => <div class="file_list_card">
                                    <div class="file_format"> <img src="images/pdf_icon.png" />
                                        <div class="share_option d-none " >
                                            <ul>
                                                <li><button><i class="fas fa-cloud-download-alt"></i> Download</button></li>
                                                <li><button><i class="fas fa-share-alt"></i> Share</button></li>
                                                <li><button><i class="fas fa-trash"></i> Remove</button></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="file_details">
                                        <label>{each}</label>
                                        <div class="menu">
                                            <a href="javascript:void(0);">  <i class="fas fa-cloud-download-alt"></i> Download</a>
                                        </div>
                                    </div>
                                </div>
                                )}

                            </div>
                        </div></div></div>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    loginDetails: state.loginReducer.loginDetails
});

const mapDispatchToProps = dispatch => ({
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ShareDataDownload);
// export default ShareDataDownload