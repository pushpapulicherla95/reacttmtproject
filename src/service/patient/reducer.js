
import actionType from '../patient/actionType';

const initialState = {
    patientHealthList: '',
    addaccidentlist: "",
    getaccidentlist: "",
    updateaccidentlist: "",
    deleteaccidentlist: "",
    addfindinglist: "",
    getfindinglist: "",
    updatefindinglist: "",
    deletefindinglist: "",
    addtherapylist: "",
    gettherapylist: "",
    updatetherapylist: "",
    deletetherapylist: "",


}
const patientReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.ADD_PATIENT_HEALTH_RECORD_SUCCESS:
            return {
                ...state,
                addpatientDetails: action.payload,

            }
        case actionType.ADD_PATIENT_HEALTH_RECORD_FAILURE:
            return {
                ...state,
                addpatientDetails: action.payload,

            }
        case actionType.UPDATE_PATIENT_HEALTH_RECORD_SUCCESS:
            return {
                ...state,
                updatepatientDetails: action.payload,

            }
        case actionType.UPDATE_PATIENT_HEALTH_RECORD_FAILURE:
            return {
                ...state,
                updatepatientDetails: action.payload,

            }
        case actionType.GET_PATIENT_HEALTH_RECORD_SUCCESS:
            return {
                ...state,
                patientHealthList: action.payload
            }
        case actionType.GET_PATIENT_HEALTH_RECORD_FAILURE:
            return {
                err: action.error
            }
        case actionType.DELETE_PATIENT_HEALTH_RECORD_SUCCESS:
            return {
                ...state
            }
        case actionType.DELETE_PATIENT_HEALTH_RECORD_FAILURE:
            return {
                error: action.error
            }
        case actionType.ADD_ACCIDENT_SUCCESS:
            return {
                ...state,
                addaccidentlist: action.payload
            }
        case actionType.ADD_ACCIDENT_FAILURE:
            return {
                addaccidentlist: action.payload
            }
        case actionType.GET_ACCIDENT_SUCCESS:
            return {
                ...state,
                getaccidentlist: action.payload
            }
        case actionType.GET_ACCIDENT_FAILURE:
            return {
                getaccidentlist: action.payload
            }
        case actionType.UPDATE_ACCIDENT_SUCCESS:
            return {
                ...state,
                updateaccidentlist: action.payload
            }
        case actionType.UPDATE_ACCIDENT_FAILURE:
            return {
                updateaccidentlist: action.payload
            }
        case actionType.DELETE_ACCIDENT_SUCCESS:
            return {
                ...state,
                deleteaccidentlist: action.payload
            }
        case actionType.DELETE_ACCIDENT_FAILURE:
            return {
                deleteaccidentlist: action.payload
            }
            case actionType.ADD_FINDING_SUCCESS:
            return {
                ...state,
                addfindinglist: action.payload
            }
        case actionType.ADD_FINDING_FAILURE:
            return {
                addfindinglist: action.payload
            }
            case actionType.GET_FINDING_SUCCESS:
            return {
                ...state,
                getfindinglist: action.payload
            }
        case actionType.GET_FINDING_FAILURE:
            return {
                getfindinglist: action.payload
            }
            case actionType.UPDATE_FINDING_SUCCESS:
            return {
                ...state,
                updatefindinglist: action.payload
            }
        case actionType.UPDATE_FINDING_FAILURE:
            return {
                updatefindinglist: action.payload
            }
            case actionType.DELETE_FINDING_SUCCESS:
            return {
                ...state,
                deletefindinglist: action.payload
            }
        case actionType.DELETE_FINDING_FAILURE:
            return {
                deletefindinglist: action.payload
            }
            //therapy
            case actionType.ADD_THERAPY_SUCCESS:
            return {
                ...state,
                addtherapylist: action.payload
            }
        case actionType.ADD_THERAPY_FAILURE:
            return {
                addtherapylist: action.payload
            }
        case actionType.GET_THERAPY_SUCCESS:
            return {
                ...state,
                gettherapylist: action.payload
            }
        case actionType.GET_THERAPY_FAILURE:
            return {
                gettherapylist: action.payload
            }
        case actionType.UPDATE_THERAPY_SUCCESS:
            return {
                ...state,
                updatetherapylist: action.payload
            }
        case actionType.UPDATE_THERAPY_FAILURE:
            return {
                updatetherapylist: action.payload
            }
        case actionType.DELETE_THERAPY_SUCCESS:
            return {
                ...state,
                deletetherapylist: action.payload
            }
        case actionType.DELETE_THERAPY_FAILURE:
            return {
                deletetherapylist: action.payload
            }
            //addcuriculum
            case actionType.ADD_CURRICULUM_SUCCESS:
            return {
                ...state
            }
        case actionType.ADD_CURRICULUM_FAILURE:
            return {
                ...state
            }
            //addeducation
            case actionType.ADD_EDUCATION_SUCCESS:
            return {
                ...state
            }
        case actionType.ADD_EDUCATION_FAILURE:
            return {
                ...state
            }
            //addpreviousjob
            case actionType.ADD_PREVIOUSJOB_SUCCESS:
            return {
                ...state
            }
        case actionType.ADD_PREVIOUSJOB_FAILURE:
            return {
                ...state
            }
            //updatepreviousjob
            case actionType.UPDATE_PREVIOUSJOB_SUCCESS:
            return {
                ...state
            }
        case actionType.UPDATE_PREVIOUSJOB_FAILURE:
            return {
                ...state
            }
            //deletejob
            case actionType.DELETE_PREVIOUSJOB_SUCCESS:
            return {
                ...state
            }
        case actionType.DELETE_PREVIOUSJOB_FAILURE:
            return {
                ...state
            }
            //updateeducation
            case actionType.UPDATE_EDUCATION_SUCCESS:
            return {
                ...state
            }
        case actionType.UPDATE_EDUCATION_FAILURE:
            return {
                ...state
            }
            //deleteeducation
            case actionType.DELETE_EDUCATION_SUCCESS:
            return {
                ...state
            }
        case actionType.DELETE_EDUCATION_FAILURE:
            return {
                ...state
            }
            //addlanguage
            case actionType.ADD_LANGUAGE_SUCCESS:
            return {
                ...state
            }
        case actionType.ADD_LANGUAGE_FAILURE:
            return {
                ...state
            }
            //updatelanguage
            case actionType.UPDATE_LANGUAGE_SUCCESS:
            return {
                ...state
            }
        case actionType.UPDATE_LANGUAGE_FAILURE:
            return {
                ...state
            }
            //deletelanguage
            case actionType.DELETE_LANGUAGE_SUCCESS:
            return {
                ...state
            }
        case actionType.DELETE_LANGUAGE_FAILURE:
            return {
                ...state
            }
            //adddoctor
            case actionType.ADD_DOCTOR_SUCCESS:
            return {
                ...state
            }
        case actionType.ADD_DOCTOR_FAILURE:
            return {
                ...state
            }
            //updatedoctor
            case actionType.UPDATE_DOCTOR_SUCCESS:
            return {
                ...state
            }
        case actionType.UPDATE_DOCTOR_SUCCESS:
            return {
                ...state
            }
            //deletedoctor
            case actionType.DELETE_DOCTOR_SUCCESS:
            return {
                ...state
            }
        case actionType.DELETE_DOCTOR_FAILURE:
            return {
                ...state
            }
        default: return state;
    }
}
export default patientReducer;