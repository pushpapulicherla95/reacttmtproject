const  moment =  require("moment");
const registerSuccess = async req => {
  return new Promise((resolve, reject) => {
    const registerPatient =
      "<!DOCTYPE html>" +
      '<html lang="en">' +
      "" +
      "<head>" +
      '    <meta charset="UTF-8">' +
      '    <meta name="viewport" content="width=device-width, initial-scale=1.0">' +
      '    <meta http-equiv="X-UA-Compatible" content="ie=edge">' +
      "    <title>REGISTER - TOMATO MEDICAL</title>" +
      '    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet"/>' +
      "    <style>" +
      "        html," +
      "        body {" +
      "            margin: 0;" +
      "            padding: 0;" +
      "            height: 100vh;" +
      "            font-family: 'Poppins', sans-serif;" +
      "           " +
      "        }" +
      "    </style>" +
      "</head>" +
      "" +
      "<body>" +
      '    <div class="bg-clr" style="width: 450px; min-height: 500px;background: #F5F6F8;padding: 15px;border-radius: 3px; margin: 15px auto;">' +
      '        <div class="content_page" style="background: #ffffff;min-height:500px;border-radius: 3px">' +
      "            <!-- Header section -->" +
      '            <div class="top-section" style="text-align: center; padding: 5px 10px 0 10px;   border-bottom: 1px solid #dddddd;">' +
      '            <img style="width: 160px;object-fit: contain;padding: 5px 0" src="http://cipldesigns.colaninfotechapps.in/tmapp/email_image/logo.jpg">' +
      "        </div>" +
      "        <!-- Body section -->" +
      '        <div class="body_area" style="padding:30px 0">' +
      '        <div class="username_text" style="text-align: center">' +
      '            <h5 style="font-size: 18px;margin-top: 0px; margin-bottom: 10px"> Hi ' +
      req.firstName +
      "!</h5>" +
      '            <p style="width: 340px;margin: auto;font-size: 14px;line-height: 25px;color: #797777;">Thanks for Signing up to the Tomato Medical. Your account is now ready for work!</p>' +
      '            <img src="http://cipldesigns.colaninfotechapps.in/tmapp/email_image/email_signup.png"style="margin-top: 30px;"/>' +
      "       " +
      "        </div>" +
      '        <div class="emailBtn" style="margin-top: 20px;text-align: center;">' +
      "            <button style=\"font-family: 'Poppins', sans-serif;border-radius: 7px;width: 70%;height: 30px;background: #249AF3;border: none;color: #fff;font-size: 14px;cursor: pointer;\">Visit Site</button>" +
      "        </div>" +
      '        <div class="need_section" style="margin-top:25px;">' +
      '            <h5 style="margin: 0;text-align: center;font-size: 14px;">Need Help? </h5>' +
      '            <p style="width: 250px;margin: 10px auto;font-size: 12px;line-height: 20px;color: #797777;text-align: center">Please send any feedback or bug reports to <a href="javascript:void(0);" style="color: #249af3;text-decoration: none;cursor: pointer;">support@tomatomedical.com</a></a></p>' +
      "        </div>" +
      "    </div>" +
      "" +
      "    <!-- Footer Section -->" +
      '    <div class="footArea" style="border-top: 1px solid #dddddd;padding: 8px;font-size: 12px;text-align: center;color: #797777;">' +
      "        <p>Copyrights @ 2019, All Rights Reserved</p>" +
      "    </div>" +
      "        </div>" +
      "    </div>" +
      "</body>" +
      "" +
      "</html>";

      const registerHealthCare =
      "<!DOCTYPE html>" +
      '<html lang="en">' +
      "" +
      "<head>" +
      '    <meta charset="UTF-8">' +
      '    <meta name="viewport" content="width=device-width, initial-scale=1.0">' +
      '    <meta http-equiv="X-UA-Compatible" content="ie=edge">' +
      "    <title>REGISTER - TOMATO MEDICAL</title>" +
      '    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet"/>' +
      "    <style>" +
      "        html," +
      "        body {" +
      "            margin: 0;" +
      "            padding: 0;" +
      "            height: 100vh;" +
      "            font-family: 'Poppins', sans-serif;" +
      "           " +
      "        }" +
      "    </style>" +
      "</head>" +
      "" +
      "<body>" +
      '    <div class="bg-clr" style="width: 450px; min-height: 500px;background: #F5F6F8;padding: 15px;border-radius: 3px; margin: 15px auto;">' +
      '        <div class="content_page" style="background: #ffffff;min-height:500px;border-radius: 3px">' +
      "            <!-- Header section -->" +
      '            <div class="top-section" style="text-align: center; padding: 5px 10px 0 10px;   border-bottom: 1px solid #dddddd;">' +
      '            <img style="width: 160px;object-fit: contain;padding: 5px 0" src="http://cipldesigns.colaninfotechapps.in/tmapp/email_image/logo.jpg">' +
      "        </div>" +
      "        <!-- Body section -->" +
      '        <div class="body_area" style="padding:30px 0">' +
      '        <div class="username_text" style="text-align: center">' +
      '            <h5 style="font-size: 18px;margin-top: 0px; margin-bottom: 10px"> Hi ' +
      req.firstName +
      "!</h5>" +
      '            <p style="width: 340px;margin: auto;font-size: 14px;line-height: 25px;color: #797777;">Thanks for Signing up to the Tomato Medical. <br/>Please wait unitil admin approves your account.</p>' +
      '            <img src="http://cipldesigns.colaninfotechapps.in/tmapp/email_image/email_signup.png"style="margin-top: 30px;"/>' +
      "       " +
      "        </div>" +
      '        <div class="emailBtn" style="margin-top: 20px;text-align: center;">' +
      "            <button style=\"font-family: 'Poppins', sans-serif;border-radius: 7px;width: 70%;height: 30px;background: #249AF3;border: none;color: #fff;font-size: 14px;cursor: pointer;\">Visit Site</button>" +
      "        </div>" +
      '        <div class="need_section" style="margin-top:25px;">' +
      '            <h5 style="margin: 0;text-align: center;font-size: 14px;">Need Help? </h5>' +
      '            <p style="width: 250px;margin: 10px auto;font-size: 12px;line-height: 20px;color: #797777;text-align: center">Please send any feedback or bug reports to <a href="javascript:void(0);" style="color: #249af3;text-decoration: none;cursor: pointer;">support@tomatomedical.com</a></a></p>' +
      "        </div>" +
      "    </div>" +
      "" +
      "    <!-- Footer Section -->" +
      '    <div class="footArea" style="border-top: 1px solid #dddddd;padding: 8px;font-size: 12px;text-align: center;color: #797777;">' +
      "        <p>Copyrights @ 2019, All Rights Reserved</p>" +
      "    </div>" +
      "        </div>" +
      "    </div>" +
      "</body>" +
      "" +
      "</html>";
      
    resolve(req.healthCare ? registerHealthCare : registerPatient );
  });
};

const registerSuccessHealthCare = async req => {
  return new Promise((resolve, reject) => {
    var myvar =
      "<!DOCTYPE html>" +
      '<html lang="en">' +
      "" +
      "<head>" +
      '    <meta charset="UTF-8">' +
      '    <meta name="viewport" content="width=device-width, initial-scale=1.0">' +
      '    <meta http-equiv="X-UA-Compatible" content="ie=edge">' +
      "    <title>REGISTER - TOMATO MEDICAL</title>" +
      '    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet"/>' +
      "    <style>" +
      "        html," +
      "        body {" +
      "            margin: 0;" +
      "            padding: 0;" +
      "            height: 100vh;" +
      "            font-family: 'Poppins', sans-serif;" +
      "           " +
      "        }" +
      "    </style>" +
      "</head>" +
      "" +
      "<body>" +
      '    <div class="bg-clr" style="width: 450px; min-height: 500px;background: #F5F6F8;padding: 15px;border-radius: 3px; margin: 15px auto;">' +
      '        <div class="content_page" style="background: #ffffff;min-height:500px;border-radius: 3px">' +
      "            <!-- Header section -->" +
      '            <div class="top-section" style="text-align: center; padding: 5px 10px 0 10px;   border-bottom: 1px solid #dddddd;">' +
      '            <img style="width: 160px;object-fit: contain;padding: 5px 0" src="http://cipldesigns.colaninfotechapps.in/tmapp/email_image/logo.jpg">' +
      "        </div>" +
      "        <!-- Body section -->" +
      '        <div class="body_area" style="padding:30px 0">' +
      '        <div class="username_text" style="text-align: center">' +
      '            <h5 style="font-size: 18px;margin-top: 0px; margin-bottom: 10px"> Hi ' +
      req.firstName +
      "!</h5>" +
      '            <p style="width: 340px;margin: auto;font-size: 14px;line-height: 25px;color: #797777;">Thanks for Signing up to the Tomato Medical. Your account is now ready for work!</p>' +
      '            <img src="http://cipldesigns.colaninfotechapps.in/tmapp/email_image/email_signup.png"style="margin-top: 30px;"/>' +
      "       " +
      "        </div>" +
      '        <div class="emailBtn" style="margin-top: 20px;text-align: center;">' +
      "            <button style=\"font-family: 'Poppins', sans-serif;border-radius: 7px;width: 70%;height: 30px;background: #249AF3;border: none;color: #fff;font-size: 14px;cursor: pointer;\">Visit Site</button>" +
      "        </div>" +
      '        <div class="need_section" style="margin-top:25px;">' +
      '            <h5 style="margin: 0;text-align: center;font-size: 14px;">Need Help? </h5>' +
      '            <p style="width: 250px;margin: 10px auto;font-size: 12px;line-height: 20px;color: #797777;text-align: center">Please send any feedback or bug reports to <a href="javascript:void(0);" style="color: #249af3;text-decoration: none;cursor: pointer;">support@tomatomedical.com</a></a></p>' +
      "        </div>" +
      "    </div>" +
      "" +
      "    <!-- Footer Section -->" +
      '    <div class="footArea" style="border-top: 1px solid #dddddd;padding: 8px;font-size: 12px;text-align: center;color: #797777;">' +
      "        <p>Copyrights @ 2019, All Rights Reserved</p>" +
      "    </div>" +
      "        </div>" +
      "    </div>" +
      "</body>" +
      "" +
      "</html>";

    resolve(myvar);
  });
};


const forgetPassword = req => {
  // console.log("hello world ", req);
  return new Promise((resolve, reject) => {
    var myvar =
      "<!DOCTYPE html>" +
      '<html lang="en">' +
      "" +
      "<head>" +
      '    <meta charset="UTF-8">' +
      '    <meta name="viewport" content="width=device-width, initial-scale=1.0">' +
      '    <meta http-equiv="X-UA-Compatible" content="ie=edge">' +
      "    <title>Forgot Password - TOMATO MEDICAL</title>" +
      '    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet"/>' +
      "    <style>" +
      "        html," +
      "        body {" +
      "            margin: 0;" +
      "            padding: 0;" +
      "            height: 100vh;" +
      "            font-family: 'Poppins', sans-serif;" +
      "           " +
      "        }" +
      "    </style>" +
      "</head>" +
      "" +
      "<body>" +
      '    <div class="bg-clr" style="width: 450px; min-height: 500px;background: #F5F6F8;padding: 15px;border-radius: 3px; margin: 15px auto;">' +
      '        <div class="content_page" style="background: #ffffff;min-height:500px;border-radius: 3px">' +
      "            <!-- Header section -->" +
      '            <div class="top-section" style="text-align: center; padding: 5px 10px 0 10px;   border-bottom: 1px solid #dddddd;">' +
      '            <img src="http://cipldesigns.colaninfotechapps.in/tmapp/email_image/logo.jpg" style="width: 160px;object-fit: contain;padding: 5px 0"/>' +
      "        </div>" +
      "        <!-- Body section -->" +
      '        <div class="body_area" style="padding:30px 0">' +
      '        <div class="username_text" style="text-align: center">' +
      '            <h5 style="font-size: 18px;margin-top: 0px; margin-bottom: 10px">Hi, John!</h5>' +
      '            <p style="width: 340px;margin: auto;font-size: 14px;line-height: 25px;color: #797777;">There was a request to reset the password!</p>' +
      '            <img src="http://cipldesigns.colaninfotechapps.in/tmapp/email_image/email_forgot.png" style="margin-top: 30px;"/>' +
      "       " +
      "        </div>" +
      '        <div class="tokan_id" style="text-align: center">' +
      '            <p style="margin:0;font-weight:600">Token Id:</p><p style="margin:0;font-weight:600;color: #d20b12" id="elem">' +
      req +
      "</p>" +
      '			<p  onclick="myFunction()" style="margin:0;font-size:13px;cursor:pointer">Copy To Clipboard</p>' +
      "			</div>" +
      '        <!-- <div class="emailBtn" style="margin-top: 10px;text-align: center;">' +
      "            <button style=\"font-family: 'Poppins', sans-serif;border-radius: 7px;width: 70%;height: 30px;background: #249AF3;border: none;color: #fff;font-size: 14px; cursor: pointer;\">Reset Your Password</button>" +
      "        </div> -->" +
      '        <div class="notice_section" style="margin-top:15px;">' +
      '            <p style="width: 300px;margin: 10px auto;font-size: 12px;line-height: 20px;color: #797777;text-align: center">If you did not make this request, Just ignore this email. Otherwise, Please copy the above token id to reset the password</p>' +
      "        </div>" +
      '        <div class="need_section" style="margin-top:25px;">' +
      "            " +
      '            <h5 style="margin: 0;text-align: center;font-size: 14px;">Need Help? </h5>' +
      '            <p style="width: 250px;margin: 10px auto;font-size: 12px;line-height: 20px;color: #797777;text-align: center">Please send any feedback or bug reports to <a href="javascript:void(0);" style="color: #249af3;text-decoration: none;cursor: pointer;">support@tomatomedical.com</a></a></p>' +
      "        </div>" +
      "    </div>" +
      "" +
      "    <!-- Footer Section -->" +
      '    <div class="footArea" style="border-top: 1px solid #dddddd;padding: 8px;font-size: 12px;text-align: center;color: #797777;">' +
      "        <p>Copyrights @ 2019, All Rights Reserved</p>" +
      "    </div>" +
      "        </div>" +
      "    </div>" +
      "</body>" +
      "<script>" +
      "function myFunction(){" +
      'console.log("dsjfghjsfd");' +
      "var range = document.createRange();" +
      "range.selectNode(document.getElementById('elem'));" +
      "window.getSelection().removeAllRanges();" +
      "window.getSelection().addRange(range);" +
      'document.execCommand("copy");' +
      "window.getSelection().removeAllRanges();}" +
      "</script>" +
      "" +
      "</html>";

    resolve(myvar);
  });
};

const bookingAppoinment = req => {
 
  const { appointmentDate, startTime, endTime, specialistName, doctorMail, email ,scheduleType} = req;
  const { firstName	} = req.userLoginData;
  const apponintMentDateMoment = moment(appointmentDate, 'YYYY-MM-DD');
  const dayOfWeek = apponintMentDateMoment.format('dddd')
  const dayOfMonth = apponintMentDateMoment.format('Do')
  const monthName = apponintMentDateMoment.format('MMMM')
  const startTimeMoment = moment(startTime, "hh:mm").format("LT")
  const endTimeMoment = moment(endTime, "hh:mm").format("LT")

  return new Promise((resolve, reject) => {
    const getTemplate = (doc , remainder) =>{
      let appointMentMainText = `Your appoinment with ${!doc ? specialistName : firstName} is Booked!`
      if(remainder && scheduleType.includes("video call")){
        appointMentMainText = `Your video call with ${!doc ? specialistName : firstName} is starting in 5 mins!`
      }
      return   "<!DOCTYPE html>" +
      '<html lang="en">' +
      "" +
      "<head>" +
      '    <meta charset="UTF-8">' +
      '    <meta name="viewport" content="width=device-width, initial-scale=1.0">' +
      '    <meta http-equiv="X-UA-Compatible" content="ie=edge">' +
      "    <title>Appoinment - TOMATO MEDICAL</title>" +
      '    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet"/>' +
      "    <style>" +
      "        html," +
      "        body {" +
      "            margin: 0;" +
      "            padding: 0;" +
      "            height: 100vh;" +
      "            font-family: 'Poppins', sans-serif;" +
      "           " +
      "        }" +
      "    </style>" +
      "</head>" +
      "" +
      "<body>" +
      '    <div class="bg-clr" style="width: 450px; min-height: 500px;background: #F5F6F8;padding: 15px;border-radius: 3px; margin: 15px auto;">' +
      '        <div class="content_page" style="background: #ffffff;min-height:500px;border-radius: 3px">' +
      "            <!-- Header section -->" +
      '            <div class="top-section" style="text-align: center; padding: 5px 10px 0 10px;   border-bottom: 1px solid #dddddd;">' +
      '            <img src="http://cipldesigns.colaninfotechapps.in/tmapp/email_image/logo.jpg" style="width: 160px;object-fit: contain;padding: 5px 0"/>' +
      "        </div>" +
      "        <!-- Body section -->" +
      '        <div class="body_area" style="padding:30px 0">' +
      '        <div class="username_text" style="text-align: center">' +
      '            <h5 style="font-size: 18px;margin-top: 0px; margin-bottom: 10px">Hi '+`${!doc ? firstName : specialistName}`+'!</h5>' +
      '            <p style="width: 340px;margin: auto;font-size: 14px;line-height: 25px;color: #797777;">'+appointMentMainText+'</p>' +
      '            <img src="http://cipldesigns.colaninfotechapps.in/tmapp/email_image/email_appoinment.png"style="margin-top: 30px;"/>' +
      "       " +
      "        </div>" +
      '        <div class="appoinment_section" style="margin-top: 20px;text-align: center;background: #F9FAFC;padding: 8px 0;">' +
      '            <h4  style="margin:0"><span>'+dayOfWeek+'</span>, <span>'+monthName+' '+dayOfMonth+'</span> from <span>'+startTimeMoment+' to '+endTimeMoment+'</span></h4>' +
      // '            <h4 style="margin:0">@ <span>Hospital Name</span> </h4>' +
      "" +
      "        </div>" +
      "       " +
      "        " +
      '        <div class="need_section" style="margin-top:25px;">' +
      "            " +
      '            <h5 style="margin: 0;text-align: center;font-size: 14px;">Need Help? </h5>' +
      '            <p style="width: 220px;margin: 10px auto;font-size: 12px;line-height: 20px;color: #797777;text-align: center">Need to check your appoinment <a href="javascript:void(0);" style="color: #249af3;text-decoration: none;cursor: pointer;">Login to your account</a></a></p>' +
      '            <p style="width: 250px;margin: 10px auto;font-size: 12px;line-height: 20px;color: #797777;text-align: center; border-top: 1px dotted #ddd;padding: 10px 0 0;">Please send any feedback or bug reports to <a href="javascript:void(0);" style="color: #249af3;text-decoration: none;cursor: pointer;">support@tomatomedical.com</a></a></p>' +
      "            " +
      "        </div>" +
      "    </div>" +
      "" +
      "    <!-- Footer Section -->" +
      '    <div class="footArea" style="border-top: 1px solid #dddddd;padding: 8px;font-size: 12px;text-align: center;color: #797777;">' +
      "        <p>Copyrights @ 2019, All Rights Reserved</p>" +
      "    </div>" +
      "        </div>" +
      "    </div>" +
      "</body>" +
      "" +
      "</html>";
    }
    const mailArray = [{ template: getTemplate(true), email: doctorMail, remaniderTemplate: getTemplate(true, true), appointmentDate, startTime },
    { template: getTemplate(), email: email, remaniderTemplate: getTemplate(false, true), appointmentDate, startTime }]
      resolve(mailArray);
  });
};

module.exports = {
  registerSuccess,
  forgetPassword,
  bookingAppoinment,
  registerSuccessHealthCare
};
