const stripe = require("stripe")("sk_test_hvPnrQ6yTlxPjvw8IMKw0JTW00kp6rSUAF");

const cardPayment = (request, response) => {
  stripe.tokens.create(request.body, (err, token) => {
    if (err) {
      response.json({
        status: false,
        message: "failure",
        data: err
      });
    } else {
      return response.json({
        status: true,
        message: "success",
        data: token
      });
    }
  });
};

const bankPayment = (request, response) => {
  stripe.tokens.create(request.body, (err, token) => {
    if (err) {
      response.json({
        status: false,
        message: "failure",
        data: err
      });
    } else {
      return response.json({
        status: true,
        message: "success",
        data: token
      });
    }
  });
};
module.exports = {
  cardPayment,
  bankPayment
};
