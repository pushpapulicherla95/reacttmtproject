import React, { Component } from "react";
import { connect } from "react-redux";
import { listDoctorInfo } from "../../../../service/appointment/action";

class MyDoctor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            anamnesis: "",
            objectives: "",
            diagnosis: "",
            treatment: "",
            feedbackpop: false,
            showDetails:null
        };
    }
    componentWillMount = () => {
        const { listdoctorDetails } = this.props
        const { userInfo } = this.props.loginDetails
        const listinfo = {
            patientId: userInfo.userId
        }
        this.props.listDoctorInfo(listinfo);


    }
    Feedbackpop = (each) => {
        this.setState({ feedbackpop: true ,showDetails:each});
    };
    modalPopUpClose = () => {
        this.setState({
            feedbackpop: false,

        });
    };

    render() {
        const { anamnesis, objectives, diagnosis, treatment, feedbackpop ,showDetails} = this.state;
        const { userInfo } = this.props.loginDetails
        console.log("profileinfo",userInfo)
        const { listdoctorDetails } = this.props
        // const patientDetails = listdoctorDetails[this.props.location.state.index[0]]
        return (
            <div className="mainMenuSide" id="main-content">
                <div className="wrapper">
                    <div className="container-fluid">
                        <div className="body-content">


                            <div className="search-result-section">
                                {/* {listdoctorDetails && listdoctorDetails.map((each, i) =>
                                    <div class="tomCardBody">
                                    <div className="row">
                                    <div className="col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                    <div class="tomCardHead">
                                    <h5>Doctor Name:<span>
                                                        {each.anamnesis}
                                                    </span> </h5>

                                </div>
                                    </div>
                                    </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                                <div class="previewItem">
                                                    <label>Anamnesis :</label>
                                                    <span>
                                                        {each.anamnesis}
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                                <div class="previewItem">
                                                    <label>Objectives :</label>
                                                    <span> {each.objectives}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                                <div class="previewItem">
                                                    <label>Diagnosis :</label>
                                                    <span> {each.diagnosis}</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                                <div class="previewItem">
                                                    <label>Treatment :</label>
                                                    <span> {each.treatment}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>)} */}
                                <div class="tomCardBody">
                                <div className="search-result-section">
                               
                               {
                                    listdoctorDetails  ?   listdoctorDetails.map((each, i) =>

                                   <div className="search-card2">
                                       <img src="https://images.pexels.com/photos/1239291/pexels-photo-1239291.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=650&w=940" />
                                       <div className="doc-title">
                                           <p className="title-name"> {each.doctorName}</p>
                                           <p> +49 9876543210</p>

                                       </div>
                                       <div className="patientView">
                                           <button onClick={()=>this.Feedbackpop(each)} ><i class="far fa-comment-dots"></i></button>
                                       </div>
                                      
                                   </div> 
                                   ): <div className="search-result-section1"><h5> There is no Appointment Available</h5></div>}



                               <div
                                   className={feedbackpop ? "modal d-block" : "modal"}
                                   id="myModal"
                               >
                                   <div class="modal-dialog">
                                       <div class="modal-content modalPopUp">
                                           <div class="modal-header borderNone">
                                               <h5 class="modal-title">DoctorName:{showDetails && showDetails.doctorName}</h5>
                                               <button
                                                   type="button"
                                                   className="close"
                                                   data-dismiss="modal"
                                                   onClick={this.modalPopUpClose}
                                               >
                                                   <i class="fas fa-times" />
                                               </button>
                                           </div>
                                           
                                               <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                   <div class="previewItem">
                                                       <label>Anamnesis :</label>
                                                       <span>
                                                           {showDetails && showDetails.anamnesis}
                                                       </span>
                                                   </div>
                                               </div>
                                               <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                   <div class="previewItem">
                                                       <label>Objectives :</label>
                                                       <span> {showDetails &&showDetails.objectives}</span>
                                                   </div>
                                               </div>
                                           
                                      
                                               <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                   <div class="previewItem">
                                                       <label>Diagnosis :</label>
                                                       <span> {showDetails &&showDetails.diagnosis}</span>
                                                   </div>
                                               </div>
                                               <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                   <div class="previewItem">
                                                       <label>Treatment :</label>
                                                       <span> {showDetails &&showDetails.treatment}</span>
                                                   </div>
                                               </div>
                                           
                                       </div>

                                   </div>

                               </div>

                           </div>
                                </div>
                             

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
const mapStateToProps = state => ({

    loginDetails: state.loginReducer.loginDetails,
    listdoctorDetails: state.appointmentReducer.listdoctorDetails
});

const mapDispatchToProps = dispatch => ({
    listDoctorInfo: (listinfo) => dispatch(listDoctorInfo(listinfo)),
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyDoctor);