import actionType from "../../service/healthrecord/actionType";

const initialState = {
  getpersonalDetails: "",
  getdieasesDetails: "",
  therapistsDiesese: "",
  therapistsfamily: null,
  diseases: null,
  addupdateDiseases: null,
  addpraganancieslist:"",
  updatepraganancieslist:"",
  deletepraganancieslist:""
};

const healthrecordReducer = (state = initialState, action) => {
  // console.log("dsfsdfsddddddddddddddddddddddddddddddd", action.payload,actionType.GET_DIEASESDATA_SUCCESS)
  switch (action.type) {
    case actionType.ADD_HOSPITAL_DATA_SUCCESS:
      return {
        ...state
      };
    case actionType.ADD_HOSPITAL_DATA_FAILURE:
      return {
        ...state
      };
    case actionType.DELETE_HOSPITAL_DATA_SUCCESS:
      return {
        ...state
      };
    case actionType.DELETE_HOSPITAL_DATA_FAILURE:
      return {
        ...state
      };
    case actionType.EDIT_HOSPITAL_DATA_SUCCESS:
      return {
        ...state
      };
    case actionType.EDIT_HOSPITAL_DATA_FAILURE:
      return {
        ...state
      };
    //iMMUNIZATION
    case actionType.ADD_IMMUNIZATION_SUCCESS:
      return {
        ...state
      };
    case actionType.ADD_IMMUNIZATION_FAILURE:
      return {
        ...state
      };
    case actionType.DELETE_IMMUNIZATION_SUCCESS:
      return {
        ...state
      };
    case actionType.DELETE_IMMUNIZATION_FAILURE:
      return {
        ...state
      };
    case actionType.EDIT_IMMUNIZATION_SUCCESS:
      return {
        ...state
      };
    case actionType.EDIT_IMMUNIZATION_FAILURE:
      return {
        ...state
      };
    case actionType.ADD_PERSONALDATA_SUCCESS:
      return {
        ...state
      };
    case actionType.ADD_PERSONALDATA_FAILURE:
      return {
        ...state
      };
    case actionType.ADD_COREDATA_SUCCESS:
      return {
        ...state
      };
    case actionType.ADD_COREDATA_FAILURE:
      return {
        ...state
      };
    case actionType.ADD_JOBDATA_SUCCESS:
      return {
        ...state
      };
    case actionType.ADD_JOBDATA_FAILURE:
      return {
        ...state
      };
    case actionType.ADD_EMERGENCY_SUCCESS:
      return {
        ...state
      };
    case actionType.ADD_EMERGENCY_FAILURE:
      return {
        ...state
      };
    case actionType.ADD_BENEFITS_SUCCESS:
      return {
        ...state
      };
    case actionType.ADD_BENEFITS_FAILURE:
      return {
        ...state
      };
    //list
    case actionType.GET_PERSONALDATA_SUCCESS:
      return {
        ...state,
        getpersonalDetails: action.payload
      };
    case actionType.GET_PERSONALDATA_FAILURE:
      return {
        ...state,
        getpersonalDetails: action.payload
      };
      //Prgnancies
     
    case actionType.ADD_PRGNANCIESLIST_SUCCESS:
      return {
        ...state,
        addpraganancieslist: action.payload
      };
    case actionType.ADD_PRGNANCIESLIST_FAILURE:
      return {
        ...state,
        addpraganancieslist: action.payload
      };
      case actionType.UPDATE_PRGNANCIESLIST_SUCCESS:
      return {
        ...state,
        updatepraganancieslist: action.payload
      };
    case actionType.UPDATE_PRGNANCIESLIST_FAILURE:
      return {
        ...state,
        updatepraganancieslist: action.payload
      };
      case actionType.DELETE_PRGNANCIESLIST_SUCCESS:
      return {
        ...state,
        deletepraganancieslist: action.payload
      };
    case actionType.DELETE_PRGNANCIESLIST_FAILURE:
      return {
        ...state,
        deletepraganancieslist: action.payload
      };
    //EDITHOSPITAL
    case actionType.EDIT_HOSPITAL_SUCCESS:
      return {
        ...state
      };
    case actionType.EDIT_HOSPITAL_FAILURE:
      return {
        ...state
      };

    // diesese list
    case actionType.DISEASESLIST_SUCCESS:
      return {
        ...state,
        therapistsDiesese: action.payload
      };
    case actionType.DISEASESLIST_FAILURE:
      return {
        ...state,
        therapistsDiesese: action.payload
      };
    case actionType.FAMILYLIST_SUCCESS:
      return {
        ...state,
        therapistsfamily: action.payload
      };
    case actionType.FAMILYLIST_FAILURE:
      return {
        ...state,
        therapistsfamily: action.payload
      };
    case actionType.ADDDISEASESLIST_SUCCESS:
      return {
        ...state,
        diseases: action.payload
      };
    case actionType.ADDDISEASESLIST_FAILURE:
      return {
        ...state,
        diseases: action.payload
      };
    case actionType.EDITDISEASESLIST_SUCCESS:
      return {
        ...state,
        diseases: action.payload
      };
    case actionType.EDITDISEASESLIST_FAILURE:
      return {
        ...state,
        diseases: action.payload
      };
    case actionType.DELETEDISEASESLIST_SUCCESS:
      return {
        ...state,
        diseases: action.payload
      };
    case actionType.DELETEDISEASESLIST_FAILURE:
      return {
        ...state,
        diseases: action.payload
      };
    case actionType.ADDUPDATEDISEASES_SUCCESS:
      return {
        ...state,
        addupdateDiseases: action.payload
      };
    case actionType.ADDUPDATEDISEASES_FAILURE:
      return {
        ...state,
        addupdateDiseases: action.payload
      };
    //sports
    case actionType.ADD_SPORTDATA_SUCCESS:
      return {
        ...state
      };
    case actionType.ADD_SPORTDATA_FAILURE:
      return {
        ...state
      };
    case actionType.UPDATE_SPORTDATA_SUCCESS:
      return {
        ...state
      };
    case actionType.UPDATE_SPORTDATA_FAILURE:
      return {
        ...state
      };
    case actionType.DELETE_SPORTDATA_SUCCESS:
      return {
        ...state
      };
    case actionType.DELETE_SPORTDATA_FAILURE:
      return {
        ...state
      };
    case actionType.ADD_PRGNANCIESDATA_SUCCESS:
      return {
        ...state
      };
    case actionType.ADD_PRGNANCIESDATA_FAILURE:
      return {
        ...state
      };
    default:
      return state;
  }

}
export default healthrecordReducer;