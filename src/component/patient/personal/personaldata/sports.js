import React, { Component } from "react";
import { connect } from "react-redux";
// import {} from "../../../../service/patient/action";
import Select from "react-select";
import $ from "jquery";
import axios from "axios";
import { getUploadFiles, handleFileDelete } from "../../../../service/upload/action";
import {
  addSportsdata,
  getPersonaldata,
  updateSportsdata,
  deleteSportsdata
} from "../../../../service/healthrecord/action";
import URL from "../../../../asset/configUrl";
import { awsPersonalFiles } from "../../../../service/common/action";
import { toastr } from "react-redux-toastr";

class sports extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 1,
      tabone: true,
      tabtwo: false,
      sport: "",
      frequency: "",
      addfindingpop: false,
      editfinding: false,
      editDetails: {},
      uploadPopup: false,
      name: "",
      fileName: "",
      fileType: "",
      fileURL: "",
      patientFindingListData: [],
      getpersonalDetails: null,
      Valid: false
    };
  }
  ValidationField = () => {
    const {
      title } = this.state
    this.setState({
      Valid:
        title



    });

  }
  handleAwsFiles = value => {
    window.open("https://data.tomatomedical.com/patientpersonal/" + value, '_blank');

  };

  handleChangePopup = e => {
    var reader = new FileReader();
    // var file = e.target.files[0];
    var file = e.target.files[0];
    let name = file.name.split('.')[0]
    let type = "." + file.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileURL: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(file);
  };
  popupSubmit = () => {
    var phrase = this.state.fileURL;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);
    var { userInfo } = this.props.loginDetails;
    const payload = {
      uid: userInfo.userId,
      z: match[1],
      fileName: this.state.fileName,
      fileType: this.state.fileType,
      name: this.state.name,
      category: "sports"
    };
    this.setState({ uploadPopup: false });

    axios
      .post(URL.FILESPORTS_UPLOAD, payload)
      .then(response => {
        response.data.status === "success"
          ? toastr.success("Message", response.data.message)
          : toastr.error("Message", response.data.message);
        if (response.data.status === "success") {
          var { userInfo } = this.props.loginDetails;
          const uploadFiles = {
            uid: userInfo.userId,
            category: "sports"
          };
          this.props.getUploadFiles(uploadFiles);
          this.setState({ uploadPopup: false });
          this.setState({
            name: "",
            fileName: "",
            fileType: "",
            fileURL: ""
          });
        }
      })
      .catch(error => {
        // this.setState({ uploadPopup: true });
      });
  };

  tabOne = () => {
    this.setState({ tabone: true, tabtwo: false });
  };
  tabTwo = () => {
    this.setState({ tabone: false, tabtwo: true });
  };
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value }, () => this.ValidationField());
  };
  findingpop = () => {
    this.setState({ addfindingpop: true }, () => {
      this.emptystate();
    });
  };
  modalPopUpClose = () => {
    this.setState({
      addfindingpop: false,
      editfinding: false
    });
  };
  handleFinding = () => {
    const { title, sport, frequency } = this.state;
    const { userInfo } = this.props.loginDetails;
    const createsports = {
      uid: userInfo.userId,
      sports: [
        {
          title: title,
          sport: sport,
          frequency: frequency
        }
      ]
    };

    this.props.addSportsdata(createsports);

    this.setState({
      addfindingpop: false
    });
  };
  componentWillMount() {
    const { userInfo } = this.props.loginDetails;
    const listpersonal = {
      uid: userInfo.userId
    };
    // const uploadFiles = {
    //   uid: userInfo.userId,
    //   category: "sports"
    // };

    // this.props.getUploadFiles(uploadFiles);
    this.props.getPersonaldata(listpersonal);

    // this.props.listFindingData(listfinding);
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.getpersonalDetails &&
      nextProps.getpersonalDetails.sport != null &&
      nextProps.getpersonalDetails.sport != 0
    ) {
      const listpersonal = nextProps.getpersonalDetails.sport;
      let patientFindingListData = JSON.parse(listpersonal);

      this.setState({
        patientFindingListData,
        addfindingpop: false
      });
    }
  }

  modalPopupOpen = e => {
    this.setState({ editfinding: true, editDetails: e });
    this.setState({
      Valid: false,

      title: e.title,
      frequency: e.frequency,
      sport: e.sport
    });
  };
  updateSport = () => {
    const { userInfo } = this.props.loginDetails;

    const { title, frequency, sport } = this.state;
    const updatesport = {
      uid: userInfo.userId,
      sports: [
        {
          title: title,
          frequency: frequency,
          sport: sport
        }
      ]
    };

    this.props.updateSportsdata(updatesport);
    this.setState({ editfinding: false });
  };
  deleteSport = e => {
    const { userInfo } = this.props.loginDetails;

    const deletesports = {
      uid: userInfo.userId,
      sports: [
        {
          title: e.title,
          frequency: e.frequency,
          sport: e.sport
        }
      ]
    };
    this.props.deleteSportsdata(deletesports);
  };
  emptystate = () => {
    this.setState({
      frequency: "",
      sport: "",
      title: "",
      Valid: false
    });
  };

  render() {
    const {
      tabone,
      tabtwo,
      title,
      sport,
      frequency,
      addfindingpop,
      patientFindingListData,
      editfinding, Valid
    } = this.state;
    const sports = this.state.patientFindingListData.sports;
    return (
      <React.Fragment>
        <div className="" id="main-content">
          <div className="wrapper">
            <div className="container-fluid">
              <div className="body-content">
                <div className="tomCard">
                  <div className="tomCardHead">
                    <h5 className="float-left">Sports</h5>
                  </div>

                  <div className="tab-menu-content">
                    <div class="tab-menu-title">
                      <ul>
                        <li className={this.state.tabone ? "menu1 active" : ""}>
                          <a href="#" onClick={this.tabOne}>
                            <p>List Of Sports</p>
                          </a>
                        </li>
                        <li className={this.state.tabtwo ? "menu1 active" : ""}>
                          <a href="#" onClick={this.tabTwo}>
                            <p>Attached Files</p>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>

                  {tabone && (
                    <div className="tab-menu-content-section">
                      <div
                        id="content-1"
                        className={tabone ? "d-block" : "d-none"}
                      >
                        <button
                          type="button"
                          onClick={this.findingpop}
                          className="commonBtn2 float-right mb-2"
                        >
                          <i className="fa fa-plus" /> Add Sports
                        </button>
                        <div className="table-responsive">
                          <table className="table table-hover">
                            <thead className="thead-default">
                              <tr>
                                <th>Title</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {sports &&
                                sports.map((each, i) => (
                                  <tr key={i}>
                                    <td data-label="startDate">{each.title}</td>

                                    <td data-label="Action">
                                      <a
                                        title="Edit"
                                        className="editBtn"
                                        onClick={e => this.modalPopupOpen(each)}
                                      >
                                        <span id={5} className="fa fa-edit" />
                                      </a>
                                      <a
                                        title="Delete"
                                        className="deleteBtn"
                                        onClick={e => this.deleteSport(each)}
                                      >
                                        <i
                                          className="fa fa-trash"
                                          data-toggle="modal"
                                          data-target="#myModal2"
                                        />
                                      </a>
                                    </td>
                                  </tr>
                                ))}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  )}

                  {tabtwo && (
                    <div
                      id="content-2"
                      className={tabtwo ? "d-block" : "d-none"}
                    >
                      <button
                        type="button"
                        className="commonBtn2 float-right mb-2"
                        onClick={() =>
                          this.setState({
                            uploadPopup: !this.state.uploadPopup
                          })
                        }
                      >
                        <i className="fa fa-plus" />
                        <span> Upload new files</span>                </button>
                      <div className="table-responsive">
                        <table className="table table-hover">
                          <thead className="thead-default">
                            <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>FileName</th>


                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.props.getFilesDetails.fileDetails &&
                              this.props.getFilesDetails.fileDetails.map(
                                (each, i) => (
                                  <tr>
                                    <td data-label="#">{i + 1}</td>
                                    <td data-label="Name">{each.name}</td>
                                    <td data-label="FileName"> {each.fileName}</td>
                                    <td data-label="Action">
                                      <button
                                        class="download_btn"
                                        onClick={() =>
                                          this.handleAwsFiles(each.fileUrl)
                                        }
                                      >
                                        <i class="fas fa-download" />
                                      </button>
                                      <button
                                        class="download_btn"
                                        onClick={() =>
                                          this.props.handleFileDelete(each.id, each.fileUrl, "sports")
                                        }
                                      >
                                        <i class="fas fa-trash" />
                                      </button>
                                    </td>
                                  </tr>
                                )
                              )}


                          </tbody>
                        </table>
                      </div>
                    </div>
                  )}
                </div>

                <div
                  className={addfindingpop ? "modal d-block" : "modal"}
                  id="myModal"
                >
                  <div class="modal-dialog">
                    <div class="modal-content modalPopUp">
                      <div class="modal-header borderNone">
                        <h5 class="modal-title">Create New Entry</h5>
                        <button
                          type="button"
                          className="close"
                          data-dismiss="modal"
                          onClick={this.modalPopUpClose}
                        >
                          <i class="fas fa-times" />
                        </button>
                      </div>
                      <div class="modal-body">
                        <form>
                          <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Title for list entry (e.g. Volleyball) <sup>*</sup>
                                </label>
                                <input
                                  className="form-control"
                                  name="title"
                                  onChange={this.handleChange}
                                  value={title}
                                  type="text"
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">Sport</label>
                                <input
                                  className="form-control"
                                  name="sport"
                                  onChange={this.handleChange}
                                  value={sport}
                                  type="text"
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Frequency (hours per week)
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="frequency"
                                  onChange={this.handleChange}
                                  type="text"
                                  value={frequency}
                                />
                                <span />
                              </div>
                            </div>

                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <button
                                type="button"
                                class="commonBtn float-right"
                                onClick={this.handleFinding} disabled={!Valid}
                              >
                                Add
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  className={editfinding ? "modal d-block" : "modal"}
                  id="myModal"
                >
                  <div class="modal-dialog">
                    <div class="modal-content modalPopUp">
                      <div class="modal-header borderNone">
                        <h5 class="modal-title">Edit Sports</h5>
                        <button
                          type="button"
                          className="close"
                          onClick={this.modalPopUpClose}
                        >
                          <i class="fas fa-times" />
                        </button>
                      </div>
                      <div class="modal-body">
                        <form>
                          <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Title for list entry (e.g. Volleyball)
                                </label>
                                <input
                                  className="form-control"
                                  name="title"
                                  onChange={this.handleChange}
                                  defaultValue={this.state.editDetails.title}
                                  type="text"
                                  readOnly
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">Sport</label>
                                <input
                                  className="form-control"
                                  name="sport"
                                  onChange={this.handleChange}
                                  defaultValue={this.state.editDetails.sport}
                                  type="text"
                                />
                                <span />
                              </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <div class="form-group">
                                <label className="form-label">
                                  Frequency (hours per week)
                                </label>
                                <input
                                  id="validation-email"
                                  className="form-control"
                                  name="frequency"
                                  onChange={this.handleChange}
                                  type="text"
                                  defaultValue={
                                    this.state.editDetails.frequency
                                  }
                                />
                                <span />
                              </div>
                            </div>

                            <div class="col-md-12 col-lg-12 col-sm-12">
                              <button
                                type="button"
                                class="commonBtn float-right"
                                onClick={this.updateSport} disabled={!Valid}
                              >
                                Update
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {
            <div
              className={this.state.uploadPopup ? "modal d-block" : "modal"}
              id="myModal"
            >
              <div class="modal-dialog">
                <div class="modal-content modalPopUp">
                  <div class="modal-header borderNone">
                    <h5 class="modal-title">Upload File</h5>
                    <button
                      type="button"
                      className="close"
                      onClick={this.trigger}
                    >
                      <i class="fas fa-times" />
                    </button>
                  </div>
                  <div class="modal-body">
                    <form>
                      <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                          <div class="form-group">
                            <label className="form-label">File name</label>
                            <input
                              className="form-control"
                              type="text"
                              name="name"
                              value={this.state.name}
                              onChange={this.handleChange}
                            />
                            <span />
                          </div>
                        </div>
                        <div class="col-md-12 col-lg-12 col-sm-12">
                          <div class="fileUp">
                            <label
                              for="fileUp1"
                              class="commonBtn text-center upload"
                            >
                              Upload File
                           </label>
                            <input
                              type="file"
                              id="fileUp1"
                              style={{ display: "none" }}
                              onChange={this.handleChangePopup}
                            />
                            {/* <p style={{ textAlign: "center" }}>hsfkjhsfj</p> */}
                          </div>
                        </div>

                        <div class="col-md-12 col-lg-12 col-sm-12 mt-2">
                          <button
                            type="button"
                            class="commonBtn float-right"
                            onClick={this.popupSubmit} disabled={!this.state.fileURL}
                          >
                            Submit
                         </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          }
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  loginDetails: state.loginReducer.loginDetails,
  getFilesDetails: state.uploadReducer.getFilesDetails,
  getpersonalDetails: state.healthrecordReducer.getpersonalDetails
});

const mapDispatchToProps = dispatch => ({
  getUploadFiles: payload => dispatch(getUploadFiles(payload)),
  addSportsdata: createsports => dispatch(addSportsdata(createsports)),
  getPersonaldata: listpersonal => dispatch(getPersonaldata(listpersonal)),
  updateSportsdata: updatesports => dispatch(updateSportsdata(updatesports)),
  deleteSportsdata: deletesports => dispatch(deleteSportsdata(deletesports)),
  handleFileDelete: (id, fileUrl, type) => dispatch(handleFileDelete(id, fileUrl, type))

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(sports);
