import React, { Component } from "react";
import MedicalSpecialList from  '../InnerMenu/MedicalSpecialistInfoMenu'
import { Switch, Route, Redirect } from "react-router-dom";


class MedicalSpecialistRouting extends Component {
  render() {
    return (
      <Switch>
        <Route path="/medical/MedicalSpecialList" component={MedicalSpecialList} />
       
      </Switch>
    );
  }
}

export default MedicalSpecialistRouting;
