import React, { Component } from "react";
import {doctorAppointmentListAPICall} from "../../service/appointment/action";
import { connect } from "react-redux";
import Loader from "react-loader-spinner";
import moment from 'moment';
import _ from 'lodash';
import { filter as _filter } from 'lodash';


class OfflineAppointments extends Component {
    constructor(props) {
        super(props)
        this.state = {
            doctorAppointmentList: [],
            openfeedback: false,
            anamnesis: "",
            objectives: "",
            diagnosis: "",
            treatment: "", scheduleLists: [],
            mainScheduleList:[],
            searchdate:moment().format("YYYY-MM-DD"),
            appointmentDate:"",
        }
        this.allInterval = []
    }
    componentWillMount = () => {
        var loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
        const { userInfo } = loginDetails;
        const doctorInfo =
        {
            //"doctorId": "1"
            "doctorId": userInfo.userId
        }
        this.props.doctorAppListMethod(doctorInfo);
    }
    componentWillReceiveProps = () => {
        const { appointmentDoctorLists, doctorName } = this.props.listDoctorAppointment
        let result = _.filter(appointmentDoctorLists, (value) => { 
            return value.scheduleType == "office meeting";
            })
        this.setState({ doctorAppointmentList: result  ,mainScheduleList:result},()=>this.searchAvailability())

    }
    searchAvailability = () => {
        const { searchdate, appointmentDoctorLists,mainScheduleList } = this.state;
        var result = [];
        result = _filter(mainScheduleList, function (list) {
            console.log("coming date",list.appointmentDate,"date",searchdate)

            return list.appointmentDate.includes(searchdate) ;
        })
        console.log("rwsulkt",result)
        
        this.setState({ doctorAppointmentList: result })

    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }
    render() {
        const { isLoading } = this.props
        const { doctorAppointmentList, anamnesis, objectives, diagnosis, treatment,searchdate,mainScheduleList  } = this.state;
        return (
            <React.Fragment>
                {isLoading && (
                    <div className="pop-mod">
                        <div className="loder-back">
                            <Loader
                                type="Circles"
                                color="#00BFFF"
                                height="100"
                                width="100"
                            />
                        </div>
                    </div>
                )}

                <div>
                    <div className="mainMenuSide" id="main-content">
                        <div className="wrapper">
                            <div className="container-fluid">
                                <div className="body-content">
                                    <div className="tomCard">
                                        <div className="tomCardHead">
                                            <h5 className="float-left">Booked Appointments</h5>

                                        </div>

                                        <div className="tab-menu-content tomCardBody">

                                            <div className="tab-menu-title">
                                                <ul>
                                                    <li className="menu1 active"><a href="JavaScript:Void(0);">
                                                        <p>List of Appointments </p>
                                                    </a></li>


                                                </ul>
                                            </div>
                                            <div className="tab-menu-content-section">
                                            <div id="content-1" style={{ display: "block" }}>
                                                <div class="row">
                                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-3 col-sm-12 col-12">
                                                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                                                        <div class="searchAvailablity mobile-width-100">
                                                            <input type="date" class="searchInputDate" name="searchdate" value={searchdate} onChange={this.handleChange}  />
                                                            <div class="calenderIcon"><i class="fas fa-calendar-alt"></i></div>
                                                           
                                                        </div>
                                                        <div>
                                                        <button type="button" class="commonBtn2 mobile-width-100" onClick={this.searchAvailability}> <i class="fa fa-search docPlus"></i> Search</button>
                                                        </div>
                                                        <div>
                                                        <button type="button" class="commonBtn2 mobile-width-100" onClick={()=> this.setState({doctorAppointmentList:mainScheduleList,searchdate:""})}> <i class="fa fa-eye docPlus"></i> Show all list</button>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    </div>

                                                    <div className="table-responsive">
                                                        <table className="table table-hover">
                                                            <thead className="thead-default">
                                                                <tr>
                                                                    <th>Appointment Date</th>
                                                                    <th>Timings</th>
                                                                    <th>Patient Name</th>
                                                                    <th>Status</th>
                                                                    <th>Appointment Type</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {
                                                                    doctorAppointmentList && doctorAppointmentList.map((each, i) => {
                                                                        let disabled;

                                                                        if (moment().format("YYYY-MM-DD") == each.appointmentDate) {
                                                                            disabled = false;
                                                                            var newInterval = setInterval(() => {
                                                                                const currentTime = moment();
                                                                                const startTime = moment(each.startTime, "HH:mm ");
                                                                                const endTime = moment(each.endTime, "HH:mm");
                                                                                const amIBetween = currentTime.isBetween(startTime, endTime);
                                                                                const beforeCheck = currentTime.isBefore(startTime)
                                                                                const afterCheck = currentTime.isAfter(endTime)
                                                                                if (document.getElementById("meeting" + i)) {
                                                                                    if (amIBetween) {
                                                                                        document.getElementById("meeting" + i).textContent = "Join Meeting"
                                                                                        document.getElementById("meeting" + i).disabled = false

                                                                                    } else if (beforeCheck) {
                                                                                        let duration = moment.duration(startTime.diff(currentTime))
                                                                                        let hours = parseInt(duration.asHours());
                                                                                        let minutes = parseInt(duration.asMinutes()) % 60;
                                                                                        let seconds =  parseInt(duration.asSeconds()) % 60;
                                                                            
                                                                                        let timeString = hours + ':' + minutes +':'+ seconds + ''
                                                                                    
                                                                                        document.getElementById("meeting" + i).textContent = "Meeting in " + timeString
                                                                                        document.getElementById("meeting" + i).disabled = true

                                                                                    } else if (afterCheck) {
                                                                                        document.getElementById("meeting" + i).textContent = "Meeting Ended"
                                                                                        document.getElementById("meeting" + i).disabled = true
                                                                                    }
                                                                                }

                                                                            }, 100)
                                                                            this.allInterval.push(newInterval)
                                                                        } else {
                                                                            disabled = true;
                                                                            const currentTime = moment();
                                                                            const beforeCheck = moment(each.appointmentDate).isAfter(currentTime)
                                                                            if (document.getElementById("meeting" + i)) {
                                                                                if (beforeCheck) {
                                                                                  
                                                                                        document.getElementById("meeting" + i).textContent = "Yet To Start"
                                                                              
                                                                                } else {
                                                                                  
                                                                                        document.getElementById("meeting" + i).textContent = "Meeting Ended"
                                                                                 
                                                                                }
                                                                            }

                                                                        }

                                                                        return (
                                                                            <tr key={i}>
                                                                                <td data-label="Appointment Date">{each.appointmentDate}</td>
                                                                                <td data-label="Start Time">{each.startTime} - {each.endTime}</td>
                                                                                <td data-label="Patient Name">{each.patientName}</td>
                                                                                <td data-label="status">{each.status} </td>
                                                                                <td data-label="Action">{each.scheduleType}</td>


                                                                            </tr>
                                                                        )

                                                                    })
                                                                }

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </React.Fragment>
        )
    }
}
const mapStateToProps = state => ({
    listDoctorAppointment: state.appointmentReducer.listDoctorAppointment,
    isLoading: state.loginReducer.isLoading,
    loginDetails: state.loginReducer.loginDetails,
});

const mapDispatchToProps = dispatch => ({
    doctorAppListMethod: (doctorInfo) => dispatch(doctorAppointmentListAPICall(doctorInfo)),
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(OfflineAppointments);