import React,{Component} from "react";

class DeleteModel extends Component{

    render(){
        return(
            <React.Fragment>
                 <div className={openallergyPop ? "modal show d-block" : "modal"} id="edit_btn">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label className="form-label">Are you sure want to delete</label>
                                    </div>
                                  
                                    <div className="form-actions">
                                        <button type="button" className="btn commonBtn mr-3" onClick={this.handleAllergy}>Yes</button>
                                        <button type="button" className="btn commonBtn mr-3" onClick={this.handleAllergy}>No</button>


                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default DeleteModel;