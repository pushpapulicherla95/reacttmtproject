import React , {Component} from 'react';
import { NavLink  } from 'react-router-dom';
import { withRouter } from "react-router";
import LandingHeader from '../common/LandingHeader';
class PrivacyPolicy extends Component{
    componentDidMount() {
        window.scrollTo(0, 0)
    }
    render(){
        return(
<>
<LandingHeader/>
<section class="disclaimer-banner about-banner banner d-flex justify-content-center align-items-center">
		<div class="container">
			<div class="bannerContent  flex-column">
				<h1>
					<span>Privacy and Policy </span>
				</h1>
			
			</div>
		</div>
	</section>
	<div class="emergencyAlert">
		<a href="" > <img src="images/emergencyCall.png"/> </a>
	</div>
<section class="about-content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-12">
				
				<div class="static_para">
                    <h2>
                        Information in accordance with Section 5 of the German Telemedia Act
                    </h2>
                    <div>
                        tomatomedical international UG (haftungsbeschränkt)<br/>
                        Schwarzhölzer Str. 1 <br/>
                        93474 Arrach <br/>
                        <br/>
                        <b>Phone:</b><br/>+499943943375 <br/>(Mo-Fr: 09:00 - 17:00)
                        <br/><br/>
                        <b>E-Mail:</b><br/> info@tomatomedical.com
                    </div>
                    <br/>
                    <b>Chief executive officer and authorized signatory:</b><br/>
                    Frau Renate Lemberger
                    <br/><br/>
                    <b>Court of registration and registration number:</b><br/>
                    Amtsgericht Regensburg: HRB 15004
                    <br/><br/>
                    <b>VAT identification number in accordance with section 27a of the German Value-added Tax Law:</b><br/>
                    DE 301767926 
    
                    <br/>
                    <br/> 
                    <div class="terms-of-use-section">
    
        <h2>
            Liability disclaimer/Release from liability
        </h2>
    
        <h3>
            1. Liability disclaimer
        </h3>
    
        <div>
            This application (app) was created with the greatest possible care. Despite this, no guarantee
            can be given for the freedom from errors and the accuracy of the information contained therein,
            the programming or data security.
        </div>
        <br/>
    
        <div>
            Any and all liability for damages that arise directly or indirectly from the use of this app is
            excluded, insofar as it is not due to malicious intent or gross negligence. In the event of
            damage caused by negligence - in particular property damage and financial losses - tomatomedical
            international UG, Schwarzhölzlstr. 1, 93474 Arrach, Germany and its vicarious agents are
            liable only in the case of violation of a material contractual obligation, but the amount is
            limited to the foreseeable damage typical of this type of contract at the time of conclusion of
            the contract.
        </div>
        <br/>
    
        <div>
            Insofar as this app refers to Internet websites or services of third parties operated by third
            parties, tomatomedical international UG assumes no responsibility for the content thereof.
        </div>
        <br/>
    
        <h3>
            2. Release from liability
        </h3>
        <div>
            The user of the app can voluntarily enter and process data (personal, health, insurance, advance
            health care directives, organ donation data, etc.) and, if desired, can store these in a cloud and
            transmit them to third parties while using this app. The app provider has no influence with regard
            to this use.
            The data saved on the mobile device or in the cloud are not suitable for diagnosis.
            In order to be able to use the app abroad, you must activate data roaming and have access to a
            mobile data network with your mobile device.
            <br/>
            I herewith undertake to indemnify the app provider tomatomedical international UG, Schwarzhölzlstr.
            1, 93474 Arrach, Germany, against claims of third parties asserted by these due to damage in
            conjunction with my use of the app; this applies in particular in compliance with data protection
            and privacy and copyright law. Furthermore, I undertake to pay for my own damages myself.
        </div>
        <br/>
        <br/>
        <div>
            Tomatomedical gives no guarantee for correctness of the included translations with the exception of German and English.
            Tomatomedical accepts no liability for damage of the user which occure by virtue of a translation error.
        </div>
        <br/><br/>
    
        <div>
            This release does not apply to harm to life, limb or health arising from a breach of obligation
            with malicious intent or negligent breach of obligation by tomatomedical international UG , any
            of its organs, representatives or vicarious agents.
        </div>
        <br/>
    
        <div>
            Furthermore, the release does not apply to property damage and financial losses arising from a
            breach of obligation with malicious intent or grossly negligent breach of obligation by
            tomatomedical international UG , Schwarzhölzlstr. 1, 93474 Arrach, Germany, any of its
            organs, representatives or vicarious agents.
        </div>
        <br/>
    
        <a name="datenschutz"></a>
        <h2>
            Data protection &amp; privacy declaration
        </h2>
    
        Last revised: 01.2018
        <br/>
        <br/>
        Data protection official:<br/>
        Alexander Bugl<br/>
        Bugl &amp; Kollegen GmbH<br/>
        Eifelstrasse 5<br/>
        93057 Regensburg<br/>
        <br/>
        E-Mail: datenschutz.bugl@tomatomedical.com
        <br/>
    
        <h3>
            1. Consent
        </h3>
    
        By consenting to this data protection and privacy declaration, you agree to permit us to use your
        personal data in the cases highlighted in bold print and underlined in this data protection and
        privacy declaration for the purposes described therein. Your consent applies in particular to the
        collection, storage, use and disclosure of your data as described in the relevant highlighted
        sections of this data protection and privacy declaration.
    
        <h3>
            2. Collection of data and responsible office
        </h3>
    
        This data protection &amp; privacy declaration informs you about which personal data are collected and
        processed by the provider during your use of the application (hereinafter referred to as &#147;app) with
        regard to the type, scope and purpose. Data are personal when they can be unambiguously assigned to
        a certain natural person. Youll find the legal basis for data protection and privacy in the German
        Federal Data Protection &amp; Privacy Act (BDSG) and the German Telemedia Act (TMG).
        <br/><br/>
        When you use the Emergency App, you will remain anonymous unless you voluntarily enter personal data
        and make it available. In the context of this data protection and privacy declaration, we use the
        designation &#147;personal data for information that can be assigned to a certain person and used for
        the identification of this person (e.g. name, address, e-mail, telephone number, IP address, health
        information, insurance data and similar information).
        <br/><br/>
        Personal data are collected, used and forwarded if this is legally permitted or if the users consent
        to the data collection.
        <br/><br/>
        Each registered user is responsible for his own data entered in the app.
        <br/><br/>
        The Emergency APP is a service of tomatomedical international UG . Should you have questions
        regarding the collection, processing or use of your personal data, for information, correction,
        blockage or deletion of data, please contact the responsible office:
        <br/>
    
        <div>
            tomatomedical international UG<br/><br/>
    
            Schwarzhölzlstr. 1<br/><br/>
    
            93474 Arrach<br/><br/>
    
            Phone number: +499943943375 <br/><br/>
    
            E-Mail: info@tomatomedical.com<br/><br/>
        </div>
    
        <h3>
            3. Collection of personal data
        </h3>
    
        You can download the Emergency App without having to set up a user account. When you share your
        personal data by entering them, you have decided not to remain anonymous.
        With your agreement to this data protection and privacy declaration and the entry of your personal
        data, you declare your consent to these data being transmitted to our servers in the Federal
        Republic of Germany and other servers around the globe, e.g. in the United States, and stored there.
        No medical or social data is stored outside of Germany. All cloud servers are located in Germany.
        We collect the following types of personal data to enable you to use and access our websites,
        applications, services and tools, to provide you with a more personalized and better user experience
        and to adapt content and advertising:
    
        <h4>
            a. Automatically recorded data
        </h4>
    
        When you access our app, information is occasionally automatically collected (i.e. not via
        registration), which is not assigned to a certain person (e.g. the operating system used, number of
        visits, average time spent on the website, publications retrieved). We use this information to
        determine the attractivity of our application and to improve its performance and content.
        <h4>
            b. Data provided by youn
        </h4>
        We record and store data that you enter in the Emergency App or forward to us during the use of our
        websites, applications, services or tools. These include:
        <ul>
            <li>
                data that you provide when setting up a user account or the registration for one of our
                services, such as your e-mail address, telephone number, mobile/cell phone number, postal
                address and health and insurance information (depending on the service used, further
                information and data such as organ donation, patient power of attorney etc.).
            </li>
            <li>
                data transmitted in the context of community discussions, chats, clarification of problems
                and correspondence/feedback via e-mail/fax/post.
            </li>
        </ul>
    
        <h4>
            c. Location data
        </h4>
        The app requires access to the location of your mobile device in order to determine your location in
        an emergency. Data concerning your location will be used only for processing your inquiry. Your
        location data is transmitted via an encrypted connection. On most mobile devices you can control or
        deactivated the location services in the &#147;Settings menu. When external crash sensors are integrated
        in the system, your current location (GPS position) is recorded, in order to be able to inform
        assistance services accordingly.
        <h3>
            4. Use of personal data
        </h3>
        The collection and use of personal data by the app is carried out for the purpose of giving you
        access to the use of our services, applications and tools, to perform the desired customer service
        and to provide you with relevant information about your user account.
        By consenting to this data protection and privacy declaration, you agree that we can use your
        personal data to:
        <br/>
        <ul>
            <li>
                give you access to the applications, services and tools and to perform the desired customer
                service via e-mail or by phone;
                to adapt, to measure and to improve our services, content and advertising;
            </li>
            <li>
                to compare the data for accuracy and to check it with the assistance of third-party
                providers;
                to inform you about about targeted marketing campaigns and
            </li>
            <li>
                advertising offers in accordance with your notification settings;
            </li>
            <li>
                to personalize, analyze and improve our advertising in accordance with your adaptation
                settings.
            </li>
        </ul>
    
        <h3>
            5. Use of Google Analytics and Google AdSense
        </h3>
        This app uses Google Analytics, a web analysis service of Google Inc. (&#147;Google), and Google
        AdSense, a service for integrating advertising from Google Inc. (&#147;Google). Google Analytics and
        Google AdSense use so-called "cookies", which are text files stored on your device you use for the
        app (mobile/cell phone, tablet, etc.) that facilitate an analysis of your use of the app. Google
        AdSense also uses so-called Web beacons (invisible graphics). With these Web beacons, information
        such as the rate of visitor traffic on the pages of this site can be analyzed.
        <br/>
        The information about your use of this app (including your IP address) generated by cookies and app
        beacons and the delivery of advertising formats are generally transmitted to a server in the United
        States and stored there.
        <br/><br/>
        Google will use this information to analyze your use of the app. Google may also transmit this
        information to third parties, insofar as this is legally required or insofar as third parties
        process this information on behalf of Google. When AdSense is used, a cookie is set by Google and
        the IP address is transmitted to Google without the possibility of anonymizing the IP address. In
        addition, Google AdSense tracks and saves individual user behavior.
        By using this app you declare your consent to the processing of the data collected about you by
        Google in the manner described above and for the aforementioned purpose.
        <br/>
    
        <h3>
            6. Disclosure of your data by us
        </h3>
    
        We disclose personal data as required to the legally permissible extent in order to comply with
        statutory requirements, to implement our principles of pursuing complaints about offers or content
        that infringe the rights of third parties, and to protect the rights, the property or the security
        and safety of others. As stated above, we will not disclose your personal data to third parties for
        marketing purposes without your explicit consent.
    
        <br/>
        We also have the right to forward your personal data to the following recipients:
        <ul>
            <li>
                service providers commissioned by us to assist us in our business operations (in particular
                advertising agencies).
            </li>
            <li>
                other third parties (e.g. physicians, health insurance companies, other insurance companies)
                to whom we send your data at your explicit request (or about whom you were explicitly
                notified and to whom you gave corresponding consent when using a particular service).
            </li>
            <li>
                other companies in conjunction with planned mergers, takeovers or company acquisition. In
                the event of such a merger, we will demand compliance with this data protection and privacy
                declaration with regard to your personal data from the newly merged company.
            </li>
        </ul>
    
        <h3>
            7. Access to, editing and correction of your data
        </h3>
    
        You can retrieve, review, edit or delete most of your personal data by logging into your user
        account. You are obligated to make any required changes or corrections to your data yourself without
        delay.
    
        <h3>
            8. Revocation, duration of data storage, data deletion
        </h3>
        Personal data entered in the app are stored only until the purpose for which you provided them to us
        has been fulfilled. You are free to delete the entered data; this may restrict the use of the app.
        You can revoke your consent to the use of your personal data in accordance with this data protection
        and privacy declaration at any time with effect for the future by deleting your user account. You
        can undertake the deletion yourself in your user account.
        <h3>
            9. Advertising
        </h3>
        The app is available in various versions and can be extended. The free version of the app as well as
        several additional tools can be used only when advertising is integrated. The integration of the
        advertising can be prevented by using the extended/paid version of the app.
        <h3>
            10. Security during data transmission, data transmission to a cloud
        </h3>
        Please note that data transmission on the Internet can have security vulnerabilities. Complete
        protection of data against access by third parties is not possible. Data is SSL-encrypted and
        transmitted on servers in the Federal Republic of Germany. With the app you can use an upload tool
        from your mobile device (smartphone, tablet) to store data (e.g. patient CDs with X-ray, CT and MRI
        images in DICOM format, etc.) in a cloud and use the app to access these data anywhere and at any
        time you can go online.
    
        <h3>
            11. Dispute Arbitration
        </h3>
        Dispute Arbitration according to Art. 14 Par. 1 ODR-VO: The European Commision provides a platform for Online Dispute Arbitration.
        You can find it at <a href="http://ec.europa.eu/consumers/odr/">http://ec.europa.eu/consumers/odr/</a>.
    
        <h3>
            12. Change in the data protection &amp; privacy declaration
        </h3>
        In the event of changes to this data protection and privacy declaration, we will inform you of the
        changed data protection and privacy declaration as well as the date on which it takes effect via an
        update.<br/>
        Should any ambiguities or cases of doubt arise and with regard to all claims or legal disputes, the
        German version of the terms and conditions apply exclusively, never the English version.
    
    
                    </div>
                    <br/>
                    <br/> 
                    <div class="copyright-resources">
    
        <div>
                Used Images:
                    
            <br/>
            <br/>
            <ul>            
                <li>55664382 - Flugzeug mit Reisepass und Impfausweis - © stockWERK / Fotolia</li>
                <li>136180792 - Alleine durch die Unterführung - Alone in the city - © Jürgen Fälchle / Fotolia</li>
                <li>94004446 - Flugzeug auf Reisepass und Impfausweis - © H. Brauer / Fotolia</li>
                <li>89363169 - Red deer stag silhouette in the mist - © mbridger68 / Fotolia</li>
                <li>19195043 - Stramme Waden - © Klaus Eppele / Fotolia</li>
                <li>91507759 - Portrait Of Male Doctor Standing In Hospital Corridor - © Monkey Business / Fotolia</li>
                <li>304416482 - barbed wire with blurred shapes of migrants - © Zoltan Major / Shutterstock</li>
                <li>156022556 - A motion blurred photograph of a young Asian Indian girl child patient on stretcher or gurney being pushed at speed through a hospital corridor by doctors &amp; nurses to an emergency room - © Spotmatik Ltd / Shutterstock</li>            
                <li>15714498 @ Andrey Kryuchkov / 123rf.com</li>
                <li>48312381 @ olegdudko / 123rf.com</li>
                <li>77227592 @ Sarote Pruksachat / 123rf.com</li>            
                <li>49383828 @ Katarzyna Białasiewicz / 123rf.com</li>
                <li>21161930 @ racorn / 123rf.com</li>
                <li>39899730 @ Andriy Popov / 123rf.com</li>            
                <li>69469876 @ Mirko Vitali / 123rf.com</li>
                <li>59136136 @ Katarzyna Białasiewicz / 123rf.com</li>
                <li>70165590 @ Christian Delbert / 123rf.com</li>
                <li>51610835 @ welcomia / 123rf.com</li>
                <li>25109370 @ Jean-Marie Guyon / 123rf.com</li>
                <li>43068231 @ Mark Bridger / 123rf.com</li>
                <li>44485812 @ morganka / 123rf.com</li>
                <li>46477085 @ senoldo / 123rf.com</li>
                <li>74186486 @ budabar / 123rf.com</li>
                <li>62107451 @ Joerg Huettenhoelscher / 123rf.com</li>
                <li>72183494 @ Andrey Ugadchikov / 123rf.com</li>
                <li>46049154 @ Aleksandr Davydov / 123rf.com</li>
                <li>72130544 @ maya23k / 123rf.com</li>
                <li>51654709 @ Igor Stevanovic / 123rf.com</li>            
                <li>73292812 @ Kurhan / 123rf.com</li>
                <li>8863355 @ Andriy Popov / 123rf.com</li>
                <li>36443210 @ Wavebreak Media Ltd  / 123rf.com</li>            
                <li>27393872 @ Andriy Popov / 123rf.com</li>
                <li>18728800 @ Arunas Gabalis / 123rf.com</li>
                <li>42883408 @ loganban / 123rf.com</li>            
                <li>5596561 @  gudella / 123rf.com</li>            
                <li>61867813 @ Andriy Popov / 123rf.com</li>            
                <li>70074223 @ Razvan Chisu / 123rf.com</li>            
                <li>46573325 @ Monika Wisniewska / 123rf.com</li>            
                <li>70377653 @ Jozef Polc / 123rf.com</li>            
                <li>16757391 @ Andreas Metz / 123rf.com</li>            
                <li>35821342 @ Anja Seibert / 123rf.com</li>            
                <li>62427676 @ Alexey Poprotsky / 123rf.com</li>            
                <li>51610928 @ Jan Andersen / 123rf.com</li>            
                <li>50563122 @ matimix / 123rf.com</li>            
                <li>49178330 @ BlueOrange Studio / 123rf.com</li>            
                <li>31534437 @ Tomas Marek / 123rf.com</li>            
                <li>50158308 @ Ian Allenden / 123rf.com</li>            
                <li>30621140 @ Dmitrii Kiselev / 123rf.com</li>            
                <li>46499231 @ Jaromír Chalabala / 123rf.com</li>            
                <li>47667334 @ Ievgenii Biletskyi / 123rf.com</li>            
                <li>19836272 @ Iuliia Sokolovska / 123rf.com</li>            
                <li>35261676 @ mihtiander / 123rf.com</li>            
                <li>23760545 @ kzenon / 123rf.com</li>            
                <li>10544200 @ lsantilli / 123rf.com</li>            
                <li>33748592 @ Katarzyna Białasiewicz / 123rf.com</li>            
                <li>14823451 @ nexus123 / 123rf.com</li>            
                <li>18086549 @ rioblanco / 123rf.com</li>
                <li>112836383 - Male dentist showing his female patient a dental implant © Drobot Dean / Fotolia</li>
                <li>66868147 - Rettungsdienst © k_rahn / Fotolia</li>
                <li>119332368 - devices web design responsive © georgejmclittle / Fotolia</li>
                <li>151205298 - hands with mobile © fotofabrika / Fotolia</li>
                <li>122261849 - Senior lady with a walker in autumn park © famveldman / Fotolia</li>
                <li>72333716 - Mountainbiker in the forest © KopoPhoto / Fotolia</li>
                <li>51987658 - disabled sprinter start block © mezzotint_fotolia / Fotolia</li>
                <li>21859479 - Cruise full ahead out of port © Lledó / Fotolia</li>
                <li>92571359 - Nurse caring for elderly person © Ingo Bartussek / Fotolia</li>
                <li>61379413 - Businessman pressing virtual button © Sikov / Fotolia</li>
                <li>67212174 - Cool Moutaineer © lassedesignen / Fotolia</li>
                <li>40159581 - medical insurance background © INFINITY / Fotolia</li>
            </ul>        
        </div>
                    </div> 
                </div>
			</div>
		</div>
		</div>	
</section>
<footer>
          <div class="footerLink">
            <div class="container">
              <ul>
                <li>
                  <NavLink to="/aboutus" >About </NavLink>
                </li>
                <li>
                <NavLink to="/partners" >For Partners </NavLink>
                </li>
                <li>
                <NavLink to="/contact" >Contact </NavLink>
                </li>
                <li>
                <NavLink to="/datasecurity" >Data Security </NavLink>
                </li>
                <li>
                <NavLink to="/privacypolicy" > Privacy Policy </NavLink>
                </li>
                <li>
                <NavLink to="/imprint">Imprint </NavLink>
                </li>
              </ul>
            </div>
          </div>

          <div class="footerCopyRights">
            <div class="container">
              <ul>
                <li>
                  <a href="">
                    <i class="fab fa-facebook-f" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-google-plus-g" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-instagram" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-twitter" />
                  </a>
                </li>
                <li>
                  <a href="">
                    <i class="fab fa-linkedin-in" />
                  </a>
                </li>
              </ul>
              <p>Copyrights @ 2019, All Rights Reserved</p>
            </div>
          </div>
        </footer>
</>
        );
    }
}
export default PrivacyPolicy;