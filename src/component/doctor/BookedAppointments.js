import React, { Component } from "react";
import { doctorAppointmentListAPICall, updateFeedback } from "../../service/appointment/action";
import { connect } from "react-redux";
import Loader from "react-loader-spinner";
import moment from 'moment';
import _ from 'lodash';
import { filter as _filter } from 'lodash';

class BoookedAppointments extends Component {


    constructor(props) {
        super(props)
        this.state = {
            doctorAppointmentList: [],
            openfeedback: false,
            anamnesis: "",
            objectives: "",
            diagnosis: "",
            treatment: "",
            scheduleLists: [],
            mainScheduleList:[],
            searchdate:moment().format("YYYY-MM-DD"),
            appointmentDate:"",

        }
        this.allInterval = []
    }
    componentWillMount = () => {
        var loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
        const { userInfo } = loginDetails;
        const doctorInfo =
        {
            //"doctorId": "1"
            "doctorId": userInfo.userId
        }
        this.props.doctorAppListMethod(doctorInfo);
    }

    componentDidMount = () => {
        console.log("inside component didmount : navigator.onLine : ", navigator.onLine);
        if (!navigator.onLine) {
            this.setState({ doctorAppointmentList: JSON.parse(localStorage.getItem('doctorAppointmentList')) });
        } else {
            var loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
            const { userInfo } = loginDetails;
            const doctorInfo =
            {
                //"doctorId": "1"
                doctorId: userInfo.userId
            }
            this.props.doctorAppListMethod(doctorInfo);
        }
    }

    componentWillReceiveProps = () => {

      const {searchdate}=this.state;
        const { appointmentDoctorLists, doctorName } = this.props.listDoctorAppointment
     
        let result = _.filter(appointmentDoctorLists, (value) => { 
            return value.scheduleType == "video call";
            })
           

        this.setState({ doctorAppointmentList: result ,mainScheduleList:result},()=>this.searchAvailability())
console.log("dsgfdsgfdhsgf",this.state.mainScheduleList)
    }
    componentWillUnmount = () => {
        this.allInterval.map((data) => {
            clearInterval(data)
        })
    }

    searchAvailability = () => {
        const { searchdate, appointmentDoctorLists,mainScheduleList } = this.state;
        var result = [];
        result = _filter(mainScheduleList, function (list) {
            console.log("coming date",list.appointmentDate,"date",searchdate)

            return list.appointmentDate.includes(searchdate) ;
        })
        console.log("rwsulkt",result)
        
        this.setState({ doctorAppointmentList: result })

    }


    openJitseeMeet = (data) => {
        let meetingRoom = data.meetingRoom;
        let doctorName = data.doctorName;
        localStorage.setItem("meetingRoom", meetingRoom);
        localStorage.setItem("doctorName", doctorName);
        localStorage.setItem("doctorId", data.doctorId);
        localStorage.setItem("patientId", data.patientId);

        this.props.history.push({ pathname: '/docdashboard', state: { submenuType: "DoctorJoinmeet" } })
    }

    // openFeedbackPop = () => {
    //     this.setState({ openfeedback: true });
    // }

    // modalPopUpClose = () => {
    //     this.setState({ openfeedback: false });
    // }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }
    // handleFeedback = () => {
    //     const {anamnesis,objectives,diagnosis,treatment} = this.state;

    //      let feedBack = {
    //        doctorId : localStorage.getItem("doctorId"),
    //         patientId : localStorage.getItem("patientId"),
    //         anamnesis:anamnesis,
    //         objectives:objectives,
    //         diagnosis:diagnosis,
    //         treatment:treatment

    //      }
    //      console.log("feedBack",feedBack);
    //    this.props.updateFeedback(feedBack);
    // this.setState({ openfeedback: false });
    // }

    render() {
        const { isLoading } = this.props
        const { doctorAppointmentList, anamnesis, objectives, diagnosis, treatment,searchdate,mainScheduleList } = this.state;
        console.log("id of pat and doc", localStorage.getItem("doctorId"), localStorage.getItem("patientId"));
        return (
            <React.Fragment>
                {isLoading && (
                    <div className="pop-mod">
                        <div className="loder-back">
                            <Loader
                                type="Circles"
                                color="#00BFFF"
                                height="100"
                                width="100"
                            />
                        </div>
                    </div>
                )}

                <div>
                    <div className="mainMenuSide" id="main-content">
                        <div className="wrapper">
                            <div className="container-fluid">
                                <div className="body-content">
                                    <div className="tomCard">
                                        <div className="tomCardHead">
                                            <h5 className="float-left">Booked Appointments</h5>

                                        </div>

                                        <div className="tab-menu-content tomCardBody">

                                            <div className="tab-menu-title">
                                                <ul>
                                                    <li className="menu1 active"><a href="JavaScript:Void(0);">
                                                        <p>List of Appointments </p>
                                                    </a></li>


                                                </ul>
                                            </div>
                                            <div className="tab-menu-content-section">
                                                <div id="content-1" style={{ display: "block" }}>
                                                <div class="row">
                                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-3 col-sm-12 col-12">
                                                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                                                        <div class="searchAvailablity mobile-width-100">
                                                            <input type="date" class="searchInputDate" name="searchdate" value={searchdate} onChange={this.handleChange}  />
                                                            <div class="calenderIcon"><i class="fas fa-calendar-alt"></i></div>
                                                           
                                                        </div>
                                                        <div>
                                                        <button type="button" class="commonBtn2 mobile-width-100" onClick={this.searchAvailability}> <i class="fa fa-search docPlus"></i> Search</button>
                                                        </div>
                                                        <div>
                                                        <button type="button" class="commonBtn2 mobile-width-100" onClick={()=> this.setState({doctorAppointmentList:mainScheduleList,searchdate:""})}> <i class="fa fa-eye docPlus"></i> Show all list</button>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <div className="table-responsive">
                                                        <table className="table table-hover">
                                                            <thead className="thead-default">
                                                                <tr>
                                                                    <th>Appointment Date</th>
                                                                    <th>Timings</th>
                                                                    <th>Patient Name</th>
                                                                    <th>Status</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {/* <tr>
                                                                    <td data-label="Appointment Date">2019-06-20</td>
                                                                    <td data-label="Start Time">13:00 - 14:00 - 13:00 - 14:00</td>
                                                                    <td data-label="Patient Name">John</td>
                                                                    <td data-label="status">Active </td>
                                                                    <td data-label="Action"><a title="Join Meeting" onClick={() => this.openFeedbackPop()} class="commonBtn2 ">Join Meeting</a></td>

                                                                </tr> */}
                                                                {
                                                                    doctorAppointmentList && doctorAppointmentList.map((each, i) => {
                                                                        // console.log(moment(each.appointmentDate).isAfter())
                                                                        let disabled;

                                                                        if (moment().format("YYYY-MM-DD") == each.appointmentDate) {
                                                                            disabled = false;
                                                                            var newInterval = setInterval(() => {
                                                                                const currentTime = moment();
                                                                                const startTime = moment(each.startTime, "HH:mm ");
                                                                                const endTime = moment(each.endTime, "HH:mm");
                                                                                const amIBetween = currentTime.isBetween(startTime, endTime);
                                                                                const beforeCheck = currentTime.isBefore(startTime)
                                                                                const afterCheck = currentTime.isAfter(endTime)
                                                                                if (document.getElementById("meeting" + i)) {
                                                                                    if (amIBetween) {
                                                                                        document.getElementById("meeting" + i).textContent = "Join Meeting"
                                                                                        document.getElementById("meeting" + i).disabled = false

                                                                                    } else if (beforeCheck) {
                                                                                        let duration = moment.duration(startTime.diff(currentTime))
                                                                                        let hours = parseInt(duration.asHours());
                                                                                        let minutes = parseInt(duration.asMinutes()) % 60;
                                                                                        let seconds =  parseInt(duration.asSeconds()) % 60;
                                                                            
                                                                                        let timeString = hours + ':' + minutes +':'+ seconds + ''
                                                                                    
                                                                                        document.getElementById("meeting" + i).textContent = "Meeting in " + timeString
                                                                                        document.getElementById("meeting" + i).disabled = true

                                                                                    } else if (afterCheck) {
                                                                                        document.getElementById("meeting" + i).textContent = "Meeting Ended"
                                                                                        document.getElementById("meeting" + i).disabled = true
                                                                                    }
                                                                                }

                                                                            }, 100)
                                                                            this.allInterval.push(newInterval)
                                                                        } else {
                                                                            disabled = true;
                                                                            const currentTime = moment();
                                                                            const beforeCheck = moment(each.appointmentDate).isAfter(currentTime)
                                                                            if (document.getElementById("meeting" + i)) {
                                                                                if (beforeCheck) {
                                                                                  
                                                                                        document.getElementById("meeting" + i).textContent = "Yet To Start"
                                                                              
                                                                                } else {
                                                                                  
                                                                                        document.getElementById("meeting" + i).textContent = "Meeting Ended"
                                                                                 
                                                                                }
                                                                            }

                                                                        }

                                                                        return (
                                                                            <tr key={i}>
                                                                                <td data-label="Appointment Date">{each.appointmentDate}</td>
                                                                                <td data-label="Start Time">{each.startTime} - {each.endTime}</td>
                                                                                <td data-label="Patient Name">{each.patientName}</td>
                                                                                <td data-label="status">{each.status} </td>
                                                                                <td data-label="Action"><button id={"meeting" + i} title="Join Meeting" disabled={disabled} onClick={() => this.openJitseeMeet(each)} className="commonBtn2 ">Join Meeting</button></td>


                                                                            </tr>
                                                                        )

                                                                    })
                                                                }

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* <div className={this.state.openfeedback ? "modal show d-block" : "modal"} >
                    <div class="modal-dialog">
                        <div class="modal-content modalPopUp">
                            <div class="modal-header borderNone">
                                <h5 class="modal-title">Update Feedback</h5>
                                <button type="button" class="popupClose" data-dismiss="modal" onClick={this.modalPopUpClose}><i class="fas fa-times"></i></button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12">

                                            <div class="form-group">
                                                <label class="commonLabel">Anamnesis</label>
                                                <textarea className="form-control" name="anamnesis" value={anamnesis} onChange={this.handleChange}></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Objectives</label>
                                                <textarea className="form-control" name="objectives" value={objectives} onChange={this.handleChange}></textarea>

                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Diagnosis</label>
                                                <textarea className="form-control" name="diagnosis" value={diagnosis} onChange={this.handleChange}></textarea>

                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="commonLabel">Treatment</label>
                                                <textarea className="form-control" name="treatment" value={treatment} onChange={this.handleChange}></textarea>

                                            </div>
                                        </div>

                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <button type="button" class="commonBtn float-right" onClick={this.handleFeedback}>Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div> */}

            </React.Fragment>
        )
    }
}
const mapStateToProps = state => ({
    listDoctorAppointment: state.appointmentReducer.listDoctorAppointment,
    isLoading: state.loginReducer.isLoading,
    loginDetails: state.loginReducer.loginDetails,
});

const mapDispatchToProps = dispatch => ({
    doctorAppListMethod: (doctorInfo) => dispatch(doctorAppointmentListAPICall(doctorInfo)),
    updateFeedback: (feedBack) => dispatch(updateFeedback(feedBack))
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BoookedAppointments);