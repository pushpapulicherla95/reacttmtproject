import URL from '../../asset/configUrl';
import axios from "axios";
import actionType from '../appointment/actionType';
import { toastr } from 'react-redux-toastr';
import { toastrOptions } from '../../component/common/toasteroptions';
import { showIsLoading, hideIsLoading } from '../login/action'
import { listScheduleInfo } from '../doctor/action'
import { sendEmailAPICall, sendSMSAPICall } from "../common/action";
import { getSearchDoctorDetails } from "../../service/medicalSpecialist/action";

//List of patient Appointments
export const patientAppointmentListAPICall = (patientInfo) => dispatch => {
    dispatch(showIsLoading());
    const configs = {
        method: 'post',
        url: URL.PATIENT_APPOINTMENT_LIST,
        data: patientInfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {

        if (res.status === 200) {
            localStorage.setItem('patientAppointmentList', JSON.stringify(res.data.appointmentDoctorLists));
            dispatch({
                type: actionType.LIST_APPOINTMENT_SUCCESS,
                payload: res.data
            })

            // toastr.success('Message', "Appoitments retrieved successfully", toastrOptions);
        }
        dispatch(hideIsLoading());
    }).catch(err => {
        dispatch({
            type: actionType.LIST_APPOINTMENT_FAILURE,
            payload: err
        })
        toastr.error("Server Problem", toastrOptions)
        dispatch(hideIsLoading());
    })
}

//Book Appointment
export const bookAppointmentAPICall = (appointmentInfo) => (dispatch, getState) => {
    const userLoginData = getState().loginReducer.loginDetails && getState().loginReducer.loginDetails.userInfo;
    dispatch(showIsLoading());
    const bookInfo = {
        scheduleId: appointmentInfo.scheduleId,
        patientId: appointmentInfo.patientId,
        doctorId: appointmentInfo.doctorId,
        scheduleType : appointmentInfo.scheduleType,
        status: "booked"
    }
    const configs = {
        method: 'post',
        url: URL.BOOK_APPOINTMENT,
        data: bookInfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }

    const scheduleInfo = {
        specialistId: appointmentInfo.specialistId,
        specialistName: appointmentInfo.specialistName
    }
    const emailInfo = {
        from: "venkatakarthik.j@gmail.com", // sender address
        to: "venkata.karthik@colanonline.com", // list of receivers
        subject: "Tomato Medical Email Notification", // Subject line
        text: "Tomato Medical Email Notification", // plain text body
    }
    const smsInfo = {
        message: 'This is the ship that made the Kessel Run in fourteen parsecs?',
        from: '+15017122661',
        to: '+15558675310'
    }
   
    axios(configs).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: actionType.BOOK_APPOINTMENT_SUCCESS,
                payload: res.data
            })
            toastr.success('Booking Status', res.data.message, toastrOptions);
            dispatch(getSearchDoctorDetails(scheduleInfo));
            dispatch(sendBookingMail({...appointmentInfo,userLoginData}));
         
        }
        dispatch(hideIsLoading());
    }).catch(err => {
        dispatch({
            type: actionType.BOOK_APPOINTMENT_FAILURE,
            payload: err
        })
        toastr.error("Server Problem", toastrOptions)
        dispatch(hideIsLoading());
    })
}

//List of doctor Appointments
export const doctorAppointmentListAPICall = (doctorInfo) => dispatch => {
    dispatch(showIsLoading());
    const configs = {
        method: 'post',
        url: URL.DOCTOR_APPOINTMENT_LIST,
        data: doctorInfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {

        if (res.status === 200) {
            localStorage.setItem('doctorAppointmentList', JSON.stringify(res.data.appointmentDoctorLists));
            dispatch({
                type: actionType.LIST_DOC_APPOINTMENT_SUCCESS,
                payload: res.data
            })

            // toastr.success('Message', "Appoitments retrieved successfully", toastrOptions);
        }
        dispatch(hideIsLoading());
    }).catch(err => {
        dispatch({
            type: actionType.LIST_DOC_APPOINTMENT_FAILURE,
            payload: err
        })
        toastr.error("Server Problem", toastrOptions)
        dispatch(hideIsLoading());
    })
}

//List of chat history
export const chatHistoryAPICall = (historyInfo) => dispatch => {
    dispatch(showIsLoading());
    const configs = {
      method: "post",
      url: URL.CHAT_HISTORY,
      data: historyInfo,
      headers: {
        "Content-Type": "application/json"
      }
    };
    axios(configs).then((res) => {
        
        if (res.status === 200) {
            dispatch({
                type: actionType.CHAT_HISTORY_SUCCESS,
                payload: res.data
            })

            toastr.success('Success', "Chat history retrieved successfully", toastrOptions);
        }
        dispatch(hideIsLoading());
    }).catch(err => {
        dispatch({
            type: actionType.CHAT_HISTORY_FAILURE,
            payload: err
        })
        toastr.error("Server Problem", toastrOptions)
        dispatch(hideIsLoading());
    })
}

export const updateFeedback = (feedback) => dispatch => {
    const configs = {
      method: "post",
      url: URL.UPDATE_CALL_FEEDBACK,
      data: feedback,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };

    axios(configs)
        .then(res => {
            if (res.data.status == "success") {
                dispatch({
                    type: actionType.UPDATE_FEEDBACK_SUCCESS,
                    payload: res.data
                })
                toastr.success('Success', res.data.message, toastrOptions);
            } else if (res.data.status == "failure") {
                toastr.error('error', "feed back updated successfully", toastrOptions);
            }
        })
        .catch(err => {
            dispatch({
                type: actionType.UPDATE_FEEDBACK_FAILURE,
                error: err
            })
            toastr.error("errr", toastrOptions)
        })

}
//show Patient List
export const listPatientInfo = listinfo => dispatch => {
    console.log("iam a patient List",listinfo)

    const configs = {
        method: 'post',
        url: URL.LIST_PATIENT_INFO,
        data: listinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
    console.log("iam a patient List",res.data.teleMedicineFormInforByDoctorId)
            dispatch({
                type: actionType.LIST_PATIENT_INFO_SUCCESS,
                payload: res.data.teleMedicineFormInforByDoctorId
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.LIST_PATIENT_INFO_FAILURE,
            payload: err
        })
    })
}
//update doctor feedback
export const updateDoctorfeedback = updaeinfo => dispatch => {

    const configs = {
        method: 'post',
        url: URL.UPDATE_DOCTOR_FEEDBACK,
        data: updaeinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    
    const listinfo = {
      "doctorId": updaeinfo.doctorId
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
    // console.log("iam a patient List",res.data.teleMedicineFormInforByDoctorId)
    dispatch(listPatientInfo(listinfo))
            dispatch({
                type: actionType.UPDATE_DOCTOR_FEEDBACK_SUCCESS,
                payload: res.data
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.UPDATE_DOCTOR_FEEDBACK_FAILURE,
            payload: err
        })
    })
}
//patientlist
export const listDoctorInfo = listinfo => dispatch => {
    console.log("iam a patient List",listinfo)

    const configs = {
        method: 'post',
        url: URL.LIST_DOCTOR_INFO,
        data: listinfo,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    }
    axios(configs).then((res) => {
        if (res.status === 200) {
    // console.log("iam a patient List",res.data.teleMedicineFormInforByDoctorId)
            dispatch({
                type: actionType.LIST_DOCTOR_INFO_SUCCESS,
                payload: res.data.teleMedicineFormInformationByPatient
            })
        }
    }).catch(err => {
        dispatch({
            type: actionType.LIST_DOCTOR_INFO_FAILURE,
            payload: err
        })
    })
}

const sendBookingMail = (payload) => dispatch => {
    axios.post(URL.BOOKINGAPPOINMENT_MAIL, payload)
        .then(response => {
            //toastr.success("Success", response.data.message, toastrOptions);
            console.log("response", response.data.message);
        })

}