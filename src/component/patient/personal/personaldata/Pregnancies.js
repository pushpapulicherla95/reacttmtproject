import React, { Component } from "react";
import { getUploadFiles, handleFileDelete } from "../../../../service/upload/action";
import {
  getPersonaldata,
  addPregnanciesdata,
  addListOfPrgnancies,
  updateListOfPrgnancies,
  deleteListOfPrgnancies
} from "../../../../service/healthrecord/action";
import { connect } from "react-redux";
import $ from "jquery";
import URL from "../../../../asset/configUrl";
import { onlyDate, onlyNumbers } from "../KeyValidation";
import { toastr } from "react-redux-toastr";
import axios from "axios";
import { awsPersonalFiles } from "../../../../service/common/action";
class Pregnancies extends Component {
  constructor(props) {
    super(props);
    // this.uid = userId;
    this.state = {
      uploadPopup: false,
      index: 1,
      tabone: true,
      tabtwo: false,
      tabthree: false,
      addfindingpop: false,
      noOfPregnancy: "",
      noOfAbortion: "",
      noOfChidren: "",
      dateOfLastMenstruation: "",
      papTestDone: "",
      dateOfLastpapTest: "",
      patientFindingListData: [],
      PregnanciesListData: [],
      editDetails: {},
      addchildpop: false,
      title: "",
      dateOfDiagnosis: "",
      estimatedDateOfBirth: "",
      birthDate: "",
      abortion: "",
      nameOfChild: "",
      height: "",
      weight: "",
      gender: "",
      childPlaceOfBirth: "",
      name: "",
      fileName: "",
      fileType: "",
      fileURL: "",
      Valid: false
    };
  }

  ValidationField = () => {
    const {
      noOfPregnancy,
      noOfAbortion,
      noOfChidren,
      dateOfLastMenstruation,
      papTestDone,
      dateOfLastpapTest,
      title,
      dateOfDiagnosis,
      estimatedDateOfBirth,
      birthDate,
      abortion,
      nameOfChild,
      height,
      weight,
      gender,
      childPlaceOfBirth } = this.state
    this.setState({
      Valid:
        title ||
        noOfPregnancy
      //   noOfAbortion||
      //   noOfChidren||
      //   dateOfLastMenstruation||
      //   papTestDone||
      //   dateOfLastpapTest||

      //   dateOfDiagnosis||
      //   estimatedDateOfBirth||
      //   birthDate||
      //   abortion||
      //   nameOfChild||
      //   height||
      //   weight||
      //   gender||
      //   childPlaceOfBirth

    });

  }
  handleAwsFiles = value => {
    window.open("https://data.tomatomedical.com/patientpersonal/" + value, '_blank');

  };
  popupSubmit = () => {
    // console.loog
    var phrase = this.state.fileURL;
    var myRegexp = /base64,(.*)/;
    var match = myRegexp.exec(phrase);
    var { userInfo } = this.props.loginDetails;
    const payload = {
      uid: userInfo.userId,
      z: match[1],
      fileName: this.state.fileName,
      fileType: this.state.fileType,
      name: this.state.name,
      category: "pregnancies"
    };
    this.setState({ uploadPopup: false });

    axios
      .post(URL.FILEPERSONAL_UPLOAD, payload)
      .then(response => {
        response.data.status === "success"
          ? toastr.success("Message", response.data.message)
          : toastr.error("Message", response.data.message);
        if (response.data.status === "success") {
          var { userInfo } = this.props.loginDetails;
          const uploadFiles = {
            uid: userInfo.userId,
            category: "pregnancies"
          };
          this.props.getUploadFiles(uploadFiles);
          this.setState({ uploadPopup: false });
          this.setState({
            name: "",
            fileName: "",
            fileType: "",
            fileURL: ""
          });
        }
      })
      .catch(error => {
        // this.setState({ uploadPopup: true });
      });
  };
  handleChangePopup = e => {
    var reader = new FileReader();
    var file = e.target.files[0];
    let name = file.name.split('.')[0]
    let type = "." + file.name.split('.')[1]
    reader.onload = () => {
      this.setState({
        fileURL: reader.result,
        fileName: name,
        fileType: type
      });
    };
    reader.readAsDataURL(file);
  };
  handleChanges = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  trigger = () => {
    this.setState({ uploadPopup: !this.state.uploadPopup });
  };
  handleChange = (e, index) => {
    this.setState({ [e.target.name]: e.target.value }, () => { this.ValidationField() });
  };
  findingpop = () => {
    this.setState({ addfindingpop: true }, () => { this.emptyState(); });
    $("body").addClass("modal-open");
  };
  Childpop = () => {
    this.setState({ addchildpop: true });
  };
  Childpopclose = () => {
    this.setState({ addchildpop: false });
  };
  tabOne = () => {
    this.setState({
      tabone: true,
      tabtwo: false,
      tabthree: false
    });
  };
  tabTwo = () => {
    this.setState({
      tabone: false,
      tabtwo: true,
      tabthree: false
    });
  };
  tabThree = () => {
    this.setState({
      tabone: false,
      tabtwo: false,
      tabthree: true
    });
  };
  modalPopUpClose = () => {
    this.setState({
      addfindingpop: false,
      editallergies: false
    }, () => this.emptyState());
    $("body").removeClass("modal-open");
  };

  componentWillMount() {
    const { userInfo } = this.props.loginDetails;

    const listpersonal = {
      uid: userInfo.userId
    };
    const uploadFiles = { uid: userInfo.userId, category: "pregnancies" };
    this.props.getUploadFiles(uploadFiles);
    this.props.getPersonaldata(listpersonal);
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps.getpersonalDetails.listOfPregnancy);
    if (
      nextProps.getpersonalDetails &&
      nextProps.getpersonalDetails.pregnancies != null &&
      nextProps.getpersonalDetails.pregnancies != 0
    ) {
      const listpersonal = nextProps.getpersonalDetails.pregnancies;
      let patientFindingListData = JSON.parse(listpersonal);
      this.setState({
        noOfPregnancy: patientFindingListData.noOfPregnancy,
        noOfAbortion: patientFindingListData.noOfAbortion,
        noOfChidren: patientFindingListData.noOfChidren,
        dateOfLastMenstruation: patientFindingListData.dateOfLastMenstruation,
        papTestDone: patientFindingListData.papTestDone,
        dateOfLastpapTest: patientFindingListData.dateOfLastpapTest
      });
    }
    if (
      nextProps.getpersonalDetails &&
      nextProps.getpersonalDetails.listOfPregnancy != null &&
      nextProps.getpersonalDetails.listOfPregnancy != 0
    ) {
      const PregnanciesList = nextProps.getpersonalDetails.listOfPregnancy;
      let PregnanciesListData = JSON.parse(PregnanciesList);
      this.setState({ PregnanciesListData: PregnanciesListData });
    }
  }
  handleData = () => {
    const {
      noOfPregnancy,
      noOfAbortion,
      noOfChidren,
      papTestDone,
      addchildpop,
      dateOfLastpapTest,
      dateOfLastMenstruation
    } = this.state;
    const { userInfo } = this.props.loginDetails;

    const createprenancies = {
      userId: userInfo.userId,
      pregnancyOverview: {
        noOfPregnancy: noOfPregnancy,
        noOfAbortion: noOfAbortion,
        noOfChidren: noOfChidren,
        papTestDone: papTestDone,
        dateOfLastpapTest: dateOfLastpapTest,
        dateOfLastMenstruation: dateOfLastMenstruation
      }
    };

    this.props.addPregnanciesdata(createprenancies);
  };
  handleAdd = () => {
    const {
      title,
      dateOfDiagnosis,
      estimatedDateOfBirth,
      birthDate,
      abortion,
      nameOfChild,
      height,
      weight,
      gender,
      childPlaceOfBirth
    } = this.state;

    const { userInfo } = this.props.loginDetails;
    const createprenancieslist = {
      uid: userInfo.userId,
      ListOfPregnancies: {
        title: title,
        dateOfDiagnosis: dateOfDiagnosis,
        estimatedDateOfBirth: estimatedDateOfBirth,
        birthDate: birthDate,
        abortion: abortion,
        nameOfChild: nameOfChild,
        gender: gender,
        childPlaceOfBirth: childPlaceOfBirth,
        weight: weight,
        height: height
      }
    };

    this.props.addListOfPrgnancies(createprenancieslist);
    this.modalPopUpClose();
  };
  modalPopupOpen = e => {
    this.setState({
      editallergies: true,
      editDetails: e,
      title: e.title,
      dateOfDiagnosis: e.dateOfDiagnosis,
      estimatedDateOfBirth: e.estimatedDateOfBirth,
      birthDate: e.birthDate,
      abortion: e.abortion,
      nameOfChild: e.nameOfChild,
      gender: e.gender,
      childPlaceOfBirth: e.childPlaceOfBirth,
      weight: e.weight,
      height: e.height
    });
    $("body").addClass("modal-open");
  };
  emptyState = () => {
    this.setState({
      title: "",
      dateOfDiagnosis: "",
      estimatedDateOfBirth: "",
      birthDate: "",
      abortion: "",
      nameOfChild: "",
      height: "",
      weight: "",
      gender: "",
      childPlaceOfBirth: ""
    });
  };
  handleUpdate = () => {
    const {
      title,
      dateOfDiagnosis,
      estimatedDateOfBirth,
      birthDate,
      abortion,
      nameOfChild,
      height,
      weight,
      gender,
      childPlaceOfBirth
    } = this.state;

    const { userInfo } = this.props.loginDetails;
    const updateprenancieslist = {
      uid: userInfo.userId,
      ListOfPregnancies: {
        title: title,
        dateOfDiagnosis: dateOfDiagnosis,
        estimatedDateOfBirth: estimatedDateOfBirth,
        birthDate: birthDate,
        abortion: abortion,
        nameOfChild: nameOfChild,
        gender: gender,
        childPlaceOfBirth: childPlaceOfBirth,
        weight: weight,
        height: height
      }
    };

    this.props.updateListOfPrgnancies(updateprenancieslist);

    this.modalPopUpClose();
  };
  handleDelete = e => {
    const { userInfo } = this.props.loginDetails;
    const deleteprenancieslist = {
      uid: userInfo.userId,
      ListOfPregnancies: {
        title: e.title,
        dateOfDiagnosis: e.dateOfDiagnosis,
        estimatedDateOfBirth: e.estimatedDateOfBirth,
        birthDate: e.birthDate,
        abortion: e.abortion,
        nameOfChild: e.nameOfChild,
        gender: e.gender,
        childPlaceOfBirth: e.childPlaceOfBirth,
        weight: e.weight,
        height: e.height
      }
    };
    this.props.deleteListOfPrgnancies(deleteprenancieslist);
  };
  render() {
    const {
      tabone,
      tabtwo,
      tabthree,
      addfindingpop,
      addfinding,
      noOfAbortion,
      noOfPregnancy,
      noOfChidren,
      dateOfLastMenstruation,
      papTestDone,
      dateOfLastpapTest,
      title,
      dateOfDiagnosis,
      editallergies,
      estimatedDateOfBirth,
      birthDate,
      abortion,
      nameOfChild,
      height,
      weight,
      gender,
      childPlaceOfBirth,
      PregnanciesListData, Valid
    } = this.state;
    const { ListOfPregnancies } = PregnanciesListData;
    console.log("coming............", this.state);
    return (
      <div className="" id="main-content">
        <div className="wrapper">
          <div className="container-fluid">
            <div className="body-content">
              <div className="tomCard">
                <div className="tomCardHead">
                  <h5 className="float-left">Pregnancies</h5>
                </div>

                <div className="tab-menu-content">
                  <div class="tab-menu-title">
                    <ul>
                      <li className={this.state.tabone ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabOne}>
                          <p>Pregnancies Overview</p>
                        </a>
                      </li>
                      <li className={this.state.tabtwo ? "menu1 active" : ""}>
                        <a href="#" onClick={this.tabTwo}>
                          <p>List of pregnancies</p>
                        </a>
                      </li>
                      <li className={this.state.tabthree ? "menu1 active" : ""}>
                        <a onClick={this.tabThree}>
                          <p>Attached Files</p>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                {tabone && (
                  <div className="tab-menu-content-section">
                    <div
                      id="content-1"
                      className={tabone ? "d-block" : "d-none"}
                    >
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Number of pregnancies <sup>*</sup></label>
                            <input
                              type="text"
                              class="form-control"
                              name="noOfPregnancy"
                              value={noOfPregnancy}
                              onChange={this.handleChange}
                            />
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Number of Abortions</label>
                            <input
                              type="text"
                              class="form-control"
                              name="noOfAbortion"
                              value={noOfAbortion} onKeyPress={onlyNumbers}
                              onChange={this.handleChange}
                            />
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Number of Children</label>
                            <input
                              type="text"
                              class="form-control"
                              name="noOfChidren"
                              value={noOfChidren}
                              onChange={this.handleChange} onKeyPress={onlyNumbers}
                            />
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Date of last Menstruation</label>
                            <input
                              type="date"
                              class="form-control"
                              name="dateOfLastMenstruation"
                              value={dateOfLastMenstruation}
                              onChange={this.handleChange}
                              onKeyPress={onlyDate}
                            />
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Pap-Test done</label>
                            <select
                              class="form-control"
                              name="papTestDone"
                              value={papTestDone}
                              onChange={this.handleChange}
                            >
                              <option value="">
                                Please select
                              </option>
                              <option value="1">Yes</option>
                              <option value="2">No</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                          <div class="form-group">
                            <label>Date of last Pap-Test</label>
                            <input
                              type="date"
                              class="form-control"
                              name="dateOfLastpapTest"
                              value={dateOfLastpapTest}
                              onChange={this.handleChange}
                              onKeyPress={onlyDate}
                            />
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-12">
                          <button
                            type="button"
                            class="commonBtn float-right"
                            onClick={this.handleData} disabled={!Valid}
                          >
                            Save
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                {tabtwo && (
                  <div className="tab-menu-content-section">
                    <div
                      id="content-1"
                      className={tabtwo ? "d-block" : "d-none"}
                    >
                      <button
                        type="button"
                        onClick={this.findingpop}
                        className="commonBtn2 float-right mb-2"
                      >
                        <i className="fa fa-plus" /> Add New Entry
                      </button>
                      <div className="table-responsive">
                        <table className="table table-hover">
                          <thead className="thead-default">
                            <tr>
                              <th>Title</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            {ListOfPregnancies &&
                              ListOfPregnancies.map((each, i) => (
                                <tr key={i}>
                                  <td data-label="startDate">{each.title}</td>
                                  <td data-label="Action">
                                    <a
                                      title="Edit"
                                      onClick={e => this.modalPopupOpen(each)}
                                      className="editBtn"
                                    >
                                      <span
                                        id={5}
                                        className="fa fa-edit"
                                      ></span>
                                    </a>
                                    <a
                                      title="Delete"
                                      className="deleteBtn"
                                      onClick={e => this.handleDelete(each)}
                                    >
                                      <i
                                        className="fa fa-trash"
                                        data-toggle="modal"
                                        data-target="#myModal2"
                                      ></i>
                                    </a>
                                  </td>
                                </tr>
                              ))}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                )}
                {this.state.tabthree && (
                  <div
                    id="content-2"
                    className={tabthree ? "d-block" : "d-none"}
                  >
                    <button
                      type="button"
                      className="commonBtn2 float-right mb-2"
                      onClick={this.trigger}
                    >
                      <i className="fa fa-plus" />
                      <span> Upload new files </span>{" "}
                    </button>
                    <div className="table-responsive">
                      <table className="table table-hover">
                        <thead className="thead-default">
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>FileName</th>

                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.props.getFilesDetails.fileDetails &&
                            this.props.getFilesDetails.fileDetails.map(
                              (each, i) => (
                                <tr>
                                  <td data-label="#">{i + 1}</td>
                                  <td data-label="Name"> {each.name}</td>
                                  <td data-label="FileName"> {each.fileName}</td>
                                  <td data-label="Action">
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.handleAwsFiles(each.fileUrl)
                                      }
                                    >
                                      <i class="fas fa-download" />
                                    </button>
                                    <button
                                      class="download_btn"
                                      onClick={() =>
                                        this.props.handleFileDelete(each.id, each.fileUrl, "pregnancies")
                                      }
                                    >
                                      <i class="fas fa-trash" />
                                    </button>
                                  </td>
                                </tr>
                              )
                            )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                )}
              </div>
              <div
                className={addfindingpop ? "modal show d-block" : "modal"}
                id="addEntry"
              >
                <div class="modal-dialog">
                  <div class="modal-content modalPopUp">
                    <div class="modal-header borderNone">
                      <h5 class="modal-title">Create New Entry</h5>
                      <button
                        type="button"
                        class="popupClose"
                        data-dismiss="modal"
                      >
                        <i class="fas fa-times"></i>
                      </button>
                      <button
                        type="button"
                        className="popupClose"
                        data-dismiss="modal"
                        onClick={this.modalPopUpClose}
                      >
                        <i class="fas fa-times" />
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="row">
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">
                                Title for list entry (e.g. Pregnancy Maria)<sup>*</sup>
                              </label>
                              <input
                                type="text"
                                class="form-control"
                                name="title"
                                value={title}
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">
                                Date of Diagnosis
                              </label>
                              <input
                                type="date"
                                class="form-control"
                                name="dateOfDiagnosis"
                                value={dateOfDiagnosis}
                                onChange={this.handleChange}
                                onKeyPress={onlyDate}
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">
                                Estimated Date of Birth
                              </label>
                              <input
                                type="date"
                                class="form-control"
                                name="estimatedDateOfBirth"
                                value={estimatedDateOfBirth}
                                onChange={this.handleChange}
                                onKeyPress={onlyDate}
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">Birthdate</label>
                              <input
                                type="date"
                                class="form-control"
                                value={birthDate}
                                name="birthDate"
                                onChange={this.handleChange}
                                onKeyPress={onlyDate}
                              />
                            </div>
                          </div>

                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">Abortion</label>
                              <input
                                type="text"
                                class="form-control"
                                name="abortion"
                                value={abortion}
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">Children</label>
                              {/* <table class="table">
                                                                <tr>
                                                                    <th>Name</th>
                                                                    <th>Gender</th>
                                                                    <th>Weight (in kg)</th>
                                                                    <th> Height (in cm)</th>
                                                                    <th>Actions</th>
                                                                </tr>
                                                                <tr>
                                                                    <td>djhfjkh</td>
                                                                    <td>M</td>
                                                                    <td>50kg</td>
                                                                    <td>150cm</td>
                                                                    <td>
                                                                        <a href="edit">Delete</a>
                                                                    </td>
                                                                </tr>
                                                            </table> */}
                            </div>
                          </div>
                          {/* <div class="col-md-12 col-lg-12 col-sm-12">
                                                        <a href="#" onClick={this.Childpop}><i class="fa fa-plus" ></i> Add child</a>
                                                    </div> */}
                          {/* { */}
                          {/* this.state.addchildpop && */}
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">
                                Name of the child{" "}
                              </label>
                              <input
                                type="text"
                                class="form-control"
                                name="nameOfChild"
                                value={nameOfChild}
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">Gender</label>
                              <select
                                class="form-control"
                                name="gender"
                                value={this.state.gender}
                                onChange={this.handleChange}
                              >
                                <option data-label="Please select" value="">
                                  Please select
                                </option>
                                <option data-label="Male" value="Male">
                                  Male
                                </option>
                                <option data-label="Female" value="Female">
                                  Female
                                </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div className="form-group">
                              <label> Child Place of birth</label>
                              <input
                                type="text"
                                className="form-control"
                                name="childPlaceOfBirth"
                                onChange={this.handleChange}
                                value={childPlaceOfBirth}
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">Weight (in kg)</label>
                              <input
                                type="text"
                                class="form-control"
                                name="weight"
                                value={this.state.weight}
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">Height (in cm)</label>
                              <input
                                type="text"
                                class="form-control"
                                name="height"
                                value={height}
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>
                          {/* <div class="col-md-12 col-lg-12 col-sm-12">
                                                                <button type="button" class="commonBtn" onClick={this.Childpopclose}>Cancel</button>
                                                                <button type="button" class="commonBtn float-right">Save Changes</button>
                                                            </div> */}
                        </div>
                        {/* } */}
                        <div class="col-md-12 col-lg-12 col-sm-12 mt-3">
                          <button
                            type="button"
                            class="commonBtn float-right"
                            onClick={this.handleAdd} disabled={!Valid}
                          >
                            Add
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              {/*  */}
              <div
                className={editallergies ? "modal show d-block" : "modal"}
                id="addEntry"
              >
                <div class="modal-dialog">
                  <div class="modal-content modalPopUp">
                    <div class="modal-header borderNone">
                      <h5 class="modal-title">Update</h5>
                      <button
                        type="button"
                        class="popupClose"
                        data-dismiss="modal"
                      >
                        <i class="fas fa-times"></i>
                      </button>
                      <button
                        type="button"
                        className="popupClose"
                        data-dismiss="modal"
                        onClick={this.modalPopUpClose}
                      >
                        <i class="fas fa-times" />
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="row">
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">
                                Title for list entry (e.g. Pregnancy Maria)
                              </label>
                              <input
                                type="text"
                                class="form-control"
                                name="title"
                                defaultValue={this.state.editDetails.title}
                                onChange={this.handleChange} readOnly
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">
                                Date of Diagnosis
                              </label>
                              <input
                                type="date"
                                class="form-control"
                                name="dateOfDiagnosis"
                                defaultValue={
                                  this.state.editDetails.dateOfDiagnosis
                                }
                                onChange={this.handleChange}
                                onKeyPress={onlyDate} readOnly
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">
                                Estimated Date of Birth
                              </label>
                              <input
                                type="date"
                                class="form-control"
                                name="estimatedDateOfBirth"
                                defaultValue={
                                  this.state.editDetails.estimatedDateOfBirth
                                }
                                onChange={this.handleChange}
                                onKeyPress={onlyDate}
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">Birthdate</label>
                              <input
                                type="date"
                                class="form-control"
                                defaultValue={this.state.editDetails.birthDate}
                                name="birthDate"
                                onKeyPress={onlyDate}
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>

                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">Abortion</label>
                              <input
                                type="text"
                                class="form-control"
                                name="abortion"
                                defaultValue={this.state.editDetails.abortion}
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">Children</label>
                              {/* <table class="table">
                                                                <tr>
                                                                    <th>Name</th>
                                                                    <th>Gender</th>
                                                                    <th>Weight (in kg)</th>
                                                                    <th> Height (in cm)</th>
                                                                    <th>Actions</th>
                                                                </tr>
                                                                <tr>
                                                                    <td>djhfjkh</td>
                                                                    <td>M</td>
                                                                    <td>50kg</td>
                                                                    <td>150cm</td>
                                                                    <td>
                                                                        <a href="edit">Delete</a>
                                                                    </td>
                                                                </tr>
                                                            </table> */}
                            </div>
                          </div>
                          {/* <div class="col-md-12 col-lg-12 col-sm-12">
                                                        <a href="#" onClick={this.Childpop}><i class="fa fa-plus" ></i> Add child</a>
                                                    </div> */}
                          {/* { */}
                          {/* this.state.addchildpop && */}
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">
                                Name of the child{" "}
                              </label>
                              <input
                                type="text"
                                class="form-control"
                                name="nameOfChild"
                                defaultValue={
                                  this.state.editDetails.nameOfChild
                                }
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">Gender</label>
                              <select
                                class="form-control"
                                name="gender"
                                value={this.state.editDetails.gender}
                                onChange={this.handleChange}
                              >
                                <option data-label="Please select" value="">
                                  Please select
                                </option>
                                <option data-label="Male" value="Male">
                                  Male
                                </option>
                                <option data-label="Female" value="Female">
                                  Female
                                </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div className="form-group">
                              <label> Child Place of birth</label>
                              <input
                                type="text"
                                className="form-control"
                                name="childPlaceOfBirth"
                                onChange={this.handleChange}
                                defaultValue={
                                  this.state.editDetails.childPlaceOfBirth
                                }
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">Weight (in kg)</label>
                              <input
                                type="text"
                                class="form-control"
                                name="weight"
                                defaultValue={this.state.editDetails.weight}
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                              <label class="commonLabel">Height (in cm)</label>
                              <input
                                type="text"
                                class="form-control"
                                name="height"
                                defaultValue={this.state.editDetails.height}
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>
                          {/* <div class="col-md-12 col-lg-12 col-sm-12">
                                                                <button type="button" class="commonBtn" onClick={this.Childpopclose}>Cancel</button>
                                                                <button type="button" class="commonBtn float-right">Save Changes</button>
                                                            </div> */}
                        </div>
                        {/* } */}
                        <div class="col-md-12 col-lg-12 col-sm-12 mt-3">
                          <button
                            type="button"
                            class="commonBtn float-right"
                            onClick={this.handleUpdate} disabled={!Valid}
                          >
                            Update
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              {/*  */}
            </div>
          </div>
        </div>
        <div
          className={this.state.uploadPopup ? "modal d-block" : "modal"}
          id="myModal"
        >
          <div class="modal-dialog">
            <div class="modal-content modalPopUp">
              <div class="modal-header borderNone">
                <h5 class="modal-title">Upload File</h5>
                <button
                  type="button"
                  className="close"
                  /* data-dismiss="modal" */
                  onClick={this.trigger}
                >
                  <i class="fas fa-times" />
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="form-group">
                        <label className="form-label">File name</label>
                        <input
                          className="form-control"
                          type="text"
                          id="name"
                          value={this.state.name}
                          name="name"
                          onChange={this.handleChanges}
                        />
                        <span />
                      </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <div class="fileUp">
                        <label
                          for="fileUp1"
                          class="commonBtn text-center upload"
                        >
                          Upload File
                        </label>
                        <input
                          type="file"
                          id="fileUp1"
                          onChange={this.handleChangePopup}
                          style={{ display: "none" }}
                        />
                        {/* <p style={{ textAlign: "center" }}>hsfkjhsfj</p> */}
                      </div>
                    </div>

                    <div class="col-md-12 col-lg-12 col-sm-12 mt-2">
                      <button
                        type="button"
                        class="commonBtn float-right"
                        disabled={!this.state.fileURL}
                        onClick={this.popupSubmit}
                      >
                        Submit
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  loginDetails: state.loginReducer.loginDetails,
  getpersonalDetails: state.healthrecordReducer.getpersonalDetails,
  getFilesDetails: state.uploadReducer.getFilesDetails
});

const mapDispatchToProps = dispatch => ({
  getPersonaldata: listpersonal => dispatch(getPersonaldata(listpersonal)),
  addPregnanciesdata: createprenancies =>
    dispatch(addPregnanciesdata(createprenancies)),
  addListOfPrgnancies: createprenancieslist =>
    dispatch(addListOfPrgnancies(createprenancieslist)),
  getUploadFiles: payload => dispatch(getUploadFiles(payload)),
  updateListOfPrgnancies: updateprenancieslist =>
    dispatch(updateListOfPrgnancies(updateprenancieslist)),
  deleteListOfPrgnancies: deleteprenancieslist =>
    dispatch(deleteListOfPrgnancies(deleteprenancieslist)),
  handleFileDelete: (id, fileUrl, type) => dispatch(handleFileDelete(id, fileUrl, type))

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Pregnancies);
