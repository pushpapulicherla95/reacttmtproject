import actionType from "../../service/insurance/actionType";

const initialState = {
  health: "",
  insuranceAccident: "",
  additionalInsurance: "",
  getAllinsuranceDetails: "",
  healthinsuranceDetails: "",
  additionalinsuranceDetails: "",
  accidentinsuranceDetails: "",
};

const insuranceReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.HEALTHINSURANCE_SUCCESS:
      return {
        ...state,
        healthinsuranceDetails: action.payload
      };
    case actionType.HEALTHINSURANCE_FAILURE:
      return {
        ...state,
        healthinsuranceDetails: action.error
      };
    case actionType.ADDITIONALINSURANCE_SUCCESS:
      return {
        ...state,
        additionalinsuranceDetails: action.payload
      };
    case actionType.ADDITIONALINSURANCE_FAILURE:
      return {
        ...state,
        additionalinsuranceDetails: action.error
      };
    case actionType.ACCIDENTINSURANCE_SUCCESS:
      return {
        ...state,
        accidentinsuranceDetails: action.payload
      };
    case actionType.ACCIDENTINSURANCE_FAILURE:
      return {
        ...state,
        accidentinsuranceDetails: action.error
      };
    case actionType.GETALLINSURANCE_SUCCESS:
      console.log("GETALLINSURANCE_SUCCESS",typeof JSON.parse(action.payload.healthInsurance));
      return {
        ...state,
        healthinsuranceDetails: JSON.parse(action.payload.healthInsurance),
        insuranceAccident: JSON.parse(action.payload.insuranceAccident),
        additionalInsurance: JSON.parse(action.payload.additionalInsurance)
      };
    case actionType.GETALLINSURANCE_FAILURE:
      return {
        ...state,
        getAllinsuranceDetails: action.error
      };

    default:
      return state;
  }
};
export default insuranceReducer;
