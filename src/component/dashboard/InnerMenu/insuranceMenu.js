import React from "react";
import InsuranceInnerRouting from "../innerrouting/InsuranceRouting";
import Logout from "../../common/Logout";
import { NavLink } from "react-router-dom";
import AsideMenu from "../../common/AsideMenu"

class InsuranceMenu extends React.Component {
//   render() {
//     return (
//       <div className="inner-pageNew">
//         <section>
//           <header className="header">
//             <div className="brand">
//               <a href="Dashboard.html" className="backArrow">
//                 <i className="fas fa-chevron-left" />
//               </a>
//               <div className="mobileLogo">
//                 <a href="Dashboard.html" className="logo">
//                   <img src="../images/logo.jpg" />{" "}
//                 </a>
//               </div>
//             </div>
//             <div className="top-nav">
//               <div className="dropdown float-right mr-2">
//                 <a className="dropdown-toggle" data-toggle="dropdown">
//                   Patient Name
//                 </a>
//                 <Logout />
//               </div>
//             </div>
//           </header>

//           <aside className="sideMenu">
//             <ul className="sidemenuItems">
//               <li>
//                 <a
//                   href="JavaScript:Void(0);"
//                   onClick={() => this.props.history.push("/personal/data")}
//                   title="Personal"
//                 >
//                   <img src="../images/icon2-color.png" />
//                 </a>
//                 <div className="menu-text">
//                   <span> Personal </span>
//                 </div>
//               </li>
//               <li>
//                 <a href="" title="Telemedicine">
//                   <img src="../images/icon5-color.png" />
//                 </a>
//                 <div className="menu-text">
//                   <span> Telemedicine </span>
//                 </div>
//               </li>
//               <li>
//                 <a href="" title="Health Records">
//                   <img src="../images/icon1-color.png" />
//                 </a>
//                 <div className="menu-text">
//                   <span>Health Records</span>
//                 </div>
//               </li>
//               <li>
//                 <a href="" title="Pharmacy">
//                   <img src="../images/icon4-color.png" />
//                 </a>{" "}
//                 <div className="menu-text">
//                   <span> Pharmacy </span>
//                 </div>
//               </li>
//               <li>
//                 <a href="" title="Services">
//                   <img src="../images/icon7-color.png" />
//                 </a>
//                 <div className="menu-text">
//                   <span> Services </span>
//                 </div>
//               </li>
//               <li>
//                 <a href="" title="Share Data">
//                   <img src="../images/icon6-color.png" />
//                 </a>
//                 <div className="menu-text">
//                   <span> Share data </span>
//                 </div>
//               </li>
//             </ul>
//           </aside>
//           <div className="inner-subMenu">
//             <div class="inner-subMenu">
//               <ul class="sid-subMenuitems">
//                 <li>
//                   <NavLink
//                     to="/insurance/healthInsurance"
//                   >
//                     Health Insurance Company
//                   </NavLink>
//                 </li>
//                 <li>
//                   <NavLink
//                     onClick={() =>
//                       this.props.history.push({
//                         pathname: "/insurance/additionalInsurance"
//                       })
//                     }
//                     title="Core Data"
//                     id="add-cli"
//                   >
//                     Additional Insurance
//                   </NavLink>
//                 </li>
//                 <li>
//                   <NavLink
//                     onClick={() =>
//                       this.props.history.push({
//                         pathname: "/insurance/insuranceAccident"
//                       })
//                     }
//                     title="Job"
//                     id="ins-cli"
//                   >
//                     Insurance Accident
//                   </NavLink>
//                 </li>
//               </ul>
//             </div>
//           </div>
//           <InsuranceInnerRouting />
//         </section>
//       </div>
//     );
//   }
// }
constructor(props) {
  super(props);

}
handleChange = (path) => {
  const clientWidth = window.innerWidth;
  if (clientWidth < 479) {
      document.getElementById("submenuHideOnMobile").style.display = "none";
  }
  this.props.history.push(path)
}
handleArrow = () => {
  const clientWidth = window.innerWidth;
  if (clientWidth < 479) {
      if (document.getElementById("submenuHideOnMobile").style.display == "none") {
          document.getElementById("submenuHideOnMobile").style.display = "block";
      } else {
          this.props.history.push("/submenu");
          document.getElementById("submenuHideOnMobile").style.display = "none";
      }
  }
}
render() {
  return (
      <div className="inner-pageNew">
          <section>
              <header className="header">
                  <div className="brand">
                      <a href="javascript:void(0);" className="backArrow" onClick={this.handleArrow }>
                          <i className="fas fa-chevron-left" ></i>
                      </a>
                      <div className="mobileLogo">
                          <a href="/mainmenu" className="logo">
                              <img src="../images/logo.jpg" /> </a>
                      </div>

                  </div>
                  <div className="top-nav">

                      <div className="dropdown float-right mr-2" >
                          <a className="dropdown-toggle" data-toggle="dropdown">
                              Patient Name
                      </a>
                          <Logout/>
                      </div>
                  </div>
              </header>
              <AsideMenu/>
              {/* <aside className="sideMenu">
                  <ul className="sidemenuItems">
                      <li><a href="/mainmenu" onClick={() => this.props.history.push("/personal/data")} title="Personal"><img src="../images/icon2-color.png" /></a><div className="menu-text"><span>Personal</span></div></li>
                      <li><a href="/mainmenu"  title="Telemedicine" ><img src="../images/icon5-color.png" /></a><div className="menu-text"><span>Telemedicine</span></div></li>
                      <li><a href="/mainmenu" title="Health Records"><img src="../images/icon1-color.png" /></a><div className="menu-text"><span>Health Records</span></div></li>
                      <li><a href="/mainmenu" title="Pharmacy"><img src="../images/icon4-color.png" /></a> <div className="menu-text"><span>Pharmacy</span></div></li>
                      <li><a href="/mainmenu" title="Find Your medical specialist"><img src="../images/icon7-color.png" /></a><div className="menu-text"><span>Find Your medical specialist</span></div></li>

                    <li><a href="/mainmenu" title="Services"><img src="../images/icon7-color.png" /></a><div className="menu-text"><span>Services</span></div></li>
                      <li><a href="/mainmenu" title="Share Data"><img src="../images/icon6-color.png" /></a><div className="menu-text"><span>Share data</span></div></li>
                  </ul>

              </aside> */}
              <div className="inner-subMenu" id="submenuHideOnMobile">
                  <ul className="sid-subMenuitems">
                      <li><a title="Personal" onClick={() => this.handleChange("/insurance/healthInsurance")}>  Health Insurance Company</a></li>
                      <li><a title="Core Data" onClick={() => this.handleChange("/insurance/additionalInsurance")}> Additional Insurance</a></li>
                      <li><a title="Job" onClick={() => this.handleChange("/insurance/insuranceAccident")}>Insurance Accident</a></li>

                  </ul>
              </div>
              <InsuranceInnerRouting />
          </section>
      </div >
  )
}

}

export default InsuranceMenu;
