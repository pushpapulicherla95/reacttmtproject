
import React, { Component } from "react";
import axios from "axios";
import CreatableSelect from "react-select/lib/Creatable";
import { toastr } from "react-redux-toastr";
import URL from '../../../asset/configUrl';
import { CSSTransition } from 'react-transition-group';

const components = {
    DropdownIndicator: null,
};

const createOption = label => ({
    label,
    value: label
});

class TextRobotSecondView extends React.Component{

  constructor(props){
      super(props)
      this.state={
          inputValue: "",
          selected_id: "",
          value: [],
          allSymptomKeys: [],
          selectedSymptomKeyData: null,
          tabIndex: 0,
          isLoading: false,
          countWebSite: 2
      }
      this.toasterOptions = {
        timeOut: 2000,
        newestOnTop: true,
        position: 'top-right',
        transitionIn: 'bounceIn',
        transitionOut: 'bounceOut',
        progressBar: false,
        closeOnToastrClick: false,
      }

  }  

  allsymptomKeywordList=()=>{
    axios.get(URL.TEXT_ROBOT_LIST).then((response) => {
        const keyArray=response.data.key
        let optionKeyToDisplay=[]
        keyArray.map((data)=>{
            optionKeyToDisplay.push({
             value:data[0],label:data[0]
            })
        })
        this.setState({
            allSymptomKeys:optionKeyToDisplay
        })
     })
  }
  componentWillMount(){
     this.allsymptomKeywordList()
  }
  handleInputChange(inputValue) {
    this.setState({ inputValue });
  }

  copyDivToClipboard = (elem) => {
      var range = document.createRange();
      range.selectNode(document.getElementById(elem));
      window.getSelection().removeAllRanges();
      window.getSelection().addRange(range);
      document.execCommand("copy");
      window.getSelection().removeAllRanges();
      toastr.success("Message", "Copied to clipboard.", {
        timeOut: 1000,
        newestOnTop: true,
        position: 'top-right',
        transitionIn: 'bounceIn',
        transitionOut: 'bounceOut',
        progressBar: false,
        closeOnToastrClick: false,
      });
  }
  //Mulitselect box
  handleKeyDown(event) {
      const { inputValue, value } = this.state;
      if (!inputValue) return;
      switch (event.key) {
          case "Enter":
          case "Tab":
          case ",":
              this.setState({
                  inputValue: "",
                  value: [...value, createOption(inputValue)]
              }, () => {
              });
              event.preventDefault();
              break;
          default:
              break;
      }
  }
  
  handleChange(value, actionMeta) {
      if ((actionMeta.action = "clear")) {
          this.setState({ value: [] });
      }
      this.setState({ value: value }, () => {
      });
  }

  onSelectSymptomWord=(value,index)=>{
    
      const allSymptomKeys=this.state.allSymptomKeys;
      allSymptomKeys.map((value,i)=>{
          try{
            if(index == i){
                document.getElementById("keyText"+i).classList.add("active")
              }else{
                document.getElementById("keyText"+i).classList.remove("active")
              }
          }catch(e){

          }
      })
      const payload = {
          searchKey: value
      }
      const configs = {
          method: 'post',
          url: URL.GET_KEY_SEARCH_LIST,
          data: payload,
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
      }
      axios(configs).then((response) => {
          this.setState({
              selectedSymptomKeyData: response.data
          })
      })
  }

  onKeyWordSearch = () => {
      if (this.state.value.length != 0) {
          this.setState({ isLoading: true }, () => {
              const allDataArray = this.state.value.map((data) => {
                  return data.value
              })
              const payload = {
                  "searchKey": allDataArray,
                  "count":this.state.countWebSite
              }
              const configs = {
                  method: 'post',
                  url: URL.TEXT_ROBOT_SEARCH,
                  data: payload,
                  headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
              }
              axios(configs).then((response) => {
                  this.setState({ isLoading: false })
                  toastr.success("Message", "Successfully Key Search done.", this.toasterOptions);
                  this.allsymptomKeywordList()
              }).catch(err => {
                  toastr.error("Error", "Error on retriving data", this.toasterOptions);
                  this.setState({ isLoading: false })
              })
          })
      }else{
        toastr.warning("Please include any keyword", this.toasterOptions);
      }
  }

  onTabChange=(currentIndex)=>{
     this.setState({
         tabIndex:currentIndex
     })
  }

  render(){

      const { selectedSymptomKeyData } = this.state
      return (
          <div className="col-lg-6 col-md-6 col-sm-12 col-12 symptomsQAEntry">
              <div className="row">
                  <div className="col-lg-12 col-md-12 col-12 col-sm-12 mb-3">
                     
                      <button type="button" onClick={()=> this.props.onClickHideButton()} class="commonBtn">Hide TextRobot</button>
                     
                  </div>
              </div>
              <div className="row">
                  <div className="col-lg-12 col-md-12 col-12 col-sm-12">
                      <div class="row"><div class="col-lg-7 col-md-12 col-12 col-sm-12">
                      <label>Search </label>
                      <CreatableSelect
                          components={components}
                          isClearable
                          isMulti
                          noOptionsMessage={() => null}
                          onChange={(e)=> this.setState({value:e})}
                          placeholder="Type Symptom Name and press enter..."
                          isLoading={this.state.isLoading}
                      />
                      </div>
                          <div class="col-lg-3 col-md-12 col-12 col-sm-12">
                              <label className="text-center w-100">No of Sites</label>
                              <div class="d-flex incr-btn">
                                  <button className="incBtn1" onClick={() => {
                                      if (this.state.countWebSite != 2) {
                                          this.setState({ countWebSite: +this.state.countWebSite - 1 })
                                      }
                                  }}>-</button>
                                  <input  type="text" style={{textAlign: "center"}} onChange={(e) => {
                                     +e.target.value > 0 &&  this.setState({
                                          countWebSite: e.target.value
                                      })
                                  }} value={this.state.countWebSite} className="form-control123" />
                                  <button className="incBtn2" onClick={() => {
                                      this.setState({ countWebSite: +this.state.countWebSite + 1 })
                                  }}>+</button></div>
                          </div>
                          <div class="col-lg-2 col-md-12 col-12 col-sm-12">
                              <button type="button" style={{
                                  marginTop: "30px",

                              }} class="commonBtn float-right" disabled={this.state.isLoading} onClick={() => this.onKeyWordSearch()}>{!this.state.isLoading ? "Search" : "Loading..."}</button>
                          </div>
                      </div>
                  </div>
              </div>
              <div className="row mt-4">
                  <div className="col-lg-12 col-md-12 col-12 col-sm-12">
                      <div className="form-group">
                          <label>Results of TextRobot </label>
                      </div>
                  </div>
              </div>
              <div className="row">
                  <div className="col-lg-12 col-md-12 col-12 col-sm-12">
                      <div className="txtrbtresult">
                          <div className="row">
                              {this.state.allSymptomKeys.map((data,index) => {
                                  return (
                                      <div className="col-lg-6 col-md-6 col-12 col-sm-12">
                                          <a>
                                              <div onClick={() => {
                                                  this.onSelectSymptomWord(data.value,index)
                                              }} id={"keyText"+index} className="listTextRobot">
                                                  <h5>{data.value}</h5>
                                              </div>
                                          </a>
                                      </div>
                                  )
                              })}                  
                          </div>
                      </div>
                  </div>
              </div>
              <CSSTransition
                  in={selectedSymptomKeyData}
                  timeout={300}
                  classNames="alert"
                  unmountOnExit

              >
                  {selectedSymptomKeyData && <div className="row mt-3">
                      <div className="col-lg-12 col-md-12 col-12 col-sm-12 mt-3 symptomsQAEntryTab">
                          <ul>
                              <li className={this.state.tabIndex == 0 && 'active'} onClick={() => this.onTabChange(0)}>TextRobot Text</li>
                              <li className={this.state.tabIndex == 1 && 'active'} onClick={() => this.onTabChange(1)}>Orginal Text</li>
                              <li className={this.state.tabIndex == 2 && 'active'} onClick={() => this.onTabChange(2)}>Urls</li>
                          </ul>
                      </div>
                      <div className="col-lg-12 col-md-12 col-12 col-sm-12 ">
                          <div className="clr1002203">
                              <div className="Qusset">
                                  <div className="form-group">
                                      <div className="row">
                                          <div className="col-lg-12 col-md-6 col-12 col-sm-12">
                                              {(() => {
                                                  switch (this.state.tabIndex) {
                                                      case 0: {
                                                          return (
                                                              <div className="listTextRobot1">
                                                                  <h5>{selectedSymptomKeyData.key}<span className="doc_txt_rbt">
                                                                      <a onClick={() => {
                                                                          this.copyDivToClipboard("TextRobotText")
                                                                      }} title="Copy all"><i className="fas fa-copy"></i></a>
                                                                  </span></h5>
                                                                  <p id="TextRobotText">
                                                                      {selectedSymptomKeyData.textRobotText}
                                                                  </p>
                                                              </div>
                                                          )
                                                          break;
                                                      }
                                                      case 1: {
                                                          return (
                                                              <div className="listTextRobot1">
                                                                  <h5>{selectedSymptomKeyData.key}<span className="doc_txt_rbt">
                                                                      <a onClick={() => {
                                                                          this.copyDivToClipboard("OrginalText")
                                                                      }} title="Copy all"><i className="fas fa-copy"></i></a>
                                                                  </span></h5>
                                                                  <p id="OrginalText">
                                                                      {selectedSymptomKeyData.orginalText}
                                                                  </p>
                                                              </div>
                                                          )
                                                          break;
                                                      }
                                                      case 2: {
                                                          try {
                                                              const urlData = JSON.parse(selectedSymptomKeyData.url)
                                                              return (
                                                                  <div>
                                                                      {urlData.url.map((url) => {
                                                                          return <div className="linkSect"><a href={url} target={"_blank"}> {url}</a> </div>
                                                                      })}
                                                                  </div>
                                                              )
                                                          } catch (e) {

                                                          }
                                                          break;
                                                      }

                                                      default:
                                                          break
                                                  }

                                              })()}

                                          </div>

                                      </div>

                                  </div>
                              </div>
                          </div>
                      </div>

                  </div>}
              </CSSTransition>

          </div>
      )
  }
}

export default  TextRobotSecondView;