const express = require('express');
const chatRouter = express.Router();
const { chatSave,chatHistory } = require('../controller/chat/chat.api');

chatRouter.route('/save')
    .post(chatSave)
chatRouter.route('/history')
    .post(chatHistory)
module.exports = chatRouter;